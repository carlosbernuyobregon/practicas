<br><br>
<div class="form-group">
    <label for="name">Nombre categoria</label>
    <input wire:model='name' type="text" class="form-control">
    @error('name') <span class="alert-danger">{{ $message }}</span> @enderror
</div>
<button wire:click="showForm" type="button" class="btn btn-dark">Cancelar</button>
@if($edit)
<button wire:click="save" type="button" class="btn btn-primary">Guardar</button>
@else
<button wire:click="store" type="button" class="btn btn-primary">Guardar</button>
@endif
<br><br>