<div wire:ignore.self class="modal fade" id="createpost" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Crear un post</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="form-group">
                    <label for="title">Titulo</label>
                    <input wire:model="title" type="text" name="title" class="form-control" id="title" aria-describedby="name">
                    @error('title')
                    <span class="badge alert-danger"> {{ $message }} </span>
                    @enderror
                    @if (session()->has('message'))
                    <span class="alert alert-danger mt-2 p-0">
                        {{ session('message') }}
                    </span>
                    @endif
                </div>
                <div class="form-group">
                    <label for="text">Contenido</label>
                    <textarea wire:model="text" name='text' class="form-control" id="content" rows="3"></textarea>
                    @error('text')
                    <span class="badge alert-danger"> {{ $message }} </span>
                    @enderror
                </div>
                <div class="form-group">
                    <label for="category">Categoria</label>
                    <select wire:model="category_id" name="category_id" class="form-control">
                        <option>...</option>
                        @foreach($categories as $category)
                        <option value="{{ $category->id }}">{{ $category->name }} </option>
                        @endforeach
                    </select>
                    @error('category_id')
                    <span class="badge alert-danger"> {{ $message }} </span>
                    @enderror
                </div>
                <div class="form-group">
                    <label for="img">Img</label>
                    <input wire:model="img" name='img' type="file" accept="image/*" class="form-control-file" id="img">
                    @error('img')
                    <span class="badge alert-danger"> {{ $message }} </span>
                    @enderror

                    @if($update)
                    <img style="margin-top: 5px;" width="150" src="/{{  $img }}">
                    @else
                    @if ($img)
                    <img style="margin-top: 5px;" width="150" src="{{ $img->temporaryUrl() }}">
                    @endif
                    @endif
                </div>
                <div class="form-group">
                    <label for="state">Estado</label>
                    <select wire:model="state" name="state" class="form-control">
                        <option>...</option>
                        <option value="pendiente">Pendiente</option>
                        <option value="public">Public</option>
                    </select>
                    @error('state')
                    <span class="badge alert-danger"> {{ $message }} </span>
                    @enderror
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                @if($update)
                <button wire:click="save({{ $post->id }})" type="button" class="btn btn-primary">Guardar</button>
                @else
                <button wire:click="store" type="button" class="btn btn-primary">Crear</button>
                @endif
            </div>
        </div>
    </div>
</div>