@extends('layouts.app')

@section('content')
<div class="container">
    @livewire('post-component')
</div>
@endsection
@section('js')
<script type="text/javascript">
    Livewire.on('postStore', () => {
        $('#createpost').modal('hide');
    });
</script>
@endsection