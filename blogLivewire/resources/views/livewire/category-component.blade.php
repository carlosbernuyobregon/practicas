<div>
    <div class="container">
        <h1>Crud Categorias</h1>
        <button wire:click="showForm" type="button" class="btn btn-primary my-3" data-toggle="modal" data-target="#createModal">
            Crear categoria
        </button>
        @if($showForm)
        @include('admin.categories.form')
        @endif

        <table class="table" id="categories">
            <thead class="thead-dark">
                <tr>
                    <th scope="col">ID</th>
                    <th scope="col">Name</th>
                    <th scope="col" style="width: 25%">Options</th>
                </tr>
            </thead>
            <tbody>
                @foreach($categories as $category)
                <tr>
                    <th scope="row"> {{ $category->id }}
                    </th>
                    <td>{{ $category->name }}</td>
                    <td>
                        <button wire:click="edit({{ $category->id }})" type="button" class="btn btn-warning mr-2">
                            Editar
                        </button>
                        <button wire:click="destroy({{ $category->id }})" class="btn btn-danger">Eliminar</button>
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</div>