<div class="container">
    <h1>Crud Posts</h1>
    <button wire:click="clearParams" type="button" class="btn btn-primary my-3" data-toggle="modal" data-target="#createpost">
        Crear post
    </button>
    <div class="input-group">
        <input wire:model='searchparam' type="text" class="form-control" aria-label="Text input with segmented dropdown button" placeholder="Buscar">       
    </div><br>
    <table class="table" id="categories">
        <thead class="thead-dark">
            <tr>
                <th scope="col">ID</th>
                <th scope="col">Titulo</th>
                <th scope="col">Categoria</th>
                <th scope="col">Estado</th>
                <th scope="col">Fecha</th>
                <th scope="col" style="width: 15%">Imagen</th>
                <th scope="col" style="width: 20%">Options</th>
            </tr>
        </thead>
        <tbody>
            @foreach($posts as $post)
            <tr>
                <th scope="row"> {{ $post->id }}
                </th>
                <td>{{ $post->title }}</td>
                <td>{{ $post->category->name }}</td>
                <td>{{ $post->state }}</td>
                <td>{{ $post->created_at }}</td>
                <td>
                    <img src="/{{ $post->img }}" alt="" class="img-fluid">
                </td>
                <td>
                    <button wire:click='edit({{$post->id}})' type="button" class="btn btn-warning mr-2" data-toggle="modal" data-target="#createpost">
                        Editar
                    </button>
                    <button data-toggle="modal" data-target="#deleteModal" wire:click='deleteId({{$post->id}})' class="btn btn-danger">Eliminar</button>
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>
    @include('admin.posts.modalcreate')
    @include('admin.posts.modalConfirmDelete')
</div>