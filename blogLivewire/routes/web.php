<?php

use App\Http\Controllers\Admin\CategoryController;
use App\Http\Controllers\Admin\PostController;

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HomeController;

use Illuminate\Support\Str;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

Auth::routes();

Route::resource("admin/categorias", CategoryController::class);
Route::resource("admin/posts", PostController::class);


Route::get('/', [HomeController::class, 'index'])->name('home');
Route::get('/categorias/{categoria?}', [HomeController::class, 'showCategories'])->name('categories');
Route::get('/{category}/{post}', [HomeController::class, 'showPost'])->name('show.post');


Route::get('/admin', function () {
    return view('admin.home');
})->middleware('auth');
