<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class Post extends Model
{
    protected $fillable = ['title', 'state', 'img', 'text', 'category_id'];
    use HasFactory;

    public function category()
    {
        return $this->belongsTo(Category::class);
    }

    public function scopeLastposts($query, $numberPosts)
    {        
        return $query->where('state', 'public')->orderBy('created_at', 'desc')->take($numberPosts)->get();
    }
}
