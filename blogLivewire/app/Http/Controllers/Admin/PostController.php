<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Category;
use App\Models\Post;
use App\Http\Requests\PostRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Str;


class PostController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.posts.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categories = Category::all();
        return view('admin.posts.create', ['categories' => $categories]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(PostRequest $request)
    {
        // dd($request->all());
        $post = new Post;
        if ($request->hasFile('img')) {
            $file = $request->file('img');
            $destinationPath = 'images/posts/';
            $fileName = $file->getClientOriginalName();
            $file->move($destinationPath, $fileName);
            $post->img = $destinationPath . $fileName;
        };
        $post->title = $request->title;
        $post->slug = Str::slug($request->title);
        $post->text = $request->text;
        $post->state = $request->state;
        $post->category_id = $request->category_id;
        $post->save();
        return redirect('/admin/posts');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function show(Post $post)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function edit($idpost)
    {
        $post = Post::find($idpost);
        $categories = Category::all();
        return view('admin.posts.edit', [
            'categories' => $categories,
            'post' => $post
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function update(PostRequest $request, $idpost)
    {
        $post = Post::find($idpost);
        if ($request->hasFile('img')) {
            $file = $request->file('img');
            $destinationPath = 'images/posts/';
            $fileName = $file->getClientOriginalName();
            $file->move($destinationPath, $fileName);
            $post->img = $destinationPath . $fileName;
        };
        $post->title = $request->title;
        $post->text = $request->text;
        $post->state = $request->state;
        $post->category_id = $request->category_id;
        $post->save();
        return redirect('/admin/posts');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function destroy($idpost)
    {
        $category = Post::find($idpost);
        $category->delete();
        return redirect('admin/posts');
    }
}
