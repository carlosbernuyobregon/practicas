<?php

namespace App\Http\Livewire;

use App\Models\Category;
use App\Models\Post;
use Exception;
use Illuminate\Database\QueryException;
use Livewire\Component;
use Livewire\WithFileUploads;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\File;


class PostComponent extends Component
{
    use WithFileUploads;

    public $posts, $categories;
    public $searchparam;
    public $idpost, $title, $text, $state, $img, $category_id;
    public $update = false;
    public $delteId;

    protected $rules = [
        'title' => 'required',
        'img' => 'max:2048',
        'text' => 'required',
        'state' => 'required',
        'category_id' => 'required',
    ];

    protected $messages = [
        'name.required' => 'Es necesario un nombre',
    ];

    public function render()
    {
        $searchparam = '%' . $this->searchparam . '%';
        $this->posts =  Post::where('title', 'like', $searchparam)->get();
        $this->categories = Category::all();
        return view('livewire.post-component');
    }

    /**
     * Write code on Method
     *
     * @return response()
     */
    public function deleteId($id)
    {
        $this->deleteId = $id;
    }

    public function destroy()
    {
        $this->deleteFile($this->deleteId);
        Post::destroy($this->deleteId);
    }

    public function store()
    {
        $this->validate();

        try {
            $post = new Post;
            if ($this->img) {
                $img = 'storage/' . $this->img->store('images', 'public');
                $post->img = $img;
            }
            $post->title = $this->title;
            $post->slug = Str::slug($this->title);
            $post->text = $this->text;
            $post->state = $this->state;
            $post->category_id = $this->category_id;
            $post->save();
            $this->emit('postStore');
        } catch (QueryException $e) {
            File::delete($img);
            session()->flash('message', 'El titulo ya existe');
        }
    }

    public function edit($idpost)
    {

        $this->resetErrorBag();
        $post = Post::find($idpost);
        $this->idpost = $post->id;
        $this->img = $post->img;
        $this->title = $post->title;
        $this->text = $post->text;
        $this->category_id = $post->category_id;
        $this->state = $post->state;
        $this->update = true;
    }

    public function save()
    {
        $this->validate();
        try {
            $post = Post::find($this->idpost);
            if ($this->img != $post->img) {
                $img = 'storage/' . $this->img->store('images', 'public');
                $post->img = $img;
                $this->deleteFile($this->idpost);
            }
            $post->title = $this->title;
            $post->slug = Str::slug($this->title);
            $post->text = $this->text;
            $post->state = $this->state;
            $post->category_id = $this->category_id;
            $post->save();
            $this->emit('postStore');
        } catch (QueryException $e) {
            session()->flash('message', 'El titulo ya existe');
        }
    }


    public function clearParams()
    {
        $this->resetErrorBag();
        $this->idpost = null;
        $this->img = null;
        $this->title = null;
        $this->text = null;
        $this->category_id = null;
        $this->state = null;
        $this->update = false;
        $this->img = null;
    }

    public function deleteFile($id)
    {
        $post = Post::find($id);

        File::delete($post->img);
    }
}
