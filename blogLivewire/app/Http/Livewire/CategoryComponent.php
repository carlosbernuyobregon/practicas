<?php

namespace App\Http\Livewire;

use App\Models\Category;
use Livewire\Component;
use Illuminate\Support\Str;

class CategoryComponent extends Component
{
    public $name;
    public $idcategory;
    public $showForm = false;
    public $edit = false;
    public $messageInfo = "";

    protected $rules = [
        'name' => 'required',
    ];

    protected $messages = [
        'name.required' => 'Es necesario un nombre',      
    ];
    
    public function render()
    {
        return view('livewire.category-component', [
            'categories' => Category::all()
        ]);
    }


    public function destroy($idcategory)
    {
        Category::destroy($idcategory);
    }


    public function store()
    {
        $this->validate();
        $category = new Category;
        $category->name = $this->name;
        $category->slug = Str::slug($this->name);
        $category->save();
        $this->name = null;
        $this->showForm = false;
    }

    public function edit($idcategory)
    {
        $category = Category::find($idcategory);
        $this->name = $category->name;
        $this->idcategory = $category->id;
        $this->showForm = true;
        $this->edit = true;
    }

    public function save()
    {
        $this->validate();
        $category = Category::find($this->idcategory);
        $category->name = $this->name;
        $category->save();
        $this->showForm();
    }

    public function showForm()
    {
        $this->showForm = !$this->showForm;
    }
}
