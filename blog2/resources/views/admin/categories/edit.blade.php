@extends('layouts.app')

@section('content')
<div class="container">
    <form class="w-50" action="{{ route('categorias.update', $category) }}" method="POST">
        @method("PUT")
        @csrf
        <div class="form-group">
            <label for="name">Nombre categoria</label>
            <input type="text" value='{{ $category->name }}' name="name" class="form-control" id="name" aria-describedby="name">
        </div>
        <button type="submit" class="btn btn-primary">Guardar</button>
    </form>
</div>
@endsection