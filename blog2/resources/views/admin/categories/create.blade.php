@extends('layouts.app')

@section('content')
<div class="container">
    <form class="w-50" action="{{ route('categorias.store') }}" method="POST">
        @csrf
        <div class="form-group">
            <label for="name">Nombre categoria</label>
            <input type="text" name="name" class="form-control" id="name" aria-describedby="name">
            @error('name')
            <span class="badge alert-danger"> {{ $message }} </span>
            @enderror
        </div>
        <button type="submit" class="btn btn-primary">Crear</button>
    </form>
</div>
@endsection