@extends('layouts.app')

@section('css')
<link rel="stylesheet" href=https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.5.2/css/bootstrap.css"">
<link rel="stylesheet" href="https://cdn.datatables.net/1.11.3/css/dataTables.bootstrap4.min.css">
@endsection

@section('content')
<div class="container">
    <h1>Crud Categorias</h1>
    <button type="button" class="btn btn-primary my-3">
        <a class="text-white" href="{{ route('categorias.create') }}">Crear categoria</a>
    </button>
    <table class="table" id="categories">
        <thead class="thead-dark">
            <tr>
                <th scope="col">ID</th>
                <th scope="col">Name</th>
                <th scope="col" style="width: 25%">Options</th>
            </tr>
        </thead>
        <tbody>
            @foreach($categories as $category)
            <tr>
                <th scope="row"> {{ $category->id }}
                </th>
                <td>{{ $category->name }}</td>
                <td>

                    <form method="POST" action="{{ route('categorias.destroy', [$category]) }}">
                        @method("DELETE")
                        @csrf
                        <button type="button" class="btn btn-warning mr-2">
                            <a class="text-dark" href="{{ route('categorias.edit', [$category]) }}">Editar</a>
                        </button>
                        <button type="submit" class="btn btn-danger">Eliminar</button>
                    </form>
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>
</div>
@endsection

@section('js')
<script src="https://code.jquery.com/jquery-3.5.1.js"></script>
<script src="https://cdn.datatables.net/1.11.3/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.11.3/js/dataTables.bootstrap4.min.js"></script>
<script>
    $('#categories').DataTable();
</script>
@endsection