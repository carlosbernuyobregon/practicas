@extends('layouts.app')

@section('css')
<link rel="stylesheet" href=https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.5.2/css/bootstrap.css"">
<link rel="stylesheet" href="https://cdn.datatables.net/1.11.3/css/dataTables.bootstrap4.min.css">
@endsection

@section('content')
<div class="container">
    <h1>Crud Posts</h1>
    <button type="button" class="btn btn-primary my-3">
        <a class="text-white" href="{{ route('posts.create') }}">Crear post</a>
    </button>
    <table class="table" id="categories">
        <thead class="thead-dark">
            <tr>
                <th scope="col">ID</th>
                <th scope="col">Titulo</th>
                <th scope="col">Categoria</th>
                <th scope="col">Estado</th>
                <th scope="col">Fecha</th>
                <th scope="col" style="width: 15%">Imagen</th>
                <th scope="col" style="width: 20%">Options</th>
            </tr>
        </thead>
        <tbody>
            @foreach($posts as $post)
            <tr>
                <th scope="row"> {{ $post->id }}
                </th>
                <td>{{ $post->title }}</td>
                <td>{{ $post->category->name }}</td>
                <td>{{ $post->state }}</td>
                <td>{{ $post->created_at }}</td>
                <td>
                    <img src="/{{ $post->img }}" alt="" class="img-fluid">
                </td>

                <td>
                    <form method="POST" action="{{ route('posts.destroy', [$post]) }}">
                        @method("DELETE")
                        @csrf
                        <button type="button" class="btn btn-warning mr-2">
                            <a class="text-dark" href="{{ route('posts.edit', [$post]) }}">Editar</a>
                        </button>
                        <button type="submit" class="btn btn-danger">Eliminar</button>
                    </form>
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>
</div>
@endsection

@section('js')
<script src="https://code.jquery.com/jquery-3.5.1.js"></script>
<script src="https://cdn.datatables.net/1.11.3/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.11.3/js/dataTables.bootstrap4.min.js"></script>
<script>
    $('#categories').DataTable();
</script>
@endsection