@extends('layouts.app')

@section('content')
<div class="container">
    <form class="w-50" action="{{ route('posts.store') }}" method="POST" enctype="multipart/form-data">
        @csrf
        <div class="form-group">
            <label for="title">Titulo</label>
            <input type="text" name="title" class="form-control" id="title" aria-describedby="name">
            @error('title')
            <span class="badge alert-danger"> {{ $message }} </span>
            @enderror
        </div>
        <div class="form-group">
            <label for="text">Contenido</label>
            <textarea name='text' class="form-control" id="content" rows="3"></textarea>
            @error('text')
            <span class="badge alert-danger"> {{ $message }} </span>
            @enderror
        </div>
        <div class="form-group">
            <label for="category">Categoria</label>
            <select name="category_id" class="form-control">
                <option>...</option>
                @foreach($categories as $category)
                <option value="{{ $category->id }}">{{ $category->name }} </option>
                @endforeach
            </select>
            @error('category_id')
            <span class="badge alert-danger"> {{ $message }} </span>
            @enderror
        </div>
        <div class="form-group">
            <label for="img">Img</label>
            <input name='img' type="file" accept="image/*" class="form-control-file" id="img">
            @error('img')
            <span class="badge alert-danger"> {{ $message }} </span>
            @enderror
        </div>
        <div class="form-group">
            <label for="state">Estado</label>
            <select name="state" class="form-control">
                <option value="pendiente">Pendiente</option>
                <option value="public">Public</option>
            </select>
        </div>
        <button type="submit" class="btn btn-primary">Crear</button>
    </form>
</div>
@endsection