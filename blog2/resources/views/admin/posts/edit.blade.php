@extends('layouts.app')

@section('content')
<div class="container">
    <form class="w-50" action="{{ route('posts.update', $post) }}" method="POST" enctype="multipart/form-data">
        @csrf
        @method("PUT")

        <div class="form-group">
            <label for="title">Titulo</label>
            <input type="text" name="title" class="form-control" id="title" aria-describedby="name" value="{{ $post->title }}">
            @error('title')
            <span class="badge alert-danger"> {{ $message }} </span>
            @enderror
        </div>
        <div class="form-group">
            <label for="text">Contenido</label>
            <textarea name='text' class="form-control" id="content" rows="3">{{ $post->text }}</textarea>
            @error('text')
            <span class="badge alert-danger"> {{ $message }} </span>
            @enderror
        </div>
        <div class="form-group">
            <label for="category">Categoria</label>
            <select name="category_id" class="form-control">
                @foreach($categories as $category)
                <!-- //Check default selected option -->
                @if($post->category->name == $category->name)
                <option value="{{ $category->id }}" selected>{{ $category->name }}</option>
                @else
                <option value="{{ $category->id }}">{{ $category->name }}</option>
                @endif
                @endforeach
            </select>
        </div>
        <div class="form-group">
            <label for="img">Actuaizar imagen</label>
            <input name='img' type="file" class="form-control-file" id="img" onchange="showPreview(event);">
            <br>
            <img width="100" id="file-ip-1-preview" src="/{{ $post->img }}">

        </div>
        <div class="form-group">
            <label for="state">Estado</label>
            <select name="state" class="form-control">
                <option value="pendiente" {{ $post->state == 'pendiente' ? 'selected' : '' }}>Pendiente</option>
                <option value="public" {{ $post->state == 'public' ? 'selected' : '' }}>Public</option>
            </select>
        </div>
        <button type="submit" class="btn btn-primary">Actualizar</button>
    </form>
</div>
@endsection

@section('js')
<script>
    function showPreview(event) {
        if (event.target.files.length > 0) {
            var src = URL.createObjectURL(event.target.files[0]);
            var preview = document.getElementById("file-ip-1-preview");
            preview.src = src;
            preview.style.display = "block";
        }
    }
</script>
@endsection