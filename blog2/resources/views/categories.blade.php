@extends('layouts.layout')
@section('content')
<br>
<h1 class="text-center">Categorias</h1>
<h4 class="text-center">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Porro deserunt eveniet temporibus recusandae aut</h4>
<br><br>
<div class="row">
    <div class="col-lg-2 col-md-12">
        <div class="card text-center my-2">
            <div class="card-body p-1">
                <a href="/categorias" class="btn btn-link">Todas las categorias</a>
                <br>
            </div>
        </div>
        @foreach($categories as $category)
        <div class="card text-center my-2">
            <div class="card-body p-1">
                <a href="/categorias/{{ $category->slug }}" class="btn btn-link">{{ $category->name }}</a>
                <br>
            </div>
        </div>
        @endforeach
    </div>
    <div class="col-lg-10 col-md-12">
        <div class="row justify-content-start">
            @foreach($posts as $post)
            <div class="col-xl-4 col-md-5 mt-4">
                <div class="card bg-light" style="width: 18rem;">
                    <a href="{{ route('show.post', [$post->category->slug, $post->slug ]) }}"> <img height="160" class="card-img-top p-2" src="/{{ $post->img }}" alt="Card image cap"></a>
                    <div class="card-body">
                        <h5 class="card-text text-center">{{ $post->title }}</h5>
                    </div>
                    <div class="card-footer">
                        <span class="badge badge-pill badge-dark py-1">{{$post->category->name}}</span>
                    </div>
                </div>
            </div>
            @endforeach
        </div>
    </div>
</div>
@endsection