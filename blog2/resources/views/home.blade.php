@extends('layouts.layout')
@section('content')
<br>
<h1 class="text-center">Titulo Blog</h1>
<h4 class="text-center">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Porro deserunt eveniet temporibus recusandae aut</h4>
<br>
<div class="row">
    <div class="col-lg-10 col-md-12">
        <div class="row justify-content-start">
            @foreach($posts as $post)
            <div class="col-xl-4 col-md-5 mt-4">
                <div class="card bg-light" style="width: 18rem;">
                    <a href="{{ route('show.post', [$post->category->slug, $post->slug ]) }}"> <img height="160" class="card-img-top p-2" src="/{{ $post->img }}" alt="Card image cap"></a>
                    <div class="card-body">
                        <h5 class="card-text text-center">{{ $post->title }}</h5>
                    </div>
                    <div class="card-footer">
                        <span class="badge badge-pill badge-dark py-1">{{$post->category->name}}</span>
                    </div>
                </div>
            </div>
            @endforeach
        </div>
    </div>
    <div class="col-lg-2 col-md-12">
        <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Fugiat expedita labore ratione, numquam quos ad ab, quaerat earum accusantium commodi ullam repellendus, culpa laudantium architecto quidem! Quisquam iste voluptatem at!</p>
        <p>Lorem ipsum dolor sit amet consectetur, adipisicing elit. Quibusdam laudantium debitis incidunt atque deleniti quas facilis aspernatur! Dolorem saepe totam quod. Adipisci corporis ipsa inventore assumenda nesciunt, eligendi ad autem?</p>
    </div>
</div>
@endsection