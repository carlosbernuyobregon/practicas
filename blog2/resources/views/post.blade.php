@extends('layouts.layout')
@section('content')
<br>
<h1>{{ $post->title }}</h1>
<div class="row">
    <img src="/{{ $post->img }}" class="img-fluid w-50 my-4" alt="...">
</div>
<div class="row">
    <div class="col">
        {{ $post->text }}
    </div>
</div>
@endsection