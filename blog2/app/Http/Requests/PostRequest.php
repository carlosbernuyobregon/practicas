<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class PostRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => 'required',
            'text' => 'required',
            'category_id' => 'size:1',
            'img' => 'image|mimes:jpeg,png,jpg,gif,svg'
        ];
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'title.required' => 'Titulo necesario',
            'category_id.size' => 'Categoria necesario',
            'text.required' => 'Contenido necesario',
            'img.mimes' => 'Imagen no valida',
        ];
    }
}
