<?php

namespace App\Http\Controllers;

use App\Models\Category;
use App\Models\Post;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    // public function __construct()
    // {
    //     $this->middleware('auth');
    // }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        //param, number of posts;
        $posts = Post::lastposts(6);
        return view('home', ["posts" => $posts]);
    }

    /**
     * Show a post
     * $category slug
     * $post slug
     */
    public function showPost($category, $post)
    {
        $post = Post::where('slug', $post)->get();
        return view('post', ['post' => $post[0]]);
    }

    /**
     * 
     */
    public function showCategories($categoryName = null)
    {
        $categories = Category::all();
        $posts = Post::where('state', 'public')->get();
        if ($categoryName) {
            $category = Category::where('name', $categoryName)->get();
            $posts = $category[0]->posts->where('state', 'public'); //Seleccionamos posts relacionados y filtramos por estado
        }
        return view('categories', [
            'categories' => $categories,
            'posts' => $posts
        ]);
    }
}
