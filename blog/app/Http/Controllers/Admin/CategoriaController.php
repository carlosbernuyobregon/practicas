<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Category;


class CategoriaController extends Controller
{
    //

    public function indexCategory()
    {
        $categories = Category::all();
        return view('categories.crudCategories', [
            'categories' => $categories
        ]);
    }
}
