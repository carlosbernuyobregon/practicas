@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-2">
            <button type="button" class="btn btn-outline-dark"> <a href="/home/categorias">Categorias</a></button>


        </div>
        <div class="col-2">
            <button type="button" class="btn btn-outline-dark"> <a href="/home/noticias">Noticias</a>
            </button>
        </div>
    </div>
    <br>
    <br>
    <!-- Button trigger modal -->
    <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal">
        Crear categoria
    </button> <br><br>
    <div>
        <table class="table table-dark">
            <thead>
                <tr>
                    <th scope="col">Id</th>
                    <th scope="col">Name</th>
                    <th scope="col">Options</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($categories as $category)
                <tr>
                    <td>{{ $category->id }}</td>
                    <td>{{ $category->name }}</td>
                    <td>
                        <button type="button" class="btn-sm btn-warning">Update</button>
                        <button type="button" class="btn-sm btn-danger">Delete</button>
                    </td>
                </tr>
                @endforeach

            </tbody>
        </table>
    </div>
</div>

@include('categories.createModal')

@endsection