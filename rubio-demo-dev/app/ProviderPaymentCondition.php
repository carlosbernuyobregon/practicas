<?php

namespace App;

use App\Traits\AutoGenerateUuid;
use Illuminate\Database\Eloquent\Model;

class ProviderPaymentCondition extends Model
{
    // use AutoGenerateUuid;

    // public $incrementing = false;
    // protected $keyType = 'string';
    protected $fillable = [
        'type',
        'description',
        'percentage',
        'default',
        'customer_id'
    ];
}
