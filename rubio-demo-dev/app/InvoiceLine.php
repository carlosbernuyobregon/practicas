<?php

namespace App;

use App\Helpers\Helpers;
use App\Library\Constants;
use App\Models\ProviderRule;
use App\Traits\AutoGenerateUuid;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Log;
use PDOException;

class InvoiceLine extends Model
{
    use AutoGenerateUuid;
    public $incrementing        = false;
    protected $keyType          = 'string';
    protected $guarded          = ['id'];
    // Properties to handle errors but not available on DDBB operations
    public $lineErrorTotalAmount        = null;
    public $lineErrorTaxesTotalAmount   = null;
    public $lineErrorTaxType            = null;
    public $lineErrorQty                = null;

    /*
     * RELATIONSHIPS
     */
    public function invoices()
    {
        return $this->belongsTo(Invoice::class, 'invoice_id');
    }

    public function tax()
    {
        return $this->belongsTo('App\TaxType','tax_type_id');
    }

    /*
     * METHODS
     */
    public function checkInvoiceLine()
    {
        $calculatedTotalAmount = 0;
        // Lets simplify this check by reusing the recalculateTotals logic and then do the checks. Recalculate do the same operation but
        // pulling tax data from relationship
        $calculatedValues = $this->recalculateTotals(true);

        // Lazy load the relationship
        $this->load('tax');

        // Defaults
        $totalAmountAreTheSame        = false;
        $taxesTotalAmountAreTheSame   = false;

        if (round($this->total_amount, 2) == round($calculatedValues['totalAmount'], 2)) {
            $totalAmountAreTheSame = true;
        }

        if (round($this->taxes_amount, 2) == round($calculatedValues['taxesTotalAmount'], 2)) {
            $taxesTotalAmountAreTheSame = true;
        }

        if ($totalAmountAreTheSame && $taxesTotalAmountAreTheSame && $this->tax_type_id != null && $this->material_qty != 0) {
            return true;
        } else {
            $this->lineErrorTotalAmount         = !$totalAmountAreTheSame;
            $this->lineErrorTaxesTotalAmount    = !$taxesTotalAmountAreTheSame;
            $this->lineErrorTaxType             = $this->tax_type_id == null;
            $this->lineErrorQty                 = $this->material_qty == 0;

            return false;
        }
    }

    /**
     * Recalculates model total amount fields
     * @param bool $justReturnCalculations
     * @return array | void
     */
    public function recalculateTotals($justReturnCalculations = false)
    {
        // Lazy load the relationship
        $this->load('invoices.tax');

        // Defaults
        $taxPercentage  = false;
        $taxSpecialType = false;

        // Get a valid tax percentage
        if ($this->tax_type_id) {
            $taxTypeModel   = TaxType::where('id', $this->tax_type_id)->first();
            if ($taxTypeModel) {
                $taxSpecialType = $taxTypeModel->special_type;
                $taxPercentage  = $taxTypeModel->percentage;
                $this->taxes    = $taxTypeModel->percentage;
                $this->taxes_id = $taxTypeModel->type;
            }
        } elseif ($this->taxes != '' || !is_null($this->taxes)) {
            $taxPercentage  = $this->taxes;
            $taxSpecialType = false;
        } else {
            if (!empty($this->invoices->tax)) {
                $taxPercentage  = $this->invoices->tax->percentage;
                $taxSpecialType = $this->invoices->tax->special_type;
            } else {
                $taxPercentage  = $this->invoices->taxes_percent;
                $taxSpecialType = false;
            }
        }

        // ToDo: borrar definitivamente si se confirma que el descuento a línea va con un tax forzado
        // If the line is a former item inherited from header (only discounts by now), don't apply any tax
        /*if (in_array($this->item_coming_from_header, [Constants::HEADER_ITEM_TO_LINE_TYPE_DISCOUNT])) {
            $taxPercentage      = null;
            $taxSpecialType     = false;
            $this->taxes        = null;
            $this->taxes_id     = null;
            $this->tax_type_id  = null;
        }*/

        // Gross amount is new (21-07-2021), will need a review when UM are applied
        if ($this->gross_amount == 0) {
            $this->gross_amount = $this->net_amount;
        }

        // Decide with discount field is leading
        $finalDiscountAmount    = false;
        $finalDiscountRate      = false;

        // Set discount amount and rate
        if (!empty($this->discount_amount)) {
            $finalDiscountAmount    = $this->discount_amount;
            $finalDiscountRate      = ($this->discount_amount * 100) / $this->gross_amount;
        } elseif (!empty($this->discount_rate)) {
            $finalDiscountAmount    = $this->gross_amount * ($this->discount_rate / 100);
            $finalDiscountRate      = $this->discount_rate;
        }

        // Calculate the net amount taking into account the discounts
        $this->net_amount = $this->gross_amount - $finalDiscountAmount;

        // Assign resultant discount values
        $this->discount_amount  = $finalDiscountAmount;
        $this->discount_rate    = $finalDiscountRate;

        // Calculate the tax amount
        $taxesTotalAmount = $this->net_amount * (intval($taxPercentage) / 100);

        // Forcing irpf to null since is not a valid concept (irpf by lines)
        $this->irpf = 0;

        switch ($taxSpecialType) {
            case Constants::TAX_SPECIAL_TYPE_AUTOREPERCUTIDO:
                $totalAmount = $this->net_amount - $this->irpf;
                break;
            default:
                $totalAmount = $this->net_amount + $taxesTotalAmount - $this->irpf;
                break;
        }

        if ($justReturnCalculations) {

            return [
                'taxSpecialType'    => $taxSpecialType,
                'totalNetAmount'    => $this->net_amount,
                'taxesTotalAmount'  => $taxesTotalAmount,
                'totalAmount'       => $totalAmount
            ];
        }

        // Assign calculated values
        $this->taxes_amount = $taxesTotalAmount;
        $this->total_amount = $totalAmount;
        $this->taxes        = $taxPercentage;

        try {
            // Save the data
            $this->save();
        } catch (PDOException $e) {
            Log::error($e->getMessage(), [
                'class'     => __CLASS__,
                'method'    => __METHOD__,
            ]);
        }

        // Trigger parent recalculateTotals
        $this->invoices->recalculateTotals();
    }

    /**
     * Applies special rules defined for some providers
     */
    public function applySpecialRulesByProvider() : void
    {
        // Get rules from current provider
        $rules = ProviderRule::where('provider_vat_number', $this->invoices->vat_number)->first();

        if ($rules) {
            if ($rules->force_tax_type) {
                // Check if there's a tax type forcing specification for the current provider

                list(
                    $this->tax_type_id, $this->taxes_id, $this->taxes
                    ) = Helpers::forceProviderTaxTypeByDefinition($this->toArray(), $this->invoices->sap_company, $this->invoices->vat_number, true);
            }
        }
    }

    /*
     * SCOPES
     */
    public function scopeSearch($query, $invoiceId, $search)
    {
        $query->where('invoice_id', $invoiceId);

        if ($search['materialCode'] != '') {
            $query->Where('material_code', 'like', '%' . $search['materialCode'] . '%');
        }
        if ($search['materialDescription'] != '') {
            $query->Where('material_description', 'like', '%' . $search['materialDescription'] . '%');
        }
        if ($search['materialQtyMin'] != '' || $search['materialQtyMax'] != '') {
            if ($search['materialQtyMin'] != '' && $search['materialQtyMax'] != '') {
                $query->WhereBetween('material_qty', [$search['materialQtyMin'], $search['materialQtyMax']]);
            } else if ($search['materialQtyMin'] != '') {
                $query->Where('material_qty', '>=', $search['materialQtyMin']);
            } else {
                $query->Where('material_qty', '<=', $search['materialQtyMax']);
            }
        }
        if ($search['unitPriceMin'] != '' || $search['unitPriceMax'] != '') {
            if ($search['unitPriceMin'] != '' && $search['unitPriceMax'] != '') {
                $query->WhereBetween('unit_price', [$search['unitPriceMin'], $search['unitPriceMax']]);
            } else if ($search['unitPriceMin'] != '') {
                $query->Where('unit_price', '>=', $search['unitPriceMin']);
            } else {
                $query->Where('unit_price', '<=', $search['unitPriceMax']);
            }
        }
        if ($search['itemOrderNumBulk'] != '') {
            $query->Where('item_order_num_bulk', 'like', '%' . $search['itemOrderNumBulk'] . '%');
        }
        if ($search['itemOrderLineBulk'] != '') {
            $query->Where('item_order_line_bulk', 'like', '%' . $search['itemOrderLineBulk'] . '%');
        }
        if ($search['netAmountMin'] != '' || $search['netAmountMax'] != '') {
            if ($search['netAmountMin'] != '' && $search['netAmountMax'] != '') {
                $query->WhereBetween('net_amount', [$search['netAmountMin'], $search['netAmountMax']]);
            } else if ($search['netAmountMin'] != '') {
                $query->Where('net_amount', '>=', $search['netAmountMin']);
            } else {
                $query->Where('net_amount', '<=', $search['netAmountMax']);
            }
        }
        if ($search['totalAmountMin'] != '' || $search['totalAmountMax'] != '') {
            if ($search['totalAmountMin'] != '' && $search['totalAmountMax'] != '') {
                $query->WhereBetween('total_amount', [$search['totalAmountMin'], $search['totalAmountMax']]);
            } else if ($search['netAmountMin'] != '') {
                $query->Where('total_amount', '>=', $search['totalAmountMin']);
            } else {
                $query->Where('total_amount', '<=', $search['totalAmountMax']);
            }
        }
    }

    /*
     * MUTATORS
     */

    /**
     * Get the line's tax amount.
     *
     * @param  string  $value
     * @return string
     */
    public function getTaxesAmountAttribute($value)
    {
        return round($value, 4);
    }

    /**
     * Set the line's tax amount.
     *
     * @param  string  $value
     * @return void
     */
    public function setTaxesAmountAttribute($value)
    {
        $this->attributes['taxes_amount'] = round($value, 4);
    }

    /**
     * Get the line's gross amount.
     *
     * @param  string  $value
     * @return string
     */
    public function getGrossAmountAttribute($value)
    {
        return round($value, 2);
    }

    /**
     * Set the line's gross amount.
     *
     * @param  string  $value
     * @return void
     */
    public function setGrossAmountAttribute($value)
    {
        $this->attributes['gross_amount'] = round($value, 2);
    }

    /**
     * Get the line's net amount.
     *
     * @param  string  $value
     * @return string
     */
    public function getNetAmountAttribute($value)
    {
        return round($value, 2);
    }

    /**
     * Set the line's net amount.
     *
     * @param  string  $value
     * @return void
     */
    public function setNetAmountAttribute($value)
    {
        $this->attributes['net_amount'] = round($value, 2);
    }

    /**
     * Get the line's irpf amount.
     *
     * @param  string  $value
     * @return string
     */
    public function getIrpfAttribute($value)
    {
        return round($value, 2);
    }

    /**
     * Set the line's irpf amount.
     *
     * @param  string  $value
     * @return void
     */
    public function setIrpfAttribute($value)
    {
        $this->attributes['irpf'] = round($value, 2);
    }


    /**
     * Get the line's total amount.
     *
     * @param  string  $value
     * @return string
     */
    public function getTotalAmountAttribute($value)
    {
        return round($value, 2);
    }

    /**
     * Set the line's total amount.
     *
     * @param  string  $value
     * @return void
     */
    public function setTotalAmountAttribute($value)
    {
        $this->attributes['total_amount'] = round($value, 2);
    }

    /**
     * Get the line's discount amount.
     *
     * @param  string  $value
     * @return string
     */
    public function getDiscountAmountAttribute($value)
    {
        return round($value, 2);
    }

    /**
     * Set the line's discount amount.
     *
     * @param  string  $value
     * @return void
     */
    public function setDiscountAmountAttribute($value)
    {
        $this->attributes['discount_amount'] = round($value, 2);
    }

    /**
     * Get the line's discount rate.
     *
     * @param  string  $value
     * @return string
     */
    public function getDiscountRateAttribute($value)
    {
        return round($value, 2);
    }

    /**
     * Set the line's discount rate.
     *
     * @param  string  $value
     * @return void
     */
    public function setDiscountRateAttribute($value)
    {
        $this->attributes['discount_rate'] = round($value, 2);
    }

    /**
     * Get the line's unit price.
     *
     * @param  string  $value
     * @return string
     */
    public function getUnitPriceAttribute($value)
    {
        return round($value, 4);
    }

    /**
     * Set the line's unit price.
     *
     * @param  string  $value
     * @return void
     */
    public function setUnitPriceAttribute($value)
    {
        $this->attributes['unit_price'] = round($value, 4);
    }

    /**
     * Get the line's tax percentage.
     *
     * @param  string  $value
     * @return string
     */
    public function getTaxesAttribute($value)
    {
        return intval($value);
    }

    /**
     * Set the line's tax percentage.
     *
     * @param  string  $value
     * @return void
     */
    public function setTaxesAttribute($value)
    {
        $this->attributes['taxes'] = intval($value);
    }
}
