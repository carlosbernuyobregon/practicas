<?php

namespace App;

use App\Traits\AutoGenerateUuid;
use Carbon\Carbon;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

use function PHPSTORM_META\map;

class User extends Authenticatable
{
    use Notifiable, AutoGenerateUuid, SoftDeletes;
    public $incrementing = false;
    protected $keyType = 'string';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'email',
        'password',
        'phone',
        'first_name',
        'last_name',
        'country',
        'timezone',
        'date_format',
        'numbers_format',
        'default_locale',
        'company_id',
        'samaccountname',
        'guid',
        'domain',
        'adpwd',
        'company_name',
        'company_address',
        'company_city',
        'company_state',
        'company_country',
        'company_postal_code',
        'customer_id'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function getFullNameAttribute()
    {
        return $this->name . ' ' . $this->last_name;
    }
    public function setCustomerIdAttribute($value)
    {
        $this->attributes['customer_id'] = ($value == "") ? null : $value;
    }

    public function setCountryIdAttribute($value)
    {
        $this->attributes['country_id'] = ($value == "") ? null : $value;
    }

    /* public function getBlockedAttribute($value)
    {
        $this->attributes['blocked'] = ($value == "0") ? null : $value;
    } */

    public function getDateFormat()
    {
        if(config('custom.database_type')=='sqlsrv') {
            return 'd.m.Y H:i:s';
        }
        return parent::getDateFormat();

    }
    public function roles()
    {
        return $this->belongsToMany('App\Role');
    }
    public function requests()
    {
        return $this->hasMany('App\Requests', 'created_by');
    }
    public function costCenters()
    {
        return $this->belongsToMany('App\CostCenter');
    }
    public function audits()
    {
        return $this->hasMany('App\Audit');
    }
    public function storeRolesSession()
    {
        $roles = [];
        foreach($this->roles as $role){
            $roles[] = $role->key_value;
        }
        session(['user.roles' => $roles]);
    }
    public function authorizeRoles($roles)
    {
        abort_unless($this->hasAnyRole($roles), 401);
        return true;
    }
    public function hasAnyRole($roles)
    {
        if (is_array($roles)) {
            foreach ($roles as $role) {
                if ($this->hasRole($role)) {
                    return true;
                }
            }
        } else {
            if ($this->hasRole($roles)) {
                return true;
            }
        }
        return false;
    }
    public function hasRole($role,$direct = null)
    {
        if($direct){
            if ($this->roles()->where('key_value', $role)->first()) {
                return true;
            }
            return false;
        }
        if(!session()->has('user.roles')){
            $this->storeRolesSession();
        }
        if(in_array($role,session()->get('user.roles'))){
            return true;
        }
        /*
        if ($this->roles()->where('key_value', $role)->first()) {

        }
        */
        return false;
    }

    public function hasNotRole($role)
    {
        return !$this->hasRole($role);
    }

    public function getRandomUser()
    {
        return User::inRandomOrder()->value('email');
    }
    public function getBusinessManager($countryId, $businessLineId)
    {
        $userId = DB::table('business_line_country_user')
            ->where('country_id', $countryId)
            ->where('business_line_id', $businessLineId)
            ->value('user_id');
        return $userId;
    }
    public function applyTimeZone($dateTime)
    {
        if ($this->timezone == '') {
            $this->timezone = config('app.timezone');
            $this->save();
        }
        return Carbon::parse($dateTime)->timezone($this->timezone)->format('d/m/yy');
    }
    public function applyDateFormat($date = null, $canBeEmpty = false)
    {
        if($date!=null){
            return Carbon::parse($date)->format('d/m/Y');
        } elseif ($canBeEmpty) {
            return '';
        }
        else{
            return 'dd/mm/yyyy';
        }
    }
    public function applyCurrencyFormat($amount)
    {
        return number_format($amount,2,'.',',');
    }
    public function getUsersByRole($role)
    {
        if($role == null){
            return User::all();
        }
        $roleId = Role::where('key_value',$role)->value('id');
        $users = DB::table('users')
        ->leftJoin('role_user', 'users.id', '=', 'role_user.user_id')
        ->select('users.*')
        ->where('role_user.role_id',$roleId)
        ->orderBy('name','asc')
        ->get();
        return $users;
    }
    public function getOneUserByRole($role)
    {
        if($role == null){
            return User::all();
        }
        $roleId = Role::where('key_value',$role)->value('id');
        $users = DB::table('users')
        ->leftJoin('role_user', 'users.id', '=', 'role_user.user_id')
        ->select('users.*')
        ->where('role_user.role_id',$roleId)
        ->first();
        return $users;
    }
    function floatvalue($val){
        $val = str_replace(",",".",$val);
        $val = preg_replace('/\.(?=.*\.)/', '', $val);
        return floatval($val);
    }
    public function applyDateTimeZoneFormat($dateTime)
    {
        if ($this->timezone == '') {
            $this->timezone = config('app.timezone');
            $this->save();
        }
        return Carbon::parse($dateTime)->timezone($this->timezone)->format('d/m/Y - H:i');
    }
}
