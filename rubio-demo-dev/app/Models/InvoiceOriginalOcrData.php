<?php

namespace App\Models;

use App\Invoice;
use Illuminate\Database\Eloquent\Model;

/**
 * Class InvoiceOriginalOcrData. This Model represents the original data pulled from the OCR. This data could never
 * be modified under any circumstances.
 *
 * @package App\Models
 */
class InvoiceOriginalOcrData extends Model
{
    /**
     * The attributes that should be guarded.
     *
     * @var array
     */
    protected $guarded = ['id'];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = ['id'];

    /**
     * The table related to model.
     *
     * @var array
     */
    protected $table = 'invoice_original_ocr_data';

    /**
     * The relationships that should always be loaded.
     *
     * @var array
     */
    protected $with = [];

    /**
     * Get the Invoice that owns the original data pulled from OCR.
     */
    public function invoice()
    {
        return $this->belongsTo(Invoice::class, 'invoice_id');
    }
}
