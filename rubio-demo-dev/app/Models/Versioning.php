<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * By now the only entity using versioning is the model Budget
 *
 * Class Versioning
 * @package App\Models
 */
class Versioning extends Model
{
    use SoftDeletes;

    /**
     * The attributes that should be guarded.
     *
     * @var array
     */
    protected $guarded = ['id'];

    /**
     * The table related to model.
     *
     * @var array
     */
    protected $table = 'versioning';

    /**
     * The relationships that should always be loaded.
     *
     * @var array
     */
    protected $with = [];
}
