<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class PaPerson extends Model
{
    use SoftDeletes;

    protected $table    = 'pa_persons';
    protected $guarded  = ['id'];
    protected $hidden   = ['id'];
}
