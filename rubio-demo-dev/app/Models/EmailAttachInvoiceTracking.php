<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class EmailAttachInvoiceTracking extends Model
{
    /**
     * The attributes that should be guarded.
     *
     * @var array
     */
    protected $guarded = ['id'];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = ['id'];

    /**
     * The table related to model.
     *
     * @var array
     */
    protected $table = 'email_attach_invoice_tracking';

    /**
     * The relationships that should always be loaded.
     *
     * @var array
     */
    protected $with = [];

    protected $casts = [
        'attachments_id'                    => 'array',
        'invoices_id'                       => 'array',
        'sent_to_ocr_by_invoice_id'         => 'array',
        'failed_sent_to_ocr_by_invoice_id'  => 'array'
    ];
}
