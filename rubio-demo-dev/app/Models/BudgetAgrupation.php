<?php

namespace App\Models;

use App\Budget;
use App\Traits\AutoGenerateUuid;
use Illuminate\Database\Eloquent\Model;

class BudgetAgrupation extends Model
{
    use AutoGenerateUuid;
    public $incrementing = false;
    protected $keyType = 'string';
    protected $guarded = [];

    public function budgets()
    {
        return $this->hasMany(Budget::class);
    }
    public function company()
    {
        return $this->belongsTo(\App\Company::class);
    }
}
