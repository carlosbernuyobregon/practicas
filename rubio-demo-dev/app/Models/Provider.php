<?php

namespace App\Models;

use App\Traits\AutoGenerateUuid;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Provider
 * @package App\Models
 */
class Provider extends Model
{
    /**
     * Traits that manage the uuid generation and soft deletes
     */
    use AutoGenerateUuid, SoftDeletes;
    public $incrementing = false;
    protected $keyType = 'string';
    /**
     * The attributes that should be guarded.
     *
     * @var array
     */
    protected $guarded = ['id'];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = ['id'];

    /**
     * The table related to model.
     *
     * @var array
     */
    protected $table = 'providers';

    /**
     * The relationships that should always be loaded.
     *
     * @var array
     */
    protected $with = [];
    public function files()
    {
        return $this->morphMany('App\File', 'fileable')->orderBy('created_at');
    }
    public function audit()
    {
        return $this->morphMany('App\Audit', 'auditable')->orderBy('created_at');
    }
}
