<?php

namespace App\Models;

use App\Traits\AutoGenerateUuid;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class ProvidersPerSapCompanyForcedTaxType
 * @package App\Models
 */
class ProvidersPerSapCompanyForcedTaxType extends Model
{
    /**
     * Traits that manage the uuid generation and soft deletes
     */
    use AutoGenerateUuid, SoftDeletes;

    /**
     * The attributes that should be guarded.
     *
     * @var array
     */
    protected $guarded = ['id'];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = ['id'];

    /**
     * The table related to model.
     *
     * @var array
     */
    protected $table = 'providers_per_sap_company_forced_tax_type';

    /**
     * The relationships that should always be loaded.
     *
     * @var array
     */
    protected $with = [];
}
