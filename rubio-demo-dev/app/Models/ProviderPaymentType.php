<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ProviderPaymentType extends Model
{
    use SoftDeletes;

    /**
     * The attributes that should be guarded.
     *
     * @var array
     */
    protected $guarded = ['id'];

    /**
     * The table related to model.
     *
     * @var array
     */
    protected $table = 'provider_payment_types';

    /**
     * The relationships that should always be loaded.
     *
     * @var array
     */
    protected $with = [];
}
