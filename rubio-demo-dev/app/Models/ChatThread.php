<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ChatThread extends Model
{
    use SoftDeletes;

    /**
     * The attributes that should be guarded.
     *
     * @var array
     */
    protected $guarded = ['id'];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = ['id'];

    /**
     * The table related to model.
     *
     * @var array
     */
    protected $table = 'chat_threads';

    /**
     * The relationships that should always be loaded.
     *
     * @var array
     */
    protected $with = [];

    protected $casts = [
        'users_id' => 'array'
    ];
}
