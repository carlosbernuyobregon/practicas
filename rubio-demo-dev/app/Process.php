<?php

namespace App;

use App\Traits\AutoGenerateUuid;
use Illuminate\Database\Eloquent\Model;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Client as GuzzleClient;
use App\Process as AppProcess;
use Illuminate\Support\Facades\DB;

use function GuzzleHttp\json_decode;

class Process extends Model
{
    //
    use AutoGenerateUuid;
    public $incrementing = false;
    protected $keyType = 'string';
    protected $fillable = [
        'key_value',
        'bpm_id',
        'bpm_version_id',
        'bpm_name',
        'name',
        'description',
        'version',
        'active',
        'tenant_id',
        'variables',
        'active'
    ];
    public $editData = [
        'id' => 'id',
        'key_value' => 'key_val',
        'model'=> 'model',
        'bpm_id' => 'bpm_id',
        'bpm_version_id' => 'bpm_version_id',
        'bpm_name' => 'bpm_name',
        'name' => 'name',
        'description' => 'description',
        'tenant_id' => 'tenant_id',
        'variables' => 'variables',
        'version' => 'version',
        'active' => 'active'
    ];
    public function getDateFormat()
    {
        if(config('custom.database_type')=='sqlsrv') {
            return 'd.m.Y H:i:s';
        }
        return parent::getDateFormat();

    }


    public function getWorkflows()
    {
        $client = new GuzzleClient();
        $res = $client->request('GET', config('custom.k2_server').'/workflows', [
            'auth' => [auth()->user()->samaccountname, decrypt(auth()->user()->adpwd)],
            'verify' => false
        ]);
        return json_decode($res->getBody())->workflows;
    }
    public function getProcessCamunda()
    {
        $client = new GuzzleClient();
        $res = $client->request('GET', config('custom.bpm_server').'/process-definition', [
            'auth' => [auth()->user()->samaccountname, decrypt(auth()->user()->adpwd)],
            'verify' => false
        ]);
        return json_decode($res->getBody())->workflows;
    }


    public function getAllTasks()
    {
        $client = new GuzzleClient();
        $res = $client->request('GET', config('custom.bpm_server').'/task');

        //$res = file_get_contents(config('custom.bpm_server').'/task');

        return json_decode($res->getBody());
    }

    public function getUserTasksCamunda($user = null,$sortBy = 'created',$sortOrder = 'asc')
    {
        if(!$user){
            $user = auth()->user()->id;
        }
        //dd($user);
        $client = new GuzzleClient();
        $res = $client->request('GET', config('custom.bpm_server').'/task?assignee=' .$user
                .'&sortBy='.$sortBy
                .'&sortOrder='.$sortOrder, [
            //'auth' => [auth()->user()->samaccountname, decrypt(auth()->user()->adpwd)],
            'verify' => false
        ]);
        //dd(json_decode($res->getBody()));
        return json_decode($res->getBody()) ?? [];
    }

   /* public function submitProcess($processKey,$dataFields,$debug = false)
    {
        $appProcess = AppProcess::where('key_value', $processKey)->first();
        echo(json_encode($dataFields));
        //die();
        $client = new GuzzleClient();
        $response = $client->post(config('custom.k2_server') . '/workflows/' . $appProcess->bpm_id, [
            'auth' => [auth()->user()->samaccountname, decrypt(auth()->user()->adpwd)],
            'verify' => false,
            'headers' => ['Content-Type' => 'application/json'],
            'body' => json_encode($dataFields),
            //'debug' => $debug,
        ]);
        $return['status'] = $response->getStatusCode();
        $return['body'] = $response->getBody();
        return $return;
    }
*/
    public function submitProcess($processKey,$dataFields,$debug = false)
    {
        //$appProcess = AppProcess::where('key_value', $processKey)->first();
        // echo(json_encode($dataFields));
        //die();
        $client = new GuzzleClient();

        $res = $client->request('post', config('custom.bpm_server').'/process-definition/key/' . $processKey .'/start', [

            'verify' => false,
            'headers' => ['Content-Type' => 'application/json'],
            'body' => json_encode($dataFields)
        ] );
          //  dd($dataFields);
        $return['status'] = $res->getStatusCode();
        $return['body'] = $res->getBody();
        return $return;
    }

    public function getTaskActions($sn)
    {
        $client = new GuzzleClient();
        $response = $client->get(config('custom.k2_server') . '/tasks/' . $sn .'/actions', [
            //'auth' => [auth()->user()->samaccountname, decrypt(auth()->user()->adpwd)],
            'verify' => false,
            'headers' => ['Content-Type' => 'application/json'],
            'body' => '',
        ]);

        $actions = json_decode($response->getBody());
        return $actions->batchableActions;
    }
    public function getTaskVariables($id)
    {

        $client = new GuzzleClient();
        $response = $client->get(config('custom.bpm_server') . '/task/' . $id .'/variables', [
        //    'auth' => [auth()->user()->samaccountname, decrypt(auth()->user()->adpwd)],
            'verify' => false,
            'headers' => ['Content-Type' => 'application/json'],
            'body' => '',
        ]);

        //$response = file_get_contents(config('custom.bpm_server') . '/task/' . $id .'/variables');
        //dd($response);
        $actions = json_decode($response->getBody());
        //dd($actions);
        return $actions;
    }
    public function getFormVariables($id)
    {

        $client = new GuzzleClient();
        $response = $client->get(config('custom.bpm_server') . '/task/' . $id .'/form-variables', [
        //    'auth' => [auth()->user()->samaccountname, decrypt(auth()->user()->adpwd)],
            'verify' => false,
            'headers' => ['Content-Type' => 'application/json'],
            'body' => '',
        ]);

        //$response = file_get_contents(config('custom.bpm_server') . '/task/' . $id .'/variables');
        //dd($response);
        $body = json_decode($response->getBody(),true);

        $actions = $body['varActionsString'];
        $value = $actions['value'];
        $value = str_replace('"','',$value);
        $value = str_replace('{','',$value);
        $value = str_replace('}','',$value);
        $actions = explode(',',$value);
        $response = [
            'actions' => $actions,
            'processId' => json_decode($response->getBody())->processId->value,
        ];
        //$actions = json_decode();
        //dd($actions);
        return $response;
    }

    public function submitTask($sn, $action, $dataFields, $debug = false)
    {
        $dataFields = '{
            "xmlFields": [
              {
              }
            ],
            "itemReferences": {
            },
            "dataFields": {
            }
          }';
        $dataFields = '{
            "xmlFields": [
              {
                "name": "string",
                "value": "string"
              }
            ],
            "itemReferences": {
            },
            "dataFields": {
            }
          }';
        $client = new GuzzleClient();
        $response = $client->post(config('custom.k2_server') . '/tasks/' . $sn .'/actions/' . $action, [
            'auth' => [auth()->user()->samaccountname, decrypt(auth()->user()->adpwd)],
            'verify' => false,
            'headers' => ['Content-Type' => 'application/json'],
            'body' => $dataFields,
        ]);
        $return['status'] = $response->getStatusCode();
        $return['body'] = $response->getBody();
        return $return;
    }

    //MODIFIED SUBMIT TASK ACTION FOR CAMUNDA API
    public function submitTaskCamunda($id, $dataFields, $debug = false)
    {
        if((auth()->user()->samaccountname != null)||(auth()->user()->adpwd != null)){
            $this->processUser = auth()->user()->samaccountname;
            $this->processPassword = decrypt(auth()->user()->adpwd);
        }else{
            $this->processUser = config('custom.k2_anonimous_user');
			//$this->processUser = 'extstrategying01@origbuff.local';
            $this->processPassword = config('custom.k2_anonimous_pwd');
        }
        $client = new GuzzleClient();
        $response = $client->post(config('custom.bpm_server') . '/task/' . $id . '/complete', [
            //'auth' => [$this->processUser, $this->processPassword],
            'verify' => false,
            'headers' => ['Content-Type' => 'application/json'],
            'body' => $dataFields,
        ]);
        $return['status'] = $response->getStatusCode();
        $return['body'] = $response->getBody();
        return $return;
    }

    // Two options, DELEGATE (reassign) and SET ASSIGNEE (change assigned)
    public function redirectTaskCamunda($id, $redirectUser)
    {
        $redirectUser = User::find($redirectUser);
        if ($redirectUser == null) {
            return false;
        }
        $dataFields = '{
            "userId": "' . $redirectUser->email . '"
          }';
        if((auth()->user()->samaccountname != null)||(auth()->user()->adpwd != null)){
            $this->processUser = auth()->user()->samaccountname;
            $this->processPassword = decrypt(auth()->user()->adpwd);
        }else{
            $this->processUser = config('custom.k2_anonimous_user');
			//$this->processUser = 'extstrategying01@origbuff.local';
            $this->processPassword = config('custom.k2_anonimous_pwd');
        }
        $client = new GuzzleClient();
        //TO ASSIGN
      //$response = $client->post(config('custom.bpm_server') . '/task/' . $id . '/assignee/', [
        //TO DELEGATE
        $response = $client->post(config('custom.bpm_server') . '/task/' . $id . '/delegate/', [
            'auth' => [$this->processUser, $this->processPassword],
            'verify' => false,
            'headers' => ['Content-Type' => 'application/json'],
            'body' => $dataFields,
        ]);

        $return['status'] = $response->getStatusCode();
        $return['body'] = $response->getBody();
        return $return;
    }
    public function reassignTaskCamunda($id, $redirectUser)
    {
        $redirectUser = User::find($redirectUser);
        if ($redirectUser == null) {
            return false;
        }
        $dataFields = '{
            "userId": "' . $redirectUser->email . '"
          }';
        if((auth()->user()->samaccountname != null)||(auth()->user()->adpwd != null)){
            $this->processUser = auth()->user()->samaccountname;
            $this->processPassword = decrypt(auth()->user()->adpwd);
        }else{
            $this->processUser = config('custom.k2_anonimous_user');
			//$this->processUser = 'extstrategying01@origbuff.local';
            $this->processPassword = config('custom.k2_anonimous_pwd');
        }
        $client = new GuzzleClient();
        //TO ASSIGN
        $response = $client->post(config('custom.bpm_server') . '/task/' . $id . '/assignee/', [
        //TO DELEGATE
        //$response = $client->post(config('custom.bpm_server') . '/task/' . $id . '/delegate/', [
            'auth' => [$this->processUser, $this->processPassword],
            'verify' => false,
            'headers' => ['Content-Type' => 'application/json'],
            'body' => $dataFields,
        ]);

        $return['status'] = $response->getStatusCode();
        $return['body'] = $response->getBody();
        return $return;
    }

    public function redirectTask($sn, $redirectUser)
    {
        $redirectUser = User::find($redirectUser);
        if($redirectUser == null){
            return false;
        }
        $dataFields = '{
            "RedirectTo": "'.$redirectUser->email.'"
          }';
        $client = new GuzzleClient();
        $response = $client->post(config('custom.k2_server') . '/tasks/' . $sn .'/actions/redirect/', [
            'auth' => [auth()->user()->samaccountname, decrypt(auth()->user()->adpwd)],
            'verify' => false,
            'headers' => ['Content-Type' => 'application/json'],
            'body' => $dataFields,
        ]);
        $return['status'] = $response->getStatusCode();
        $return['body'] = $response->getBody();
        return $return;
    }
    public function cancelWorkflow($id)
    {
        $dataFields = '{
            "id": "'.$id.'"
          }';
        $client = new GuzzleClient();
        $response = $client->post(config('custom.k2_server') . '/tasks/' . $id .'/actions/redirect/', [
            'auth' => [auth()->user()->samaccountname, decrypt(auth()->user()->adpwd)],
            'verify' => false,
            'headers' => ['Content-Type' => 'application/json'],
            'body' => $dataFields,
        ]);
        $return['status'] = $response->getStatusCode();
        $return['body'] = $response->getBody();
        return $return;
    }

    public function createUser($profile, $id, $firstName, $lastName, $email, $password)
    {
        $profile = '{
            {"profile":
                {"id": "'.$id.'",
                "firstName":"'.$firstName.'"
                "lastName":"'.$lastName.'",
                "email":"'.$email.'"},
              "credentials":
                {"password":"'.$password.'"}
              }
          }';
        $client = new GuzzleClient();
        $response = $client->post(config('custom.bpm_server') . '/user/create', [
            'auth' => [auth()->user()->samaccountname, decrypt(auth()->user()->adpwd)],
            'verify' => false,
            'headers' => ['Content-Type' => 'application/json'],
            'body' => json_encode($profile),
            //'debug' => $debug,
        ] .'/start');
        $return['status'] = $response->getStatusCode();
        return $return;
    }

    public function editUserDetails($profile, $id, $firstName, $lastName, $email)
    {
        $profile = '{
            {"profile":
                {"id": "'.$id.'",
                "firstName":"'.$firstName.'"
                "lastName":"'.$lastName.'",
                "email":"'.$email.'"}
          }';
        $client = new GuzzleClient();
        $response = $client->put(config('custom.bpm_server') . '/user/' . $id .'profile', [
            'auth' => [auth()->user()->samaccountname, decrypt(auth()->user()->adpwd)],
            'verify' => false,
            'headers' => ['Content-Type' => 'application/json'],
            'body' => json_encode($profile),
            //'debug' => $debug,
        ] .'/start');
        $return['status'] = $response->getStatusCode();
        return $return;
    }

    public function editUserCredentials($id, $credentials, $password, $authenticatedUserPassword)
    {
        $credentials = '{
            {"profile":
                {"password": "'.$password.'",
                "authenticatedUserPassword":"'.$authenticatedUserPassword.'"
              }
          }';
        $client = new GuzzleClient();
        $response = $client->put(config('custom.bpm_server') . '/user/' . $id .'credentials', [
            'auth' => [auth()->user()->samaccountname, decrypt(auth()->user()->adpwd)],
            'verify' => false,
            'headers' => ['Content-Type' => 'application/json'],
            'body' => json_encode($credentials),
            //'debug' => $debug,
        ] .'/start');
        $return['status'] = $response->getStatusCode();
        return $return;
    }

    public function deleteUser($id)
    {
        $client = new GuzzleClient();
        $response = $client->delete(config('custom.bpm_server') . '/user/' . $id, [
            'auth' => [auth()->user()->samaccountname, decrypt(auth()->user()->adpwd)],
            'verify' => false,
            'headers' => ['Content-Type' => 'application/json']
            //'debug' => $debug,
        ] .'/start');
        $return['status'] = $response->getStatusCode();
        return $return;
    }

    public function getUserProfile($id)
    {
        $client = new GuzzleClient();
        $response = $client->get(config('custom.bpm_server') . '/user/' . $id .'profile', [
            'auth' => [auth()->user()->samaccountname, decrypt(auth()->user()->adpwd)],
            'verify' => false,
            'headers' => ['Content-Type' => 'application/json']
            //'debug' => $debug,
        ] .'/start');
        $return['status'] = $response->getStatusCode();
        $return['body'] = $response->getBody();
        return $return;
    }

    public function getUserList()
    {
        $client = new GuzzleClient();
        $response = $client->get(config('custom.bpm_server') . '/user/', [
            'auth' => [auth()->user()->samaccountname, decrypt(auth()->user()->adpwd)],
            'verify' => false,
            'headers' => ['Content-Type' => 'application/json']
            //'debug' => $debug,
        ] .'/start');
        $return['status'] = $response->getStatusCode();
        $return['body'] = $response->getBody();
        return $return;
    }

    public function getAuthorization()
    {
        $client = new GuzzleClient();
        $response = $client->get(config('custom.bpm_server') . '/authorization/', [
            'auth' => [auth()->user()->samaccountname, decrypt(auth()->user()->adpwd)],
            'verify' => false,
            'headers' => ['Content-Type' => 'application/json']
            //'debug' => $debug,
        ] .'/start');
        $return['status'] = $response->getStatusCode();
        $return['body'] = $response->getBody();
        return $return;
    }

    public function createAuthorization($type, $permissions, $userId, $groupId, $resourceType, $resourceId)
    {
        $dataFields = '{
            "type": "'.$type.'",
            "permissions": "'.$permissions.'",
            "userId": "'.$userId.'",
            "groupId": "'.$groupId.'",
            "resourceType": "'.$resourceType.'",
            "resourceId": "'.$resourceId.'"
          }';
        $client = new GuzzleClient();
        $response = $client->post(config('custom.bpm_server') . '/authorization/create', [
            'auth' => [auth()->user()->samaccountname, decrypt(auth()->user()->adpwd)],
            'verify' => false,
            'headers' => ['Content-Type' => 'application/json'],
            'body' => $dataFields
            //'debug' => $debug,
        ] .'/start');
        $return['status'] = $response->getStatusCode();
        $return['body'] = $response->getBody();
        return $return;
    }

    public function updateAuthorization($id, $permissions, $userId, $groupId, $resourceType, $resourceId)
    {
        $dataFields = '{
            "permissions": "'.$permissions.'",
            "userId": "'.$userId.'",
            "groupId": "'.$groupId.'",
            "resourceType": "'.$resourceType.'",
            "resourceId": "'.$resourceId.'"
          }';
        $client = new GuzzleClient();
        $response = $client->put(config('custom.bpm_server') . '/authorization/' . $id,  [
            'auth' => [auth()->user()->samaccountname, decrypt(auth()->user()->adpwd)],
            'verify' => false,
            'headers' => ['Content-Type' => 'application/json'],
            'body' => $dataFields
            //'debug' => $debug,
        ] .'/start');
        $return['status'] = $response->getStatusCode();
        return $return;
    }

    public function deleteAuthorization($id)
    {
        $client = new GuzzleClient();
        $response = $client->delete(config('custom.bpm_server') . '/authorization/' . $id,  [
            'auth' => [auth()->user()->samaccountname, decrypt(auth()->user()->adpwd)],
            'verify' => false,
            'headers' => ['Content-Type' => 'application/json']
            //'debug' => $debug,
        ] .'/start');
        $return['status'] = $response->getStatusCode();
        return $return;
    }

}
