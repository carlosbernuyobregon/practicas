<?php

namespace App;

use App\Traits\AutoGenerateUuid;
use Illuminate\Database\Eloquent\Model;

class Email extends Model
{
    use AutoGenerateUuid;
    public $incrementing = false;
    protected $keyType = 'string';

    protected $fillable = [
        'email_uid',
        'email',
        'from',
        'attachments',
        'to',
        'subject',
        'body',
        'customer_id'
    ];
    public function EmailAttachments()
    {
        return $this->hasMany('App\EmailAttachment');
    }
}
