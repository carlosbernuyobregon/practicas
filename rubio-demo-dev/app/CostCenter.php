<?php

namespace App;

use App\Traits\AutoGenerateUuid;
use Illuminate\Database\Eloquent\Model;

class CostCenter extends Model
{
    use AutoGenerateUuid;
    public $incrementing = false;
    protected $keyType = 'string';

    protected $fillable = [
        'customer_id',
        'external_id',
        'name',
        'company',
        'manager_1_id',
        'manager_2_id',
        'manager_3_id',
        'amount_level_1',
        'amount_level_2',
        'amount_level_3',
        'amount_level_secondary_1',
        'amount_level_secondary_2',
        'amount_level_secondary_3',
    ];

    public function setCustomerIdAttribute($value)
    {
        $this->attributes['customer_id'] = ($value == "") ? null : $value;
    }
    public function users()
    {
        return $this->belongsToMany('App\User');
    }
    public function budgets()
    {
        return $this->belongsToMany(Budget::class);
    }
}
