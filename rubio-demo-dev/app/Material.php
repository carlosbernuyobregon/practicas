<?php

namespace App;

use App\Traits\AutoGenerateUuid;
use Illuminate\Database\Eloquent\Model;

class Material extends Model
{
    use AutoGenerateUuid;
    public $incrementing = false;
    protected $keyType = 'string';

    protected $fillable = [
        'customer_id',
            'external_id',
            'material_name',
            'company',
            'type',
    ];

    public function setCustomerIdAttribute($value)
    {
        $this->attributes['customer_id'] = ($value == "") ? null : $value;
    }
}
