<?php

namespace App\Http\Resources;

use App\Company;
use App\Invoice;
use App\Order;
use Illuminate\Http\Resources\Json\JsonResource;

class OrderLine extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        if(!$this->center){
            $order = Order::find($this->order_id);
            $company = Company::where('code',$order->company)->first();
            $this->center = $company->center;
        }
        return [
            'id'                        => $this->id,
            //'company'                   => $this->company ?? $this->order->company,
            'imputation_type_code'      => config('custom.json-orders-default-order-type'),
            'center'                    => $this->center,
            'measure_unit'              => $this->mesureUnit->unit ?? null,
            'accounting_account'        => $this->accountingAccount->external_id ?? $this->budget->external_id ?? null,
            'cost_center'               => $this->costCenter->external_id ?? null,
            'budget'                    => $this->budget->name ?? null,
            'tax_type'                  => $this->taxType->type ?? null,
            'description'               => $this->description,
            'position'                  => $this->position,
            'net_amount'                => $this->net_amount,
            'qty'                       => $this->qty,
            'currency'                  => $this->currency,
            'delivery_date'             => $this->delivery_date,
            'pa_business_area'          => $this->pa_business_area,
            'pa_distribution_channel'   => $this->pa_distribution_channel,
            'pa_country'                => $this->pa_country,
            'pa_order'                  => $this->pa_order,
            'pa_person'                 => $this->pa_person,
            'pa_family'                 => $this->pa_family,
            'pa_client'                 => $this->pa_client,
        ];
    }
}
