<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use App\Http\Resources\InvoiceLine;

class Invoice extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
                'id'                                                => $this->id,
                'sap_company'                                       => $this->sap_company,
                'invoice_number'                                    => $this->invoice_number,
                'lotus_id'                                          => $this->lotus_id,
                'taxes_id'                                          => $this->taxes_id,
                'customer_name'                                     => $this->customer_name,
                'date'                                              => $this->date,
                'operation_date'                                    => $this->operation_date,
                'document_type'                                     => $this->document_type,
                'document_type_label'                               => $this->document_type_label,
                'vat_number'                                        => $this->vat_number,
                'provider_name'                                     => $this->provider_name,
                'provider_country'                                  => $this->provider_country,
                'intern_order'                                      => $this->intern_order,
                'payment_conditions'                                => $this->payment_conditions,
                'payment_date'                                      => $this->payment_date,
                'intern_vat_number'                                 => $this->intern_vat_number,
                'total'                                             => $this->total,
                'total_net'                                         => $this->total_net,
                'total_net_for_non_zero_tax_percentage'             => $this->total_net_for_non_zero_tax_percentage,
                'total_net_for_non_zero_tax_percentage_calculated'  => $this->total_net_for_non_zero_tax_percentage_calculated,
                'taxes_total_amount'                                => $this->taxes_total_amount,
                'taxes_percent'                                     => $this->taxes_percent,
                'taxes_type'                                        => $this->taxes_type,
                'taxes_irpf_amount'                                 => $this->taxes_irpf_amount,
                'taxes_irpf_rate'                                   => $this->taxes_irpf_rate,
                'tax_balance_total'                                 => $this->tax_balance_total,
                'taxes_other_amount'                                => $this->taxes_other_amount,
                'currency'                                          => $this->currency,
                'status'                                            => $this->status,
                'duplicated_from'                                   => $this->duplicated_from,
                'file'                                              => $this->file,
                'exchange_type'                                     => $this->exchange_type,
                'sap_error_code'                                    => $this->sap_error_code,
                'sap_error_description'                             => $this->sap_error_description,
                'sap_document_number'                               => $this->sap_document_number,
                'created_at'                                        => $this->created_at,
                'updated_at'                                        => $this->updated_at,
                'deleted_at'                                        => $this->deleted_at,
                'email_attachment_id'                               => $this->email_attachment_id,
                'sent_to_ocr'                                       => $this->sent_to_ocr,
                'sent_to_certify'                                   => $this->sent_to_certify,
                'ocr_processed'                                     => $this->ocr_processed,
                'is_set_as_ocr_document_downloaded'                 => $this->is_set_as_ocr_document_downloaded,
                'certify_generated'                                 => $this->certify_generated,
                'department'                                        => $this->department,
                'comercial_sales'                                   => $this->comercial_sales,
                'customer_id'                                       => $this->customer_id,
                'delivery_amount'                                   => $this->delivery_amount,
                'total_net_2'                                       => $this->total_net_2,
                'tax_rate_2'                                        => $this->tax_rate_2,
                'tax_type_2'                                        => $this->tax_type_2,
                'tax_balance_range_2'                               => $this->tax_balance_range_2,
                'adjusted_tax_amount_2'                             => $this->adjusted_tax_amount_2,
                'tax_amount_2'                                      => $this->tax_amount_2,
                'total_amount_2'                                    => $this->total_amount_2,
                'adjusted_total_amount_2'                           => $this->adjusted_total_amount_2,
                'total_net_3'                                       => $this->total_net_3,
                'tax_rate_3'                                        => $this->tax_rate_3,
                'tax_type_3'                                        => $this->tax_type_3,
                'tax_balance_range_3'                               => $this->tax_balance_range_3,
                'adjusted_tax_amount_3'                             => $this->adjusted_tax_amount_3,
                'tax_amount_3'                                      => $this->tax_amount_3,
                'total_amount_3'                                    => $this->total_amount_3,
                'adjusted_total_amount_3'                           => $this->adjusted_total_amount_3,
                'total_net_4'                                       => $this->total_net_4,
                'tax_rate_4'                                        => $this->tax_rate_4,
                'tax_type_4'                                        => $this->tax_type_4,
                'tax_balance_range_4'                               => $this->tax_balance_range_4,
                'adjusted_tax_amount_4'                             => $this->adjusted_tax_amount_4,
                'tax_amount_4'                                      => $this->tax_amount_4,
                'total_amount_4'                                    => $this->total_amount_4,
                'adjusted_total_amount_4'                           => $this->adjusted_total_amount_4,
                'total_net_5'                                       => $this->total_net_5,
                'tax_rate_5'                                        => $this->tax_rate_5,
                'tax_type_5'                                        => $this->tax_type_5,
                'tax_balance_range_5'                               => $this->tax_balance_range_5,
                'adjusted_tax_amount_5'                             => $this->adjusted_tax_amount_5,
                'tax_amount_5'                                      => $this->tax_amount_5,
                'total_amount_5'                                    => $this->total_amount_5,
                'adjusted_total_amount_5'                           => $this->adjusted_total_amount_5,
                'bank_code_number'                                  => $this->bank_code_number,
                'bank_iban'                                         => $this->bank_iban,
                'discount_amount'                                   => $this->discount_amount,
                'comments'                                          => $this->comments,
                'sap_update'                                        => $this->sap_update,
                'provider_street'                                   => $this->provider_street,
                'provider_city'                                     => $this->provider_city,
                'provider_postal_code'                              => $this->provider_postal_code,
                'provider_state'                                    => $this->provider_state,
                'downloaded_at'                                     => $this->downloaded_at,
                'filename'                                          => $this->filename,
                'is_correct'                                        => $this->is_correct,
                'processed_at'                                      => $this->processed_at,
                'contabilized_at'                                   => $this->contabilized_at,
                'aws_route'                                         => $this->aws_route,
                'alert_flag_from_ocr'                               => $this->alert_flag_from_ocr,
                'taxes_total_amount_without_adjustment'             => $this->taxes_total_amount_without_adjustment,
                'invalidation_reason'                               => $this->invalidation_reason,
                'invalidation_reason_extended'                      => $this->invalidation_reason_extended,
                'base_irpf_sap'                                     => $this->base_irpf_sap,
                'has_unread_messages'                               => $this->has_unread_messages,
                'concat_tax_types'                                  => $this->concat_tax_types,
                'invoice_lines'                                     => InvoiceLine::collection($this->invoiceLines),
                'chats'                                             => $this->chats,
        ];

        /*
        "id": "065af200-84b8-40e6-a451-24d4db98928e",
                        "invoice_id": "0c65ae4f-cf77-4268-840e-461517f8b9ff",
                        "material_code": "",
                        "material_description": "10% Network Management Services",
                        "material_qty": "1.00",
                        "packaging_type": null,
                        "unit_price": 0,
                        "currency": null,
                        "taxes": 10,
                        "tax_type_id": "8fa7e1be-1fe9-4b76-b28c-6c3eb0ca4a11",
                        "gross_amount": 6300,
                        "net_amount": 6300,
                        "irpf": 0,
                        "taxes_amount": 630,
                        "total_amount": 6930,
                        "taxes_id": "S2",
                        "created_at": "2021-04-14T07:25:42.000000Z",
                        "updated_at": "2021-10-20T14:40:42.000000Z",
                        "item_order_line_bulk": 10,
                        "item_order_num_bulk": "J-00011/2021",
                        "customer_id": null,
                        "discount_amount": 0,
                        "discount_rate": 0,
                        "qty_measuring_unit": null,
                        "unit_price_measuring_unit": null,
                        "item_coming_from_header": null



                        // Chats

                        "id": "7b5f0dd1-c60d-4040-a27d-bd80b6d52a99",
                        "commentable_id": "0c65ae4f-cf77-4268-840e-461517f8b9ff",
                        "commentable_type": "App\\Invoice",
                        "user_id": "541d6122-4acf-4a5b-b05a-ec4621d4bb21",
                        "comment": "con users",
                        "is_new": 0,
                        "thread_id": 3,
                        "created_at": "2021-09-30T16:40:34.000000Z",
                        "updated_at": "2021-09-30T16:41:05.000000Z"

        */

        //return parent::toArray($request);
    }
}
