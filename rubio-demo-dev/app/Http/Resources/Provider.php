<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class Provider extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'vat_number'            => $this->vat_number,
            'community_vat_number'  => $this->community_vat_number,
            'name'                  => $this->name,
            'country'               => $this->country,
            'zip_code'              => $this->zip_code,
            'city'                  => $this->city,
            'address'               => $this->address,
            'company'               => $this->company,
            'payment_condition'     => $this->payment_condition,
            'payment_type'          => $this->payment_type,
            'mediator'              => $this->mediator,
            'phone'                 => $this->phone ?? null,
            'smtp_addr'             => $this->email_orders ?? $this->email ?? $this->rgpd_email ?? ''
        ];
    }

    public function with($request)
    {
        return ['status' => 'success'];
    }
}
