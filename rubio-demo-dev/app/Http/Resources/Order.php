<?php

namespace App\Http\Resources;

use App\Company;
use App\Order as AppOrder;
use Illuminate\Http\Resources\Json\JsonResource;

class Order extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $order = AppOrder::find($this->id);
        $company = Company::where('code',$order->company)->first();
        $this->buyer = $company->buyer;
        $this->save();
        return [
            'id'                    => $this->id,
            'supplier_code'         => $this->provider->external_id ?? null,
            'payment_conditions'    => $this->provider->payment_condition ?? null,
            'order_number'          => $this->order_number,
            'company'               => $this->company,
            'date'                  => $this->date,
            'buyer'                 => $this->buyer,
            'purchasing_group'      => $this->purchasing_group,
            'document_type'         => $this->document_type,
            'order_lines'           => OrderLine::collection($this->orderLines),
        ];
    }

    public function with($request)
    {
        return ['status' => 'success'];
    }
}
