<?php

namespace App\Http\Resources;

use App\Invoice;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Str;

class InvoiceLine extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $invoice = Invoice::where('id',$this->invoice_id)->first();
        if(($this->unit_price == 0)&&(Str::startsWith($invoice->intern_order,'47'))) {
            $this->unit_price = $invoice->net_amount/$this->material_qty;
            //$this->net_amount = $invoice->net_amount;
            //$this->total_amount = $invoice->total_amount;
        }
        return [
            'id' => $this->id,
            'invoice_id' => $this->invoice_id,
            'material_code' => $this->material_code,
            'material_description' => $this->material_description,
            'material_qty' => $this->material_qty,
            'packaging_type' => $this->packaging_type,
            'unit_price' => $this->unit_price ?? $invoice->total_net,
            'currency' => $this->currency,
            'taxes' => $this->taxes,
            'tax_type_id' => $this->tax_type_id,
            'gross_amount' => $this->gross_amount,
            'net_amount' => $this->net_amount,
            'irpf' => $this->irpf,
            'taxes_amount' => $this->taxes_amount,
            'total_amount' => $this->total_amount,
            'taxes_id' => $this->taxes_id,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'item_order_line_bulk' => $this->item_order_line_bulk,
            'item_order_num_bulk' => $this->item_order_num_bulk,
            'customer_id' => $this->customer_id,
            'discount_amount' => $this->discount_amount,
            'discount_rate' => $this->discount_rate,
            'qty_measuring_unit' => $this->qty_measuring_unit,
            'unit_price_measuring_unit' => $this->unit_price_measuring_unit,
            'item_coming_from_header' => $this->item_coming_from_header,
        ];
        //return parent::toArray($request);
    }
}
