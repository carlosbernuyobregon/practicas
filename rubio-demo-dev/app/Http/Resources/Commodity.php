<?php

namespace App\Http\Resources;

use Carbon\Carbon;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Facades\Log;
use function GuzzleHttp\Promise\all;

class Commodity extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        try {
            $orderLine = \App\OrderLine::with('order')->findOrFail($this->order_line_id);

            if (!$request->test) {
                $this->resource->save();
            }

            return [
                'id'                => $this->id,
                'order_number'      => $orderLine->order->order_number ?? $orderLine->order->external_id ?? null,
                'position'          => $orderLine->position,
//                'amount'            => $this->amount,
                'qty'               => $this->qty,
                'delivered_date'    => Carbon::parse($this->delivered_at)->toDateString(),
            ];

        } catch (\Exception $e) {
            Log::error($e->getMessage(), [
                'class'     => __CLASS__,
                'method'    => __METHOD__,
            ]);
        }

        return [];
    }

    public function with($request)
    {
        return ['status' => 'success'];
    }
}
