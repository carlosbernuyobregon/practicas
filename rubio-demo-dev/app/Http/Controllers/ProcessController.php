<?php

namespace App\Http\Controllers;

use App\Invoice;
use App\Process;

use Illuminate\Http\Request;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Client as GuzzleClient;


class ProcessController extends Controller
{
    //

    public function getWorkflows()
    {
        $client = new GuzzleClient();
        $res = $client->request('GET', config('custom.k2_server').'/workflows', [
            'auth' => [auth()->user()->samaccountname, decrypt(auth()->user()->adpwd)],
            'verify' => false
        ]);
        echo $res->getStatusCode();
        // "200"
        echo $res->getHeader('content-type')[0];
        // 'application/json; charset=utf8'
        echo $res->getBody();
        echo 'Valid!';
        die();
    }
    public function getAllTasks()
    {
        $client = new GuzzleClient();
        $res = $client->request('GET', config('custom.k2_server').'/tasks?state=All', [
            'auth' => [auth()->user()->samaccountname, decrypt(auth()->user()->adpwd)],
            'verify' => false
        ]);

        $tasks = json_decode($res->getBody())->tasks;
        return view('alltasks',compact('tasks'));
    }
    public function getMyTask()
    {
        return view('invoices.new-list',[
            'tableType' => 'pending',
            'status'    => 'Listado de tareas',
            'table'     => 'invoices',
            'view'      => false,
            'entries'   => config('custom.max-entries-for-invoices-tasks-table-list')
        ]);
    }
    public function getTasks()
    {
        $client = new GuzzleClient();
        $res = $client->request('GET', config('custom.k2_server').'/tasks?state=All', [
            'auth' => [auth()->user()->samaccountname, decrypt(auth()->user()->adpwd)],
            'verify' => false
        ]);
        //echo $res->getStatusCode();
        // "200"
        //echo $res->getHeader('content-type')[0];
        // 'application/json; charset=utf8'
        //echo $res->getBody();
        //echo 'Valid!';
        $requestIds = array();
        $validationIds = array();
        $requestUrls = array();
        $flowsUrls = array();
        foreach(json_decode($res->getBody())->tasks as $task){
            $url = parse_url($task->formURL);
            $path = explode('/',$url['path']);
            if(count($path)>2){
                switch ($path[2]) {
                    case 'request':
                        $taskId = strtoupper($path[3]);
                        $requestIds[] = $taskId;
                        $requestUrls[$taskId] = $task->formURL;
                        $flowsUrls[$taskId] = $task->viewFlowURL;
                        break;
                }
            }
        }
        $processRequests = array();
        return view('tasks', compact('processRequests','requestUrls','flowsUrls'));
    }
    public function reassignTask(Request $request, $sn)
    {
        if ($sn != $request->input('bpm_sn')){
            return redirect()->back()->with('status','Error.');
        }
        $process = new Process;
        $process->redirectTask($sn,$request->input('reassign_user'));
    }
    public function endWorkflow($workflowId)
    {
        $dataFields = [
            'folio' => 'Workflow : ' . $workflowId,
            'expectedDuration' => 86400,
            'priority' => 1,
            'dataFields' => [
                'WorkflowProcID' => $workflowId,
            ]
        ];
        $process = new Process;
        $response =  $process->submitProcess('end-workflow',$dataFields,false);
        return redirect()->route('tasks.list')->with('status', 'Process ended.');
    }

}
