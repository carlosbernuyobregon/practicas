<?php

namespace App\Http\Controllers;

use App\Order;
use App\Process;
use Illuminate\Http\Request;

class OrderController extends Controller
{
    public function open($processName, $processId = null, $processInstance = null, $processActivity = null, Request $request)
    {
        $process = new Process();
        $variables = $process->getFormVariables($processInstance);
        //dd($variables);
        try {
            if($variables['processId'] != $processId){
                return redirect()
                            ->route('order.tasks')
                            ->with('danger', 'Task ID ' . $processInstance . ' does not exist');
            }
            $order = Order::find($processId);
            if(!$order){
                return redirect()
                            ->route('order.tasks')
                            ->with('danger', 'The order ' . $processInstance . ' does not exist');
            }
        } catch (\Throwable $th) {
            return redirect()
                            ->route('order.tasks')
                            ->with('danger', 'There is a problem with this task');
        }
        //dd($order);
        return view('orders.edit',[
            'order' => $order,
            'editable' => (in_array($processActivity,['modificar-solicitud'])) ? true : false,
            'uploadFiles' => true,
            'actions' => $variables['actions'],
            'processInstance' => $processInstance
        ]);
    }
    public function index($status = null)
    {
        //$orders = Order::where('external_id','!=',null)->get();
        return view('orders.index',[
            'task' => false,
            'status' => null
        ]);
    }
    public function indexAdmin()
    {
        return view('orders.index',[
            'task' => false,
            'status' => null,
            'admin' => true
        ]);
    }
    public function dashboard()
    {
        return view('orders.dashboard');
    }
    public function new()
    {
        return view('orders.edit',[
            'task' => true
        ]);
    }
    public function edit(Order $order)
    {

        return view('orders.edit',[
            'order' => $order,
            'editable' => true,
            'task' => ($order->status == 0) ? true : false
        ]);
    }
    public function show(Order $order)
    {
        return view('orders.edit',[
            'order' => $order,
            'editable' => false,
            'task' => ($order->status == 0) ? true : false
        ]);
    }
    public function test(Order $order = null)
    {
        dd('hola');
        return view('testing',[
            'order' => $order
        ]);
    }
    public function tasks()
    {
        dd('hola');

    }
    public function taskList($status = 1)
    {
        /*
        Managers:
            - Alex Tamayo (IT)
            - Toni Felis (Finanzas)
            - Andrés Ramón: aramon@labrubio.com (Compras)
        Directors:
            - Andrés Ramón (IT)
            - Andrés Ramón (Finanzas)
            - Toni Félis (Compras)
        Directora general:
            - Isabel Ramírez
        Usuaris:
            - Yolanda (administració i compres)
            - Sara (administració i compres)
            - Isabel (administració i compres)
            - Alba Marín amarin@labrubio.com (administració)
            - Zeus Díaz zdiaz@labrubio.com (IT)
            - David Rodriguez drodriguez@labrubio.com (IT)
        */
        return view('orders.index',[
            'task' => true,
            'status' => $status
        ]);
    }
    public function orderFile(Order $order)
    {
        ini_set('max_execution_time', 3000);
        ini_set("memory_limit","2024M");
        $pdf = \PDF::loadView('docs.order',[
            'order' => $order,
            'orderLines' => $order->orderLines
        ]);
        return $pdf->stream('O_' . $order->order_number . '.pdf')->header('Content-Type','application/pdf');
        return view('docs.order',[
            'order' => $order,
            'orderLines' => $order->orderLines
        ]);

    }
}
