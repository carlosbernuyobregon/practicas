<?php

namespace App\Http\Controllers;


use App\Email;
use App\EmailAttachment;
use App\Invoice;
use App\Models\EmailAttachInvoiceTracking;
use App\Readsoft;
use App\ReadsoftConnection;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use LdapRecord\Query\Events\Read;
use Webklex\IMAP\Facades\Client as FacadesClient;
use Webklex\PHPIMAP\Client;
use Webklex\PHPIMAP\ClientManager;
use Webklex\PHPIMAP\IMAP;


class InvoicesIntegrationController extends Controller
{
    public static function integrateToReadsoft()
    {
        $cm = new ClientManager();

        $client = $cm->make([
            'host'          => config('custom.email_integration.host'),
            'port'          => config('custom.email_integration.port'),
            'encryption'    => config('custom.email_integration.encryption'),
            'validate_cert' => config('custom.email_integration.validate_cert'),
            'username'      => config('custom.email_integration.username'),
            'password'      => config('custom.email_integration.password'),
            'protocol'      => config('custom.email_integration.protocol')
        ]);
        try {
            $client->connect();
        } catch (\Exception $e) {
            Log::error($e->getMessage(), [
                'class'     => __CLASS__,
                'method'    => __METHOD__,
            ]);

            return false;
        }

        $folders = $client->getFolders();

        $folder     = $client->getFolder(config('custom.email_integration.read_folder'));
        $messages   = $folder->query()->all()->limit(10)->get();
        $i          = 0;
        $tracking   = false;
        $email      = false;

        foreach ($messages as $message) {
            $existingEmail = Email::where('email_uid', (string)$message->getUid())->where('double_check', true)->first();

            if ($existingEmail == null) {
                $i++;
                $existingEmail  = Email::where('email_uid', (string)$message->getUid())->first();
                $tracking       = EmailAttachInvoiceTracking::firstOrCreate(['message_uuid' => (string)$message->getUid()]);

                if ($existingEmail == null) {
                    try {
                        $to = (string)$message->getTo()[0]->mail;
                    } catch (\Throwable $th) {
                        $to = 'not available';
                    }
                    try {
                        $date = $message->getAttributes()['date'][0];
                    } catch (\Throwable $th) {
                        $date = now();
                    }
                    $data = [
                        'email_uid'     => (string)$message->getUid() ?? '',
                        'email'         => utf8_encode((string)$message->getMessageId()) ?? '',
                        'from'          => utf8_encode((string)$message->getFrom()[0]->mail) ?? '',
                        'attachments'   => $message->getAttachments()->count() ?? 0,
                        'to'            => $to,
                        'subject'       => utf8_encode((string)$message->getSubject()) ?? '',
                        'email_date'    => $date,
                    ];
                    try {
                        $email                  = Email::create($data);
                        $tracking->email_uuid   = $email->id;

                        try {
                            $tracking->save();
                        } catch (\PDOException $e) {
                            Log::error($e->getMessage(), [
                                'class'     => __CLASS__,
                                'method'    => __METHOD__,
                            ]);
                            dump($e);
                        }

                    } catch (\Exception $th) {
                        dd($th);
                    }
                } else {
                    try {
                        $date = $message->getAttributes()['date'][0];
                    } catch (\Throwable $th) {
                        $date = now();
                    }
                    $existingEmail->double_check    = true;
                    $existingEmail->email_date      = $date;
                    $existingEmail->save();
                    $email                          = $existingEmail;
                    $tracking->email_uuid           = $email->id;
                    $tracking->email_double_check   = true;

                    try {
                        $tracking->save();
                    } catch (\PDOException $e) {
                        Log::error($e->getMessage(), [
                            'class'     => __CLASS__,
                            'method'    => __METHOD__,
                        ]);
                        dump($e);
                    }
                }

                //dd($email);
                if (($email != null) && ($message->getAttachments()->count() > 0)) {
                    $attachmentNumber = EmailAttachment::where('email_id', $email->id)->count();
                    if ($attachmentNumber == 0) {
                        $attachments            = $message->getAttachments();
                        $localPath              = sys_get_temp_dir() . '/';
                        $dateString             = Carbon::now()->format('Y/m/d');
                        $awsPath                = config('custom.iworking_public_bucket_folder_invoices') . '/' . $dateString . '/' . (string)$email->id;
                        $trackingAttachments    = [];
                        $trackingInvoices       = [];
                        $trackingSentOcr        = [];
                        $trackingFailedSentOcr  = [];

                        foreach ($attachments as $attachment) {

                            $newFilename        = Str::uuid() . '.' . $attachment->getExtension();
                            $filePath           = $localPath . $newFilename;
                            $attachment->save($localPath, $newFilename);
                            $awsPathWithName    = $awsPath . '/' . $newFilename;
                            $awsFileName        = Storage::disk('s3')->putFileAs($awsPath, $filePath, $newFilename);

                            if (File::exists($filePath)) {
                                File::delete($filePath);
                            }

                            $attachNewDoc = EmailAttachment::create([
                                'email_id'          => (string)$email->id,
                                'original_filename' => (string)$attachment->getName(),
                                'filename'          => $newFilename,
                                'filetype'          => (string)$attachment->getContentType(),
                                'aws_route'         => $awsFileName
                            ]);

                            $trackingAttachments[] = $attachNewDoc->id;

                            $invoice = Invoice::create([
                                'email_attachment_id'   => (string)$attachNewDoc->id,
                                'downloaded_at'         => now(),
                                'status'                => 0,
                                'filename'              => $newFilename,
                                'aws_route'             => $awsFileName
                            ]);

                            $trackingInvoices[] = $invoice->id;

                            $readsoft   = new Readsoft();
                            $connect    = $readsoft->connect();
                            if ($connect->getStatusCode() == 200) {
                                try {
                                    $readsoft->sendFile($awsFileName, (string)$attachNewDoc->id, $awsPath, $attachNewDoc->filetype);
                                } catch (\Throwable $th) {
                                    echo 'document error: ' . (string)$attachment->getContentType();
                                    echo "\n";
                                    $trackingFailedSentOcr[] = ['invoice_id' => $invoice->id];

                                    // Send warning email
                                    $readsoft->sendErrorSendingFileWarningEmail([
                                        'invoiceId'         => $invoice->id,
                                        'emailAttachmentId' => $attachNewDoc->id,
                                        'awsRoute'          => $awsFileName,
                                        'originalFilename'  => (string)$attachment->getName(),
                                        'fileType'          => (string)$attachment->getContentType(),
                                    ]);
                                }

                                $trackingSentOcr[] = ['invoice_id' => $invoice->id];
                            } else {
                                echo 'Readsoft connection error.';
                                $trackingFailedSentOcr[] = ['invoice_id' => $invoice->id];
                                try {
                                    $tracking->fill([
                                        'attachments_id'                    => $trackingAttachments,
                                        'invoices_id'                       => $trackingInvoices,
                                        'sent_to_ocr_by_invoice_id'         => $trackingSentOcr,
                                        'failed_sent_to_ocr_by_invoice_id'  => (!empty($trackingFailedSentOcr)) ? $trackingFailedSentOcr : 0
                                    ]);
                                    $tracking->save();
                                } catch (\PDOException $e) {
                                    Log::error($e->getMessage(), [
                                        'class'     => __CLASS__,
                                        'method'    => __METHOD__,
                                    ]);
                                    dump($e);
                                }
                                die();
                            }
                            $invoice->sent_to_ocr = true;
                            $invoice->save();

                            $tracking->fill([
                                'attachments_id'                    => $trackingAttachments,
                                'invoices_id'                       => $trackingInvoices,
                                'sent_to_ocr_by_invoice_id'         => $trackingSentOcr,
                                'failed_sent_to_ocr_by_invoice_id'  => (!empty($trackingFailedSentOcr)) ? $trackingFailedSentOcr : 0,
                            ]);
                            try {
                                $tracking->save();
                            } catch (\PDOException $e) {
                                Log::error($e->getMessage(), [
                                    'class'     => __CLASS__,
                                    'method'    => __METHOD__,
                                ]);
                                dump($e);
                            }
                        }
                    }
                }
                $message->setFlag('Seen');

                $tracking->email_seen = true;
                try {
                    $tracking->save();
                } catch (\PDOException $e) {
                    Log::error($e->getMessage(), [
                        'class'     => __CLASS__,
                        'method'    => __METHOD__,
                    ]);
                    dump($e);
                }
            } else {
                $message->setFlag('Seen');
                $tracking               = EmailAttachInvoiceTracking::where('message_uuid', $message->getUid())->first();

                if ($tracking) {
                    $tracking->email_seen   = true;
                }

                if ($message->move(config('custom.email_integration.save_folder')) == true){
                    echo 'Message has been moved';
                    echo "\n";
                    if ($tracking) {
                        $tracking->email_moved = true;
                    }
                } else {
                    echo 'Message could not be moved';
                    echo "\n";
                }

                if ($tracking) {
                    try {
                        $tracking->save();
                    } catch (\PDOException $e) {
                        Log::error($e->getMessage(), [
                            'class' => __CLASS__,
                            'method' => __METHOD__,
                        ]);
                        dump($e);
                    }
                }
                sleep(5);
            }
        }
        $invoices = $i;
        return $invoices;
    }
    public static function getFromReadsoft()
    {
        $readsoft = new Readsoft();
        $connect = $readsoft->connect();
        return $readsoft->getFiles();
    }
}
