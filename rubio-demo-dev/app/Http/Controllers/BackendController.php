<?php

namespace App\Http\Controllers;

use App\Country;
use App\Currency;
use App\Invoice;
use App\Request as AppRequest;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Session;
use App\Libraries\ApiAlfresco;
use Carbon\Carbon;
use DateTime;
use Exception;
use Illuminate\Support\Facades\DB;

class BackendController extends Controller
{
    //
    public function setLocale($locale)
    {
        try {
            Session::put('locale', $locale);
            Session::save();
            return back();
        } catch (\Exception $e) {
            return redirect('errors/500');
        }
    }
    public function dashboard()
    {
        $invoicesMonthQry = Invoice::select(DB::raw('COUNT(*) as count, MONTH(created_at) as month'))
            ->where('status','>=',1)
            ->groupBy(DB::raw('MONTH(created_at)'));

        // Determines if cache should be used or not
        (config('app.cache.active'))
            ? $invoicesMonth = Cache::remember('dashboard-invoices-per-month', config('custom.dashboard-cache-time-in-seconds'), function () use ($invoicesMonthQry) {
                return $invoicesMonthQry->get();
            })
            : $invoicesMonth = $invoicesMonthQry->get();

            $invoicesByMonth['month'] = '';
            $invoicesByMonth['count'] = '';
        foreach($invoicesMonth as $invoice){
            //dd($invoice);
            $monthNum  = $invoice->month;
            $dateObj   = DateTime::createFromFormat('!m', $monthNum);
            $monthName = $dateObj->format('F');
            $invoicesByMonth['month'] = $invoicesByMonth['month']."'".$monthName . "',";


            $invoicesByMonth['count'] = $invoicesByMonth['count'].$invoice->count . ',';
        }

        $invoicesByProviderQry = DB::table('invoices')
            ->select(DB::raw('provider_name as name, COUNT(*) as count'))
            ->where('status','>=',1)
            ->groupBy('provider_name')
            ->orderBy('count','desc')
            ->limit(10);

        // Determines if cache should be used or not
        (config('app.cache.active'))
            ? $invoicesByProvider = Cache::remember('dashboard-providers', config('custom.dashboard-cache-time-in-seconds'), function () use ($invoicesByProviderQry) {
                return $invoicesByProviderQry->get();
            })
            : $invoicesByProvider = $invoicesByProviderQry->get();

            $invoicesByCustomer['name'] = '';
            $invoicesByCustomer['count'] = '';
        foreach($invoicesByProvider as $invoice){
            //dd($invoice);
            $invoicesByCustomer['name'] = $invoicesByCustomer['name']."'".$invoice->name . "',";
            $invoicesByCustomer['count'] = $invoicesByCustomer['count'].$invoice->count . ',';
        }

        $amountByProviderQry = DB::table('invoices')
            ->select(DB::raw('provider_name as name, SUM(total_net) as count'))
            ->where('status','>=',1)
            ->groupBy('provider_name')
            ->orderBy('count','desc')
            ->limit(10);

        // Determines if cache should be used or not
        (config('app.cache.active'))
            ? $amountByProvider = Cache::remember('dashboard-amount-by-provider', config('custom.dashboard-cache-time-in-seconds'), function () use ($amountByProviderQry) {
                return $amountByProviderQry->get();
            })
            : $amountByProvider = $amountByProviderQry->get();

            $amountByProviderData['name'] = '';
            $amountByProviderData['count'] = '';
        foreach($amountByProvider as $invoice){
            //dd($invoice);
            $amountByProviderData['name'] = $amountByProviderData['name']."'".$invoice->name . "',";
            $amountByProviderData['count'] = $amountByProviderData['count'].$invoice->count . ',';
        }

        $invoicesByStatusQry = DB::table('invoices')
            ->select(DB::raw('status as name, COUNT(*) as count'))
            ->where('status','>=',1)
            ->groupBy('status');

        (config('app.cache.active'))
            ? $invoicesByStatus = Cache::remember('dashboard-invoices-by-status', config('custom.dashboard-cache-time-in-seconds'), function () use ($invoicesByStatusQry) {
                return $invoicesByStatusQry->get();
            })
            : $invoicesByStatus = $invoicesByStatusQry->get();

        $invoicesByStatusData['name'] = '';
        $invoicesByStatusData['count'] = '';
            //dd($invoicesByStatus);
        foreach($invoicesByStatus as $invoice){
            //dd($invoice);
            $invoicesByStatusData['name'] = $invoicesByStatusData['name']."'".trans('status.'.$invoice->name) . "',";
            $invoicesByStatusData['count'] = $invoicesByStatusData['count'].$invoice->count . ',';
        }
        //dd($invoicesByStatusData);
        return view('dashboard',[
            'invoicesByCustomer' => $invoicesByCustomer,
            'amountByProvider' => $amountByProviderData,
            'invoicesByStatus' => $invoicesByStatusData,
            'invoicesByMonth' => $invoicesByMonth
        ]);
    }
    public function tasksList()
    {
        $processRequests = AppRequest::where('next_task_user_id', auth()->user()->id)
            ->orWhere(function ($query) {
                $query->where('created_by', auth()->user()->id)
                    ->where('draft', true);
            })->get();
        return view('tasks', compact('processRequests'));
    }
    public function tasksListSubmited()
    {
        if(auth()->user()->hasRole('division-coordinator')){
            $processRequests = AppRequest::where('draft', false)
            ->get();
        }else{
            $processRequests = AppRequest::where('created_by', auth()->user()->id)
            ->where('draft', false)
            ->get();
        }

        return view('myrequest', compact('processRequests'));
    }
    public function getCurrencyByCountry(Request $request)
    {
        $country = Country::where('id',$request->input('countryId'))->first();
        return response()->json($country->currency);
    }

    public function validationPage()
    {
        echo 'hola';
        die();
        return view('processes.validations.partials.form');
    }
    public function testPage()
    {
        $appQualify = 123456;


        $urlRepository = 'http://alfresco-dev/alfresco/cmisatom';
        $user = 'extstrategying01';
        $pass = 'Depexe39';
        $folder =  '/Shared';
        $folderId = 'workspace://SpacesStore/xxx-xxx-xxx-xxx-xxx';
        $fileId = 'workspace://SpacesStore/xxx-xxx-xxx-xx-xxxx;`x.X';
        $childrenId = 'workspace://SpacesStore/xxx-xxx-Xxx-xxx-xxx';


/*
        $connection = ApiAlfresco::getInstance();
        try {
            $connection->connect(config('custom.cmis_url'),config('custom.cmis_user'),config('custom.cmis_password'));
        } catch (Exception $e) {
            dd($e);
        }
        $connection->setFolderById($folderId);
        $targetFolder = 'workspace://SpacesStore/60ead138-37a0-4b03-9893-a88da9b3aa18';

        $children = $connection->getChildrenFolder();
        foreach($children->objectList as $child){
            if ($child->properties['cmis:objectTypeId'] == 'cmis:document'){
                dd($child);
            }
        }
        die();


*/



        //return view('processes.validations.partials.form',compact('appQualify'));
        return view('test.test');

    }
    public function projects(){
        return view('test.projects');
    }
}
