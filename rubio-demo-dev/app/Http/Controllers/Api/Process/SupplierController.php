<?php

namespace App\Http\Controllers\Api\Process;

use App\Customer;
use App\Helpers\Helpers;
use App\Http\Controllers\Controller;
use App\Models\Provider;
use App\User;
use Illuminate\Http\Request;

class SupplierController extends Controller
{
    public $successStatus       = 200;
    public $errorStatus         = 400;
    public $unauthorizedStatus  = 401;
    public $internalError       = 500;

    /**
     * Validates request
     *
     * @param null $publicApiKey
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function validateCustomer(Request $request, $publicApiKey)
    {
        $customer = Customer::where('public_api_key', $publicApiKey)->first();

        if ($customer == null) {
            return response()->json(false, $this->errorStatus);
        }

        if ($request->bearerToken() != $customer->non_encripted) {
            return response()->json(false, $this->unauthorizedStatus);
        }

        return $customer;
    }

    public function setStatus(Request $request, $publicApiKey, Provider $provider)
    {
        /*
         * Lets wait until barer token is implemented on BPM
         * $response = $this->validateCustomer($request, $publicApiKey);

        if ($response instanceof \Illuminate\Http\JsonResponse) {
            return $response;
        } elseif ($response instanceof \Illuminate\Database\Eloquent\Model) {

        } else {
            return response()->json(false, $this->errorStatus);
        }*/
        // Get request data
        $requestData = json_decode($request->getContent(),true);

        try {
            $status             = $requestData['status'];
            $provider->status   = $status;

            if ($provider->save()) {
                try {
                    $provider->audit()->create([
                        'user_id'   => 'system',
                        'status'    => $provider->status,
                        'text'      => __('backend.providers.status.' . $provider->status),
                    ]);
                } catch (\Throwable $th) {
                    return response((string)$provider->id, $this->internalError);
                }

                // Send email to track sync
                Helpers::sendSyncEmail([
                    'syncType'      => 'BpmToProvider',
                    'object'        => $provider,
//                        'extraSendTo'   => config('custom.provider-process-validator-email')
                    'extraSendTo'   => User::find($provider->creator_user_id)->email ?? null
                ]);

                return response((string)$provider->id, $this->successStatus);
            } else {
                return response((string)$provider->id, $this->internalError);
            }
        } catch (\Throwable $th) {
            return response(false, $this->internalError);
        }
    }
}
