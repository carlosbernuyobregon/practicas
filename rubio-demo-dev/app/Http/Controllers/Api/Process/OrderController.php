<?php

namespace App\Http\Controllers\Api\Process;

use App\CostCenter;
use App\Http\Controllers\Controller;
use App\Mail\Orders\NewStatusNotification;
use App\Mail\SupplierOrderNotification;
use App\Order;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Mail;

class OrderController extends Controller
{
    public function userAssignee($publicApiKey = null, Order $order, Request $request)
    {
        $requestData = json_decode($request->getContent(),true);
        /*
        $monthlyTotal = Order::whereMonth('created_at',Carbon::now->mont())
                            where('status',8)->get();
        */
        try {
            Mail::to([$order->user->email])
                ->send(new NewStatusNotification($order));
        } catch (\Throwable $th) {
            $order->audit()->create([
                'user_id'   => 'system',
                'status'    => $order->status,
                'text'      => 'No se ha podido enviar email de notificación al usuario.'
            ]);
        }

        try {
            $level = $requestData['level'];
            if($level == 0){
                if($order->user_id){
                    return response((string)$order->user_id,200);
                }
             }else{
                $costCenter = CostCenter::select('manager_'.$level.'_id as manager','amount_level_'.$level.' as amount')
                                ->where('id',$order->cost_center_id)
                                ->first();
            }
            if($costCenter->manager == null){
                return response('false',400);
            }elseif($costCenter->manager == $order->user_id){
                return response('not-applies',200);
            }elseif($order->totalLines() >= $costCenter->amount){
                $order->manager_id = $costCenter->manager;
                $order->save();
                return response($costCenter->manager,200);
            }
            return response('not-applies',200);
        } catch (\Throwable $th) {
            return response('false',500);
        }

        // in case there's no manager to approve due to total amount, return "not-applies"
        // return response('not-applies',200);
    }
    public function setStatus($publicApiKey = null, Order $order, Request $request)
    {
        $requestData = json_decode($request->getContent(),true);
        try {
            $status = $requestData['status'];
            $order->status = $status;
            $order->save();
            $order->audit()->create([
                'user_id'   => 'system',
                'status'    => $order->status,
                'text'      => __('backend.orders.status.' . $order->status),
            ]);
            return response((string)$order->id, 200);
        } catch (\Throwable $th) {
            return response(false, 500);
        }
    }
    public function setAudit($publicApiKey = null, Order $order, Request $request)
    {
        try {
            $order->audit()->create([
                'user_id'   => 'system',
                'status'    => $order->status,
                'text'      => __('backend.orders.status.' . $order->status),
            ]);
            Mail::to([$order->user->email])
                ->send(new NewStatusNotification($order));
        } catch (\Throwable $th) {
            return response((string)$order->id,500);
        }
        return response((string)$order->id, 200);
    }
    public function confirmOrder($publicApiKey = null, Order $order, Request $request)
    {
        // Load relationship
        $order->load('provider');
        // Finally if orders is active, we send the email.
        if ($order->receiving_order_copy) {
            try {
                $order->audit()->create([
                    'user_id'   => 'system',
                    'status'    => $order->status,
                    'text'      => 'Pedido enviado a proveedor'
                ]);
                if(App::environment('production')){
                    Mail::to([$order->provider->email_orders ?? $order->provider->email])
                            ->bcc(['lpiqueras@strategying.com','dplanas@strategying.com','fcampos@strategying.com'])
                            ->send(new SupplierOrderNotification($order,true));
                }else{
                    Mail::to($order->user->email ?? 'dplanas@strategying.com')
                            ->bcc(['lpiqueras@strategying.com','dplanas@strategying.com','fcampos@strategying.com'])
                            ->send(new SupplierOrderNotification($order,false));
                }

            } catch (\Throwable $th) {
                return response((string)$order->id,500);
            }
        }
    }
}
