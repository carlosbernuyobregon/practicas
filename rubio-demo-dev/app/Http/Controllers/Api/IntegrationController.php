<?php

namespace App\Http\Controllers\Api;

use App\AccountingAccount;
use App\Asset;
use App\Budget;
use App\CostCenter;
use App\Delivery;
use App\Helpers\Helpers;
use App\Http\Controllers\Controller;
use App\Invoice;
use App\Library\Constants;
use App\MesureUnit;
use Illuminate\Http\Request;
use App\Customer;
use App\Material;
use App\Models\Provider;
use App\Order;
use App\PepElement;
use App\PepElementLine;
use App\TaxType;
use Exception;
use Illuminate\Http\Resources\Json\ResourceCollection;
use Illuminate\Support\Facades\Log;

class IntegrationController extends Controller
{
    public $successStatus       = 200;
    public $errorStatus         = 400;
    public $unauthorizedStatus  = 401;

    /**
     * Validates request
     *
     * @param null $publicApiKey
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function validateCustomer(Request $request, $publicApiKey)
    {
        $customer = Customer::where('public_api_key', $publicApiKey)->first();

        if ($customer == null) {
            return response()->json(false, $this->errorStatus);
        }

        if ($request->bearerToken() != $customer->non_encripted) {
            return response()->json(false, $this->unauthorizedStatus);
        }

        return $customer;
    }

    /**
     * IMPORTS
     */

    /**
     * Updates Invoice data after being posted on SAP
     *
     * @param null $publicApiKey
     * @param Invoice|null $invoice
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function invoiceUpdateFromSap(Request $request, $publicApiKey, Invoice $invoice = null)
    {
        $response = $this->validateCustomer($request, $publicApiKey);

        if ($response instanceof \Illuminate\Http\JsonResponse) {
            return $response;
        } elseif ($response instanceof \Illuminate\Database\Eloquent\Model) {

            $requestData = json_decode($request->getContent(),true);

            if ($invoice == null) {
                return response()->json(false, $this->errorStatus);
            }
            if ($invoice->id != $requestData['id']) {
                return response()->json(false, $this->errorStatus);
            }
            if (($requestData['status'] == 'true') || ($requestData['status'] == 'KO')) {
                $invoice->status                = 6;
                $invoice->sap_document_number   = $requestData['sap_document_number'];
                $invoice->contabilized_at       = now();
                $invoice->save();
                $invoice->audit()->create([
                    'user_id'   => null,
                    'status'    => $invoice->status,
                    'text'      => __('auditMessages.posted' . ' ' . $invoice->sap_document_number)
                ]);

                return response()->json($invoice->sap_document_number, $this->successStatus);
            } else {
                $invoice->status                = Constants::INVOICE_STATUS_ACCOUNTING_ERROR;
                $invoice->sap_error_code        = $requestData['sap_error_code'] ?? null;
                $invoice->sap_error_description = $requestData['sap_error_description'] ?? null;
                $invoice->contabilized_at       = now();
                $invoice->save();
                $invoice->audit()->create([
                    'user_id'   => null,
                    'status'    => $invoice->status,
                    'text'      => __('auditMessages.errorPosting' . ' ' . $invoice->sap_error_description . '-' . $invoice->sap_error_code)
                ]);

                return response()->json(false, $this->successStatus);
            }
        } else {
            return response()->json(false, $this->errorStatus);
        }
    }

    /**
     * Updates Order data after being posted on SAP
     *
     * @param Request $request
     * @param $publicApiKey
     * @param Order|null $order
     * @return \Illuminate\Http\JsonResponse
     */
    public function orderUpdateFromSap(Request $request, $publicApiKey, Order $order = null)
    {
        $response = $this->validateCustomer($request, $publicApiKey);

        if ($response instanceof \Illuminate\Http\JsonResponse) {
            return $response;
        } elseif ($response instanceof \Illuminate\Database\Eloquent\Model) {

            $requestData = json_decode($request->getContent(), true);
            if (!$order) {
                return response()->json(false, $this->errorStatus);
            }

            if ($order->id != $requestData['id']) {
                return response()->json(false, $this->errorStatus);
            }

            if (($requestData['status'] == 'true') || ($requestData['status'] == 'false')) {
                $order->status = $requestData['status'] == 'true'
                    ? Constants::ORDER_STATUS_UPDATED_IN_SAP_SUCCESS
                    : Constants::ORDER_STATUS_UPDATED_IN_SAP_ERROR;

                $order->order_number    = $requestData['sap_order_number'];
                $order->sap_updated_at  = now();
                $order->save();

                // Audit
                $auditTxt = 'Estado: ' . __('backend.orders.status.' . $order->status);

                if (!empty($requestData['sap_error_code'])) {
                    $auditTxt .= ' - Cod. Error: ' . $requestData['sap_error_code'];
                }

                if (!empty($requestData['sap_error_description'])) {
                    $auditTxt .= ' - Error: ' .$requestData['sap_error_description'];
                }

                $order->audit()->create([
                    'user_id'   => null,
                    'status'    => $order->status,
                    'text'      => $auditTxt
                ]);

                return response()->json($order->order_number, $this->successStatus);
            }
        }
    }

    /**
     * Imports Cost Centers from SAP
     *
     * @param null $publicApiKey
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function costCenterImport(Request $request, $publicApiKey)
    {
        $response = $this->validateCustomer($request, $publicApiKey);

        if ($response instanceof \Illuminate\Http\JsonResponse) {
            return $response;
        } elseif ($response instanceof \Illuminate\Database\Eloquent\Model) {

            $requestData    = json_decode($request->getContent(),true);
            $total          = $requestData['lines'];
            $i              = 0;

            foreach ($requestData['cecos'] as $item) {
                try {
                    CostCenter::updateOrCreate(
                        [
                            'external_id'   => $item['kostl'],
                            'company'       => $item['bukrs'],
                            'customer_id'   => (string)$response->id
                        ],
                        [
                            'name' => $item['ktext']
                        ]
                    );
                    $i++;
                } catch (Exception $e) {
                    return response()->json($item['kostl'], $this->errorStatus);
                }
            }

            if ($i != $total) {
                return response()->json(false, $this->successStatus);
            }
            return response()->json(true, $this->successStatus);

        } else {
            return response()->json(false, $this->errorStatus);
        }
    }

    /**
     * Imports accounting data from SAP
     *
     * @param null $publicApiKey
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function accountingAccountImport(Request $request, $publicApiKey)
    {
        $response = $this->validateCustomer($request, $publicApiKey);

        if ($response instanceof \Illuminate\Http\JsonResponse) {
            return $response;
        } elseif ($response instanceof \Illuminate\Database\Eloquent\Model) {

            $requestData    = json_decode($request->getContent(),true);
            $total          = $requestData['lines'];
            $i              = 0;

            foreach ($requestData['ctas_mayor'] as $item) {
                try {
                    AccountingAccount::updateOrCreate(
                        [
                            'external_id'   => $item['saknr'],
                            'company'       => $item['bukrs'],
                            'customer_id'   => (string)$response->id
                        ],
                        [
                            'name' => $item['txt20']
                        ]
                    );
                    $i++;
                } catch (Exception $e) {
                    return response()->json($item['saknr'], $this->errorStatus);
                }
            }
            if ($i != $total) {
                return response()->json(false, $this->successStatus);
            }

            return response()->json(true, $this->successStatus);
        } else {
            return response()->json(false, $this->errorStatus);
        }
    }

    /**
     * Imports materials data from SAP
     *
     * @param null $publicApiKey
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function materialImport(Request $request, $publicApiKey)
    {
        $response = $this->validateCustomer($request, $publicApiKey);

        if ($response instanceof \Illuminate\Http\JsonResponse) {
            return $response;
        } elseif ($response instanceof \Illuminate\Database\Eloquent\Model) {

            $requestData    = json_decode($request->getContent(),true);
            $total          = $requestData['lines'];
            $i              = 0;

            foreach ($requestData['mat_serv'] as $item) {
                try {
                    Material::updateOrCreate(
                        [
                            'external_id'   => $item['matnr'],
                            'company'       => $item['bukrs'],
                            'customer_id'   => (string)$response->id
                        ],
                        [
                            'material_name' => $item['maktx'],
                            'type'          => $item['mtart'],
                        ]
                    );
                    $i++;
                } catch (Exception $e) {
                    return response()->json($item['matnr'], $this->errorStatus);
                }
            }

            if ($i != $total) {
                return response()->json(false, $this->successStatus);
            }

            return response()->json(true, $this->successStatus);
        } else {
            return response()->json(false, $this->errorStatus);
        }
    }

    /**
     * Imports asset data from SAP
     *
     * @param null $publicApiKey
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function assetImport(Request $request, $publicApiKey)
    {
        $response = $this->validateCustomer($request, $publicApiKey);

        if ($response instanceof \Illuminate\Http\JsonResponse) {
            return $response;
        } elseif ($response instanceof \Illuminate\Database\Eloquent\Model) {

            $requestData    = json_decode($request->getContent(),true);
            $total          = $requestData['lines'];
            $i              = 0;

            foreach ($requestData['activosfijos'] as $item) {
                try {
                    Asset::updateOrCreate(
                        [
                            'external_id'   => $item['anln1'],
                            'subnumber'     => $item['anln2'],
                            'company'       => $item['bukrs'],
                            'customer_id'   => (string)$response->id
                        ],
                        [
                            'name'      => ($item['txt50'] == '') ? null : $item['txt50'],
                            'status'    => ($item['encurso'] == 'X') ? true : false,
                        ]
                    );
                    $i++;
                } catch (Exception $e) {
                    return response()->json($item['anln1'], $this->errorStatus);
                }
            }
            if ($i != $total) {
                return response()->json(false, $this->successStatus);
            }
            return response()->json(true, $this->successStatus);
        } else {
            return response()->json(false, $this->errorStatus);
        }
    }

    /**
     * Imports pep elements data from SAP
     *
     * @param null $publicApiKey
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function pepElementImport(Request $request, $publicApiKey)
    {
        $response = $this->validateCustomer($request, $publicApiKey);

        if ($response instanceof \Illuminate\Http\JsonResponse) {
            return $response;
        } elseif ($response instanceof \Illuminate\Database\Eloquent\Model) {

            $requestData    = json_decode($request->getContent(),true);
            $total          = $requestData['lines'];
            $i              = 0;

            foreach ($requestData['proyectos'] as $item) {

                try {
                    $parent = PepElement::updateOrCreate(
                        [
                            'external_id' => $item['pspid'],
                            'customer_id' => (string)$response->id
                        ],
                        [
                            'name'          => $item['post1'],
                            'sap_number_id' => $item['objnr'],
                            'type'          => $item['profl'],
                            'class'         => $item['scope'],
                        ]);
                    foreach ($item['peps'] as $childItem) {
                        $child = PepElementLine::updateOrCreate(
                            [
                                'external_id'       => $childItem['posid'],
                                'pep_element_id'    => (string)$parent->id,
                                'customer_id'       => (string)$response->id,
                            ],
                            [
                                'name_2'                    => $childItem['postu'] ?? '',
                                'pep_object_sap'            => $childItem['objnrpep'],
                                'top_node_id'               => $childItem['nodosuperior'],
                                'order'                     => $childItem['orden'],
                                'status'                    => $childItem['status'],
                                'company_co'                => $childItem['pkokr'],
                                'manageging_cost_center'    => $childItem['fkokr'],
                                'planification_element'     => $childItem['plakz'],
                                'imputation_element'        => $childItem['belkz'],
                                'company_fi'                => $childItem['pbukr'],
                                'pep_object_type'           => $childItem['scopepep'],
                                'currency'                  => $childItem['pwpos'],
                                'costing_schema'            => $childItem['kalsm'],
                                'interests_schema'          => $childItem['zschm'],
                                'investment_profile'        => $childItem['imprf'],
                                'pep_statistic_element'     => $childItem['xstat'],
                                'afec_id'                   => $childItem['anln1'],
                                'afec_subnumber'            => $childItem['anln2'],
                            ]);
                    }
                    $i++;
                } catch (Exception $e) {
                    return response()->json($item['pspid'], $this->errorStatus);
                }
            }
            if ($i != $total) {
                return response()->json(false, $this->successStatus);
            }

            return response()->json(true, $this->successStatus);
        } else {
            return response()->json(false, $this->errorStatus);
        }
    }

    /**
     * Imports budget data from SAP
     *
     * @param null $publicApiKey
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function budgetImport(Request $request, $publicApiKey)
    {
        /**
         * ToDo: 07-09-2021 - Al importar los budgets, falta poder ligarlo con un accounting account id, por ende nos debería llegar el nro de cuenta
         */
        $response = $this->validateCustomer($request, $publicApiKey);

        if ($response instanceof \Illuminate\Http\JsonResponse) {
            return $response;
        } elseif ($response instanceof \Illuminate\Database\Eloquent\Model) {

            $requestData = json_decode($request->getContent(),true);

            try {
                $total = $requestData['lines'];
            } catch (\Throwable $th) {
                return response()->json(false, $this->errorStatus);
            }

            $i = 0;

            foreach ($requestData['presupuestos'] as $item) {
                try {
                    $budget = Budget::updateOrCreate(
                        [
                            'external_id'       => $item['psphi'], // objnr de Elementos PEP
                            'pep_element_id'    => $item['pspnr'], // objnrpep de Posiciones elementos PEP
                            'customer_id'       => (string)$response->id
                        ],
                        [
                            'initial'       => $item['inicial'] ?? 0,
                            'released'      => $item['liberado'] ?? 0,
                            'rest'          => $item['resto'] ?? 0,
                            'available'     => $item['disponible'] ?? 0,
                            'consumed'      => $item['consumido'] ?? 0,
                            'compromised'   => $item['comprometido'] ?? 0,
                        ]
                    );
                    $i++;
                } catch (Exception $e) {
                    return response()->json($item['psphi'], $this->errorStatus);
                }
            }

            if ($i != $total) {
                return response()->json(false, $this->successStatus);
            }
            return response()->json(true, $this->successStatus);
        } else {
            return response()->json(false, $this->errorStatus);
        }
    }

    /**
     * Imports tax types data from SAP
     *
     * @param null $publicApiKey
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function taxesImport(Request $request, $publicApiKey)
    {
        $response = $this->validateCustomer($request, $publicApiKey);

        if ($response instanceof \Illuminate\Http\JsonResponse) {
            return $response;
        } elseif ($response instanceof \Illuminate\Database\Eloquent\Model) {

            $requestData    = json_decode($request->getContent(),true);
            $total          = $requestData['lines'];
            $i              = 0;

            foreach ($requestData['presupuestos'] as $item){
                try {
                    TaxType::updateOrCreate(
                        [
                            'external_id'       => $item['psphi'], // objnr de Elementos PEP
                            'pep_element_id'    => $item['pspnr'], // objnrpep de Posiciones elementos PEP
                            'customer_id'       => (string)$response->id
                        ],
                        [
                            'initial'       => $item['inicial'],
                            'released'      => $item['liberado'],
                            'rest'          => $item['resto'],
                            'available'     => $item['disponible'],
                            'consumed'      => $item['consumido'],
                            'compromised'   => $item['comprometido'],
                        ]
                    );
                    $i++;
                } catch (Exception $e) {
                    return response()->json($item['psphi'], $this->errorStatus);
                }
            }

            if ($i != $total) {
                return response()->json(false, $this->successStatus);
            }

            return response()->json(true, $this->successStatus);
        } else {
            return response()->json(false, $this->errorStatus);
        }
    }

    /**
     * Imports suppliers from SAP, creating new ones and updating existent ones
     *
     * @param Request $request
     * @param null $publicApiKey
     * @return \Illuminate\Http\JsonResponse
     */
    public function supplierImport(Request $request, $publicApiKey)
    {
        $response = $this->validateCustomer($request, $publicApiKey);

        if ($response instanceof \Illuminate\Http\JsonResponse) {
            return $response;
        } elseif ($response instanceof \Illuminate\Database\Eloquent\Model) {
            $customer       = $response;
            $requestData    = ($request->getContent()) ? json_decode($request->getContent(),true) : false;
            $total          = $requestData['lines'];
            $i              = 0;

            if (empty($requestData['proveedores'])) {
                return response()->json(false, $this->successStatus);
            }

            foreach ($requestData['proveedores'] as $item) {

                $provider = Provider::where('external_id', $item['lifnr'])
                    ->where('company', $item['bukrs'])
                    ->where('customer_id', (string)$customer->id)
                    ->first();

                if ($provider == null) {
                    $provider = Provider::where('external_id', null)
                        ->where('vat_number', $item['stcd1'])
                        ->where('customer_id', (string)$customer->id)
                        ->first();
                }

                if ($provider == null) {
                    Provider::create([
                        'external_id'           => $item['lifnr'],
                        'company'               => $item['bukrs'],
                        'customer_id'           => (string)$customer->id,
                        'vat_number'            => $item['stcd1'],
                        'community_vat_number'  => $item['stceg'],
                        'name'                  => $item['name1'],
                        'payment_condition'     => $item['zterm'],
                        'payment_type'          => $item['zwels'],
                        'country'               => $item['land1'],
                        'zip_code'              => $item['pstlz'],
                        'city'                  => $item['ort01'],
                        'address'               => $item['stras'],
                        /*'phone_number'          => $item['xxxx'],*/
                    ]);
                } else {
                    $provider->fill([
                        'external_id'           => $item['lifnr'],
                        'company'               => $item['bukrs'],
                        'customer_id'           => (string)$customer->id,
                        'vat_number'            => $item['stcd1'],
                        'community_vat_number'  => $item['stceg'],
                        'name'                  => $item['name1'],
                        'payment_condition'     => $item['zterm'],
                        'payment_type'          => $item['zwels'],
                        'country'               => $item['land1'],
                        'zip_code'              => $item['pstlz'],
                        'city'                  => $item['ort01'],
                        'address'               => $item['stras'],
                        /*'phone_number'          => $item['xxxx'],*/
                    ]);

                    $provider->save();
                }
                $i++;
            }

            if ($i != $total) {
                /*
                // Send email to track sync
                Helpers::sendSyncEmail([
                    'syncType'  => 'providersFromSap',
                    'processed' => $i,
                    'total'     => $total,
                    'success'   => false
                ]);
                */
                return response()->json(false, $this->successStatus);
            }
            /*
            // Send email to track sync
            Helpers::sendSyncEmail([
                'syncType'  => 'providersFromSap',
                'processed' => $i,
                'total'     => $total,
                'success'   => true
            ]);
            */
            return response()->json(true, $this->successStatus);
        } else {
            return response()->json(false, $this->errorStatus);
        }
    }

    /**
     * Imports Orders from SAP
     *
     * @param null $publicApiKey
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function orderImport(Request $request, $publicApiKey)
    {
        /**
         * December 2021.
         * There is no delivery_date coming from SAP, therefore order lines will no have one.
         * These are STOCK orders and there is no cost center related to any order.
         */
        $response = $this->validateCustomer($request, $publicApiKey);

        if ($response instanceof \Illuminate\Http\JsonResponse) {
            return $response;
        } elseif ($response instanceof \Illuminate\Database\Eloquent\Model) {
            $customer       = $response;
            $requestData    = json_decode($request->getContent(),true);
            $total          = $requestData['lines'];
            $i = 0;

            foreach ($requestData['pedidos'] as $item) {
                // Create an Order header
                try {
                    $order = Order::updateOrCreate([
                        'external_id'       => $item['ebeln']
                    ], [
                        'customer_id'       => (string)$customer->id,
                        'company'           => $item['bukrs'],
                        'supplier_code'     => $item['lifnr'],
                        'provider_id'       => (string)Provider::where('external_id', $item['lifnr'])
                            ->where('company', $item['bukrs'])
                            ->value('id'),
                        'date'              => str_replace('‑', '-', $item['bedat']),
                        'order_number'      => $item['ebeln'],
                        /*
                         * Not necessary for stock orders (45xxxxxxxx)
                        'purchasing_group'  => config('custom.orders-default-purchasing-group'),
                        'buyer'             => config('custom.orders-default-buyer'),
                        'document_type'     => config('custom.orders-default-document-type'),
                        'order_type'        => config('custom.orders-default-order-type'), // Once the other types are available, this value should be sent by SAP
                        */
                        'active'            => true,
                        'status'            => Constants::ORDER_STATUS_APPROVED
                    ]);
                } catch (Exception $e) {
                    Log::error($e->getMessage(), [
                        'class'     => __CLASS__,
                        'method'    => __METHOD__,
                    ]);
                    return response()->json($item['ebeln'], $this->errorStatus);
                }

                if ($order) {
                    // And now the order line
                    try {
                        $order->orderLines()->create([
                            'customer_id'           => (string)$customer->id,
                            'order_id'              => $order->id,
                            'position'              => $item['ebelp'],
                            'material_id'           => !empty($item['matnr'])
                                ? null
                                : (string)Material::where('external_id', $item['matnr'])
                                    ->where('company', $item['bukrs'])
                                    ->value('id'),
                            'position_material'     => !empty($item['matnr']) ? null : $item['matnr'],
                            'qty'                   => $item['menge'],
                            'net_amount'            => $item['netpr'],
                            'payment_conditions'    => $item['zterm'],
                            'payment_type'          => $item['zwels'],
                            'mesure_unit_id'        => MesureUnit::where('unit', $item['meins'])
                                ->value('id'),
                            'currency'              => $item['waers'],
                            'tax_type'              => $item['mwskz'],
                            'tax_type_id'           => TaxType::where('type', $item['mwskz'])
                                ->value('id'),
                            'delivery_date'         => null // There is no delivery_date coming from SAP

                        ]);
                    } catch (Exception $e) {
                        Log::error($e->getMessage(), [
                            'class'     => __CLASS__,
                            'method'    => __METHOD__,
                        ]);
                        return response()->json($item['ebeln'], $this->errorStatus);
                    }
                    $i++;
                }
            }

            if ($i != $total) {
                /*
                // Send email to track sync
                Helpers::sendSyncEmail([
                    'syncType'  => 'ordersFromSap',
                    'processed' => $i,
                    'total'     => $total,
                    'success'   => false
                ]);
                */
                return response()->json(false, $this->successStatus);
            }
            /*
            Helpers::sendSyncEmail([
                'syncType'  => 'ordersFromSap',
                'processed' => $i,
                'total'     => $total,
                'success'   => true
            ]);
            */
            return response()->json(true, $this->successStatus);

        } else {
            return response()->json(false, $this->errorStatus);
        }
    }

    /**
     * EXPORTS
     */

    /**
     * Exports Invoices to SAP (invoices ready to be posted)
     *
     * @param null $publicApiKey
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse | ResourceCollection
     */
    public function invoicesToSap(Request $request, $publicApiKey)
    {
        $response = $this->validateCustomer($request, $publicApiKey);

        if ($response instanceof \Illuminate\Http\JsonResponse) {
            return $response;
        } elseif ($response instanceof \Illuminate\Database\Eloquent\Model) {

            $invoices = Invoice::where('status', Constants::INVOICE_STATUS_ACCOUNTING_PENDING)
                ->with('invoiceLines')->get();



            return \App\Http\Resources\Invoice::collection($invoices);
            //return response()->json($invoices, $this->successStatus);
        } else {
            return response()->json(false, $this->errorStatus);
        }
    }

    /**
     * Exports Orders to SAP
     *
     * @param null $publicApiKey
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse | ResourceCollection
     */
    public function ordersToSap(Request $request, $publicApiKey)
    {
        $response = $this->validateCustomer($request, $publicApiKey);

        if ($response instanceof \Illuminate\Http\JsonResponse) {
            return $response;
        } elseif ($response instanceof \Illuminate\Database\Eloquent\Model) {

            $orders = Order::where('customer_id', $response->id)
                ->where('external_id', null)
                ->where('status', Constants::ORDER_STATUS_APPROVED)
                ->with(['orderLines.budget.accountingAccount', 'orderLines.accountingAccount'])
                ->get();
            /*
            // Send email to track sync
            Helpers::sendSyncEmail([
                'syncType'  => 'ordersToSap',
                'total'     => $orders->count()
            ]);
            */
            return \App\Http\Resources\Order::collection($orders);
//            return response()->json($orders, $this->successStatus);
        } else {
            return response()->json(false, $this->errorStatus);
        }
    }

    /**
     * Exports Providers  to SAP (ll new providers created from the app)
     *
     * @param null $publicApiKey
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse | ResourceCollection
     */
    public function providersToSap(Request $request, $publicApiKey)
    {
        $response = $this->validateCustomer($request, $publicApiKey);

        if ($response instanceof \Illuminate\Http\JsonResponse) {
            return $response;
        } elseif ($response instanceof \Illuminate\Database\Eloquent\Model) {

            $providers = Provider::where('status', Constants::PROVIDER_STATUS_APPROVED)
                ->where('customer_id', $response->id)
                ->whereNull('external_id')
                ->whereNotNull('vat_number')
                ->get();

            $filteredProviders = $providers->filter(function ($provider) {
                return $provider->vat_number != '' && strlen($provider->vat_number) > 5;
            });
            /*
            // Send email to track sync
            Helpers::sendSyncEmail([
                'syncType'  => 'providersToSap',
                'total'     => $filteredProviders->count()
            ]);
            */
            return \App\Http\Resources\Provider::collection($filteredProviders);
//            return response()->json($providers, $this->successStatus);
        } else {
            return response()->json(false, $this->errorStatus);
        }
    }

    /**
     * Exports Commodities to SAP
     *
     * @param null $publicApiKey
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse | ResourceCollection
     */
    public function commoditiesToSap(Request $request, $publicApiKey)
    {
        $response = $this->validateCustomer($request, $publicApiKey);

        if ($response instanceof \Illuminate\Http\JsonResponse) {
            return $response;
        } elseif ($response instanceof \Illuminate\Database\Eloquent\Model) {

            $commodities = Delivery::whereNull('sap_updated_at')
                ->get();

            /*
            // Send email to track sync
            Helpers::sendSyncEmail([
                'syncType'  => 'commoditiesToSap',
                'total'     => $commodities->count()
            ]);
            */
            return \App\Http\Resources\Commodity::collection($commodities);
        } else {
            return response()->json(false, $this->errorStatus);
        }
    }

    /**
     * Updates Delivery data after being posted on SAP
     *
     * @param Request $request
     * @param $publicApiKey
     * @param Delivery|null $delivery
     * @return \Illuminate\Http\JsonResponse
     */
    public function commodityUpdateFromSap(Request $request, $publicApiKey, Delivery $delivery = null)
    {
        $response = $this->validateCustomer($request, $publicApiKey);

        if ($response instanceof \Illuminate\Http\JsonResponse) {
            return $response;
        } elseif ($response instanceof \Illuminate\Database\Eloquent\Model) {

            $requestData = json_decode($request->getContent(), true);

            if (!$delivery) {
                return response()->json(false, $this->errorStatus);
            }

            if ($delivery->id != $requestData['id']) {
                return response()->json(false, $this->errorStatus);
            }

            if (($requestData['status'] == 'true') || ($requestData['status'] == 'false')) {
                $delivery->status = $requestData['status'] == 'true'
                    ? Constants::DELIVERY_STATUS_UPDATED_IN_SAP_SUCCESS
                    : Constants::DELIVERY_STATUS_UPDATED_IN_SAP_ERROR;

                $delivery->external_id     = $requestData['sap_delivery_number'];
                $delivery->sap_updated_at  = now();
                $delivery->save();

                // Audit
                $auditTxt = 'Estado: ' . __('backend.deliveries.status.' . $delivery->status);

                if (!empty($requestData['sap_error_code'])) {
                    $auditTxt .= ' - Cod. Error: ' . $requestData['sap_error_code'];
                }

                if (!empty($requestData['sap_error_description'])) {
                    $auditTxt .= ' - Error: ' .$requestData['sap_error_description'];
                }

                $delivery->audit()->create([
                    'user_id'   => null,
                    'status'    => $delivery->status,
                    'text'      => $auditTxt
                ]);

                return response()->json($delivery->external_id, $this->successStatus);
            }
        }

        return response()->json(false, $this->errorStatus);
    }
}
