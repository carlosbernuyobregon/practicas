<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Process;
use App\Project;
use App\Quote as AppQuote;
use App\QuoteLine;
use App\Role;
use App\User;
use Carbon\Carbon;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ProjectController extends Controller
{
    public $successStatus = 200;
    public $failStatus = 400;

    public function createFromQuoteId($quoteId = null)
    {
        if($quoteId == null){
            return response([],$this->failStatus);
        }
        $quote = AppQuote::find($quoteId);
        $projectType=$quote->projectType();

        if($quote->projectType()==='"A"'){
            $projectType = 'A';
        }else{
            $projectType = 'B';
        }
        $project = Project::create([
            'quote_id' => $quote->id,
            'project_type' => $projectType,
            'status' => 0,
            'manager_id' => $quote->request->created_by,
        ]);
        $project->project_id = $project->id;
        $project->save();
        $project->setPublicCode();

        $targetDate = '';
        foreach($quote->quoteLines as $line){
            if($line->delivery_date > $targetDate){
                $targetDate = $line->delivery_date;
            }
            $subprojectType ='';
            if(Str::contains($line->packagingCustomizationType->key_value, ['custom'])){
                $subprojectType = 'packaging';
                $packagingSubproject = Project::create([
                    'quote_id' => $quote->id,
                    'parent_id' => $project->id,
                    'quote_line_id' => $line->id,
                    'project_type' => $project->project_type,
                    'status' => 0,
                    'target_date' => $line->delivery_date,
                    'subproject_type' => 'packaging'
                ]);
                $packagingSubproject->setPublicCodeSubproject();
            }
            $subproject = Project::create([
                'quote_id' => $quote->id,
                'parent_id' => $project->id,
                'quote_line_id' => $line->id,
                'project_type' => $project->project_type,
                'status' => 0,
                'target_date' => $line->delivery_date,
                'subproject_type' => 'product'
            ]);
            $subproject->setPublicCodeSubproject();
        }
        $project->target_date = $targetDate;
        $project->update();
        return response()->json($project->id,$this->successStatus);
    }
    public function assignProject(Project $project)
    {

    }
    public function assignPackagingProjects(Project $project)
    {
        $packagingDesignerRole = Role::where('key_value','communication-designer')->first();

        $user = new User();
        $designer = $user->getOneUserByRole('communication-designer');

        $subprojects = $project->getPackagingSubprojects();
        foreach ($subprojects as $subproject){
            $item = Project::find($subproject->id);
            $item->designer_id = $designer->id;
            $item->save();
        }
        return response()->json($project->id,$this->successStatus);
    }
    public function getSubprojects(Project $project)
    {
        return response()->json($project->getSubprojects(),$this->successStatus);
    }
    public function getPackagignSubprojects(Project $project)
    {
        return response()->json($project->getPackagingSubprojects(),$this->successStatus);
    }
    public function getNonPackagingSubrojects(Project $project)
    {
        return response()->json($project->getNonPackagingSubprojects(),$this->successStatus);
    }
    public function generatePublicCodes()
    {
        $projects = Project::where('parent_id',null)->get();
        foreach($projects as $project){
            $project->setPublicCode();
            $c = 0;
            foreach($project->getSubprojects() as $subproject){
                $subproject->year_id = Carbon::now()->format('y');
                $subproject->code_id = $c;
                $subproject->save();
                $c++;
            }
        }
        return 'ok';
    }
    public function getProjectList(Request $request = null)
    {

        $query = Project::query();
        $query->where('status',0);
        $query->where('parent_id','!=',null);
        $data['iTotalRecords'] = $query->count();
        /*
        if (null !== $request->input('projectType') ) {
            $query->where('project_type', $request->input('projectType'));
        }
        if ($request->input('projectStatus') ) {
            $query->where('status', $request->input('projectStatus'));
        }
        if ($request->input('country') ) {
            $requests = DB::table('requests')->where('country_id', $request->input('country'))->pluck('id');
            $quotes = AppQuote::whereIn('request_id', $requests)->pluck('id');
            $query->whereIn('quote_id', $quotes);
        }
        if ($request->input('businessManager') ) {
            $requests = DB::table('requests')->where('created_by', $request->input('businessManager'))->pluck('id');
            $quotes = AppQuote::whereIn('request_id', $requests)->pluck('id');
            $query->whereIn('quote_id', $quotes);
        }
        if ($request->input('minUnits') != 0) {
            $quoteLines = QuoteLine::where('qty', '>=', $request->input('minUnits'))->pluck('id');
            $projects = Project::whereIn('quote_line_id', $quoteLines)->pluck('parent_id');
            $query->whereIn('id', $projects);
        }
        if ($request->input('maxUnits') != 0) {
            $quoteLines = QuoteLine::where('qty', '<=', $request->input('maxUnits'))->pluck('id');
            $query->whereIn('quote_line_id', $quoteLines);
        }
        if ($request->input('minTargetDate') != 0) {
            $query->where('target_date', '>=', $request->input('minTargetDate'));
        }
        if ($request->input('maxTargetDate') != 0) {
            $query->where('target_date', '<=', $request->input('maxTargetDate'));
        }
        if ($request->input('designer') ) {
            $query->where('designer_id', $request->input('designer'));
        }
        */
        $projects = $query->get();
        $data['iTotalDisplayRecords'] = $query->count();
        $data['sEcho'] = 0;
        $data['aaData'] = array();
        foreach($projects as $project){
            $item['Type'] = $project->project_type;
            $item['Status'] = $project->namedStatus();
            $item['ParentProject'] = $project->parent->getPublicCodeAttribute();
            $item['ProjectName'] = $project->getPublicCodeAttribute();
            $item['TargetDate'] = $project->target_date;
            $item['Designer'] = $project->designer->name;
            $item['Actions'] = null;
            $data['aaData'][] = $item;
            unset($item);
        }
        return response()->json($data,$this->successStatus);
    }
    public function getRequiresValidation(Project $project = null)
    {
        if($project == null){
            return response()->json(false,$this->failStatus);
        }
        $response = [
            'validation' => $project->requires_validation,
        ];
        return response()->json($response,$this->successStatus);
    }
    public function generateNewVersion(Project $project = null)
    {
        if($project == null){
            return response()->json(false,$this->failStatus);
        }
        $newProject = $project->replicate();
        $newProject->version = $newProject->version + 1;
        $newProject->parent_id;
        $newProject->save();
    }
}
