<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use App\User;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use LdapRecord\Container;
use LdapRecord\Models\ActiveDirectory\User as LdapUser;


class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }
    public function authenticated(Request $request, $user)
    {
        $user->storeRolesSession();
        //dd(session()->get('user.roles'));
        //session(['user.roles' => $user->roles]);
        return redirect()->route('dashboard');
    }
    public function login(Request $request)
    {
        $explodedEmail = explode('@', $request->input('email'));
        if(in_array($explodedEmail[1], config('custom.ad_domains'))){
            $connection = Container::getConnection('default');
/*
            try {
                $user = LdapUser::findByOrFail('samaccountname', $explodedEmail[0]);
                //$user = LdapUser::findByOrFail('samaccountname', $request->input('email'));
            } catch (\LdapRecord\Models\ModelNotFoundException $e) {
                return redirect()->back()->withError("User not found.");
            }
			*/
			$dn = "cn=". $explodedEmail[0] .",".config('ldap.connections.default.base_dn');
            //if ($connection->auth()->attempt($user->getDn(), $request->input('password'))) {
			if(count($explodedEmail) == 1){
				$email = $explodedEmail[0] . config('custom.internal_ad_domain_name');
			}else{
				$email = $request->input('email');
            }
            if(config('custom.ad_domain_access_type')=='email'){
                $loginUser = $explodedEmail[0] . config('custom.ad_domain_name');
            }else{
                try {
                    $newLoginUser = LdapUser::findByOrFail('samaccountname', $explodedEmail[0]);
                    //$user = LdapUser::findByOrFail('samaccountname', $request->input('email'));
                } catch (\LdapRecord\Models\ModelNotFoundException $e) {
                    return redirect()->back()->withError("Domain user not found.");
                }
                $loginUser = $newLoginUser->getDn();
            }
            //dd($loginUser);
            if ($connection->auth()->attempt($loginUser, $request->input('password'), $stayAuthenticated = true)) {
                $localUser = User::firstOrCreate(
                    ['email' => $email],
                    [
                        'samaccountname'=> $explodedEmail[0] . config('custom.ad_domain_name'),
                        'password' => bcrypt($request->input('password')),
                        'name' => $explodedEmail[0],
                        'first_name' => $explodedEmail[0],
                        'last_name' => $explodedEmail[0],
                        'adpwd' => encrypt($request->input('password')),
                        'domain' => $dn,
                        'guid' => $explodedEmail[0],
                    ]);
                    if($localUser->samaccountname==null){
                        $localUser->domain = $explodedEmail[0];
                        $localUser->guid = $explodedEmail[0];
                    }
                    if(config('custom.ad_domain_access_type')=='email'){
                        $localUser->samaccountname = $explodedEmail[0] . config('custom.ad_domain_name');
                    }else{
                        $localUser->samaccountname = $explodedEmail[0];
                    }

                    $localUser->password = bcrypt($request->input('password'));
                    $localUser->adpwd = encrypt($request->input('password'));
                    $localUser->save();
                    //dd($localUser);
                if($localUser->blocked){
                    $localUser->blocked = false;
                    $localUser->save();
                }
                if(!$localUser->active){
                    $localUser->active = true;
                    $localUser->save();
                }
            } else {
                $error = $connection->getLdapConnection()->getDiagnosticMessage();
                if (strpos($error, '532') !== false) {
                    return redirect()->back()->withError("Your password has expired.");
                } elseif (strpos($error, '533') !== false) {
                    return redirect()->back()->withError("Your account is disabled.");
                } elseif (strpos($error, '701') !== false) {
                    return redirect()->back()->withError("Your account has expired.");
                } elseif (strpos($error, '775') !== false) {
                    return redirect()->back()->withError("Your account is locked.");
                }
                return redirect()->back()->withError("Username or password is incorrect.");
            }
        }
        $this->validateLogin($request);

        // If the class is using the ThrottlesLogins trait, we can automatically throttle
        // the login attempts for this application. We'll key this by the username and
        // the IP address of the client making these requests into this application.
        if (method_exists($this, 'hasTooManyLoginAttempts') && $this->hasTooManyLoginAttempts($request)) {
            $this->fireLockoutEvent($request);
            return $this->sendLockoutResponse($request);
        }

        if ($this->attemptLogin($request)) {
            return $this->sendLoginResponse($request);
        }

        // If the login attempt was unsuccessful we will increment the number of attempts
        // to login and redirect the user back to the login form. Of course, when this
        // user surpasses their maximum number of attempts they will get locked out.
        $this->incrementLoginAttempts($request);

        return $this->sendFailedLoginResponse($request);
    }
}
