<?php

namespace App\Http\Controllers;

use App\Models\Provider;
use App\Order;
use App\Process;
use Illuminate\Http\Request;

class SupplierController extends Controller
{
    public function open($processName, $processId = null, $processInstance = null, $processActivity = null, Request $request)
    {

        $process = new Process();
        //dd($process->getTaskVariables($processInstance));
        $variables = $process->getFormVariables($processInstance);
        //$variables = $process->getFormVariables('b3d2bb15-4ab9-11ec-9655-0a93d3b15900');
        $provider = Provider::find($processId);
        //dd($provider);

        return view('suppliers.edit',[
            'provider' => $provider,
            'editable' => (in_array($processActivity,['alta-proveedor'])) ? true : false,
            'uploadFiles' => true,
            'task' => true,
            'actions' => $variables['actions'],
            'processInstance' => $processInstance
        ]);
    }
}
