<?php

namespace App\Http\Controllers;

use App\EmailAttachment;
use App\Invoice;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class InvoiceController extends Controller
{
    //
    public function review(Invoice $invoice)
    {
        // Lazy load the relationship to find out if there is more than one tax type applied to invoice lines
        $invoice->load('invoiceLines');

        // Default
        $hasMoreThanOneLineTax = false;

        if ($invoice->invoiceLines->pluck('tax_type_id')->unique()->count() > 1) {
            $hasMoreThanOneLineTax = true;
        }

        return view('invoices.review',[
            'invoice'               => $invoice,
            'view'                  => false,
            'hasMoreThanOneLineTax' => $hasMoreThanOneLineTax
        ]);
    }

    public function show(Invoice $invoice)
    {
        // Lazy load the relationship to find out if there is more than one tax type applied to invoice lines
        $invoice->load('invoiceLines');

        // Default
        $hasMoreThanOneLineTax = false;

        if ($invoice->invoiceLines->pluck('tax_type_id')->unique()->count() > 1) {
            $hasMoreThanOneLineTax = true;
        }

        return view('invoices.review',[
            'invoice'               => $invoice,
            'view'                  => true,
            'hasMoreThanOneLineTax' => $hasMoreThanOneLineTax
        ]);
    }

    public function pending()
    {
        return view('invoices.new-list',[
            'invoices' => Invoice::where('status',0)->get(),
            'status' => 'Documentos pendientes',
            'table' => 'email',
            'view' => false
        ]);
    }
    public function processed()
    {
        return view('invoices.list',[
            'invoices' => Invoice::where('status',1)->get(),
            'status' => 'Facturas procesadas',
            'table' => 'invoices',
            'view' => false
        ]);
    }
    public function all()
    {
        return view('invoices.new-list',[
            'tableType' => 'processed',
            'status' => 'Listado de facturas',
            'table' => 'invoices',
            'view' => true
        ]);
    }
    public function list()
    {
        return view('invoices.new-list',[
            'invoices' => Invoice::where('status','>=',1)->get(),
            'status' => 'Todos los documentos',
            'table' => 'all-email',
            'view' => true
        ]);

    }
    public function downloadInvoice($filename = null)
    {
        if($filename == null){
            return false;
        }
        $email = EmailAttachment::where('filename',$filename)->first();
        //$myFile = public_path('attachments/' . $email->email_id . '/' . $email->filename);
        return Storage::disk('s3')->download($email->aws_route);
        $headers = ['Content-Type: application/pdf'];
		return response()->download($myFile, $filename, $headers);
    }

    /**
     * Serves the invoice json data. Simulating what SAP gets when invoice data is sent to it.
     *
     * @param $invoiceId
     * @return \Illuminate\Http\JsonResponse
     */
    public function showMeTheJson($invoiceId)
    {
        $invoice = Invoice::find($invoiceId);

        if ($invoice) {
            return \App\Http\Resources\Invoice::collection($invoice);
            /*
            $invoice->load('invoiceLines');
            return response()->json($invoice, 200);
            */
        }
        return response()->json('Factura no encontrada', 401);
    }
}
