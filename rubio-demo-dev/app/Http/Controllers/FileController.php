<?php

namespace App\Http\Controllers;

use App\File;
use Illuminate\Http\Request;
use Ramsey\Uuid\Uuid;
use App\Libraries\ApiAlfresco;
use Exception;
use Illuminate\Support\Facades\Storage;
use Illuminate\View\View;

class FileController extends Controller
{
    //
    public function fileUpload($model = null, $id = null, Request $request)
    {
        /*
        $name = (string) Uuid::uuid4();
        $fileName = $name.'.'.$request->file('image')->extension();
        $request->file('image')->move(public_path('files'),$fileName);
        return true;
        */



        switch ($model) {
            case 'qualify-logo':
                $type = 'logo';
                $model = 'App\Qualify';
                $folderId = 'workspace://SpacesStore/60ead138-37a0-4b03-9893-a88da9b3aa18';
                break;
            case 'qualify-image':
                $type = 'image';
                $model = 'App\Qualify';
                $folderId = 'workspace://SpacesStore/60ead138-37a0-4b03-9893-a88da9b3aa18';
                break;
            case 'qualify-packaging':
                $type = 'packaging';
                $model = 'App\Qualify';
                $folderId = 'workspace://SpacesStore/60ead138-37a0-4b03-9893-a88da9b3aa18';
                break;
            default:
                $type = '';
                $model = 'App\\' . ucfirst($model);
                break;
        }
        if ($model == null || $id == null) {
            return false;
        }
        $name = (string) Uuid::uuid4();
        $fileName = $name . '.' . $request->file('image')->extension();
        $connection = ApiAlfresco::getInstance();
        try {
            $connection->connect(config('custom.cmis_url'), config('custom.cmis_user'), config('custom.cmis_password'));
        } catch (Exception $e) {
            echo 'Error connecting Alfresco';
        }

        $connection->setFolderById($folderId);

        $uploadedFile = $connection->createFile($fileName, array(), file_get_contents($request->file('image')), $request->file('image')->getClientMimeType());
        //$uploadedFile = $connection->uploadFile($request->file('image'));
        //dd($uploadedFile);

        //$request->file('image')->move(public_path('files'),$fileName);
        $uploadedFileId = explode('/', $uploadedFile->id);
        $uploadedFileId = explode(';', $uploadedFileId[3]);
        $publicUrl = 'http://alfresco-dev/share/proxy/alfresco/slingshot/node/content/workspace/SpacesStore/' . $uploadedFileId[0] . '/' . $fileName;
        $data = [
            //'fileable_id' => '5cc23e21-2bb3-46f6-91bf-a7b9a1385c5e',
            'fileable_id' => $id,
            'fileable_type' => $model,
            'name' => $request->file('image')->getClientOriginalName(),
            'filename' => $uploadedFile->properties['cmis:name'],
            'type' => $type,
            'cmis_id' => $uploadedFile->id,
            'cmis_url' => $publicUrl
        ];
        $file = File::create($data);
        return response()->json([
            //'url' => url('files') . '/' . $fileName
            'url' => route('files.view',$file->id)
        ]);
    }
    public function fileView($id){
        $file = File::find($id);
        if($file->cmis_url != null){
            $connection = ApiAlfresco::getInstance();
            try {
                $connection->connect(config('custom.cmis_url'), config('custom.cmis_user'), config('custom.cmis_password'));
            } catch (Exception $e) {
                echo 'Error connecting Alfresco';
            }
            $object = $connection->getObjectById($file->cmis_id);
            if($object != null){
                return $connection->displayFile($file->cmis_id);
            }
            return false;

        }
    }
    public function fileDownload(File $file)
    {
        return Storage::disk('s3')->download($file->cmis_url);
    }
    public function fileDelete($id = null)
    {
        if (!$id) {
            return false;
        }
        $file = File::find($id);
        if($file->cmis_url != null){
            $connection = ApiAlfresco::getInstance();
            try {
                $connection->connect(config('custom.cmis_url'), config('custom.cmis_user'), config('custom.cmis_password'));
            } catch (Exception $e) {
                echo 'Error connecting Alfresco';
            }
            $object = $connection->getObjectById($file->cmis_id);
            if($object != null){
                $connection->delete($file->cmis_id);
            }
        }
        $file->destroy($id);
        return true;
    }
    public function fileList($type, $modelId)
    {
        switch ($type) {
            case 'logo':
            case 'image':
            case 'packaging':
                $fileable_type = 'App\Qualify';
                break;
            default:
                $fileable_type = false;
                break;
        }
        $files = File::where('type', $type)
        ->where('fileable_type',$fileable_type)
        ->where('fileable_id', $modelId)->get();

        $html = view('processes.components.imagesList', compact($files))->render();
        return response()->json(array('success' => true, 'html' => $html));
    }
    public function fileGetList($type,$modelId){
        switch ($type) {
            case 'logo':
            case 'image':
            case 'packaging':
                $fileable_type = 'App\Qualify';
                break;
            default:
                $fileable_type = false;
                break;
        }
        $files = File::where('type', $type)
        ->where('fileable_type',$fileable_type)
        ->where('fileable_id', $modelId)->get();
        $html = '';
        foreach ($files as $file){
            $html .= '<div class="col-3 my-1 text-center px-1">';
            $html .= '<div class="embed-responsive embed-responsive-1by1">';
            $html .= '<a class="embed-responsive-item" target="_blank" href="' .route('files.view',$file->id) .'"">';
            $html .= '<img class="img-fluid" src="'. route('files.view',$file->id) .'"/>';
            $html .= '</a>';
            $html .= '</div>';
            $html .= '<br>';
			$html .= '<button type="button" class="btn btn-danger btn-sm mr-1" id="' .$file->id .'" onclick="deleteFile(this.id)">';
			$html .= '<i class="fas fa-trash-alt"></i>';
			$html .= '</button>';
			$html .= '<a href="'. route('files.download',$file->id) .'" class="btn btn-info btn-sm mr-1" download>';
			$html .= '<i class="fas fa-download"></i>';
			$html .= '</a>';
            $html .= '</div>';
        }
        return response()->json(array('success'=>true,'html'=>$html));
    }

}
