<?php

namespace App\Http\Controllers;

use App\BusinessLine;
use App\BusinessLineCountryUser;
use App\Country;
use App\Role;
use App\User;
use Illuminate\Http\Request;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $users = User::all();
        return view('cruds.users.index',compact('users'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $roles = Role::orderByTranslation('name')->get();
        return view('cruds.users.edit',compact('roles'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $data = $request->except('_token','password_confirmation','roles');
        $data['password'] = bcrypt($data['password']);
        $user = User::create($data);
        foreach ($request->input('roles') as $role => $value){
            $user->roles()->attach(Role::find($role));
        }
        return redirect()->route('user.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(User $user)
    {
        //
        $roles = Role::orderByTranslation('name')->get();
        return view('cruds.users.edit',compact('roles','user'));
    }
    public function profile()
    {
        $user = auth()->user();
        $roles = Role::orderByTranslation('name')->get();
        return view('cruds.users.edit',compact('roles','user'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, User $user)
    {
        //
        $user->update($request->except('_token','password_confirmation','roles'));
        $user->roles()->detach();
        foreach ($request->input('roles') as $role => $value){
            $user->roles()->attach(Role::find($role));
        }
        return redirect()->back()->with('status', 'User saved successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
    public function businessLineCountryUserList()
    {

        $users = BusinessLineCountryUser::all();
        return view('cruds.users.partials.listCountryBL',compact('users'));
    }
    public function businessLineCountryUserEdit($blCountryId = null)
    {
        $user = new User;
        $users = $user->getUsersByRole('business-manager');
        $countries = Country::all();
        $businessLines = BusinessLine::all();
        return view('cruds.users.partials.formCountryBL',compact('users','countries','businessLines','blCountryId'));
    }

}
