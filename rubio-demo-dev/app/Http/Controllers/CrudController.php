<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class CrudController extends Controller
{
    public function countries()
    {
        return view('cruds.countries.index');
    }
    public function currencies()
    {
        return view('cruds.currencies.index');
    }
    public function users()
    {
        return view('cruds.users-new.index');
    }

    public function roles()
    {
        return view('cruds.roles-new.index');
    }

    public function taxTypes()
    {
        return view('cruds.tax-types.index');
    }

    public function costCenters()
    {
        return view('cruds.cost-centers.index');
    }

    public function budgets()
    {
        return view('cruds.budgets.index');
    }

    public function accountingAccounts()
    {
        return view('cruds.accounting-accounts.index');
    }

    public function materials()
    {
        return view('cruds.materials.index');
    }

    public function assets()
    {
        return view('cruds.assets.index');
    }

    public function suppliers()
    {
        return view('cruds.suppliers.index');
    }

    public function orders()
    {
        return view('cruds.orders.index');
    }

    public function providerPaymentConditions()
    {
        return view('cruds.provider-payment-conditions.index');
    }
  
    public function providerPaymentTypes(){
        return view('cruds.provider-payment-types.index');
    }
}
