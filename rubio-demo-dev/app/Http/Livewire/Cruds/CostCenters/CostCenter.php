<?php

namespace App\Http\Livewire\Cruds\CostCenters;

use App\CostCenter as AppCostCenter;
use App\Customer;
use App\User;
use Illuminate\Support\Facades\Auth;
use Livewire\Component;
use Livewire\WithPagination;

class CostCenter extends Component
{
    use WithPagination;

    protected $paginationTheme = 'bootstrap';

    public ?AppCostCenter $costCenter = null;

    public $confirmDeleteId;
    public $entries = 10;
    public $search = '';

    protected $rules = [
        'costCenter.external_id' => 'nullable',
        'costCenter.customer_id' => 'nullable',
        'costCenter.name' => 'string|required',
        'costCenter.company' => 'string|required',
        'costCenter.manager_1_id' => 'string|required',
        'costCenter.manager_2_id' => 'string|required',
        'costCenter.manager_3_id' => 'string|required',
        'costCenter.amount_level_1' => 'required|between:0,999999.99',
        'costCenter.amount_level_2' => 'required|between:0,999999.99',
        'costCenter.amount_level_3' => 'required|between:0,999999.99',
        'costCenter.amount_level_secondary_1' => 'required|between:0,999999.99',
        'costCenter.amount_level_secondary_2' => 'required|between:0,999999.99',
        'costCenter.amount_level_secondary_3' => 'required|between:0,999999.99',
    ];

    protected $validationAttributes = [
        'costCenter.name' => 'name',
        'costCenter.company' => 'company',
    ];

    private function showModal()
    {
        $this->dispatchBrowserEvent('showModal');
    }


    /**
     * Renders the component view
     *
     * @return mixed
     */
    public function render()
    {
        return view('livewire.cruds.cost-centers.cost-center', [
            'customers' => Customer::orderBy('name')->get(),
            'users' => User::where('customer_id',Auth::user()->customer_id)->get(),
            'costCenters' =>  AppCostCenter::where('customer_id',Auth::user()->customer_id)
                ->where('name', 'like', '%' . $this->search . '%')
                ->orWhere('company', 'like', '%' . $this->search . '%')
                ->orderBy('name')
                ->paginate($this->entries),
        ]);
    }

    /**
     * Runs before a property called $search is updated
     *
     * @var array $search
     *
     * @return void
     */
    public function updatingSearch()
    {
        $this->gotoPage(1);
    }

    /**
     * Runs after any update to the Livewire component's data
     * (Using wire:model, not directly inside PHP)
     *
     * @param string $propertyName
     * @return void
     */
    public function updated($propertyName)
    {
        $this->validateOnly($propertyName);
    }

    /**
     * Resets all the form inputs
     *
     * @return void
     */
    public function resetInputs()
    {
        $this->resetValidation();
        $this->costCenter = new AppCostCenter();
    }

    /**
     * Create a new CostCenter and ShowModal
     */
    public function addNew()
    {
        $this->resetInputs();
        $this->showModal();
    }

    /**
     * Save new $costCenter object or updates an existing one
     * @var object $costCenter
     *
     * @return void
     */
    public function save()
    {
        $this->validate();

        try {
            //Create or update costCenter
            $this->costCenter->save();

            $this->dispatchBrowserEvent('costCenterStore');

            $this->resetInputs();

            session()->flash('status', trans('backend.crud.cost-center.messages.save'));
        } catch (\Illuminate\Database\QueryException $exception) {
            $errorInfo = $exception->errorInfo;

            session()->flash('error', $errorInfo[2]);
        }
    }

    /**
     * Gets an exististing $costCenter object to edit it
     * @var object $costCenter
     *
     * @param string $id
     * @return void
     */
    public function edit($id)
    {
        $this->resetInputs();

        $this->costCenter = AppCostCenter::findOrFail($id);

        $this->showModal();
    }

    /**
     * Changes the $confirmDeleteId value
     * @var string $confirmDeleteId
     *
     * @param string $id
     * @return void
     */
    public function confirmDelete($id)
    {
        $this->confirmDeleteId = $id;
    }

    /**
     * Deletes an existing $costCenter object
     * @var object $costCenter
     *
     * @return void
     */
    public function delete()
    {
        AppCostCenter::findOrFail($this->confirmDeleteId)->delete();
        $this->dispatchBrowserEvent('deleteItem');
        session()->flash('status', trans('backend.crud.cost-center.messages.delete'));
    }
}
