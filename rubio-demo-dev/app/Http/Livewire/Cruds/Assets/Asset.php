<?php

namespace App\Http\Livewire\Cruds\Assets;

use App\Asset as AppAsset;
use App\Customer;
use Livewire\Component;
use Livewire\WithPagination;

class Asset extends Component
{
    use WithPagination;

    protected $paginationTheme = 'bootstrap';

    public ?AppAsset $asset = null;

    public $confirmDeleteId;
    public $entries = 10;
    public $search = '';

    protected $rules = [
        'asset.external_id' => 'nullable',
        'asset.customer_id' => 'nullable',
        'asset.subnumber' => 'string|required',
        'asset.name' => 'string|required',
        'asset.company' => 'string|required',
        'asset.status' => 'numeric|required',
    ];

    protected $validationAttributes = [
        'asset.name' => 'name',
        'asset.subnumber' => 'subnumber',
        'asset.company' => 'company',
        'asset.status' => 'status',
    ];

    private function showModal()
    {
        $this->dispatchBrowserEvent('showModal');
    }

    public function render()
    {
        return view('livewire.cruds.assets.asset', [
            'customers' => Customer::orderBy('name')->get(),
            'assets' =>  AppAsset::where('name', 'like', '%' . $this->search . '%')
                ->orWhere('company', 'like', '%' . $this->search . '%')
                ->orWhere('status', 'like', '%' . $this->search . '%')
                ->orWhere('subnumber', 'like', '%' . $this->search . '%')
                ->orderBy('name')
                ->paginate($this->entries),
        ]);
    }

    /**
     * Runs before a property called $search is updated
     *
     * @var array $search
     *
     * @return void
     */
    public function updatingSearch()
    {
        $this->gotoPage(1);
    }

    /**
     * Runs after any update to the Livewire component's data 
     * (Using wire:model, not directly inside PHP)
     *
     * @param string $propertyName
     * @return void
     */
    public function updated($propertyName)
    {
        $this->validateOnly($propertyName);
    }

    /**
     * Resets all the form inputs
     *
     * @return void
     */
    public function resetInputs()
    {
        $this->resetValidation();
        $this->asset = new AppAsset();
    }

    /**
     * Create a new Asset and ShowModal
     */
    public function addNew()
    {
        $this->resetInputs();
        $this->showModal();
    }

    /**
     * Save new $asset object or updates an existing one
     * @var object $asset
     *
     * @return void
     */
    public function save()
    {
        $this->validate();

        try {
            //Create or update asset
            $this->asset->save();

            $this->dispatchBrowserEvent('assetStore');

            $this->resetInputs();

            session()->flash('status', trans('backend.crud.asset.messages.save'));
        } catch (\Illuminate\Database\QueryException $exception) {
            $errorInfo = $exception->errorInfo;

            session()->flash('error', $errorInfo[2]);
        }
    }

    /**
     * Gets an exististing $asset object to edit it
     * @var object $asset
     *
     * @param string $id
     * @return void
     */
    public function edit($id)
    {
        $this->resetInputs();

        $this->asset = AppAsset::findOrFail($id);

        $this->showModal();
    }

    /**
     * Changes the $confirmDeleteId value
     * @var string $confirmDeleteId
     *
     * @param string $id
     * @return void
     */
    public function confirmDelete($id)
    {
        $this->confirmDeleteId = $id;
    }

    /**
     * Deletes an existing $asset object
     * @var object $asset
     *
     * @return void
     */
    public function delete()
    {
        AppAsset::findOrFail($this->confirmDeleteId)->delete();
        $this->dispatchBrowserEvent('deleteItem');
        session()->flash('status', trans('backend.crud.asset.messages.delete'));
    }
}
