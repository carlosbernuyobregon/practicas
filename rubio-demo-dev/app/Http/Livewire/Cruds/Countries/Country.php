<?php

namespace App\Http\Livewire\Cruds\Countries;

use App\Country as AppCountry;
use App\Currency;
use App\Customer;
use Illuminate\Support\Facades\Lang;
use Illuminate\Support\Str;
use Livewire\Component;
use Livewire\WithPagination;

class Country extends Component
{
    use WithPagination;

    protected $paginationTheme = 'bootstrap';

    public ?AppCountry $country = null;

    public $confirmDeleteId;
    public $entries = 10;
    public $search = '';

    public $data = [];

    protected $rules = [
        'country.key_value' => 'required|unique:countries,key_value',
        'country.code'  => 'required|min:2|max:2',
        'country.currency_id' => 'required',
        'country.customer_id' => 'nullable',
        'data.en.name'  => 'required',
        'data.*.description' => 'nullable',
    ];

    protected $validationAttributes = [
        'data.en.name' => 'country',
        'country.key_value' => 'country',
        'country.currency_id' => 'currency',
    ];

    private function showModal()
    {
        $this->dispatchBrowserEvent('showModal');
    }

    /**
     * Renders the component view
     *
     * @return mixed
     */
    public function render()
    {
        return view('livewire.cruds.countries.country', [
            'countries' =>  AppCountry::where('key_value', 'like', '%' . $this->search . '%')
                ->orWhere('code', 'like', '%' . $this->search . '%')
                ->orderBy('key_value')
                ->paginate($this->entries),
            'currencies' =>  Currency::orderBy('name')->get(),
            'customers' => Customer::orderBy('name')->get(),
            'locales'   => config('translatable.locales'),
        ]);
    }

    /**
     * Runs before a property called $search is updated
     *
     * @var array $search
     *
     * @return void
     */
    public function updatingSearch()
    {
        $this->gotoPage(1);
    }

    /**
     * Runs after any update to the Livewire component's data 
     * (Using wire:model, not directly inside PHP)
     *
     * @param string $propertyName
     * @return void
     */
    public function updated($propertyName)
    {
        if (isset($this->country->id)) {
            $this->validateOnly($propertyName, [
                'country.key_value' => 'required|unique:countries,key_value,' . $this->country->id,
                'country.code'  => 'required|min:2|max:2',
                'country.currency_id' => 'required',
                'country.customer_id' => 'nullable',
                'data.en.name'  => 'required',
                'data.*.description' => 'nullable',
            ]);
        } else {
            $this->validateOnly($propertyName);
        }
    }

    /**
     * Resets all the form inputs
     *
     * @return void
     */
    public function resetInputs()
    {
        $this->resetValidation();
        $this->country = new AppCountry();
        foreach (config('translatable.locales') as $locale) {
            $this->data[$locale]['name'] = '';
            $this->data[$locale]['description'] = '';
        }
    }

    /**
     * Create a new Country and ShowModal
     */
    public function addNew()
    {
        $this->resetInputs();
        $this->showModal();
    }

    /**
     * Save new $country object or updates an existing one
     * @var object $country
     *
     * @return void
     */
    public function save()
    {
        if ($this->country->id == null) {
            $this->country->key_value = Str::slug($this->data['en']['name'], '-');
        }

        $this->country->code = Str::upper($this->country->code);

        if ($this->country->id != null) {
            $this->validate([
                'country.key_value' => 'required|unique:countries,key_value,' . $this->country->id,
                'country.code'  => 'required|min:2|max:2',
                'country.currency_id' => 'required',
                'country.customer_id' => 'nullable',
                'data.en.name'  => 'required',
                'data.*.description' => 'nullable',
            ]);
        } else {
            $this->validate();
        }

        foreach (config('translatable.locales') as $locale) {
            if (isset($this->data[$locale])) {
                $this->data[$locale]['name'] = Str::upper($this->data[$locale]['name'] ?? '');
                $this->data[$locale]['description'] = Str::ucfirst($this->data[$locale]['description'] ?? '');
                $this->country->translateOrNew($locale)->name = $this->data[$locale]['name'];
                $this->country->translateOrNew($locale)->description = $this->data[$locale]['description'];
            }
        }

        // Save Country
        try {
            $this->country->save();

            $this->dispatchBrowserEvent('countryStore');

            $this->resetInputs();

            session()->flash('status', trans('backend.crud.country.messages.save'));
        } catch (\Illuminate\Database\QueryException $exception) {
            $errorInfo = $exception->errorInfo;
            session()->flash('error', $errorInfo[2]);
        }
    }

    /**
     * Gets an exististing $country object to edit it
     * @var object $country
     *
     * @param string $id
     * @return void
     */
    public function edit($id)
    {
        $this->resetInputs();
        $this->country = AppCountry::findOrFail($id);
        $translations = $this->country->getTranslationsArray();

        foreach (config('translatable.locales') as $locale) {
            if (isset($translations[$locale])) {
                $this->data[$locale]['name'] = $translations[$locale]['name'];
                $this->data[$locale]['description'] = $translations[$locale]['description'];
            }
        }
        $this->showModal();
    }

    /**
     * Changes the $confirmDeleteId value
     * @var string $confirmDeleteId
     *
     * @param string $id
     * @return void
     */
    public function confirmDelete($id)
    {
        $this->confirmDeleteId = $id;
    }

    /**
     * Deletes an existing $country object
     * @var object $country
     *
     * @return void
     */
    public function delete()
    {
        AppCountry::findOrFail($this->confirmDeleteId)->delete();
        $this->dispatchBrowserEvent('deleteItem');
        session()->flash('status', trans('backend.crud.country.messages.delete'));
    }
}
