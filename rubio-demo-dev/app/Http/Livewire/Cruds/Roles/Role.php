<?php

namespace App\Http\Livewire\Cruds\Roles;

use App\Customer;
use App\Role as AppRole;
use Illuminate\Support\Facades\Lang;
use Illuminate\Support\Str;
use Livewire\Component;
use Livewire\WithPagination;

class Role extends Component
{
    use WithPagination;

    protected $paginationTheme = 'bootstrap';

    public ?AppRole $role;

    public $confirmDeleteId;
    public $entries = 10;
    public $search = '';

    public $data = [];

    protected $rules = [
        'role.key_value' => 'required|unique:roles,key_value',
        'role.customer_id' => 'nullable',
        'data.en.name'  => 'required',
        'data.*.description' => 'nullable'
    ];

    protected $validationAttributes = [
        'data.en.name' => 'role name',
        'role.key_value' => 'role',
        'role.customer_id' => 'customer',
    ];
    
    private function showModal()
    {
        $this->dispatchBrowserEvent('showModal');
    }

    /**
     * Renders the component view
     *
     * @return mixed
     */
    public function render()
    {
        return view('livewire.cruds.roles.role', [
            'roles' => AppRole::where('key_value', 'like', '%' . $this->search . '%')
                ->orderBy('key_value')
                ->paginate($this->entries),
            'customers' => Customer::orderBy('name')->get(),
            'locales'   => config('translatable.locales')
        ]);
    }

    /**
     * Runs before a property called $search is updated
     *
     * @var array $search
     *
     * @return void
     */
    public function updatingSearch()
    {
        $this->gotoPage(1);
    }

    /**
     * Runs after any update to the Livewire component's data 
     * (Using wire:model, not directly inside PHP)
     *
     * @param string $propertyName
     * @return void
     */
    public function updated($propertyName)
    {
        if (isset($this->role->id)) {
            $this->validateOnly($propertyName, [
                'role.key_value' => 'required|unique:roles,key_value,' . $this->role->id,
                'role.customer_id' => 'nullable',
                'data.en.name'  => 'required',
                'data.*.description' => 'nullable'
            ]);
        } else {
            $this->validateOnly($propertyName);
        }
        
    }

    /**
     * Resets all the form inputs
     *
     * @return void
     */
    public function resetInputs()
    {
        $this->resetValidation();
        $this->role = new AppRole();
        foreach (config('translatable.locales') as $locale) {
            $this->data[$locale]['name'] = '';
            $this->data[$locale]['description'] = '';
        }
    }

    /**
     * Create a new Role and ShowModal
     */
    public function addNew()
    {
        $this->resetInputs();
        $this->showModal();
    }

    /**
     * Save new $role object or updates an existing one
     * @var object $role
     *
     * @return void
     */
    public function save()
    {
        if ($this->role->id == null) {
            $this->role->key_value = Str::slug($this->data['en']['name'], '-');
        }

        if ($this->role->id != null) {
            $this->validate([
                'role.key_value' => 'required|unique:roles,key_value,' . $this->role->id,
                'role.customer_id' => 'nullable',
                'data.en.name'  => 'required',
                'data.*.description' => 'nullable'
            ]);
        } else {
            $this->validate();
        }

        foreach (config('translatable.locales') as $locale) {
            if (isset($this->data[$locale])) {
                $this->data[$locale]['name'] = Str::ucfirst($this->data[$locale]['name'] ?? '');
                $this->data[$locale]['description'] = Str::ucfirst($this->data[$locale]['description'] ?? '');
                $this->role->translateOrNew($locale)->name = $this->data[$locale]['name'];
                $this->role->translateOrNew($locale)->description = $this->data[$locale]['description'];
            }
        }
        //Save Role
        try {
            $this->role->save();

            $this->dispatchBrowserEvent('roleStore');

            $this->resetInputs();

            session()->flash('status', trans('backend.crud.role.messages.save'));
        } catch (\Illuminate\Database\QueryException $exception) {
            $errorInfo = $exception->errorInfo;

            session()->flash('error', $errorInfo[2]);
        }
    }

    /**
     * Gets an exististing $role object to edit it
     * @var object $role
     *
     * @param string $id
     * @return void
     */
    public function edit($id)
    {
        $this->resetInputs();
        $this->role = AppRole::findOrFail($id);
        $translations = $this->role->getTranslationsArray();

        foreach (config('translatable.locales') as $locale) {
            if (isset($translations[$locale])) {
                $this->data[$locale]['name'] = $translations[$locale]['name'];
                $this->data[$locale]['description'] = $translations[$locale]['description'];
            }
        }

        $this->showModal();
    }

    /**
     * Changes the $confirmDeleteId value
     * @var string $confirmDeleteId
     *
     * @param string $id
     * @return void
     */
    public function confirmDelete($id)
    {
        $this->confirmDeleteId = $id;
    }

    /**
     * Deletes an existing $role object
     * @var object $role
     *
     * @return void
     */
    public function delete()
    {
        AppRole::findOrFail($this->confirmDeleteId)->delete();
        $this->dispatchBrowserEvent('deleteItem');
        session()->flash('status', trans('backend.crud.role.messages.delete'));
    }
}
