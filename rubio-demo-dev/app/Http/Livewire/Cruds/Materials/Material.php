<?php

namespace App\Http\Livewire\Cruds\Materials;

use App\Customer;
use App\Material as AppMaterial;
use Livewire\Component;
use Livewire\WithPagination;

class Material extends Component
{
    use WithPagination;

    protected $paginationTheme = 'bootstrap';

    public ?AppMaterial $material = null;

    public $confirmDeleteId;
    public $entries = 10;
    public $search = '';

    protected $rules = [
        'material.external_id' => 'nullable',
        'material.customer_id' => 'nullable',
        'material.material_name' => 'string|required',
        'material.company' => 'string|required',
        'material.type' => 'string|required',
    ];

    protected $validationAttributes = [
        'material.material_name' => 'material name',
        'material.company' => 'company',
        'material.type' => 'type',
    ];

    private function showModal()
    {
        $this->dispatchBrowserEvent('showModal');
    }

    public function render()
    {
        return view('livewire.cruds.materials.material', [
            'customers' => Customer::orderBy('name')->get(),
            'materials' =>  AppMaterial::where('material_name', 'like', '%' . $this->search . '%')
                ->orWhere('company', 'like', '%' . $this->search . '%')
                ->orWhere('type', 'like', '%' . $this->search . '%')
                ->orderBy('material_name')
                ->paginate($this->entries),
        ]);
    }

    /**
     * Runs before a property called $search is updated
     *
     * @var array $search
     *
     * @return void
     */
    public function updatingSearch()
    {
        $this->gotoPage(1);
    }

    /**
     * Runs after any update to the Livewire component's data 
     * (Using wire:model, not directly inside PHP)
     *
     * @param string $propertyName
     * @return void
     */
    public function updated($propertyName)
    {
        $this->validateOnly($propertyName);
    }

    /**
     * Resets all the form inputs
     *
     * @return void
     */
    public function resetInputs()
    {
        $this->resetValidation();
        $this->material = new AppMaterial();
    }

    /**
     * Create a new Material and ShowModal
     */
    public function addNew()
    {
        $this->resetInputs();
        $this->showModal();
    }

    /**
     * Save new $material object or updates an existing one
     * @var object $material
     *
     * @return void
     */
    public function save()
    {
        $this->validate();

        try {
            //Create or update material
            $this->material->save();

            $this->dispatchBrowserEvent('materialStore');

            $this->resetInputs();

            session()->flash('status', trans('backend.crud.material.messages.save'));
        } catch (\Illuminate\Database\QueryException $exception) {
            $errorInfo = $exception->errorInfo;

            session()->flash('error', $errorInfo[2]);
        }
    }

    /**
     * Gets an exististing $material object to edit it
     * @var object $material
     *
     * @param string $id
     * @return void
     */
    public function edit($id)
    {
        $this->resetInputs();

        $this->material = AppMaterial::findOrFail($id);

        $this->showModal();
    }

    /**
     * Changes the $confirmDeleteId value
     * @var string $confirmDeleteId
     *
     * @param string $id
     * @return void
     */
    public function confirmDelete($id)
    {
        $this->confirmDeleteId = $id;
    }

    /**
     * Deletes an existing $material object
     * @var object $material
     *
     * @return void
     */
    public function delete()
    {
        AppMaterial::findOrFail($this->confirmDeleteId)->delete();
        $this->dispatchBrowserEvent('deleteItem');
        session()->flash('status', trans('backend.crud.material.messages.delete'));
    }
}
