<?php

namespace App\Http\Livewire\Cruds\Currencies;

use App\Currency as AppCurrency;
use App\Customer;
use Illuminate\Support\Facades\Lang;
use Illuminate\Support\Str;
use Livewire\Component;
use Livewire\WithPagination;

class Currency extends Component
{
    use WithPagination;

    protected $paginationTheme = 'bootstrap';

    public ?AppCurrency $currency = null;
    
    public $confirmDeleteId;
    public $entries = 10;
    public $search = '';

    protected $rules = [
        'currency.key_value' => 'required|unique:currencies,key_value',
        'currency.customer_id' => 'nullable',
        'currency.code' => 'required|min:3|max:3',
        'currency.name' => 'required',
    ];

    protected $validationAttributes = [
        'currency.key_value' => 'currency',
    ];

    private function showModal()
    {
        $this->dispatchBrowserEvent('showModal');
    }

    /**
     * Renders the component view
     *
     * @return mixed
     */
    public function render()
    {
        return view('livewire.cruds.currencies.currency', [
            'currencies' =>  AppCurrency::Where('code', 'like', '%' . $this->search . '%')
                ->orWhere('name', 'like', '%' . $this->search . '%')
                ->orderBy('name')
                ->paginate($this->entries),
            'customers' => Customer::orderBy('name')->get(),
        ]);
    }

    /**
     * Runs before a property called $search is updated
     * 
     * @var array $search
     *
     * @return void
     */
    public function updatingSearch()
    {
        $this->gotoPage(1);
    }

    /**
     * Runs after any update to the Livewire component's data 
     * (Using wire:model, not directly inside PHP)
     *
     * @param string $propertyName
     * @return void
     */
    public function updated($propertyName)
    {
        if (isset($this->currency->id)) {
            $this->validateOnly($propertyName, [
                'currency.key_value' => 'required|unique:currencies,key_value,' . $this->currency->id,
                'currency.customer_id' => 'nullable',
                'currency.code' => 'required|min:3|max:3',
                'currency.name' => 'required'
            ]);
        } else {
            $this->validateOnly($propertyName);
        }
        
    }

    /**
     * Resets all the form inputs
     *
     * @return void
     */
    public function resetInputs()
    {
        $this->resetValidation();
        $this->currency = new AppCurrency();
    }

    /**
     * Create a new Currency and ShowModal
     */
    public function addNew()
    {
        $this->resetInputs();
        $this->showModal();
    }

    /**
     * Save new $currency object or updates an existing one
     * @var object $currency
     *
     * @return void
     */
    public function save()
    {
        if ($this->currency->id == null) {
            $this->currency->key_value = Str::slug($this->currency->code, '-');
        }

        $this->currency->code = Str::upper($this->currency->code);
        $this->currency->name = Str::ucfirst($this->currency->name);

        if ($this->currency->id != null) {
            $this->validate([
                'currency.key_value' => 'required|unique:currencies,key_value,' . $this->currency->id,
                'currency.customer_id' => 'nullable',
                'currency.code' => 'required|min:3|max:3',
                'currency.name' => 'required'
            ]);
        } else {
            $this->validate();
        }

        try {
            $this->currency->save();

            $this->dispatchBrowserEvent('currencyStore');

            $this->resetInputs();

            session()->flash('status', trans('backend.crud.currency.messages.save'));
        } catch (\Illuminate\Database\QueryException $exception) {
            $errorInfo = $exception->errorInfo;

            session()->flash('error', $errorInfo[2]);
        }
    }

    /**
     * Gets an exististing $currency object to edit it
     * @var object $currency
     *
     * @param string $id
     * @return void
     */
    public function edit($id)
    {
        $this->resetInputs();
        $this->currency = AppCurrency::findOrFail($id);
        $this->showModal();
    }

    /**
     * Changes the $confirmDeleteId value
     * @var string $confirmDeleteId
     *
     * @param string $id
     * @return void
     */
    public function confirmDelete($id)
    {
        $this->confirmDeleteId = $id;
    }

    /**
     * Deletes an existing $currency object
     * @var object $currency
     *
     * @return void
     */
    public function delete()
    {
        AppCurrency::findOrFail($this->confirmDeleteId)->delete();
        $this->dispatchBrowserEvent('deleteItem');
        session()->flash('status', trans('backend.crud.currency.messages.delete'));
    }
}
