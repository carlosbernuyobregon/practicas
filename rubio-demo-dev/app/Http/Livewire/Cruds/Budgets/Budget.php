<?php

namespace App\Http\Livewire\Cruds\Budgets;

use App\AccountingAccount;
use App\Budget as AppBudget;
use App\CostCenter;
use App\Customer;
use App\Models\BudgetAgrupation;
use App\User;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Facades\Auth;
use Livewire\Component;
use Livewire\WithPagination;

class Budget extends Component
{
    use WithPagination;

    protected $paginationTheme = 'bootstrap';

    public $budget = null;
    public $costCenters = [];
    public $newCostCenterId = '';

    public $confirmDeleteId;
    public $entries = 10;
    public $search = '';
    public $accountingAccounts = [];

    protected $rules = [
        'budget.name' => 'string|required',
        'budget.budget_agrupation_id' => 'string|required',
        'budget.accounting_account_id' => 'string|required',
    ];

    protected $validationAttributes = [
        'budget.name' => 'name',
        'budget.company' => 'company',
    ];

    public function mount()
    {
        $this->accountingAccounts = AccountingAccount::where('customer_id',Auth::user()->customer_id)->orderBy('external_id','asc')->get();
        $this->budgetAgrupations = BudgetAgrupation::where('customer_id',Auth::user()->customer_id)->orderBy('name','asc')->get();
        $this->budget = new AppBudget;
    }
    private function showModal()
    {

        $this->dispatchBrowserEvent('showModal');
    }


    /**
     * Renders the component view
     *
     * @return mixed
     */
    public function render()
    {
        //dump(Auth::user()->customer_id);
        //dd($budgets);
        return view('livewire.cruds.budgets.budget', [
            'customers' => Customer::orderBy('name')->get(),
            'users' => User::where('customer_id',Auth::user()->customer_id)->get(),
            'budgets' =>  AppBudget::where('customer_id',Auth::user()->customer_id)
                ->where('name', 'like', '%' . $this->search . '%')
                ->with(['costCenters','budgetAgrupation'])
                ->orderBy('name')
                ->paginate($this->entries),
        ]);
    }

    /**
     * Runs before a property called $search is updated
     *
     * @var array $search
     *
     * @return void
     */
    public function updatingSearch()
    {
        $this->gotoPage(1);
    }

    /**
     * Runs after any update to the Livewire component's data
     * (Using wire:model, not directly inside PHP)
     *
     * @param string $propertyName
     * @return void
     */
    public function updated($propertyName)
    {
        $this->validateOnly($propertyName);
    }

    /**
     * Resets all the form inputs
     *
     * @return void
     */
    public function resetInputs()
    {
        $this->resetValidation();
        $this->budget = new AppBudget();
    }

    /**
     * Create a new CostCenter and ShowModal
     */
    public function addNew()
    {

        $this->resetInputs();
        $this->showModal();
    }

    /**
     * Save new $budget object or updates an existing one
     * @var object $budget
     *
     * @return void
     */
    public function save()
    {
        $this->validate();

        try {
            //Create or update costCenter
            $this->budget->customer_id = Auth::user()->customer_id;
            $this->budget->save();
            $this->dispatchBrowserEvent('budgetStore');

            $this->resetInputs();

            session()->flash('status', trans('backend.crud.budget.messages.save'));
        } catch (\Illuminate\Database\QueryException $exception) {
            $errorInfo = $exception->errorInfo;

            session()->flash('error', $errorInfo[2]);
        }
    }

    /**
     * Gets an exististing $budget object to edit it
     * @var object $budget
     *
     * @param string $id
     * @return void
     */
    public function edit($id)
    {
        $this->resetInputs();
        $this->budget = AppBudget::findOrFail($id);
        $this->costCenters = CostCenter::where('customer_id',Auth::user()->customer_id)
                                ->where('company',$this->budget->accountingAccount->company)->orderBy('external_id')->get();
        $this->showModal();
    }

    /**
     * Changes the $confirmDeleteId value
     * @var string $confirmDeleteId
     *
     * @param string $id
     * @return void
     */
    public function confirmDelete($id)
    {
        $this->confirmDeleteId = $id;
    }

    /**
     * Deletes an existing $budget object
     * @var object $budget
     *
     * @return void
     */
    public function delete()
    {
        AppBudget::findOrFail($this->confirmDeleteId)->delete();
        $this->dispatchBrowserEvent('deleteItem');
        session()->flash('status', trans('backend.crud.budget.messages.delete'));
    }

    public function addCostCenter()
    {
        if($this->newCostCenterId != ''){
            $this->budget->costCenters()->attach($this->newCostCenterId);
            $this->budget->refresh();
        }
    }
    public function removeCostCenter($id)
    {
        $this->budget->costCenters()->toggle($id);
        $this->budget->refresh();
    }
}
