<?php

namespace App\Http\Livewire\Cruds\AccountingAccounts;

use App\AccountingAccount as AppAccountingAccount;
use App\Customer;
use Livewire\Component;
use Livewire\WithPagination;

class AccountingAccount extends Component
{
    use WithPagination;

    protected $paginationTheme = 'bootstrap';

    public ?AppAccountingAccount $accountingAccount = null;

    public $confirmDeleteId;
    public $entries = 10;
    public $search = '';

    protected $rules = [
        'accountingAccount.external_id' => 'nullable',
        'accountingAccount.customer_id' => 'nullable',
        'accountingAccount.name' => 'string|required',
        'accountingAccount.company' => 'string|required',
    ];

    protected $validationAttributes = [
        'accountingAccount.name' => 'name',
        'accountingAccount.company' => 'company',
    ];

    private function showModal()
    {
        $this->dispatchBrowserEvent('showModal');
    }

    public function render()
    {
        return view('livewire.cruds.accounting-accounts.accounting-account', [
            'customers' => Customer::orderBy('name')->get(),
            'accountingAccounts' =>  AppAccountingAccount::where('name', 'like', '%' . $this->search . '%')
                ->orWhere('company', 'like', '%' . $this->search . '%')
                ->orderBy('name')
                ->paginate($this->entries),
        ]);
    }

    /**
     * Runs before a property called $search is updated
     *
     * @var array $search
     *
     * @return void
     */
    public function updatingSearch()
    {
        $this->gotoPage(1);
    }

    /**
     * Runs after any update to the Livewire component's data 
     * (Using wire:model, not directly inside PHP)
     *
     * @param string $propertyName
     * @return void
     */
    public function updated($propertyName)
    {
        $this->validateOnly($propertyName);
    }

    /**
     * Resets all the form inputs
     *
     * @return void
     */
    public function resetInputs()
    {
        $this->resetValidation();
        $this->accountingAccount = new AppAccountingAccount();
    }

    /**
     * Create a new Accounting Account and ShowModal
     */
    public function addNew()
    {
        $this->resetInputs();
        $this->showModal();
    }

    /**
     * Save new $accountingAccount object or updates an existing one
     * @var object $accountingAccount
     *
     * @return void
     */
    public function save()
    {
        $this->validate();

        try {
            //Create or update accountingAccount
            $this->accountingAccount->save();

            $this->dispatchBrowserEvent('accountingAccountStore');

            $this->resetInputs();

            session()->flash('status', trans('backend.crud.accounting-account.messages.save'));
        } catch (\Illuminate\Database\QueryException $exception) {
            $errorInfo = $exception->errorInfo;

            session()->flash('error', $errorInfo[2]);
        }
    }

    /**
     * Gets an exististing $accountingAccount object to edit it
     * @var object $accountingAccount
     *
     * @param string $id
     * @return void
     */
    public function edit($id)
    {
        $this->resetInputs();

        $this->accountingAccount = AppAccountingAccount::findOrFail($id);

        $this->showModal();
    }

    /**
     * Changes the $confirmDeleteId value
     * @var string $confirmDeleteId
     *
     * @param string $id
     * @return void
     */
    public function confirmDelete($id)
    {
        $this->confirmDeleteId = $id;
    }

    /**
     * Deletes an existing $accountingAccount object
     * @var object $accountingAccount
     *
     * @return void
     */
    public function delete()
    {
        AppAccountingAccount::findOrFail($this->confirmDeleteId)->delete();
        $this->dispatchBrowserEvent('deleteItem');
        session()->flash('status', trans('backend.crud.accounting-account.messages.delete'));
    }
}
