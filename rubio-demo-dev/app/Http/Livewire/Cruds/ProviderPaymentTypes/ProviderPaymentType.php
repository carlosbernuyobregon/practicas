<?php

namespace App\Http\Livewire\Cruds\ProviderPaymentTypes;

use Livewire\Component;
use Livewire\WithPagination;
use Rap2hpoutre\FastExcel\FastExcel;
use App\ProviderPaymentType as AppProviderPaymentType;

class ProviderPaymentType extends Component
{
    use WithPagination;

    protected $paginationTheme = 'bootstrap';

    public ?AppProviderPaymentType $providerPaymentType = null;

    public $confirmDeleteId = null;
    public $entries = 10;
    public $search = null;
    public $sortBy = 'created_at';
    public $sortDirection = 'desc';

    protected $rules = [
        'providerPaymentType.code' => 'required|min:2|max:2',
        'providerPaymentType.name' => 'required|max:255',
        'providerPaymentType.description' => 'nullable|max:255',
        'providerPaymentType.is_default' => 'nullable',
    ];

    /**
     * ask for show modal
     */
    private function showModal()
    {
        $this->dispatchBrowserEvent('showModal');
    }

    /**
     * Render of component view
     * 
     * return mixed
     */
    public function render()
    {
        $data = AppProviderPaymentType::select('*');
        if ($this->search) {
            $search = '%' . $this->search . '%';
            $data->where('name', 'like', $search)
                ->orWhere('code', 'like', $search)
                ->orWhere('description', 'like', $search)
                ->orWhereDate('created_at', date('Y-m-d', strtotime(str_replace('/', '-', $this->search))));
            if (strtoupper($this->search) == "YES" || strtoupper($this->search) == "SI") {
                $data->orWhere('is_default', 1);
            } elseif (strtoupper($this->search) == "NO") {
                $data->orWhere('is_default', 0);
            }
        }
        return view('livewire.cruds.provider-payment-types.provider-payment-type', [
            'providerPaymentTypes' => $data->orderBy($this->sortBy, $this->sortDirection)
                ->paginate($this->entries)

        ]);
    }

    /**
     * Runs before a property called $search is updated
     *
     * @var array $search
     *
     * @return void
     */
    public function updatingSearch()
    {
        $this->gotoPage(1);
    }

    /**
     * Runs after any update to the Livewire component's data 
     * (Using wire:model, not directly inside PHP)
     *
     * @param string $propertyName
     * @return void
     */
    public function updated($propertyName)
    {
        $this->validateOnly($propertyName);
    }

    /**
     * Resets all the form inputs
     *
     * @return void
     */
    public function resetInputs()
    {
        $this->resetValidation();
        $this->providerPaymentType = new AppProviderPaymentType();
    }

    /**
     * Create a new ProviderPaymentType and ShowModal
     */
    public function addNew()
    {
        $this->resetInputs();
        $this->showModal();
    }

    /**
     * Save new $providerPaymentType object or updates an existing one
     * @var object $providerPaymentType
     *
     * @return void
     */
    public function save()
    {
        $this->validate();

        try {
            //Create or update providerPaymentType
            $this->providerPaymentType->save();

            $this->dispatchBrowserEvent('providerPaymentTypeStore');

            $this->resetInputs();

            session()->flash('status', trans('backend.crud.provider-payment-type.messages.save'));
        } catch (\Illuminate\Database\QueryException $exception) {
            $errorInfo = $exception->errorInfo;

            session()->flash('error', $errorInfo[2]);
        }
    }

    /**
     * Gets an exististing $providerPaymentType object to edit it
     * @var object $providerPaymentType
     *
     * @param string $id
     * @return void
     */
    public function edit($id)
    {
        $this->resetInputs();

        $this->providerPaymentType = AppProviderPaymentType::findOrFail($id);

        $this->showModal();
    }


    /**
     * Changes the $confirmDeleteId value
     * @var string $confirmDeleteId
     *
     * @param string $id
     * @return void
     */
    public function confirmDelete($id)
    {
        $this->confirmDeleteId = $id;
    }

    /**
     * Deletes an existing $providerPaymentType object
     * @var object $providerPaymentType
     *
     * @return void
     */
    public function delete()
    {
        AppProviderPaymentType::findOrFail($this->confirmDeleteId)->delete();
        $this->dispatchBrowserEvent('deleteItem');
        session()->flash('status', trans('backend.crud.provider-payment-type.messages.delete'));
    }

    /**
     * Set the order by the vaue ofthe column
     * 
     * @var string $field
     */
    public function sortBy($field)
    {
        if ($this->sortBy == $field) {
            $this->sortDirection = $this->sortDirection === 'asc' ? 'desc' : 'asc';
        } else {
            $this->sortDirection = 'asc';
        }
        $this->sortBy = $field;
    }

    /**
     * Generates a file with xlsx (Excel) format
     *
     * @param $path
     * @return object
     * @throws \Box\Spout\Common\Exception\IOException
     * @throws \Box\Spout\Common\Exception\InvalidArgumentException
     * @throws \Box\Spout\Common\Exception\UnsupportedTypeException
     * @throws \Box\Spout\Writer\Exception\WriterNotOpenedException
     */
    public function exportToExel($path)
    {
        $data = AppProviderPaymentType::all();

        return (new FastExcel($data))->export($path, function ($providerPT) {
            $default = null;
            switch ($providerPT->is_default) {
                case '1':
                    $default = __('backend.crud.provider-payment-type.form.default.options.yes');
                    break;
                case '0':
                    $default = __('backend.crud.provider-payment-type.form.default.options.no');
                    break;
                default:
                    $default = '';
                    break;
            }
            return [
                __('backend.crud.created_at') => $providerPT->created_at != null ? auth()->user()
                    ->applyDateFormat($providerPT->created_at) : '',
                __('backend.crud.provider-payment-type.list.code') => $providerPT->code,
                __('backend.crud.provider-payment-type.list.name') => $providerPT->name,
                __('backend.crud.provider-payment-type.list.description') => $providerPT->description,
                __('backend.crud.provider-payment-type.list.default') => $default,
            ];
        });
    }

    /**
     * Downloads excel file generated in exportToExcel()
     *
     * @return mixed
     */
    public function downloadExcel()
    {
        $path = tempnam(sys_get_temp_dir(), "FOO");
        $this->exportToExel($path);
        return response()->download($path, 'TipoPagoProveedor.xlsx', [
            'Content-Type' => 'application/vnd.ms-excel',
            'Content-Disposition' => 'inline; filename="TipoPagoProveedor.xlsx"'
        ]);
    }
}
