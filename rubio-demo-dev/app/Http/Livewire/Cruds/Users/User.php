<?php

namespace App\Http\Livewire\Cruds\Users;

use App\Country;
use App\Customer;
use App\Role;
use App\CostCenter;
use App\User as AppUser;
use Livewire\Component;
use Livewire\WithPagination;

class User extends Component
{
    use WithPagination;

    protected $paginationTheme = 'bootstrap';

    public $user = null;

    public $password;
    public $password_confirmation;

    public $confirmDeleteId;
    public $search;
    public $entries = 10;

    public $userRoles = [];
    public $dateFormats = [];
    public $numberFormats = [];
    public $blocked = null;
    public $active = 1;
    public $countries;
    public $cost_centers_user = [];
    public $cost_center_value;
    public $default_cost_center;

    protected $rules = [
        'user.name' => 'required',
        'user.first_name' => 'required',
        'user.last_name' => 'required',
        'user.email' =>  'required|email|unique:users,email',
        'user.phone' =>  'nullable',
        'password'  => 'required|min:8|confirmed',
        'user.timezone'  => 'required',
        'user.customer_id' => 'nullable',
        'user.date_format' => 'required|string',
        'user.numbers_format' => 'required|string',
        'user.active' => 'required|string',
        'user.blocked' => 'required|string',
        'user.country_id' => 'nullable',
        'user.default_cost_center_id' => 'nullable',
    ];

    protected $validationAttributes = [
        'user.name' => 'name',
        'user.first_name' => 'first name',
        'user.last_name' => 'last name',
        'user.email' =>  'email',
        'user.timezone'  => 'timezone',
    ];


    private function showModal()
    {
        $this->dispatchBrowserEvent('showModal');
    }

    public function mount()
    {
        $this->dateFormats = config('custom.date-formats', ['dd/mm/yyyy' => 'd/m/Y']);
        $this->numberFormats = config('custom.number-formats', ['decimals']);
        $this->countries = Country::orderBy('key_value')->withTranslation()->get();
    }

    /**
     * Renders the component view
     *
     * @return mixed
     */
    public function render()
    {
        return view('livewire.cruds.users.user', [
            'users' => AppUser::where('name', 'like', '%' . $this->search . '%')
                ->orWhere('email', 'like', '%' . $this->search . '%')
                ->orWhere('phone', 'like', '%' . $this->search . '%')
                ->orWhere('created_at', 'like', '%' . $this->search . '%')
                ->orderBy('name')
                ->paginate($this->entries),
            'roles' => Role::orderBy('key_value')->get(),
            'customers' => Customer::orderBy('name')->get(),
            'cost_centers' => $this->loadCostCenterList(),
            'cost_centers_selected' => $this->loadUserCostCenters()

        ]);
    }

    public function updatedActive()
    {
        $this->user->active = ($this->active == 1) ? "1" : "0";
    }

    public function updatedBlocked()
    {
        $this->user->blocked = ($this->blocked == 1) ? "1" : "0";
    }

    /**
     * Runs before a property called $search is updated
     *
     * @var array $search
     *
     * @return void
     */
    public function updatingSearch()
    {
        $this->gotoPage(1);
    }

    /**
     * Runs after any update to the Livewire component's data
     * (Using wire:model, not directly inside PHP)
     *
     * @param string $propertyName
     * @return void
     */
    public function updated($propertyName)
    {
        if (isset($this->user->id)) {
            $this->validateOnly($propertyName, [
                'user.name' => 'required',
                'user.first_name' => 'required',
                'user.last_name' => 'required',
                'user.email' =>  'required|email|unique:users,email,' . $this->user->id,
                'user.phone' =>  'nullable',
                'user.timezone'  => 'required',
                'password'  => 'required|min:8|confirmed',
                'user.customer_id' => 'nullable',
                'user.date_format' => 'required|string',
                'user.numbers_format' => 'required|string',

            ]);
        } else {
            $this->validateOnly($propertyName);
        }
    }

    /**
     * Resets all the form inputs
     *
     * @return void
     */
    public function resetInputs()
    {
        $this->resetValidation();
        $this->user = new AppUser();
        $this->userRoles = null;
        $this->password = null;
        $this->password_confirmation = null;
        $this->active = 1;
        $this->blocked = null;
        $this->user->active = "1";
        $this->user->blocked = "0";
        $this->cost_centers_user = [];
        $this->user->default_cost_center_id = null;
    }

    /**
     * Create a new Role and ShowModal
     */
    public function addNew()
    {
        $this->resetInputs();
        $this->showModal();
    }

    /**
     * Save new $user object or updates an existing one
     * @var object $user
     *
     * @return void
     */
    public function save()
    {
        if ($this->user->id != null) {
            $this->validate([
                'user.name' => 'required',
                'user.first_name' => 'required',
                'user.last_name' => 'required',
                'user.email' =>  'required|email|unique:users,email,' . $this->user->id,
                'user.phone' =>  'nullable',
                'password' => 'nullable|min:8|confirmed',
                'user.timezone'  => 'required',
                'user.customer_id' => 'nullable',
                'user.date_format' => 'required|string',
                'user.numbers_format' => 'required|string',
                'user.active' => 'required|string',
                'user.blocked' => 'required|string',
                'user.country_id' => 'nullable',
                'user.default_cost_center_id' => 'nullable'
            ]);

            if ($this->password != null) {
                $this->user->password = bcrypt($this->password);
            }
        } else {
            $this->validate();
            $this->user->password = bcrypt($this->password);
        }

        try {
            //Create user
            $this->user->save();

            try {
                //Create relations
                $userRolesArray = array_map(function ($role) {
                    if ($role) {
                        return ['user_id' => $this->user->id];
                    }
                }, $this->userRoles);

                //Deletes null values
                $userRolesArrayFiltered = array_filter($userRolesArray, function ($index) {
                    if ($index != null) {
                        return $index;
                    }
                });

                $this->user->roles()->sync($userRolesArrayFiltered);

                //User Cost Centers relation
                $this->user->costCenters()->sync($this->cost_centers_user);


                $this->dispatchBrowserEvent('userStore');

                $this->resetInputs();

                session()->flash('status', trans('backend.crud.user.messages.save'));
            } catch (\Illuminate\Database\QueryException $exception) {
                $errorInfo = $exception->errorInfo;

                session()->flash('error', $errorInfo[2]);
            }
        } catch (\Illuminate\Database\QueryException $exception) {
            $errorInfo = $exception->errorInfo;

            session()->flash('error', $errorInfo[2]);
        }
    }

    /**
     * Gets an exististing $user object to edit it
     * @var object $user
     *
     * @param string $id
     * @return void
     */
    public function edit($id)
    {
        $this->resetInputs();

        $this->user = AppUser::findOrFail($id);
        $this->active = ($this->user->active == "1") ? 1 : null;
        $this->blocked = ($this->user->blocked == "1") ? 1 : null;
        $this->default_cost_center = $this->user->default_cost_center_id;

        //Fill actual user roles
        foreach ($this->user->roles as $role) {
            $this->userRoles[$role->id] = true;
        }

        foreach ($this->user->costCenters as $cost_center) {
            array_push($this->cost_centers_user, $cost_center->id);
        }

        $this->showModal();
    }

    /**
     * Changes the $confirmDeleteId value
     * @var string $confirmDeleteId
     *
     * @param string $id
     * @return void
     */
    public function confirmDelete($id)
    {
        $this->confirmDeleteId = $id;
    }

    /**
     * Deletes an existing $user object
     * @var object $user
     *
     * @return void
     */
    public function delete()
    {
        AppUser::findOrFail($this->confirmDeleteId)->delete();
        $this->dispatchBrowserEvent('deleteItem');
        session()->flash('status', trans('backend.crud.user.messages.delete'));
    }

    public function onSelectCostCenter($value)
    {
        if (count($this->cost_centers_user) == 0)
            $this->user->default_cost_center_id = $value;

        array_push($this->cost_centers_user, $value);
    }

    public function deleteSelectedCostCenter($cost_center_id)
    {
        $id = array_search($cost_center_id, $this->cost_centers_user, true);
        unset($this->cost_centers_user[$id]);
        if ($this->default_cost_center == $cost_center_id) {
            if (count($this->cost_centers_user) > 0)
                $this->default_cost_center = $this->cost_centers_user[array_key_first($this->cost_centers_user)];
            else
                $this->default_cost_center = null;
        }

        $this->user->default_cost_center_id = $this->default_cost_center;
    }

    public function loadCostCenterList()
    {
        $this->cost_center_value = "";
        if (count($this->cost_centers_user) > 0)
            return CostCenter::whereNotIn('id', $this->cost_centers_user)->get();
        else
            return CostCenter::all();
    }

    public function loadUserCostCenters()
    {
        return CostCenter::whereIn('id', $this->cost_centers_user)->orderBy('external_id', 'ASC')->get();
    }

    public function setDefaultCostCenter($cost_center_id)
    {
        $this->default_cost_center = $cost_center_id;
        $this->user->default_cost_center_id = $this->default_cost_center;
    }
}
