<?php

namespace App\Http\Livewire\Cruds\Users;

use App\Customer;
use App\Role;
use App\User  as AppUser;
use App\CostCenter;

use App\Country;

use Livewire\Component;


class Profile extends Component
{
    public ?AppUser $user;

    public $password;
    public $password_confirmation;

    public $userRoles = [];
    public $dateFormats = [];
    public $numberFormats = [];
    public $countries;

    protected $rules = [
        'user.name' => 'required',
        'user.first_name' => 'required',
        'user.last_name' => 'required',
        'user.email' =>  'required|email|unique:users,email',
        'user.phone' =>  'nullable',
        'password'  => 'required|min:8|confirmed',
        'user.timezone'  => 'required',
        'user.customer_id' => 'nullable',
        'user.date_format' => 'required|string',
        'user.numbers_format' => 'required|string',
        'user.country_id' => 'nullable',
    ];

    protected $validationAttributes = [
        'user.name' => 'name',
        'user.first_name' => 'first name',
        'user.last_name' => 'last name',
        'user.email' =>  'email',
        'user.timezone'  => 'timezone',
    ];


    /**
     * Runs after any update to the Livewire component's data
     *
     * @param string $propertyName
     * @return void
     */
    public function updated($propertyName)
    {
        $this->validateOnly($propertyName);
    }

    /**
     * Updates an existing one
     * @var object $user
     *
     * @return void
     */
    public function save()
    {
        $this->validate([
            'user.name' => 'required',
            'user.first_name' => 'required',
            'user.last_name' => 'required',
            'user.email' =>  'required|email|unique:users,email,' . $this->user->id,
            'user.phone' =>  'nullable',
            'password' => 'nullable|min:8|confirmed',
            'user.timezone'  => 'required',
            'user.customer_id' => 'nullable',
            'user.date_format' => 'required|string',
            'user.numbers_format' => 'required|string',
            'user.country_id' => 'nullable',
        ]);

        if ($this->password != null) {
            $this->user->password = bcrypt($this->password);
        }

        try {
            $this->user->save();

            session()->flash('status', trans('backend.crud.user.messages.save'));
        } catch (\Illuminate\Database\QueryException $exception) {
            $errorInfo = $exception->errorInfo;
            session()->flash('error', $errorInfo[2]);
        }
    }

    public function mount($userId)
    {
        $this->dateFormats = config('custom.date-formats', ['dd/mm/yyyy' => 'd/m/Y']);
        $this->numberFormats = config('custom.number-formats', ['decimals']);
        $this->countries = Country::orderBy('key_value')->withTranslation()->get();
        $this->user = AppUser::findOrFail($userId);

        foreach ($this->user->roles as $role) {
            $this->userRoles[$role->id] = true;
        }
    }

    public function render()
    {
        return view('livewire.cruds.users.profile', [
            'customers' => Customer::all(),
            'roles' => Role::all(),
        ]);
    }
}
