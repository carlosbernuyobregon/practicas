<?php

namespace App\Http\Livewire\Cruds\Suppliers;

use App\Company;
use App\Country;
use App\Helpers\Helpers;
use App\Models\PaPerson;
use App\Models\Provider;
use App\Models\ProviderPaymentCondition;
use App\Models\ProviderPaymentType;
use App\Process;
use App\Rules\ProviderVat;
use App\User;
use Livewire\Component;

class Edit extends Component
{
    public $providers = [];
    public $providerPaymentTypes;
    public $providerPaymentTerms;
    public $providerPaymentTypeRequiringFiles = [1,7];
    public $companies = [];
    public $countries = [];
    public $mediators;
    public $editable = true;
    public $uploadFiles = false;
    public $showRgpd = false;
    public $provider = null;
    public $processInstance = null;
    public $actions = [];
    protected $validationAttributes = [
        'provider.rgpd_name'            => 'nombre (rgpd)',
        'provider.rgpd_last_name'       => 'apellidos (rgpd)',
        'provider.rgpd_dni'             => 'DNI (rgpd)',
        'provider.rgpd_email'           => 'email (rgpd)',
        'provider.rgpd_role'            => 'cargo / posición (rgpd)'
    ];
    public function rules()
    {
        $nullableOrRequiredRgpd = 'nullable|';

        if ($this->showRgpd) {
            $nullableOrRequiredRgpd = 'required|';
        }

        return [
            'order.company'                 => 'string',
            'order.purchasing_group'        => 'string',
            'order.buyer'                   => 'string',
            'order.date'                    => 'date',
            'order.deny_reason'             => 'string',
            'order.delivery_point'          => 'string',
            'order.provider_id'             => 'string|required',
            'order.description'             => 'string',
            'order.comments'                => 'nullable|string',
            'provider.company'              => 'string|required',
            'provider.name'                 => 'string|required',
            'provider.vat_number'           => 'string|required',
            'provider.payment_condition'    => 'string|required',
            'provider.payment_type'         => 'string|required',
            'provider.address'              => 'string|required',
            'provider.city'                 => 'string|required',
            'provider.country'              => 'string|required',
            'provider.zip_code'             => 'string|required',
            'provider.phone_number'         => 'numeric|required|digits_between:6,20',
            'provider.mediator'             => 'string|required',
            'provider.receiving_order_copy' => 'string|required',
            'provider.email'                => 'nullable',
            'provider.email_orders'         => 'nullable',
            'provider.iban'                 => 'nullable|regex:/^([a-zA-Z]{2}[0-9]{22,22})$/',
            'provider.rgpd_name'            => $nullableOrRequiredRgpd . 'string|max:50',
            'provider.rgpd_last_name'       => $nullableOrRequiredRgpd . 'string|max:50',
            'provider.rgpd_dni'             => $nullableOrRequiredRgpd . 'string|max:15',
            'provider.rgpd_email'           => $nullableOrRequiredRgpd . 'email',
            'provider.rgpd_role'            => $nullableOrRequiredRgpd . 'string|max:50',
            'order.invoice_number'          => 'nullable|string',
            'order.invoice_total_net'       => 'nullable|numeric',
            'order.invoice_date'            => 'nullable|date',
        ];
    }
    public function mount($provider, $editable, $task, $actions, $processInstance)
    {
        $this->companies            = Company::all();
        $this->countries            = Country::orderByTranslation('name')->withTranslation()->get();
        $this->provider             = $provider;
        $this->providerPaymentTypes = ProviderPaymentType::all();
        $this->providerPaymentTerms = ProviderPaymentCondition::all();
        $this->mediators            = PaPerson::orderBy('name')->get();
        $this->actions              = $actions;
        $this->processInstance      = $processInstance;
        $this->editable             = $editable;
    }
    public function render()
    {
        return view('livewire.cruds.suppliers.edit');
    }
    public function saveProvider() : void
    {
        // Form inputs validation
        $this->validateProviderForm();

        list($newVat, $newCommunityVat) = Helpers::VatFormat($this->provider->vat_number, $this->provider->country);

        // Add normalized VATs
        $this->provider->vat_number             = $newVat;
        $this->provider->community_vat_number   = $newCommunityVat;
        // Add creator
        $this->provider->creator_user_id        = auth()->id();
        // RGPD
        $this->provider->rgpd_user_id           = ($this->showRgpd) ? auth()->user()->id : null;

        if (!$this->showRgpd) {
            // Clean RGPD input data
            $this->provider->rgpd_name      = null;
            $this->provider->rgpd_last_name = null;
            $this->provider->rgpd_dni       = null;
            $this->provider->rgpd_email     = null;
            $this->provider->rgpd_role      = null;
            $this->provider->rgpd_user_id   = null;
        }

        // Save Provider
        $this->provider->save();


        session()->flash('success', 'Los datos se han guardado correctamente');
    }

    public function providerFilesUpdated()
    {
        if ($this->provider) {
            try {
                $this->provider->fresh();
            } catch (\Throwable $th) {
                // not provider selected
            }
        }
    }
    public function initNewProviderWorkflow($id)
    {
        // Form inputs validation
        $this->validateProviderForm();
        $process = new Process();
        $dataFileds = [
            'variables' => [
                'portalInstance' => [
                    'value' => config('app.url'),
                    'type' => 'string'
                ],
                'processId' => [
                    'value' => $id,
                    'type' => 'string'
                ],
                'varAssignedUser' => [
                    'value' => (string)User::where('email', config('custom.provider-process-validator-email'))->value('id'),
                    'type' => 'string'
                ]

            ]
           //  'businessKey' => "Rubio"
          // quitado BusinessKey para no sobreescribir
        ];

        // $this->provider = new Provider(); // This cause provider data name losing once it was created and document uplaoded
        $this->providerForm = false;
        try {
            $value = $process->submitProcess('alta_proveedores',$dataFileds);
            session()->flash('success', 'Proceso de alta de proveedor iniciado.');
        } catch (\Throwable $th) {
            session()->flash('success', 'No se ha podido iniciar el proceso de alta de proveedor.');
        }
    }
    public function validateProviderForm()
    {
        // Form inputs validation
        $nullableOrRequiredRgpd = 'nullable|';

        if ($this->showRgpd) {
            $nullableOrRequiredRgpd = 'required|';
        }

        $this->validate([
            'provider.company'              => 'string|required',
            'provider.name'                 => 'string|required',
            'provider.vat_number'           => [
                'required', 'string', 'between:6,12', 'regex:/(^[A-Za-z0-9]+$)+/', new ProviderVat(
                    $this->provider->company, $this->provider->id
                )
            ],
            'provider.payment_condition'    => 'string|required',
            'provider.payment_type'         => 'string|required',
            'provider.address'              => 'string|required',
            'provider.city'                 => 'string|required',
            'provider.country'              => 'string|required',
            'provider.zip_code'             => 'string|required',
            'provider.phone_number'         => 'numeric|required|digits_between:6,20',
            'provider.mediator'             => 'string|required',
            'provider.receiving_order_copy' => 'string|required',
            'provider.iban'                 => 'nullable|regex:/^([a-zA-Z]{2}[0-9]{22,22})$/',
            'provider.rgpd_name'            => $nullableOrRequiredRgpd . 'string|max:50',
            'provider.rgpd_last_name'       => $nullableOrRequiredRgpd . 'string|max:50',
            'provider.rgpd_dni'             => $nullableOrRequiredRgpd . 'string|max:15',
            'provider.rgpd_email'           => $nullableOrRequiredRgpd . 'email',
            'provider.rgpd_role'            => $nullableOrRequiredRgpd . 'string|max:50',
        ]);
    }

    public function providerDefaults()
    {
        $defaultType                        = $this->providerPaymentTypes->where('is_default')->first();
        $defaultTypeCondition               = $this->providerPaymentTerms->where('is_default')->first();
        $this->provider->payment_type       = $defaultType->code ?? false;
        $this->provider->payment_condition  = $defaultTypeCondition->code ?? false;
        $this->provider->country            = 'ES';
//        $this->provider->mediator           = auth()->user()->id;
    }
    public function rejectProvider()
    {
        if($this->provider->id != null){
            $this->provider->delete();
        }
        return redirect()->route('order.dashboard');
    }
}
