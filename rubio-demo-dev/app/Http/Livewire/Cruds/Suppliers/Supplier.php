<?php

namespace App\Http\Livewire\Cruds\Suppliers;

use App\Country;
use App\Customer;
use App\Models\PaPerson;
use App\Models\Provider;
use App\Models\ProviderPaymentCondition;
use App\Models\ProviderPaymentType;
use App\Rules\ProviderVat;
use Livewire\Component;
use Livewire\WithPagination;
use Illuminate\Support\Facades\Auth;

class Supplier extends Component
{
    use WithPagination;

    protected $paginationTheme = 'bootstrap';

    public ?Provider $supplier = null;

    public $confirmDeleteId;
    public $entries = 10;
    public $search = '';
    public $countries;
    public $showRgpd = false;
    public $providerPaymentTypes;
    public $providerPaymentTerms;
    public $mediators;

    protected $validationAttributes = [
        'supplier.name'                     => 'name',
        'supplier.vat_number'               => 'vat number',
        'supplier.community_vat_number'     => 'community vat number',
        'supplier.company'                  => 'company',
        'supplier.payment_condition'        => 'payment condition',
        'supplier.payment_type'             => 'payment type',
        'supplier.country'                  => 'country',
        'supplier.zip_code'                 => 'zip code',
        'supplier.city'                     => 'city',
        'supplier.address'                  => 'address',
        'supplier.mediator'                 => 'string|required',
        'supplier.receiving_order_copy'     => 'nullable',
        'supplier.iban'                     => 'IBAN',
        'supplier.rgpd_name'                => 'name (rgpd)',
        'supplier.rgpd_last_name'           => 'last name (rgpd)',
        'supplier.rgpd_dni'                 => 'DNI (rgpd)',
        'supplier.rgpd_email'               => 'email (rgpd)',
        'supplier.rgpd_role'                => 'role (rgpd)',
        'supplier.rgpd_service_description' => 'servicio a prestar (rgpd)'
    ];

    public function mount()
    {
        $this->countries            = Country::orderBy('key_value')->withTranslation()->get();
        $this->providerPaymentTypes = ProviderPaymentType::all();
        $this->providerPaymentTerms = ProviderPaymentCondition::all();
        $this->mediators            = PaPerson::orderBy('name')->get();
    }

    public function rules()
    {
        $nullableOrRequired = 'nullable|';

        if ($this->showRgpd) {
            $nullableOrRequired = 'required|';
        }

        return [
            'supplier.external_id'              => 'nullable',
            'supplier.customer_id'              => 'nullable',
            'supplier.vat_number'               => 'string|required|between:6,12|regex:/(^[A-Za-z0-9]+$)+/',
            /*'supplier.vat_number'           => [
                'required', 'string', 'between:6,12', 'regex:/(^[A-Za-z0-9]+$)+/', new ProviderVat($this->supplier->company ?? null)
            ],*/
            'supplier.community_vat_number'     => 'string|required',
            'supplier.name'                     => 'string|required',
            'supplier.company'                  => 'string|required',
            'supplier.payment_condition'        => 'string|required',
            'supplier.payment_type'             => 'string|required',
            'supplier.country'                  => 'string|required',
            'supplier.zip_code'                 => 'string|required',
            'supplier.city'                     => 'string|required',
            'supplier.address'                  => 'string|required',
            'supplier.mediator'                 => 'string|required',
            'supplier.receiving_order_copy'     => 'nullable',
            'supplier.email'                    => 'nullable',
            'supplier.email_orders'             => 'nullable',
            'supplier.iban'                     => 'nullable|regex:/^([a-zA-Z]{2}[0-9]{22,22})$/',
            'supplier.rgpd_name'                => $nullableOrRequired . 'string|max:50',
            'supplier.rgpd_last_name'           => $nullableOrRequired . 'string|max:50',
            'supplier.rgpd_dni'                 => $nullableOrRequired . 'string|max:15',
            'supplier.rgpd_email'               => $nullableOrRequired . 'email',
            'supplier.rgpd_role'                => $nullableOrRequired . 'string|max:50',
            'supplier.rgpd_service_description' => $nullableOrRequired . 'string',
        ];
    }

    private function showModal()
    {
        $this->dispatchBrowserEvent('showModal');
    }

    public function render()
    {
        return view('livewire.cruds.suppliers.supplier', [
            'customers' => Customer::orderBy('name')->get(),
            'suppliers' =>  Provider::where('name', 'like', '%' . $this->search . '%')
                ->orWhere('company', 'like', '%' . $this->search . '%')
                ->orWhere('payment_condition', 'like', '%' . $this->search . '%')
                ->orWhere('payment_type', 'like', '%' . $this->search . '%')
                ->orWhere('vat_number', 'like', '%' . $this->search . '%')
                ->orderBy('name')
                ->paginate($this->entries),
        ]);
    }


    /**
     * Runs before a property called $search is updated
     *
     * @var array $search
     *
     * @return void
     */
    public function updatingSearch()
    {
        $this->gotoPage(1);
    }

    /**
     * Runs after any update to the Livewire component's data
     * (Using wire:model, not directly inside PHP)
     *
     * @param string $propertyName
     * @return void
     */
    public function updated($propertyName)
    {
        $this->validateOnly($propertyName);
    }

    public function updatedShowRgpd()
    {
        /*if (!$this->showRgpd) {
            // Clean RGPD input data
            $this->supplier->rgpd_name      = null;
            $this->supplier->rgpd_last_name = null;
            $this->supplier->rgpd_dni       = null;
            $this->supplier->rgpd_email     = null;
            $this->supplier->rgpd_role      = null;
            $this->supplier->rgpd_user_id   = null;
        }*/
    }

    /**
     * Resets all the form inputs
     *
     * @return void
     */
    public function resetInputs()
    {
        $this->resetValidation();
        $this->supplier = new Provider();
    }

    /**
     * Create a new Supplier and ShowModal
     */
    public function addNew()
    {
        $this->resetInputs();
        $this->showModal();
    }

    /**
     * Save new $supplier object or updates an existing one
     * @var object $supplier
     *
     * @return void
     */
    public function save()
    {
        $this->validate();

        try {
            // Add creator
            $this->supplier->creator_user_id    = auth()->id();
            $this->supplier->rgpd_user_id       = ($this->showRgpd) ? auth()->user()->id : null;

            if (!$this->showRgpd) {
                // Clean RGPD input data
                $this->supplier->rgpd_name      = null;
                $this->supplier->rgpd_last_name = null;
                $this->supplier->rgpd_dni       = null;
                $this->supplier->rgpd_email     = null;
                $this->supplier->rgpd_role      = null;
                $this->supplier->rgpd_user_id   = null;
            }

//            dd($this->supplier);

            //Create or update supplier
            $this->supplier->save();

            $this->dispatchBrowserEvent('supplierStore');

            $this->resetInputs();

            session()->flash('status', trans('backend.crud.supplier.messages.save'));
        } catch (\Illuminate\Database\QueryException $exception) {
            $errorInfo = $exception->errorInfo;

            session()->flash('error', $errorInfo[2]);
        }
    }

    /**
     * Gets an exististing $supplier object to edit it
     * @var object $supplier
     *
     * @param string $id
     * @return void
     */
    public function edit($id)
    {
        $this->resetInputs();

        $this->supplier = Provider::findOrFail($id);

        if (!empty($this->supplier->rgpd_user_id)) {
            $this->showRgpd = true;
        }

        $this->showModal();
    }

    /**
     * Changes the $confirmDeleteId value
     * @var string $confirmDeleteId
     *
     * @param string $id
     * @return void
     */
    public function confirmDelete($id)
    {
        $this->confirmDeleteId = $id;
    }

    /**
     * Deletes an existing $supplier object
     * @var object $supplier
     *
     * @return void
     */
    public function delete()
    {
        Provider::findOrFail($this->confirmDeleteId)->delete();
        $this->dispatchBrowserEvent('deleteItem');
        session()->flash('status', trans('backend.crud.supplier.messages.delete'));
    }
}
