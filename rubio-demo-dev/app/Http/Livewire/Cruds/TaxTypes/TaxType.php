<?php

namespace App\Http\Livewire\Cruds\TaxTypes;

use App\Customer;
use App\TaxType as AppTaxType;
use Livewire\Component;
use Livewire\WithPagination;

class TaxType extends Component
{

    use WithPagination;

    protected $paginationTheme = 'bootstrap';

    public ?AppTaxType $taxType = null;

    public $confirmDeleteId;
    public $search;
    public $entries = 10;

    protected $rules = [
        'taxType.type' => 'required',
        'taxType.description' => 'required',
        'taxType.percentage' => 'required|min:0|max:100',
        'taxType.default' => 'required',
        'taxType.customer_id' => 'nullable',
    ];

    protected $validationAttributes = [
        'taxType.type' => 'type',
        'taxType.description' => 'description',
        'taxType.percentage' => 'percentage',
        'taxType.default' => 'default',
    ];

    private function showModal()
    {
        $this->dispatchBrowserEvent('showModal');
    }

    /**
     * Renders the component view
     *
     * @return mixed
     */
    public function render()
    {
        return view('livewire.cruds.tax-types.tax-type',[
            'taxTypes' => AppTaxType::where('type', 'like', '%' . $this->search . '%')
                        ->orWhere('description', 'like', '%' . $this->search . '%')
                        ->orderBy('type')
                        ->paginate($this->entries),
            'customers' => Customer::orderBy('name')->get(),
        ]);
    }

    /**
     * Runs before a property called $search is updated
     *
     * @var array $search
     *
     * @return void
     */
    public function updatingSearch()
    {
        $this->gotoPage(1);
    }

    /**
     * Runs after any update to the Livewire component's data 
     * (Using wire:model, not directly inside PHP)
     *
     * @param string $propertyName
     * @return void
     */
    public function updated($propertyName)
    {
        $this->validateOnly($propertyName);
    }

    /**
     * Resets all the form inputs
     *
     * @return void
     */
    public function resetInputs()
    {
        $this->resetValidation();
        $this->taxType = new AppTaxType();
    }

    /**
     * Create a new TaxType and ShowModal
     */
    public function addNew()
    {
        $this->resetInputs();
        $this->showModal();
    }

    /**
     * Save new $taxType object or updates an existing one
     * @var object $taxType
     *
     * @return void
     */
    public function save()
    {
        $this->validate();

        try {
            //Create or update taxType
            $this->taxType->save();

            $this->dispatchBrowserEvent('taxTypeStore');

            $this->resetInputs();

            session()->flash('status', trans('backend.crud.tax-type.messages.save'));
        } catch(\Illuminate\Database\QueryException $exception) {
            $errorInfo = $exception->errorInfo;

            session()->flash('error', $errorInfo[2]);
        }
    }

    /**
     * Gets an exististing $taxType object to edit it
     * @var object $taxType
     *
     * @param string $id
     * @return void
     */
    public function edit($id)
    {
        $this->resetInputs();

        $this->taxType = AppTaxType::findOrFail($id);

        $this->showModal();
    }

    /**
     * Changes the $confirmDeleteId value
     * @var string $confirmDeleteId
     *
     * @param string $id
     * @return void
     */
    public function confirmDelete($id)
    {
        $this->confirmDeleteId = $id;
    }

    /**
     * Deletes an existing $taxType object
     * @var object $taxType
     *
     * @return void
     */
    public function delete()
    {
        AppTaxType::findOrFail($this->confirmDeleteId)->delete();
        $this->dispatchBrowserEvent('deleteItem');
        session()->flash('status', trans('backend.crud.tax-type.messages.delete'));
    }
}
