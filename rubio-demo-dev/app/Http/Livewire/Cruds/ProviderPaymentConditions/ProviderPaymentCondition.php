<?php

namespace App\Http\Livewire\Cruds\ProviderPaymentConditions;

use Livewire\Component;
use Livewire\WithPagination;
use App\ProviderPaymentCondition as ProviderPayment;
use Rap2hpoutre\FastExcel\FastExcel;

class ProviderPaymentCondition extends Component
{

    use WithPagination;

    protected $paginationTheme = 'bootstrap';

    public ?ProviderPayment $providerPayment = null;
    public $sortBy = 'created_at';
    public $sortDirection = 'desc';
    public $confirmDeleteId = null;
    public $search = null;
    public $entries = 10;


    protected $rules = [
        'providerPayment.name' => 'required|max:255',
        'providerPayment.description' => 'nullable|max:255',
        'providerPayment.code' => 'required|min:2|max:2',
        'providerPayment.is_default' => 'nullable',
    ];

    /**
     * Renders the component view
     *
     * @return mixed
     */
    public function render()
    {
        $queryPayments = ProviderPayment::select('*');
        if ($this->search) {
            $search = '%' . $this->search . '%';
            $searchDefault = -1;
            if ((strtolower($this->search) == 'yes') || (strtolower($this->search) == 'si')) {
                $searchDefault = 1;
            } else if (strtolower($this->search) == 'no') {
                $searchDefault = 0;
            }
            $queryPayments = ProviderPayment::where('code', 'like', $search)
                ->orWhere('name', 'like', $search)
                ->orWhere('description', 'like', $search)
                ->orWhereDate('created_at', date('Y-m-d', strtotime(str_replace('/', '-', $this->search))))
                ->orWhere('is_default', $searchDefault);
        }

        return view(
            'livewire.cruds.provider-payment-conditions.provider-payment-condition',
            [
                'providerPayments' =>  $queryPayments
                    ->orderBy($this->sortBy, $this->sortDirection)
                    ->paginate($this->entries)
            ]
        );
    }

    /**
     * Runs before a property called $search is updated
     *
     * @var array $search
     *
     * @return void
     */
    public function updatingSearch()
    {
        $this->gotoPage(1);
    }

    /**
     * Runs after any update to the Livewire component's data 
     * (Using wire:model, not directly inside PHP)
     *
     * @param string $propertyName
     * @return void
     */
    public function updated($propertyName)
    {
        $this->validateOnly($propertyName);
    }

    /**
     * Resets all the form inputs
     *
     * @return void
     */
    public function resetInputs()
    {
        $this->resetValidation();
        $this->providerPayment = new ProviderPayment();
    }

    /**
     * Renders the component view
     *
     * @return mixed
     */
    private function showModal()
    {
        $this->dispatchBrowserEvent('showModal');
    }

    /**
     * Create a new ProviderPayment and ShowModal
     */
    public function addNew()
    {
        $this->resetInputs();
        $this->showModal();
    }

    /**
     * Gets an exististing $providerPayment object to edit it
     * @var object $providerPayment
     *
     * @param string $id
     * @return void
     */
    public function edit($id)
    {
        $this->resetInputs();

        $this->providerPayment = ProviderPayment::findOrFail($id);

        $this->showModal();
    }

    /**
     * Save new $providerPayment object or updates an existing one
     * @var object $providerPayment
     *
     * @return void
     */
    public function save()
    {
        $this->validate();

        try {
            //Create or update providerPayment
            $this->providerPayment->save();

            $this->dispatchBrowserEvent('paymentStore');

            $this->resetInputs();

            session()->flash('status', trans('backend.crud.provider-payment-condition.messages.save'));
        } catch (\Illuminate\Database\QueryException $exception) {
            $errorInfo = $exception->errorInfo;

            session()->flash('error', $errorInfo[2]);
        }
    }

    /**
     * Changes the $confirmDeleteId value
     * @var string $confirmDeleteId
     *
     * @param string $id
     * @return void
     */
    public function confirmDelete($id)
    {
        $this->confirmDeleteId = $id;
    }

    /**
     * Deletes an existing $providerPayment object
     * @var object $providerPayment
     *
     * @return void
     */
    public function delete()
    {
        ProviderPayment::findOrFail($this->confirmDeleteId)->delete();
        $this->dispatchBrowserEvent('deleteItem');
        session()->flash('status', trans('backend.crud.provider-payment-condition.messages.delete'));
    }


    /**
     * Generates a file with xlsx (Excel) format
     *
     * @param $path
     * @return object
     * @throws \Box\Spout\Common\Exception\IOException
     * @throws \Box\Spout\Common\Exception\InvalidArgumentException
     * @throws \Box\Spout\Common\Exception\UnsupportedTypeException
     * @throws \Box\Spout\Writer\Exception\WriterNotOpenedException
     */
    public function exportToExcel($path)
    {
        $data = ProviderPayment::all();
        return (new FastExcel($data))->export($path, function ($payment) {
            $default = null;
            switch ($payment->is_default) {
                case '0':
                    $default = __('backend.crud.provider-payment-condition.form.default.options.no');
                    break;
                case '1':
                    $default = __('backend.crud.provider-payment-condition.form.default.options.yes');
                    break;
                default:
                    $default = '';
                    break;
            }
            return [
                __('backend.crud.created_at') => auth()->user()->applyDateFormat($payment->created_at),
                __('backend.crud.provider-payment-condition.list.code')  => $payment->code,
                __('backend.crud.provider-payment-condition.list.name')  => $payment->name,
                __('backend.crud.provider-payment-condition.list.description') => $payment->description,
                __('backend.crud.provider-payment-condition.list.default') => $default
            ];
        });
    }

    /**
     * Downloads excel file generated in exportToExcel()
     *
     * @return mixed
     */
    public function downloadExcel()
    {
        $path = tempnam(sys_get_temp_dir(), "FOO");
        $this->exportToExcel($path);

        return response()->download($path, 'Condiciones_pago_proveedor.xlsx', [
            'Content-Type'          => 'application/vnd.ms-excel',
            'Content-Disposition'   => 'inline; filename="invoices.xlsx"'
        ]);
    }

    /**
     * Order of query
     *      
     *  @return void
     */
    public function sortBy($field)
    {
        if ($this->sortBy == $field) {
            $this->sortDirection = $this->sortDirection === 'asc' ? 'desc' : 'asc';
        } else {
            $this->sortDirection = 'asc';
        }
        $this->sortBy = $field;
    }
}
