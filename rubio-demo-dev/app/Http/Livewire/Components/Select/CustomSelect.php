<?php

namespace App\Http\Livewire\Components\Select;

use Livewire\Component;

/**
 * Class CustomUnitsSelect
 * This class can be extended to create specific select2 dropdowns
 *
 * @package App\Http\Livewire\Components\Select
 *
 */
class CustomSelect extends Component
{
    public $selectName;
    public $selectId;
    public $objectsAll;
    public $object;
    public $objectValue;
    public $optionIdFieldMapping;
    public $optionNameFieldMapping;
    public $parentEvent;
    public $parentComponentName;
    public $placeholder;
    public $checkValuesDebugButton;
    public $modelPropertyName;
    public $optionFieldOrderBy;
    public $optionFieldOrderDirection;
    public $minimumInputLength;
    public $includeJsCdn        = true;
    public $renderWithError     = false;
    public $defaultObjectValue  = false;
    public $hardResetSelect     = false;
    protected $listeners        = [
        'refreshCustomSelect' => '$refresh',
        'resetCustomSelect',
        'editingCustomSelect'
    ];

    /**
     * In Livewire components, you use mount() instead of a class constructor __construct()
     * like you may be used to.
     *
     * @param $selectName
     * @param $selectId
     * @param $modelToCreateOptions
     * @param $scopeToCreateOptions
     * @param $optionIdFieldMapping
     * @param $optionNameFieldMapping
     * @param $parentComponentName
     * @param $parentEvent
     * @param $placeholder
     * @param bool $objectValue
     * @param bool $includeJsCdn
     * @param $modelPropertyName
     * @param $checkValuesDebugButton
     * @param $optionFieldOrderBy
     * @param $optionFieldOrderDirection
     * @param $minimumInputLength
     * @param $hardResetSelect
     */
    public function mount(
        $selectName,
        $selectId,
        $modelToCreateOptions,
        $modelPropertyName,
        $optionFieldOrderBy,
        $optionFieldOrderDirection,
        $hardResetSelect        = false,
        $minimumInputLength     = 0,
        $scopeToCreateOptions   = false,
        $checkValuesDebugButton = false,
        $placeholder            = 'Seleccione una opción...',
        $optionIdFieldMapping   = 'id',
        $optionNameFieldMapping = 'name',
        $parentComponentName    = false,
        $parentEvent            = false,
        $objectValue            = false,
        $includeJsCdn           = false
    ) : void
    {
        // Check if model from where options will come is actually a real model
        if (class_exists($modelToCreateOptions)) {
            // New instance to be able to check if the class is actually a model
            $class = new $modelToCreateOptions();

            // Check if its an eloquent model instance
            if ($class instanceof \Illuminate\Database\Eloquent\Model) {

                // Reload default values before assignations
                $this->reload();

                if ($scopeToCreateOptions) {
                    $this->objectsAll   = $class->{$scopeToCreateOptions}()
                        ->orderBy($optionFieldOrderBy, $optionFieldOrderDirection)
                        ->get();
                    /*$cacheKey           = 'custom-select-' . $selectId . '-' . $scopeToCreateOptions . '-' . $optionFieldOrderBy  . '-' . $optionFieldOrderDirection;
                    $this->objectsAll   = Cache::remember($cacheKey, config('custom.global-cache-time-in-seconds'),
                        function () use ($class, $optionFieldOrderBy, $optionFieldOrderDirection, $scopeToCreateOptions) {
                            return $class->{$scopeToCreateOptions}()
                                ->orderBy($optionFieldOrderBy, $optionFieldOrderDirection)->get();
                        });*/
                } else {
                    // Get all from model to build up the options array
                    $this->objectsAll   = $class::orderBy($optionFieldOrderBy, $optionFieldOrderDirection)->get();
                    /*$cacheKey           = 'custom-select-' . $selectId . '-' . $optionFieldOrderBy  . '-' . $optionFieldOrderDirection;
                    $this->objectsAll   = Cache::remember($cacheKey, config('custom.global-cache-time-in-seconds'),
                        function () use ($class, $optionFieldOrderBy, $optionFieldOrderDirection) {
                            return $class::orderBy($optionFieldOrderBy, $optionFieldOrderDirection)->get();
                        });*/
                }

                // Assign setting values
                $this->selectName                   = $selectName;
                $this->selectId                     = $selectId;
                $this->placeholder                  = $placeholder;
                $this->objectValue                  = $objectValue;
                $this->includeJsCdn                 = $includeJsCdn;
                $this->optionIdFieldMapping         = $optionIdFieldMapping;
                $this->optionNameFieldMapping       = $optionNameFieldMapping;
                $this->parentComponentName          = $parentComponentName;
                $this->parentEvent                  = $parentEvent;
                $this->modelPropertyName            = $modelPropertyName;
                $this->checkValuesDebugButton       = $checkValuesDebugButton;
                $this->optionFieldOrderBy           = $optionFieldOrderBy;
                $this->optionFieldOrderDirection    = $optionFieldOrderDirection;
                $this->defaultObjectValue           = $objectValue;
                $this->minimumInputLength           = $minimumInputLength;
                $this->hardResetSelect              = $hardResetSelect;

            } else {
                $this->renderWithError = true;
            }

        } else {
            $this->renderWithError = true;
        }
    }

    /**
     * Renders the component view
     *
     * @return mixed
     */
    public function render()
    {
        if ($this->renderWithError) {
            $this->renderWithError = false;
            return view('livewire.components.select.custom-select-error')
                ->with('error', 'modelToCreateOptions debe ser un modelo válido');
        } else {
            return view('livewire.components.select.custom-select2');
        }
    }

    /**
     * Runs after objectValue property is updated
     *
     * @param $value
     */
    public function updatedObjectValue($value) : void
    {
        if ($value) {
            // Get the updated value
            $this->objectValue = $value;

            // Get the object for that id
            $this->object = $this->objectsAll->where($this->optionIdFieldMapping, $value)->first();

            // Emit a custom event if both are specified
            if ($this->parentComponentName && $this->parentEvent) {
                // Emit parent event to assign value to model property
                $this->emitUp(
                    $this->parentEvent,
                    $this->modelPropertyName,
                    $value
                );
            }
        }
    }

    /**
     * Just for testing proposes
     */
    public function checkValues() : void
    {
        dd('checkValues', $this);
    }

    /**
     * Triggers a browser event to assign a value to the custom select when parent component is being edited
     * @param $data
     */
    public function editingCustomSelect($data) : void
    {
        if (!empty($data[$this->selectId])) {
            $this->dispatchBrowserEvent(
                'editingSelect',
                [
                    'selectId'      => $this->selectId,
                    'objectValue'   => $data[$this->selectId]['value'],
                    'objectField'   => $data[$this->selectId]['field']
                ]
            );
        }
    }

    /**
     * Resets the component by dispatching a browser event to trigger a select2 change event
     *
     * @param $type
     */
    public function resetCustomSelect($type = false) : void
    {
        $eventName = 'resetCustomSelect';

        if ($type == 'hardReset') {
            $eventName = 'hardResetCustomSelect';
        }

        $this->dispatchBrowserEvent(
            $eventName,
            [
                'selectId'      => $this->selectId,
                'objectValue'   => $this->defaultObjectValue // this resets to default value coming from instantiated component
            ]
        );
    }

    /**
     * Reloads component's properties
     */
    public function reload() : void
    {
        $this->object                       = false;
        $this->objectValue                  = false;
        $this->objectsAll                   = false;
        $this->selectName                   = false;
        $this->selectId                     = false;
        $this->placeholder                  = false;
        $this->includeJsCdn                 = false;
        $this->optionIdFieldMapping         = false;
        $this->optionNameFieldMapping       = false;
        $this->parentComponentName          = false;
        $this->parentEvent                  = false;
        $this->modelPropertyName            = false;
        $this->checkValuesDebugButton       = false;
        $this->optionFieldOrderBy           = false;
        $this->optionFieldOrderDirection    = false;
        $this->minimumInputLength           = false;
        $this->hardResetSelect              = false;
    }

    /**
     * Applies sorting parameters on model to create select options
     * @deprecated
     */
    public function applySortingOnModelToCreateOptions() : void
    {
        /*if (
            !empty($this->optionFieldOrderBy) &&
            !empty($this->optionFieldOrderDirection) &&
            in_array($this->optionFieldOrderDirection, ['asc', 'desc'])
        ) {
            if ($this->optionFieldOrderDirection == 'asc') {
                $this->objectsAll->sortBy($this->optionFieldOrderBy);
            } else {
                $this->objectsAll->sortByDesc($this->optionFieldOrderBy);
            }
        }*/
    }
}
