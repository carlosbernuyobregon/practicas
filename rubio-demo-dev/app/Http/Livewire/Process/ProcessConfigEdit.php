<?php

namespace App\Http\Livewire\Process;

use App\Process;
use Illuminate\Support\Facades\DB;
use Livewire\Component;

class ProcessConfigEdit extends Component
{

    public $data;
    public $show;

    protected $rules = [
        'editData.id' => 'string',
        'editData.key_value' => 'required|string',
        'editData.model'=> 'required|string',
        'editData.bpm_id' => 'required|string',
        'editData.bpm_version_id' => 'required|string',
        'editData.bpm_name' => 'required|string',
        'editData.name' => 'required|string',
        'editData.description' => 'required|string',
        'editData.tenant_id' => 'required|string',
        'editData.variables' => 'required|string',
        'editData.version' => 'required|int',
        'editData.active' => 'bool'
    ];
    public $process = null;
    public Process $editData;

    protected $listeners = ['showModal' => 'showModal'];

    public function mount(Process $editData)
    {
        $this->editData = $editData;
       // dd($editData);
        $this->show = false;
    }


    public function editProcess()
    {
        $this->validate();

        $this->editData->save();

        $this->returnParent();

    }

    public function deleteProcess()
    {
        $this->validate();

        $this->editData->delete();

        $this->returnParent();
    }

    public function returnParent()
    {
        return redirect()->route('process.list');
    }

    public function render()
    {

        return view('livewire.process.process-config-edit');
    }

}
