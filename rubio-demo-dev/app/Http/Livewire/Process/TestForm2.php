<?php

namespace App\Http\Livewire\Process;

use Livewire\Component;

class TestForm2 extends Component
{
    public function render()
    {
        return view('livewire.process.test-form2');
    }
}
