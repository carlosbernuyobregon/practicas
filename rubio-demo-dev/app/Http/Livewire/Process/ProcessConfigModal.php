<?php

namespace App\Http\Livewire\Process;

use App\Process;
use Illuminate\Support\Facades\DB;
use Livewire\Component;

class ProcessConfigModal extends Component
{

    public $data;
    public $show;

    protected $rules = [
        'editData.id' => 'string',
        'editData.key_value' => 'required|string',
        'editData.model'=> 'required|string',
        'editData.bpm_id' => 'required|string',
        'editData.bpm_version_id' => 'required|string',
        'editData.bpm_name' => 'required|string',
        'editData.name' => 'required|string',
        'editData.description' => 'required|string',
        'editData.tenant_id' => 'required|string',
        'editData.variables' => 'required|string',
        'editData.version' => 'required|int',
        'editData.active' => 'int'
    ];
    public $process = null;
    public Process $editData;

    protected $listeners = ['showModal' => 'showModal'];

    public function mount(Process $editData)
    {
        $this->editData = $editData;

        $this->show = false;
    }

    public function showModal($data)
    {
        $this->data = $data;
        $this->showDetail();
    }

    public function showDetail()
    {
        $this->show = true;
    }

    public function hideDetail()
    {
        $this->show = false;
    }

    public function addNewProcess()
    {
        $this->validate();
        //dd($this->editData);
        $this->editData->save();
        //dd($this->editData);
        $this->hideDetail();
        $this->returnParent();

    }

    public function returnParent()
    {
        return redirect()->route('process.list');
    }


    public function render()
    {
        return view('livewire.process.process-config-modal');
    }

}
