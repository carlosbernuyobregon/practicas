<?php

namespace App\Http\Livewire\Process;

use Illuminate\Support\Facades\DB;
use Livewire\Component;

class ProcessConfig extends Component
{


    public function render()
    {
        $process = DB::select('select * from processes');

        return view('livewire.process.process-config',['process'=>$process]);
    }



    public function delete()
    {
        $process = DB::delete('delete from processes where id is not null');
    }

}
