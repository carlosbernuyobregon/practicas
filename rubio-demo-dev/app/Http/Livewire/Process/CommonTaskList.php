<?php

namespace App\Http\Livewire\Process;

use App\Order;
use App\Process;
use Carbon\Carbon;
use Livewire\Component;

class CommonTaskList extends Component
{
    public $entries = 10;
    public function render()
    {
        $process = new Process();
        //$instancesTasks = $process->getUserTasksCamunda('459510b2-a3f3-40ea-850b-73a89e07dd50');
        $instancesTasks = $process->getUserTasksCamunda();
        //dd($instancesTasks);
        $tasks = [];
        foreach($instancesTasks as $item){
            //$variables = $process->getTaskVariables($item->id);
            //$variables= $process->getFormVariables($item->id);
            //dd($variables);
            try {
                $variables= $process->getFormVariables($item->id);
                //$order = Order::find($variables['processId']);
                //dd($actions);
                try {
                    $data = [
                        'actions' => $variables['actions'],
                        'processId' => $variables['processId'],
                        //'data' => Order::where('id',$variables['processId'])->with(['user','provider'])->first(),
                        'formUrl' => str_replace('{taskId}',$item->id,$item->formKey),
                        'taskId' => $item->id,
                        'task' => $item->taskDefinitionKey,
                        'created' => Carbon::parse($item->created)->format('d/m/Y H:i:s'),
                        'priority' => $item->priority,
                        'name' => $item->name
                    ];
                } catch (\Throwable $th) {
                    //dump('Error generación array: ' . $item->id);
                }
                $tasks[] = $data;

            } catch (\Throwable $th) {
                //dump('Error captura variables: ' . $item->id);
            }
        }
        //dd($tasks);
        return view('livewire.process.common-task-list',[
            'tasks' => $tasks
        ]);
    }
}
