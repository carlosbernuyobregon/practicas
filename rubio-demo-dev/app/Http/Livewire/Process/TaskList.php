<?php

namespace App\Http\Livewire\Process;

use App\Process;
use Livewire\Component;

class TaskList extends Component
{
    public function render()
    {
        $process = new Process();
        $tasks = $process->getAllTasks(); 
        return view('livewire.process.task-list',[
            'tasks' => $tasks
        ]);
    }
}
