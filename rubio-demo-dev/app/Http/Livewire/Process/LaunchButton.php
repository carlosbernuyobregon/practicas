<?php

namespace App\Http\Livewire\Process;

use App\Process;
use Livewire\Component;
use App\Setting;
use Faker\Generator as Faker;
use Illuminate\Support\Str;

class LaunchButton extends Component
{
    public function test()
    {
        dd('tested button');
    }
    public function launch()
    {
        $data = [
            'key_value' => Str::random(10),
            'value' => 'testvalue',
            'count' => 2
        ];

        $processKey = 'Solicitud_Pedidos_No_Stock';
       // $dataFields  = '{"variables": {},"businessKey" : "myBusinessKey"}';

      $setting = Setting::create($data);
      $dataFields  = [
          'variables'=>[
              'processId'=>[
                  'value'=>(string)$setting->id,
                  'type'=>'string'
              ],
              'portalInstance'=>[
                'value'=>(string)$portal->portal,
                'type'=>'string'
            ],
              ],
          'businessKey'=>'started Portal'
      ];
      $process = new Process();
      $process->submitProcess($processKey,$dataFields);
     // dd($process);

     //$this->render();
    }

    public function render()
    {

        return view('livewire.process.launch-button');
    }
}
