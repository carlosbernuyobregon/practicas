<?php

namespace App\Http\Livewire\Process;

use App\Process;
use Livewire\Component;

class Actions extends Component
{
    public $actions = [];
    public $processInstance = null;
    public function mount($actions = null,$processInstance = null)
    {
        $this->actions = $actions;
        $this->processInstance = $processInstance;
    }
    public function render()
    {
        return view('livewire.process.actions');
    }
    public function submitAction($action = null)
    {
        $dataFields = [
            'variables' => [
               'varActionsDecision' => [
                   'value' => trim($action)
               ]
            ],
            'withVariablesInReturn' => true
          //  'businessKey' => "Rubio"
          // quitado BusinessKey para no sobreescribir
        ];
        $process = new Process();
        $response = $process->submitTaskCamunda($this->processInstance,json_encode($dataFields));
        return redirect()->route('order.tasks')
                        ->with('success','Tarea completa correctamente.');

    }
}
