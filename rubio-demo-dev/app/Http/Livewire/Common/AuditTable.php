<?php

namespace App\Http\Livewire\Common;

use App\Invoice;
use Livewire\Component;
use Livewire\WithPagination;

class AuditTable extends Component
{
    use WithPagination;

    protected $paginationTheme = 'bootstrap';

    /**
     * Corregir
     */
    protected $listeners = [
        'rechargeAudit' => 'getAuditProperty',
        'rechargeAuditLine' => 'getAuditProperty'
    ];

    public $dataValue = null;

    public $entries = 10;

    /**
     * In Livewire components, you use mount() instead of a class constructor __construct()
     * like you may be used to.
     * @param mixed $name
     * @return void
     */
    public function mount($dataValue = null)
    {
        $this->dataValue = $dataValue;
    }

    /**
     * Renders the component view
     *
     * @return mixed
     */
    public function render()
    {
        return view('livewire.common.audit-table',[
            'auditLines' => $this->dataValue->audit()->paginate($this->entries),
        ]);
    }

    /**
     * Reloads the $invoice object
     * @var object $invoice
     *
     * @return void
     */
    public function getAuditProperty()
    {
        $this->dataValue->refresh();
    }
}
