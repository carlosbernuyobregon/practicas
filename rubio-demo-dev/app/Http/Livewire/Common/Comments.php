<?php

namespace App\Http\Livewire\Common;

use App\Comment;
use App\Models\ChatThread;
use App\User;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Facades\Log;
use Illuminate\View\View;
use Livewire\Component;
use function Clue\StreamFilter\fun;

class Comments extends Component
{
    public $entityId;
    public $entityType;
    public $comments;
    public $editable;
    public $tabParentId;
    public $tabId;
    public $allowUsersApproach;

    public $usersList;
    public $threadId;
    public $newComment      = '';
    public $textTitle       = '';
    public $selectedUsers   = [];
    public $allowedUsers;
    public $selectedUsersId = [];

    protected $rules    = [
        'newComment' => 'required|string'
    ];
    protected $listeners = [
        'setNewMessageAsRead',
        'setTabOnlyForAllowedUsers',
        'setTabNewMessageNotification',
        'reloadMessages',
        '$refresh'
    ];

    /**
     * In Livewire components, you use mount() instead of a class constructor __construct()
     * like you may be used to.
     *
     * @param null $entityId
     * @param null $entityType
     * @param string $textTitle
     * @param bool $editable
     * @param $tabParentId
     * @param $tabId
     * @param $allowUsersApproach
     */
    public function mount(
        $entityId               = null,
        $entityType             = null,
        $textTitle              = 'Comments module',
        $editable               = true,
        $tabParentId            = false,
        $tabId                  = false,
        $allowUsersApproach     = false
    ) : void
    {
        $this->entityId          = $entityId;
        $this->entityType           = $entityType;
        $this->editable             = $editable;
        $this->textTitle            = $textTitle;
        $this->tabParentId          = $tabParentId;
        $this->tabId                = $tabId;
        $this->allowUsersApproach   = $allowUsersApproach;

        $this->comments = Comment::where('commentable_type', $this->entityType)
            ->where('commentable_id', $this->entityId)
            ->orderBy('created_at', 'desc')
            ->get();

        if ($this->allowUsersApproach) {
            // Set up user permissions approach
            $this->setUsersApproachConfiguration();
        }
    }

    /**
     * Renders the component view
     *
     * @return View
     */
    public function render()
    {
        $this->reloadMessages();
//        $this->emit('showNewMessageNotification');

        return view('livewire.common.comments')
            ->with([
                'unreadMessages' => $this->isNewToCurrentUser()
            ]);
    }

    /**
     * Sets up all necessary limit chat functionality to certain group of users
     */
    public function setUsersApproachConfiguration()
    {
        // Get the chat thread and grab the users added to it
        $thread                 = $this->getTheThread();
        $this->threadId         = $thread->id ?? false;
        $this->selectedUsersId  = $thread->users_id ?? [];

        // Get the allowed users list
        $this->allowedUsers = User::whereHas('roles', function (Builder $query) {
            $query
                ->where('key_value', '=', 'administration-manager')
                ->orWhere('key_value', '=', 'administration-user');
        })->where('email', 'like', '%' . config('custom.user-email-domain-for-chat-permission') . '%')->get();

        // Manage lists
        $this->setUsersLists();
    }

    /**
     * Saves new comments
     */
    public function save() : void
    {
        $newThread = ChatThread::updateOrCreate(
            ['id' => $this->threadId],
            ['users_id' => $this->selectedUsersId]
        );

        $this->threadId = $newThread->id;

        $this->validate();
        $data = [
            'comment'           => $this->newComment,
            'commentable_id'    => $this->entityId,
            'commentable_type'  => $this->entityType,
            'user_id'           => auth()->user()->id,
            'thread_id'         => $this->threadId
        ];

        try {
            Comment::create($data);
        } catch (\PDOException $e) {
            Log::error($e->getMessage(), [
                'class'     => __CLASS__,
                'method'    => __METHOD__,
            ]);
        }

        $this->newComment = '';
        $this->emit('refresh');
    }

    /**
     * Checks if last message was sent to current user
     *
     * @return bool
     */
    public function isNewToCurrentUser()
    {
        // Get the last chat message
        $lastMessage = ($this->comments->isNotEmpty()) ? $this->comments->first() : false;

        if (!$lastMessage) {
            return false;
        }

        return auth()->user()->id != $lastMessage->user_id && $lastMessage->is_new;
    }

    /**
     * Sets the chat tab only for allowed users
     */
    public function setTabOnlyForAllowedUsers() : void
    {
        // Dispatch event to hide tab for those users left out of the allowed users list
        if (
            $this->allowedUsers->isEmpty() ||
            !empty($this->selectedUsers) && !array_key_exists(auth()->user()->id, $this->selectedUsers)
        ) {
//            dd('here', $this->allowedUsers->isEmpty(), $this->selectedUsers);
            $this->dispatchBrowserEvent('showTabOnlyForAllowedUsers', [
                'tabId' => $this->tabId
            ]);
        }
    }

    /**
     * Sets the new message notification right into the tab
     */
    public function setTabNewMessageNotification() : void
    {
        if ($this->isNewToCurrentUser() && $this->tabId) {
            $this->dispatchBrowserEvent('showNewMessageNotification', [
                'tabParentId'   => $this->tabParentId,
                'tabId'         => $this->tabId,
                'messageCount'  => $this->comments->where('is_new', true)->count()
            ]);
        }
    }

    /**
     * Sets new message as read
     */
    public function setNewMessageAsRead() : void
    {
        if ($this->isNewToCurrentUser()) {
            Comment::where('commentable_type', $this->entityType)
                ->where('commentable_id', $this->entityId)
                ->where('user_id', '!=', auth()->user()->id)
                ->update(['is_new' => 0]);

            $this->reloadMessages();
        }
    }

    /**
     * Reloads all chat messages for a given entity
     */
    public function reloadMessages() : void
    {
        $this->comments = Comment::where('commentable_type', $this->entityType)
            ->where('commentable_id', $this->entityId)
            ->orderBy('created_at', 'desc')
            ->get();
    }

    /**
     * Gets the current chat thread model
     * @return mixed
     */
    public function getTheThread()
    {
        if ($this->comments->isNotEmpty()) {
            $firstChat = $this->comments->first();
            $firstChat->load('thread');
            return  $firstChat->thread;
        }

        return false;
    }

    /**
     * Sets users lists depending on allowed and selected users for that particular chat implementation
     */
    public function setUsersLists() : void
    {
        // Users added to chat
        $selectedUsersId = $this->selectedUsersId;

        // Allowed users not added to chat
        $this->usersList = $this->allowedUsers->filter( function ($user, $key) use ($selectedUsersId) {
            return !in_array($user->id, $selectedUsersId);
        });

        // Get the selected users list
        if ($this->allowedUsers->isNotEmpty()) {

            if (empty($selectedUsersId)) {
//                $selectedUsersId = $this->usersList->pluck('id')->toArray();
            }

            foreach ($selectedUsersId as $selectedUserId) {
                $firstUser = $this->allowedUsers->where('id', $selectedUserId)->first();
                if ($firstUser) {
                    $this->selectedUsers[$selectedUserId] = $firstUser->toArray();
                }
            }
        }

//        dd($this->usersList, $this->selectedUsers, $this->allowedUsers, $selectedUsersId);
    }

    /**
     * Add or remove users from selected or allowed users lists
     * @param $id
     */
    public function toggleSelectedUsers($id) : void
    {
        if (in_array($id, $this->selectedUsersId)) {
            // Remove the id from array
            unset($this->selectedUsersId[$id]);
            unset($this->selectedUsers[$id]);
        } else {
            // Add the id to array
            $this->selectedUsersId[$id] = $id;
            $this->selectedUsers[$id]   = $this->allowedUsers->where('id', $id)->first()->toArray(); // Forcing to array since livewire turns objects into array to travel back and forth
        }

        $this->setUsersLists();

        // Update users list in thread, only if exists
        if ($this->threadId) {
            ChatThread::updateOrCreate(
                ['id' => $this->threadId],
                ['users_id' => $this->selectedUsersId]
            );
        }
    }
}
