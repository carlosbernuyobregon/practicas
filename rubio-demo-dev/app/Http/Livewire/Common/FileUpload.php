<?php

namespace App\Http\Livewire\Common;

use App\File;
use Carbon\Carbon;
use Illuminate\Support\Facades\Storage;
use Livewire\Component;
use Ramsey\Uuid\Uuid;
use Illuminate\Support\Str;
use Livewire\WithFileUploads;
use Livewire\WithPagination;

class FileUpload extends Component
{
    use WithFileUploads, WithPagination;
    public $s3 = true;
    public $path = '';
    public $modelId = null;
    public $model = null;
    public $type = null;
    public $routeId = null;
    public $route = null;
    public $files = [];
    public $enableUpload = true;
    public $enableDelete;
    public $eventIdentifier = '';
    public $title;

    protected $paginationTheme = 'bootstrap';

    protected $rules = [
        'files.*' => 'file|max:2000000000', // 2Gb Max
    ];

    protected $messages = [
        'files.*.max' => 'This file is too large. The system accepts files up to 2Gb'
    ];

    public function mount(
        $s3 = true,
        $path = null,
        $modelId = null,
        $model = null,
        $type = null,
        $enableUpload = true,
        $eventIdentifier = null,
        $title = 'Archivos adjuntos'
    ) {
        $this->s3 = $s3;
        $this->path = $path;
        $this->modelId = $modelId;
        $this->model = $model;
        $this->type = $type;
        $this->enableUpload = $enableUpload;
        $this->eventIdentifier = $eventIdentifier ?? Str::random(10);
        $this->title = $title;
    }

    public function render()
    {
        $uploadedFiles = File::where('fileable_id', $this->modelId)
            ->where('fileable_type', $this->model)
            ->where('type',$this->type);

        return view('livewire.common.file-upload',[
            'filteredFiles' => $uploadedFiles->orderBy('created_at','desc')->paginate(5)
        ]);
    }

    public function save()
    {
        $this->validate([
            'files.*' => 'required|file|max:2000000000',
        ]);
        //dd($this->files);
        if(empty($this->files)){
            $this->addError('files', 'Adjuntar minimo un archivo.');
        }
        foreach ($this->files as $file) {
            try {
                $originalFilename = $file->getClientOriginalName();
                $originalExtension = $file->getClientOriginalExtension();
            } catch (\Throwable $th) {
                dump('error uploading file');
                die();
            }
            $name = (string) Uuid::uuid4() . '.' . $originalExtension;
            //dd($this->path);
            $this->route = $file->storeAs($this->path,$name,'s3');

            //dd($this->route);
            $fileData = [
                'fileable_id' => $this->modelId,
                'fileable_type' => $this->model,
                'name' => $file->getClientOriginalName(),
                'filename' => $name,
                'type' => $this->type,
                'cmis_id' => $this->routeId,
                'cmis_url' => $this->route,
                'filetype' => $originalExtension
            ];
            File::create($fileData);
            $this->files = [];
            $this->emitUp($this->eventIdentifier);
            session()->flash('success', 'File saved successfully.');
            $this->render();
        }
    }

    /**
     * Deletes file model and filesystem document
     * @param $id
     */
    public function delete($id) : void
    {
        // Get the file model
        $file = File::find($id);

        // Check File and Document existence
        if ($file && Storage::disk('s3')->exists($file->cmis_url)) {
            // Delete file from filesystem
            if (Storage::disk('s3')->delete($file->cmis_url)) {
                // Soft delete model
                $file->delete();
            }
        }
        $this->emitUp($this->eventIdentifier);
        $this->render();
    }
}
