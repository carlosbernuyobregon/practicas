<?php

namespace App\Http\Livewire\Test;

use Livewire\Component;

class Test extends Component
{
    public $selCity = '';

    public $cities = [

        'Rajkot',

        'Surat',

        'Baroda',

    ];
    public function updated($propertyName)
    {
        dd($propertyName);
    }
    public function render()
    {
        return view('livewire.test.test');
    }
}
