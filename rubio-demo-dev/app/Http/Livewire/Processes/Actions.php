<?php

namespace App\Http\Livewire\Processes;

use Livewire\Component;

class Actions extends Component
{
    public function render()
    {
        return view('livewire.processes.actions');
    }
}
