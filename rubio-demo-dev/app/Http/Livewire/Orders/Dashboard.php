<?php

namespace App\Http\Livewire\Orders;

use App\CostCenter;
use App\Models\Provider;
use App\Order;
use App\User;
use Asantibanez\LivewireCharts\Charts\LivewireColumnChart;
use Asantibanez\LivewireCharts\Facades\LivewireCharts;
use Asantibanez\LivewireCharts\Models\ColumnChartModel;
use DateTime;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Livewire\Component;

class Dashboard extends Component
{
    public $providers;
    public $costCenters;
    public $filtersMode = false;
    public $search      = [
        'from'          => '',
        'to'            => '',
        'costCenterId'  => '',
        'providerId'    => '',
    ];
    // Custom select implementation
    public $customSelectModelPropertyNames = [
        'providerCustomSelect' => 'providerId'
    ];
    // Custom select implementation

    protected $listeners = [
        // Custom select implementation. Self method to update values coming from custom select changed event
        'changedCustomSelect' => 'updateModelPropertyComingFromCustomSelect',
        // Custom select implementation
    ];

    /**
     * @var $usersWhoHaveCreatedAnOrder
     */
    protected $usersWhoHaveCreatedAnOrder;

    /**
     * In Livewire components, you use mount() instead of a class constructor __construct()
     * like you may be used to.
     */
    public function mount() : void
    {
        // Filters saved on session
        if (session()->has('search')) {
            foreach ($this->search as $key => $filter) {
                if (session()->has('search.'.$key) == $key) {
                    $this->search[$key] = session()->get('search.'.$key)[0];
                }
            }
        }

        // Default Cost centers
        $this->costCenters = CostCenter::where('customer_id', auth()->user()->customer_id)
            ->orderBy('name', 'asc')
            ->get();

        $currentUser = auth()->user();
        $currentUser->load('costCenters');

        if ($currentUser->hasRole('direccion-general')) {
            // Director general -> select costcen where manager_3_id  + himself
            // Get the cost centers managed by user with DG role
            $costCenters = CostCenter::where('manager_3_id', $currentUser->id)
                ->where('customer_id', auth()->user()->customer_id)
                ->orderBy('name', 'asc')
                ->get();

            // Get the Orders users from related cost centers
            $this->usersWhoHaveCreatedAnOrder = $this->giveMeUsersListFromCostCenter($costCenters, 'manager_2_id');

        } elseif ($currentUser->hasRole('direccion-de-pedidos')) {
            // Director de pedidos -> select costcen where manager_2_id + himself
            // Get the cost centers managed by user with DP role
            $costCenters = CostCenter::where('manager_2_id', $currentUser->id)
                ->where('customer_id', auth()->user()->customer_id)
                ->orderBy('name', 'asc')
                ->get();

            // Get the Orders users from related cost centers
            $this->usersWhoHaveCreatedAnOrder   = $this->giveMeUsersListFromCostCenter($costCenters, 'manager_1_id');
//
        } elseif ($currentUser->hasRole('manager-de-pedidos')) {
            // Get the cost centers managed by user with MP role
            $costCenters = CostCenter::where('manager_1_id', $currentUser->id)
                ->where('customer_id', auth()->user()->customer_id)
                ->orderBy('name', 'asc')
                ->get();

            // Get the Orders users from related cost centers
            $this->usersWhoHaveCreatedAnOrder   = $this->giveMeUsersListFromCostCenter($costCenters);

            // Default cost centers to be used on the chart amountByCeCoChart
            $this->search['initialCostCenterId'] = $costCenters->pluck('id')->toArray();

            if (count($this->search['initialCostCenterId']) == 1) {
                $this->search['costCenterId'] = $this->search['initialCostCenterId'];
            }

        } elseif ($currentUser->hasRole('usuario-de-pedidos')) {
            // Add the current user
            $this->usersWhoHaveCreatedAnOrder[] = $currentUser->id;
            // Get the cost centers related to the current user
            $this->costCenters = $currentUser->costCenters->sortBy('name');

            // Default cost centers to be used on the chart amountByCeCoChart
            $this->search['initialCostCenterId'] = $currentUser->costCenters->pluck('id')->toArray();
        }

        if (!empty($costCenters)) {
            // Set the cost centers for dropdown
            $this->costCenters = $costCenters;
        }

        // Commented out cos providers come from the custom select 2 component
        // Providers
        /*$this->providers = Provider::select('id','external_id','name','vat_number')
            ->where('customer_id', auth()->user()->customer_id)
            ->orderBy('name', 'asc')
            ->limit(10)
            ->get();*/
    }

    /**
     * Runs after any update to the Livewire component's data
     * (Using wire:model, not directly inside PHP)
     *
     * @param string $propertyName
     * @param mixed $value
     * @return void
     */
    public function updated($propertyName, $value) : void
    {
        $this->manageSessionAndFilters($propertyName, $value);
    }

    /**
     * Renders the component view
     *
     * @return mixed
     */
    public function render()
    {
        return view('livewire.orders.dashboard',[
            'ordersByMonth'     => $this->ordersByMonthsChart(),
            'amountByMonth'     => $this->amountByMonthsChart(),
            'amountByProvider'  => $this->amountByProvidersChart(),
            'amountByCeCo'      => $this->amountByCeCoChart(),
        ]);
    }

    /**
     * Resets the search options
     *
     */
    public function resetFilters() : void
    {
        session()->forget('search');

        $this->search = [
            'from'          => '',
            'to'            => '',
            'costCenterId'  => '',
            'providerId'    => '',
        ];

        // Reset custom select
        $this->emit('resetCustomSelect', 'hardReset');
    }

    /**
     * Sets a column chart model of orders by months
     *
     * @return ColumnChartModel
     */
    protected function ordersByMonthsChart()
    {
        // Get Orders (orders by month chart)
        $ordersByMonthQuery = Order::select(DB::raw('COUNT(*) as count, MONTH(created_at) as month'))
            ->where('customer_id', auth()->user()->customer_id)
            ->where('order_number', '!=', null);

        // Current user limitation
        if (!empty($this->usersWhoHaveCreatedAnOrder)) {
            $ordersByMonthQuery = $ordersByMonthQuery->whereIn('user_id', $this->usersWhoHaveCreatedAnOrder);
        }

        // Add filtering values
        if ($this->search['costCenterId']) {
            $ordersByMonthQuery->where('cost_center_id', $this->search['costCenterId']);
        }

        if ($this->search['providerId']) {
            $ordersByMonthQuery->where('provider_id', $this->search['providerId']);
        }

        if ($this->search['from']) {
            $ordersByMonthQuery->whereDate('created_at', '>=' , $this->search['from']);
        }

        if ($this->search['to']) {
            $ordersByMonthQuery->whereDate('created_at', '<=', $this->search['to']);
        }

        $ordersByMonthQuery
            ->groupBy('month')
            ->orderBy('month','asc');

        // Get the results
        $orders = $ordersByMonthQuery->get();

        // New orders by month chart
        $ordersByMonth =
            (new ColumnChartModel())
                ->setTitle('Pedidos por mes')
                ->setAnimated(true)
                ->withDataLabels()
                ->multiColumn()
                ->setXAxisCategories(['Mes'])
                ->setColors($this->getChartColors());

        // Adding column bars to the chart
        foreach ($orders as $order) {
            $dateObj        = DateTime::createFromFormat('!m', $order->month);
            $monthName      = $dateObj->format('F');
            $categories[]   = $monthName;
            $ordersByMonth->addSeriesColumn($monthName, false, $order->count);
        }

        /*$ordersByMonth
            ->setXAxisCategories(['Mes'])
        ;*/

        return $ordersByMonth;
    }

    /**
     * Sets a column chart model of order amounts by month
     *
     * @return ColumnChartModel
     */
    protected function amountByMonthsChart()
    {
        // Get Orders (orders amount by month chart)
        $amountByMonthQuery = Order::select(DB::raw('SUM(order_lines.net_amount) as total, MONTH(orders.created_at) as month'))
            ->leftJoin('order_lines', 'order_lines.order_id', '=', 'orders.id')
            ->where('orders.customer_id', auth()->user()->customer_id)
            ->where('orders.order_number', '!=' ,null);

        // Add filtering values
        if ($this->search['costCenterId']) {
            $amountByMonthQuery->where('orders.cost_center_id', $this->search['costCenterId']);
        }

        if ($this->search['providerId']) {
            $amountByMonthQuery->where('orders.provider_id', $this->search['providerId']);
        }

        if ($this->search['from']) {
            $amountByMonthQuery->whereDate('orders.created_at', '>=', $this->search['from']);
        }

        if ($this->search['to']) {
            $amountByMonthQuery->whereDate('orders.created_at', '<=', $this->search['to']);
        }

        // Get the results
        $orders = $amountByMonthQuery->groupBy('month')
            ->orderBy('month','asc')
            ->get();

        // New orders amounts by month chart
        $amountByMonth =
            (new ColumnChartModel())
                ->setTitle('Importe por mes')
                ->setAnimated(true)
//                ->withDataLabels()
                ->multiColumn()
                ->setXAxisCategories(['Mes'])
                ->setXAxisVisible(false)
//                ->setLegendVisibility(false)
                ->setColors($this->getChartColors());

        // Adding column bars to the chart
        foreach ($orders as $order) {
            $dateObj   = DateTime::createFromFormat('!m', $order->month);
            $monthName = $dateObj->format('F');
            $amountByMonth->addSeriesColumn($monthName, false, number_format($order->total,2,',','.'));
        }

        return $amountByMonth;
    }

    /**
     * Sets a column chart model of order amounts by provider
     *
     * @return ColumnChartModel
     */
    protected function amountByProvidersChart()
    {
        // Get Orders (orders amount by provider)
        $orders = Order::select(DB::raw('SUM(order_lines.net_amount) as total, providers.name'))
            ->join('providers', 'providers.id', '=', 'orders.provider_id')
            ->join('order_lines', 'order_lines.order_id', '=', 'orders.id')
            ->where('orders.customer_id', auth()->user()->customer_id)
            ->where('orders.order_number', '!=', null);


        // Add filtering values
        if ($this->search['costCenterId']) {
            $orders->where('orders.cost_center_id', $this->search['costCenterId']);
        }

        if ($this->search['providerId']) {
            $orders->where('orders.provider_id', $this->search['providerId']);
        }

        if ($this->search['from']) {
            $orders->whereDate('orders.created_at', '>=', $this->search['from']);
        }

        if ($this->search['to']) {
            $orders->whereDate('orders.created_at', '<=', $this->search['to']);
        }

        $orders = $orders->groupBy('providers.name')
            ->having('total', '>', 0)
            ->orderBy('total', 'desc')
            ->take(10)
            ->get();

        // New orders amounts by provider
        $amountByProvider =
            (new ColumnChartModel())
                ->setTitle('Importe por proveedor')
                ->setAnimated(true)
//                ->withDataLabels()
                ->multiColumn()
                ->setXAxisCategories(['Proveedor'])
                ->setXAxisVisible(false)
//                ->setLegendVisibility(false)
                ->setColors($this->getChartColors());

        // Adding column bars to the chart
        foreach ($orders as $order) {
            $amountByProvider->addSeriesColumn((string)$order->name, false, (int)$order->total);
//            $amountByProvider->addSeriesColumn((string)$order->name, false, number_format($order->total,2,',','.'));
        }

        return $amountByProvider;
    }

    /**
     * Sets a column chart model of order amounts by cost center
     *
     * @return ColumnChartModel
     */
    protected function amountByCeCoChart()
    {
        // Get Orders (orders amount by cost center)
        $orders = Order::select(DB::raw('SUM(order_lines.net_amount) as total, cost_centers.name'))
            ->join('cost_centers','cost_centers.id','=','orders.cost_center_id')
            ->join('order_lines','order_lines.order_id','=','orders.id')
            ->where('orders.customer_id', auth()->user()->customer_id)
            ->where('orders.order_number', '!=', null);

        // Add filtering values
        if (!empty($this->search['costCenterId'])) {
            $orders->where('orders.cost_center_id', $this->search['costCenterId']);
        } elseif (!empty($this->search['initialCostCenterId'])) {
            $orders->whereIn('orders.cost_center_id', $this->search['initialCostCenterId']);
        }

        if ($this->search['providerId']) {
            $orders->where('orders.provider_id', $this->search['providerId']);
        }

        if ($this->search['from']) {
            $orders->whereDate('orders.created_at', '>=', $this->search['from']);
        }

        if ($this->search['to']) {
            $orders->whereDate('orders.created_at', '<=', $this->search['to']);
        }

        $orders = $orders->groupBy('cost_centers.name')
            ->orderBy('total','desc')
            ->take(20)
            ->get();

        // New orders amounts by cost center
        $amountByCeCo =
            (new ColumnChartModel())
                ->setTitle('Importe por Centro de Coste')
                ->setAnimated(true)
//                ->withDataLabels()
                ->multiColumn()
                ->setXAxisCategories(['Centro de Coste'])
                ->setXAxisVisible(false)
//                ->setLegendVisibility(false)
                ->setColors($this->getChartColors());

        // Adding column bars to the chart
        foreach ($orders as $order) {
            $amountByCeCo->addSeriesColumn((string)$order->name, false, (int)$order->total);
        }

        return $amountByCeCo;
    }

    /**
     * Sets a column chart model colors
     *
     * @return array
     */
    private function getChartColors()
    {
        return ['rgba(255, 99, 132)',
            'rgba(54, 162, 235)',
            'rgba(255, 206, 86)',
            'rgba(75, 192, 192)',
            'rgba(153, 102, 255)',
            'rgba(255, 159, 64)',
            'rgba(255, 99, 132)',
            'rgba(54, 162, 235)',
            'rgba(255, 206, 86)',
            'rgba(75, 192, 192)',
            'rgba(153, 102, 255)',
            'rgba(255, 159, 64)'];
    }

    /**
     * Updates the model property mapped to custom select component
     *
     * @param $propertyName
     * @param $value
     */
    public function updateModelPropertyComingFromCustomSelect($propertyName, $value) : void
    {
        $this->manageSessionAndFilters($propertyName, $value, true);
        $this->render();
    }

    /**
     * Manages session and filters variables
     *
     * @param $propertyName
     * @param $value
     * @param bool $forceSearchValue
     */
    public function manageSessionAndFilters($propertyName, $value, $forceSearchValue = false) : void
    {
        // Assign value to model property
        if ($propertyName) {
            session()->forget($propertyName);

            // Save the search filters on user session
            if ($value != '') {
                session()->push($propertyName, $value);
                if ($forceSearchValue) {
                    $tmp = explode('.', $propertyName);
                    if (Arr::first($tmp) == 'search') {
                        $this->search[$tmp[1]] = $value;
                    }
                }
            }
        }

        if (empty(session('search'))) {
            session()->forget('search');
        }
    }

    /**
     * Returns the list of users who have created orders
     * @param $costCenters
     * @param bool $extraManagerId
     * @return mixed
     */
    protected function giveMeUsersListFromCostCenter($costCenters, $extraManagerId = false)
    {
        if (!empty($costCenters)) {
            foreach ($costCenters as $costCenter) {
                $costCenter->load('users');
                if (!empty($costCenter->users)) {
                    $this->usersWhoHaveCreatedAnOrder = $costCenter->users->pluck('id')->toArray();
                }

                // Add the specified manager user id
                if ($extraManagerId) {
                    $this->usersWhoHaveCreatedAnOrder[] = $costCenter->{$extraManagerId};
                }
            }
        }

        // Add the current user
        $this->usersWhoHaveCreatedAnOrder[] = auth()->user()->id;

        return $this->usersWhoHaveCreatedAnOrder;
    }
}
