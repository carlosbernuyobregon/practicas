<?php

namespace App\Http\Livewire\Orders;

use App\CostCenter;
use App\Order;
use App\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Livewire\Component;
use Livewire\WithPagination;

class Table extends Component
{
    // ToDo - Search - Fix manager relationship. Total_amount is always null and totalLines() or appended calculated_total are not searchable
    use WithPagination;
    protected $paginationTheme = 'bootstrap';
    public $entries             = 10;
    public $profileFilter       = null;
    public $task                = false;
    public $admin               = false;
    public $sortDirection       = 'desc';
    public $sortBy              = 'created_at';
    public $filtersMode         = false;
    public $draft               = false;
    public $search              = [
        'orderNumber'   => '',
        'company'       => '',
        'vatNumber'     => '',
        'provider'      => '',
        'status'        => '',
        'totalMin'      => '',
        'totalMax'      => '',
        'createdAtFrom' => '',
        'createdAtTo'   => '',
        'dateFrom'      => '',
        'dateTo'        => '',
        'manager'       => '',
    ];

    public function sortBy($field){
        if ($this->sortDirection == 'asc') {
            $this->sortDirection = 'desc';
        } else {
            $this->sortDirection = 'asc';
        }

        $this->sortBy = $field;
    }
    public function mount($task = false, $status = null, $admin = false, $draft = false)
    {
        $this->task = $task;
        $this->profileFilter = $status;
        $this->admin = $admin;
        $this->draft = $draft;
        $this->costCenters = CostCenter::orderBy('external_id','asc')->get();
        // Filters saved on session
        if (session()->has('search')) {
            foreach ($this->search as $key => $filter) {
                if (session()->has('search.'.$key) == $key) {
                    $this->search[$key] = session()->get('search.'.$key)[0];
                }
            }
        }
    }

    /**
     * Runs after any update to the Livewire component's data
     * (Using wire:model, not directly inside PHP)
     *
     * @param string $propertyName
     * @param mixed $value
     * @return void
     */
    public function updated($propertyName, $value)
    {
        session()->forget($propertyName);

        //Save the search filters on user session
        if ($value != '') {
            session()->push($propertyName, $value);
        }

        if (empty(session('search'))) {
            session()->forget('search');
        }
    }

    /**
     * Deletes all the previous saved filters on the user session
     * and reset the filters array $search
     * @var array $search
     *
     * @return void
     */
    public function clearFilters()
    {
        session()->forget('search');
        $this->search = [
            'orderNumber'   => '',
            'company'       => '',
            'vatNumber'     => '',
            'provider'      => '',
            'status'        => '',
            'totalMin'      => '',
            'totalMax'      => '',
            'createdAtFrom' => '',
            'createdAtTo'   => '',
            'dateFrom'      => '',
            'dateTo'        => '',
            'manager'       => '',
        ];
    }

    public function render()
    {
        if($this->draft){
            $order = Order::select('orders.*')
                        ->where('user_id', Auth::id())
                        ->where('status',0);
        }elseif($this->admin){
            $order = Order::where('status','>',0);
        }else{
            $order = Order::select('orders.*')
                ->rightJoin('cost_centers', 'orders.cost_center_id', 'cost_centers.id')
                ->where('orders.status','>',0);
            if (!Auth::user()->hasRole('orders-viewer')) {
                $order->where('orders.user_id', Auth::id());
                if (Auth::user()->hasRole('manager-de-pedidos')) {
                    $order->orWhere(function ($query1) {
                        $query1->where('cost_centers.manager_1_id', Auth::id())
                            ->where('orders.status','>',0);
                    });
                }
                if (Auth::user()->hasRole('direccion-de-pedidos')) {
                    $order->orWhere(function ($query2) {
                        $query2->where('cost_centers.manager_2_id', Auth::id())
                            ->where('orders.status','>',0);
                    });
                }
                if (Auth::user()->hasRole('direccion-general')) {
                    $order->orWhere(function ($query3) {
                        $query3->where('cost_centers.manager_3_id', Auth::id())
                            ->where('orders.status','>',0);
                    });
                }
            }
        }
        /*
        if (false) {
            if (Auth::user()->hasRole('admin')) {
                $order = Order::select('orders.*');
                switch ($this->profileFilter) {
                    case '1':
                        $order->where(function ($query) {
                            $query->where('status', 1);
                        });
                        break;
                    case '2':
                        $order->where('status', 2);
                        break;
                    case '3':
                        $order->where('status', 3);
                        break;
                    case '7':
                        $order->where('status', 7);
                    default:
                        $order->where(function ($query1) {
                            $query1->where('orders.user_id', Auth::id())
                                ->whereIn('orders.status', [0, 7]);
                        });
                        break;
                }
            } else {
                $order = Order::select('orders.*')
                    ->rightJoin('cost_centers', 'orders.cost_center_id', 'cost_centers.id')
                    ->where(function ($query1) {
                        $query1->where('orders.user_id', Auth::id())
                            ->whereIn('orders.status', [0, 7]);
                    });
                if (Auth::user()->hasRole('manager-de-pedidos')) {
                    $order->orWhere(function ($query2) {
                        $query2->where('cost_centers.manager_1_id', Auth::id())
                            ->where('orders.status', 1);
                    });
                }
                if (Auth::user()->hasRole('direccion-de-pedidos')) {
                    $order->orWhere(function ($query3) {
                        $query3->where('cost_centers.manager_2_id', Auth::id())
                            ->where('orders.status', 2);
                    });
                }
                if (Auth::user()->hasRole('direccion-general')) {
                    $order->orWhere(function ($query4) {
                        $query4->where('cost_centers.manager_3_id', Auth::id())
                            ->where('orders.status', 3);
                    });
                }
            }
        } else {
            $order = Order::select('orders.*')->where('user_id', Auth::id());
        }
        if($this->admin){

            $order = Order::where('status','>',1);
        }
        */
        //$order->dd();

        // Apply filters
        $order->tableSearch($this->search);

        return view('livewire.orders.table', [
            'orders' => $order->with('orderLines', 'provider','costCenter')->orderBy($this->sortBy, $this->sortDirection)->simplePaginate($this->entries),
        ]);
    }
    public function confirmDelete($id)
    {
        $order = Order::find($id);
        $order->destroy($id);
        $this->render();
    }
}
