<?php

namespace App\Http\Livewire\Orders;

use App\Company;
use App\Helpers\Helpers;
use App\Library\Constants;
use App\Models\PaPerson;
use App\Models\Provider;
use App\Models\ProviderPaymentCondition;
use App\Models\ProviderPaymentType;
use App\Order;
use App\Country;
use App\OrderLine;
use App\Process;
use App\Rules\ProviderVat;
use App\User;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use Livewire\Component;

use Livewire\WithPagination;
use function GuzzleHttp\json_decode;

class Form extends Component
{
    use WithPagination;

    public $order = null;
    public $task = true;
    public $actions;
    public $newOrder = true;
    public $provider;
    public $editable = true;
    public $providerSearch = '';
    public $providers = [];
    public $orderCompany = 1000;
    public $orderType = null;
    public $deliveryPointOption = null;
    public $providerForm = false;
    public $companies = null;
    public $processInstance = null;
    public $log = [];
    public $deliveryPoints;
    public $countries;
    public $mediators;
    public $uploadFiles = false;
    public $showRgpd = false;
    public $finalizedStatus = [];
    public $orderTypeValue = [
        'K' => 'Gastos Departamento',
        'A' => 'Activos',
        'P' => 'Proyecto'
    ];
    /*public $orderTypeCode = [
        'ceco' => 'A',
        'active' => 'K',
        'project' => 'P'
    ];
    public $orderTypeByCode = [
        'A' => 'ceco',
        'K' => 'active',
        'P' => 'project'
    ];*/

    public $providerPaymentTypes;
    public $providerPaymentTerms;
    public $providerPaymentTypeRequiringFiles = [1,6];

    public $enableComplete = false;

    protected $listeners = [
        'updateSelect'          => 'updateSelect',
        'orderLinesUpdated'     => 'orderLinesUpdated',
        'providerFilesUpdated'  => 'providerFilesUpdated',
        // Custom select implementation. Self method to update values coming from custom select changed event
        'changedCountryCustomSelect'   => 'updateModelPropertyComingFromCustomSelect',
        // Custom select implementation
    ];

    protected $validationAttributes = [
        'provider.rgpd_name'                => 'nombre (rgpd)',
        'provider.rgpd_last_name'           => 'apellidos (rgpd)',
        'provider.rgpd_dni'                 => 'DNI (rgpd)',
        'provider.rgpd_email'               => 'email (rgpd)',
        'provider.rgpd_role'                => 'cargo / posición (rgpd)',
        'provider.rgpd_service_description' => 'servicio a prestar (rgpd)'
    ];

    public function mount($order = null,$editable = true, $actions = false, $processInstance = null, $task = true, $uploadFiles = false)
    {
        $this->order                = new Order();
        $this->order                = $order ?? new Order();
        $this->companies            = Company::where('customer_id',auth()->user()->customer_id)->orderBy('code','asc')->get();
        $this->countries            = Country::orderBy('key_value', 'asc')->withTranslation()->get();
        $this->provider             = new Provider();
        $this->providerPaymentTypes = ProviderPaymentType::all();
        $this->providerPaymentTerms = ProviderPaymentCondition::all();
        $this->finalizedStatus      = [
                                        Constants::ORDER_STATUS_APPROVED,
                                        Constants::ORDER_STATUS_UPDATED_IN_SAP,
                                        Constants::ORDER_STATUS_UPDATED_IN_SAP_SUCCESS,
                                        Constants::ORDER_STATUS_UPDATED_IN_SAP_ERROR
                                    ];
        // Provider defaults
        $this->providerDefaults();
        // Delivery points from config
        $this->deliveryPoints   = explode(',', config('custom.orders-delivery-points'));

        if ($order) {
            $this->orderType    = $order->order_type;
            $this->order        = $order;
            $this->newOrder     = false;
            $this->provider     = $this->order->provider ?? null;
            if (in_array($this->order->delivery_point,$this->deliveryPoints)) {
                $this->deliveryPointOption = $this->order->delivery_point;
            } else {
                $this->deliveryPointOption = 'Otros';
            }
        }
        $this->editable         = $editable;
        $this->processInstance  = $processInstance;
        $this->actions          = $actions;
        $this->task             = $task;
        $this->uploadFiles      = $uploadFiles;
        $this->mediators        = PaPerson::orderBy('name')->get();
        $this->enableComplete();
    }

    public function updated($propertyName)
    {
        $this->validateOnly($propertyName);

        switch ($propertyName) {
            case 'orderType':

                break;
            case 'deliveryPointOption':
                if($this->deliveryPointOption != 'Otros'){
                    $this->order->delivery_point = $this->deliveryPointOption;
                }else{
                    $this->order->delivery_point = '';
                }
                break;
            case 'order.provider_id':

                break;
            case 'providerSearch':
                $this->render();
                break;
            default:
                # code...
                break;
        }
    }

    public function deleteOrder()
    {
        if ($this->order->id != null) {
            $lines = OrderLine::where('order_id',$this->order->id)->delete();
            $order = Order::where('id',$this->order->id)->delete();
        }
        session()->flash('success', 'Pedido eliminado correctamente.');
        return redirect()->route('order.tasks');
    }
    public function saveOrder()
    {
        $this->order->order_type = $this->orderType;

        $this->validate([
            'order.company'             => 'required',
            'order.date'                => 'required',
            'order.delivery_point'      => 'required',
            'order.provider_id'         => 'required',
            'order.order_type'          => 'required',
            'order.invoice_number'      => 'nullable|string',
            'order.invoice_total_net'   => 'nullable|numeric',
            'order.invoice_date'        => 'nullable|date',
            'order.comments'            => 'nullable|string',
        ]);

        if ($this->order->id == null) {
            $this->order->document_type     = config('custom.orders-default-document-type');
            $this->order->user_id           = Auth::id();
            $lastOrder                      = Order::where('order_number', '!=', null)->orderBy('order_number', 'desc')->first();
            $this->order->order_number      = ($lastOrder->order_number ?? 4700000000) + 1;
            $this->order->customer_id       = auth()->user()->customer_id;
//            $this->order->company           = $this->orderCompany; // Why forcing company 1000?
            $this->order->purchasing_group  = config('custom.orders-default-purchasing-group');
            $this->order->buyer             = $this->order->provider->buyer ?? null;
        }

        $this->order->save();
        // Audit message
        if ($this->newOrder) {
            $this->order->audit()->create([
                'user_id'   => auth()->id(),
                'status'    => Constants::ORDER_STATUS_DRAFT,
                'text'      => 'Orden creada'
            ]);
            return redirect(route('order.edit', [
                'order' => $this->order->id
            ]));
        } else {
            $this->order->audit()->create([
                'user_id'   => auth()->id(),
                'status'    => Constants::ORDER_STATUS_DRAFT,
                'text'      => 'Orden actualizada'
            ]);
            $this->render();
        }

        // Reset validation errors
        $this->resetErrorBag();
        $this->resetValidation();

        session()->flash('success', 'Los datos se han actualizado correctamente');

        return true;
    }

    public function saveProvider() : void
    {
        // Form inputs validation
        $this->validateProviderForm();

        list($newVat, $newCommunityVat) = Helpers::VatFormat($this->provider->vat_number, $this->provider->country);

        // Customer id
        $this->provider->customer_id            = auth()->user()->customer_id;
        // Add normalized VATs
        $this->provider->vat_number             = $newVat;
        $this->provider->community_vat_number   = $newCommunityVat;
        // Add creator
        $this->provider->creator_user_id        = auth()->id();
        // RGPD
        $this->provider->rgpd_user_id = ($this->showRgpd) ? auth()->user()->id : null;

        if (!$this->showRgpd) {
            // Clean RGPD input data
            $this->provider->rgpd_name      = null;
            $this->provider->rgpd_last_name = null;
            $this->provider->rgpd_dni       = null;
            $this->provider->rgpd_email     = null;
            $this->provider->rgpd_role      = null;
            $this->provider->rgpd_user_id   = null;
        }

        // Save Provider
        $this->provider->save();

        // Assign provider id to Order
        $this->order->provider_id = (string)$this->provider->id;

        // Audit message
        if ($this->order->id != null) {
            $this->order->audit()->create([
                'user_id'   => auth()->id(),
                'status'    => Constants::ORDER_STATUS_DRAFT,
                'text'      => 'Nuevo Proveedor creado desde el corriente Pedido'
            ]);
        }

        session()->flash('success', 'Los datos se han guardado correctamente');
    }

    public function providerFilesUpdated()
    {
        if ($this->provider) {
            try {
                $this->provider->fresh();
            } catch (\Throwable $th) {
                // not provider selected
            }
        }
    }

    public function initNewProviderWorkflow($id)
    {
        // Form inputs validation
        $this->validateProviderForm();

        $process = new Process();
        $dataFileds = [
            'variables' => [
                'portalInstance' => [
                    'value' => config('app.url'),
                    'type' => 'string'
                ],
                'processId' => [
                    'value' => $id,
                    'type' => 'string'
                ],
                'varAssignedUser' => [
                    'value' => (string)User::where('email', config('custom.provider-process-validator-email'))->value('id'),
                    'type' => 'string'
                ]

            ],
            'businessKey' => '[' + $this->provider->provider_number + ']' + 'Rubio Provider'
            //Provider number en lugar de solo "Rubio"
        ];

//        $this->provider = new Provider(); // This cause provider data name losing once it was created and document uplaoded
        $this->providerForm = false;
        try {
            $value = $process->submitProcess('alta_proveedores',$dataFileds);
            session()->flash('success', 'Proceso de alta de proveedor iniciado.');

            $this->hideNewProviderModal();
        } catch (\Throwable $th) {
            session()->flash('success', 'No se ha podido iniciar el proceso de alta de proveedor.');
        }

    }

    /**
     * Shows bulk action modal
     */
    public function showNewProviderModal() : void
    {
        $this->dispatchBrowserEvent('showNewProviderModal');
    }

    /**
     * Hides bulk action modal
     */
    public function hideNewProviderModal() : void
    {
        $this->dispatchBrowserEvent('hideNewProviderModal');
    }

    public function clearProviderModal() : void
    {
        $this->providerForm = false;
        $this->resetValidation();
        $this->order->provider_id = null;
        $this->unsetProvider();
    }

    public function newProvider()
    {
        $this->provider->company = $this->order->company ?? null;
        $this->showNewProviderModal();
        $this->providerSearch = '';
//        $this->providerForm = true;
    }

    public function rejectProvider()
    {
        $this->providerForm = false;
        $this->provider = new Provider();
        // Audit message
        if($this->order->id != null){
            /*$this->order->audit()->create([
                'user_id'   => auth()->id(),
                'status'    => Constants::ORDER_STATUS_DRAFT,
                'text'      => 'Proveedor descartado'
            ]);*/
        }
        $this->clearProviderModal();
    }

    public function setProvider($id)
    {
        $this->provider = Provider::find($id);
        $this->order->provider_id = $id;
        $this->providerSearch = '';
        // Audit message
        if($this->order->id != null){
            /*$this->order->audit()->create([
                'user_id'   => auth()->id(),
                'status'    => Constants::ORDER_STATUS_DRAFT,
                'text'      => 'New provider selected'
            ]);*/
        }
        $this->render();
    }

    public function unsetProvider()
    {
        $this->provider = new Provider();
        $this->providerDefaults();
        $this->order->provider_id = null;
        $this->order->save();
        if($this->order->id != null){
            /*$this->order->audit()->create([
                'user_id' => auth()->id(),
                'status' => Constants::ORDER_STATUS_DRAFT,
                'text' => 'Provider unset'
            ]);*/
        }
        $this->render();
    }

    public function enableComplete()
    {
        $lines = OrderLine::where('order_id',$this->order->id)->count();
        if($lines>0){
            $this->enableComplete = true;
        }else{
            $this->enableComplete = false;
        }
    }

    public function orderLinesUpdated()
    {
        $this->order->refresh();
        $this->enableComplete();
        $this->validateBudget();
        $this->render();
    }

    public function initProcess()
    {
        $process = new Process();

        $dataFileds = [
            'variables' => [
                'portalInstance' => [
                    'value' => config('app.url'),
                    'type' => 'string'
                ],
                'processId' => [
                    'value' => (string)$this->order->id,
                    'type' => 'string'
                ],
                'varActionsDecision' => [
                    'value' => 'front',
                    'type' => 'string'
                ],
                'varPresupuesto' => [
                    'value' => 'true',
                    'type' => 'boolean'
                ],
                'varAssignedUser' => [
                    'value' => auth()->id(),
                    'type' => 'string'
                ]
            ],
            'businessKey' => '[' + $this->order->order_number + "]" + 'Rubio Order'
            // order number en lugar de "Rubio" como business key
        ];

        try {
            $value = $process->submitProcess('solicitud_pedidos_no_stock',$dataFileds);

            $this->order->bpm_id = json_decode($value['body'])->definitionId;
            $this->order->save();
            // Audit message
            $this->order->audit()->create([
                'user_id'   => auth()->id(),
                'status'    => Constants::ORDER_STATUS_DRAFT,
                'text'      => 'Proceso de solicitud de Pedido iniciado'
            ]);
            session()->flash('success', 'Pedido creado correctamente con número: ' . $this->order->order_number);
            return redirect()->route('order.tasks');

        } catch (\Throwable $th) {
            session()->flash('alert', 'El pedido no se ha podido confirmar correctamente, vuelva a intentarlo más tarde.');
            Log::error($th->getMessage(), [
                'class'     => __CLASS__,
                'method'    => __METHOD__,
            ]);
        }
    }

    public function render()
    {
        if ($this->providerSearch != '') {
            $providersList = Provider::select('id','external_id','name','vat_number','community_vat_number','address','city','zip_code')
                ->where('customer_id', auth()->user()->customer_id);

            if ($this->order->company) {
                $providersList = $providersList->where('company', $this->order->company);
            }

            $providersList = $providersList->where(function($query) {
                    $query->where('external_id','LIKE','%'. $this->providerSearch .'%')
                        ->orWhere('vat_number','like','%'. $this->providerSearch .'%')
                        ->orWhere('community_vat_number','like','%'. $this->providerSearch .'%')
                        ->orWhere(function($subQuery){
                            $subQuery
                                ->where('name','LIKE','%'. $this->providerSearch .'%')
                                ->where('name', 'NOT LIKE', '@%');
                        });
                })
                ->orderBy('name', 'asc')
                ->simplePaginate(10);
        }
        return view('livewire.orders.form',[
            'providersList' => $providersList ?? []
        ]);
    }

    public function rules()
    {
        $nullableOrRequiredRgpd = 'nullable|';

        if ($this->showRgpd) {
            $nullableOrRequiredRgpd = 'required|';
        }

        return [
            'order.company'                     => 'string',
            'order.purchasing_group'            => 'string',
            'order.buyer'                       => 'string',
            'order.date'                        => 'date',
            'order.deny_reason'                 => 'string',
            'order.delivery_point'              => 'string',
            'order.provider_id'                 => 'string|required',
            'order.description'                 => 'string',
            'order.comments'                    => 'nullable|string',
            'provider.company'                  => 'string|required',
            'provider.name'                     => 'string|required',
            'provider.email'                    => 'nullable',
            'provider.email_orders'             => 'nullable',
            'provider.vat_number'               => 'string|required',
            'provider.payment_condition'        => 'string|required',
            'provider.payment_type'             => 'string|required',
            'provider.address'                  => 'string|required',
            'provider.city'                     => 'string|required',
            'provider.country'                  => 'string|required',
            'provider.zip_code'                 => 'string|required',
            'provider.phone_number'             => 'numeric|required|digits_between:6,20',
            'provider.mediator'                 => 'string|required',
            'provider.receiving_order_copy'     => 'nullable',
            'provider.iban'                     => 'nullable|regex:/^([a-zA-Z]{2}[0-9]{22,22})$/',
            'provider.rgpd_name'                => $nullableOrRequiredRgpd . 'string|max:50',
            'provider.rgpd_last_name'           => $nullableOrRequiredRgpd . 'string|max:50',
            'provider.rgpd_dni'                 => $nullableOrRequiredRgpd . 'string|max:15',
            'provider.rgpd_email'               => $nullableOrRequiredRgpd . 'email',
            'provider.rgpd_role'                => $nullableOrRequiredRgpd . 'string|max:50',
            'provider.rgpd_service_description' => $nullableOrRequiredRgpd . 'string',
            'order.invoice_number'              => 'nullable|string',
            'order.invoice_total_net'           => 'nullable|numeric',
            'order.invoice_date'                => 'nullable|date',
            'order.receiving_order_copy'        => 'nullable',
        ];
    }

    public function validateProviderForm()
    {
        // Form inputs validation
        $nullableOrRequiredRgpd = 'nullable|';

        if ($this->showRgpd) {
            $nullableOrRequiredRgpd = 'required|';
        }

        $this->validate([
            'provider.company'              => 'string|required',
            'provider.name'                 => 'string|required',
            'provider.vat_number'           => [
                'required', 'string', 'between:6,12', 'regex:/(^[A-Za-z0-9]+$)+/', new ProviderVat(
                    $this->provider->company, $this->provider->id
                )
            ],
            'provider.payment_condition'        => 'string|required',
            'provider.payment_type'             => 'string|required',
            'provider.address'                  => 'string|required',
            'provider.city'                     => 'string|required',
            'provider.email'                    => 'nullable',
            'provider.email_orders'             => 'nullable',
            'provider.country'                  => 'string|required',
            'provider.zip_code'                 => 'string|required',
            'provider.phone_number'             => 'numeric|required|digits_between:6,20',
            'provider.mediator'                 => 'string|required',
            'provider.receiving_order_copy'     => 'nullable',
            'provider.iban'                     => 'nullable|regex:/^([a-zA-Z]{2}[0-9]{22,22})$/',
            'provider.rgpd_name'                => $nullableOrRequiredRgpd . 'string|max:50',
            'provider.rgpd_last_name'           => $nullableOrRequiredRgpd . 'string|max:50',
            'provider.rgpd_dni'                 => $nullableOrRequiredRgpd . 'string|max:15',
            'provider.rgpd_email'               => $nullableOrRequiredRgpd . 'email',
            'provider.rgpd_role'                => $nullableOrRequiredRgpd . 'string|max:50',
            'provider.rgpd_service_description' => $nullableOrRequiredRgpd . 'string',
        ]);
    }

    public function providerDefaults()
    {
        $defaultType                        = $this->providerPaymentTypes->where('is_default')->first();
        $defaultTypeCondition               = $this->providerPaymentTerms->where('is_default')->first();
        $this->provider->payment_type       = $defaultType->code ?? false;
        $this->provider->payment_condition  = $defaultTypeCondition->code ?? false;
        $this->provider->country            = 'ES';
//        $this->provider->mediator           = auth()->user()->id;
    }
    public function generatePdf()
    {
        $this->order->generateOrderDocument();
    }
    public function validateBudget()
    {
        if(false){
            session()->flash('alert', 'El pedido excede el presupuesto asignado a este Centro de Costes.');
        }

    }
}
