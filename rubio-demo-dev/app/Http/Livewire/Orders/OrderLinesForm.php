<?php

namespace App\Http\Livewire\Orders;

use App\Audit;
use App\Budget;
use App\Company;
use App\CostCenter;
use App\Country;
use App\Helpers\Helpers;
use App\Library\Constants;
use App\MesureUnit;
use App\Models\BudgetAgrupation;
use App\Models\PaBusinessArea;
use App\Models\PaClient;
use App\Models\PaDistributionChannel;
use App\Models\PaFamily;
use App\Models\PaOrder;
use App\Models\PaPerson;
use App\Order;
use App\OrderLine;
use App\Rules\OrderDeliveryDateDateGreaterThanOrderDate;
use App\TaxType;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use Livewire\Commands\CopyCommand;
use Livewire\Component;
use Livewire\WithPagination;

class OrderLinesForm extends Component
{
    use WithPagination;

    public $order;
    public $taxTypes;
    public $mesureUnits;
    public $currencies;
    public $defaultCurrency;
    public $defaultMesureUnitId;
    public $orderId;
    public $orderLine;
    public $currentBudgetsVersion;
    public $addLineDisabled         = false;
    public $orderLines              = [];
    public $editable                = true;
    public $selectedCostCenter      = null;
    public $costCenters             = [];
    public $costCenter              = null;
    public $pepElements             = [];
    public $showPaObjectData        = false;
    public $paBusinessAreas         = [];
    public $paClients               = [];
    public $paDistributionChannels  = [];
    public $paFamilies              = [];
    public $paOrders                = [];
    public $paPersons               = [];
    public $countries               = [];
    public $budgetAgrupations       = [];
    public $budgetAgrupationId      = '';
    public $company                 = null;

    // Custom select implementation
    /*public $customSelectModelPropertyNames = [
        'measureUnitIdCustomSelect' => 'mesure_unit_id',
        'currencyCustomSelect'      => 'currency'
    ];*/
    // Custom select implementation

    protected $listeners = [
        // Custom select implementation. Self method to update values coming from custom select changed event
//        'changedCustomSelect' => 'updateModelPropertyComingFromCustomSelect',
        // Custom select implementation
        'resetPADataListener' =>  'resetPAData'
    ];

    public function mount($orderId = null, $editable = true)
    {
        // Disable add line button
//        $this->checkAddLineButton();

        $this->orderId      = $orderId;

        // Order
        $this->order = Order::find($this->orderId);
        // Company
        if($this->orderId != null){
            $this->company = Company::where('code',$this->order->company)->first();
            /*
            $this->costCenters = CostCenter::where('customer_id',auth()->user()->customer_id)
                ->where('company',$this->order->company)
                ->with(['users' => function($query){
                    $query->where('user_id',auth()->user()->id);
                }]);
            */
            $this->costCenters  = auth()->user()->costCenters()->where('company',$this->order->company)->get();
        }


        //$this->costCenters  = auth()->user()->costCenters()->where('company',$this->order->company);
        // Setear por default a la companyId que se ha seleccionado en la cabecera
        $this->costCenter   = auth()->user()->costCenters->first();

        $this->mesureUnits = MesureUnit::select('id', 'name', 'unit')
            ->orderBy('name', 'asc')
            ->get();

        $this->currencies           = explode(',',config('custom.currencies-short-list'));
        $this->defaultCurrency      = 'EUR';
        $defaultMeasureUnit         = MesureUnit::where('name', 'unit')->first();
        $this->defaultMesureUnitId  = $defaultMeasureUnit->id ?? null;

        $lines = OrderLine::where('order_id',$orderId)->get();

        if (count($lines) > 0) {
            $this->costCenters = CostCenter::where('id',$lines->first()->cost_center_id)->get();
        }

        $this->orderLine = new OrderLine();
        $this->orderLine['mesure_unit_id']  = $this->defaultMesureUnitId;
        $this->orderLine['currency']        = $this->defaultCurrency;
        $this->orderLine['customer_id']     = auth()->user()->customer_id;
        $this->editable = $editable;




        // PA object data
        $this->countries                = Country::orderBy('key_value', 'asc')
                                                ->withTranslation()->get();


        // Last budgets version
        $this->currentBudgetsVersion    = Helpers::getLastVersion(Budget::class);
        if($this->orderId != null){
            $this->paBusinessAreas          = PaBusinessArea::select('area','name')->orderBy('name', 'asc')->get();
            $this->paClients                = PaClient::select('client','name')->orderBy('name', 'asc')->get();
            $this->paDistributionChannels   = PaDistributionChannel::select('channel','name')->orderBy('name', 'asc')->get();
            $this->paFamilies               = PaFamily::select('family','name')->orderBy('name', 'asc')->get();
            $this->paOrders                 = PaOrder::select('order','name')->orderBy('name', 'asc')->get();
            $this->paPersons                = PaPerson::select('person','name')->orderBy('name', 'asc')->get();
            /*
            if(auth()->user()->hasRole('marketing')){
                $this->budgetAgrupations = BudgetAgrupation::select('budget_agrupations.id','budget_agrupations.name')
                    ->join('budgets','budget_agrupations.id','budgets.budget_agrupation_id')
                    ->where('budget_agrupations.customer_id', auth()->user()->customer_id)
                    ->where('budget_agrupations.company_id',(string)$this->company->id)
                    ->distinct('budget_agrupations.id')
                    ->orderBy('budget_agrupations.name', 'asc')
                    ->get();
            }
            */
        }

    }

    public function addLine()
    {
        // Disable add new line button
//        $this->addLineDisabled = true;

        $this->validate();

        // Disable add line button
//        $this->checkAddLineButton();

        $providerTaxData = Helpers::giveMeTheTax('21', $this->order->provider->country ?? 'ES');

        $lastLine                       = OrderLine::where('order_id', $this->orderId)->orderBy('position', 'desc')->value('position');
        $this->orderLine->tax_type_id   = (!empty($providerTaxData[0]))
            ? $providerTaxData[0] : TaxType::where('type','S3')->value('id');
        $this->orderLine->tax_type      = (!empty($providerTaxData[1]))
            ? $providerTaxData[1] : 'S3';
        $this->orderLine->net_amount    = $this->floatvalue($this->orderLine->net_amount);
        $this->orderLine->qty           = $this->floatvalue($this->orderLine->qty);

        if ($this->orderLine->id == null) {
            $this->orderLine->customer_id   = auth()->user()->customer_id;
            $this->orderLine->order_id      = $this->orderId;
            $this->orderLine->position      = ($this->orderLine->position != null) ? $this->orderLine->position : ($lastLine ?? 0) + 10;
            $this->orderLine->material_id   = $this->orderId;
        }

        // Assign an Accounting Account through the budget
        if ($this->orderLine->budget_id) {
            $budget = Budget::with(['accountingAccount'])->find($this->orderLine->budget_id);
            $this->orderLine->accounting_account_id = $budget->accountingAccount->id ?? null;
        }

        if ($this->orderLine->id == null) {
            $this->orderLine->save();
            session()->flash('success', 'Línea de pedido generada correctamente.');
        } else {
            $this->orderLine->save();
            session()->flash('success', 'Línea de pedido actualizada correctamente.');
        }

        $this->order->cost_center_id        = $this->orderLine->cost_center_id;
        $this->order->save();

        if ($this->selectedCostCenter == null) {
            $this->selectedCostCenter   = $this->orderLine->cost_center_id;
            $this->costCenters          = CostCenter::where('id',$this->orderLine->cost_center_id)->get();
        }

//        $this->emit('resetCustomSelect');

        $this->emitUp('orderLinesUpdated');
        $this->resetForm();

        // Reset validation errors
        $this->resetErrorBag();
//        $this->resetValidation();
        // Disable add line button
//        $this->checkAddLineButton();

        // Audit
//        $this->doAudit(false, 'New line');

//        $this->emit('refreshCustomSelect');
        $this->orderLine = new OrderLine();
        $this->orderLine['mesure_unit_id']  = $this->defaultMesureUnitId;
        $this->orderLine['currency']        = $this->defaultCurrency;
    }

    public function cancelLine()
    {
        // Disable add line button
//        $this->checkAddLineButton();
        $this->resetForm();
        // Custom select event
//        $this->emit('resetCustomSelect');
        // Custom select event

        // Audit
//        $this->doAudit(false, 'Cancelled line');

        // Reset validation errors
        $this->resetErrorBag();

//        $this->resetValidation();
    }

    public function deleteLine($id)
    {
        $orderLine = OrderLine::find($id);
        $this->emitUp('orderLinesUpdated');
        $orderLine->destroy($id);
        // Reset validation errors
        $this->resetErrorBag();
//        $this->resetValidation();

        // Audit
//        $this->doAudit(false, 'Deleted line');

        session()->flash('success', 'Línea eliminada correctamente.');
    }

    public function editLine($id)
    {
        $this->orderLine = OrderLine::find($id);
        $this->budgetAgrupationId = $this->orderLine->budget->budget_agrupation_id;
        $this->pepElements = Budget::select('budgets.id','budgets.name')
            ->leftJoin('budget_cost_center','budgets.id','budget_cost_center.budget_id')
            ->where('budgets.customer_id', auth()->user()->customer_id)
            //->where('cost_center_id',$this->orderLine->cost_center_id)
            ->where('budget_cost_center.cost_center_id',$this->orderLine->cost_center_id)
            ->where('budgets.non_stock',true);
        if($this->budgetAgrupationId){
            $this->pepElements->where('budgets.budget_agrupation_id',$this->budgetAgrupationId);
        }
        if ($this->currentBudgetsVersion) {
            $this->pepElements->where('budgets.version', $this->currentBudgetsVersion);
        }

        $this->pepElements = $this->pepElements
            ->distinct('budgets.id')
            ->orderBy('budgets.name', 'asc')
            ->get();

//        dd($this->orderLine);

        // Reset validation errors
        $this->resetErrorBag();
//        $this->resetValidation();

//        $this->checkAddLineButton();

        // Audit
        //$this->doAudit(false, 'Edited line');

        $this->render();
    }

    public function resetForm() : void
    {
        $this->orderLine = new OrderLine();
        $this->budgetAgrupationId = '';
    }

    public function updated($propertyName)
    {
        $this->validateOnly($propertyName);
//        $this->checkAddLineButton();


        if ($propertyName == 'orderLine.cost_center_id') {
            if(auth()->user()->hasRole('marketing')){
                if($this->budgetAgrupationId != ''){
                    if($this->orderLine->cost_center_id != ''){

                        $this->pepElements = Budget::select('budgets.id','budgets.name')
                        ->leftJoin('budget_cost_center','budgets.id','budget_cost_center.budget_id')
                        ->where('budgets.customer_id', auth()->user()->customer_id)
                        ->where('budget_cost_center.cost_center_id',$this->orderLine->cost_center_id)
                        ->where('budgets.non_stock',true);
                        if($this->budgetAgrupationId){

                            //$others = BudgetAgrupation::where('id',$this->budgetAgrupationId)->value('others');
                            if($this->budgetAgrupationId != 'others'){
                                $this->pepElements->where('budgets.budget_agrupation_id',$this->budgetAgrupationId);
                            }else{
                                $this->pepElements->where('budgets.budget_agrupation_id',null);
                            }
                        }
                        if ($this->currentBudgetsVersion) {
                            $this->pepElements->where('budgets.version', $this->currentBudgetsVersion);
                        }

                        $this->pepElements = $this->pepElements
                            ->distinct('budgets.id')
                            ->orderBy('budgets.name', 'asc')
                            ->get();
                    }else{
                        $this->budgetAgrupations = [];
                        $this->pepElements = [];
                    }
                }else{
                    $this->budgetAgrupations = BudgetAgrupation::select('budget_agrupations.id','budget_agrupations.name')
                                                ->join('budgets','budget_agrupations.id','budgets.budget_agrupation_id')
                                                ->join('budget_cost_center','budgets.id','budget_cost_center.budget_id')
                                                ->where('budget_agrupations.customer_id', auth()->user()->customer_id)
                                                ->where('budget_agrupations.company_id',(string)$this->company->id)
                                                ->where('budget_cost_center.cost_center_id',(string)$this->orderLine->cost_center_id)
                                                ->distinct('budget_agrupations.id')
                                                ->orderBy('budget_agrupations.name', 'asc')
                                                ->get();
                }
            }else{
                if($this->orderLine->cost_center_id != ''){

                    $this->pepElements = Budget::select('budgets.id','budgets.name')
                        ->leftJoin('budget_cost_center','budgets.id','budget_cost_center.budget_id')
                        ->where('budgets.customer_id', auth()->user()->customer_id)
                        //->where('cost_center_id',$this->orderLine->cost_center_id)

                        ->where('budget_cost_center.cost_center_id',$this->orderLine->cost_center_id)
                        ->where('budgets.non_stock',true);
                    if ($this->currentBudgetsVersion) {
                        $this->pepElements->where('budgets.version', $this->currentBudgetsVersion);
                    }

                    $this->pepElements = $this->pepElements
                        ->distinct('budgets.id')
                        ->orderBy('budgets.name', 'asc')
                        ->get();
                }else{
                    $this->pepElements = [];
                }


            }
        }
        if($propertyName == 'budgetAgrupationId'){
            if($this->orderLine->cost_center_id != null){
                $this->pepElements = Budget::select('budgets.id','budgets.name')
                    ->leftJoin('budget_cost_center','budgets.id','budget_cost_center.budget_id')
                    ->where('budgets.customer_id', auth()->user()->customer_id)
                    //->where('cost_center_id',$this->orderLine->cost_center_id)
                    ->where('budget_cost_center.cost_center_id',$this->orderLine->cost_center_id)
                    ->where('budgets.non_stock',true);
                    if($this->budgetAgrupationId){
                        //$others = BudgetAgrupation::where('id',$this->budgetAgrupationId)->value('others');
                        if($this->budgetAgrupationId != 'others'){
                            $this->pepElements->where('budgets.budget_agrupation_id',$this->budgetAgrupationId);
                        }else{
                            $this->pepElements->where('budgets.budget_agrupation_id',null);
                        }
                    }
                    if ($this->currentBudgetsVersion) {
                        $this->pepElements->where('budgets.version', $this->currentBudgetsVersion);
                    }

                    $this->pepElements = $this->pepElements
                        ->distinct('budgets.id')
                        ->orderBy('budgets.name', 'asc')
                        ->get();
            }else{
                $this->pepElements = [];
            }
        }

        // Audit
//        $this->doAudit(false, 'Updated line');
    }

    public function render()
    {
        $lines = OrderLine::where('order_id',$this->orderId)
            ->with(['costCenter','mesureUnit'])
            ->orderBy('position','asc')
            ->simplePaginate(10);

        return view('livewire.orders.order-lines-form',[
            'lines' => $lines ?? []
        ]);
    }

    /**
     * Reset PA Data
     */
    public function resetPAData()
    {

        $this->orderLine->pa_business_area          = null;
        $this->orderLine->pa_client                 = null;
        $this->orderLine->pa_distribution_channel   = null;
        $this->orderLine->pa_family                 = null;
        $this->orderLine->pa_order                  = null;
        $this->orderLine->pa_person                 = null;
        $this->orderLine->pa_country                = null;
    }

    /**
     * Toggle add new line button
     */
    public function checkAddLineButton() : void
    {
        if (
            !empty($this->orderLine->description) &&
            !empty($this->orderLine->delivery_date) &&
            !empty($this->orderLine->cost_center_id) &&
            !empty($this->orderLine->budget_id) &&
            !empty($this->orderLine->qty) &&
            !empty($this->orderLine->net_amount) &&
            !empty($this->orderLine->mesure_unit_id) &&
            !empty($this->orderLine->currency)
        ) {
            $this->addLineDisabled = false;
        } else {
            $this->addLineDisabled = true;
        }
    }

    /**
     * Gets the validation rules
     * @return array
     */
    public function rules()
    {
        // Default delivery date rule
        $deliveryRule   = ['required', 'date'];

        if ($this->order) {
            $deliveryRule = ['required', 'date', new OrderDeliveryDateDateGreaterThanOrderDate($this->order)];
        }

        // Optional inputs validation
        $nullableOrRequiredPAObject = 'nullable';

        if ($this->showPaObjectData) {
//            $nullableOrRequiredPAObject = 'required';
        }

        return [
            'orderLine.description'             => 'required|string|max:40',
            'orderLine.delivery_date'           => $deliveryRule,
            'orderLine.cost_center_id'          => 'required|string',
            'orderLine.budget_id'               => 'required|uuid',
            'orderLine.qty'                     => 'required|numeric',
            'orderLine.net_amount'              => 'required|numeric',
            'orderLine.mesure_unit_id'          => 'required|uuid',
            'orderLine.currency'                => 'required|string',
            'orderLine.pa_business_area'        => $nullableOrRequiredPAObject,
            'orderLine.pa_client'               => $nullableOrRequiredPAObject,
            'orderLine.pa_distribution_channel' => $nullableOrRequiredPAObject,
            'orderLine.pa_family'               => $nullableOrRequiredPAObject,
            'orderLine.pa_order'                => $nullableOrRequiredPAObject,
            'orderLine.pa_person'               => $nullableOrRequiredPAObject,
            'orderLine.pa_country'              => $nullableOrRequiredPAObject,
        ];
    }

    public function floatvalue($val){
        $val = str_replace(",",".",$val);
        $val = preg_replace('/\.(?=.*\.)/', '', $val);
        return floatval($val);
    }

    /**
     * Creates an audit record for the Order
     *
     * @param bool $status
     * @param bool $text
     */
    private function doAudit($status = false, $text = false) : void
    {
        // Audit message
        $data =  [
            'auditable_id'      => $this->orderId,
            'auditable_type'    => Order::class,
            'user_id'           => auth()->id(),
            'text'              => $text ?? '',
            'status'            => $status ?? Constants::ORDER_STATUS_DRAFT
        ];
        try {
            Audit::create($data);
        } catch (\PDOException $e) {
            Log::error($e->getMessage(), [
                'class'     => __CLASS__,
                'method'    => __METHOD__,
            ]);
        }
    }

    /**
     * Toggles PA object data form
     *
     */
    public function togglePaObjectData()
    {
        $this->showPaObjectData = !$this->showPaObjectData;
    }

    /**
     * Updates the model property mapped to custom select component
     *
     * @param $propertyName
     * @param $value
     */
    public function updateModelPropertyComingFromCustomSelect($propertyName, $value) : void
    {
        // Assign value to model property
//        $this->orderLine->{$propertyName} = $value;
        // Enable add line button
//        $this->checkAddLineButton();
    }

    /**
     * Emits custom select event when editing data
     */
    public function emitCustomSelectEditingEvent() : void
    {
        // Defaults
        $emitData = [];

        // Build up data to be sent, including each component implementation data
        /*foreach ($this->customSelectModelPropertyNames as $customSelectId => $modelPropertyName) {
            $emitData[$customSelectId] = [
                'field' => $modelPropertyName,
                'value' => $this->orderLine->{$modelPropertyName}
            ];

        }*/

        // Emit custom select editing event
//        $this->emit('editingCustomSelect', $emitData);
    }
}
