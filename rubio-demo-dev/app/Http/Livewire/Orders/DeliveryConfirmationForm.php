<?php

namespace App\Http\Livewire\Orders;

use App\Delivery;
use App\Library\Constants;
use Livewire\Component;
use Livewire\WithPagination;

class DeliveryConfirmationForm extends Component
{
    use WithPagination;
    public $order;
    public $editable = true;
    public $delivery = null;
    //public $deliveries = [];
    public $addLineDisabled = true;
    public $maxQty = 0;
    public $sapSuccessStatus = null;

    protected function rules()
    {
        return [
            'delivery.order_line_id'    => 'required',
            'delivery.qty'              => ['required', 'numeric', 'max: ' . $this->maxQty, 'gt:0'],
            'delivery.delivered_at'     => 'required'
        ];
    }
    public function mount($order, $editable)
    {
        $this->order        = $order;
        $this->delivery     = new Delivery();
        $this->editable     = $editable;
        $this->sapSuccessStatus = Constants::DELIVERY_STATUS_UPDATED_IN_SAP_SUCCESS;

    }
    public function updated($propertyName)
    {
        $this->validateOnly($propertyName);
        if ($propertyName == 'delivery.order_line_id') {
            $totalDelivered = Delivery::where('order_line_id', $this->delivery->order_line_id)->sum('qty');
            $this->maxQty   = $this->delivery->orderLine->qty + config('custom.orders-delivery-qty-variation') - $totalDelivered;
        }

        $this->addLineDisabled = false;
    }
    public function render()
    {
        $deliveries = Delivery::select('deliveries.*')
            ->join('order_lines','order_lines.id','deliveries.order_line_id')
            ->where('order_lines.order_id',(string)$this->order->id);
        return view('livewire.orders.delivery-confirmation-form',[
            'deliveries' => $deliveries->simplePaginate(10)
        ]);
    }
    public function addLine()
    {
        $this->validate();
        if($this->delivery->position == null){
            $lastLine                       = Delivery::select('deliveries.*')
                                                ->join('order_lines','order_lines.id','deliveries.order_line_id')
                                                ->where('order_lines.order_id',(string)$this->order->id)
                                                ->orderBy('deliveries.position','asc')
                                                ->value('position');
            $this->delivery->position       = $lastLine ? $lastLine + 10 : 10;
        }
        $this->delivery->save();
        $this->delivery = new Delivery();
        $this->addLineDisabled = true;
        $this->resetValidation();
        $this->render();
    }
    public function cancelLine()
    {
        $this->resetValidation();
        $this->delivery = new Delivery();
        $this->addLineDisabled = true;

    }
    public function editLine($id)
    {
        $this->delivery = Delivery::find($id);
    }
    public function deleteLine($id)
    {
        $this->delivery = Delivery::find($id);
        $this->delivery->delete();
        $this->delivery = new Delivery();
        $this->render();
    }
}
