<?php

namespace App\Http\Livewire\Orders\Component;

use App\Supplier;
use Livewire\Component;

/**
 * Class SuppliersSelect
 * @package App\Http\Livewire\Orders\Component
 *
 * @deprecated
 */
class SuppliersSelect extends Component
{
    public $suppliers;
    public $selectedSupplier;
    public function mount($supplier = null)
    {
        $this->suppliers = Supplier::where('customer_id', auth()->user()->customer_id)->orderBy('name', 'asc')->get();
        if($supplier != null){
            $this->selectedSupplier = Supplier::find($supplier);
        }
    }
    public function updated()
    {
        dd($this->selectedSupplier);
        $this->emitUp('selectedSupplier',$this->selectedSupplier);
    }
    public function render()
    {
        return view('livewire.orders.component.suppliers-select');
    }
}
