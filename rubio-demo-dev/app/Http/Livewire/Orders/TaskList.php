<?php

namespace App\Http\Livewire\Orders;

use App\Order;
use App\Process;
use Livewire\Component;

class TaskList extends Component
{
    public $entries = 10;
    public function render()
    {
        $process = new Process();
        $instancesTasks = $process->getUserTasksCamunda();
        //dd($instancesTasks);
        $tasks = [];
        foreach($instancesTasks as $item){
            //$variables = $process->getTaskVariables($item->id);
            //$variables= $process->getFormVariables($item->id);
            //dd($variables);
            try {
                $variables= $process->getFormVariables($item->id);
                //$order = Order::find($variables['processId']);
                //dd($actions);
                try {
                    $data = [
                        'actions' => $variables['actions'],
                        'processId' => $variables['processId'],
                        'data' => Order::where('id',$variables['processId'])->with(['user','provider'])->first(),
                        'formUrl' => str_replace('{taskId}',$item->id,$item->formKey),
                        'taskId' => $item->id
                    ];
                } catch (\Throwable $th) {
                    //dump('Error generación array: ' . $item->id);
                }
                if($data['data']){
                    $tasks[] = $data;
                }
            } catch (\Throwable $th) {
                //dump('Error captura variables: ' . $item->id);
            }
        }
        //dd($tasks);
        return view('livewire.orders.task-list',[
            'tasks' => $tasks
        ]);
    }
}
