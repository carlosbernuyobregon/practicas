<?php

namespace App\Http\Livewire\Orders;

use App\AccountingAccount;
use App\Budget;
use App\Company;
use App\CostCenter;
use App\Helpers\Helpers;
use App\Library\Constants;
use App\Material;
use App\MesureUnit;
use App\Order;
use App\OrderLine;
use App\PepElement;
use App\Models\Provider;
use App\TaxType;
use App\User;
use Illuminate\Support\Facades\Auth;
use Livewire\Component;
use Livewire\WithPagination;

class Create extends Component
{
    use WithPagination;
    protected $paginationTheme = 'bootstrap';
    public $order;
    //public $providers;
    public $orderLine;
    //public $orderLines = [];
    public $materials = null;
    public $taxTypes = [];
    public $accountingAccounts = [];
    public $costCenters = [];
    public $orderCompany = 1000;
    public $pepElements = [];
    public $orderType = 'ceco';
    public $task = false;
    public $mesureUnits = [];
    public $lines = 0;
    public $costCenterId = '0000001074';
    public $costCenter = null;
    public $editable = false;
    public $newSupplierForm = false;
    public $provider;
    public $newSupplier = null;
    public $selectedMesureUnit = '';
    public $currencies;
    public $deliveryPointOption = '';
    public $deliveryPointOthers = '';
    public $showDenyReason = false;
    public $defaultMesureUnitId = '';
    public $defaultCurrency = '';
    public $currentBudgetsVersion;


    public $selCity;


    public $orderTypeValue = [
        'ceco' => 'Gastos Departamento',
        'active' => 'Activos',
        'project' => 'Proyecto'
    ];
    public $orderTypeCode = [
        'ceco' => 'A',
        'active' => 'K',
        'project' => 'P'
    ];
    public $orderTypeByCode = [
        'A' => 'ceco',
        'K' => 'active',
        'P' => 'project'
    ];
    protected $rules = [
        'deliveryPointOthers' => 'string',
        'deliveryPointOption' => 'string',
        'order.company' => 'string',
        'order.purchasing_group' => 'string',
        'order.buyer' => 'string',
        'order.date' => 'date',
        'order.deny_reason' => 'string',
        'order.delivery_point' => 'string',
        'order.provider_id' => 'string|required',
        'order.description' => 'string',
        'orderLine.description' => 'required|string|max:40',
        'orderLine.location_name' => 'required|string',
        'orderLine.tax_type_id' => 'required|string',
        'orderLine.net_amount' => 'required|numeric',
        'orderLine.material_id' => 'required|string',
        'orderLine.accounting_account_id' => 'required|string',
        'orderLine.cost_center_id' => 'required|string',
        'orderLine.qty' => 'required|numeric',
        'orderLine.delivery_date' => 'date|required',
        'orderLine.pep_element_id' => 'uuid',
        'orderLine.budget_id' => 'uuid|required',
        'orderLine.mesure_unit_id' => 'uuid|required',
        'orderLine.currency' => 'string|required',
        'provider.vat_number' => 'string|required',
        'provider.name' => 'string|required',
        'provider.company' => 'string|required',
        'provider.payment_condition' => 'string|required',
        'provider.payment_type' => 'string|required',
    ];
    public function mount($order = null, $orderType = null, $task = false, $editable = false)
    {
        $this->editable = $editable;
        if ($order != null) {
            //dd($order);
            $this->order = Order::find($order->id);
            $this->resetOrderLine();
            $this->orderType = $this->orderTypeByCode[$this->order->order_type ?? 'A'];
            $this->lines = OrderLine::where('order_id',$this->order->id)->count();
            //$this->costCenter = CostCenter::where('customer_id',auth()->user()->customer_id)->where('external_id',$this->costCenterId)->first();
            $this->costCenter = Auth::user()->costCenters->first();
            //$this->costCenters = CostCenter::where('customer_id', auth()->user()->customer_id)->where('company',$this->order->buyer)->orderBy('name', 'asc')->get();
            $this->costCenters = Auth::user()->costCenters;
        } else {
            $this->order = new Order();
            $this->orderType = $orderType;
            //$this->orderType = null;
            //$this->costCenters = CostCenter::where('customer_id', auth()->user()->customer_id)->orderBy('name', 'asc')->get();
            $this->costCenters = Auth::user()->costCenters;
        }
        //$this->providers = Provider::select('id','external_id','name')->where('customer_id', auth()->user()->customer_id)->orderBy('name', 'asc')->get();
        //$this->materials = Material::where('customer_id', auth()->user()->customer_id)->orderBy('material_name', 'asc')->get();
        $this->taxTypes = TaxType::orderBy('type', 'asc')->get();
        $this->mesureUnits = MesureUnit::select('id','name')->orderBy('name', 'asc')->get();
        //$this->pepElements = PepElement::where('customer_id', auth()->user()->customer_id)->orderBy('name', 'asc')->get();
        $this->costCenter = Auth::user()->costCenters->first();
        //$this->costCenter = CostCenter::where('external_id','0000001074')->first();

        if ($this->costCenter) {
            $this->pepElements = Budget::select('id','name')->where('customer_id', auth()->user()->customer_id)->where('cost_center_id',$this->costCenter->id)->where('non_stock',true)->orderBy('name', 'asc')->get();
        }
        //$this->accountingAccounts = AccountingAccount::select('id','name')->where('customer_id', auth()->user()->customer_id)->orderBy('name', 'asc')->get();
        $this->currencies = explode(',',config('custom.currencies-short-list'));
        $this->defaultCurrency = 'eur';
        $this->defaultMesureUnitId = MesureUnit::where('name','unit')->take(1)->value('id');
        if($task){
            $this->task = $task;
        }elseif(isset($this->order->status)&&($this->order->status > Constants::ORDER_STATUS_DRAFT)){
            $this->task = true;
        }else{
            $this->task = false;
        }

        // Last budgets version
        $this->currentBudgetsVersion = Helpers::getLastVersion(Budget::class);
    }
    public function updated($propertyName)
    {
        $this->validateOnly($propertyName);
        switch ($propertyName) {
            case 'orderLine.cost_center_id':
                $this->order->cost_center_id = $this->orderLine->cost_center_id;
                $this->costCenter = CostCenter::find($this->order->cost_center_id);
                $this->order->save();
                $this->pepElements = Budget::where('customer_id', auth()->user()->customer_id)
                    ->where('cost_center_id', $this->order->cost_center_id)
                    ->where('non_stock',true);

                if ($this->currentBudgetsVersion) {
                    $this->pepElements = $this->pepElements->where('version', $this->currentBudgetsVersion);
                }

                $this->pepElements = $this->pepElements
                    ->orderBy('name', 'asc')
                    ->get();

                $this->render();
                break;
            default:
                # code...
                break;
        }

    }
    public function saveOrder()
    {
        //$this->validateOnly('order.provider_id');
        if ($this->order->order_number == null) {

            $this->order->document_type = config('custom.orders-default-document-type');
            //$this->order->date = today();
            $this->order->user_id           = Auth::id();
            $this->order->cost_center_id    = $this->costCenter->id;
            $lastOrder                      = Order::where('order_number', '!=', null)->orderBy('order_number', 'desc')->first();
            $this->order->order_number      = ($lastOrder->order_number ?? 4700000000) + 1;
            $this->order->customer_id       = auth()->user()->customer_id;
            $this->order->company           = $this->orderCompany;
            $this->order->purchasing_group  = config('custom.orders-default-purchasing-group');

            $this->order->buyer             = config('custom.orders-default-buyer');
            $this->order->order_type        = $this->orderTypeCode[$this->orderType];
            if ($this->deliveryPointOption != 'Otros') {
                $this->delivery_point       = $this->deliveryPointOthers;
            } else {
                $this->delivery_point       = $this->deliveryPointOthers;
            }

            $this->order->save();
            //dd($this->order);

            $this->resetOrderLine();

            return redirect(route('order.edit', [
                'order' => $this->order->id
            ]));
        } else {
            if($this->deliveryPointOption != 'Otros'){
                $this->delivery_point = $this->deliveryPointOthers;
            }else{
                $this->delivery_point = $this->deliveryPointOthers;
            }
            $this->order->save();
        }
    }
    public function floatvalue($val){
        $val = str_replace(",",".",$val);
        $val = preg_replace('/\.(?=.*\.)/', '', $val);
        return floatval($val);
    }
    public function addLine()
    {
        //dd($this->orderLine);
        $this->validate([
            'orderLine.description' => 'required|string|max:40',
            'orderLine.delivery_date' => 'date|required',
            'orderLine.cost_center_id' => 'required|string',
            'orderLine.budget_id' => 'uuid|required',
            'orderLine.qty' => 'required|numeric',
            'orderLine.net_amount' => 'required|numeric',
            'orderLine.mesure_unit_id' => 'uuid|required',
            'orderLine.currency' => 'string|required'
        ]);
        $lastLine = OrderLine::where('order_id', $this->order->id)->orderBy('position', 'desc')->value('position');
        $this->orderLine->tax_type_id = TaxType::where('type','S3')->value('id');
        $this->orderLine->net_amount = $this->floatvalue($this->orderLine->net_amount);
        $this->orderLine->qty = $this->floatvalue($this->orderLine->qty);

        $this->orderLine->customer_id = $this->order->customer_id;
        $this->orderLine->order_id = (string)$this->order->id;
        $this->orderLine->position = ($this->orderLine->position != null) ? $this->orderLine->position : ($lastLine ?? 0) + 10;
        //$this->orderLine->center_id = 1001;
        $this->orderLine->material_id = (string)$this->order->id;
        //dd($this->orderLine);
        $this->orderLine->save();
        $this->resetOrderLine();
        $this->selectedMesureUnit = '';
        $this->order->refresh();
        $this->render();
        $this->emit('cleanSelect','mesure_unit_select');
    }
    public function resetOrderLine()
    {
        $this->orderLine = new OrderLine();
        $this->orderLine->currency = $this->defaultCurrency;
        $this->orderLine->mesure_unit_id = $this->defaultMesureUnitId;
    }
    public function deleteLine($lineId)
    {
        OrderLine::destroy($lineId);
        $this->resetOrderLine();
        $this->render();
    }
    public function editLine($lineId)
    {
        $this->orderLine = OrderLine::find($lineId);
        //$this->selectedMesureUnit = $this->orderLine->mesure_unit_id;
        $this->render();
        $this->emit('orderLineLoaded',$this->orderLine->mesure_unit_id);
    }
    public function deleteOrder()
    {
        $orderLines = OrderLine::where('order_id',$this->order->id)->get();
        foreach($orderLines as $line){
            $line->delete();
        }
        $this->order->delete();
        session()->flash('status', 'Pedido borrado correctamente.');
        return redirect()->to(route('order.index'));
    }
    public function approve()
    {
        $previousStatus = $this->order->status;

        switch ($previousStatus) {
            case Constants::ORDER_STATUS_DRAFT:
                $this->order->status = Constants::ORDER_STATUS_PENDING_APPROVAL_BY_MANAGER;
                $this->order->save();
                session()->flash('status', 'Pedido correctamente confirmado.');
                return redirect()->to(route('order.index'));
                break;
            case Constants::ORDER_STATUS_PENDING_APPROVAL_BY_MANAGER:
                $this->order->status = Constants::ORDER_STATUS_APPROVED_BY_MANAGER;
                $this->order->manager_id = auth()->id();
                break;
            case Constants::ORDER_STATUS_APPROVED_BY_MANAGER:
                $this->order->status = Constants::ORDER_STATUS_APPROVED_BY_MANAGEMENT;
                break;
            case Constants::ORDER_STATUS_APPROVED_BY_MANAGEMENT:
                $this->order->status = Constants::ORDER_STATUS_APPROVED_BY_GENERAL_MANAGEMENT;
                break;
            case 7:
                $this->order->status = Constants::ORDER_STATUS_PENDING_APPROVAL_BY_MANAGER;
                $this->order->save();
                session()->flash('status', 'Pedido correctamente confirmado.');
                return redirect()->to(route('order.index'));
                break;
        }
        $this->order->save();
        session()->flash('status', 'Pedido aprobado correctamente.');
        return redirect()->to(route('order.taskList',[
            'status' => $previousStatus
        ]));
    }
    public function cancel()
    {
        $this->showDenyReason = true;
    }
    public function confirmCancel()
    {
        $previousStatus = $this->order->status;
        $this->order->status = Constants::ORDER_STATUS_CANCELED;
        $this->order->save();
        session()->flash('status', 'Pedido denegado corrrectamente.');
        return redirect()->to(route('order.taskList',[
            'status' => $previousStatus
        ]));
    }
    public function modificate()
    {
        $previousStatus = $this->order->status;
        $this->order->status = Constants::ORDER_STATUS_MODIFICATION_PENDING;
        $this->order->save();
        session()->flash('status', 'Pedido enviado a modificar corrrectamente.');
        return redirect()->to(route('order.taskList',[
            'status' => $previousStatus
        ]));
    }
    public function hold()
    {
        $previousStatus = $this->order->status;
        if($previousStatus != Constants::ORDER_STATUS_BLOCKED){
            $this->order->status = Constants::ORDER_STATUS_BLOCKED;
            $this->order->save();
            session()->flash('warning', 'Pedido bloqueado corrrectamente.');
        }else{
            $this->order->status = Constants::ORDER_STATUS_DRAFT;
            $this->order->save();
            session()->flash('warning', 'Pedido desbloqueado corrrectamente.');
        }
        return redirect()->to(route('order.taskList',[
            'status' => $previousStatus
        ]));
    }
    public function newSupplier()
    {
        $this->newSupplierForm = true;
        $this->provider = new Provider();
    }
    public function saveSupplier()
    {
        $this->validate([
            'provider.vat_number' => 'string|required',
            'provider.name' => 'string|required',
            'provider.company' => 'string|required',
            'provider.payment_condition' => 'string|required',
            'provider.payment_type' => 'string|required',
        ]);
        $this->provider->save();
        $this->order->provider_id = $this->provider->id;
        $this->newSupplier = $this->provider;
        $this->newSupplierForm = false;
        $this->provider = new Provider();

    }
    public function rejectSupplier()
    {
        $this->newSupplierForm = false;
        $this->provider = new Provider();
    }
    public function render()
    {
        if($this->order->id != null){
            $this->lines = OrderLine::where('order_id',$this->order->id)->count();
            $providers = [];
        }else{
            $providers = Provider::select('id','external_id','name','vat_number')->where('customer_id', auth()->user()->customer_id)->orderBy('name', 'asc')->get();
        }
        return view('livewire.orders.create', [
            'orderLines' => OrderLine::where('order_id', $this->order->id)->with('mesureUnit','costCenter','budget')->orderBy('position', 'asc')->paginate(10),
            'providers' => $providers

        ]);
    }
}
