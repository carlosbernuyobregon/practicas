<?php

namespace App\Http\Livewire\Invoices;

use App\Invoice;
use Illuminate\Database\Eloquent\Builder;
use Livewire\Component;
use Livewire\WithPagination;

class Pending extends Component
{
    use WithPagination;

    public $status;

    protected $paginationTheme = 'bootstrap';

    public $sortBy = 'email_attachments.created_at';
    public $sortDirection = 'asc';
    public $entries = 10;
    public $filtersMode = false;
    public $emailSearch = [
        "email" => "",
        "subject" => "",
        "fromDownloadDate" => "",
        "toDownloadDate" => "",
        "fromProcessedDate" => "",
        "toProcessedDate" => "",
    ];

    public function clearFilters()
    {
        session()->forget('emailSearch');
        $this->emailSearch = [
            "email" => "",
            "subject" => "",
            "fromDownloadDate" => "",
            "toDownloadDate" => "",
            "fromProcessedDate" => "",
            "toProcessedDate" => "",
        ];
    }

    public function mount()
    {
        //Filters saved on session
        if (session()->has('emailSearch')) {
            foreach ($this->emailSearch as $key => $filter) {
                if (session()->has('emailSearch.' . $key) == $key) {
                    $this->emailSearch[$key] = session()->get('emailSearch.' . $key)[0];
                }
            }
        }
    }

    public function updated($propertyName, $value)
    {
        session()->forget($propertyName);

        //Save the search filters on user session
        if ($value != '') {
            session()->push($propertyName, $value);
        }

        if (empty(session('emailSearch'))) {
            session()->forget('emailSearch');
        }
    }

    public function render()
    {
        $invoices = Invoice::where('status', 0)
        ->whereHas('emailAttachment.email', function (Builder $query) {
            if ($this->emailSearch['email'] != '') {
                $query->where('from', 'like', '%' . $this->emailSearch['email'] . '%');
            }
            if ($this->emailSearch['subject'] != '') {
                $query->where('subject', 'like', '%' . $this->emailSearch['subject'] . '%');
            }
        })
        ->whereHas('emailAttachment', function (Builder $query) {
            if ($this->emailSearch['fromDownloadDate'] != '' || $this->emailSearch['toDownloadDate'] != '') {
                if ($this->emailSearch['fromDownloadDate'] != '' && $this->emailSearch['toDownloadDate'] != '') {
                    $query->wherebetween('created_at', [$this->emailSearch['fromDownloadDate'], $this->emailSearch['toDownloadDate']]);
                } else if ($this->emailSearch['fromDownloadDate'] != '') {
                    $query->Where('created_at', '>=', $this->emailSearch['fromDownloadDate']);
                } else {
                    $query->Where('created_at', '<=', $this->emailSearch['toDownloadDate']);
                }
            }

            if ($this->emailSearch['fromProcessedDate'] != '' || $this->emailSearch['toProcessedDate'] != '') {
                if ($this->emailSearch['fromProcessedDate'] != '' && $this->emailSearch['toProcessedDate'] != '') {
                    $query->wherebetween('created_at', [$this->emailSearch['fromProcessedDate'], $this->emailSearch['toProcessedDate']]);
                } else if ($this->emailSearch['fromProcessedDate'] != '') {
                    $query->Where('updated_at', '>=', $this->emailSearch['fromProcessedDate']);
                } else {
                    $query->Where('updated_at', '<=', $this->emailSearch['toProcessedDate']);
                }
            }
        })
        ->join('email_attachments', 'email_attachments.id', '=', 'invoices.email_attachment_id')
        ->join('emails', 'emails.id', '=', 'email_attachments.email_id')
        ->orderBy($this->sortBy, $this->sortDirection)
        ->paginate($this->entries);
        return view('livewire.invoices.pending',[
            'invoices' => $invoices,
        ]);
    }

    public function sortBy($field)
    {
        if ($this->sortDirection == 'asc') {
            $this->sortDirection = 'desc';
        } else {
            $this->sortDirection = 'asc';
        }

        return $this->sortBy = $field;
    }

    public function updatingSearch()
    {
        $this->resetPage();
    }

    public function modeFilters()
    {
        $this->filtersMode = !$this->filtersMode;
    }
}
