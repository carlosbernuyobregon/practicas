<?php

namespace App\Http\Livewire\Invoices;

use App\Invoice;
use App\InvoiceLine;
use App\Library\Constants;
use App\Rules\MultipleOf;
use App\TaxType;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Str;
use Livewire\Component;
use Livewire\WithPagination;
use function Livewire\str;

class InvoiceLineTable extends Component
{
    use WithPagination;

    protected $paginationTheme = 'bootstrap';

    protected $listeners = [
        'rechargeInvoice'       => 'getInvoiceProperty',
        'rechargeInvoiceLine'   =>'getInvoiceProperty'
    ];

    public $editMode            = false;
    public $filtersMode         = false;
    public $invoice             = null;
    public $view                = false;
    public $sortBy              = 'item_order_num_bulk';
    public $sortDirection       = 'asc';
    public $search              = [
        'materialCode'          => '',
        'materialDescription'   => '',
        'materialQtyMin'        => '',
        'materialQtyMax'        => '',
        'unitPriceMin'          => '',
        'unitPriceMax'          => '',
        'itemOrderNumBulk'      => '',
        'itemOrderLineBulk'     => '',
        'netAmountMin'          => '',
        'netAmountMax'          => '',
        'totalAmountMin'        => '',
        'totalAmountMax'        => '',
    ];
    public $invoiceLinesArray   = [];
    public $message             = '';
    public $error               = '';
    public $bulkDisabled        = true;
    public $bulkConfirmDisabled = true;
    public $confirmAction       = false;
    public $selectAll           = false;
    public $selectedLines;
    public $confirmDeleteId;
    public $entries;
    public $bulkOptions;
    public $taxTypes;
    public $genericInput;
    public $genericInputText;
    public $genericInputPlaceHolderText;
    public $chosenBulkOption;
    public $chosenBulkCustomValue;

    /**
     * In Livewire components, you use mount() instead of a class constructor __construct()
     * like you may be used to.
     *
     * @param mixed $invoice
     * @param boolean $view
     * @return void
     */
    public function mount($invoice = null, $view = false)
    {
        $this->invoice  = $invoice;
        // Lazy load relationship
        $this->invoice->load(['invoiceLines', 'invoiceLinesLite']);
        $this->view     = $view;
        $this->createInvoiceLinesArray();
        $this->message  = '';
        $this->error    = '';
        $this->reloadBulkData();
    }

    /**
     * Renders the component view
     *
     * @return mixed
     */
    public function render()
    {
        // Hash the search array combined with the amount of lines to be able to create the right cache tag
        $searchHash = md5(json_encode($this->search)) . '-' . $this->invoice->invoiceLinesLite->count();

        // Get invoices from cache or from DB if its not cached yet
        $invoiceLinesQry = InvoiceLine::search($this->invoice->id, $this->search)
            ->orderBy($this->sortBy, $this->sortDirection)
            ->orderBy('item_order_line_bulk', 'asc');

        // Determines if cache should be used or not
        (config('app.cache.active'))
            ? $invoiceLines = Cache::remember('invoice-lines-table-search-' . $searchHash, config('custom.global-cache-time-in-seconds'), function () use ($invoiceLinesQry) {
            return $invoiceLinesQry->paginate($this->entries);
        })
            : $invoiceLines = $invoiceLinesQry->paginate($this->entries);

        // Enable / disable bulk action button depending on the number of checked checkboxes
        $this->bulkDisabled = $this->selectedLines->filter(fn($p) => $p)->count() < 2;

        // Check / uncheck checkboxes with JS events
        /*($this->selectAll)
            ? $this->selectAllLineCheckboxes()
            : $this->unselectAllLineCheckboxes();*/

        return view('livewire.invoices.invoice-line-table', [
            'invoiceLines'      => $invoiceLines,
            'taxes'             => TaxType::orderBy('type', 'asc')->get(),
            'defaultCurrency'   => $this->invoice->currency
        ])
            ->with('message', $this->message)
            ->with('error', $this->error);
    }

    /**
     * Runs after rechargeInvoice event listener.
     * Reloads the $invoice object
     * @var object $invoice
     *
     * @return void
     */
    public function getInvoiceProperty()
    {
        $this->invoice = Invoice::find($this->invoice->id);
        $this->createInvoiceLinesArray();
        $this->emitTo('invoices.header', 'checkInvoice');
    }

    /**
     * Changes the $sortBy and the $sortDirection values
     * @var string $sortBy
     * @var string $sortDirection
     *
     * @param string $field
     * @return void
     */
    public function sortBy($field)
    {
        if ($this->sortDirection == 'asc') {
            $this->sortDirection = 'desc';
        } else {
            $this->sortDirection = 'asc';
        }

        $this->sortBy = $field;
    }

    /**
     * Runs before any update to the Livewire component's data
     * (Using wire:model, not directly inside PHP)
     *
     * @return void
     */
    public function updating()
    {
        $this->resetPage();
    }

    /**
     * Runs after any update to the Livewire component's data
     * (Using wire:model, not directly inside PHP)
     *
     * @param string $name
     * @param $value
     * @return void
     * @throws \Illuminate\Validation\ValidationException
     */
    public function updated($name, $value)
    {
        $this->validateOnly($name);

        // Apply discount priority logic
        $this->applyDiscountFieldsPriorityLogic($name, $value);
    }

    /**
     * This function creates an array that contains editable content of invoiceLine objects.
     *
     * @return void
     */
    public function createInvoiceLinesArray()
    {
        $invoiceLines = $this->invoice->invoiceLines;

//        dd($this->invoiceLinesArray, $invoiceLines);
        $lineCounter = 10;
        $prevOrderNum = '';

        foreach ($invoiceLines as $line) {
            $this->invoiceLinesArray[$line->id] = $line;

            //Check if "pedido" is null or empty
            if ($this->invoiceLinesArray[$line->id]['item_order_num_bulk'] == null ||
                $this->invoiceLinesArray[$line->id]['item_order_num_bulk'] == '') {
                $this->invoiceLinesArray[$line->id]['item_order_num_bulk'] = $this->invoice->intern_order;
                $this->invoiceLinesArray[$line->id]->save();
            }
            //dump($this->invoiceLinesArray[$line->id]['item_order_num_bulk']);

            //Check if line positions are correct
            if ($this->invoiceLinesArray[$line->id]['item_order_line_bulk'] == null ||
                $this->invoiceLinesArray[$line->id]['item_order_line_bulk'] <=> $lineCounter ||
                $this->invoiceLinesArray[$line->id]['item_order_num_bulk'] != $prevOrderNum) {

                if ($this->invoiceLinesArray[$line->id]['item_order_num_bulk'] != $prevOrderNum) {
                    $lineCounter = 10;
                    $this->invoiceLinesArray[$line->id]['item_order_line_bulk'] = $lineCounter;
                } else {
                    //dump('Entra',$lineCounter);
                    $this->invoiceLinesArray[$line->id]['item_order_line_bulk'] = $lineCounter;
                }

                $this->invoiceLinesArray[$line->id]->save();
            }


            // Check if tax type is null or empty
            if ($this->invoiceLinesArray[$line->id]['tax_type_id'] == null &&
                $this->invoiceLinesArray[$line->id]['taxes_id'] == null &&
                !empty($this->invoice->tax->type)) {

                $this->invoiceLinesArray[$line->id]['tax_type_id']  = $this->invoice->taxes_type;
                $this->invoiceLinesArray[$line->id]['taxes_id']     = $this->invoice->tax->type;

                $taxesTotalAmount =
                    $this->invoiceLinesArray[$line->id]['net_amount'] * (intval($this->invoice->tax->percentage) / 100);

                $this->invoiceLinesArray[$line->id]['taxes_amount'] = $taxesTotalAmount;

                // Forcing irpf to null since is not a valid concept (irpf by lines)
                $this->invoiceLinesArray[$line->id]['irpf'] = 0;

                switch ($this->invoice->tax->special_type) {
                    case Constants::TAX_SPECIAL_TYPE_AUTOREPERCUTIDO:
                        $totalAmount = $this->invoiceLinesArray[$line->id]['net_amount'] - $this->invoiceLinesArray[$line->id]['irpf'];
                        break;
                    default:
                        $totalAmount = $this->invoiceLinesArray[$line->id]['net_amount'] - $this->invoiceLinesArray[$line->id]['irpf'] + $taxesTotalAmount;
                        break;
                }

                $this->invoiceLinesArray[$line->id]['total_amount'] = round($totalAmount, 2);
                $this->invoiceLinesArray[$line->id]->save();
            }

            $prevOrderNum = $this->invoiceLinesArray[$line->id]['item_order_num_bulk'];
            $lineCounter += 10;
            //dump($lineCounter);
        }
    }

    /**
     * Adds a new invoice line
     *
     * @return void
     */
    public function addNewInvoiceLine()
    {
        $newLine = new InvoiceLine();
        $newLine->invoice_id = $this->invoice->id;
        $newLine->item_order_line_bulk = 0;
        try {
            $newLine->save();
        } catch (\Throwable $th) {
            session()->flash('error', 'Error al actualizar los datos.');
            $this->error = 'Error al actualizar los datos.';
        }
        $this->emit('rechargeInvoice');
    }

    /**
     * Changes the $confirmDeleteId value
     * @var string $confirmDeleteId
     *
     * @param string $id
     * @return void
     */
    public function confirmDelete($id)
    {
        $this->confirmDeleteId = $id;
    }

    /**
     * Deletes an InvoiceLine object after confirmation
     *
     * @return void
     */
    public function delete()
    {
        unset($this->invoiceLinesArray[$this->confirmDeleteId]);
        InvoiceLine::findOrFail($this->confirmDeleteId)->delete();
        $this->dispatchBrowserEvent('deleteItem');
        $this->emit('rechargeInvoice');
    }

    /**
     * Changes the $editMode value
     * @var bool $editMode
     *
     * @return void
     */
    public function mode()
    {
        $this->editMode = !$this->editMode;
        $this->createInvoiceLinesArray();
        $this->render();
    }

    /**
     * Changes the $filtersMode value
     * @var bool $filtersMode
     *
     * @return void
     */
    public function modeFilters()
    {
        $this->filtersMode = !$this->filtersMode;
    }

    /**
     * Calculates the Invoice line tax amount
     *
     * @param float $netAmount
     * @param int $percentage
     * @return float
     */
    public function calculateTaxesAmount($netAmount, $percentage)
    {
        return round($netAmount * $percentage / 100, 2);
    }

    /**
     * Triggers a recalculation for totals and taxes values
     */
    public function recalculateLineTotals()
    {
        $this->invoice->invoiceLines->each(function ($line) {
            $line->recalculateTotals();
//            $line->save(); // Not needed anymore, the saving is being triggered inside the method
        });
        // Call recalculate method for those broke down values (total_net_2, total_net_3, etc)
        $this->invoice->recalculateTotalsAndTaxesForBreakDownFields();
        $this->emitUp('rechargeInvoice', true);
        $this->emitTo('invoices.header', 'rechargeInvoice');
        $this->message = 'Totales e impuestos han sido recalculados';
        $this->render();
    }

    /**
     * Saves the $invoice object data, saves the invoice lines and makes the audit
     *
     * @return void
     */
    public function save()
    {
        $this->validate();

//        dd($this->invoiceLinesArray);

        try {
            $this->invoice->save();

            //Saves editable content of invoiceLine objects
            foreach ($this->invoiceLinesArray as $item) {

                try {
                    $provisionalInvoice = InvoiceLine::find($item['id']);

                    if ($provisionalInvoice != null) {

                        // Get the tax type
                        $taxType = TaxType::find($item['tax_type_id']);

                        $provisionalInvoice->item_order_line_bulk   = $item['item_order_line_bulk'] ?? $this->invoice->intern_order;
                        $provisionalInvoice->item_order_num_bulk    = $item['item_order_num_bulk'];
                        $provisionalInvoice->material_code          = $item['material_code'];
                        $provisionalInvoice->material_description   = $item['material_description'];
                        $provisionalInvoice->material_qty           = $item['material_qty'];
                        // Forcing irpf to null since is not a valid concept (irpf by lines)
                        $provisionalInvoice->irpf                   = 0;
                        $provisionalInvoice->unit_price             = auth()->user()->floatvalue($item['unit_price']);
                        $provisionalInvoice->net_amount             = auth()->user()->floatvalue($item['net_amount']);
                        $provisionalInvoice->gross_amount           = auth()->user()->floatvalue($item['gross_amount']);
                        $provisionalInvoice->tax_type_id            = $item['tax_type_id'];
                        $provisionalInvoice->taxes_id               = ($taxType) ? $taxType->type : null;
                        $provisionalInvoice->taxes                  = ($taxType) ? $taxType->percentage : null;
                        $provisionalInvoice->discount_amount        = $item['discount_amount'];
                        $provisionalInvoice->discount_rate          = $item['discount_rate'];

                        // Recalculate taxes and totals
                        $provisionalInvoice->recalculateTotals();
                        // Save data
//                        $provisionalInvoice->save(); // Not needed anymore, the saving is being triggered inside the method
                        $this->message = 'Datos actualizados correctamente.';
                    }
                    unset($provisionalInvoice);

                } catch (\Throwable $th) {
                    session()->flash('error', 'Error al actualizar los datos.');
                    $this->error = 'Error al actualizar los datos.';
                    Log::error($th->getMessage(), [
                        'class'     => __CLASS__,
                        'method'    => __METHOD__,
                    ]);
                }
            }
            session()->flash('message', 'Datos actualizados correctamente.');

            // Call recalculate method for those broke down values (total_net_2, total_net_3, etc)
            $this->invoice->recalculateTotalsAndTaxesForBreakDownFields();

            $this->mode();
            //$this->invoice->doAudit('saveLine');
            $this->invoice->audit()->create([
                'user_id' => auth()->user()->id,
                'status' => $this->invoice->status,
                'text' => __('auditMessages.saveLine')
            ]);
            $this->emit('rechargeInvoiceLine');
            $this->emitTo('invoices.header', 'render');
            $this->emitTo('invoices.header', 'rechargeInvoice');
            $this->emitTo('invoices.header', 'refreshComponent');
        } catch (\Illuminate\Database\QueryException $exception) {
            $errorInfo = $exception->errorInfo;

            session()->flash('error', $errorInfo[2]);
            Log::error($exception->getMessage(), [
                'class'     => __CLASS__,
                'method'    => __METHOD__,
            ]);
        }
    }

    /**
     * Gets the validation rules
     * @return array
     */
    public function rules()
    {
        return[
            'invoiceLinesArray.*.item_order_num_bulk'   => ['required', 'string'],
            'invoiceLinesArray.*.item_order_line_bulk'  => ['required', 'numeric', new MultipleOf()],
            'invoiceLinesArray.*.net_amount'            => ['required', 'numeric'],
            'invoiceLinesArray.*.gross_amount'          => ['required', 'numeric'],
            'invoiceLinesArray.*.taxes_amount'          => ['required', 'numeric'],
            'invoiceLinesArray.*.tax_type_id'           => ['required', 'string'],
            'invoiceLinesArray.*.unit_price'            => ['required', 'numeric'],
//            'invoiceLinesArray.*.irpf'                  => ['required', 'numeric'],
            'invoiceLinesArray.*.discount_amount'       => ['required', 'numeric'],
            'invoiceLinesArray.*.discount_rate'         => ['nullable', 'numeric'],
            'invoiceLinesArray.*.material_qty'          => ['required', 'numeric', 'gt:0']
        ];
    }

    /**
     * Returns the property name
     *
     * @param $propertyName
     * @return mixed
     */
    public function splitUpPropertyName($propertyName)
    {
        $exploded = explode('.', $propertyName);

        if (count($exploded) <= 1) {
            return [false, false];
        }

        // Return line id and field name
        return [$exploded[1], last($exploded)];
    }

    /**
     * Applies discount fields priority logic. This means when some of the discount related fields value is set
     * the opposite field is set to zero
     *
     * @param $propertyName
     * @param $propertyValue
     */
    public function applyDiscountFieldsPriorityLogic($propertyName, $propertyValue)
    {
        // Get the line id and field name
        list($lineId, $fieldName) = $this->splitUpPropertyName($propertyName);

        // Apply priority logic
        switch ($fieldName) {
            case 'discount_amount':
                // If discount_amount have a valid value, then clean up discount_rate value
                if ($propertyValue != 0) {
                    $this->invoiceLinesArray[$lineId]['discount_rate'] = 0;
                }
                break;
            case 'discount_rate':
                // If discount discount_rate a valid value, then clean up discount_amount value
                if ($propertyValue != 0) {
                    $this->invoiceLinesArray[$lineId]['discount_amount'] = 0;
                }
                break;
            default:
                break;
        }
    }

    /**
     * START BULK ACTIONS METHODS
     */

    /**
     * Reloads bulk action data
     */
    public function reloadBulkData() : void
    {
        $this->selectedLines                = collect();
        $this->bulkOptions                  = config('custom.line-bulk-available-options');
        $this->bulkConfirmDisabled          = true;
        $this->chosenBulkOption             = null;
        $this->chosenBulkCustomValue        = null;
        $this->taxTypes                     = null;
        $this->genericInput                 = null;
        $this->confirmAction                = null;
        $this->selectAll                    = false;
    }

    /**
     * Confirms the user is aware of doing bulk actions
     */
    public function confirmBulkAction() : void
    {
        $this->confirmAction = true;
    }

    /**
     * Performs selected bulk action
     */
    public function doBulkActions() : void
    {
        // Selected lines id as array
        $selectedLinesArray = $this->selectedLines->filter(fn($line) => $line)->keys()->toArray();

        // Bring back the collection for selected lines since is better to operate with collections
        $invoiceLines = $this->invoice->invoiceLines->whereIn('id', $selectedLinesArray);

        switch ($this->chosenBulkOption) {
            case 'change-tax-type':
                // Get the selected tax type data
                $chosenTaxType = $this->taxTypes->where('id', $this->chosenBulkCustomValue)->first();
                // Go through each one of them and change related tax type values
                $invoiceLines->each(function ($line) use ($chosenTaxType) {
                    $line->tax_type_id  = $chosenTaxType->id;
                    $line->taxes        = $chosenTaxType->percentage;
                    $line->taxes_id     = $chosenTaxType->type;
                    $line->save();
                });
                break;
            case 'delete':
                // Go get the models and delete them. A new query is needed to delete records.
                InvoiceLine::whereIn('id', $selectedLinesArray)->delete();
                break;
            case 'change-num-order':
                // Go through each one of them and change related tax type values
                $invoiceLines->each(function ($line) {
                    $line->item_order_num_bulk = $this->chosenBulkCustomValue;
                    $line->save();
                });
                break;
        }

        $this->hideModal();
        $this->reloadBulkData();
        $this->emit('rechargeInvoiceLine');
        $this->emitTo('invoices.header', 'rechargeInvoice');
        $this->emitTo('invoices.header', 'refreshComponent');
        $this->recalculateLineTotals();
    }

    /**
     * Runs after selectedLines property is updated
     *
     * @param $value
     */
    public function updatedSelectedLines($value) : void
    {
        $this->uncheckHeaderLineCheckbox();
    }

    /**
     * Runs after selectedAll property is updated
     *
     * @param $value
     */
    public function updatedSelectAll($value) : void
    {
        if ($value) {
            $this->selectedLines = $invoiceLines = InvoiceLine::search($this->invoice->id, $this->search)
                ->orderBy($this->sortBy, $this->sortDirection)
                ->orderBy('item_order_line_bulk', 'asc')
                ->paginate($this->entries)
                ->pluck('id', 'id');
            $this->selectAllLineCheckboxes();
        } else {
            $this->selectedLines = collect();
            $this->unselectAllLineCheckboxes();
        }
    }

    /**
     * Runs after chosenBulkOption property is updated
     *
     * @param $value
     */
    public function updatedChosenBulkOption($value) : void
    {
        $this->bulkConfirmDisabled      = ($value) ? false : true;
        $this->chosenBulkCustomValue    = null;

        switch ($value) {
            case 'change-tax-type':
                $this->taxTypes             = TaxType::orderBy('type')->get();
                $this->genericInput         = false;
                $this->bulkConfirmDisabled  = true;
                $this->confirmAction        = false;
                break;
            case 'change-num-order':
                $this->taxTypes                     = false;
                $this->genericInput                 = true;
                $this->bulkConfirmDisabled          = true;
                $this->confirmAction                = false;
                $this->genericInputText             = __('backend.forms.lines.bulk.actions.insert-num-order-text');
                $this->genericInputPlaceHolderText  = __('backend.forms.lines.bulk.actions.insert-num-order-placeholder-text');
                break;
            default:
            case 'delete':
                $this->taxTypes         = false;
                $this->genericInput     = false;
                $this->confirmAction    = false;
                break;
        }
    }

    /**
     * Runs after chosenBulkCustomValue property is updated
     *
     * @param $value
     */
    public function updatedChosenBulkCustomValue($value) : void
    {
        $this->bulkConfirmDisabled = ($value) ? false : true;
    }

    /**
     * Shows bulk action modal
     */
    public function showModal() : void
    {
        $this->dispatchBrowserEvent('showModal');
    }

    /**
     * Hides bulk action modal
     */
    public function hideModal() : void
    {
        $this->dispatchBrowserEvent('hideModal');
    }

    /**
     * Sets all line checkboxes as checked
     */
    public function selectAllLineCheckboxes() : void
    {
        $this->dispatchBrowserEvent('checkAll');
    }

    /**
     * Sets all line checkboxes as unchecked
     */
    public function unselectAllLineCheckboxes() : void
    {
        $this->dispatchBrowserEvent('uncheckAll');
    }

    /**
     * Sets the header (check all) checkbox as unchecked
     */
    public function uncheckHeaderLineCheckbox() : void
    {
        $this->dispatchBrowserEvent('uncheckHeaderLineCheckbox');
    }

    /**
     * END BULK ACTIONS METHODS
     */
}
