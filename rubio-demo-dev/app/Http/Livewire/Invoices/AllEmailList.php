<?php

namespace App\Http\Livewire\Invoices;

use App\Invoice;
use Barryvdh\DomPDF\Facade as PDF;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Facades\Lang;
use Livewire\Component;
use Livewire\WithPagination;
use Rap2hpoutre\FastExcel\FastExcel;
use ZipArchive;
use File;

class AllEmailList extends Component
{
    use WithPagination;

    public $status;

    protected $paginationTheme = 'bootstrap';

    public $sortBy = 'email_attachments.created_at';
    public $sortDirection = 'asc';
    public $entries = 10;
    public $filtersMode = false;
    public $allEmailSearch = [
        "providerName" => "",
        "email" => "",
        "subject" => "",
        "fromDownloadDate" => "",
        "toDownloadDate" => "",
        "fromProcessedDate" => "",
        "toProcessedDate" => "",
        "status" => "",
    ];

    //Selection of status types to show on Table view
    public $statusToShow = [
        '0','1', '9', '4', '2', '6', '11', '5'
    ];

    /**
     * Deletes all the previous saved filters on the user session
     * and reset the filters array $allEmailsearch
     * @var array $allEmailSearch
     *
     * @return void
     */
    public function clearFilters()
    {
        session()->forget('allEmailSearch');
        $this->allEmailSearch = [
            "providerName" => "",
            "email" => "",
            "subject" => "",
            "fromDownloadDate" => "",
            "toDownloadDate" => "",
            "fromProcessedDate" => "",
            "toProcessedDate" => "",
            "status" => "",
        ];
    }

    /**
     * In Livewire components, you use mount() instead of a class constructor __construct()
     * like you may be used to.
     *
     * @return void
     */
    public function mount()
    {
        //Filters saved on session
        if (session()->has('allEmailSearch')) {
            foreach ($this->allEmailSearch as $key => $filter) {
                if (session()->has('allEmailSearch.' . $key) == $key) {
                    $this->allEmailSearch[$key] = session()->get('allEmailSearch.' . $key)[0];
                }
            }
        }
    }

    /**
     * Runs after any update to the Livewire component's data
     * (Using wire:model, not directly inside PHP)
     *
     * @param string $propertyName
     * @param mixed $value
     * @return void
     */
    public function updated($propertyName, $value)
    {
        session()->forget($propertyName);

        //Save the search filters on user session
        if ($value != '') {
            session()->push($propertyName, $value);
        }

        if (empty(session('allEmailSearch'))) {
            session()->forget('allEmailSearch');
        }
    }

    /**
     * Renders the component view
     *
     * @return mixed
     */
    public function render()
    {
        $invoices = Invoice::emailSearch($this->allEmailSearch)
                    ->orderBy($this->sortBy, $this->sortDirection)
                    ->paginate($this->entries);

        // In case there are Invoices without provider name, this code inctroduces it the database
        foreach ($invoices as $invoice) {
            if ($invoice->status === 0 && $invoice->provider_name == null) {
                $providerName = explode("@",$invoice->emailAttachment->email->from);
                $invoice->provider_name = strtoupper(explode(".",$providerName[1])[0]);
                $invoice->save();
            }
        }

        $statusTypes = Lang::get('status');
        asort($statusTypes);

        return view('livewire.invoices.all-email-list', [
            'invoices' => $invoices,
            'statusTypes' => $statusTypes,
        ]);
    }

    /**
     * Changes the $sortBy and the $sortDirection values
     * @var string $sortBy
     * @var string $sortDirection
     *
     * @param string $field
     * @return void
     */
    public function sortBy($field)
    {
        if ($this->sortDirection == 'asc') {
            $this->sortDirection = 'desc';
        } else {
            $this->sortDirection = 'asc';
        }

        $this->sortBy = $field;
    }

    /**
     * Runs before a property called $allEmailSearch is updated
     *
     * @var array $allEmailSearch
     *
     * @return void
     */
    public function updatingAllEmailSearch()
    {
        $this->resetPage();
    }

    /**
     * Deletes Excel export file if exists
     *
     * @return void
     */
    public function deleteOldExcelFile()
    {
        $file = public_path() . "/fileEmails.xlsx";
        if (file_exists($file)) {
            unlink($file);
        }
    }

    /**
     * Generates a file with xlsx (Excel) format
     *
     * @param $path
     * @return object
     * @throws \Box\Spout\Common\Exception\IOException
     * @throws \Box\Spout\Common\Exception\InvalidArgumentException
     * @throws \Box\Spout\Common\Exception\UnsupportedTypeException
     * @throws \Box\Spout\Writer\Exception\WriterNotOpenedException
     */
    public function exportToExcel($path)
    {
        $data = Invoice::emailSearch($this->allEmailSearch)
            ->orderBy($this->sortBy, $this->sortDirection)
            ->get();

        return (new FastExcel($data))->export($path, function ($invoice){
            return [
                'Remitente'         => $invoice->provider_name,
                'Email'             => $invoice->emailAttachment->email->from,
                'Asunto'            => $invoice->emailAttachment->email->subject,
                'Nombre archivo'    => $invoice->emailAttachment->filename,
                'Fecha descarga'    => $invoice->emailAttachment->created_at->toDateTimeString(),
                'Fecha procesado'   => $invoice->emailAttachment->updated_at->toDateTimeString(),
                'Estado'            => __('status.'.$invoice->status),
            ];
        });
    }

    /**
     * Downloads excel file generated in exportToExcel()
     *
     * @return mixed
     */
    public function downloadExcel()
    {
        $path = tempnam(sys_get_temp_dir(), "FOO");
        $this->exportToExcel($path);
        //$tempPath = sys_get_temp_dir().'/';

        //$name = tempnam(sys_get_temp_dir(), "FOO");
        return response()->download($path, 'documents.xlsx', [
            'Content-Type'          => 'application/vnd.ms-excel',
            'Content-Disposition'   => 'inline; filename="documents.xlsx"'
        ]);
    }

    /**
     * Generates and download a PDF file
     *
     * @return mixed
     */
    public function exportToPDF()
    {
        $data = [
            'invoices' => Invoice::emailSearch($this->allEmailSearch)
                            ->orderBy($this->sortBy, $this->sortDirection)
                            ->get()
        ];

        $pdf = PDF::loadView('exports.pdf-export-emails', $data)
            ->setPaper('a4', 'landscape')
            ->output();

        return response()->streamDownload(
            fn () =>print($pdf), 'emailInvoices.pdf');
    }

    /**
     * Generates and downloads a ZIP file with all Invoices previously filtered
     *
     * @return mixed
     */
    public function downloadZip()
    {
        $fileName = 'facturasEmail.zip';

        //Delete last ZIP export file if exists
        $oldFile = public_path($fileName);

        if (file_exists($oldFile)) {
            unlink($oldFile);
        }
        $invoices = Invoice::emailSearch($this->allEmailSearch)
                    ->orderBy($this->sortBy, $this->sortDirection)
                    ->get();

        $zip = new ZipArchive;

        if ($zip->open(public_path($fileName), ZipArchive::CREATE) === TRUE)
        {
            foreach ($invoices as $invoice) {
                if (file_exists($invoice->getDownloadFileUrlAttribute())) {
                    $zip->addFile($invoice->getDownloadFileUrlAttribute(), $invoice->provider_name.'-'.$invoice->invoice_number.'.pdf');
                }
            }

            $zip->close();
        }

        if (file_exists(public_path($fileName))) {
            return response()->download(public_path($fileName));
        } else {
            session()->flash('error', 'Ha habido un error. No se han podido descargar las facturas.');
        }
    }
}
