<?php

namespace App\Http\Livewire\Invoices;

use App\Currency;
use App\Invoice;
use App\Models\InvoiceOriginalOcrData;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Log;
use Livewire\Component;

class OcrOriginalData extends Component
{
//    public $invoice;
    public $ocrData;
    public $editMode;
    public $message;
    public $error;

    protected $listeners = [
//        'rechargeData' => 'getInvoiceProperty'
    ];

    public function render()
    {
        return view('livewire.invoices.ocr-original-data')
            ->with('message', $this->message)
            ->with('error', $this->error);
    }

    public function mount(InvoiceOriginalOcrData $ocrData)
    {
        /*$this->invoice  = $invoice;
        $this->invoice->load('originalData');*/
        $this->ocrData  = $ocrData;
        $this->message  = false;
        $this->error    = false;
        $this->editMode = false;

    }

    /**
     * Sets the $editMode on - off
     */
    public function mode() : void
    {
        $this->editMode = !$this->editMode;

        //Resets the inputs
        if (!$this->editMode) {
            $this->emit('rechargeData');
        }
    }

    /**
     * Aborts the edit mode on
     *
     */
    public function cancel() : void
    {
        $this->mode();
        $this->resetValidation();
    }

    public function save()
    {
        if ($this->ocrData->isDirty()) {
            $this->ocrData->was_edited  = true;
            $this->ocrData->edited_at   = Carbon::now();
        }

        $this->validate();

        try {
            $this->ocrData->save();
        } catch (\PDOException $e) {
            Log::error($e->getMessage(), [
                'class'     => __CLASS__,
                'method'    => __METHOD__,
            ]);
            $this->error = 'Algo ha ido mal editando los datos originales del OCR';
        }

        $this->mode();
        $this->emit('rechargeData', true);
        $this->emitTo('invoices.header', 'rechargeInvoice');
        $this->message = 'Datos actualizados correctamente.';
        $this->render();
    }

    /**
     * Runs after any update to the Livewire component's data
     * (Using wire:model, not directly inside PHP)
     *
     * @param string $propertyName
     * @return void
     * @throws \Illuminate\Validation\ValidationException
     */
    public function updated($propertyName)
    {
        $this->validateOnly($propertyName);
    }

    /**
     * Gets the validation rules
     * @return array
     */
    public function rules()
    {
        return [
            'ocrData.taxes_total_amount'   => 'required|numeric',
            'ocrData.taxes_irpf_amount'    => 'required|numeric',
            'ocrData.total'                => 'required|numeric',
        ];
    }
}
