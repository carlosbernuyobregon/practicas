<?php

namespace App\Http\Livewire\Invoices;

use App\Invoice;
use Livewire\Component;
use Livewire\WithPagination;

class Processed extends Component
{
    use WithPagination;

    public $status;

    protected $paginationTheme = 'bootstrap';

    public $sortBy = 'sap_company';
    public $sortDirection = 'asc';
    public $entries = 10;
    public $search = [
        "sapCompany" => '',
        "providerName" => '',
        "vatNumber" => '',
        "invoiceNumber" => '',
        "date" => '',
        "totalNetMin" => '',
        "totalNetMax" => '',
        "taxesTotalAmountMin" => '',
        "taxesTotalAmountMax" => '',
        "totalMin" => '',
        "totalMax" => '',
        "paymentDate" => '',
        "sapDocumentNumber" => '',
        "internOrder" => '',
    ];
    public function mount($tableType = null)
    {
        $this->tableType = $tableType;
    }
    public function render()
    {
        $invoices = Invoice::where('status', 1)
            ->when($this->search['sapCompany'] != '', function($query){
                $query->Where('sap_company', 'like', '%'.$this->search['sapCompany'].'%');
            })
            ->when($this->search['providerName'] != '', function ($query) {
                $query->Where('provider_name', 'like', '%' . $this->search['providerName'] . '%');
            })
            ->when($this->search['vatNumber'] != '', function ($query) {
                $query->Where('vat_number', 'like', '%' . $this->search['vatNumber'] . '%');
            })
            ->when($this->search['invoiceNumber'] != '', function ($query) {
                $query->Where('invoice_number', 'like', '%' . $this->search['invoiceNumber'] . '%');
            })
            ->when($this->search['date'] != '', function ($query) {
                $query->Where('date', 'like', '%' . $this->search['date'] . '%');
            })
            ->when($this->search['totalNetMin'] != '' || $this->search['totalNetMax'] != '', function ($query) {
                if ($this->search['totalNetMin'] != '' && $this->search['totalNetMax'] != '') {
                    $query->WhereBetween('total_net', [$this->search['totalNetMin'], $this->search['totalNetMax']]);
                } else if ($this->search['totalNetMin'] != '') {
                    $query->Where('total_net', '>=', $this->search['totalNetMin']);
                }else {
                    $query->Where('total_net', '<=', $this->search['totalNetMax']);
                }
            })
            ->when($this->search['taxesTotalAmountMin'] != '' || $this->search['taxesTotalAmountMax'] != '', function ($query) {
                if ($this->search['taxesTotalAmountMin'] != '' && $this->search['taxesTotalAmountMax'] != '') {
                    $query->WhereBetween('taxes_total_amount', [$this->search['taxesTotalAmountMin'], $this->search['taxesTotalAmountMax']]);
                } else if ($this->search['taxesTotalAmountMin'] != '') {
                    $query->Where('taxes_total_amount', '>=', $this->search['taxesTotalAmountMin']);
                } else {
                    $query->Where('taxes_total_amount', '<=', $this->search['taxesTotalAmountMax']);
                }
            })
            ->when($this->search['totalMin'] != '' || $this->search['totalMax'] != '', function ($query) {
                if ($this->search['totalMin'] != '' && $this->search['totalMax'] != '') {
                    $query->WhereBetween('total', [$this->search['totalMin'], $this->search['totalMax']]);
                } else if ($this->search['totalMin'] != '') {
                    $query->Where('total', '>=', $this->search['totalMin']);
                } else {
                    $query->Where('total', '<=', $this->search['totalMax']);
                }
            })
            ->when($this->search['paymentDate'] != '', function($query) {
                $query->Where('payment_date', 'like', '%' . $this->search['paymentDate'] . '%');
            })
            ->when($this->search['sapDocumentNumber'] != '', function ($query) {
                $query->Where('sap_document_number', 'like', '%' . $this->search['sapDocumentNumber'] . '%');
            })
            ->when($this->search['internOrder'] != '', function ($query) {
                $query->Where('intern_order', 'like', '%' . $this->search['internOrder'] . '%');
            })
            ->orderBy($this->sortBy, $this->sortDirection)
            ->paginate($this->entries);

        return view('livewire.invoices.processed',[
            'invoices' => $invoices,
        ]);
    }

    public function sortBy($field){
        if ($this->sortDirection == 'asc') {
            $this->sortDirection = 'desc';
        } else {
            $this->sortDirection = 'asc';
        }

        return $this->sortBy = $field;
    }

    public function updatingSearch()
    {
        $this->resetPage();
    }
}
