<?php

namespace App\Http\Livewire\Invoices;

use App\Invoice;
use App\Library\Constants;
use App\TaxType;
use Barryvdh\DomPDF\Facade as PDF;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Lang;
use Illuminate\Support\Facades\Storage;
use Livewire\Component;
use Livewire\WithPagination;
use Rap2hpoutre\FastExcel\FastExcel;
use ZipArchive;

class Table extends Component
{
    use WithPagination;

    public $status;

    protected $paginationTheme = 'bootstrap';

    public $sortBy          = 'sap_company';
    public $sortDirection   = 'asc';
    public $entries         = 10;
    public $tableType       = '';
    public $view            = null;
    public $filtersMode     = false;
    public $search          = [
        "sapCompany"            => '',
        "providerName"          => '',
        "vatNumber"             => '',
        "invoiceNumber"         => '',
        "fromDate"              => '',
        "toDate"                => '',
        "fromDatePayment"       => '',
        "toDatePayment"         => '',
        "fromContabDate"        => '',
        "toContabDate"          => '',
        "totalNetMin"           => '',
        "totalNetMax"           => '',
        "taxesTotalAmountMin"   => '',
        "taxesTotalAmountMax"   => '',
        "totalMin"              => '',
        "totalMax"              => '',
        "paymentDate"           => '',
        "sapDocumentNumber"     => '',
        "internOrder"           => '',
        "status"                => '',
        "currency"              => '',
        "isCorrect"             => '',
        "taxType"               => '',
        "documentType"          => '',
        'unreadMessages'        => ''
    ];

    public $negativeWord = 'no';

    //Selection of status types to show on Table view
    public $statusToShow = [
        Constants::INVOICE_STATUS_VALIDATION_PENDING,
        Constants::INVOICE_STATUS_MANAGER_APPROVAL_PENDING,
        Constants::INVOICE_STATUS_BLOCKED,
        Constants::INVOICE_STATUS_ACCOUNTING_PENDING,
        Constants::INVOICE_STATUS_POSTED,
        Constants::INVOICE_STATUS_MANUALLY_POSTED,
        Constants::INVOICE_STATUS_ACCOUNTING_ERROR,
        Constants::INVOICE_STATUS_MANUAL_ACCOUNTING_APPROVED,
        Constants::INVOICE_STATUS_DUPLICATED
    ];

    /**
     * In Livewire components, you use mount() instead of a class constructor __construct()
     * like you may be used to.
     *
     * @param string $tableType
     * @param boolean $view
     * @return void
     */
    public function mount(string $tableType = '',$view = false)
    {
        $this->tableType    = $tableType;
        $this->view         = $view;

        // Serve different status depending on the view
        if ($this->tableType == 'pending') {
            $this->statusToShow = [
                Constants::INVOICE_STATUS_MANUAL_ACCOUNTING_APPROVED,
                Constants::INVOICE_STATUS_BLOCKED,
                Constants::INVOICE_STATUS_DUPLICATED,
                Constants::INVOICE_STATUS_MANAGER_APPROVAL_PENDING,
                Constants::INVOICE_STATUS_ACCOUNTING_PENDING,
                Constants::INVOICE_STATUS_VALIDATION_PENDING
            ];
        } elseif ($this->tableType == 'processed') {
            $this->statusToShow = [
                Constants::INVOICE_STATUS_INVALID,
                Constants::INVOICE_STATUS_MANUAL_ACCOUNTING_APPROVED,
                Constants::INVOICE_STATUS_BLOCKED,
                Constants::INVOICE_STATUS_POSTED,
                Constants::INVOICE_STATUS_MANUALLY_POSTED,
                Constants::INVOICE_STATUS_DUPLICATED,
                Constants::INVOICE_STATUS_ACCOUNTING_ERROR,
                Constants::INVOICE_STATUS_MANAGER_APPROVAL_PENDING,
                Constants::INVOICE_STATUS_ACCOUNTING_PENDING,
                Constants::INVOICE_STATUS_VALIDATION_PENDING,
                Constants::INVOICE_STATUS_IS_NOT_AN_INVOICE
            ];
        }

        //Filters saved on session
        if (session()->has('search')) {
            foreach ($this->search as $key => $filter) {
                if (session()->has('search.'.$key) == $key) {
                    $this->search[$key] = session()->get('search.'.$key)[0];
                }
            }
        }
    }

    /**
     * Runs after any update to the Livewire component's data
     * (Using wire:model, not directly inside PHP)
     *
     * @param string $propertyName
     * @param mixed $value
     * @return void
     */
    public function updated($propertyName, $value)
    {
        session()->forget($propertyName);

        //Save the search filters on user session
        if ($value != ''){
            session()->push($propertyName, $value);
        }

        if (empty(session('search'))) {
            session()->forget('search');
        }
    }

    /**
     * Deletes all the previous saved filters on the user session
     * and reset the filters array $search
     * @var array $search
     *
     * @return void
     */
    public function clearFilters()
    {
        session()->forget('search');
        $this->search = [
            "sapCompany"            => '',
            "providerName"          => '',
            "vatNumber"             => '',
            "invoiceNumber"         => '',
            "fromDate"              => '',
            "toDate"                => '',
            "fromDatePayment"       => '',
            "toDatePayment"         => '',
            "fromContabDate"        => '',
            "toContabDate"          => '',
            "totalNetMin"           => '',
            "totalNetMax"           => '',
            "taxesTotalAmountMin"   => '',
            "taxesTotalAmountMax"   => '',
            "totalMin"              => '',
            "totalMax"              => '',
            "paymentDate"           => '',
            "sapDocumentNumber"     => '',
            "internOrder"           => '',
            "status"                => '',
            "currency"              => '',
            "isCorrect"             => '',
            "taxType"               => '',
            "documentType"          => '',
            'unreadMessages'        => ''
        ];
    }

    /**
     * Renders the component view
     *
     * @return mixed
     */
    public function render()
    {
        // Hash the search array combined with the pagination entries to be able to create the right cache tag
        $searchHash = md5(json_encode($this->search)) . '-' . $this->entries;

        // Get invoices from cache or from DB if its not cached yet
        $invoicesQry = Invoice::tableSearch($this->tableType, $this->search, $this->negativeWord)
            ->orderBy($this->sortBy, $this->sortDirection);

        // Determines if cache should be used or not
        (config('app.cache.active'))
            ? $invoices = Cache::remember('invoices-table-search-' . $searchHash, config('custom.global-cache-time-in-seconds'), function () use ($invoicesQry) {
                return $invoicesQry->paginate($this->entries);
            })
            : $invoices = $invoicesQry->paginate($this->entries);

        // Check if the invoice is correct to show it well through the filter.
        foreach ($invoices as $invoice) {
            $invoice->is_correct = $invoice->checkInvoice();
            $invoice->save();
        }

        $statusTypes = Lang::get('status');
        asort($statusTypes);

        return view('livewire.invoices.table',[
            'invoices' => $invoices,
            'sapCompanies' => Invoice::where('sap_company', 1000)
                            ->orWhere('sap_company', 2000)
                            ->pluck('customer_name', 'sap_company'),
            'statusTypes' => $statusTypes,
            'taxes' => TaxType::orderBy('type', 'asc')->get(),
        ]);
    }

    /**
     * Changes the $sortBy and the $sortDirection values
     * @var string $sortBy
     * @var string $sortDirection
     *
     * @param string $field
     * @return void
     */
    public function sortBy($field){
        if ($this->sortDirection == 'asc') {
            $this->sortDirection = 'desc';
        } else {
            $this->sortDirection = 'asc';
        }

        $this->sortBy = $field;
    }

    /**
     * Runs before any update to the Livewire component's data
     * (Using wire:model, not directly inside PHP)
     *
     * @return void
     */
    public function updating()
    {
        $this->resetPage();
    }

    /**
     * Generates a file with xlsx (Excel) format
     *
     * @param $path
     * @return object
     * @throws \Box\Spout\Common\Exception\IOException
     * @throws \Box\Spout\Common\Exception\InvalidArgumentException
     * @throws \Box\Spout\Common\Exception\UnsupportedTypeException
     * @throws \Box\Spout\Writer\Exception\WriterNotOpenedException
     */
    public function exportToExcel($path)
    {
        $data = Invoice::tableSearch($this->tableType, $this->search, $this->negativeWord)
            ->orderBy($this->sortBy, $this->sortDirection)
            ->get();

        return (new FastExcel($data))->export($path, function ($invoice){
            return [
                'Proveedor'         => $invoice->provider_name,
                'NIF/VAT'           => $invoice->vat_number,
                'Número factura'    => $invoice->invoice_number,
                'Fecha factura'     => $invoice->date,
                'Importe Neto'      => $invoice->total_net,
                'IVA'               => $invoice->taxes_total_amount,
                'Porcentaje'        => $invoice->taxes_percent.'%',
                'Importe Total'     => $invoice->total,
                'Divisa'            => $invoice->currency,
                'Fecha de pago'     => $invoice->payment_date,
                'Pedido'            => $invoice->intern_order
            ];
        });
    }

    /**
     * Deletes Excel export file if exists
     *
     * @return void
     */
    public function deleteOldExcelFile()
    {
        //Delete Excel export file if exists
        $file = public_path() . "/invoices.xlsx";
        if (file_exists($file)) {
            unlink($file);
        }
    }

    /**
     * Downloads excel file generated in exportToExcel()
     *
     * @return mixed
     */
    public function downloadExcel()
    {
        $path = tempnam(sys_get_temp_dir(), "FOO");
        $this->exportToExcel($path);

        return response()->download($path, 'invoices.xlsx', [
            'Content-Type'          => 'application/vnd.ms-excel',
            'Content-Disposition'   => 'inline; filename="invoices.xlsx"'
        ]);
    }

    /**
     * Generates and download a PDF file
     *
     * @return mixed
     */
    public function exportToPDF()
    {
        $data = [
            'invoices' => Invoice::tableSearch($this->tableType, $this->search, $this->negativeWord)
                        ->orderBy($this->sortBy, $this->sortDirection)
                        ->get(),
        ];

        $pdf = PDF::loadView('exports.pdf-export', $data)
            ->setPaper('a4', 'landscape')
            ->output();

        return response()->streamDownload(
            fn () => print($pdf), 'invoices.pdf');
    }

    /**
     * Generates and downloads a ZIP file with all Invoices previously filtered
     * @return bool
     * @throws \Illuminate\Contracts\Filesystem\FileNotFoundException
     */
    public function downloadZip()
    {
        $invoices = Invoice::tableSearch($this->tableType, $this->search, $this->negativeWord)
            ->orderBy($this->sortBy, $this->sortDirection)
            ->get();

        $fileName   = 'invoices.zip';
        $zipFile    = Storage::disk('tmp')->url($fileName);
        $zip        = new \ZipArchive();
        $awsPath    = config('custom.iworking_public_bucket_folder_invoices') . '/downloads';

        if ($zip->open($zipFile, ZipArchive::CREATE) === true) {
            foreach ($invoices as $invoice) {
                if (Storage::disk('s3')->exists($invoice->aws_route)) {
                    $zip->addFromString($invoice->provider_name . '-' . $invoice->invoice_number.'.pdf', Storage::disk('s3')->get($invoice->aws_route));
                }
            }

            $zip->close();

            Storage::disk('s3')->putFileAs($awsPath, $zipFile, $fileName);
        }

        if (Storage::disk('s3')->exists($awsPath . DIRECTORY_SEPARATOR . $fileName)) {

            return Storage::disk('s3')->download($awsPath . DIRECTORY_SEPARATOR . $fileName);
            /*return response()->streamDownload(
                fn () => print(Storage::disk('s3')->get($awsPath . DIRECTORY_SEPARATOR . $fileName)),
                $fileName
            );*/
        }

        return false;
    }
}
