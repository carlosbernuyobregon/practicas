<?php

namespace App\Http\Livewire\Invoices;

use Livewire\Component;

class StatusChanger extends Component
{
    public function render()
    {
        return view('livewire.invoices.status-changer');
    }
}
