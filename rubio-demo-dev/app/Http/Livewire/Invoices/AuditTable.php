<?php

namespace App\Http\Livewire\Invoices;

use App\Invoice;
use Livewire\Component;
use Livewire\WithPagination;

class AuditTable extends Component
{
    use WithPagination;

    protected $paginationTheme = 'bootstrap';

    protected $listeners = [
        'rechargeInvoice' => 'getInvoiceProperty',
        'rechargeInvoiceLine' => 'getInvoiceProperty'
    ];

    public $invoice = null;

    public $entries = 10;

    /**
     * In Livewire components, you use mount() instead of a class constructor __construct() 
     * like you may be used to.
     * @param mixed $name
     * @return void
     */
    public function mount($invoice = null)
    {
        $this->invoice = $invoice;
    }

    /**
     * Renders the component view
     *
     * @return mixed
     */
    public function render()
    {
        return view('livewire.invoices.audit-table',[
            'auditLines' => $this->invoice->audit()->paginate($this->entries),
        ]);
    }

    /**
     * Reloads the $invoice object
     * @var object $invoice
     *
     * @return void
     */
    public function getInvoiceProperty()
    {
        $this->invoice = Invoice::find($this->invoice->id);
    }
}
