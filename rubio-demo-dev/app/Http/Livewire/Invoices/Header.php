<?php

namespace App\Http\Livewire\Invoices;

use App\Currency;
use App\Helpers\Helpers;
use App\Invoice;
use App\Library\Constants;
use App\Mail\invoicesAskInfo;
use App\Rules\InvoiceDateGreaterThanToday;
use App\Rules\NotEmptyValue;
use App\TaxType;
use App\User;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;
use Livewire\Component;

class Header extends Component
{
    public ?Invoice $invoice = null;
    public $users = null;
    public $editMode = false;
    public $editCommentsMode = false;
    public $editDocSAPMode = false;
    public $askForInfoMode = false;
    public $askForInfoUserId = null;
    public $askForInfoEmail = '';
    public $askForInfoCopy = false;
    public $emailSent = false;
    public $view = false;
    public $previousTax = '';
    public $message = '';
    public $error = '';

    public $isInvoiceCorrect        = false;
    public $areInvoiceLinesCorrect  = false;
    public $invoiceErrors           = false;
    public $invoiceWarnings         = false;
    public $hasMoreThanOneLineTax   = false;

    public $originalMissedDataError     = false;
    public $originalTotalError          = false;
    public $originalTotalTaxesError     = false;
    public $originalTotalIrpfError      = false;
    public $baseForIrpfError            = false;
    public $paymentDateVTypesError      = false;
    public $unknownProviderError        = false;
    #INVALIDATION
    public $invalidationReasons;
    public $invalidationConfirmDisabled;
    public $chosenInvalidationReason;
    public $textInvalidationReason;
    public $textInvalidationReasonRequired;
    public $confirmInvalidation;
    public $notValidStatusToInvalidate;
    #INVALIDATION

    protected $listeners = [
        'rechargeInvoice'       => 'getInvoiceProperty',
        'rechargeInvoiceLine'   => 'getInvoiceProperty',
        'checkInvoice'          => 'updateCheckInvoice',
        'refreshComponent'      => '$refresh',
        'render'                => 'render'
    ];

    /**
     * In Livewire components, you use mount() instead of a class constructor __construct()
     * like you may be used to.
     *
     * @param mixed $invoice
     * @param boolean $view
     * @return void
     */
    public function mount(Invoice $invoice = null, $view = false)
    {
        $this->invoice                  = $invoice;
        // Lazy load relationship
        $this->invoice->load(['invoiceLines', 'invoiceLinesLite']);
        $this->users                    = User::all();
        $this->view                     = $view;
        $this->isInvoiceCorrect         = $this->invoice->checkInvoice();
        $this->areInvoiceLinesCorrect   = $this->invoice->checkInvoiceLines();
        list (
            $this->invoiceErrors,
            $this->invoiceWarnings
            )                           = $this->invoice->checkInvoice(true);
        $this->previousTax              = $invoice->taxes_type;
        $this->message                  = '';
        $this->error                    = '';
        $this->hasMoreThanOneLineTax    = $this->prepareMoreThanOneTaxTypeInLinesData();
        $this->initTableValuesErrors();
        $this->reloadInvalidationData();
    }

    /**
     * Renders the component view
     *
     * @return mixed
     */
    public function render()
    {
        return view('livewire.invoices.header',[
            'taxes'         => TaxType::orderBy('type','asc')->get(),
            'currencies'    => Currency::whereIn('key_value', explode(',', config('custom.currencies-short-list')))->get()
        ])
            ->with('message', $this->message)
            ->with('error', $this->error);
    }

    /**
     * Runs after vat_number property is updated
     *
     * @param $value
     */
    public function updatedInvoiceVatNumber($value) : void
    {
        // Get the new provider name
        $provider = Helpers::giveMeTheProvider([
            'TaxRegistrationNumber' => $value
        ]);

        if ($provider) {
            $this->invoice->provider_name = $provider->name;
        } else {
            $this->invoice->provider_name   = null;
            $this->unknownProviderError     = 'El NIF no corresponde a ningún Proveedor';
        }

    }

    /**
     * Runs after any update to the Livewire component's data
     * (Using wire:model, not directly inside PHP)
     *
     * @param string $propertyName
     * @return void
     * @throws \Illuminate\Validation\ValidationException
     */
    public function updated($propertyName)
    {
        $this->validateOnly($propertyName);
        $this->isInvoiceCorrect         = $this->invoice->checkInvoice();
        $this->areInvoiceLinesCorrect   = $this->invoice->checkInvoiceLines();
        list (
            $this->invoiceErrors,
            $this->invoiceWarnings
            )                           = $this->invoice->checkInvoice(true);
        $this->initTableValuesErrors();
        // Lazy load the relationship to find out if there is more than one tax type applied to invoice lines
        $this->invoice->load('invoiceLines');
        $this->hasMoreThanOneLineTax    = $this->prepareMoreThanOneTaxTypeInLinesData();
    }

    /*public function updatingInvoiceTaxesIrpfAmount($value)
    {
        $this->validateOnly('invoice.taxes_irpf_amount');
    }*/

    /**
     * Runs after checkInvoice event listener.
     * Call the $invoice->checkInvoice() to check if
     * the invoice is correct with the new changes on the $invoice object
     *
     * @return void
     */
    public function updateCheckInvoice()
    {
        $this->isInvoiceCorrect         = $this->invoice->checkInvoice();
        $this->areInvoiceLinesCorrect   = $this->invoice->checkInvoiceLines();
        list (
            $this->invoiceErrors,
            $this->invoiceWarnings
            )                           = $this->invoice->checkInvoice(true);

        $this->updateTaxesPerLineProperty();
        $this->initTableValuesErrors();

        if (session()->has('message')) {
            session()->pull('message', 'Datos actualizados correctamente.');
        }
    }

    /**
     * Updates the property that determines if the invoice has single or multiple tax types
     */
    public function updateTaxesPerLineProperty()
    {
        $this->hasMoreThanOneLineTax = $this->prepareMoreThanOneTaxTypeInLinesData();
    }

    /**
     * Runs after rechargeInvoice and rechargeInvoiceLine events listeners.
     * Reloads the $invoice object
     * @var object $invoice
     *
     * @param boolean $showMessage
     * @return void
     */
    public function getInvoiceProperty($showMessage = false)
    {
        $this->invoice = Invoice::find($this->invoice->id);
        if ($showMessage) {
            session()->put('message', 'Datos actualizados correctamente.');
        }
        $this->updateTaxesPerLineProperty();
        $this->updateCheckInvoice();
    }

    public function convertSpecialHeaderItemsIntoLines()
    {
        // Move header items to lines
        $this->invoice->convertSpecialHeaderItemsIntoLines();
    }

    /**
     * Saves the $invoice object data and makes the audit
     *
     * @return void
     */
    public function save()
    {
        $this->validate();

        // Trigger header items conversion (things that should be issued as lines. By now we only do this for header discounts)
        // Lets keep this commented until the second part (header items converted to lines) is tackled
        // $this->convertSpecialHeaderItemsIntoLines();

        // Trigger recalculation logic for invoice
        $this->invoiceRecalculationCommons();

        // Check if Taxes are being modified and eventually recalculate totals for the line
        if ($this->previousTax != $this->invoice->taxes_type) {
//            $this->changeInvoiceLinesTaxes(); // Deprecated
        }

        $this->mode();
        //$this->invoice->doAudit('save');
        $this->invoice->audit()->create([
            'user_id' => auth()->user()->id,
            'status' => $this->invoice->status,
            'text' => __('auditMessages.save')
        ]);
        $this->emit('rechargeInvoice', true);
        $this->message = 'Datos actualizados correctamente.';
        $this->render();
    }

    /**
     * Runs when the user change tax type input. Then this function
     * changes the tax type of the associate invoiceLines
     *
     * @return void
     * @deprecated
     */
    public function changeInvoiceLinesTaxes()
    {
        // Lazy load the relationship
        $this->invoice->load('invoiceLines');

        // Default values, not the best option cos is a static data representing a value given by a relationship
        $taxesId            = $this->invoice->taxes_id;
        $lineTax            = $this->invoice->taxes_percent;
        // ToDo: warning, keep this as false in order to keep going through the normal flow and don't apply header discount on lines
//        $invoiceHasDiscount = false; // ($this->invoice->discount_amount) ? true : false;

        // Check if relationship exists
        if (!empty($this->invoice->tax)) {
            // Better if we keep in line with what relationship says about the type
            $taxesId = $this->invoice->tax->type;
            $lineTax = $this->invoice->tax->percentage;
        }

        $discountToBeAddedPerLine = false;

        /*if ($invoiceHasDiscount) {
            $discountAllLines           = $this->invoice->invoiceLines->sum('discount_amount');
            $existentDiscounts          = $this->invoice->invoiceLines->where('discount_amount', '!=', 0)->count();
            $linesCount                 = $this->invoice->invoiceLines->count();
            $invoiceDiscountToBeApplied = $this->invoice->discount_amount - $discountAllLines;

            if (abs(round($invoiceDiscountToBeApplied, 0)) != 0) {
                $discountToBeAddedPerLine = $invoiceDiscountToBeApplied / ($linesCount - $existentDiscounts);
            }
        }*/

        // Get the tax type uniqueness
        $uniqueTaxTypeId = $this->invoice->invoiceLines->unique('tax_type_id');

        // Only perform header tax type inheritance to lines if there is a unique tax type applied to them
        if ($uniqueTaxTypeId->pluck('tax_type_id')->count() == 1) {
            foreach ($this->invoice->invoiceLines as $line) {
                $line->taxes_id     = $taxesId;
                $line->tax_type_id  = $this->invoice->taxes_type;
                $line->taxes        = $lineTax;

                if ($line->discount_amount == 0) {
                    $line->discount_amount = $discountToBeAddedPerLine;
                }

                $line->recalculateTotals();
            }

            // Call recalculate method for those broke down values (total_net_2, total_net_3, etc)
            $this->invoice->recalculateTotalsAndTaxesForBreakDownFields();
        }
    }

    /**
     * Takes care of totals recalculation and value setting for invoice
     */
    private function invoiceRecalculationCommons()
    {
        // Lazy load the relationship
        $this->invoice->load('tax');

        // Check if relationship exists
        if (!empty($this->invoice->tax)) {
            // Assign the new tax_type pulled from relationship
            $this->invoice->taxes_id        = $this->invoice->tax->type;
            $this->invoice->taxes_percent   = $this->invoice->tax->percentage;
        }

        // Recalculate totals
        $this->invoice->recalculateTotals();

        // Save model
//        $this->invoice->save(); // Not needed anymore, the saving is being triggered inside the method
    }

    /**
     * Triggers a recalculation for totals and taxes values
     */
    public function recalculateTotals()
    {
        // Trigger recalculation logic for invoice
        $this->invoiceRecalculationCommons();
        // Trigger recalculation logic for invoice lines
        // $this->changeInvoiceLinesTaxes(); // Deprecated
        $this->emit('rechargeInvoice', true);
        $this->message = 'Totales e impuestos han sido recalculados';
        $this->render();
    }

    /**
     * Changes the invoice status to validation pending and delete the reference to the original invoice
     */
    public function markAsNotDuplicated()
    {
        // Check if the current status is duplicated
        if ($this->invoice->status == 13) {
            $this->invoice->status          = 1;
            $this->invoice->duplicated_from = null;
            $this->invoice->save();
            $this->emit('rechargeInvoice', true);
            $this->message = 'La factura a sido desmarcada como duplicada';
            $this->render();
        }
    }

    /**
     * Revert the changes on the $invoice object
     * @var object $invoice
     *
     * @return void
     */
    public function cancel()
    {
        //This avoids an error with number_format($invoice->total_net,2,',','.')
        if ($this->invoice->total_net == "") {
            $this->invoice->total_net = 0;
        }
        $this->mode();
        $this->resetValidation();
    }

    /**
     * Changes the $editMode value, the $editCommentsMode or $editDocSAPMode
     * @var bool $editMode
     *
     * $invoice->status == 9 //only Comments can be edited (when status is 9)
     * $invoice->status == 11 //only comments and SAP document can be edited (when status is 11)
     * else // All inputs can be edited
     * @return void
     */
    public function mode()
    {
        if ($this->invoice->status == 9) {
            $this->editCommentsMode = !$this->editCommentsMode;
        } elseif (in_array($this->invoice->status, [11, 5])) {
            $this->editDocSAPMode   = !$this->editDocSAPMode;
            $this->editCommentsMode = !$this->editCommentsMode;
        } else {
            $this->editMode = !$this->editMode;
        }

        //Resets the inputs
        if (!$this->editMode || !$this->editCommentsMode || !$this->editDocSAPMode) {
            $this->emit('rechargeInvoice');
        }
    }

    /**
     * Changes the $askForInfoMode value
     * @var bool $askForInfoMode
     *
     * @return void
     */
    public function askForInfo()
    {
        $this->askForInfoMode = !$this->askForInfoMode;
    }

    /**
     * Check if the Invoice meets the requirements to be approved
     *
     * @return bool
     */
    public function invoiceCanBeApproved()
    {
        if ($this->invoice->checkInvoice() && $this->invoice->vat_number != null && $this->invoice->vat_number != "[]") {
            return true;
        } else {
            session()->flash('error', 'Faltan datos para poder aprobar la factura o los importes son incorrectos');
        }
        return false;
    }

    /**
     * Check if the Invoice meets the requirements to be approved
     *
     * @return bool
     */
    public function invoiceCanBeApprovedOnlyCheckingDataAndNoTotals()
    {
        if (
            ($this->invoice->intern_order != null || $this->invoice->intern_order != '') &&
            $this->invoice->date != null &&
            $this->invoice->taxes_type != null &&
            $this->invoice->vat_number != null &&
            $this->invoice->vat_number != "[]"
        ) {
            return true;
        } else {
            $details    = [];
            $detailsTxt = false;

            if ($this->invoice->intern_order == null || $this->invoice->intern_order == '') {
                $details = Arr::prepend($details, 'Nro pedido');
            }

            if ($this->invoice->date == null) {
                $details = Arr::prepend($details, 'Fecha factura');
            }

            if ($this->invoice->taxes_type == null) {
                $details = Arr::prepend($details, 'Tipo IVA');
            }

            if ($this->invoice->vat_number == null || $this->invoice->vat_number == '' || $this->invoice->vat_number == "[]") {
                $details = Arr::prepend($details, 'CIF/NIF/VAT');
            }

            if (!empty($details)) {
                $detailsTxt = implode(' - ', $details);
            }

            $errorMsg = 'Faltan datos para poder aprobar la factura';

            if ($detailsTxt) {
                $errorMsg .= ' (' . $detailsTxt . ')';
            }
            session()->flash('error', $errorMsg);
        }

        return false;
    }

    /**
     * Changes the $invoice status to approved (2)
     *
     * @return void
     */
    public function approve()
    {
        if ($this->invoiceCanBeApproved()) {
            $this->invoice->status = 2;
            $this->invoice->save();
            session()->flash('message', 'Factura aprobada.');
            //$this->invoice->doAudit('approve');
            $this->invoice->audit()->create([
                'user_id' => auth()->user()->id,
                'status' => $this->invoice->status,
                'text' => __('auditMessages.approve')
            ]);
            $this->emit('rechargeInvoice');
            $this->render();
        }
    }

    /**
     * Changes the $invoice status to authorized (9)
     *
     * @return void
     */
    public function authorize()
    {
        if ($this->invoiceCanBeApprovedOnlyCheckingDataAndNoTotals()) {
            $this->invoice->status = 9;
            $this->invoice->save();
            session()->flash('message', 'Autorización solicitada.');
            //$this->invoice->doAudit('authorize');
            $this->invoice->audit()->create([
                'user_id' => auth()->user()->id,
                'status' => $this->invoice->status,
                'text' => __('auditMessages.authorize')
            ]);
            $this->emit('rechargeInvoice');
            $this->render();
        }
    }

    /**
     * Changes the $invoice status to blocked (4)
     *
     * @return void
     */
    public function block()
    {
        $this->invoice->status = 4;
        $this->invoice->save();
        session()->flash('message', 'Factura bloqueada.');
        //$this->invoice->doAudit('block');
        $this->invoice->audit()->create([
            'user_id' => auth()->user()->id,
            'status' => $this->invoice->status,
            'text' => __('auditMessages.block')
        ]);
        $this->emit('rechargeInvoice');
        $this->render();
    }

    /**
     * Changes the $invoice status to unblocked (1)
     *
     * @return void
     */
    public function unblock()
    {
        $this->invoice->status                  = 1;
        $this->invoice->alert_flag_from_ocr     = null;
        $this->invoice->save();
        session()->flash('message', 'Factura desbloqueada.');
        //$this->invoice->doAudit('unblock');
        $this->invoice->audit()->create([
            'user_id' => auth()->user()->id,
            'status' => $this->invoice->status,
            'text' => __('auditMessages.unblock')
        ]);
        $this->emit('rechargeInvoice');
        $this->render();
    }

    /**
     * Changes the $invoice status to unapproved (1)
     *
     * @return void
     */
    public function unapprove()
    {
        $this->invoice->status = 1;
        $this->invoice->save();
        session()->flash('message', 'Aprobación retirada.');
        //$this->invoice->doAudit('unapprove');
        $this->invoice->audit()->create([
            'user_id' => auth()->user()->id,
            'status' => $this->invoice->status,
            'text' => __('auditMessages.unapprove')
        ]);
        $this->emit('rechargeInvoice');
        $this->render();
    }

    /**
     * Changes the $invoice status to manual accounted (11)
     *
     * @return void
     */
    public function manualAccounting()
    {
        if ($this->invoiceCanBeApprovedOnlyCheckingDataAndNoTotals()) {
            $this->invoice->status = 11;
            $this->invoice->save();
            session()->flash('message', 'Factura contabilizada manualmente');
            //$this->invoice->doAudit('manual');
            $this->invoice->audit()->create([
                'user_id' => auth()->user()->id,
                'status' => $this->invoice->status,
                'text' => __('auditMessages.manual')
            ]);
            $this->emit('rechargeInvoice');
            $this->render();
        }
    }

    /**
     * Changes the $invoice status to manual accounted (11)
     *
     * @return void
     */
    public function manualAccountingApproved()
    {
        if ($this->invoiceCanBeApprovedOnlyCheckingDataAndNoTotals()) {
            $this->invoice->status = 12;
            $this->invoice->save();
            session()->flash('message', 'Factura aprobada para contabilizar manualmente');
            //$this->invoice->doAudit('approveManual');
            $this->invoice->audit()->create([
                'user_id' => auth()->user()->id,
                'status' => $this->invoice->status,
                'text' => __('auditMessages.approveManual')
            ]);
            $this->emit('rechargeInvoice');
            $this->render();
        }
    }

    /**
     * Sends an email asking for $invoice info
     *
     * @return void
     */
    public function sendEmail()
    {
        $users = [];
        if($this->askForInfoUserId != null){
            $user = User::find($this->askForInfoUserId);
            $users[] = $user->email;
        }
        if($this->askForInfoEmail != null){
            $users[] = $this->askForInfoEmail;
        }
        if($this->askForInfoCopy == true){
            $users[] = Auth::user()->email;
        }
        if(count($users)>0){
            foreach($users as $userEmail){
                try {
                    Mail::to($userEmail)->send(new invoicesAskInfo($this->invoice));
                } catch (\Exception $e) {
                    Log::error($e->getMessage(), [
                        'class'     => __CLASS__,
                        'method'    => __METHOD__,
                    ]);
                    $this->error = 'Algo ha ido mal con el envío del mail';
                }

            }
        }
        $this->askForInfoUserId = null;
        $this->askForInfoEmail = '';
        $this->askForInfoCopy = false;
        $this->emailSent = true;
        $this->addError('email.sent', 'Mensaje enviado correctamente');
        session()->flash('message', 'Email enviado correctamente.');
        ($this->error) ?: $this->message = 'Email enviado correctamente';
        //$this->invoice->doAudit('sendEmail');
        $this->invoice->audit()->create([
            'user_id' => auth()->user()->id,
            'status' => $this->invoice->status,
            'text' => __('auditMessages.sendEmail')
        ]);
        $this->block();
        $this->emit('rechargeInvoice');
    }

    /**
     * Checks if there is more than one tax type applied to lines and return label to be used on the table to indicate
     * which tax types are applied
     *
     * @return boolean | string
     */
    private function prepareMoreThanOneTaxTypeInLinesData()
    {
        return $this->invoice->giveMeConcatTaxTypes();
    }

    /**
     * Gets the validation rules
     * @return array
     */
    public function rules()
    {
        // Check if tax type is required depending on single or multiple tax types
        $taxTypeRequired = true;

        if ($this->prepareMoreThanOneTaxTypeInLinesData()) {
            // Lets keep this commented until the second part (header items converted to lines) is tackled
            //$taxTypeRequired = false;
        }

        // Make payment date required if invoice has at least one line with V (Tracto Sucesivo) tax type
        (Helpers::atLeastOneLineWithTaxType($this->invoice, 'V')) ? $customPaymentDateRules = 'required|date' : $customPaymentDateRules = 'nullable|date';

        return [
            'invoice.date'                                  => ['required', 'date', new InvoiceDateGreaterThanToday()],
            'invoice.operation_date'                        => 'nullable|date',
            'invoice.payment_date'                          => $customPaymentDateRules,
            'invoice.vat_number'                            => 'required|string',
            'invoice.provider_name'                         => 'required|string',
            'invoice.invoice_number'                        => 'required|string',
            'invoice.total_net'                             => 'required|numeric',
            'invoice.taxes_total_amount'                    => 'required|numeric',
            'invoice.taxes_type'                            => ($taxTypeRequired) ? 'required|string' : 'sometimes',
            'invoice.currency'                              => 'required|string',
            'invoice.taxes_irpf_amount'                     => 'required|numeric',
            'invoice.total_net_for_non_zero_tax_percentage' => 'required|numeric',
//            'invoice.tax_balance_range'                   => 'required|numeric|min:-2|max:2',
            'invoice.intern_order'                          => 'required|string|max:50',
            'invoice.sap_company'                           => 'required|string',
            'invoice.document_type'                         => 'required|string',
            'invoice.exchange_type'                         => 'required_unless:invoice.currency,EUR|numeric|min:0',
            'invoice.discount_amount'                       => 'required|numeric',
            'invoice.sap_document_number'                   => 'string|nullable',
            'invoice.lotus_id'                              => 'string|nullable',
            'invoice.comercial_sales'                       => 'string|nullable',
            'invoice.comments'                              => 'string|nullable',
            'invoice.contabilized_at'                       => 'date|nullable',
            'invoice.tax_balance_range_2'                   => ['required', 'numeric', 'min:-2', 'max:2', new NotEmptyValue()],
            'invoice.tax_balance_range_3'                   => ['required', 'numeric', 'min:-2', 'max:2', new NotEmptyValue()],
            'invoice.tax_balance_range_4'                   => ['required', 'numeric', 'min:-2', 'max:2', new NotEmptyValue()],
            'invoice.tax_balance_range_5'                   => ['required', 'numeric', 'min:-2', 'max:2', new NotEmptyValue()],
            'invoice.adjusted_tax_amount_2'                 => ['required', 'numeric', new NotEmptyValue()],
            'invoice.adjusted_tax_amount_3'                 => ['required', 'numeric', new NotEmptyValue()],
            'invoice.adjusted_tax_amount_4'                 => ['required', 'numeric', new NotEmptyValue()],
            'invoice.adjusted_tax_amount_5'                 => ['required', 'numeric', new NotEmptyValue()]
        ];
    }

    /**
     * Assign errors coming from model
     */
    private function initTableValuesErrors() : void
    {
        $this->originalMissedDataError  = $this->invoice->originalMissedDataError;
        $this->originalTotalError       = $this->invoice->originalTotalError;
        $this->originalTotalTaxesError  = $this->invoice->originalTotalTaxesError;
        $this->originalTotalIrpfError   = $this->invoice->originalTotalIrpfError;
        $this->baseForIrpfError         = $this->invoice->baseForIrpfError;
        $this->paymentDateVTypesError   = $this->invoice->paymentDateVTypesError;
        $this->unknownProviderError     = false;
    }

    /**
     * START INVALIDATION METHODS
     */

    /**
     * Reloads invoice invalidation data
     */
    public function reloadInvalidationData() : void
    {
        $this->invalidationReasons              = config('custom.invoice-invalidation-reasons');
        $this->notValidStatusToInvalidate       = config('custom.invoice-invalidation-forbidden-status');
        $this->invalidationConfirmDisabled      = true;
        $this->confirmInvalidation              = false;
        $this->chosenInvalidationReason         = null;
        $this->textInvalidationReason           = null;
        $this->textInvalidationReasonRequired   = null;
    }

    /**
     * Confirms the user is aware of invalidate the invoice
     */
    public function confirmInvalidation() : void
    {
        $this->confirmInvalidation = true;
    }

    /**
     * Performs invoice invalidation
     */
    public function invalidateInvoice() : void
    {
        if ($this->confirmInvalidation && $this->invoiceCanBeInvalidated()) {
            // Do invalidation stuff
            if ($this->chosenInvalidationReason) {
                // Save reason and optional text
                $this->invoice->invalidation_reason             = $this->chosenInvalidationReason;
                $this->invoice->invalidation_reason_extended    = $this->textInvalidationReason;
                // Change status and save
                $this->invoice->status = Constants::INVOICE_STATUS_INVALID;

                try {
                    // Save
                    $this->invoice->save();
                    $this->message = 'La factura se ha invalidado correctamente.';

                    // Prepare audit info
                    $auditTxt = __('backend.forms.invoice.invalidation.reasons.' . $this->chosenInvalidationReason);
                    (!$this->textInvalidationReason) ?: $auditTxt .=  ' - ' . $this->textInvalidationReason;

                    // Audit
                    $this->invoice->audit()->create([
                        'user_id'   => auth()->user()->id,
                        'status'    => $this->invoice->status,
                        'text'      => $auditTxt
                    ]);
                } catch (\PDOException $e) {
                    Log::error($e->getMessage(), [
                        'class'     => __CLASS__,
                        'method'    => __METHOD__,
                    ]);
                    $this->error = 'Algo ha ido mal cambiando el estado de la factura';
                }
            }
        } else {
            $this->error = 'No es posible invalidar la factura dado su estado actual';
        }

        $this->hideInvalidationModal();
        $this->reloadInvalidationData();
        $this->emit('rechargeInvoice');
    }

    /**
     * Shows invalidation modal
     */
    public function showInvalidationModal() : void
    {
        $this->dispatchBrowserEvent('showInvalidationModal');
    }

    /**
     * Hides invalidation modal
     */
    public function hideInvalidationModal() : void
    {
        $this->dispatchBrowserEvent('hideInvalidationModal');
    }

    /**
     * Checks if invoice can be invalidated taking into account the current status
     * @return bool
     */
    public function invoiceCanBeInvalidated()
    {
        if (!in_array($this->invoice->status, $this->notValidStatusToInvalidate)) {
            return true;
        }

        return false;
    }

    /**
     * Runs after chosenInvalidationReason property is updated
     *
     * @param $value
     */
    public function updatedChosenInvalidationReason($value) : void
    {
        switch ($value) {
            case 'reason-others':
                $this->invalidationConfirmDisabled      = true;
                $this->textInvalidationReasonRequired   = true;
                break;
            default:
                $this->invalidationConfirmDisabled      = ($value) ? false : true;
                $this->textInvalidationReasonRequired   = false;
                break;
        }
    }

    /**
     * Runs after textInvalidationReason property is updated
     *
     * @param $value
     */
    public function updatedTextInvalidationReason($value) : void
    {
        if ($value && strlen($value) > 5) {
            $this->invalidationConfirmDisabled      = false;
            $this->textInvalidationReasonRequired   = false;
        } else {
            $this->invalidationConfirmDisabled      = true;
            $this->textInvalidationReasonRequired   = true;
        }
    }

    /**
     * END INVALIDATION METHODS
     */
}
