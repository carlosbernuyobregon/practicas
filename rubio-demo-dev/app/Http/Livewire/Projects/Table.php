<?php

namespace App\Http\Livewire\Projects;

use App\Country;
use App\Project;
use App\Quote as AppQuote;
use App\QuoteLine;
use App\Request as AppRequest;
use App\User;
use Illuminate\Support\Facades\DB;
use Livewire\Component;
use Livewire\WithPagination;

class Table extends Component
{
    use WithPagination;

    public $results = 10;

    public $projectType = null;
    public $projectStatus = null;
    public $customer = null;
    public $country = null;
    public $businessManager = null;
    public $minUnits = 0;
    public $maxUnits = 0;
    public $minTargetDate = 0;
    public $maxTargetDate = 0;
    public $designer = null;
    public $searchTerm = null;

    public $validate = false;

    public function mount($toValidate = false)
    {
        $this->validate = $toValidate;
    }
    public function render()
    {
        $query = Project::query();
        $query->where('parent_id','!=',null);
        if ($this->projectType != null) {
            $query->where('project_type', $this->projectType);
        }
        if ($this->projectStatus != null) {
            $query->where('status', $this->projectStatus);
        }
        if ($this->country != null) {
            $requests = DB::table('requests')->where('country_id', $this->country)->pluck('id');
            $quotes = AppQuote::whereIn('request_id', $requests)->pluck('id');
            $query->whereIn('quote_id', $quotes);
        }
        if ($this->businessManager != null) {
            $requests = DB::table('requests')->where('created_by', $this->businessManager)->pluck('id');
            $quotes = AppQuote::whereIn('request_id', $requests)->pluck('id');
            $query->whereIn('quote_id', $quotes);
        }
        if ($this->minUnits != 0) {
            $quoteLines = QuoteLine::where('qty', '>=', $this->minUnits)->pluck('id');
            $projects = Project::whereIn('quote_line_id', $quoteLines)->pluck('parent_id');
            $query->whereIn('id', $projects);
        }
        if ($this->maxUnits != 0) {
            $quoteLines = QuoteLine::where('qty', '<=', $this->maxUnits)->pluck('id');
            $query->whereIn('quote_line_id', $quoteLines);
        }
        if ($this->minTargetDate != 0) {
            $query->where('target_date', '>=', $this->minTargetDate);
        }
        if ($this->maxTargetDate != 0) {
            $query->where('target_date', '<=', $this->maxTargetDate);
        }
        if ($this->designer != null) {
            $query->where('designer_id', $this->designer);
        }
        $user = new User();
        $designers = $user->getUsersByRole('graphic-designer');
        $businessManagers = $user->getUsersByRole('business-manager');
        $countries = Country::orderBy('name','asc')->withTranslation()->get();
        return view(
            'livewire.projects.table',
            [
                'projects' => $query->simplePaginate($this->results),
                'designers' => $designers,
                'businessManagers' => $businessManagers,
                'countries' => $countries,
                'toValidate' => $this->validate,
            ]
        );
    }
}
