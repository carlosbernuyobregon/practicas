<?php

namespace App\Http\Middleware;

use Closure;

class AdministrationRoles
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (auth()->user()->hasRole('administration-user') || auth()->user()->hasRole('administration-manager')) {
            return $next($request);
        }
        return redirect('/dashboard');
    }
}
