<?php

namespace App;

use App\Traits\AutoGenerateUuid;
use Illuminate\Database\Eloquent\Model;

class BudgetLine extends Model
{
    use AutoGenerateUuid;
    public $incrementing = false;
    protected $keyType = 'string';

    protected $fillable = [
        'customer_id',
        'project',
        'pep_element_id',
        'initial',
        'released',
        'rest',
        'available',
        'consumed',
        'compromised',
    ];
}
