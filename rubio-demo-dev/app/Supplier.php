<?php

namespace App;

use App\Traits\AutoGenerateUuid;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Supplier
 * @package App
 * @deprecated
 */
class Supplier extends Model
{
    use AutoGenerateUuid;
    public $incrementing = false;
    protected $keyType = 'string';

    protected $fillable = [
        'customer_id',
        'external_id',
        'vat_number',
        'name',
        'company',
        'payment_condition',
        'payment_type',
    ];

    public function setCustomerIdAttribute($value)
    {
        $this->attributes['customer_id'] = ($value == "") ? null : $value;
    }
    public function files()
    {
        return $this->morphMany('App\File', 'fileable')->orderBy('created_at');
    }
}
