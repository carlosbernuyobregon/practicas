<?php

namespace App;

use App\Traits\AutoGenerateUuid;
use Illuminate\Database\Eloquent\Model;

class Setting extends Model
{
    //
    use AutoGenerateUuid;
    public $incrementing = false;
    protected $keyType = 'string';
    protected $fillable = [
        'key_value',
        'value',
        'count',
        'customer_id'
    ];
    public function getDateFormat()
    {
        if(config('custom.database_type')=='sqlsrv')
        {
            return 'd.m.Y H:i:s';
        }
return parent::getDateFormat();
    }
    public function getTermsConditionsUrl($country = null, $businessLine = null)
    {
        return 'www.rubio.com';
    }
    public function getPrivacyPolicyUrl($country = null, $businessLine = null)
    {
        return 'www.rubio.com';
    }
}
