<?php

namespace App;

use App\Models\BudgetAgrupation;
use App\Traits\AutoGenerateUuid;
use Illuminate\Database\Eloquent\Model;

class Budget extends Model
{
    use AutoGenerateUuid;
    public $incrementing = false;
    protected $keyType = 'string';

    protected $fillable = [
        'customer_id',
        'project',
        'pep_element_id',
        'initial',
        'released',
        'rest',
        'available',
        'consumed',
        'compromised',
        'cost_center_id',
        'name'
    ];

    public function costCenters()
    {
        return $this->belongsToMany(CostCenter::class);
    }

    public function accountingAccount()
    {
        return $this->belongsTo(AccountingAccount::class);
    }
    public function budgetAgrupation()
    {
        return $this->belongsTo(BudgetAgrupation::class);
    }
}
