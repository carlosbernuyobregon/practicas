<?php

namespace App\Rules;

use App\Models\Provider;
use Illuminate\Contracts\Validation\Rule;

class ProviderVat implements Rule
{
    /**
     * Create a new rule instance.
     * @param $company
     * @return void
     */

    /**
     * @var $company
     */
    public $company;

    /**
     * @var $providerId
     */
    public $providerId;

    /**
     * @var $country
     */
    public $country;

    public function __construct($company, $providerId = false, $country = false)
    {
        $this->company      = $company;
        $this->providerId   = $providerId;
        $this->country      = $country;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        if (!$this->company) {
            return false;
        }

        $query = Provider::where('company', $this->company)
            ->where(function ($query) use ($value) {
                $query->where('vat_number', $value)
                    ->orWhere('community_vat_number', $value);
            });

        if ($this->providerId) {
            $query = $query->where('id', '!=', $this->providerId);
        }

        $query = $query->count();

//        dd($query->toSql(), $query->getBindings(), 'ProviderVatUnique');

        return !$query;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        if (!$this->company) {
            $message = 'Seleccione una Sociedad para validar si el VAT de Proveedor existe';
        } else {
            $message = 'Ya existe un Proveedor con el mismo NIF/VAT/NIE. ';
        }

        return $message;
    }
}
