<?php

namespace App\Rules;

use App\Order;
use Illuminate\Contracts\Validation\Rule;
use Illuminate\Support\Carbon;

class OrderDeliveryDateDateGreaterThanOrderDate implements Rule
{
    protected $order;
    /**
     * Create a new rule instance.
     *
     * @param $order
     * @return void
     */
    public function __construct(Order $order)
    {
        $this->order = $order;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $oderDeliveryDate   = Carbon::parse($value)->startOfDay();
        $orderDate          = Carbon::parse($this->order->date)->startOfDay();

        return $oderDeliveryDate->gte($orderDate);
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'La fecha de entrega es inferior a la fecha de pedido';
    }
}
