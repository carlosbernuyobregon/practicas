<?php

namespace App;

use App\Traits\AutoGenerateUuid;
use Illuminate\Database\Eloquent\Model;

class Currency extends Model
{
    use AutoGenerateUuid;
    public $incrementing = false;
    protected $keyType = 'string';

    protected $fillable = [
        'key_value',
        'name',
        'code',
        'customer_id'
    ];

    public function setCustomerIdAttribute($value)
    {
        $this->attributes['customer_id'] = ($value == "") ? null : $value;
    }

    public function countries()
    {
        return $this->hasMany('App\Country');
    }

    public function requests()
    {
        return $this->hasMany('App\Request');
    }

    public function getDateFormat()
    {
        if (config('custom.database_type') == 'sqlsrv') {
            return 'd.m.Y H:i:s';
        }
        return parent::getDateFormat();
    }

    public function scopeGetShortList($query)
    {
        $shortListCodes = explode(',', config('custom.currencies-short-list'));
        $query->whereIn('code', $shortListCodes);
    }
}
