<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Storage;

class ErrorSendingFileToReadSoftWarning extends Mailable
{
    use Queueable, SerializesModels;

    public $data;

    /**
     * Create a new message instance.
     * @param $data
     *
     * @return void
     */
    public function __construct($data)
    {
        $this->data = $data;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        // Defaults
        $filePath   = false;
        // Check if file exists
        if (Storage::disk('s3')->exists($this->data['awsRoute'])) {
            $filePath = $this->data['awsRoute'];
        }

        $subject = 'Error enviando archivo a ReadSoft desde el mail';

        // Body data
        $bodyData = "\nDatos proporcionados:
                    \nID interno de factura: {$this->data['invoiceId']}
                    \nID interno del attachment: {$this->data['emailAttachmentId']}
                    \nRuta del archivo en Amazon S3: {$this->data['awsRoute']}
                    \nNombre original del archivo: {$this->data['originalFilename']}\n";

        $templateData = [
            'title'     => "Error al enviar un documento a ReadSoft para su procesamiento\n",
            'body'      => "El documento no pudo ser procesado por ReadSoft debido a su tamaño o por algún problema de encoding\n",
            'bodyData'  => $bodyData
        ];

        $mail = $this->markdown('emails.invoices.error-sending-file-to-readsoft', $templateData)
            ->subject($subject)
            ->from(config('mail.from.address'), config('mail.from.name'));

        if ($filePath) {
            $mail->attachFromStorageDisk('s3', $filePath, $this->data['awsRoute'], [
                'mime' => 'application/pdf'
            ]);
        }

        return $mail;
    }
}
