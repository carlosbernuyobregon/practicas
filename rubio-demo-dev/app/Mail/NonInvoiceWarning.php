<?php

namespace App\Mail;

use App\Invoice;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Storage;

class NonInvoiceWarning extends Mailable
{
    use Queueable, SerializesModels;

    public $invoice;
    public $xmlData;
    public $extraData;

    /**
     * Create a new message instance.
     *
     * @param Invoice $invoice
     * @param $xmlData
     * @param $extraData
     */
    public function __construct(Invoice $invoice, $xmlData, $extraData)
    {
        $this->invoice      = $invoice;
        $this->xmlData      = $xmlData;
        $this->extraData    = $extraData;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        // Defaults
        $filePath   = false;
        // Check if file exists
        if (Storage::disk('s3')->exists($this->invoice->aws_route)) {
            $filePath = $this->invoice->aws_route;
        }

        $translations   = __('backend.mailables.non-invoice-notification');
        $fromEmail      = !empty($this->extraData['originalSender']) ? $this->extraData['originalSender'] : false;
        $emailDate      = !empty($this->extraData['sent_at']) ? $this->extraData['sent_at'] : false;

        // Title
        $title          = $translations['title'];
        // Body top
        $bodyTop        = $translations['body'];
        // Body data
        $bodyDetails    = __('backend.mailables.non-invoice-notification.body-details', [
            'fromEmail' => $fromEmail,
            'emailDate' => $emailDate,
        ]);

        // Body bottom
        $bodyBottom     = $translations['body-bottom'];

        // Thanks
        $thanks         = false;

        // Template data
        $data           = [
            'title'         => $title,
            'bodyTop'       => $bodyTop,
            'bodyDetails'   => $bodyDetails,
            'bodyBottom'    => $bodyBottom,
            'thanks'        => $thanks
        ];

        // Subject
        $subject        = $title;

        $mail           = $this->markdown('emails.invoices.non-invoice', $data)
            ->subject($subject)
            ->from(config('mail.from.address'), config('mail.from.name'));

        if ($filePath) {
            $mail->attachFromStorageDisk('s3', $filePath, $this->invoice->filename,[
                'mime' => 'application/pdf'
            ]);
        }

        return $mail;
    }
}
