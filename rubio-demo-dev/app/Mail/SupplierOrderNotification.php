<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class SupplierOrderNotification extends Mailable
{
    use Queueable, SerializesModels;
    public $subject = 'SOLICITUD DE PEDIDO Nº: ';
    public $order;
    public $emailProvider = true;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($order,$emailProvider)
    {
        $this->order = $order;
        $this->emailProvider = $emailProvider;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject($this->subject . $this->order->order_number)
                    ->from(config('mail.from.address'), config('mail.from.name'))
                    ->attachData($this->order->generateOrderDocument(), 'O_' . $this->order->order_number . '.pdf', [
                        'mime' => 'application/pdf',
                    ])
                    ->view('emails.orders.supplier-info-notification');
    }
}
