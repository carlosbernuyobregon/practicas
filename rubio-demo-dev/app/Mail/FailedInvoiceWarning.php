<?php

namespace App\Mail;

use App\Invoice;
use App\Library\Constants;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;

class FailedInvoiceWarning extends Mailable
{
    use Queueable, SerializesModels;

    public $invoice;
    public $xmlData;
    public $extraData;

    /**
     * Create a new message instance.
     *
     * @param Invoice $invoice
     * @param $xmlData
     * @param $extraData
     */
    public function __construct(Invoice $invoice, $xmlData, $extraData)
    {
        $this->invoice      = $invoice;
        $this->xmlData      = $xmlData;
        $this->extraData    = $extraData;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        // Defaults
        $filePath   = false;
        // Check if file exists
        if (Storage::disk('s3')->exists($this->invoice->aws_route)) {
            $filePath = $this->invoice->aws_route;
        }

        // List of status
        $allStatus = __('status', [], 'en');

        // Current string status
        $currentStatus = (!empty($allStatus[$this->invoice->status]))
            ? $allStatus[$this->invoice->status] . ' (estado: ' . $this->invoice->status . ')'
            : $this->invoice->status;

        $externalId = (!empty($this->extraData['externalId']))
            ? $this->extraData['externalId']
            : false;

        // Body header
        $bodyFirstLine = "\nSe ha detectado un comportamiento inusual procesando la factura {$this->xmlData['invoice_number']}";

        // Body data
        $bodyData = "\nDatos proporcionados:
                    \nID de Factura: {$this->invoice->id}
                    \nStatus de Factura: {$currentStatus}\n";

        if ($externalId) {
            $bodyData .= "\nAttachmentId: {$this->extraData['externalId']}\n";
        }

        if ($this->invoice->trashed()) {
            $bodyData .= "\nFactura marcada como borrada el día {$this->invoice->deleted_at}\n" ;
        }

        if (!empty($this->extraData) && !empty($this->extraData['reason'])) {
            $bodyData .= "\nMotivo: {$this->extraData['reason']}\n" ;
        }

        if (!empty($data) && !empty($data['readsoftTrackId'])) {
            $bodyData .= "\nTrack id (documento en ReadSoft): {$this->extraData['readsoftTrackId']}\n" ;
        }

        $thanks = false;

        $data = [
            'title'     => 'Algo ha ido mal procesando una factura (bajando contenido del ocr)',
            'body'      => $bodyFirstLine,
            'bodyData'  => $bodyData,
            'thanks'    => $thanks
        ];

        $subject = 'Se ha marcado como borrada una factura' ;

        $mail = $this->markdown('emails.invoices.failed-invoice-warning', $data)
            ->subject($subject)
            ->from(config('mail.from.address'), config('mail.from.name'));

        if ($filePath) {
            $mail->attachFromStorageDisk('s3', $filePath, $this->invoice->filename, [
                'mime' => 'application/pdf'
            ]);
        }

        return $mail;
    }
}
