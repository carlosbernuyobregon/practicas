<?php

namespace App\Mail;

use App\Library\Constants;
use Carbon\Carbon;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class GlobalSyncNotification extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * @var $data
     */
    public $data;

    /**
     * Create a new message instance.
     * @param $data
     *
     * @return void
     */
    public function __construct($data)
    {
        $this->data = $data;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $syncType   = $this->data['syncType'];
        $object     = $this->data['object'] ?? null;

        switch ($syncType) {
            case 'ordersFromSap':
                $subject        = 'Sincronización de Pedidos (SAP to APP)';
                $title          = "Se ha ejecutado la sincronización de **Pedidos** (SAP to APP)";
                $formattedData  = "\nTotal pedidos enviados: {$this->data['total']}
            \nTotal pedidos procesados: {$this->data['processed']}\n";

                if ($this->data['success']) {
                    $formattedData .= "\nResultado final: completado";
                } else {
                    $formattedData .= "\nResultado final: no completado";
                }
                break;
            case 'providersFromSap':
                $subject        = 'Sincronización de Proveedores (SAP to APP)';
                $title          = "Se ha ejecutado la sincronización de **Provedores** (SAP to APP)";
                $formattedData  = "\nTotal proveedores enviados: {$this->data['total']}
            \nTotal proveedores procesados: {$this->data['processed']}\n";

                if ($this->data['success']) {
                    $formattedData .= "\nResultado final: completado";
                } else {
                    $formattedData .= "\nResultado final: no completado";
                }
                break;

            case 'providersToSap':
                $subject        = 'Sincronización de Proveedores (APP to SAP)';
                $title          = "Se ha ejecutado la sincronización de **Provedores** (APP to SAP)";
                $formattedData  = "Se han enviado {$this->data['total']} nuevos Proveedores a SAP";
                break;

            case 'ordersToSap':
                $subject        = 'Sincronización de Pedidos (APP to SAP)';
                $title          = "Se ha ejecutado la sincronización de **Pedidos** (APP to SAP)";
                $formattedData  = "Se han enviado {$this->data['total']} nuevos Pedidos a SAP";
                break;

            case 'commoditiesToSap':
                $subject        = 'Sincronización de Entrada de Mercancías (APP to SAP)';
                $title          = "Se ha ejecutado la sincronización de **Mercancías** (APP to SAP)";
                $formattedData  = "Se han enviado {$this->data['total']} nuevas Entradas de Mercancías a SAP";
                break;

            case 'BpmToProvider':
                $statusTxt      = __('backend.providers.status.' . $object['status']);
                $subject        = 'Actualización Alta de Proveedor';
                $title          = 'Actualización Alta de Proveedor';

                ($object['status'] == Constants::PROVIDER_STATUS_APPROVED)
                    ? $formattedData  = "El alta del proveedor {$object['name']} (VAT: {$object['vat_number']} - Company: {$object['company']}) ha sido aprobado"
                    : $formattedData  = "La solicitud de alta del proveedor {$object['name']} (VAT: {$object['vat_number']} - Company: {$object['company']}) ha sido denegado con fecha: " . Carbon::now()->toDateTimeString();
                break;
        }

        return $this->markdown('emails.invoices.providers-sync-notification', [
            'title'         => $title,
            'formattedData' => $formattedData
        ])
            ->subject($subject)
            ->from(config('mail.from.address'), config('mail.from.name'));
    }
}
