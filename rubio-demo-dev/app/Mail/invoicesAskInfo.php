<?php

namespace App\Mail;

use App\Invoice;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;

class invoicesAskInfo extends Mailable
{
    use Queueable, SerializesModels;

    public $invoice;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(Invoice $invoice = null)
    {
        $this->invoice = $invoice;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        // Defaults
        $filePath   = false;
        $view       = false;
        // Check if file exists
        if (Storage::disk('s3')->exists($this->invoice->aws_route)) {
            $filePath = $this->invoice->aws_route;
        }

        /*Log::info('FILEPATH', [
            'filePath'      => $filePath,
            'invoiceRoute'  => $this->invoice->aws_route,
            'exists'        => Storage::disk('s3')->exists($this->invoice->aws_route),
            'from'          => config('mail.from.address'),
            'name'          => config('mail.from.name'),
        ]);*/

        try {
            $view = $this
                ->subject('Solicitud de información de factura: ' . $this->invoice->invoice_number)
                ->from(config('mail.from.address'), config('mail.from.name'));

            if ($filePath) {
                $view->attachFromStorageDisk('s3',$this->invoice->aws_route, $this->invoice->filename,[
                    'mime' => 'application/pdf'
                ]);
            }

            $view->view('emails.invoices.askInfo');

        } catch (\Exception $e) {
            Log::error($e->getMessage(), [
                'class'     => __CLASS__,
                'method'    => __METHOD__,
            ]);
        }

        return $view;
    }
}
