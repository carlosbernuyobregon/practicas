<?php

namespace App\Mail;

use App\Invoice;
use App\Library\Constants;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;

class OrphanOcrDocumentWarning extends Mailable
{
    use Queueable, SerializesModels;

    public $invoice;
    public $documentUri;
    public $externalId;

    /**
     * Create a new message instance.
     *
     * @param $documentUri
     * @param $externalId
     */
    public function __construct($documentUri, $externalId)
    {
        $this->documentUri  = $documentUri;
        $this->externalId   = $externalId;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        // Body header
        $bodyFirstLine = "\nDocumento en Readsoft (Document export in progress) que no tiene un registro en la tabla de invoices";

        // Body data
        $bodyData = "\nDatos proporcionados:
                    \nUrl del xml: {$this->documentUri}
                    \nID interno del attachment: {$this->externalId}\n";

        $thanks = false;

        $data = [
            'title'     => 'Documento en Readsoft huérfano sin procesar',
            'body'      => $bodyFirstLine,
            'bodyData'  => $bodyData,
            'thanks'    => $thanks
        ];

        $subject = 'Documento en Readsoft huérfano sin procesar' ;

        $mail = $this->markdown('emails.invoices.orphan-ocr-document-warning', $data)
            ->subject($subject)
            ->from(config('mail.from.address'), config('mail.from.name'));

        return $mail;
    }
}
