<?php

namespace App\Library;

class Constants
{
    // INVOICE STATUS
    const INVOICE_STATUS_IN_PROCESS                             = 0; // En proceso
    const INVOICE_STATUS_VALIDATION_PENDING                     = 1; // Pendiente de validar
    const INVOICE_STATUS_ACCOUNTING_PENDING                     = 2; // Pendiente de contabilizar
    const INVOICE_STATUS_APPROVED                               = 3; // Aprobada
    const INVOICE_STATUS_BLOCKED                                = 4; // Bloqueada
    const INVOICE_STATUS_ACCOUNTING_ERROR                       = 5; // Error contabilizar
    const INVOICE_STATUS_POSTED                                 = 6; // Contabilizada
    const INVOICE_STATUS_ACCOUNTING_ENTRY_NUMBER                = 7; // Num. Asiento
    const INVOICE_STATUS_ACCOUNTING_DATE                        = 8; // Fecha contabilización
    const INVOICE_STATUS_MANAGER_APPROVAL_PENDING               = 9; // Pendiente aprobación manager
    const INVOICE_STATUS_ACCOUNTING_OFFICE_INFORMATION_PENDING  = 10; // Pendiente información departamento
    const INVOICE_STATUS_MANUALLY_POSTED                        = 11; // Contabilizado manual
    const INVOICE_STATUS_MANUAL_ACCOUNTING_APPROVED             = 12; // Aprobada contabilización manual
    const INVOICE_STATUS_DUPLICATED                             = 13; // Duplicada
    const INVOICE_STATUS_INVALID                                = 14; // No válida
    const INVOICE_STATUS_OCR_PROCESSING                         = 91; // Procesando en OCR
    const INVOICE_STATUS_SENT_TO_SAP                            = 92; // Enviada a SAP
    const INVOICE_STATUS_IS_NOT_AN_INVOICE                      = 999; // No es factura

    // INVOICE SPECIAL RULES BY PROVIDERS
    const FORCE_TAX_TYPE            = 'force-tax-type';
    const BLOCK_DUE_UNIT_MEASURE    = 'block-due-unit-measure';

    // INVOICE / INVOICE LINE
    const HEADER_ITEM_TO_LINE_TYPE_DISCOUNT = 'header-discount';
    const HEADER_ITEM_TO_LINE_TYPE_IRPF     = 'header-irpf';

    // INVOICE ERRORS FOR CHECKING AGAINST ORIGINAL DATA
    const CHECK_ERROR   = 'error';
    const CHECK_WARNING = 'warning';

    // INVOICE MARKS
    const INVOICE_IS_NOT_INVOICE = 'NO FACTURA';

    // TAXES
    const TAX_SPECIAL_TYPE_AUTOREPERCUTIDO = 'autorepercutido';

    // CHECK INVOICES METHODS
    const CHECK_INVOICE_REGULAR_METHOD      = 'regular-check-invoice';
    const CHECK_INVOICE_SPECIFIC_METHOD     = 'specific-check-invoice';
    const CHECK_INVOICE_REASON_NO_DATA      = 'no-data-check-invoice';
    const CHECK_INVOICE_REASON_SPANISH      = 'spanish-check-invoice';
    const CHECK_INVOICE_REASON_NO_SPANISH   = 'no-spanish-check-invoice';

    // ORDER STATUS
    const ORDER_STATUS_DRAFT                            = 0; // Borrador de pedido
    const ORDER_STATUS_PENDING_APPROVAL_BY_MANAGER      = 1; // Pendiente aprobación Manager
    const ORDER_STATUS_APPROVED_BY_MANAGER              = 2; // Aprobado Manager
    const ORDER_STATUS_APPROVED_BY_MANAGEMENT           = 3; // Aprobado Dirección
    const ORDER_STATUS_APPROVED_BY_GENERAL_MANAGEMENT   = 4; // Aprobado Dirección General
    const ORDER_STATUS_BLOCKED                          = 5; // Pedido bloqueado
    const ORDER_STATUS_CANCELED                         = 6; // Pedido cancelado
    const ORDER_STATUS_MODIFICATION_PENDING             = 7; // Pedido pendiente de modificación
    const ORDER_STATUS_APPROVED                         = 8; // Pedido aprobado
    const ORDER_STATUS_UPDATED_IN_SAP                   = 9; // Pedido actualizado en SAP
    const ORDER_STATUS_UPDATED_IN_SAP_SUCCESS           = 91; // Pedido actualizado en SAP CORRECTAMENTE
    const ORDER_STATUS_UPDATED_IN_SAP_ERROR             = 92; // Pedido actualizado en SAP ERRONEAMENTE

    const DELIVERY_STATUS_UPDATED_IN_SAP_SUCCESS        = 91; // Delivery actualizado en SAP CORRECTAMENTE
    const DELIVERY_STATUS_UPDATED_IN_SAP_ERROR          = 92; // Delivery actualizado en SAP ERRONEAMENTE

    const PROVIDER_STATUS_APPROVED                      = 1; // Proveedor aprobado
    const PROVIDER_STATUS_REJECTED                      = 2; // Proveedor rechazado
}
