<?php

namespace App;

use App\Traits\AutoGenerateUuid;
use Illuminate\Database\Eloquent\Model;
use Astrotomic\Translatable\Contracts\Translatable as TranslatableContract;
use Astrotomic\Translatable\Translatable;

class Role extends Model implements TranslatableContract
{
    //
    use AutoGenerateUuid, Translatable;
    public $incrementing = false;
    protected $keyType = 'string';

    protected $fillable = [
        'key_value',
        'customer_id'
    ];
    public $translatedAttributes = [
        'name',
        'description'
    ];

    public function setCustomerIdAttribute($value)
    {
        $this->attributes['customer_id'] = ($value == "") ? null : $value;
    }

    public function users()
    {
        return $this->belongsToMany('App\User');
    }

    public function getDateFormat()
    {
        if (config('custom.database_type') == 'sqlsrv') {
            return 'd.m.Y H:i:s';
        }
        return parent::getDateFormat();
    }
}
