<?php

namespace App;

use App\Traits\AutoGenerateUuid;
use Illuminate\Database\Eloquent\Model;

class Customer extends Model
{
    use AutoGenerateUuid;
    public $incrementing = false;
    protected $keyType = 'string';

    protected $fillable = [
        'key_value',
        'email',
        'name',
        'public_api_key',
        'secret_api_key',
        'non_encripted'

    ];
    public function getDateFormat()
    {
        if(config('custom.database_type')=='sqlsrv')
        {
            return 'd.m.Y H:i:s';
        }
return parent::getDateFormat();
    }
}
