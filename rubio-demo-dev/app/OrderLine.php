<?php

namespace App;

use App\Traits\AutoGenerateUuid;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class OrderLine extends Model
{
    use AutoGenerateUuid;
    public $incrementing = false;
    protected $keyType = 'string';

    protected $fillable = [
        'customer_id',
        'order_id',
        'company',
        'description',
        'position',
        'material_id',
        'material_name',
        'imputation_type_id',
        'location_id',
        'tax_type_id',
        'tax_type',
        'net_amount',
        'total_amount',
        'qty',
        'accounting_account_id',
        'cost_center_id',
        'pep_element_id',
        'delivery_date',
        'budget_id',
        'mesure_unit_id',
        'currency'
    ];

    /*protected $appends = [
        'imputation_type_code_txt',
        'accounting_account_txt',
        'cost_center_txt',
        'pep_element_txt',
        'budget_txt',
        'mesure_unit_txt',
        'center_txt'
    ];*/

    public function order()
    {
        return $this->belongsTo('App\Order');
    }
    public function customer()
    {
        return $this->belongsTo('App\Customer');
    }
    public function imputationType()
    {
        return $this->belongsTo('App\ImputationType');
    }
    public function taxType()
    {
        return $this->belongsTo('App\TaxType');
    }
    /*
    public function location()
    {
        return $this->belongsTo('App\Location');
    }
    */
    public function accountingAccount()
    {
        return $this->belongsTo('App\AccountingAccount');
    }
    public function costCenter()
    {
        return $this->belongsTo('App\CostCenter');
    }
    public function pepElement()
    {
        return $this->belongsTo('App\PepElement');
    }
    public function budget()
    {
        return $this->belongsTo('App\Budget');
    }
    public function mesureUnit()
    {
        return $this->belongsTo('App\MesureUnit');
    }
    public function audit()
    {
        return $this->morphMany('App\Audit', 'auditable')->orderBy('created_at');
    }
    public function deliveries()
    {
        return $this->hasMany(Delivery::class);
    }
    public function notDelivered()
    {
        $delivered = Delivery::where('order_line_id',(string)$this->id)->sum('amount');
        if($delivered > 0){
            return $this->qty*$this->net_amount;
        }else{
            return $this->qty*$this->net_amount - $delivered;
        }

    }

    /**
     * Checks if line have at least one PA Object data added
     * @return bool
     */
    public function PaObjectDataAvailable()
    {
        if (
            $this->pa_business_area ||
            $this->pa_distribution_channel ||
            $this->pa_country ||
            $this->pa_order ||
            $this->pa_person ||
            $this->pa_family ||
            $this->pa_client
        ) {
            return true;
        }

        return false;
    }

    /**
     * MUTATORS
     */
    /*public function getImputationTypeCodeTxtAttribute()
    {
        return $this->imputationType->name ?? '';
    }
    public function getAccountingAccountTxtAttribute()
    {
        return $this->accountingAccount->name ?? '';
    }
    public function getCostCenterTxtAttribute()
    {
        return $this->costCenter->name ?? '';
    }
    public function getPepElementTxtAttribute()
    {
        return $this->pepElement->name ?? '';
    }
    public function getBudgetTxtAttribute()
    {
        return $this->budget->name ?? '';
    }
    public function getMesureUnitTxtAttribute()
    {
        return $this->mesureUnit->name ?? '';
    }
    public function getCenterTxtAttribute()
    {
        return $this->center ?? '';
    }*/
}
