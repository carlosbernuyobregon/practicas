<?php

namespace App;

use App\Helpers\Helpers;
use App\Library\Constants;
use App\Mail\ErrorSendingFileToReadSoftWarning;
use App\Mail\FailedInvoiceWarning;
use App\Mail\NonInvoiceWarning;
use App\Mail\OrphanOcrDocumentWarning;
use App\Models\Provider;
use GuzzleHttp\Client;
use GuzzleHttp\Cookie\CookieJar;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Arr;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Storage;

class Readsoft extends Model
{
    protected $url = 'https://services.readsoftonline.com/authentication/rest/authenticate';
    protected $user;
    protected $password;
    protected $key;
    protected $version;
    protected $uiculture;
    protected $culture;
    protected $client = null;
    protected $headers = null;
    protected $jar = null;
    protected $customerid;
    protected $buyerid;
    protected $testingMode;

    public function __construct()
    {
        // ReadSoft
        $this->user         = config('readsoft.config.user');
        $this->password     = config('readsoft.config.password');
        $this->key          = config('readsoft.config.key');
        $this->version      = config('readsoft.config.version');
        $this->uiculture    = config('readsoft.config.uiculture');
        $this->culture      = config('readsoft.config.culture');
        $this->customerid   = config('readsoft.config.customer-id');
        $this->buyerid      = config('readsoft.config.buyer-id');

        $this->jar = new CookieJar();
        $this->headers = [
            'Content-Type' => 'application/json',
            'x-rs-key' => $this->key,
            'x-rs-version' => $this->version,
            'x-rs-uiculture' => $this->uiculture,
            'x-rs-culture' => $this->culture,
        ];
        $this->client = new Client([
            'cookies' => $this->jar,
            'headers' => $this->headers
        ]);

        // Testing mode
        $this->testingMode = request()->testing && request()->testing == true;
    }

    public function connect()
    {
        $response = $this->client->post('https://services.readsoftonline.com/authentication/rest/authenticate', [
            'body' => json_encode(
                [
                    'UserName' => $this->user,
                    'Password' => $this->password,
                    'AuthenticationType' => 0
                ]
            )
        ]);
        return $response;

    }

    /**
     * @param null $filename
     * @param null $externalid
     * @param null $awsPath
     * @param null $filetype
     * @return bool|\Psr\Http\Message\ResponseInterface
     * @throws \Exception
     */
    public function sendFile($filename = null, $externalid = null,$awsPath = null,$filetype = null)
    {
        //$this->connect();
        //$bodyDocument = Utils::streamFor($emailPath.$filename);
        //$bodyDocument = file_get_contents($emailPath.$filename);
        //dd($this->jar);
        $xRsHost = $this->jar->getCookieByName('x-rs-host');
        $xRsAuthV1 = $this->jar->getCookieByName('x-rs-auth-v1');
        $headers = [
            'Content-Type' => $filetype,
            'x-rs-key' => $this->key,
            'x-rs-version' => $this->version,
            'x-rs-uiculture' => $this->uiculture,
            'x-rs-culture' => $this->culture,
            'cookies' => [
                'x-rs-host' => $xRsHost->getValue(),
                'x-rs-auth-v1' => $xRsAuthV1->getValue(),
            ]
        ];
        //dd($headers);
        $response = false;

        try {
            // Utilizamos el endpoint 2 y el parametro sortingmethod=2 para evitar que separe los pdf's en hojas
            //ob_end_clean();
            $response = $this->client->post('https://services.readsoftonline.com/files/rest/image2?'.
                'filename='.$filename.
                '&customerid='.$this->customerid.
                '&batchexternalid='.$externalid.
                '&buyerid='.$this->buyerid.
                '&documenttype=' . $filetype.
                '&sortingmethod=2',[
                    'headers' => $headers,
                    'body' => Storage::disk('s3')->get($filename),
                ]);
        } catch (\Exception $th) {
            echo "Error sending file to ReadSoft\n";
            Log::error($th->getMessage(), [
                'class'     => __CLASS__,
                'method'    => __METHOD__
            ]);
            dump($th->getMessage());
            // Throw exception to be handled by the controller invoking this function
            throw new \Exception('Error sending file to ReadSoft');
        }

        return $response;
/*

        $response = $this->client->post('https://services.readsoftonline.com/files/rest/image?'.
        'filename='.$filename.
        '&customerid='.$this->customerid.
        '&batchexternalid='.$externalid.
        '&buyerid='.$this->buyerid.
        '&documenttype=pdf',[
            'body' => base64_encode(file_get_contents($emailPath.$filename)),
        ]);
        return $response;

        */
    }
    public function getFiles()
    {
        $xRsHost = $this->jar->getCookieByName('x-rs-host');
        $xRsAuthV1 = $this->jar->getCookieByName('x-rs-auth-v1');
        $headers = [
            'Content-Type' => 'application/xml',
            'x-rs-key' => $this->key,
            'x-rs-version' => $this->version,
            'x-rs-uiculture' => $this->uiculture,
            'x-rs-culture' => $this->culture,
            'cookies' => [
                'x-rs-host' => $xRsHost->getValue(),
                'x-rs-auth-v1' => $xRsAuthV1->getValue(),
            ]
        ];
        try {
            $response = $this->client->get('https://services.readsoftonline.com/documents/rest/customers/' .
                $this->customerid . '/outputdocuments',[
                'headers' => $headers,
            ]);
            $body = (string)$response->getBody();
            $xml = simplexml_load_string($body);
            $newInvoices = 0;
            $oldInvoices = 0;
            foreach($xml as $item){
                unset($documentUri);
                unset($batchExternalId);
                if(is_string($item->DocumentUri)){
                    $documentUri = (string)$item->DocumentUri;
                }elseif(is_array($item->DocumentUri)){
                    $documentUri = (string)$item->DocumentUri[0];
                }else{
                    try {
                        $newXml = (array)$item->DocumentUri;
                        if(is_array($newXml)){
                            $documentUri = (string)$newXml[0];
                        }
                    } catch (\Exception $th) {
                        $documentUri = null;
                    }
                    if(!is_string($documentUri)){
                        $documentUri=null;
                    }
                }
                if(is_string($item->BatchExternalId)){
                    $batchExternalId = (string)$item->BatchExternalId;
                }elseif(is_array($item->BatchExternalId)){
                    $batchExternalId = (string)$item->BatchExternalId[0];
                }else{
                    try {
                        $newXml = (array)$item->BatchExternalId;
                        if(is_array($newXml)){
                            $batchExternalId = (string)$newXml[0];
                        }
                    } catch (\Exception $th) {
                        $batchExternalId = null;
                    }
                    if(!is_string($batchExternalId)){
                        $batchExternalId=null;
                    }
                }
                if(($documentUri!=null)&&($batchExternalId!=null)){
                    /*dump($documentUri);
                    dump($batchExternalId);
                    echo '----';*/
                    $newDoc = $this->getFileData($documentUri,$batchExternalId);
                    if($newDoc){
                        $newInvoices++;
                    }else{
                        $oldInvoices++;
                    }
                }
            }
            /*echo 'New invoices = ' . $newInvoices;
            echo "<br> \n";
            echo 'Old invoices = ' . $oldInvoices;
            echo "<br> \n";*/
            return [$newInvoices, $oldInvoices];
        } catch (\Exception $th) {
            Log::error($th->getMessage(), [
                'class'     => __CLASS__,
                'method'    => __METHOD__,
            ]);
            dump($th->getMessage());
            return [false, false];
        }

        //return json_encode($response);
    }

    public function getFileData($documentUri = null, $externalId = null)
    {
        $invoice            = Invoice::where('email_attachment_id', $externalId)->where('status',0)->first();
        $emailAttachment    = EmailAttachment::find($externalId);

        if ($invoice == null) {
            // Get the invoice, no matter the current status
            $checkInvoice = Invoice::where('email_attachment_id', $externalId)->first();

            // Check existence
            if (!isset($checkInvoice->id)) {
                // These are invoices sent to ReadSoft but with no rows in invoices table
                $this->sendOrphanDocumentEmail($documentUri, $externalId);
            } else {
                // These are old invoices already processed but with no documentDownloaded approach.
                // Only applies to invoices already processed
                if ($checkInvoice->status <= Constants::INVOICE_STATUS_VALIDATION_PENDING) {
                    return false;
                }

                // Get the ocr document id (this value is not save on any table and it should be. ToDo: add it to any related table
                preg_match('/rest\/(.*)/m', $documentUri, $matches);

                // Trigger documentDownloaded to fix them
                if (!empty($matches[1])) {
                    $docExternalId = $matches[1];

                    if (!$this->testingMode && self::documentDownloaded((string)$docExternalId)) {
                        $checkInvoice->is_set_as_ocr_document_downloaded = true;

                        try {
                            $checkInvoice->save();

                            dump('is_set_as_ocr_document_downloaded updated for invoice', $checkInvoice->id);
                        } catch (\PDOException $e) {
                            Log::error($e->getMessage(), [
                                'class'     => __CLASS__,
                                'method'    => __METHOD__,
                            ]);

                            dump($e->getMessage());
                        }
                    }
                }
            }

            return false;
        }

        $xRsHost = $this->jar->getCookieByName('x-rs-host');
        $xRsAuthV1 = $this->jar->getCookieByName('x-rs-auth-v1');
        $headers = [
            'Content-Type' => 'application/xml',
            'x-rs-key' => $this->key,
            'x-rs-version' => $this->version,
            'x-rs-uiculture' => $this->uiculture,
            'x-rs-culture' => $this->culture,
            'cookies' => [
                'x-rs-host' => $xRsHost->getValue(),
                'x-rs-auth-v1' => $xRsAuthV1->getValue(),
            ]
        ];

        $response = false;

        try {
            $response = $this->client->get($documentUri,[
                'headers' => $headers,
            ]);
            $body = $response->getBody();
            $xml = simplexml_load_string($body);
            //dd($xml);
            $json  = json_encode($xml);
            $xmlArr = json_decode($json, true);
            $readsoftDocumentId = $xmlArr['Id'];

            // Defaults
            $data               = Helpers::fillFieldsWithDefaultValue('header');
            $dataLine           = [];
            $headerToLine       = [];
            $duplicatedCheck    = [];

            foreach ($xmlArr['Parties']['Party'] as $party) {
                if ($party['Type'] == 'supplier') {
                    // Try to match it with a Provider
                    $providerData                       = Helpers::giveMeTheProvider($party);
                    $data['provider_name']              = ($providerData) ? $providerData['name'] : trim($party['Name']) ?? false;
                    $duplicatedCheck['provider_name']   = $data['provider_name'];

                    if ($providerData) {
                        if (!empty($providerData['community_vat_number'])) {
                            $data['vat_number'] = preg_replace("/[^A-Za-z0-9 ]/", '', ($providerData['community_vat_number']));
                        } elseif (!empty($providerData['vat_number'])) {
                            $data['vat_number'] = preg_replace("/[^A-Za-z0-9 ]/", '', ($providerData['vat_number']));
                        } else {
                            $data['vat_number'] = null;
                        }

                    } elseif (isset($party['TaxRegistrationNumber']) && $party['TaxRegistrationNumber'] != '' && !is_array($party['TaxRegistrationNumber'])) {
                        $data['vat_number'] = preg_replace("/[^A-Za-z0-9 ]/", '', ($party['TaxRegistrationNumber'] ?? ''));
                    } else {
                        $data['vat_number'] = null;
                    }

                    // If invoice provider can not be found, block invoice & audit
                    if (!$providerData) {
                        $invoice->status = Constants::INVOICE_STATUS_BLOCKED;
                        $invoice->audit()->create([
                            'user_id'   => 'system',
                            'status'    => $invoice->status,
                            'text'      => '[Factura bloqueada - No se puede encontrar el proveedor en la tabla de proveedores]'
                        ]);
                    }

                    $duplicatedCheck['vat_number'] = $data['vat_number'];

                    // Country selection. First lets try to get it from the providers table
                    if ($providerData && !empty($providerData['country'])) {
                        $data['provider_country'] = $providerData['country'];
                    } elseif ($party['CountryName']) {
                        $data['provider_country'] = Country::whereTranslation('name',$party['CountryName'])->value('code') ?? Country::where('code',$party['CountryName'])->value('code') ?? '';
                    } else {
                        $data['provider_country'] = '';
                    }
                    $data['provider_street']        = $party['Street'] ?? '';
                    $data['provider_city']          = $party['City'] ?? '';
                    $data['provider_postal_code']   = $party['PostalCode'] ?? '';
                    $data['provider_state']         = $party['State'] ?? '';
                } else {
                    $data['intern_vat_number']      = $party['TaxRegistrationNumber'];
                }
            }

            foreach ($xmlArr['HeaderFields']['HeaderField'] as $headerItem){
                switch ($headerItem['Type']) {
                    case 'invoicenumber':
                        $data['invoice_number']                 = (!empty($headerItem['Text'])) ? $headerItem['Text'] : null;
                        $duplicatedCheck['invoice_number']      = $data['invoice_number'];
                        break;
                    case 'invoicedate':
                        $data['date']                           = (!empty($headerItem['Text'])) ? $headerItem['Text'] : null;
                        $duplicatedCheck['date']                = $data['date'];
                        break;
                    case 'invoicetotalvatexcludedamount':
                        $data['total_net']                      = (!empty($headerItem['Text'])) ? $headerItem['Text'] : 0;
                        break;
                    case 'invoicetotalvatratepercent':
                        $data['taxes_percent']                  = floatval((!empty($headerItem['Text'])) ? $headerItem['Text'] : 0);
                        break;
                    case 'invoicetotalvatamount':
                        $data['taxes_total_amount']             = (!empty($headerItem['Text'])) ? $headerItem['Text'] : 0;
                        break;
                    case 'invoicetotalvatincludedamount':
                        $data['total']                          = (!empty($headerItem['Text'])) ? $headerItem['Text'] : 0;
                        break;
                    case 'invoicecurrency':
                        $data['currency']                       = (!empty($headerItem['Text'])) ? $headerItem['Text'] : null;
                        break;
                    case 'invoiceordernumber':
                        $data['intern_order']                   = (!empty($headerItem['Text'])) ? $headerItem['Text'] : null;
                        $duplicatedCheck['intern_order']        = $data['intern_order'];
                        break;
                    case 'deliverycost':
                        $data['delivery_amount']                = (!empty($headerItem['Text'])) ? $headerItem['Text'] : 0;
                        break;
                    case 'invoicenetamount2':
                        $data['total_net_2']                    = (!empty($headerItem['Text'])) ? $headerItem['Text'] : 0;
                        break;
                    case 'invoicetaxrate2':
                        $data['tax_rate_2']                     = (!empty($headerItem['Text'])) ? $headerItem['Text'] : 0;
                        break;
                    case 'invoicetaxamount2':
                        $data['tax_amount_2']                   = (!empty($headerItem['Text'])) ? $headerItem['Text'] : 0;
                        break;
                    case 'invoicenetamount3':
                        $data['total_net_3']                    = (!empty($headerItem['Text'])) ? $headerItem['Text'] : 0;
                        break;
                    case 'invoicetaxrate3':
                        $data['tax_rate_3']                     = (!empty($headerItem['Text'])) ? $headerItem['Text'] : 0;
                        break;
                    case 'invoicetaxamount3':
                        $data['tax_amount_3']                   = (!empty($headerItem['Text'])) ? $headerItem['Text'] : 0;
                        break;
                    case 'invoicediscountamount':
                        $data['discount_amount']                = (!empty($headerItem['Text'])) ? $headerItem['Text'] : 0;
                        break;
                    case 'supplieriban1':
                        $data['bank_iban']                      = (!empty($headerItem['Text'])) ? $headerItem['Text'] : null;
                        break;
                    case 'supplierbankcodenumber1':
                        $data['bank_code_number']               = (!empty($headerItem['Text'])) ? $headerItem['Text'] : null;
                        break;
                    case 'Sociedad':
                        $data['intern_vat_number']              = (!empty($headerItem['Text'])) ? $headerItem['Text'] : null;
                        $data['intern_vat_number']              = preg_replace("/^ES/", '', $data['intern_vat_number']);
                        $data['intern_vat_number']              = preg_replace("/[^A-Za-z0-9 ]/", '', $data['intern_vat_number']);
                        break;
                    case 'CifProveedor':
                        $data['vat_number']                     = (!empty($headerItem['Text'])) ? $headerItem['Text'] : null;
                        break;
                    case 'FormaPago':
                        $data['payment_conditions']             = (!empty($headerItem['Text'])) ? $headerItem['Text'] : null;
                        break;
                    case 'FechaVencimiento2':
                        $data['payment_date']                   = (!empty($headerItem['Text'])) ? $headerItem['Text'] : null;
                        break;
                    case 'ImporteIRPF':
                        $data['taxes_irpf_amount']              = (!empty($headerItem['Text'])) ? $headerItem['Text'] : 0;
                        break;
                    case 'InvoiceTaxRate1':
                        $data['taxes_irpf_rate']                = (!empty($headerItem['Text'])) ? $headerItem['Text'] : 0;
                        break;
                    case 'InvoiceTaxRate4':
                        $data['tax_rate_4']                     = (!empty($headerItem['Text'])) ? $headerItem['Text'] : 0;
                        break;
                    case 'InvoiceTaxRate5':
                        $data['tax_rate_5']                     = (!empty($headerItem['Text'])) ? $headerItem['Text'] : 0;
                        break;
                    case 'InvoiceNetAmount4':
                        $data['total_net_4']                    = (!empty($headerItem['Text'])) ? $headerItem['Text'] : 0;
                        break;
                    case 'InvoiceNetAmount5':
                        $data['total_net_5']                    = (!empty($headerItem['Text'])) ? $headerItem['Text'] : 0;
                        break;
                    case 'InvoiceTaxAmount5':
                        $data['tax_amount_5']                   = (!empty($headerItem['Text'])) ? $headerItem['Text'] : 0;
                        break;
                    case 'InvoiceTaxAmount4':
                        $data['tax_amount_4']                   = (!empty($headerItem['Text'])) ? $headerItem['Text'] : 0;
                        break;
                    case 'CONCEPTO_FACTURA':
                        $headerToLine['material_description']   = (!empty($headerItem['Text'])) ? $headerItem['Text'] : null;
                        break;
                    case 'FECHA_OPERACION':
                        $data['operation_date']                 = (!empty($headerItem['Text'])) ? $headerItem['Text'] : null;
                        break;
                    case 'FLAG_ALERTA':
                        $data['alert_flag_from_ocr']            = (!empty($headerItem['Text'])) ? $headerItem['Text'] : null;
                        break;
                    case 'DocumentType':
                        $data['document_type_label']            = (!empty($headerItem['Text'])) ? $headerItem['Text'] : null;
                        break;
                    case 'InstrumentoDePago':
                    case 'CampoComercialVenta':
                    case 'NumClienteProv':
                    case 'InvoiceInsurance':
                        break;
                }
            }

            // Seleccionar sociedad correcta
            if (isset($data['intern_vat_number'])) {
                switch ($data['intern_vat_number']) {
                    case 'B65664013':
                        // P&T
                        $data['sap_company'] = '2000';
                        break;
                    case 'A08222465':
                        // Rubió
                        $data['sap_company'] = '1000';
                        break;
                    default:
                        $data['sap_company'] = null;
                        break;
                }
            } else {
                // Assign it to Rubió and block the invoices adding audit and messages
                $data['sap_company']    = '1000';
                $invoice->status        = Constants::INVOICE_STATUS_BLOCKED;
                $invoice->comments      = '[Factura bloqueada - Datos insuficientes para asignar la factura a una Sociedad. Por defecto se asigna a Rubió y se bloquea]';
                $invoice->audit()->create([
                    'user_id'   => 'system',
                    'status'    => $invoice->status,
                    'text'      => '[Factura bloqueada - Datos insuficientes para asignar la factura a una Sociedad. Por defecto se asigna a Rubió y se bloquea]'
                ]);
            }

            // SAP company checkpoint
            // The soft deletion will only be applied if its not flagged from OCR as NO FACTURA
            if (is_null($data['sap_company']) && $data['alert_flag_from_ocr'] != Constants::INVOICE_IS_NOT_INVOICE) {
                try {
                    // Delete invoice (soft delete)
                    $invoice->delete();

                    // Clean up messed up values
                    $data = $this->cleanCorruptDataFromDataArray($data);
                    // Save original sensible data (totals, taxes, everything related to calculations into a related table (relationship)
                    $this->saveOriginalData($invoice, $data, $documentUri);

                    if (!$this->testingMode) {
                        $this->documentDownloaded((string)$readsoftDocumentId);
                    }

                    $this->sendWarningEmail(
                        $invoice,
                        $data,
                        [
                            'externalId'        => $externalId,
                            'reason'            => 'sap_company is null',
                            'readsoftTrackId'   => (!empty($xmlArr['TrackId'])) ? $xmlArr['TrackId'] : ''
                        ]
                    );

                } catch (\PDOException $e) {
                    Log::error($e->getMessage(), [
                        'class'     => __CLASS__,
                        'method'    => __METHOD__,
                    ]);

                    dump($e->getMessage());
                }

                return false;
            }

            if ($data['total_net'] > 0) {
                $data['document_type'] = 1;
            } else {
                $data['document_type'] = 2;
            }

            if ($emailAttachment != null) {
                $data['filename'] = $emailAttachment->filename ?? '';
            }

//            if (isset($data['taxes_percent'])) {} // Since foreign invoices get a forced Tax Type, there is no need to do this
            // Call the normalized method
            list($data['taxes_type'], $data['taxes_id'], $invoiceZone) = Helpers::giveMeTheTax(
                $data['taxes_percent'], $data['provider_country']
            );

            // Block non spanish invoices
            if ($invoiceZone != 'ES') {
                $data['status'] = Constants::INVOICE_STATUS_BLOCKED;
                $invoice->audit()->create([
                    'user_id'   => 'system',
                    'status'    => $invoice->status,
                    'text'      => '[Factura bloqueada - Factura EU o Terceros países]'
                ]);
            }

            // Clean up messed up values
            $data = $this->cleanCorruptDataFromDataArray($data);

            // Calculate IRPF amount if it doesn't come from OCR and IRPF rate is set
            if (empty($data['taxes_irpf_amount']) && !empty($data['taxes_irpf_rate'])) {
                $data['taxes_irpf_amount'] = $data['total_net'] * (intval($data['taxes_irpf_rate']) / 100);
            }

            // Save invoice data
            try {
                $invoice->update($data);
            } catch (\PDOException $e) {
                Log::error($e->getMessage(), [
                    'class'     => __CLASS__,
                    'method'    => __METHOD__,
                ]);

                dump($e->getMessage());
            }

            // INVOICES CUSTOM RULES DEPENDING ON PROVIDER STARTS HERE
            $invoice->applySpecialRulesByProvider();
            // INVOICES CUSTOM RULES DEPENDING ON PROVIDER ENDS HERE

            // Save original sensible data (totals, taxes, everything related to calculations into a related table (relationship)
            $this->saveOriginalData($invoice, $data, $documentUri);

//            dump('$invoice', $invoice);
            $i=0;
            $itemOrderLine = 1;
            foreach ($xmlArr['Tables']['Table']['TableRows'] as $row){
                if(count($row)>1){
                    foreach($row as $rowLevel) {
                        $finalRow = $rowLevel['ItemFields'];
                        $dataLine = Helpers::fillFieldsWithDefaultValue('line');
                        foreach($finalRow['ItemField'] as $line) {
                            // Fill array with default values

                            $dataLine['invoice_id'] = (string)$invoice->id;

                            switch ($line['Type']) {
                                case 'LIT_ArticleName':
                                    $dataLine['material_description']       = empty($line['Text']) ? '' : $line['Text'];
                                    break;
                                case 'LIT_UnitPriceAmount':
                                    $dataLine['unit_price']                 = empty($line['Text']) ? 0 : (float)$line['Text'];
                                    break;
                                case 'LIT_VatExcludedAmount':
                                    $dataLine['net_amount']                 = empty($line['Text']) ? 0 : (float)$line['Text'];
                                    break;
                                case 'LI_IRPF_Linea':
//                                    $dataLine['irpf']                       = empty($line['Text']) ? 0 : (float)$line['Text'];
                                    // Forcing irpf to null since is not a valid concept (irpf by lines)
                                    $dataLine['irpf']                       = 0;
                                    break;
                                case 'LIT_ArticleIdentifier':
                                    $dataLine['material_code']              = empty($line['Text']) ? '' : $line['Text'];
                                    break;
                                case 'LIT_DeliveredQuantity':
                                    $dataLine['material_qty']               = empty($line['Text']) ? 1 : $line['Text'];
                                    // If captured value is 0 or 0.00, turn it into 1 (default)
                                    if ($dataLine['material_qty'] == '0') {
                                        $dataLine['material_qty'] = 1;
                                    }
                                    break;
                                case 'LIT_OrderNumber':
                                    $dataLine['item_order_num_bulk']        = empty($line['Text']) ? ($invoice->intern_order ?? null) : $line['Text'];
                                    break;
                                case 'LIT_DiscountAmount':
                                    $dataLine['discount_amount']            = empty($line['Text']) ? 0 : (float)$line['Text'];
                                    break;
                                case 'LIT_DiscountRate':
                                    $dataLine['discount_rate']              = empty($line['Text']) ? 0 : (float)$line['Text'];
                                    break;
                                case 'LIT_TIPOIVA':
                                    if (empty($line['Text'])) {
                                        $dataLine['taxes'] = $data['taxes_percent'];
                                    } else {
                                        $dataLine['taxes'] = (float)$line['Text'];
                                    }
                                    break;
                                case 'LI_ImporteIVALinea':
                                    $dataLine['taxes_amount']               = empty($line['Text']) ? 0 : (float)$line['Text'];
                                    break;
                                case 'LIT_Cantidad_UM':
                                    $dataLine['qty_measuring_unit']         = empty($line['Text']) ? 0 : $line['Text'];
                                    break;
                                case 'LIT_Precio_UM':
                                    $dataLine['unit_price_measuring_unit']  = empty($line['Text']) ? 0 : $line['Text'];
                                    break;
                                // Not used fields
                                case 'LI_Especiales':
                                case 'LI_Matricula':
                                case 'LI_Periodo':
                                case 'LI_NumTarjeta':
                                case 'LI_Bonificacion':
                                case 'LI_Embalaje':
                                case 'LI_Alquiler':
                                case 'LI_MANTENIMIENTO':
                                    break;
                            }
                        }

                        $dataLine['item_order_line_bulk'] = $itemOrderLine * 10;

                        // Determine the line tax type
                        // Since foreign invoices get a forced Tax Type, there is no need to do this. Commented out
                        /*if (isset($dataLine['taxes'])) {

                            // Call the normalized method
                            list($dataLine['tax_type_id'], $dataLine['taxes_id'], $invoiceZone) = Helpers::giveMeTheTax(
                                $dataLine['taxes'], $data['provider_country']
                            );
                        } else {
                            // Add the header tax data to the line
                            $dataLine['taxes_id']       = $data['taxes_id'] ?? null;
                            $dataLine['tax_type_id']    = $data['taxes_type'] ?? null;
                            $dataLine['taxes']          = $data['taxes_percent'] ?? null;
                        }*/

                        // Call the normalized method
                        list($dataLine['tax_type_id'], $dataLine['taxes_id'], $invoiceZone) = Helpers::giveMeTheTax(
                            $dataLine['taxes'], $data['provider_country']
                        );

                        // Create the new eloquent object
                        $invoiceLine = new InvoiceLine();
                        $invoiceLine->fill($dataLine);

                        // INVOICES CUSTOM RULES DEPENDING ON PROVIDER STARTS HERE
                        $invoiceLine->applySpecialRulesByProvider();
                        // INVOICES CUSTOM RULES DEPENDING ON PROVIDER ENDS HERE

                        // Or we can stick with simple calculation to let the invoice reflects the data pulled from OCR
                        $simpleCalculationData = [
                            'net_amount'    => $dataLine['net_amount']      ?? 0,
                            'taxes_amount'  => $dataLine['taxes_amount']    ?? 0,
                            'discount'      => $dataLine['discount_amount'] ?? 0,
                            'irpf'          => $dataLine['irpf']            ?? 0,
                        ];

                        // Decide whether to use discount rate or amount
                        $simpleCalculationData['discount'] = $this->resolveLineDiscount($dataLine);

                        // This simple operation is not taking into account the special tax types like 'autorepercutido'
                        // But will be applied during recalculation
                        $invoiceLine->total_amount =
                            $simpleCalculationData['net_amount'] +
                            $simpleCalculationData['taxes_amount'] -
                            $simpleCalculationData['irpf'];

                        // Save the data
                        $invoiceLine->save();

                        // Use recalculate function to set things correctly and avoid client intervention
                        $invoiceLine->recalculateTotals();

                        if (empty($duplicatedCheck['net_amount'])) {
                            $duplicatedCheck['net_amount'] = $dataLine['net_amount'];
                        } else {
                            $duplicatedCheck['net_amount'] += $dataLine['net_amount'];
                        }

                        unset($dataline);
                        unset($invoiceLine);
                    }
                } else {
                    $finalRow = $row['ItemFields'];
                    $dataLine = Helpers::fillFieldsWithDefaultValue('line');
                    foreach($finalRow['ItemField'] as $line) {
                        // Fill array with default values
                        $dataLine['invoice_id'] = (string)$invoice->id;
                        switch ($line['Type']) {
                            case 'LIT_ArticleName':
                                $dataLine['material_description']       = empty($line['Text']) ? '' : $line['Text'];
                                break;
                            case 'LIT_UnitPriceAmount':
                                $dataLine['unit_price']                 = empty($line['Text']) ? 0 : (float)$line['Text'];
                                break;
                            case 'LIT_VatExcludedAmount':
                                $dataLine['net_amount']                 = empty($line['Text']) ? 0 : (float)$line['Text'];
                                break;
                            case 'LI_IRPF_Linea':
//                                $dataLine['irpf']                       = empty($line['Text']) ? 0 : (float)$line['Text'];
                                // Forcing irpf to null since is not a valid concept (irpf by lines)
                                $dataLine['irpf']                       = 0;
                                break;
                            case 'LIT_ArticleIdentifier':
                                $dataLine['material_code']              = empty($line['Text']) ? '' : $line['Text'];
                                break;
                            case 'LIT_DeliveredQuantity':
                                $dataLine['material_qty']               = empty($line['Text']) ? 1 : $line['Text'];
                                // If captured value is 0 or 0.00, turn it into 1 (default)
                                if ($dataLine['material_qty'] == '0') {
                                    $dataLine['material_qty'] = 1;
                                }
                                break;
                            case 'LIT_OrderNumber':
                                $dataLine['item_order_num_bulk']        = empty($line['Text']) ? ($invoice->intern_order ?? null) : $line['Text'];
                                break;
                            case 'LIT_DiscountAmount':
                                $dataLine['discount_amount']            = empty($line['Text']) ? 0 : (float)$line['Text'];
                                break;
                            case 'LIT_DiscountRate':
                                $dataLine['discount_rate']              = empty($line['Text']) ? 0 : (float)$line['Text'];
                                break;
                            case 'LI_Especiales':
                                //$dataLine['invoice_number'] = $line['Text'];
                                break;
                            case 'LIT_TIPOIVA':
                                if (empty($line['Text'])) {
                                    $dataLine['taxes'] = $data['taxes_percent'];
                                } else {
                                    $dataLine['taxes'] = (float)$line['Text'];
                                }
                                break;
                            case 'LI_ImporteIVALinea':
                                $dataLine['taxes_amount']               = empty($line['Text']) ? 0 : (float)$line['Text'];
                                break;
                            case 'LIT_Cantidad_UM':
                                $dataLine['qty_measuring_unit']         = empty($line['Text']) ? 0 : $line['Text'];
                                break;
                            case 'LIT_Precio_UM':
                                $dataLine['unit_price_measuring_unit']  = empty($line['Text']) ? 0 : $line['Text'];
                                break;
                            // Not used fields
                            case 'LI_Matricula':
                            case 'LI_Periodo':
                            case 'LI_NumTarjeta':
                            case 'LI_Bonificacion':
                            case 'LI_Embalaje':
                            case 'LI_Alquiler':
                            case 'LI_MANTENIMIENTO':
                                break;
                        }

                    }

                    $dataLine['item_order_line_bulk']   = $itemOrderLine * 10;
                    $duplicatedCheck['net_amount']      = $dataLine['net_amount'];

                    // Determine the line tax type
                    // Since foreign invoices get a forced Tax Type, there is no need to do this. Commented out
                    /*if (isset($dataLine['taxes'])) {

                        // Call the normalized method
                        list($dataLine['tax_type_id'], $dataLine['taxes_id'], $invoiceZone) = Helpers::giveMeTheTax(
                            $dataLine['taxes'], $data['provider_country']
                        );
                    } else {
                        // Add the header tax data to the line
                        $dataLine['taxes_id']       = $data['taxes_id'] ?? null;
                        $dataLine['tax_type_id']    = $data['taxes_type'] ?? null;
                        $dataLine['taxes']          = $data['taxes_percent'] ?? null;
                    }*/

                    // Call the normalized method
                    list($dataLine['tax_type_id'], $dataLine['taxes_id'], $invoiceZone) = Helpers::giveMeTheTax(
                        $dataLine['taxes'], $data['provider_country']
                    );

                    // Create the new eloquent object
                    $invoiceLine = new InvoiceLine();
                    $invoiceLine->fill($dataLine);

                    // INVOICES CUSTOM RULES DEPENDING ON PROVIDER STARTS HERE
                    $invoiceLine->applySpecialRulesByProvider();
                    // INVOICES CUSTOM RULES DEPENDING ON PROVIDER ENDS HERE

                    // Or we can stick with simple calculation to let the invoice reflects the data pulled from OCR
                    $simpleCalculationData = [
                        'net_amount'    => $dataLine['net_amount']      ?? 0,
                        'taxes_amount'  => $dataLine['taxes_amount']    ?? 0,
                        'irpf'          => $dataLine['irpf']            ?? 0,
                    ];

                    // Decide whether to use discount rate or amount
                    $simpleCalculationData['discount'] = $this->resolveLineDiscount($dataLine);

                    // This simple operation is not taking into account the special tax types like 'autorepercutido'
                    // But will be applied during recalculation
                    $invoiceLine->total_amount =
                        $simpleCalculationData['net_amount'] -
                        $simpleCalculationData['discount'] +
                        $simpleCalculationData['taxes_amount'] -
                        $simpleCalculationData['irpf'];

                    // Save the data
                    $invoiceLine->save();

                    // Use recalculate function to set things correctly and avoid client intervention
                    $invoiceLine->recalculateTotals();

                    unset($dataline);
                    unset($invoiceLine);
                }
                $itemOrderLine = $itemOrderLine + 1;
            }

            if ($itemOrderLine == 1) {
                $headerToLine                           = Helpers::fillFieldsWithDefaultValue('line');
                $headerToLine['invoice_id']             = (string)$invoice->id;
                $headerToLine['unit_price']             = 0;
                $headerToLine['net_amount']             = $invoice->total_net;
                $headerToLine['taxes_amount']           = $invoice->taxes_total_amount;
                $headerToLine['total_amount']           = $invoice->total;
//                $headerToLine['irpf']                   = $invoice->taxes_irpf_amount;
                // Forcing irpf to null since is not a valid concept (irpf by lines)
                $headerToLine['irpf']                   = 0;
                $headerToLine['item_order_line_bulk']   = 10;
                $headerToLine['item_order_num_bulk']    = $invoice->intern_order;
                $headerToLine['taxes_id']               = $data['taxes_id']         ?? null;
                $headerToLine['tax_type_id']            = $data['taxes_type']       ?? null;
                $headerToLine['taxes']                  = $data['taxes_percent']    ?? null;

                $invoiceLine = InvoiceLine::create($headerToLine);
                unset($headerToLine);
                // Use recalculate function to set things correctly and avoid client intervention
                $invoiceLine->recalculateTotals();
            }
            $itemOrderLine=1;

            // APPLYING GENERAL RULES

            // Invoice must inherit intern order from lines in case it is empty
            if (!$invoice->intern_order) {
                $invoice->load('invoiceLines');
                $linesInternOder = [];
                $invoice->invoiceLines->each(function ($line, $key) use (&$linesInternOder) {
                    if (!empty($line->item_order_num_bulk)) {
                        $linesInternOder[] = $line->item_order_num_bulk;
                    }
                });

                // If there is any order number in lines, assign it to the header using a special mark [**]
                if (!empty($linesInternOder)) {
//                    $invoice->intern_order = '[**]' . array_first($linesInternOder);
                    $invoice->intern_order = array_first($linesInternOder);
                }
            }

            // Invoice has been flagged from the OCR Service & audit
            if (!is_null($invoice->alert_flag_from_ocr)) {
                switch ($invoice->alert_flag_from_ocr) {
                    case Constants::INVOICE_IS_NOT_INVOICE:

                        // Clean up values before saving
                        $cleanInvoice = Helpers::fillFieldsWithDefaultValue('header');

                        // Add only basic data for a non invoice document
                        $cleanInvoice['status']                 = Constants::INVOICE_STATUS_IS_NOT_AN_INVOICE;
                        $cleanInvoice['alert_flag_from_ocr']    = $invoice->alert_flag_from_ocr;
                        $cleanInvoice['email_attachment_id']    = $invoice->email_attachment_id;
                        $cleanInvoice['aws_route']              = $invoice->aws_route;
                        $cleanInvoice['filename']               = $invoice->filename;
                        $cleanInvoice['file_url']               = $invoice->file_url;
                        $cleanInvoice['vat_number']             = $invoice->vat_number;
                        $cleanInvoice['provider_name']          = $invoice->provider_name;

                        // Delete invoice lines
                        $invoice->load('invoiceLines');
                        $invoice->invoiceLines->each(function($line) {
                            $line->delete();
                        });

                        try {
                            // Save clean invoice
                            $invoice->update($cleanInvoice);

                        } catch (\PDOException $e) {
                            Log::error($e->getMessage(), [
                                'class'     => __CLASS__,
                                'method'    => __METHOD__,
                            ]);
                            dump($e->getMessage());
                        }

                        // Audit
                        $invoice->audit()->create([
                            'user_id'   => 'system',
                            'status'    => $invoice->status,
                            'text'      => '[Flag de alerta indicado desde el OCR con valor: ' . $invoice->alert_flag_from_ocr . ' ]'
                        ]);

                        // Send invoice back email notification
                        $this->sendNonInvoiceEmail(
                            $invoice,
                            $data,
                            [
                                'externalId'        => $externalId,
                                'reason'            => Constants::INVOICE_IS_NOT_INVOICE,
                                'readsoftTrackId'   => !empty($xmlArr['TrackId']) ? $xmlArr['TrackId'] : ''
                            ]
                        );

                        // Skip flag
                        $skipProcessing = true;

                        // Mark document as downloaded
                        if (!$this->testingMode) {
                            $this->documentDownloaded((string)$readsoftDocumentId);
                        }

                        break;
                    default:
                        // Assign new status
                        $invoice->status = Constants::INVOICE_STATUS_BLOCKED;
                        // Audit
                        $invoice->audit()->create([
                            'user_id'   => 'system',
                            'status'    => $invoice->status,
                            'text'      => '[Factura bloqueada - Flag de alerta indicado desde el OCR con valor: ' . $invoice->alert_flag_from_ocr . ' ]'
                        ]);
                        break;
                }
            }

            // Skip processing
            if (!empty($skipProcessing)) {
                return false;
            }

            // If any of the total fields is zero, then block the invoice & audit
            if ($invoice->total == 0 || $invoice->total_net == 0) {
                $invoice->status = Constants::INVOICE_STATUS_BLOCKED;
                $invoice->audit()->create([
                    'user_id'   => 'system',
                    'status'    => $invoice->status,
                    'text'      => '[Factura bloqueada - Al menos uno de los totales es cero]'
                ]);
            }

            // If invoice date is greater than today, block invoice & audit
            if ($invoice->date) {
                if (!$invoice->isInvoiceDateValid('greaterThanToday')) {
                    // Block invoice
                    $invoice->status = Constants::INVOICE_STATUS_BLOCKED;
                    // Add audit
                    $invoice->audit()->create([
                        'user_id'   => 'system',
                        'status'    => $invoice->status,
                        'text'      => '[Factura bloqueada - La fecha de factura es mayor al día en curso]'
                    ]);
                }
            }

            // APPLYING GENERAL RULES ENDED

            // Only set status to 1 (normal situation) if there is no previous status manipulation
            if ($invoice->status == Constants::INVOICE_STATUS_IN_PROCESS) {
                $invoice->status = Constants::INVOICE_STATUS_VALIDATION_PENDING;
            }

            // Invoice duplicated checkpoint
            $duplicatedId = $this->isDuplicatedInvoice($duplicatedCheck, $invoice->id);

            if ($duplicatedId) {
                $invoice->status            = Constants::INVOICE_STATUS_DUPLICATED; // new duplicated status
                $invoice->duplicated_from   = $duplicatedId; // new duplicated from
            }

            $invoice->processed_at = now();
            // Apply header items conversion to line
            // Lets keep this commented until the second part (header items converted to lines) is tackled
            // $invoice->convertSpecialHeaderItemsIntoLines();
            $invoice->recalculateTotals();

            try {
                $invoice->save();
            } catch (\PDOException $e) {
                Log::error($e->getMessage(), [
                    'class'     => __CLASS__,
                    'method'    => __METHOD__,
                ]);
                dump($e->getMessage());
            }

            // Using testing mode to avoid set invoices as done on OCR service
            if (!$this->testingMode && self::documentDownloaded((string)$readsoftDocumentId)) {
                $invoice->is_set_as_ocr_document_downloaded = true;

                try {
                    $invoice->save();
                } catch (\PDOException $e) {
                    Log::error($e->getMessage(), [
                        'class'     => __CLASS__,
                        'method'    => __METHOD__,
                    ]);

                    dump($e->getMessage());
                }
            }

            unset($data);
            unset($duplicatedCheck);
        } catch (\Exception $th) {
            //dd($dataLine);
            Log::error($th->getMessage(), [
                'class'     => __CLASS__,
                'method'    => __METHOD__,
            ]);
            dump($th);
        }
        return json_encode($response);
    }

    /**
     * Checks if an invoice is duplicated
     *
     * @param $duplicatedCheck
     * @param $currentInvoiceId
     * @return bool
     */
    private function isDuplicatedInvoice($duplicatedCheck, $currentInvoiceId)
    {
        $invoice = false;

        // Get the invoice by number
        if (!empty($duplicatedCheck['invoice_number'])) {
            $invoice = Invoice::where('invoice_number', $duplicatedCheck['invoice_number'])
                ->where('id', '!=', $currentInvoiceId)
                ->whereNotIn('status', [13])
                ->first();

            if ($invoice) {
                if (!empty($duplicatedCheck['vat_number'])) {
                    // Lets check if the vat number is the same
                    if ($duplicatedCheck['vat_number'] == $invoice->vat_number) {
                        return $invoice->id;
                    }
                } else {
                    // Lets check if the provider is the same. Taking into account provider name might be typed different
                    // Due to that error gap, lets do proper text comparison
                    $comparison             = new \Atomescrochus\StringSimilarities\Compare();
                    $all                    = $comparison->all(strtolower($duplicatedCheck['provider_name']), strtolower($invoice->provider_name));
                    $acceptancePercentage   = Setting::where('key_value', 'provider_name_comparison_acceptance_percentage')->first();

                    if ($acceptancePercentage) {
                        $acceptancePercentage = $acceptancePercentage->value;
                    } else {
                        $acceptancePercentage = 85;
                    }

                    // If similarity is over 90%, then the provider is the same
                    if ($all['similar_text'] >= $acceptancePercentage) {
                        return $invoice->id;
                    }
                }

            }
        }

        // Get invoice by key values
        if (
            !empty($duplicatedCheck['provider_name']) &&
            !empty($duplicatedCheck['net_amount']) &&
            !empty($duplicatedCheck['date']) &&
            !empty($duplicatedCheck['intern_order'])
        ) {
            $invoice = Invoice::where('id', '!=', $currentInvoiceId)
                ->where('provider_name', $duplicatedCheck['provider_name'])
                ->where('total_net', $duplicatedCheck['net_amount'])
                ->where('date', Carbon::parse($duplicatedCheck['date'])->format('Y-m-d'))
//                ->where('date', $duplicatedCheck['date'])
                ->where('intern_order', $duplicatedCheck['intern_order'])
                ->whereNotIn('status', [13])
                ->first();

            if ($invoice) {
                return $invoice->id;
            }
        }

        return false;
    }

    /**
     * Gets rid of the problematic values assigned to any position of the data array
     *
     * @param $data
     * @return mixed
     */
    private function cleanCorruptDataFromDataArray($data)
    {
        if (is_array($data)) {
            foreach ($data as $key => $value) {
                /*if (in_array($value, ['[]'])) {
                    $data[$key] = str_replace(['[]'], null, $value);
                }

                if (in_array($value, [''])) {
                    $data[$key] = str_replace('', null, $value);
                }*/

                if (is_array($value)) {
                    $data[$key] = null;
                }
            }
        }
        return $data;
    }

    /**
     * Resolve the discount amount (amount vs rate)
     * @param $dataLine
     * @return bool|float|int
     */
    private function resolveLineDiscount($dataLine)
    {
        // Default
        $resolvedDiscount = false;

        if (!empty($dataLine['discount_amount'])) {
            $resolvedDiscount   = $dataLine['discount_amount'];
        } elseif (!empty($dataLine['discount_rate'])) {
            // Calculate discount amount from discount rate
            $lineGrossTotal     = $dataLine['material_qty'] * $dataLine['unit_price'];
            $resolvedDiscount   = $lineGrossTotal * (intval($dataLine['discount_rate']) / 100);
        }

        return $resolvedDiscount;
    }

    /**
     * Sets the document as downloaded on ReadSoft
     *
     * @param null $readsoftDocumentId
     * @param bool $returnResponse
     * @return bool|\Psr\Http\Message\ResponseInterface
     */
    public function documentDownloaded($readsoftDocumentId = null, $returnResponse = false)
    {
        $url = 'https://services.readsoftonline.com:443/documents/rest/' . $readsoftDocumentId . '/downloaded';

        if ($readsoftDocumentId == null){
            return false;
        }

        $xRsHost    = $this->jar->getCookieByName('x-rs-host');
        $xRsAuthV1  = $this->jar->getCookieByName('x-rs-auth-v1');
        $headers    = [
            'Content-Type'      => 'application/xml',
            'x-rs-key'          => $this->key,
            'x-rs-version'      => $this->version,
            'x-rs-uiculture'    => $this->uiculture,
            'x-rs-culture'      => $this->culture,
            'cookies'           => [
                'x-rs-host'     => $xRsHost->getValue(),
                'x-rs-auth-v1'  => $xRsAuthV1->getValue(),
            ]
        ];

        try {
            $response = $this->client->put($url,[
                'headers' => $headers,
            ]);
        } catch (\Exception $th) {
            dump($th->getMessage());
            return false;
        }

        if ($returnResponse) {
            return $response;
        }

        return true;
    }

    /**
     * Gets the OCR document status for a given id
     *
     * @param $readsoftDocumentId
     * @return bool|\Psr\Http\Message\ResponseInterface | array
     */
    public function documentStatus($readsoftDocumentId)
    {
        // Defaults
        $response   = false;
//        $url        = 'https://services.readsoftonline.com/documents/rest/file/' . $readsoftDocumentId . '/data';
        $url        = 'https://services.readsoftonline.com/documents/rest/' . $readsoftDocumentId;

        $xRsHost    = $this->jar->getCookieByName('x-rs-host');
        $xRsAuthV1  = $this->jar->getCookieByName('x-rs-auth-v1');
        $headers    = [
            'Content-Type'      => 'application/xml',
            'x-rs-key'          => $this->key,
            'x-rs-version'      => $this->version,
            'x-rs-uiculture'    => $this->uiculture,
            'x-rs-culture'      => $this->culture,
            'cookies' => [
                'x-rs-host'     => $xRsHost->getValue(),
                'x-rs-auth-v1'  => $xRsAuthV1->getValue(),
            ]
        ];
        $body = json_encode([
            'UserName'              => $this->user,
            'Password'              => $this->password,
            'AuthenticationType'    => 0
        ]);

        try {
            $response = $this->client->get($url, [
//                'body'      => $body,
                'headers'   => $headers,
            ]);
        } catch (\Exception $e) {
            Log::error($e->getMessage(), [
                'class'     => __CLASS__,
                'method'    => __METHOD__,
            ]);
        }

        if (!$response) {
            return false;
        }

        $trackId        = false;
        $lastStatusTxt  = false;
        $body           = $response->getBody();
        $xml            = simplexml_load_string($body);
        $json           = json_encode($xml);
        $xmlArr         = json_decode($json, true);

        if (!empty($xmlArr['History']['Item'])) {
            $docHistory     = collect($xmlArr['History']['Item']);
            $statusChanges  = $docHistory->where('EntryType', 'StatusChange');

            if (!empty($statusChanges)) {
                $lastStatus     = $statusChanges->last();
                $lastStatusTxt  = $lastStatus['Status'];
            }
        }

        if (!empty($xmlArr['TrackId'])) {
            $trackId = $xmlArr['TrackId'];
        }

        return [$lastStatusTxt, $trackId];
    }

    /**
     * Sends warning email notification for those failed files sent to ReadSoft
     *
     * @param $data
     */
    public function sendErrorSendingFileWarningEmail($data)
    {
        $sendingTo = explode(',', config('custom.warning-emails-to'));

        if (empty($sendingTo || !is_array($sendingTo))) {
            Log::error('Receiver list must be an array of email addresses', [
                'class'     => __CLASS__,
                'method'    => __METHOD__,
            ]);

        } else {
            // Only send warning notification if file is a pdf
            if ($data['fileType'] == 'application/pdf') {
                // Mail warning
                try {
                    Mail::to($sendingTo)
                        ->send(new ErrorSendingFileToReadSoftWarning($data));

                } catch (\Exception $e) {
                    Log::error($e->getMessage(), [
                        'class'     => __CLASS__,
                        'method'    => __METHOD__,
                    ]);
                }
            }
        }
    }

    /**
     * Sends warning email notification for those OCR documents with no Invoice model
     *
     * @param $documentUri
     * @param $externalId
     */
    public function sendOrphanDocumentEmail($documentUri, $externalId) : void
    {
        $sendingTo = explode(',', config('custom.warning-emails-to'));

        if (empty($sendingTo || !is_array($sendingTo))) {
            Log::error('Receiver list must be an array of email addresses', [
                'class'     => __CLASS__,
                'method'    => __METHOD__,
            ]);

        } else {
            // Mail warning about this deletion and attach the aws_route file
            try {
                Mail::to($sendingTo)
                    ->send(new OrphanOcrDocumentWarning($documentUri, $externalId));

            } catch (\Exception $e) {
                Log::error($e->getMessage(), [
                    'class'     => __CLASS__,
                    'method'    => __METHOD__,
                ]);
            }
        }
    }

    /**
     * Sends warning email notification for those soft deleted invoices
     *
     * @param $invoice
     * @param $xmlData
     * @param $extraData
     */
    public function sendWarningEmail($invoice, $xmlData, $extraData)
    {
        $sendingTo = explode(',', config('custom.warning-emails-to'));

        if (empty($sendingTo || !is_array($sendingTo))) {
            Log::error('Receiver list must be an array of email addresses', [
                'class'     => __CLASS__,
                'method'    => __METHOD__,
            ]);

        } else {
            // Mail warning about this deletion and attach the aws_route file
            try {
                Mail::to($sendingTo)
                    ->send(new FailedInvoiceWarning($invoice, $xmlData, $extraData));

            } catch (\Exception $e) {
                Log::error($e->getMessage(), [
                    'class'     => __CLASS__,
                    'method'    => __METHOD__,
                ]);
            }
        }
    }

    /**
     * Sends warning email notification for those invoices marked as NO FACTURA
     *
     * @param $invoice
     * @param $xmlData
     * @param $extraData
     */
    public function sendNonInvoiceEmail($invoice, $xmlData, $extraData)
    {
        // Defaults
        $originalSender = false;
        $sentAt         = false;
        // Lazy load relationships
        $invoice->load('emailAttachment.email');

        if ($invoice->emailAttachment && $invoice->emailAttachment->email && $invoice->emailAttachment->email->from) {
            $originalSender = $invoice->emailAttachment->email->from;
            $sentAt         = $invoice->emailAttachment->email->created_at;
        }

        // Set receivers
        $sendingTo = explode(',', config('custom.warning-emails-to'));

        (!$originalSender) ?: $sendingTo = Arr::prepend($sendingTo, $originalSender);

        if (empty($sendingTo || !is_array($sendingTo))) {
            Log::error('Receiver list must be an array of email addresses', [
                'class'     => __CLASS__,
                'method'    => __METHOD__,
            ]);

        } else {
            // Add some extra data
            $extraData['originalSender']    = $originalSender;
            $extraData['sent_at']           = $sentAt;

            try {
                // Send email
                Mail::to($sendingTo)
                    ->send(new NonInvoiceWarning($invoice, $xmlData, $extraData));

            } catch (\Exception $e) {
                Log::error($e->getMessage(), [
                    'class'     => __CLASS__,
                    'method'    => __METHOD__,
                ]);
            }
        }
    }

    /**
     * Saves original data grabbed by Ocr pulling data class
     *
     * @param $invoice
     * @param $data
     * @param bool $documentUri
     */
    public function saveOriginalData($invoice, $data, $documentUri = false) : void
    {
        // Save original sensible data (totals, taxes, everything related to calculations into a related table (relationship)
        $data['ocr_reference'] = $documentUri ?? null;

        try {
            $invoice->originalData()->create($data);
        } catch (\PDOException $e) {
            Log::error($e->getMessage(), [
                'class'     => __CLASS__,
                'method'    => __METHOD__,
            ]);
        }
    }
}
