<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CostCenterUser extends Model
{
    protected $fillable = [
        'user_id',
        'cost_center_id',
    ];
}
