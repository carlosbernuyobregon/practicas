<?php

namespace App;

use App\Traits\AutoGenerateUuid;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Storage;

class File extends Model
{
    //
    use AutoGenerateUuid;
    use SoftDeletes;

    public $incrementing = false;
    protected $keyType = 'string';
    protected $fillable = [
        'fileable_id',
        'fileable_type',
        'name',
        'filename',
        'active',
        'type',
        'cmis_id',
        'cmis_url'
    ];
    public function getDateFormat()
    {
        if(config('custom.database_type')=='sqlsrv') {
            return 'd.m.Y H:i:s';
        }
        return parent::getDateFormat();

    }
    public function fileable()
    {
        return $this->morphTo();
    }

    public function getFileUrlAttribute()
    {
        Storage::disk('s3')->download($this->cmis_url);
    }
    public function getVisualizeUrlAttribute()
    {
        $filename = 'invoice.pdf';
        $tempFilePath = public_path() . 'temp';
        $fileData = Storage::disk('s3')->get(config('custom.iworking_public_bucket_folder_invoices').$this->filename);
        Storage::putFileAs($tempFilePath,$fileData,$filename);
        return response()->download($tempFilePath,$filename,[],'inline')
            ->deleteFileAfterSend();
    }
    public function download()
    {

    }
}
