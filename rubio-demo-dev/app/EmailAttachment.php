<?php

namespace App;

use App\Traits\AutoGenerateUuid;
use Illuminate\Database\Eloquent\Model;

class EmailAttachment extends Model
{
    use AutoGenerateUuid;
    public $incrementing = false;
    protected $keyType = 'string';

    protected $fillable = [
        'email_id',
        'filename',
        'filetype',
        'customer_id',
        'aws_route'
    ];
    public function email()
    {
        return $this->belongsTo('App\Email');
    }
}
