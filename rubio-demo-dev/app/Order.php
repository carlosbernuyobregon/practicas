<?php

namespace App;

use App\Traits\AutoGenerateUuid;
use PDF;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Order extends Model
{
    use AutoGenerateUuid, SoftDeletes;
    public $incrementing = false;
    protected $keyType = 'string';
    // Appends
    protected $appends = ['calculated_total'];

    protected $fillable = [
        'customer_id',
        'external_id',
        'position',
        'material_id',
        'position_material',
        'provider_id',
        'supplier_code',
        'company',
        'qty',
        'unit_price',
        'payment_conditions',
        'payment_type',
        'description',
        'order_number',
        'date',
        'buyer',
        'purchasing_group',
        'document_type',
        'net_amount',
        'total_amount',
        'status',
        'active',
        'order_type',
        'cost_center_id',
        'user_id',
        'bpm_id'
    ];



    /**
     * RELATIONSHIPS
     */
    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function provider() // change this
    {
        return $this->belongsTo('App\Models\Provider','provider_id');
    }

    public function orderLines()
    {
        return $this->hasMany('App\OrderLine')->orderBy('position', 'asc');
    }

    public function manager()
    {
        return $this->belongsTo(User::class,'manager_id');
    }
    public function audit()
    {
        return $this->morphMany('App\Audit', 'auditable')->orderBy('created_at');
    }
    public function files()
    {
        return $this->morphMany('App\File', 'fileable')->orderBy('created_at');
    }
    public function deliveries()
    {
        return $this->hasManyThrough(Delivery::class,OrderLine::class);
    }
    public function costCenter()
    {
        return $this->belongsTo(CostCenter::class);
    }
    public function totalLines()
    {
        $total = 0;
        foreach($this->orderLines as $line){
            $total = $total + ($line->net_amount * $line->qty);
        }
        return $total;
    }

    /**
     * MUTATORS
     */
    /**
     * Get a new custom attribute on reading
     * @param $value
     * @return mixed
     */
    public function getCalculatedTotalAttribute($value)
    {
        return $this->totalLines();
    }

    public function setCustomerIdAttribute($value)
    {
        $this->attributes['customer_id'] = ($value == "") ? null : $value;
    }

    public function setMaterialIdAttribute($value)
    {
        $this->attributes['material_id'] = ($value == "") ? null : $value;
    }

    public function setProviderIdAttribute($value)
    {
        $this->attributes['provider_id'] = ($value == "") ? null : $value;
    }

    /*
     * SCOPES
     */
    public function scopeTableSearch($query, $search)
    {
        if (!empty($search['orderNumber'])) {
            $query->where('orders.order_number', 'like', '%' . $search['orderNumber'] . '%');
        }

        if (!empty($search['company'])) {
            $query->where('orders.company', $search['company']);
        }

        if (!empty($search['vatNumber'])) {
            $query->whereHas('provider', function (Builder $subQuery) use ($search) {
                $subQuery->where('vat_number', 'like', '%' . $search['vatNumber'] . '%');
            });
        }
        if (!empty($search['costCenter'])) {
            $query->where('orders.cost_center_id', $search['costCenter']);
        }

        if (!empty($search['provider'])) {
            $query->whereHas('provider', function (Builder $subQuery) use ($search) {
                $subQuery->where('name', 'like', '%' . $search['provider'] . '%');
            });
        }

        /*if (!empty($search['manager'])) {
            $query->whereHas('user', function (Builder $subQuery) use ($search) {
                $subQuery->where('first_name', 'like', '%' . $search['manager'] . '%')
                    ->orWhere('last_name', 'like', '%' . $search['manager'] . '%');
            });
        }*/

        if (isset($search['status']) && $search['status'] !== '') {
            $query->where('orders.status', $search['status']);
        }

        /*
         // There is no total field, since is a calculated values, therefore is not possible to query on mutators / appends
         if (!empty($search['totalMin']) || !empty($search['totalMax'])) {
            if (!empty($search['totalMin']) && !empty($search['totalMax'])) {
                $query->whereBetween('calculated_total', [$search['totalMin'], $search['totalMax']]);
            } else if (!empty($search['totalMin'])) {
                $query->where('calculated_total', '>=', $search['totalMin']);
            } else {
                $query->where('calculated_total', '<=', $search['totalMax']);
            }
        }*/

        if (!empty($search['manager'])) {
            $query->whereHas('manager', function (Builder $subQuery) use ($search) {
                $subQuery
                    ->where('name', 'like', '%' . $search['manager'] . '%')
                    ->orWhere('last_name', 'like', '%' . $search['manager'] . '%');
            });
        }

        if (!empty($search['createdAtFrom']) || !empty($search['createdAtTo'])) {
            if (!empty($search['createdAtFrom']) && !empty($search['createdAtTo'])) {
                $query->whereBetween('orders.created_at', [$search['createdAtFrom'], $search['createdAtTo']]);
            } else if (!empty($search['createdAtFrom'])) {
                $query->where('orders.created_at', '>=', $search['createdAtFrom']);
            } else {
                $query->where('orders.created_at', '<=', $search['createdAtTo']);
            }
        }

        if (!empty($search['dateFrom']) || !empty($search['dateTo'])) {
            if (!empty($search['dateFrom']) && !empty($search['dateTo'])) {
                $query->whereBetween('orders.date', [$search['dateFrom'], $search['dateTo']]);
            } else if (!empty($search['dateFrom'])) {
                $query->where('orders.date', '>=', $search['dateFrom']);
            } else {
                $query->where('orders.date', '<=', $search['dateTo']);
            }
        }

//        dd($query->toSql(), $query->getBindings());
    }
    public function generateOrderDocument()
    {
        ini_set('max_execution_time', 3000);
        ini_set("memory_limit","2024M");
        $pdf = \PDF::loadView('docs.order',[
            'order' => $this,
            'orderLines' => $this->orderLines
        ]);
        return $pdf->stream('O_' . $this->order_number . '.pdf')->header('Content-Type','application/pdf');
    }
}
