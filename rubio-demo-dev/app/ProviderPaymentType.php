<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class ProviderPaymentType extends Model
{
    
    protected $fillable = [
        'created_at',
        'code',
        'name',
        'description',
        'is_default'
    ];
}
