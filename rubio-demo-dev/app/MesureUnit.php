<?php

namespace App;

use App\Traits\AutoGenerateUuid;
use Illuminate\Database\Eloquent\Model;

class MesureUnit extends Model
{
    use AutoGenerateUuid;
    public $incrementing = false;
    protected $keyType = 'string';

    protected $fillable = [
        'unit',
        'name',
    ];
}
