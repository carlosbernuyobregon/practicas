<?php

namespace App;

use App\Traits\AutoGenerateUuid;
use Astrotomic\Translatable\Contracts\Translatable as ContractsTranslatable;
use Astrotomic\Translatable\Translatable;
use Illuminate\Database\Eloquent\Model;

class CustomerType extends Model implements ContractsTranslatable
{
    //
    use AutoGenerateUuid, Translatable;
    public $incrementing = false;
    protected $keyType = 'string';

    protected $fillable = [
        'key_value',

    ];
    public $translatedAttributes = [
        'name',
        'description'
    ];
    public function getDateFormat()
    {
        if (config('custom.database_type') == 'sqlsrv') {
            return 'd.m.Y H:i:s';
        }
        return parent::getDateFormat();
    }
    public function requests()
    {
        return $this->hasMany('App\Request');
    }
}
