<?php

namespace App;

use App\Traits\AutoGenerateUuid;
use Astrotomic\Translatable\Contracts\Translatable as ContractsTranslatable;
use Astrotomic\Translatable\Translatable;
use Illuminate\Database\Eloquent\Model;

class Country extends Model implements ContractsTranslatable
{
    //
    use AutoGenerateUuid, Translatable;
    public $incrementing = false;
    protected $keyType = 'string';

    protected $fillable = [
        'key_value',
        'code',
        'currency_id',
        'customer_id'

    ];
    public $translatedAttributes = [
        'name',
        'description'
    ];

    public function setCustomerIdAttribute($value)
    {
        $this->attributes['customer_id'] = ($value == "") ? null : $value;
    }

    public function currency()
    {
        return $this->belongsTo('App\Currency');
    }

    public function getDateFormat()
    {
        if (config('custom.database_type') == 'sqlsrv') {
            return 'd.m.Y H:i:s';
        }
        return parent::getDateFormat();
    }
}
