<?php

namespace App\Console\Commands;

use App\Http\Controllers\InvoicesIntegrationController;
use Illuminate\Console\Command;

class PullMessagesFromImapClient extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'invoices:pull-messages-from-imap-client';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Pulls down the messages from IMAP client, adds the emails, attachments and invoices data 
    to the database, saves the pdf files to S3 and sends them to the third party OCR service';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $processedInvoices = InvoicesIntegrationController::integrateToReadsoft();
        $this->info('Messages processed: ' . $processedInvoices);
    }
}
