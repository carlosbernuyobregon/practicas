<?php

namespace App\Console\Commands;

use App\Http\Controllers\InvoicesIntegrationController;
use Illuminate\Console\Command;

class GetInvoicesFromOcrService extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'invoices:get-analized-invoices-from-ocr';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Gets the analized invoices from third party OCR service and integrates them as full invoices data';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        list($newInvoices, $oldInvoices) = InvoicesIntegrationController::getFromReadsoft();

        $this->info('New invoices processed: ' . $newInvoices);
        $this->info('Old invoices processed: ' . $oldInvoices);
    }
}
