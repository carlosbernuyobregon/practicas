<?php

namespace App\Console\Commands;

use App\Invoice;
use App\Readsoft;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;

class MakeReadsoftDocumentDownloadBackwards extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'invoices:set-readsoft-document-as-downloaded';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Sets a document and downloaded on Readsoft';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        // ToDo: Think how to do with those invoices with no original data
        // Counter
        $downloads = [];
        // Get all invoices > 0
        $invoices = Invoice::with('originalData')
            ->where('status', '>', 0)
            ->whereNull('is_set_as_ocr_document_downloaded')
            ->get();

        // Go through all of them
        $invoices->each(function ($invoice, $key) use (&$downloads) {
            $this->info('Processing invoice id: ' . $invoice->id);
            if ($invoice->originalData) {
                $this->info('Original OCR data has been found');
                // Get the original data from backup table and get the ReadSoft internal document id from the field ocr_reference
                $reference      = $invoice->originalData->ocr_reference;
                $regex          = '/rest\/(\w+)/';
                $ocrDocumentId  = false;

                if (preg_match($regex, $reference, $matches)) {
                    $ocrDocumentId = isset($matches[1]) ? $matches[1] : null;
                }

                if ($ocrDocumentId) {
                    $this->info('OCR document id has been found: ' . $ocrDocumentId);
                    // Check the document status
                    $readSoft       = new Readsoft();
                    $connect        = $readSoft->connect();
                    list($documentStatus, $trackId) = $readSoft->documentstatus($ocrDocumentId);

                    if ($documentStatus == 'DocumentExportInProgress') {
                        // If document is in the right status, set it as downloaded
                        $response = $readSoft->documentDownloaded($ocrDocumentId, true);

                        if ($response->getStatusCode() == 200  && $response->getReasonPhrase() == 'OK') {
                            $downloads['successful'][]  = $invoice->id . ' (documentId: ' . $ocrDocumentId . ' | trackId: ' . $trackId . ')';
                            $setAsOcrDocumentDownloaded = true;
                        } else {
                            $downloads['unknown'][]     = $invoice->id . " (documentId: {$ocrDocumentId} | trackId: {$trackId} | status code: {$response->getStatusCode()} - reason phrase: {$response->getReasonPhrase()})";
                            $setAsOcrDocumentDownloaded = -1;
                        }
                    } else {
                        $this->warn('OCR document with id:' . $ocrDocumentId . ' could not be loaded. This means is not available for download');
                        $downloads['already-done'][]    = $invoice->id . " (documentId: {$ocrDocumentId} | trackId: {$trackId} | not available for download";
                        $setAsOcrDocumentDownloaded     = -2;
                    }
                } else {
                    $this->warn('OCR document id couldn\'t be parsed');
                    $setAsOcrDocumentDownloaded = -3;
                }
            } else {
                $this->warn('Original OCR data is missing');
                $downloads['ocr-data-missing'][]    = $invoice->id;
                $setAsOcrDocumentDownloaded         = -4;
            }

            // Mark those processed docs on the original data row
            $invoice->is_set_as_ocr_document_downloaded = $setAsOcrDocumentDownloaded;
            try {
                $invoice->save();
            } catch (\PDOException $e) {
                Log::error($e->getMessage(), [
                    'class'     => __CLASS__,
                    'method'    => __METHOD__,
                ]);

                $this->error($e->getMessage() . " [invoice id: {$invoice->id}]");
            }
        });

        dump($downloads);

        return true;
    }
}
