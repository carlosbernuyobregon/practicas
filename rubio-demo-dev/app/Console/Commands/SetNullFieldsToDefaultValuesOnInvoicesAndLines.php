<?php

namespace App\Console\Commands;

use App\Invoice;
use Illuminate\Console\Command;
use Illuminate\Database\Eloquent\Builder;

class SetNullFieldsToDefaultValuesOnInvoicesAndLines extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'invoices:set-null-fields-to-default-values {--justFillTaxTypes}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Sets those null fields to default values';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        // Retrieve a specific option...
        $justFillTaxTypes = $this->option('justFillTaxTypes');

        if ($justFillTaxTypes) {

            $this->info('running taxTypes mode');

            $touchedInvoices = [];

            $noTaxTypeInvoices = Invoice::whereHas('invoiceLines', function (Builder $query) {
                $query->whereNull('tax_type_id');
            })->whereIn('status', [1, 2, 3, 4, 6, 9, 11])->get();

            foreach ($noTaxTypeInvoices as $invoice) {
                $touched = false;
                foreach ($invoice->invoiceLines as $line) {
                    if (is_null($line->tax_type_id)) {
                        $line->tax_type_id  = $invoice->taxes_type;
                        $line->taxes        = $invoice->taxes_percent;
                        $line->taxes_id     = $invoice->taxes_id;
                        $touched = $line->save();

                    }
                }
                if ($touched) {
                    $touchedInvoices[] = 'https://rubio.iworking.app/invoices/' . $invoice->id . '/review';
//                    $touchedInvoices[] = 'http://127.0.0.1:8004/invoices/' . $invoice->id . '/review';
                }
            }

            dd($touchedInvoices);

            return true;
        }


        $allInvoices = Invoice::with('invoiceLines')
            ->whereNotIn('status', [0])->get();
//            ->whereIn('status', [1, 2, 4, 5, 11])->get();
        $updatesInHeader    = 0;
        $updatesInLines     = 0;

        $allInvoices->each(function ($invoice) use (&$updatesInHeader, &$updatesInLines) {
            // Set irpf to zero if it is null
            if (is_null($invoice->taxes_irpf_amount)) {
                $invoice->taxes_irpf_amount = 0;
                $updatesInHeader++;
            }

            // Set discount to zero if it is null
            if (is_null($invoice->discount_amount)) {
                $invoice->discount_amount = 0;
                $updatesInHeader++;
            }

            // Go set up defaults on lines fields
            if ($invoice->invoiceLines->isNotEmpty()) {
                $invoice->invoiceLines->each(function ($line) use (&$updatesInLines) {
                    if ($line) {
                        if ($line->material_qty === '0' || $line->material_qty === '0.00' || is_null($line->material_qty)) {
                            $line->material_qty = 1;
                            $updatesInLines++;
                        }

                        if (is_null($line->irpf)) {
                            $line->irpf = 0;
                            $updatesInLines++;
                        }

                        // Set discount to zero if it is null
                        if (is_null($line->discount_amount)) {
                            $line->discount_amount = 0;
                            $updatesInLines++;
                        }

                        // Save lines changes
                        $line->save();
                    }
                });
            }

            // Save invoice changes
            $invoice->save();
        });

        $this->info('Updates in header: ' . $updatesInHeader);
        $this->info('Updates in lines: ' . $updatesInLines);

        return true;
    }
}
