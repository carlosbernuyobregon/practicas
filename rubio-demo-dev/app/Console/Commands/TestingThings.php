<?php

namespace App\Console\Commands;

use App\Invoice;
use App\Library\Constants;
use App\Library\GroWe;
use App\Mail\FailedInvoiceWarning;
use App\Mail\OrphanOcrDocumentWarning;
use App\Mail\TestEmailSending;
use App\Models\Provider;
use App\Models\User;
use Faker\DefaultGenerator;
use Faker\Factory;
use Faker\Provider\Image;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;

class TestingThings extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'tests:testing-things {?--thing=} {?--emailType=} {?--invoiceId=} {?--raw=}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Testing things';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return boolean
     */
    public function handle()
    {
        switch ($this->option('thing')) {
            case 'emailSending':
                $this->info('emailSending test');

                $emailType = $this->option('emailType');

                $invoiceId = $this->option('invoiceId');

                $sendingTo = explode(',', config('custom.warning-emails-to'));

                if (empty($sendingTo || !is_array($sendingTo))) {
                    $this->error('Aborting. Receiver list must be an array of email addresses');
                }

                // Send email
                if ($emailType) {

                    switch ($emailType) {
                        case 'OrphanOcrDocumentWarning':

                            $documentUri = 'fake-doc-uri';
                            $externalId = 'fake-external-id';

                            try {
                                Mail::to($sendingTo)
                                    ->send(new OrphanOcrDocumentWarning($documentUri, $externalId));
                            } catch (\Exception $e) {
                                Log::error($e->getMessage(), [
                                    'class'     => __CLASS__,
                                    'method'    => __METHOD__,
                                ]);

                                $this->error('Aborting. Error: ' . $e->getMessage());
                            }
                            break;

                        case 'FailedInvoiceWarning':

                            if (!$invoiceId) {
                                $this->error('Aborting. Missing invoiceId param');
                                return false;
                            }

                            $invoice = Invoice::withTrashed()->find($invoiceId);

                            if (!$invoice) {
                                $this->error('Wrong Invoice id. Aborting');
                                return false;
                            }

                            try {
                                $data['reason'] = 'Testing email sending';

                                $xmlData        = [
                                    'invoice_number' => $invoice->invoice_number
                                ];

                                $extraData = [
                                    'externalId'        => 'dasdas-rwqerqwe-dasda-eqweq',
                                    'reason'            => 'sap_company is null',
                                    'readsoftTrackId'   => (!empty($xmlArr['TrackId'])) ? $xmlArr['TrackId'] : ''
                                ];

                                Mail::to($sendingTo)
                                    ->send(new FailedInvoiceWarning($invoice, $xmlData, $extraData));


                            } catch (\Exception $e) {
                                Log::error($e->getMessage(), [
                                    'class'     => __CLASS__,
                                    'method'    => __METHOD__,
                                ]);

                                $this->error('Aborting. Error: ' . $e->getMessage());
                            }
                            break;
                    }

                    $this->info('Email sent');
                } else {
                    $this->error('Aborting. Missing emailType param');
                }
                break;
            case 'matchInvoicesAndProviders':
                $invoices = Invoice::where('status', '>=', Constants::INVOICE_STATUS_VALIDATION_PENDING)
//                $invoices = Invoice::where('status', Constants::INVOICE_STATUS_VALIDATION_PENDING)
                    ->whereNotNull('vat_number')
                    ->where('vat_number', '!=', '[]')
                    ->whereRaw('LENGTH(vat_number) > 3')
                    ->get(['id', 'vat_number', 'provider_name', 'status']);

                $matched    = 0;
                $unmatched  = [];

                foreach ($invoices as $invoice) {
                    $provider = Provider::where('vat_number', trim($invoice->vat_number))
                        ->orWhere('community_vat_number', trim($invoice->vat_number))
                        ->first();

                    if ($provider) {
                        $matched++;
                    } else {
                        $unmatched['https://rubio.iworking.app/invoices/' . $invoice->id] = $invoice->vat_number . ' [' . $invoice->provider_name . '] | (status: ' . $invoice->status . ')';
                    }
                }

                dd('invoices count: ' . $invoices->count(), 'matched providers: ' . $matched, 'unmatched invoices/vat: ', $unmatched);
                break;
        }
    }
}
