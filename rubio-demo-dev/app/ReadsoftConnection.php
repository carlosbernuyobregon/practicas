<?php

namespace App;

use HTTP_Request2;
use Illuminate\Database\Eloquent\Model;

class ReadsoftConnection extends Model
{
    //
    protected $request = null;
    protected $response = null;
    protected $user = "UserAPI";
    protected $password = "#EJ_+W@a";
    protected $key = 'df9aab45530c45a39aedc23db496a1a2';
    protected $version = '2011-10-14';
    protected $uiculture = 'en-US';
    protected $culture = 'en-US';
    protected $client = null;
    protected $headers = null;
    protected $jar = null;
    protected $customerid = 'df9aab45530c45a39aedc23db496a1a2';
    protected $buyerid = 'c9eb571ce05d43b0a9166be3ae95ae85';

    public function connect()
    {
        $this->request = new HTTP_Request2();
        $this->request->setUrl('https://services.readsoftonline.com/authentication/rest/authenticate');
        $this->request->setMethod(HTTP_Request2::METHOD_POST);
        $this->request->setConfig(array(
        'follow_redirects' => TRUE
        ));
        $this->request->setHeader(array(
        'x-rs-key' => ' df9aab45530c45a39aedc23db496a1a2',
        'x-rs-version' => '2011-10-14',
        'x-rs-uiculture' => 'en-US',
        'x-rs-culture' => 'en-US',
        'Content-Type' => 'application/json',
        'Cookie' => 'x-rs-auth-v1=A4E875714862A377BB7A53635089251C685ED5FD787FB1D56237864B37F076F7844E42E478DD53AC79B967A3BB6A347CF074B954E2AEF2D9D7CF5297D8A0EF9CAFC71B7543DBEA905DEF8DD5439AB9533110ED34FC04D3247F33F54E051C505CB1D1DEEC802B32789FFEBA23F05BD7A10777502A583F557706219CD8950A3DF364BA9947ECA126A9554112500B4E57CC163ACED66ED6610DC7916228FED0048AE1336ADD04A82BA6C8837154ABDCCC838C3341E36488910D60B077BC62FF54481B2BA0C29ED2BBA85E03B81F95967842FF063ABF7AE94B446DE0DDD6B8A39DCD00F2C69F9D809F809851E17DEB1F265EE94F0ACA613B69624A5755EEA0654257BCEDC7ADE1197A65CEE8DDA33A629ADA87FF9D9212CD9B98F20F1EDE322046EC97A8B6C15A44150EE58CDA189C01B861; x-rs-host=strategying.readsoftonline.com'
        ));
        $this->request->setBody('{"UserName":"UserAPI","Password":"#EJ_+W@a","AuthenticationType":0}');
        try {
        $this->response = $this->request->send();
        if ($this->response->getStatus() == 200) {
            echo $this->response->getBody();
        }
        else {
            echo 'Unexpected HTTP status: ' . $this->response->getStatus() . ' ' .
            $this->response->getReasonPhrase();
        }
        }
        catch(\HTTP_Request2_Exception $e) {
        echo 'Error: ' . $e->getMessage();
        }
    }
    public function sendFile($filename = null, $externalid = null,$emailPath = null)
    {
        $url = 'https://services.readsoftonline.com/files/rest/image?'.
        'filename='.$filename.
        '&customerid='.$this->customerid.
        '&batchexternalid='.$externalid.
        '&buyerid='.$this->buyerid.
        '&documenttype=pdf';
        $this->request->setUrl($url);
        $this->request->setMethod(HTTP_Request2::METHOD_POST);
        $this->request->setConfig(array(
          'follow_redirects' => TRUE
        ));
        $this->request->setHeader(array(
          'x-rs-key' => ' df9aab45530c45a39aedc23db496a1a2',
          'x-rs-version' => '2011-10-14',
          'x-rs-uiculture' => 'en-US',
          'x-rs-culture' => 'en-US',
          'Content-Type' => 'application/pdf',
          'Cookie' => 'x-rs-auth-v1=A4E875714862A377BB7A53635089251C685ED5FD787FB1D56237864B37F076F7844E42E478DD53AC79B967A3BB6A347CF074B954E2AEF2D9D7CF5297D8A0EF9CAFC71B7543DBEA905DEF8DD5439AB9533110ED34FC04D3247F33F54E051C505CB1D1DEEC802B32789FFEBA23F05BD7A10777502A583F557706219CD8950A3DF364BA9947ECA126A9554112500B4E57CC163ACED66ED6610DC7916228FED0048AE1336ADD04A82BA6C8837154ABDCCC838C3341E36488910D60B077BC62FF54481B2BA0C29ED2BBA85E03B81F95967842FF063ABF7AE94B446DE0DDD6B8A39DCD00F2C69F9D809F809851E17DEB1F265EE94F0ACA613B69624A5755EEA0654257BCEDC7ADE1197A65CEE8DDA33A629ADA87FF9D9212CD9B98F20F1EDE322046EC97A8B6C15A44150EE58CDA189C01B861; x-rs-host=strategying.readsoftonline.com'
        ));
        $this->request->setBody(file_get_contents($emailPath.$filename));
        try {
            $this->response = $this->request->send();
            if ($this->response->getStatus() == 200) {
              echo $this->response->getBody();
            }
            else {
              echo 'Unexpected HTTP status: ' . $this->response->getStatus() . ' ' .
              $this->response->getReasonPhrase();
            }
          }
          catch(\HTTP_Request2_Exception $e) {
            echo 'Error: ' . $e->getMessage();
          }
    }
}
