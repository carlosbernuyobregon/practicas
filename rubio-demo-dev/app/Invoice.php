<?php

namespace App;

use App\Helpers\Helpers;
use App\Library\Constants;
use App\Models\InvoiceOriginalOcrData;
use App\Models\Provider;
use App\Models\ProviderRule;
use App\Traits\AutoGenerateUuid;
use http\Client\Request;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Arr;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;

class Invoice extends Model
{
    use AutoGenerateUuid, SoftDeletes;
    public $incrementing            = false;
    // Errors
    public $adjustedVsTotalError    = false;
    public $originalTotalError      = false;
    public $originalTotalTaxesError = false;
    public $originalTotalIrpfError  = false;
    public $originalMissedDataError = false;
    public $baseForIrpfError        = false;
    public $paymentDateVTypesError  = false;
    // End Errors
    protected $keyType              = 'string';
    protected $guarded              = ['id'];
    //protected $invoiceAwsFolder = '/invoices/attachments/';

    // Appends
    protected $appends = ['base_irpf_sap', 'has_unread_messages', 'concat_tax_types'];

    /*
     * RELATIONSHIPS
     */
    public function invoiceLines()
    {
        return $this->hasMany('App\InvoiceLine')->orderBy('item_order_num_bulk');
    }
    public function invoiceLinesLite()
    {
        return $this->hasMany(InvoiceLine::class)->select([
            'invoice_id',
            'tax_type_id',
            'taxes_id',
            'net_amount',
            'taxes_amount',
            'total_amount',
            'taxes',
            'irpf',
            'discount_amount',
            'item_coming_from_header'
        ])->orderBy('item_order_num_bulk');
    }
    public function tax()
    {
        return $this->belongsTo('App\TaxType','taxes_type');
    }
    public function emailAttachment()
    {
        return $this->belongsTo('App\EmailAttachment');
    }
    public function audit()
    {
        return $this->morphMany('App\Audit', 'auditable')->orderBy('created_at');
    }

    /**
     * Get the Invoice original data saved at the time the invoice was pulled and created from OCR.
     */
    public function originalData()
    {
        return $this->hasOne(InvoiceOriginalOcrData::class);
    }

    /**
     * Get the Invoice provider data.
     */
    public function provider()
    {
        return $this->hasOne(Provider::class, 'vat_number', 'vat_number');
    }

    /**
     * Get all of the invoice's chats.
     */
    public function chats()
    {
        return $this->morphMany(Comment::class, 'commentable');
    }

    /*
     * METHODS
     */

    public function getFileUrlAttribute()
    {
        return route('invoice.download',['filename' => $this->filename]);
        return Storage::disk('s3')->download(config('custom.iworking_public_bucket_folder_invoices').$this->filename);

        if($this->file!=null){
            return asset('files/invoices/'.$this->file);
        }
        $email = EmailAttachment::find($this->email_attachment_id);
        return asset('attachments/' . $email->email_id . '/' . $email->filename);
    }
    public function getVisualizeUrlAttribute()
    {
        $filename = 'invoice.pdf';
        $tempFilePath = public_path() . 'temp';
        $fileData = Storage::disk('s3')->get(config('custom.iworking_public_bucket_folder_invoices').$this->filename);
        Storage::putFileAs($tempFilePath,$fileData,$filename);
        return response()->download($tempFilePath,$filename,[],'inline')
            ->deleteFileAfterSend();
    }
    public function getDownloadFileUrlAttribute()
    {
        $email = EmailAttachment::find($this->email_attachment_id);
        return public_path('attachments/' . $email->email_id . '/' . $email->filename);
    }

    /**
     * Gets the invoice country through Provider relationship
     * @return bool
     */
    public function giveMeTheInvoiceCountryByProvider()
    {
        $this->load('provider');

        if ($this->provider) {
            return $this->provider->country;
        }

        return $this->provider_country;
    }

    /**
     * Returns the check invoice type to be performed and the reason of that choice
     *
     * @return array
     */
    public function giveMeTheTypeOfCheckInvoice()
    {
        $invoiceCountry = $this->giveMeTheInvoiceCountryByProvider();

        if (!$invoiceCountry) {
            // No data
            $checkType          = Constants::CHECK_INVOICE_REGULAR_METHOD;
            $checkTypeReason    = Constants::CHECK_INVOICE_REASON_NO_DATA;
        } elseif ($invoiceCountry == 'ES') {
            // Regular checks
            $checkType          = Constants::CHECK_INVOICE_REGULAR_METHOD;
            $checkTypeReason    = Constants::CHECK_INVOICE_REASON_SPANISH;
        } else {
            // Specific checks
            $checkType          = Constants::CHECK_INVOICE_SPECIFIC_METHOD;
            $checkTypeReason    = Constants::CHECK_INVOICE_REASON_NO_SPANISH;
        }

        return [$checkType, $checkTypeReason];
    }

    /**
     * Checks if invoice key values are correct
     *
     * @param bool $justReturnErrors
     * @return array|bool|mixed
     */
    public function checkInvoice($justReturnErrors = false)
    {
        // The checks are totally different between spanish and non spanish invoices, lets find out the invoice's country
        list($checkType, $checkTypeReason)  = $this->giveMeTheTypeOfCheckInvoice();
        $isForeignInvoice                   = $checkType == Constants::CHECK_INVOICE_SPECIFIC_METHOD && $justReturnErrors;

        // New considerations, taking into account discounts application and special tax types (autorepercutido, etc)
        $calculatedValues           = $this->recalculateTotals(true, $isForeignInvoice);
        $calculatedValuesFromLines  = collect($this->recalculateTotalsAndTaxesForBreakDownFields(false, true, $isForeignInvoice));

        // Lazy load relationship
        $this->load('invoiceLines');

        // Defaults
        $this->is_correct = false;

        // Single or multiple Tax Type)
        $uniqueTaxTypeId    = $this->invoiceLines->unique('tax_type_id');
        $singleTaxType      = $uniqueTaxTypeId->pluck('tax_type_id')->count() == 1;

        // Compare calculated total amount vs real value stored in db
        // Some tweaks are needed in order to correctly compare values for both approaches (single and multiple tax type)
        // Total to compare comes from different way of calculating the final total amount. When it comes from a multiple
        // tax type invoice, the total is the result of the sum of the lines totals and the global IRPF (from the inovice header)
        // is not included in that line total calculation, that's why it is subtracted from the that summation
        /*
         * Since every invoice get the total values from lines, this is not necessary anymore. I'll keep it for a
         * while and then delete this chunk of code
         * if ($singleTaxType) {
            $totalToCompare = $calculatedValuesFromLines->sum('totalAmount');
        } else {
            $totalToCompare = $calculatedValuesFromLines->sum('totalAmount') - $this->taxes_irpf_amount;
        }*/

        // The total is the result of the sum of the lines totals and the global IRPF (from the inovice header)
        // is not included in that line total calculation, that's why it is subtracted from that summation
        $totalToCompare = $calculatedValuesFromLines->sum('adjustedTotalAmount') - $this->taxes_irpf_amount;

        // Verify if the sum of the imports are correct.
        // This check must be done comparing calculated values against those broke down values coming from lines
        // Lets do the 'one cent gap' allowance

        // [LAST JULY REFACTORING] I think this not necessary anymore since the tax amount from grouped lines is being
        // calculated right way. Applying the tax percentage to the group net_amount and not total sum of the line taxes
        $towCentsGap = Helpers::applyTwoCentsGapRuleForTotals($calculatedValues, $calculatedValuesFromLines, $totalToCompare);

        $realTotalVsCalculated = Helpers::numberBetween(
            (!$isForeignInvoice) ? $this->total : $calculatedValues['totalAmount'],
            round($totalToCompare + 0.02, 2),
            round($totalToCompare - 0.02, 2)
        );

        // Invoice date check. Should not be greater than today
        if ($this->date) {
            $dateIsValid = $this->isInvoiceDateValid('greaterThanToday');
        } else {
            $dateIsValid = false;
        }

        if (round($calculatedValues['totalNetAmount'], 2)   == round($calculatedValuesFromLines->sum('netAmount'), 2) &&
            round($calculatedValues['taxesTotalAmount'], 2) == round($calculatedValuesFromLines->sum('adjustedTaxAmount'), 2) &&
            round($calculatedValues['totalAmount'], 2)      == round($totalToCompare, 2)
        ) {
            // Verify if the invoice has all the required data
            if (
                $realTotalVsCalculated &&
                $dateIsValid &&
                !empty($this->invoice_number) &&
                !empty($this->sap_company) &&
                !empty($this->vat_number) &&
                ($this->total != null || $this->total != 0) &&
                ($this->total_net != null || $this->total_net != 0) &&
                ($this->intern_order != null || $this->intern_order != '') &&
                (is_null($this->alert_flag_from_ocr))
            ) {
                // Tax type on header is not used anymore
                $this->is_correct = true;
                // Tax type check, since can be null for invoices with multiple tax types
                /*if (empty($this->taxes_type)) {
                    ($singleTaxType) ? $this->is_correct = false : $this->is_correct = true;
                } else {
                    $this->is_correct = true;
                }*/
            } elseif ($this->alert_flag_from_ocr  && $this->status != 4) {
                $this->is_correct = true;
            }
        }

        /**
         * Extra checks
         * These are extra checks. Lets leave the main logic check as it is and start adding new checks below.
         * The new checks must change the value of the [$this->is_correct] attribute in order to avoid invoice approval
         * The error text will be added to the [$errors] list.
         */

        // Compares current invoice key amounts with the original data scrapped by the ocr service
        $this->areTotalAmountAndOriginalTotalAmountTheSame();

        // Checks if the base value for IRPF calculation satisfies a special formula
//        $this->checkBaseForIrpfCalculation(); // Client requested to hide this check. They want to edit the value no matter what

        // aaa
        if (Helpers::atLeastOneLineWithTaxType($this, 'V') && !$this->payment_date) {
            $this->is_correct               = false;
            $this->paymentDateVTypesError   = Constants::CHECK_ERROR;
        }

        /**
         * Extra checks end
         */

        // Just return an array with errors
        if ($justReturnErrors) {
            // Defaults
            $errors     = [];
            $warnings   = [];

            if ($isForeignInvoice) {
                // Only throw the warning if there is at least one regular tax type (since autorrepercutido is not
                // adding the taxes to the total amount by default) and the tax amount is different from zero
                $atLeastOneTaxNoSpecialType = false;
                foreach ($calculatedValuesFromLines as $fromLine) {
                    if ($fromLine['taxSpecialType'] != Constants::TAX_SPECIAL_TYPE_AUTOREPERCUTIDO) {
                        $atLeastOneTaxNoSpecialType = true;
                    }
                }

                if ($atLeastOneTaxNoSpecialType && $calculatedValues['taxesTotalAmount'] != 0) {
                    $warningForeignInvoiceTxt   = 'Atención! Las comprobaciones del total se ha efectuado sin tener en cuenta el IVA, ya que es una factura extranjera';
                    $warnings                   = Arr::prepend($warnings, $warningForeignInvoiceTxt, 'foreignInvoice');
                }
            }

            // Lets do the warnings
            /*if ($towCentsGap) {
                $warnings = [];

                if (round($calculatedValues['totalNetAmount'], 2) != round($calculatedValuesFromLines->sum('netAmount'), 2)) {
                    $errorTotalNetTxt   = 'La base imponible difiere de la suma de totales neto de las líneas (' . number_format($calculatedValuesFromLines->sum('netAmount'),2,',','.') . ' ' . $this->currency . ')';
                    $errorTotalNetTxt   = false;
                    $warnings           = Arr::prepend($warnings, $errorTotalNetTxt, 'invoiceTotalNetError');
                }

                if ((round($calculatedValues['taxesTotalAmount'], 2) != round($calculatedValuesFromLines->sum('taxAmount'), 2))) {
                    $errorTotalTaxesTxt = 'El total de impuestos difiere de la suma de totales de impuestos de las líneas (' . number_format($calculatedValuesFromLines->sum('taxAmount'),2,',','.') . ' ' . $this->currency . ')';
                    $errorTotalTaxesTxt = false;
                    $warnings           = Arr::prepend($warnings, $errorTotalTaxesTxt, 'invoiceTotalTaxesError');
                }

                if ((round($calculatedValues['totalAmount'], 2) != round($totalToCompare, 2))) {
                    $errorTotalTxt  = 'El total difiere de la suma de totales de las líneas (' . number_format($totalToCompare,2,',','.') . ' ' . $this->currency . ')';
                    $errorTotalTxt  = false;
                    $warnings       = Arr::prepend($warnings, $errorTotalTxt, 'invoiceTotalAmountError');
                }

                if (!empty($warnings)) {
//                    $warningText                = 'En cuanto a totales, la factura se puede aprobar pero se debe tener en cuenta que fue aplicada regla de la diferencia de +-2 céntimos en las comprobaciones de los mismos';
                    $warningText                = 'Aplicada regla de la diferencia de +-2 céntimos en las comprobaciones de los totales';
                    $warnings['warningText']    = $warningText;
                }
            }*/

            // Only fill it if there is actually some error
            if (!$this->is_correct) {
                $errors = [
                    'invoiceTotalNetError'      => (round($calculatedValues['totalNetAmount'], 2) != round($calculatedValuesFromLines->sum('netAmount'), 2) && empty($warnings['invoiceTotalNetError']))
                        ? 'La base imponible difiere de la suma de totales neto de las líneas (' . number_format($calculatedValuesFromLines->sum('netAmount'),2,',','.') . ' ' . $this->currency . ')' : null,
                    /*'invoiceTotalTaxesError'    => (round($calculatedValues['taxesTotalAmount'], 2) != round($calculatedValuesFromLines->sum('taxAmount'), 2) && empty($warnings['invoiceTotalTaxesError']))
                        ? 'El total de impuestos difiere de la suma de totales de impuestos de las líneas (' . number_format($calculatedValuesFromLines->sum('taxAmount'),2,',','.') . ' ' . $this->currency . ')' : null,
                    'invoiceTotalAmountError'   => ((round($calculatedValues['totalAmount'], 2) != round($totalToCompare, 2) || !$realTotalVsCalculated) && empty($warnings['invoiceTotalAmountError']))
                        ? 'El total difiere de la suma de totales de las líneas (' . number_format($totalToCompare,2,',','.') . ' ' . $this->currency . ')' : null,*/
                    'internOrderError'          => empty($this->intern_order)
                        ? 'Falta número de pedido' : null,
                    'dateError'                 => empty($this->date)
                        ? 'Falta fecha de factura' : null,
                    'dateGreaterThanTodayError' => !$this->isInvoiceDateValid('greaterThanToday')
                        ? 'Fecha de factura mayor al día en curso' : null,
                    'taxTypeError'              => (empty($this->taxes_type) && $singleTaxType)
                        ? 'Falta tipo de impuesto' : null,
                    'totalError'                => ($this->total == null || $this->total == 0)
                        ? 'El total no es válido' : null,
                    'totalNetError'             => ($this->total_net == null || $this->total_net == 0)
                        ? 'La base imponible no es válida' : null,
                    'sapCompanyError'           => empty($this->sap_company)
                        ? 'Falta Sociedad' : null,
                    'invoiceNumberError'        => empty($this->invoice_number)
                        ? 'Falta nro de factura' : null,
                    'invoiceVatError'           => empty($this->vat_number)
                        ? 'Falta CIF/NIF/VAT' : null,
                    'ocrMissingData'            => $this->originalMissedDataError ?
                        'No hemos podido recuperar los datos de los totales guardados por el OCR. Por favor revise la factura manualmente' : null,
                    'baseForIrpfError'          => $this->baseForIrpfError ?
                        'La base para el cálculo del IRPF ha sido editada y no es correcta' : null,
                    'paymentDateVTypesError'    => $this->paymentDateVTypesError ?
                        'La fecha de vencimiento es obligatoria para facturas con IVA de tipo V (Tracto Sucesivo)' : null

//                    'AdjustedVsRealValueError'  => !empty($this->adjustedVsTotalError) ? '<span class="badge badge-pill badge-danger">&nbsp</span>El valor ajustado no se corresponde con el valor calculado' : null
                ];

                // Alert flag error (info coming from OCR)
                if ($this->alert_flag_from_ocr && $this->status == 4) {
                    switch ($this->alert_flag_from_ocr) {
                        case '0001':
                            $flagError = 'Faltan datos requeridos';
                            break;
                        case '0002':
                            $flagError = 'Factura repetida';
                            break;
                        case '0003':
                            $flagError = 'Más de una factura en el mismo PDF';
                            break;
                        case '0004':
                            $flagError = 'Factura antigua';
                            break;
                        case '0005':
                            $flagError = 'No es una factura';
                            break;
                        default:
                            $flagError = null;
                            break;
                    }

                    if ($flagError) {
                        $errors['alertFlagError'] = $flagError . ' (fuente: OCR)';
                    }
                }
            }

            return [$errors, $warnings];
        }

        return $this->is_correct;
    }

    /**
     * Checks if grouped lines totals are similar to adjusted values
     *
     * @param $groupedLinesData
     */
    private function areAdjustedAndTotalAmountTheSame($groupedLinesData)
    {
        foreach ($groupedLinesData as $lineData) {
            if (round($lineData['totalAmount'], 2) == round($lineData['adjustedTotalAmount'], 2)) {
                $lineData['adjustedVsTotal'] = true;
            } else {
                $lineData['adjustedVsTotal']    = false;
                $this->is_correct               = false;
                $this->adjustedVsTotalError     = true;
            }
        }
    }

    /**
     * Compares current key values against original values pulled from the OCR service
     */
    private function areTotalAmountAndOriginalTotalAmountTheSame() : void
    {
        // Lazy load relationship
        $this->load('originalData');

        if ($this->originalData) {
            // Original data
            $originalTotal = round($this->originalData->total, 2);
            // Check totals
            if ($originalTotal != round($this->total, 2)) {
                // If the original value is zero, lets throw a warning and not and error, since could be a missing value
                if ($originalTotal == 0) {
                    $this->originalTotalError = Constants::CHECK_WARNING;
                } else {
                    $this->originalTotalError = Constants::CHECK_ERROR;
                    // Mark invoice as incorrect
                    $this->is_correct = false;
                }
            }

            // Original data
            $originalTaxTotal = round($this->originalData->taxes_total_amount, 2);
            // Check taxes
            if ($originalTaxTotal != round($this->taxes_total_amount, 2)) {
                // If the original value is zero, lets throw a warning and not and error, since could be a missing value
                if ($originalTaxTotal == 0) {
                    $this->originalTotalTaxesError = Constants::CHECK_WARNING;
                } else {
                    $this->originalTotalTaxesError = Constants::CHECK_ERROR;
                    // Mark invoice as incorrect
                    $this->is_correct = false;
                }
            }

            // Original data
            $originalIrpfTotal = round($this->originalData->taxes_irpf_amount, 2);
            // Check IRPF
            if ($originalIrpfTotal != round($this->taxes_irpf_amount, 2)) {
                // If the original value is zero, lets throw a warning and not and error, since could be a missing value
                if ($originalIrpfTotal == 0) {
                    $this->originalTotalIrpfError = Constants::CHECK_WARNING;
                } else {
                    $this->originalTotalIrpfError = Constants::CHECK_ERROR;
                    // Mark invoice as incorrect
                    $this->is_correct = false;
                }
            }

        } else {
            $this->originalMissedDataError  = true;
            $this->originalTotalError       = Constants::CHECK_WARNING;
            $this->originalTotalTaxesError  = Constants::CHECK_WARNING;
            $this->originalTotalIrpfError   = Constants::CHECK_WARNING;
        }
    }

    /**
     * Checks if the edited value of the calculation base for IRPF is correct
     */
    private function checkBaseForIrpfCalculation() : void
    {
        if ($this->total_net_for_non_zero_tax_percentage != 0 && $this->total_net_for_non_zero_tax_percentage != $this->total_net_for_non_zero_tax_percentage_calculated) {
            $valueToCheck =
                round($this->total - $this->taxes_total_amount + $this->taxes_irpf_amount, 2);

            if ($this->total_net_for_non_zero_tax_percentage != $valueToCheck) {
                $this->baseForIrpfError = Constants::CHECK_ERROR;
                $this->is_correct       = false;
            }
        }
    }

    /**
     * Checks if invoice lines key values are correct
     *
     * @return bool
     */
    public function checkInvoiceLines()
    {
        // Lazy load the relationship
        $this->load('invoiceLines');

        foreach ($this->invoiceLines as $line) {
            if (!$line->checkInvoiceLine()) {
                return false;
            }
        }

        return true;
    }

    /**
     * Recalculates model total amount fields
     * @param bool $justReturnCalculations
     * @param bool $isForeignInvoice
     * @return array | void
     */
    public function recalculateTotals($justReturnCalculations = false, $isForeignInvoice = false)
    {
        // Lazy load the relationship
        $this->load('tax', 'invoiceLines');

        // Grouping Tax Types approach. Works for both, single and multiple
        if (!$justReturnCalculations) {
            $this->recalculateTotalsAndTaxesForBreakDownFields(true);
        }
        $taxesTotalAmount                   = false;
        $taxesTotalAmountWithoutAdjustment  = false;
        $totalAmount                        = false;
        $totalNetAmount                     = false;
        $taxSpecialType                     = false;

        for ($i = 2; $i <= 5; $i++) {
            $currentTaxAmount                   = 'tax_amount_'             . $i;
            $currentTotalAmount                 = 'adjusted_total_amount_'           . $i;
            $currentNetAmount                   = 'total_net_'              . $i;
            $currentAdjustedTax                 = 'adjusted_tax_amount_'    . $i;
            $taxesTotalAmountWithoutAdjustment  += $this->{$currentTaxAmount};
            $taxesTotalAmount                   += $this->{$currentAdjustedTax};
            // Since this values come from the lines, the calculation is already including discounts and special
            // tax types maths (like autorepercutido)
            $totalAmount        += $this->{$currentTotalAmount};

            // Replace header values that don't have any sense for invoices with multiple tax types
            $totalNetAmount     += $this->{$currentNetAmount};
        }

        // IRPF and Discount should be subtracted from the total, since is a header item and it is no included in the total line calculation
        $totalAmount -= $this->taxes_irpf_amount;

        if ($justReturnCalculations) {

            if ($isForeignInvoice) {
                // Do not apply taxes at all if the invoice is not a spanish one.
                // Since the recalculation logic is already complex, lets the normal flow apart and do the job here
                return [
                    'taxSpecialType'                    => $taxSpecialType,
                    'totalNetAmount'                    => $this->total_net,
                    'taxesTotalAmount'                  => $taxesTotalAmount,
                    'taxesTotalAmountWithoutAdjustment' => $taxesTotalAmountWithoutAdjustment,
                    'totalAmount'                       => $this->total_net,
                ];
            }

            return [
                'taxSpecialType'                    => $taxSpecialType,
                'totalNetAmount'                    => $this->total_net,
                'taxesTotalAmount'                  => $this->taxes_total_amount,
                'taxesTotalAmountWithoutAdjustment' => $this->taxes_total_amount_without_adjustment,
                'totalAmount'                       => $this->total,
            ];
        }

        // Calculate the right value for total_net_for_non_zero_tax_percentage
        /*if ($this->taxes_irpf_amount == 0) {
            $totalBaseForIRPFCalculation = 0;
        } else {
            if ($this->total_net_for_non_zero_tax_percentage != 0) {
                $totalBaseForIRPFCalculation = $this->total_net_for_non_zero_tax_percentage;
            } else {
                $totalBaseForIRPFCalculation = $this->getTotalNetForNonZeroTaxTypeLines()
            }
        }*/

        // Assign calculated values
        $this->taxes_total_amount                               = $taxesTotalAmount;
        $this->taxes_total_amount_without_adjustment            = $taxesTotalAmountWithoutAdjustment;
        $this->total                                            = $totalAmount;
        $this->total_net_for_non_zero_tax_percentage            = $this->giveMeTheRightBaseValueForIrpfCalculation();
        $this->total_net_for_non_zero_tax_percentage_calculated = ($this->taxes_irpf_amount != 0) ? $this->getTotalNetForNonZeroTaxTypeLines() : 0;

        if (isset($totalNetAmount)) {
            $this->total_net = $totalNetAmount;
        }

        try {
            $this->save();
        } catch (\PDOException $e) {
            Log::error($e->getMessage(), [
                'class'     => __CLASS__,
                'method'    => __METHOD__,
            ]);
        }

    }

    /**
     * Resets the value for those fields related to break down totals and taxes (total_net_2, total_net_3, etc)
     *
     * @param $unsetUnusedFields
     * @param $justReturnCalculations
     * @param bool $isForeignInvoice
     * @return array | void
     *
     */
    public function recalculateTotalsAndTaxesForBreakDownFields(
        $unsetUnusedFields = false,
        $justReturnCalculations = false,
        $isForeignInvoice = false
    )
    {
        // Default
        $breakDown      = [];
        $calculation    = [];

        // Lazy load relationship
        $this->load('invoiceLinesLite');

        // Go and update the extra fields for total, taxes break down, even if there is just one tax type in lines
        // Group the lines by tax type
        $groupedLines = $this->invoiceLinesLite->groupBy('tax_type_id');
        // Leave out those lines inherited from header items (only discount type by now)
//        $groupedLines = $this->invoiceLinesLite->whereNotIn('item_coming_from_header', [Constants::HEADER_ITEM_TO_LINE_TYPE_DISCOUNT])->groupBy('tax_type_id');

        if (!$justReturnCalculations) {
            // Reset values for those break down fields (total_net_2, etc)
            $this->resetTotalsBreakDownFields();
        }

        // Default
        $dataPerGroup = [];

        // Go through each group and make maths
        try {
            $groupedLines->each(function ($group, $key) use (&$dataPerGroup) {
                // Prepare data per group
                $dataPerGroup[$key] = [
                    'netAmount'     => round($group->sum('net_amount'), 2),
                    'taxAmount'     => round($group->sum('taxes_amount'), 2),
                    'totalAmount'   => round($group->sum('total_amount'), 2),
                    'taxRate'       => $group->pluck('taxes')->first(),
                    'taxType'       => $group->pluck('taxes_id')->first(),
                    'irpf'          => round($group->sum('irpf'), 2),
                    'discount'      => round($group->sum('discount_amount'), 2),
                ];
            });
        } catch (\Exception $e) {
            Log::error($e->getMessage(), [
                'class'     => __CLASS__,
                'method'    => __METHOD__,
            ]);
        }

        // Safety unset
        unset($groupedLines);

        // Assign broke down values to invoice (total_net_2, total_net_3, etc)
        foreach (collect($dataPerGroup)->values() as $key => $group) {
            // This represents the amount of break down fields available in the table (from total_net_2 to total_net_5
            if ($key > 4) {
                continue;
            }

            try {
                $currentTotalNet                = 'total_net_'              . strval($key + 2);
                $currentTaxAmount               = 'tax_amount_'             . strval($key + 2);
                $currentTaxRate                 = 'tax_rate_'               . strval($key + 2);
                $currentTaxType                 = 'tax_type_'               . strval($key + 2);
                $currentTaxBalance              = 'tax_balance_range_'      . strval($key + 2);
                $currentAdjustedTax             = 'adjusted_tax_amount_'    . strval($key + 2);
                $currentTotalAmount             = 'total_amount_'           . strval($key + 2);
                $currentAdjustedTotalAmount     = 'adjusted_total_amount_'  . strval($key + 2);

                // Seek for special tax type
                $taxType = TaxType::where('type', $group['taxType'])->first();

                if (!$justReturnCalculations) {

                    // Only assign new values if there is a proper calculation and not just a simple data return
                    $this->{$currentTotalNet}               = $group['netAmount'];
//                    $this->{$currentTaxAmount}              = $group['taxAmount'];
                    $this->{$currentTaxAmount}              = $group['netAmount'] * (intval($group['taxRate']) / 100);
                    $this->{$currentTaxRate}                = $group['taxRate'];
                    $this->{$currentTaxType}                = $group['taxType'];
                    $this->{$currentAdjustedTax}            = $this->{$currentTaxAmount} + ($this->{$currentTaxBalance} / 100); // Since tax_balance_range_x represents cents;

                    switch ($taxType->special_type) {
                        case Constants::TAX_SPECIAL_TYPE_AUTOREPERCUTIDO:
                            $this->{$currentTotalAmount}            = $this->{$currentTotalNet} - $group['irpf'];
                            $this->{$currentAdjustedTotalAmount}    = $this->{$currentTotalAmount};
                            break;
                        default:
                            $this->{$currentTotalAmount}            = $this->{$currentTotalNet} + $this->{$currentTaxAmount} - $group['irpf'];
                            $this->{$currentAdjustedTotalAmount}    = $this->{$currentTotalAmount} + $this->{$currentAdjustedTax} - $this->{$currentTaxAmount};
                            break;
                    }
                }

                if ($isForeignInvoice) {
                    // Do not apply taxes at all if the invoice is not a spanish one.
                    // Since the recalculation logic is already complex, lets the normal flow apart and do the job here
                    $calculation[$key + 2] = collect([
                        'taxSpecialType'        =>$taxType->special_type,
                        'netAmount'             => $group['netAmount'],
                        'taxAmount'             => $this->{$currentTaxAmount},
                        'adjustedTaxAmount'     => $this->{$currentAdjustedTax},
                        'totalAmount'           => $group['netAmount'],
                        'adjustedTotalAmount'   => $group['netAmount'],
                    ]);
                } else {
                    $calculation[$key + 2] = collect([
                        'netAmount'             => $group['netAmount'],
                        'taxAmount'             => $this->{$currentTaxAmount},
//                    'taxAmount'             => round($group['netAmount'] * (intval($group['taxRate']) / 100), 2),
                        'taxRate'               => $group['taxRate'],
                        'taxBalanceRange'       => $this->{$currentTaxBalance},
                        'adjustedTaxAmount'     => $this->{$currentAdjustedTax},
                        'taxType'               => $group['taxType'],
                        'irpf'                  => $group['irpf'],
                        'discount'              => $group['discount'],
                        'totalAmount'           => $group['totalAmount'],
                        'adjustedTotalAmount'   => $this->{$currentAdjustedTotalAmount},
                    ]);
                }

            } catch (\Exception $e) {
                Log::error($e->getMessage(), [
                    'class'     => __CLASS__,
                    'method'    => __METHOD__,
                ]);
            }
        }

        if ($justReturnCalculations) {
            return $calculation;
        }

        if ($unsetUnusedFields) {
            // Unset header values that have no sense when there are multiple tax types
            // By now lets keep this fields active because if the multiple tax types get rolled back
            // (setting the same tax types to all the lines) the header needs this data
        }

        // Save the broke down values
        try {
            $this->save();
        } catch (\PDOException $e) {
            Log::error($e->getMessage(), [
                'class'     => __CLASS__,
                'method'    => __METHOD__,
            ]);
        }
    }

    /**
     * Resets the value for those fields related to break down totals and taxes
     */
    public function resetTotalsBreakDownFields() : void
    {
        $key = 2;
        while ($key <= 5) {
            $currentTotalNet                = 'total_net_'              . strval($key);
            $currentTaxRate                 = 'tax_rate_'               . strval($key);
            $currentTaxTypeBalanceRange     = 'tax_balance_range_'      . strval($key);
            $currentTaxType                 = 'tax_type_'               . strval($key);
            $currentAdjustedTaxAmount       = 'adjusted_tax_amount_'    . strval($key);
            $currentTaxAmount               = 'tax_amount_'             . strval($key);
            $currentTotalAmount             = 'total_amount_'           . strval($key);
            $currentAdjustedTotalAmount     = 'adjusted_total_amount_'  . strval($key);
            $this->{$currentTotalNet}               = 0;
            $this->{$currentTaxAmount}              = 0;
            $this->{$currentTaxRate}                = 0;
            $this->{$currentTotalAmount}            = 0;
            // $this->{$currentTaxTypeBalanceRange}    = 0; // This one can not be reset, since the value comes from an input
            $this->{$currentTaxType}                = 0;
            $this->{$currentAdjustedTaxAmount}      = 0;
            $this->{$currentAdjustedTotalAmount}    = 0;
            $key++;
        }
    }

    /**
     * Converts header items to lines
     */
    public function convertSpecialHeaderItemsIntoLines() : void
    {
        // Lets keep this commented until the second part (header items converted to lines) is tackled
        /*
        // Apply header discount conversion
        $this->convertHeaderDiscountToLine();

        // Apply header IRFP conversion
        $this->convertHeaderIrpfToLine();*/
    }

    /**
     * Fills up a new invoice line object that comes from an header item conversion
     * @param $type
     * @param $invoice
     * @param $value
     * @return InvoiceLine
     */
    private function fillUpConversionObject($invoice, $type, $value)
    {
        // By now lets force the line to a tax type S0
        $taxTypeToAssign    = TaxType::whereType('S0')->first();
        $newLine            = new InvoiceLine(Helpers::fillFieldsWithDefaultValue('line'));

        switch ($type) {
            case Constants::HEADER_ITEM_TO_LINE_TYPE_IRPF:
                $description = 'IRPF [System - Item/Valor de cabecera agregado como línea]';
                break;
            case Constants::HEADER_ITEM_TO_LINE_TYPE_DISCOUNT:
                $description = 'Descuento [System - Item/Valor de cabecera agregado como línea]';
                break;
            default:
                $description = null;
        }

        $newLine->invoice_id                = $invoice->id;
        $newLine->net_amount                = -1 * abs($value);
        $newLine->unit_price                = -1 * abs($value);
        $newLine->item_order_line_bulk      = ($invoice->invoiceLines->count() + 1) * 10;
        $newLine->item_coming_from_header   = $type;
        $newLine->material_qty              = 1;
        $newLine->material_description      = $description;
        $newLine->taxes                     = $taxTypeToAssign->percentage;
        $newLine->taxes_id                  = $taxTypeToAssign->type;
        $newLine->tax_type_id               = $taxTypeToAssign->id;

        return $newLine;
    }

    /**
     * Converts header IRPF to line
     */
    private function convertHeaderIrpfToLine() : void
    {
        if (isset($this->taxes_irpf_amount)) {
            if ($this->taxes_irpf_amount != 0) {
                // Check if the discount has been already added as line
                $discountAsLine = $this->invoiceLines
                    ->where('item_coming_from_header', Constants::HEADER_ITEM_TO_LINE_TYPE_IRPF);

                // If there is no line representing the former header item, create one
                if ($discountAsLine->isEmpty()) {
                    // Build up a proper object with all defaults and from a common method
                    $newLine = $this->fillUpConversionObject($this, Constants::HEADER_ITEM_TO_LINE_TYPE_IRPF, $this->taxes_irpf_amount);

                    try {
                        $newLine->save();
                    } catch (\Exception $e) {
                        Log::error($e->getMessage(), [
                            'class'     => __CLASS__,
                            'method'    => __METHOD__,
                        ]);
                    }

                    $newLine->recalculateTotals();

                } else {
                    // Update discount value in case it has been changed
                    $discountAsLine             = $discountAsLine->first();
                    $discountAsLine->net_amount = -1 * abs($this->taxes_irpf_amount);
                    $discountAsLine->unit_price = -1 * abs($this->taxes_irpf_amount);

                    try {
                        $discountAsLine->save();
                    } catch (\Exception $e) {
                        Log::error($e->getMessage(), [
                            'class'     => __CLASS__,
                            'method'    => __METHOD__,
                        ]);
                    }

                    $discountAsLine->recalculateTotals();
                }
            } else {
                // Discount was set to zero, lets delete the line representing it
                $discountAsLine = $this->invoiceLines
                    ->where('item_coming_from_header', Constants::HEADER_ITEM_TO_LINE_TYPE_IRPF);

                if ($discountAsLine->isNotEmpty()) {
                    $discountAsLine->first()->delete();
                }
            }
        }
    }

    /**
     * Converts header discount to line
     */
    private function convertHeaderDiscountToLine() : void
    {
        if (isset($this->discount_amount)) {
            if ($this->discount_amount != 0) {
                // Check if the discount has been already added as line
                $discountAsLine = $this->invoiceLines
                    ->where('item_coming_from_header', Constants::HEADER_ITEM_TO_LINE_TYPE_DISCOUNT);

                // If there is no line representing the former header item, create one
                if ($discountAsLine->isEmpty()) {
                    // Build up a proper object with all defaults and from a common method
                    $newLine = $this->fillUpConversionObject($this, Constants::HEADER_ITEM_TO_LINE_TYPE_DISCOUNT, $this->discount_amount);

                    try {
                        $newLine->save();
                    } catch (\Exception $e) {
                        Log::error($e->getMessage(), [
                            'class'     => __CLASS__,
                            'method'    => __METHOD__,
                        ]);
                    }

                    $newLine->recalculateTotals();

                } else {
                    // Update discount value in case it has been changed
                    $discountAsLine             = $discountAsLine->first();
                    $discountAsLine->net_amount = -1 * abs($this->discount_amount);
                    $discountAsLine->unit_price = -1 * abs($this->discount_amount);

                    try {
                        $discountAsLine->save();
                    } catch (\Exception $e) {
                        Log::error($e->getMessage(), [
                            'class'     => __CLASS__,
                            'method'    => __METHOD__,
                        ]);
                    }

                    $discountAsLine->recalculateTotals();
                }
            } else {
                // Discount was set to zero, lets delete the line representing it
                $discountAsLine = $this->invoiceLines
                    ->where('item_coming_from_header', Constants::HEADER_ITEM_TO_LINE_TYPE_DISCOUNT);

                if ($discountAsLine->isNotEmpty()) {
                    $discountAsLine->first()->delete();
                }
            }
        }
    }

    /**
     * Applies special rules defined for some providers
     */
    public function applySpecialRulesByProvider() : void
    {
        // Get rules from current provider
        $rules = ProviderRule::where('provider_vat_number', $this->vat_number)->first();

        if ($rules) {
            if ($rules->force_tax_type) {
                // Check if there's a tax type forcing specification for the current provider
                list(
                    $this->taxes_type, $this->taxes_id, $this->taxes_percent, $this->status
                    ) = Helpers::forceProviderTaxTypeByDefinition($this->toArray(), $this->sap_company, $this->vat_number);

                if ($this->status == 4) {
                    $this->audit()->create([
                        'user_id'   => 'system',
                        'status'    => $this->status,
                        'text'      => '[automatic comment - Blocked due forced Tax Type]'
                    ]);
                }
            }

            if ($rules->block_due_unit_measure) {
                $this->status = 4;
                $this->audit()->create([
                    'user_id'   => 'system',
                    'status'    => $this->status,
                    'text'      => '[Factura bloqueada - Unidades de medida]'
                ]);
            }
        }
    }

    /**
     * Checks if invoice date is valid for a given type
     *
     * @param $checkType
     * @return bool
     */
    public function isInvoiceDateValid($checkType)
    {
        switch ($checkType) {
            case 'greaterThanToday':
                $invoiceDate    = Carbon::parse($this->date)->startOfDay();
                $todayDate      = Carbon::now()->startOfDay();
                $isValid        = true;

                // If invoice date is greater than today
                if ($invoiceDate->gt($todayDate)) {
                    $isValid = false;
                }

                break;
            default:
                $isValid = true;
        }

        return $isValid;
    }

    /**
     * Gets the total net amount of those lines with a tax type with none zero percentage
     * depending on the existence of IRPF amount
     *
     * @return float
     */
    private function getTotalNetForNonZeroTaxTypeLines()
    {
        // Lazy load relationship
        $this->load('invoiceLinesLite.tax');

        // Defaults
        $totalNetForNonZeroTaxType = floatval(0);

        // Collect those total_net where the tax type percentage is not zero %
        $this->invoiceLinesLite->each(function ($line, $key) use (&$totalNetForNonZeroTaxType) {
            if ($line->tax && $line->tax->percentage != 0) {
                $totalNetForNonZeroTaxType += $line->net_amount;
            }
        });

        return ($totalNetForNonZeroTaxType);
    }

    /**
     * Gets the base amount for IRPF calculation depending on the existence of IRPF amount
     *
     * @return float
     */
    private function giveMeTheRightBaseValueForIrpfCalculation()
    {
        // Defaults
        $totalNetForNonZeroTaxType = floatval(0);

        if ($this->taxes_irpf_amount != 0) {
            if ($this->total_net_for_non_zero_tax_percentage != 0) {
                $totalNetForNonZeroTaxType = $this->total_net_for_non_zero_tax_percentage;
            } else {
                // Collect those total_net where the tax type percentage is not zero %
                $totalNetForNonZeroTaxType = $this->getTotalNetForNonZeroTaxTypeLines();
            }
        }

        return $totalNetForNonZeroTaxType;
    }

    /**
     * Checks if there is more than one tax type applied to lines and return a label indicating
     * which tax types are used through the lines
     *
     * @return boolean | string
     */
    public function giveMeConcatTaxTypes()
    {
        // Defaults
        $taxTypeConcat = false;
        // Lazy load the relationship to find out if there is more than one tax type applied to invoice lines
        $this->load('invoiceLines');

        if ($this->invoiceLines->pluck('tax_type_id')->unique()->count() > 0) {
            // Group the lines by tax type
            $groupedLines = $this->invoiceLines->groupBy('tax_type_id');

            // Go get the tax types
            $taxTypes = TaxType::whereIn('id', $groupedLines->keys())->pluck('type');

            // Create the concat label
            if ($taxTypes) {
                $taxTypes       = $taxTypes->toArray();
                $taxTypeConcat  = implode(' - ', $taxTypes);
            }
        }

        return $taxTypeConcat;
    }

    /*
     * SCOPES
     */
    public function scopeTableSearch($query, $tableType, $search, $negativeWord)
    {
        if (!empty($search['unreadMessages'])) {
            // Lazy load relationship
            $this->load('chats');

            $query->whereHas('chats', function (Builder $subQuery) {
                $subQuery
                    ->where('is_new', '=', true);
            });
        }

        // Default
        $searchedStatus = false;

        // Lets apply the searched status logic here
        if (!empty($search['status'])) {
            $searchedStatus = intval($search['status']);
        }

        switch ($tableType) {
            case 'processed':
                // Apply searched status
                if ($searchedStatus && $searchedStatus != 1) {
                    $query->where('status', $searchedStatus);
                } else {
                    $query->where('status', '>=', 1);
                }

                break;
            case 'pending':
                if (auth()->user()->hasRole('administration-manager')) {

                    // Apply searched status
                    if ($searchedStatus && $searchedStatus != 9) {
                        $query->where('status', $searchedStatus);
                    } else {
                        $query->where('status', 9);
                    }

                } else {
                    // Case default statuses
                    $statuses = [1, 4, 12];

                    // Apply searched status
                    if ($searchedStatus) {
                        $statuses = [];
                        $statuses = Arr::prepend($statuses, $searchedStatus);
                        $statuses = array_unique($statuses);
                    }

                    $query->whereIn('status', $statuses);
                }

                break;
            default:
                (!$searchedStatus)
                    ?: $query->where('status', $searchedStatus);

                break;
        }

        if ($search['sapCompany'] != '') {
            $query->Where('sap_company', 'like', '%' . $search['sapCompany'] . '%');
        }
        if ($search['providerName'] != '') {
            $query->Where('provider_name', 'like', '%' . $search['providerName'] . '%');
        }
        if ($search['vatNumber'] != '') {
            $query->Where('vat_number', 'like', '%' . $search['vatNumber'] . '%');
        }
        if ($search['invoiceNumber'] != '') {
            $query->Where('invoice_number', 'like', '%' . $search['invoiceNumber'] . '%');
        }
        if ($search['fromDate'] != '' || $search['toDate'] != '') {
            if ($search['fromDate'] != '' && $search['toDate'] != '') {
                $query->wherebetween('date', [$search['fromDate'], $search['toDate']]);
            } else if ($search['fromDate'] != '') {
                $query->Where('date', '>=', $search['fromDate']);
            } else {
                $query->Where('date', '<=', $search['toDate']);
            }
        }
        if ($search['totalNetMin'] != '' || $search['totalNetMax'] != '') {
            if ($search['totalNetMin'] != '' && $search['totalNetMax'] != '') {
                $query->WhereBetween('total_net', [$search['totalNetMin'], $search['totalNetMax']]);
            } else if ($search['totalNetMin'] != '') {
                $query->Where('total_net', '>=', $search['totalNetMin']);
            } else {
                $query->Where('total_net', '<=', $search['totalNetMax']);
            }
        }
        if ($search['taxesTotalAmountMin'] != '' || $search['taxesTotalAmountMax'] != '') {
            if ($search['taxesTotalAmountMin'] != '' && $search['taxesTotalAmountMax'] != '') {
                $query->WhereBetween('taxes_total_amount', [$search['taxesTotalAmountMin'], $search['taxesTotalAmountMax']]);
            } else if ($search['taxesTotalAmountMin'] != '') {
                $query->Where('taxes_total_amount', '>=', $search['taxesTotalAmountMin']);
            } else {
                $query->Where('taxes_total_amount', '<=', $search['taxesTotalAmountMax']);
            }
        }
        if ($search['taxType'] != '') {
            $query->whereHas('invoiceLines', function (Builder $subQuery) use ($search) {
                $subQuery->where('tax_type_id', $search['taxType']);
            });
        }
        if ($search['totalMin'] != '' || $search['totalMax'] != '') {
            if ($search['totalMin'] != '' && $search['totalMax'] != '') {
                $query->WhereBetween('total', [$search['totalMin'], $search['totalMax']]);
            } else if ($search['totalMin'] != '') {
                $query->Where('total', '>=', $search['totalMin']);
            } else {
                $query->Where('total', '<=', $search['totalMax']);
            }
        }
        if ($search['fromDatePayment'] != '' || $search['toDatePayment'] != '') {
            if ($search['fromDatePayment'] != '' && $search['toDatePayment'] != '') {
                $query->wherebetween('payment_date', [$search['fromDatePayment'], $search['toDatePayment']]);
            } else if ($search['fromDatePayment'] != '') {
                $query->Where('payment_date', '>=', $search['fromDatePayment']);
            } else {
                $query->Where('payment_date', '<=', $search['toDatePayment']);
            }
        }

        if ($search['fromContabDate'] != '' || $search['toContabDate'] != '') {
            if ($search['fromContabDate'] != '' && $search['toContabDate'] != '') {
                $query->wherebetween('contabilized_at', [$search['fromContabDate'], $search['toContabDate']]);
            } else if ($search['fromContabDate'] != '') {
                $query->Where('contabilized_at', '>=', $search['fromContabDate']);
            } else {
                $query->Where('contabilized_at', '<=', $search['toContabDate']);
            }
        }

        if ($search['sapDocumentNumber'] != '') {
            $query->Where('sap_document_number', 'like', '%' . $search['sapDocumentNumber'] . '%');
        }
        if ($search['internOrder'] != '') {
            //This works in case the user wants to search opposite results.
            $containsWord = stripos($search['internOrder'], $negativeWord);
            if ($containsWord === false) {
                $query->Where('intern_order', 'like', '%' . trim($search['internOrder']) . '%');
            } else {
                $internOrderTosearch = trim(str_ireplace($negativeWord, '', $search['internOrder']));
                if ($internOrderTosearch == '') {
                    $query->Where('intern_order', '=', null);
                } else {
                    $query->Where('intern_order', 'not like', '%' . $internOrderTosearch . '%');
                }
            }
        }
        if ($search['status'] != '') {
            // Lets move the filtering logic to where the status is applied for searching [may 2021]
//            $query->where('status', intval($search['status']));
        }
        if ($search['currency'] != '') {
            $query->where('currency', 'like', '%' . $search['currency'] . '%');
        }
        if ($search['isCorrect'] != '') {
            $query->where('is_correct', '=', $search['isCorrect']);
        }

        if ($search['documentType'] != '') {
            $query->where('document_type', '=', $search['documentType']);
        }

        /*Log::info('filter', [
            'query'     => $query->toSql(),
            'bindings'  => $query->getBindings()
        ]);*/
    }

    public function scopeEmailSearch($query, $search)
    {
        if ($search['providerName']) {
            $query->where('provider_name', 'like', '%' . $search['providerName'] . '%');
        }
        $query->whereHas('emailAttachment.email', function (Builder $query) use ($search) {
            if ($search['email'] != '') {
                $query->where('from', 'like', '%' . $search['email'] . '%');
            }
            if ($search['subject'] != '') {
                $query->where('subject', 'like', '%' . $search['subject'] . '%');
            }
        })
        ->whereHas('emailAttachment', function (Builder $query) use ($search) {
            if ($search['fromDownloadDate'] != '' || $search['toDownloadDate'] != '') {
                if ($search['fromDownloadDate'] != '' && $search['toDownloadDate'] != '') {
                    $query->wherebetween('created_at', [$search['fromDownloadDate'], $search['toDownloadDate']]);
                } else if ($search['fromDownloadDate'] != '') {
                    $query->where('created_at', '>=', $search['fromDownloadDate']);
                } else {
                    $query->where('created_at', '<=', $search['toDownloadDate']);
                }
            }

            if ($search['fromProcessedDate'] != '' || $search['toProcessedDate'] != '') {
                if ($search['fromProcessedDate'] != '' && $search['toProcessedDate'] != '') {
                    $query->wherebetween('created_at', [$search['fromProcessedDate'], $search['toProcessedDate']]);
                } else if ($search['fromProcessedDate'] != '') {
                    $query->where('updated_at', '>=', $search['fromProcessedDate']);
                } else {
                    $query->where('updated_at', '<=', $search['toProcessedDate']);
                }
            }
        })
        ->when($search['status'] != '', function ($query) use ($search) {
            $query->where('status', intval($search['status']));
        })
        ->join('email_attachments', 'email_attachments.id', '=', 'invoices.email_attachment_id')
        ->join('emails', 'emails.id', '=', 'email_attachments.email_id');
    }

    /*
     * MUTATORS
     *
     */

    /**
     * Get the invoice total amount.
     *
     * @param  string  $value
     * @return string
     */
    public function getTotalAttribute($value)
    {
        return round($value, 2);
    }

    /**
     * Set the invoice total amount.
     *
     * @param  string  $value
     * @return void
     */
    public function setTotalAttribute($value)
    {
        $this->attributes['total'] = round($value, 2);
    }

    /**
     * Get the invoice total net amount.
     *
     * @param  string  $value
     * @return string
     */
    public function getTotalNetAttribute($value)
    {
        return round($value, 2);
    }

    /**
     * Set the invoice total net amount.
     *
     * @param  string  $value
     * @return void
     */
    public function setTotalNetAttribute($value)
    {
        $this->attributes['total_net'] = round($value, 2);
    }

    /**
     * Get the invoice discount amount.
     *
     * @param  string  $value
     * @return string
     */
    public function getDiscountAmountAttribute($value)
    {
        return round($value, 2);
    }

    /**
     * Set the invoice discount amount.
     *
     * @param  string  $value
     * @return void
     */
    public function setDiscountAmountAttribute($value)
    {
        $this->attributes['discount_amount'] = round($value, 2);
    }

    /**
     * Get the invoice total net for non zero tax percentage amount.
     *
     * @param  string  $value
     * @return string
     */
    public function getTotalNetForNonZeroTaxPercentageAttribute($value)
    {
        return round($value, 2);
    }

    /**
     * Set the invoice total net for non zero tax percentage amount.
     *
     * @param  string  $value
     * @return void
     */
    public function setTotalNetForNonZeroTaxPercentageAttribute($value)
    {
        $this->attributes['total_net_for_non_zero_tax_percentage'] = round($value, 2);
    }

    /**
     * Get the invoice total net for non zero tax percentage amount.
     *
     * @param  string  $value
     * @return string
     */
    public function getTotalNetForNonZeroTaxPercentageCalculatedAttribute($value)
    {
        return round($value, 2);
    }

    /**
     * Set the invoice total net for non zero tax percentage amount.
     *
     * @param  string  $value
     * @return void
     */
    public function setTotalNetForNonZeroTaxPercentageCalculatedAttribute($value)
    {
        $this->attributes['total_net_for_non_zero_tax_percentage_calculated'] = round($value, 2);
    }

    /**
     * Get the invoice taxes total amount without adjustment.
     *
     * @param  string  $value
     * @return string
     */
    public function getTaxesTotalAmountWithoutAdjustmentAttribute($value)
    {
        return round($value, 2);
    }

    /**
     * Set the invoice taxes total amount without adjustment.
     *
     * @param  string  $value
     * @return void
     */
    public function setTaxesTotalAmountWithoutAdjustmentAttribute($value)
    {
        $this->attributes['taxes_total_amount_without_adjustment'] = round($value, 2);
    }

    /**
     * Get the invoice tax amount.
     *
     * @param  string  $value
     * @return string
     */
    public function getTaxesTotalAmountAttribute($value)
    {
        return round($value, 2);
    }

    /**
     * Set the invoice tax amount.
     *
     * @param  string  $value
     * @return void
     */
    public function setTaxesTotalAmountAttribute($value)
    {
        $this->attributes['taxes_total_amount'] = round($value, 2);
    }

    /**
     * Get the invoice IRPF tax amount.
     *
     * @param  string  $value
     * @return string
     */
    public function getTaxesIrpfAmountAttribute($value)
    {
        return round($value, 2);
    }

    /**
     * Set the invoice IRPF tax amount.
     *
     * @param  string  $value
     * @return void
     */
    public function setTaxesIrpfAmountAttribute($value)
    {
        $this->attributes['taxes_irpf_amount'] = round($value, 2);
    }

    /**
     * Get the invoice other tax amount.
     *
     * @param  string  $value
     * @return string
     */
    public function getTaxesOtherAmountAttribute($value)
    {
        return round($value, 2);
    }

    /**
     * Set the invoice other tax amount.
     *
     * @param  string  $value
     * @return void
     */
    public function setTaxesOtherAmountAttribute($value)
    {
        $this->attributes['taxes_other_amount'] = round($value, 2);
    }

    /**
     * Important note for fields following the _x naming convention
     *
     * To make the mutators work well, it is necessary the use of two methods to accomplish that (getTaxAmount_xAttribute & getTaxAmountXAttribute).
     * Since the field has a _x as part of the name and it makes some noise between
     * camelCase and snake_case during the transformation and match between the field and the mutator
     * source: https://stackoverflow.com/questions/53444989/model-accessors-not-working-when-response-is-json-and-field-contains-a-number
     */

    /**
     * Get the invoice tax amount 2.
     *
     * @param  string  $value
     * @return string
     */
    public function getTaxAmount_2Attribute($value)
    {
        return $this->getTaxAmount2Attribute($value);
    }

    /**
     * Get the invoice tax amount 2.
     *
     * @param  string  $value
     * @return string
     */
    public function getTaxAmount2Attribute($value)
    {
        return round($value, 2);
    }

    /**
     * Set the invoice tax amount 2.
     *
     * @param  string  $value
     * @return void
     */
    public function setTaxAmount2Attribute($value)
    {
        $this->attributes['tax_amount_2'] = round($value, 2);
    }

    /**
     * Get the invoice tax amount 3.
     *
     * @param  string  $value
     * @return string
     */
    public function getTaxAmount_3Attribute($value)
    {
        return $this->getTaxAmount3Attribute($value);
    }

    /**
     * Get the invoice tax amount 3.
     *
     * @param  string  $value
     * @return string
     */
    public function getTaxAmount3Attribute($value)
    {
        return round($value, 2);
    }

    /**
     * Set the invoice tax amount 3.
     *
     * @param  string  $value
     * @return void
     */
    public function setTaxAmount3Attribute($value)
    {
        $this->attributes['tax_amount_3'] = round($value, 2);
    }

    /**
     * Get the invoice tax amount 4.
     *
     * @param  string  $value
     * @return string
     */
    public function getTaxAmount_4Attribute($value)
    {
        return $this->getTaxAmount4Attribute($value);
    }

    /**
     * Get the invoice tax amount 3.
     *
     * @param  string  $value
     * @return string
     */
    public function getTaxAmount4Attribute($value)
    {
        return round($value, 2);
    }

    /**
     * Set the invoice tax amount 4.
     *
     * @param  string  $value
     * @return void
     */
    public function setTaxAmount4Attribute($value)
    {
        $this->attributes['tax_amount_4'] = round($value, 2);
    }

    /**
     * Get the invoice tax amount 5.
     *
     * @param  string  $value
     * @return string
     */
    public function getTaxAmount_5Attribute($value)
    {
        return $this->getTaxAmount5Attribute($value);
    }

    /**
     * Get the invoice tax amount 5.
     *
     * @param  string  $value
     * @return string
     */
    public function getTaxAmount5Attribute($value)
    {
        return round($value, 2);
    }

    /**
     * Set the invoice tax amount 5.
     *
     * @param  string  $value
     * @return void
     */
    public function setTaxAmount5Attribute($value)
    {
        $this->attributes['tax_amount_5'] = round($value, 2);
    }

    /**
     * Get the invoice total net 2.
     *
     * @param  string  $value
     * @return string
     */
    public function getTotalNet_2Attribute($value)
    {
        return $this->getTotalNet2Attribute($value);
    }

    /**
     * Get the total net 2.
     *
     * @param  string  $value
     * @return string
     */
    public function getTotalNet2Attribute($value)
    {
        return round($value, 2);
    }

    /**
     * Set the invoice total net 2.
     *
     * @param  string  $value
     * @return void
     */
    public function setTotalNet2Attribute($value)
    {
        $this->attributes['total_net_2'] = round($value, 2);
    }

    /**
     * Get the invoice total net 3.
     *
     * @param  string  $value
     * @return string
     */
    public function getTotalNet_3Attribute($value)
    {
        return $this->getTotalNet3Attribute($value);
    }

    /**
     * Get the total net 3.
     *
     * @param  string  $value
     * @return string
     */
    public function getTotalNet3Attribute($value)
    {
        return round($value, 2);
    }

    /**
     * Set the invoice total net 3.
     *
     * @param  string  $value
     * @return void
     */
    public function setTotalNet3Attribute($value)
    {
        $this->attributes['total_net_3'] = round($value, 2);
    }

    /**
     * Get the invoice total net 4.
     *
     * @param  string  $value
     * @return string
     */
    public function getTotalNet_4Attribute($value)
    {
        return $this->getTotalNet4Attribute($value);
    }

    /**
     * Get the total net 4.
     *
     * @param  string  $value
     * @return string
     */
    public function getTotalNet4Attribute($value)
    {
        return round($value, 2);
    }

    /**
     * Set the invoice total net 4.
     *
     * @param  string  $value
     * @return void
     */
    public function setTotalNet4Attribute($value)
    {
        $this->attributes['total_net_4'] = round($value, 2);
    }

    /**
     * Get the invoice total net 5.
     *
     * @param  string  $value
     * @return string
     */
    public function getTotalNet_5Attribute($value)
    {
        return $this->getTotalNet5Attribute($value);
    }

    /**
     * Get the total net 5.
     *
     * @param  string  $value
     * @return string
     */
    public function getTotalNet5Attribute($value)
    {
        return round($value, 2);
    }

    /**
     * Set the invoice total net 5.
     *
     * @param  string  $value
     * @return void
     */
    public function setTotalNet5Attribute($value)
    {
        $this->attributes['total_net_5'] = round($value, 2);
    }

    /**
     * Get the invoice adjusted tax amount 2.
     *
     * @param  string  $value
     * @return string
     */
    public function getAdjustedTaxAmount_2Attribute($value)
    {
        return $this->getAdjustedTaxAmount2Attribute($value);
    }

    /**
     * Get the adjusted tax amount 2.
     *
     * @param  string  $value
     * @return string
     */
    public function getAdjustedTaxAmount2Attribute($value)
    {
        return round($value, 2);
    }

    /**
     * Set the invoice adjusted tax amount 2.
     *
     * @param  string  $value
     * @return void
     */
    public function setAdjustedTaxAmount2Attribute($value)
    {
        $this->attributes['adjusted_tax_amount_2'] = round($value, 2);
    }

    /**
     * Get the invoice adjusted tax amount 3.
     *
     * @param  string  $value
     * @return string
     */
    public function getAdjustedTaxAmount_3Attribute($value)
    {
        return $this->getAdjustedTaxAmount3Attribute($value);
    }

    /**
     * Get the adjusted tax amount 3.
     *
     * @param  string  $value
     * @return string
     */
    public function getAdjustedTaxAmount3Attribute($value)
    {
        return round($value, 2);
    }

    /**
     * Set the invoice adjusted tax amount 3.
     *
     * @param  string  $value
     * @return void
     */
    public function setAdjustedTaxAmount3Attribute($value)
    {
        $this->attributes['adjusted_tax_amount_3'] = round($value, 2);
    }

    /**
     * Get the invoice adjusted tax amount 4.
     *
     * @param  string  $value
     * @return string
     */
    public function getAdjustedTaxAmount_4Attribute($value)
    {
        return $this->getAdjustedTaxAmount4Attribute($value);
    }

    /**
     * Get the adjusted tax amount 4.
     *
     * @param  string  $value
     * @return string
     */
    public function getAdjustedTaxAmount4Attribute($value)
    {
        return round($value, 2);
    }

    /**
     * Set the invoice adjusted tax amount 4.
     *
     * @param  string  $value
     * @return void
     */
    public function setAdjustedTaxAmount4Attribute($value)
    {
        $this->attributes['adjusted_tax_amount_4'] = round($value, 2);
    }

    /**
     * Get the invoice adjusted tax amount 5.
     *
     * @param  string  $value
     * @return string
     */
    public function getAdjustedTaxAmount_5Attribute($value)
    {
        return $this->getAdjustedTaxAmount5Attribute($value);
    }

    /**
     * Get the adjusted tax amount 5.
     *
     * @param  string  $value
     * @return string
     */
    public function getAdjustedTaxAmount5Attribute($value)
    {
        return round($value, 2);
    }

    /**
     * Set the invoice adjusted tax amount 5.
     *
     * @param  string  $value
     * @return void
     */
    public function setAdjustedTaxAmount5Attribute($value)
    {
        $this->attributes['adjusted_tax_amount_5'] = round($value, 2);
    }

    /**
     * Get the invoice total amount 2.
     *
     * @param  string  $value
     * @return string
     */
    public function getTotalAmount_2Attribute($value)
    {
        return $this->getTotalAmount2Attribute($value);
    }

    /**
     * Get the total amount 2.
     *
     * @param  string  $value
     * @return string
     */
    public function getTotalAmount2Attribute($value)
    {
        return round($value, 2);
    }

    /**
     * Set the invoice total amount 2.
     *
     * @param  string  $value
     * @return void
     */
    public function setTotalAmount2Attribute($value)
    {
        $this->attributes['total_amount_2'] = round($value, 2);
    }

    /**
     * Get the invoice total amount 3.
     *
     * @param  string  $value
     * @return string
     */
    public function getTotalAmount_3Attribute($value)
    {
        return $this->getTotalAmount3Attribute($value);
    }

    /**
     * Get the total amount 3.
     *
     * @param  string  $value
     * @return string
     */
    public function getTotalAmount3Attribute($value)
    {
        return round($value, 2);
    }

    /**
     * Set the invoice total amount 3.
     *
     * @param  string  $value
     * @return void
     */
    public function setTotalAmount3Attribute($value)
    {
        $this->attributes['total_amount_3'] = round($value, 2);
    }

    /**
     * Get the invoice total amount 4.
     *
     * @param  string  $value
     * @return string
     */
    public function getTotalAmount_4Attribute($value)
    {
        return $this->getTotalAmount4Attribute($value);
    }

    /**
     * Get the total amount 4.
     *
     * @param  string  $value
     * @return string
     */
    public function getTotalAmount4Attribute($value)
    {
        return round($value, 2);
    }

    /**
     * Set the invoice total amount 4.
     *
     * @param  string  $value
     * @return void
     */
    public function setTotalAmount4Attribute($value)
    {
        $this->attributes['total_amount_4'] = round($value, 2);
    }

    /**
     * Get the invoice total amount 5.
     *
     * @param  string  $value
     * @return string
     */
    public function getTotalAmount_5Attribute($value)
    {
        return $this->getTotalAmount5Attribute($value);
    }

    /**
     * Get the total amount 5.
     *
     * @param  string  $value
     * @return string
     */
    public function getTotalAmount5Attribute($value)
    {
        return round($value, 2);
    }

    /**
     * Set the invoice adjusted total amount 2.
     *
     * @param  string  $value
     * @return void
     */
    public function setTotalAmount5Attribute($value)
    {
        $this->attributes['total_amount_5'] = round($value, 2);
    }

    /**
     * Get the invoice adjusted total amount 2.
     *
     * @param  string  $value
     * @return string
     */
    public function getAdjustedTotalAmount_2Attribute($value)
    {
        return $this->getAdjustedTotalAmount2Attribute($value);
    }

    /**
     * Get the invoice adjusted total amount 2.
     *
     * @param  string  $value
     * @return string
     */
    public function getAdjustedTotalAmount2Attribute($value)
    {
        return round($value, 2);
    }

    /**
     * Set the invoice adjusted total amount 2.
     *
     * @param  string  $value
     * @return void
     */
    public function setAdjustedTotalAmount2Attribute($value)
    {
        $this->attributes['adjusted_total_amount_2'] = round($value, 2);
    }

    /**
     * Get the invoice adjusted total amount 3.
     *
     * @param  string  $value
     * @return string
     */
    public function getAdjustedTotalAmount_3Attribute($value)
    {
        return $this->getAdjustedTotalAmount3Attribute($value);
    }

    /**
     * Get the invoice adjusted total amount 3.
     *
     * @param  string  $value
     * @return string
     */
    public function getAdjustedTotalAmount3Attribute($value)
    {
        return round($value, 2);
    }

    /**
     * Set the invoice adjusted total amount 3.
     *
     * @param  string  $value
     * @return void
     */
    public function setAdjustedTotalAmount3Attribute($value)
    {
        $this->attributes['adjusted_total_amount_3'] = round($value, 2);
    }

    /**
     * Get the invoice adjusted total amount 4.
     *
     * @param  string  $value
     * @return string
     */
    public function getAdjustedTotalAmount_4Attribute($value)
    {
        return $this->getAdjustedTotalAmount4Attribute($value);
    }

    /**
     * Get the invoice adjusted total amount 4.
     *
     * @param  string  $value
     * @return string
     */
    public function getAdjustedTotalAmount4Attribute($value)
    {
        return round($value, 2);
    }

    /**
     * Set the invoice adjusted total amount 4.
     *
     * @param  string  $value
     * @return void
     */
    public function setAdjustedTotalAmount4Attribute($value)
    {
        $this->attributes['adjusted_total_amount_4'] = round($value, 2);
    }

    /**
     * Get the invoice adjusted total amount 5.
     *
     * @param  string  $value
     * @return string
     */
    public function getAdjustedTotalAmount_5Attribute($value)
    {
        return $this->getAdjustedTotalAmount5Attribute($value);
    }

    /**
     * Get the invoice adjusted total amount 5.
     *
     * @param  string  $value
     * @return string
     */
    public function getAdjustedTotalAmount5Attribute($value)
    {
        return round($value, 2);
    }

    /**
     * Set the invoice adjusted total amount 5.
     *
     * @param  string  $value
     * @return void
     */
    public function setAdjustedTotalAmount5Attribute($value)
    {
        $this->attributes['adjusted_total_amount_5'] = round($value, 2);
    }

    /**
     * Get the invoice payment date and set it as null if value is empty
     * @param $value
     * @return |null
     */
    public function getPaymentDateAttribute($value)
    {
        if (!$value) {
            $value = null;
        }
        return $value;
    }

    /**
     * Get a new custom attribute on reading
     * @param $value
     * @return mixed
     */
    public function getBaseIrpfSapAttribute($value)
    {
        return $this->total_net_for_non_zero_tax_percentage;
    }

    /**
     * Set the intern order attribute.
     *
     * @param  string  $value
     * @return void
     */
    public function setInternOrderAttribute($value)
    {
        $this->attributes['intern_order'] = substr($value, 0, 50);
    }

    /**
     * Get a new custom attribute for unread messages on reading
     *
     * @return mixed
     */
    public function getHasUnreadMessagesAttribute()
    {
        // Lazy load relationship
        $this->load('chats');

        if ($this->chats->isNotEmpty()) {
            return $this->chats->where('is_new', true)->count();

        }

        return false;
    }

    /**
     * Get a new custom attribute for unread messages on reading
     *
     * @param $value
     * @return mixed
     */
    public function getConcatTaxTypesAttribute()
    {
        return $this->giveMeConcatTaxTypes();
    }
}
