<?php

namespace App;

use App\Traits\AutoGenerateUuid;
use Illuminate\Database\Eloquent\Model;

class CountryTranslation extends Model
{
    //
    use AutoGenerateUuid;
    public $incrementing = false;
    protected $keyType = 'string';

    protected $fillable = [
        'name',
        'description'
    ];
    public function getDateFormat()
    {
        if(config('custom.database_type')=='sqlsrv')
        {
            return 'd.m.Y H:i:s';
        }
return parent::getDateFormat();
    }
}
