<?php

namespace App;

use App\Models\ChatThread;
use App\Traits\AutoGenerateUuid;
use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
    use AutoGenerateUuid;
    public $incrementing = false;
    protected $keyType = 'string';

    protected $guarded = [
        'id'
    ];

    public function getDateFormat()
    {
        if (config('custom.database_type') == 'sqlsrv') {
            return 'd.m.Y H:i:s';
        }
        return parent::getDateFormat();
    }

    public function commentable()
    {
        return $this->morphTo();
    }

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function thread()
    {
        return $this->belongsTo(ChatThread::class);
    }
}
