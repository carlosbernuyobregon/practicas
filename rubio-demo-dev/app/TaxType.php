<?php

namespace App;

use App\Traits\AutoGenerateUuid;
use Illuminate\Database\Eloquent\Model;

class TaxType extends Model
{
    use AutoGenerateUuid;
    public $incrementing = false;
    protected $keyType = 'string';
    protected $fillable = [
        'type',
        'description',
        'percentage',
        'default',
        'customer_id'
    ];


    public function setCustomerIdAttribute($value)
    {
        $this->attributes['customer_id'] = ($value == "") ? null : $value;
    }
}
