<?php

namespace App;

use App\Traits\AutoGenerateUuid;
use Illuminate\Database\Eloquent\Model;

class PepElement extends Model
{
    use AutoGenerateUuid;
    public $incrementing = false;
    protected $keyType = 'string';

    protected $fillable = [
        'customer_id',
        'project_id',
        'sap_number_id',
        'type',
        'pep_element_id',
        'name',
        'pep_object_sap',
        'top_node_id',
        'order',
        'status',
        'company_co',
        'manageging_cost_center',
        'planification_element',
        'imputation_element',
        'company_fi',
        'pep_object_type',
        'currency',
        'costing_schema',
        'interests_schema',
        'investment_profile',
        'pep_statistic_element',
        'afec_id',
        'afec_subnumber',
    ];

    public function pepElementLines()
    {
        return $this->hasMany('App\PepElementLine');
    }
}
