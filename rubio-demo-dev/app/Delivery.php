<?php

namespace App;

use App\Traits\AutoGenerateUuid;
use Illuminate\Database\Eloquent\Model;

class Delivery extends Model
{
    use AutoGenerateUuid;
    public $incrementing = false;
    protected $keyType = 'string';

    protected $fillable = [
        'order_line_id',
        'amount',
        'qty',
        'status',
        'external_id',
        'delivered_at',
        'sent_to_sap_at',
        'position'

    ];

    public function orderLine()
    {
        return $this->belongsTo(OrderLine::class);
    }

    public function audit()
    {
        return $this->morphMany(Audit::class, 'auditable')->orderBy('created_at');
    }
}
