<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Facades\Mail;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        JsonResource::withoutWrapping();
        if ($this->app->environment(['local','test'])) {
            Mail::alwaysTo(['lpiqueras@strategying.com','dplanas@strategying.com','fcampos@strategying.com','xcasado@strategying.com']);
        }
    }
}
