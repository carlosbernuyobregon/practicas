<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Permission extends Model
{
    //
    public function getDateFormat()
    {
        if(config('custom.database_type')=='sqlsrv') {
            return 'd.m.Y H:i:s';
        }
        return parent::getDateFormat();
    }
}
