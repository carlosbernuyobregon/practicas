<?php

namespace App\Helpers;

use App\Budget;
use App\Country;
use App\Library\Constants;
use App\Mail\GlobalSyncNotification;
use App\Models\Provider;
use App\Models\ProvidersPerSapCompanyForcedTaxType;
use App\Setting;
use App\TaxType;
use App\Models\Versioning;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;
use Symfony\Component\HttpKernel\EventListener\FragmentListener;

/**
 * Class Helpers
 * @package App\Helpers
 */
class Helpers
{
    /**
     * Selects the right tax type for those providers belonging to a special group.
     *
     * @param $data
     * @param $sapCompany
     * @param $vatNumber
     * @param $isLine
     * @return array
     */
    public static function forceProviderTaxTypeByDefinition($data, $sapCompany, $vatNumber, $isLine = false)
    {
        ($isLine) ? $taxesValueField = $data['taxes'] : $taxesValueField = $data['taxes_percent'];

        $oneHourInSecs  = 216000;

        // Get all Forced Tax Type
        $forcedTaxTypes = Cache::remember('providers-per-sap-company-forced-tax-type', $oneHourInSecs, function () {
            return ProvidersPerSapCompanyForcedTaxType::all();
        });

        // Defaults values are the original values
        $return['taxes_type_id']    = $data['taxes_type']   ?? null;
        $return['taxes_id']         = $data['taxes_id']     ?? null;
        $return['status']           = $data['status']       ?? 0;
        $return['taxes_percent']    = $taxesValueField      ?? 0;

        // First lets check if the provider is listed as one of those who must have forced tax type
        $resultantTaxType = $forcedTaxTypes->where('sap_company', $sapCompany)
            ->where('provider_vat_number', $vatNumber);

        if ($resultantTaxType->isNotEmpty()) {

            // Get all the tax types and then use the collections, to avoid querying each time
            $taxTypeAll =  TaxType::all();

            if ($resultantTaxType->count() > 1) {
                // If there is more than one, lets decide which tax type should be applied taking into account
                // the percentage coming from data
                if (isset($taxesValueField)) {
                    switch ($taxesValueField) {
                        case 0:
                            $partialAcronym = '0';
                            break;
                        case 4:
                            $partialAcronym = '1';
                            break;
                        case 10:
                            $partialAcronym = '2';
                            break;
                        case 21:
                            $partialAcronym = '3';
                            break;
                        default:
                            $partialAcronym = false;
                            break;
                    }

                    if ($partialAcronym) {
                        // Compose the acronym (V3, N1, etc)
                        $acronym  = $resultantTaxType->first()->tax_type_acronym . $partialAcronym;

                        $return['taxes_type_id']    = $taxTypeAll->firstWhere('type', $acronym)->id ?? null;
                        $return['taxes_percent']    = $taxTypeAll->firstWhere('type', $acronym)->percentage ?? null;
                        $return['taxes_id']         = $acronym;

                    } else {
                        $return['taxes_type_id']    = $resultantTaxType->first()->tax_type_id_fallback;
                        $return['taxes_id']         = $resultantTaxType->first()->tax_type_fallback;
                        $return['taxes_percent']    = $taxTypeAll->firstWhere('id', $resultantTaxType->first()->tax_type_id_fallback)->percentage ?? null;
                    }
                } else {
                    $return['taxes_type_id']    = $resultantTaxType->first()->tax_type_id_fallback;
                    $return['taxes_id']         = $resultantTaxType->first()->tax_type_fallback;
                    $return['taxes_percent']    = $taxTypeAll->firstWhere('id', $resultantTaxType->first()->tax_type_id_fallback)->percentage ?? null;
                }
            } else {
                // If there is only one result, lets force the tax type with the fallback value
                $return['taxes_type_id']    = $resultantTaxType->first()->tax_type_id_fallback;
                $return['taxes_id']         = $resultantTaxType->first()->tax_type_fallback;
                $return['taxes_percent']    = $taxTypeAll->firstWhere('id', $resultantTaxType->first()->tax_type_id_fallback)->percentage ?? null;
            }

            $return['status'] = 4;
        }

        return [$return['taxes_type_id'], $return['taxes_id'], $return['taxes_percent'], $return['status']];
    }

    /**
     * Checks if a number is between two values
     * @param $varToCheck
     * @param $high
     * @param $low
     *
     * @return boolean
     */
    public static function numberBetween($varToCheck, $high, $low)
    {
        if ($varToCheck < $low) return false;
        if ($varToCheck > $high) return false;

        return true;
    }

    /**
     * Checks if given set of totals values from invoice are between the two cents gap added as an exception for its approval
     *
     * @param $calculatedValues
     * @param $calculatedValuesFromLines
     * @param $totalToCompare
     * @return bool
     */
    public static function applyTwoCentsGapRuleForTotals($calculatedValues, $calculatedValuesFromLines, $totalToCompare = false)
    {
        $definitiveTotalToCompare = $calculatedValuesFromLines->sum('totalAmount');

        if ($totalToCompare) {
            $definitiveTotalToCompare = $totalToCompare;
        }

        if (
            self::numberBetween(
                round($calculatedValues['totalNetAmount'], 2),
                round($calculatedValuesFromLines->sum('netAmount') + 0.02, 2),
                round($calculatedValuesFromLines->sum('netAmount') - 0.02, 2)
            ) &&
            self::numberBetween(
                round($calculatedValues['taxesTotalAmount'], 2),
                round($calculatedValuesFromLines->sum('taxAmount') + 0.02, 2),
                round($calculatedValuesFromLines->sum('taxAmount') - 0.02, 2)
            ) &&
            self::numberBetween(
                round($calculatedValues['totalAmount'], 2),
                round($definitiveTotalToCompare + 0.02, 2),
                round($definitiveTotalToCompare - 0.02, 2)
            )
        ) {
            return true;
        }

        return false;
    }

    /**
     * Converts a number to its roman presentation.
     * @param $num
     * @return string
     */
    public static function numberToRoman($num)
    {
        // Be sure to convert the given parameter into an integer
        $n      = intval($num);
        $result = '';

        // Declare a lookup array that we will use to traverse the number:
        $lookup = array(
            'M' => 1000, 'CM' => 900, 'D' => 500, 'CD' => 400,
            'C' => 100, 'XC' => 90, 'L' => 50, 'XL' => 40,
            'X' => 10, 'IX' => 9, 'V' => 5, 'IV' => 4, 'I' => 1
        );

        foreach ($lookup as $roman => $value)
        {
            // Look for number of matches
            $matches = intval($n / $value);

            // Concatenate characters
            $result .= str_repeat($roman, $matches);

            // Substract that from the number
            $n = $n % $value;
        }

        return $result;
    }

    /**
     * Fills the complete set of fields with default values for invoices and lines
     *
     * @param $type
     * @return array
     */
    public static function fillFieldsWithDefaultValue($type)
    {
        $defaultData = [];
        switch ($type) {
            case 'header':
                $defaultData = [
                    'alert_flag_from_ocr'                               => null,
                    'bank_code_number'                                  => null,
                    'bank_iban'                                         => null,
                    'currency'                                          => null,
                    'date'                                              => null,
                    'delivery_amount'                                   => 0,
                    'discount_amount'                                   => 0,
                    'document_type'                                     => null,
                    'document_type_label'                               => null,
                    'filename'                                          => null,
                    'intern_order'                                      => null,
                    'intern_vat_number'                                 => null,
                    'invoice_number'                                    => null,
                    'ocr_reference'                                     => null,
                    'operation_date'                                    => null,
                    'payment_conditions'                                => null,
                    'payment_date'                                      => null,
                    'provider_city'                                     => null,
                    'provider_country'                                  => null,
                    'provider_name'                                     => null,
                    'provider_postal_code'                              => null,
                    'provider_state'                                    => null,
                    'provider_street'                                   => null,
                    'sap_company'                                       => null,
                    'tax_amount_2'                                      => 0,
                    'tax_amount_3'                                      => 0,
                    'tax_amount_4'                                      => 0,
                    'tax_amount_5'                                      => 0,
                    'tax_rate_2'                                        => 0,
                    'tax_rate_3'                                        => 0,
                    'tax_rate_4'                                        => 0,
                    'tax_rate_5'                                        => 0,
                    'taxes_id'                                          => null,
                    'taxes_irpf_amount'                                 => 0,
                    'taxes_irpf_rate'                                   => 0,
                    'taxes_percent'                                     => 0,
                    'taxes_total_amount'                                => 0,
                    'taxes_type'                                        => null,
                    'total'                                             => 0,
                    'total_net'                                         => 0,
                    'total_net_2'                                       => 0,
                    'total_net_3'                                       => 0,
                    'total_net_4'                                       => 0,
                    'total_net_5'                                       => 0,
                    'vat_number'                                        => null,
                    'tax_balance_range'                                 => 0,
                    'total_net_for_non_zero_tax_percentage'             => 0,
                    'total_net_for_non_zero_tax_percentage_calculated'  => 0,
                    'tax_balance_range_2'                               => 0,
                    'tax_balance_range_3'                               => 0,
                    'tax_balance_range_4'                               => 0,
                    'tax_balance_range_5'                               => 0,
                    'adjusted_tax_amount_2'                             => 0,
                    'adjusted_tax_amount_3'                             => 0,
                    'adjusted_tax_amount_4'                             => 0,
                    'adjusted_tax_amount_5'                             => 0,
                    'tax_balance_total'                                 => 0,
                    'taxes_total_amount_without_adjustment'             => 0,
                ];
                break;
            case 'line':
                $defaultData = [
                    'discount_amount'	        => 0,
                    'discount_rate'	            => 0,
                    'invoice_id'	            => null,
                    'irpf'	                    => 0,
                    'item_order_line_bulk'	    => null,
                    'item_order_num_bulk'	    => null,
                    'material_code'	            => null,
                    'material_description'	    => null,
                    'material_qty'	            => 1,
                    'net_amount'	            => 0,
                    'qty_measuring_unit'	    => 0,
                    'tax_type_id'	            => null,
                    'taxes'	                    => null,
                    'taxes_amount'	            => 0,
                    'taxes_id'	                => null,
                    'unit_price'	            => 0,
                    'unit_price_measuring_unit'	=> 0,
                ];
                break;
        }

        return $defaultData;
    }

    /**
     * Checks if given Invoice has at least one line belonging to given Tax Type acronym (V, N, etc)
     *
     * @param $invoice
     * @param $taxTypeAcronym
     * @return bool
     */
    public static function atLeastOneLineWithTaxType($invoice, $taxTypeAcronym)
    {
        $taxTypeAcronym = strtoupper($taxTypeAcronym);
        // Group the lines by tax type
        $groupedLines = $invoice->invoiceLines->groupBy('tax_type_id');
        // Go get the tax types
        $taxTypes = TaxType::whereIn('id', $groupedLines->keys())->pluck('type');

        $atLeastOne = $taxTypes->filter(function ($value, $key) use ($taxTypeAcronym) {
            return starts_with($value, $taxTypeAcronym);
        });

        return $atLeastOne->count() > 0;
    }

    /**
     * Returns the tax id (alphanumeric) and the taxType id for a given percentage and country
     *
     * @param $taxPercent
     * @param $providerCountry
     * @return array
     */
    public static function giveMeTheTax($taxPercent, $providerCountry)
    {
        // Get all the tax types and then use the collections, to avoid querying each time
        $taxTypeAll =  TaxType::all();
        // Get the custom countries zones
        $allCountries = Country::all();
        // EU zone // ES must be pulled off this group
        $euZone             = $allCountries
            ->where('eu_zone', true)
            ->where('code', '!=', 'ES')
            ->pluck('code')
            ->toArray();

        // Rest of Europe
        $nonEuZone          = $allCountries
            ->where('non_eu_zone', true)
            ->pluck('code')
            ->toArray();

        // Rest of the world
        $restOfTheWorldZone = $allCountries
            ->where('rest_of_the_world_zone', true)
            ->pluck('code')
            ->toArray();

//        dd($euZone, $nonEuZone, $restOfTheWorldZone, $providerCountry);

        // Defaults
        $data['taxes_type_id']  = false;
        $data['taxes_id']       = false;
        $invoiceZone            = false;

        if ($providerCountry == 'ES') {
            // Spanish invoices
            $invoiceZone = 'ES';
            switch (intval($taxPercent)) {
                case 21:
                    $data['taxes_type_id']  = $taxTypeAll->firstWhere('type', 'S3')->id ?? null;
                    $data['taxes_id']       = 'S3';
                    break;
                case 10:
                    $data['taxes_type_id']  = $taxTypeAll->firstWhere('type', 'S2')->id ?? null;
                    $data['taxes_id']       = 'S2';
                    break;
                case 4:
                    $data['taxes_type_id']  = $taxTypeAll->firstWhere('type', 'S1')->id ?? null;
                    $data['taxes_id']       = 'S1';
                    break;
                case 0:
                    $data['taxes_type_id']  = $taxTypeAll->firstWhere('type', 'S0')->id ?? null;
                    $data['taxes_id']       = 'S0';
                    break;
                default:
                    $data['taxes_type_id']  = $taxTypeAll->firstWhere('type', 'S3')->id ?? null;
                    $data['taxes_id']       = 'S3';
                    break;
            }
        } elseif (in_array($providerCountry, $euZone)) {
            // UE
            $data['taxes_type_id']  = $taxTypeAll->firstWhere('type', 'A3')->id ?? null;
            $data['taxes_id']       = 'A3';
            $invoiceZone            = 'EU';
        } elseif (in_array($providerCountry, $restOfTheWorldZone) || in_array($providerCountry, $nonEuZone)) {
            //  Rest of Europe & Rest of the world
            $data['taxes_type_id']  = $taxTypeAll->firstWhere('type', 'Q3')->id ?? null;
            $data['taxes_id']       = 'Q3';
            $invoiceZone            = 'ROTW';
        }

        return [$data['taxes_type_id'], $data['taxes_id'], $invoiceZone];
    }

    /**
     * Returns the closest provider following names matching
     * @param $data
     * @return mixed
     */
    public static function giveMeTheProvider($data)
    {
        if (!empty($data['TaxRegistrationNumber'])) {
            // VAT number approach
            return Provider::where(function ($query) use ($data) {
                $query->where('vat_number', $data['TaxRegistrationNumber'])
                    ->orWhere('community_vat_number', $data['TaxRegistrationNumber']);
            })->where('name', 'not like', '@%')->first();
        } else {
            // Search by name approach
            $selectedProviders      = [];
            $possibleProviders      = Provider::where('name', 'like', '%' . trim($data['Name']) . '%')
                ->where('name', 'not like', '@%')
                ->get();
            $comparison             = new \Atomescrochus\StringSimilarities\Compare();
            $acceptancePercentage   = Setting::where('key_value', 'provider_name_comparison_acceptance_percentage')->first();

            if ($acceptancePercentage) {
                $acceptancePercentage = $acceptancePercentage->value;
            } else {
                $acceptancePercentage = 85;
            }

            foreach ($possibleProviders as $possibleProvider) {
                // Get comparisons
                $all = $comparison->all(strtolower($data['Name']), strtolower(trim($possibleProvider->name)));

                // If similarity is over 90%, then the provider might be the same
                if ($all['similar_text'] >= $acceptancePercentage) {
                    $selectedProviders[$possibleProvider->id] = $all['similar_text'];
                }
            }

            // Sort by values
            uasort($selectedProviders, function($a, $b) {
                return $b - $a;
            });

            return $possibleProviders->where('id', array_key_first($selectedProviders))->first();
        }
    }

    /**
     * Returns an array of Order status
     *
     * @return array
     */
    public static function buildOrdersStatusArray()
    {
        return [
            Constants::ORDER_STATUS_DRAFT                           => __('backend.orders.status.' . Constants::ORDER_STATUS_DRAFT),
            Constants::ORDER_STATUS_PENDING_APPROVAL_BY_MANAGER     => __('backend.orders.status.' . Constants::ORDER_STATUS_PENDING_APPROVAL_BY_MANAGER),
            Constants::ORDER_STATUS_APPROVED_BY_MANAGER             => __('backend.orders.status.' . Constants::ORDER_STATUS_APPROVED_BY_MANAGER),
            Constants::ORDER_STATUS_APPROVED_BY_MANAGEMENT          => __('backend.orders.status.' . Constants::ORDER_STATUS_APPROVED_BY_MANAGEMENT),
            Constants::ORDER_STATUS_APPROVED_BY_GENERAL_MANAGEMENT  => __('backend.orders.status.' . Constants::ORDER_STATUS_APPROVED_BY_GENERAL_MANAGEMENT),
            Constants::ORDER_STATUS_BLOCKED                         => __('backend.orders.status.' . Constants::ORDER_STATUS_BLOCKED),
            Constants::ORDER_STATUS_CANCELED                        => __('backend.orders.status.' . Constants::ORDER_STATUS_CANCELED),
            Constants::ORDER_STATUS_MODIFICATION_PENDING            => __('backend.orders.status.' . Constants::ORDER_STATUS_MODIFICATION_PENDING),
            Constants::ORDER_STATUS_APPROVED                        => __('backend.orders.status.' . Constants::ORDER_STATUS_APPROVED),
            Constants::ORDER_STATUS_UPDATED_IN_SAP                  => __('backend.orders.status.' . Constants::ORDER_STATUS_UPDATED_IN_SAP),
            Constants::ORDER_STATUS_UPDATED_IN_SAP_SUCCESS                  => __('backend.orders.status.' . Constants::ORDER_STATUS_UPDATED_IN_SAP_SUCCESS),
            Constants::ORDER_STATUS_UPDATED_IN_SAP_ERROR                  => __('backend.orders.status.' . Constants::ORDER_STATUS_UPDATED_IN_SAP_ERROR),
        ];
    }

    /**
     * Normalizes VAT and Community VAT
     * @param $vat
     * @param bool $countryCode
     * @return array
     */
    public static function VatFormat($vat, $countryCode = false)
    {
        // Defaults
        $newVat             = preg_replace("/[^A-Za-z0-9]/", '', $vat);
        $newCommunityVat    = false;

        if ($countryCode) {
            $country = Country::where('code', $countryCode)->first();

            // Normalize vat_number and community_vat_number for those EU zone countries
            if (!empty($country) && $country->eu_zone) {
                $newVat           = preg_replace("/[\b[A-Za-z]{2}/", '', $newVat);
                $newCommunityVat  = strtoupper($country->code) . $newVat;
            }
        }

        return [$newVat, $newCommunityVat];
    }

    /**
     * Returns the lastest version number for a given type
     *
     * @param $versionableType
     * @return integer|boolean
     */
    public static function getLastVersion($versionableType)
    {
        $lastVersion = Versioning::where('versionable_type', $versionableType)->orderBy('version', 'desc')->first();

        return $lastVersion->version ?? null;
    }

    /**
     * Sends warning email notification when Providers get sync
     *
     * @param $data
     */
    public static function sendSyncEmail($data)
    {
        $sendingTo = explode(',', config('custom.warning-emails-to'));

        if (!empty($data['extraSendTo'])) {
            $sendingTo = Arr::prepend($sendingTo, $data['extraSendTo']);
        }

        if (empty($sendingTo || !is_array($sendingTo))) {
            Log::error('Receiver list must be an array of email addresses', [
                'class'     => __CLASS__,
                'method'    => __METHOD__,
            ]);

        } else {
            // Mail warning
            try {
                Mail::to($sendingTo)
                    ->send(new GlobalSyncNotification($data));

            } catch (\Exception $e) {
                Log::error($e->getMessage(), [
                    'class'     => __CLASS__,
                    'method'    => __METHOD__,
                ]);
            }
        }
    }
}
