<?php

use App\Library\Constants;

return [

    /*
|--------------------------------------------------------------------------
| Application Name
|--------------------------------------------------------------------------
|
| This value is the name of your application. This value is used when the
| framework needs to place the application's name in a notification or
| any other location as required by the application or its packages.
|
*/

    'date_format' => env('DB_DATE_FORMAT', 'Y-m-d H:m:s'),
    'database_type' => env('DB_TYPE', 'mysql'),
    'k2_server' => env('K2_API_SERVER', 'localhost'),
    'ad_domains' => explode(',', env('AD_DOMAINS', '')),
    'ad_domain_access_type' => env('AD_DOMAIN_ACCESS_TYPE', 'dc'),
    'ad_domain_name' => env('AD_DOMAIN_NAME', 'domain.local'),
    'cmis_url' => env('CMIS_URL', 'http://localhost'),
    'cmis_user' => env('CMIS_USER'),
    'cmis_password' => env('CMIS_PASSWORD'),
    'iworking_public_bucket' => env('IWORKING_PUBLIC_S3_BUCKET_URL'),
    'iworking_public_bucket_folder' => env('IWORKING_PUBLIC_S3_BUCKET_URL_FOLDER'),
    'iworking_public_bucket_folder_invoices' => env('IWORKING_PUBLIC_S3_BUCKET_URL_FOLDER') . '/invoices/attachments',
    'iworking_public_bucket_folder_invoices_docs' => env('IWORKING_PUBLIC_S3_BUCKET_URL_FOLDER') . '/invoices/documents',
    'iworking_public_bucket_folder_orders' => env('IWORKING_PUBLIC_S3_BUCKET_URL_FOLDER') . '/orders/attachments',
    'date-formats' => [
        'd/m/Y' => 'dd/mm/yyyy', // 01/01/2021
        'j/n/y' => 'd/m/yy', // 1/1/21
        'm/d/Y' => 'mm/dd/yyyy', // 12/31/2021
        'n/j/y' => 'm/d/yyyy' // 3/31/21
    ],
    'number-formats' => ['decimals'],
    'email_integration' => [
        'host' => env('EMAIL_HOST'),
        'port' => env('EMAIL_PORT'),
        'encryption' => env('EMAIL_ENCRYPTION'),
        'validate_cert' => env('VALIDATE_CERT', false),
        'username' => env('EMAIL_USERNAME'),
        'password' => env('EMAIL_PASSWORD'),
        'default_account' => env('DEFAULT_ACCOUNT'),
        'protocol' => env('EMAIL_PROTOCOL'),
        'read_folder' => env('EMAIL_READ_FOLDER'),
        'save_folder' => env('EMAIL_SAVE_FOLDER')
    ],
    'currencies-short-list' => env('CURRENCIES_SHORT_LIST', 'eur,usd,gbp,jpy,krw'),
    'line-bulk-available-options' => [
        'delete', 'change-tax-type', 'change-num-order'
    ],
    'allowed-users-special-header-buttons'      => env('ALLOWED_USERS_SPECIAL_HEADER_BUTTONS', 'lpiqueras@example.com'),
    'max-entries-for-invoices-tasks-table-list' => env('MAX_ENTRIES_FOR_INVOICES_TASKS_TABLE_LIST', 100),
    'max-entries-for-invoices-lines-table'      => env('MAX_ENTRIES_FOR_INVOICES_LINES_TABLE_LIST', 100),
    'global-cache-time-in-seconds'              => env('GLOBAL_CACHE_TIME_IN_SECONDS', 300), // 5 min
    'dashboard-cache-time-in-seconds'           => env('DASHBOARD_CACHE_TIME_IN_SECONDS', 1800), // 30 min
    'invoice-invalidation-reasons'              => [
        'reason-duplicated', 'reason-incomplete', 'reason-proforma', 'reason-no-charges', 'reason-others'
    ],
    'invoice-invalidation-forbidden-status'     => [
        Constants::INVOICE_STATUS_POSTED, Constants::INVOICE_STATUS_MANUALLY_POSTED, Constants::INVOICE_STATUS_INVALID
    ],
    'user-email-domain-for-chat-permission' => env('USER_EMAIL_DOMAIN_CHAT_PERMISSION', '@labrubio'),
    'warning-emails-to' => env('WARNING_EMAILS_TO', 'ncanteli@strategying.com, lpiqueras@strategying.com'),
    'bpm_anonymous_user' => env('BPM_ANONYMOUS_USER'),
    'bpm_anonymous_pwd' => env('BPM_ANONYMOUS_PWD'),
    'bpm_server' => env('BPM_API_SERVER','localhost'),
    'orders-delivery-points' => env('ORDERS_DELIVERY_POINTS', 'Castellbisbal,Castellbisbal Recepción,Castellbisbal Almacén,Picking literaturas,Picking,Otros'),
    'orders-default-purchasing-group'   => '001',
    'orders-default-buyer'              => '1000',
    'orders-default-document-type'      => 'ZPOR',
    'orders-default-order-type'         => 'K',
    'json-orders-default-order-type'    => 'O',
    'orders-order-line-default-center'  => '1001',
    'orders-delivery-qty-variation'     => env('ORDERS_DELIVERY_QTY_VARIATION', 0),
    'provider-process-validator-email'  => env('PROVIDER_PROCESS_VALIDATOR_EMAIL', 'ybeza@labrubio.com')
];
