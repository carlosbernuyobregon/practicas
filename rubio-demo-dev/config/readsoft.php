<?php

return [

    'config' => [
        'user' => env('READSOFT_USER'),
        'password' => env('READSOFT_PASSWORD'),
        'customer-id' => env('READSOFT_CUSTOMER_ID'),
        'buyer-id' => env('READSOFT_BUYER_ID'),
        'key' => env('READSOFT_KEY'),
        'version' => env('READSOFT_VERSION'),
        'uiculture' => env('READSOFT_UICULTURE'),
        'culture' => env('READSOFT_CULTURE')
    ],
];
