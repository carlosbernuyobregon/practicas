<?php
/**
 * Configuration for Alfresco-Laravel connection
 */

return [
	'url' => env('ALFRESCO_URL',''), //URL of alfresco
	'api' => env('ALFRESCO_API','cmis'), //cmis or rest
	'api_version' => env('ALFRESCO_API_VERSION','1.1'), //alfresco api version
	'repository_id' => env('ALFRESCO_REPOSITORY_ID','-default-'), //Repository where the files will we uploaded
	'base_id' => env('ALFRESCO_BASE_ID','f36ee9c4-48ba-4bc6-b46c-71b6f4f84ae8'), //Folder where the files will we uploaded, must already exist in the site
	'base_path' => env('ALFRESCO_BASE_PATH','/Shared'), //Folder where the files will we uploaded, must already exist in the site
	'user' => env('ALFRESCO_USER','extstrategying01'), //Username to acces alfresco
	'pass' => env('ALFRESCO_PASSWORD','Depexe39'), //Password to access alfresco
	'repeated_policy' => env('ALFRESCO_REPEATED_POLICY','rename'), //rename or overwrite
	'debug' => env('ALFRESCO_DEBUG',true), //rename or overwrite
	'explorer' => env('ALFRESCO_EXPLORER',false),
];
