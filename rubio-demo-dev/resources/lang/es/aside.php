<?php

return [
    'dashboard' => 'Dashboard',
    'options' => 'Opciones',
    'title' => [
        'masters' => 'Maestros'
    ],
    'links' => [
        'users' => 'Usuarios',
        'management' => [
            'roles' => 'Gestión de roles',
            'users' => 'Gestión de usuarios',
            'permissions' => 'Gestión de permisos',
        ],
        'requests' => 'Procesos de negocio',
        'request' => [
            'quotation' => 'Nueva petición'
        ],
        'cruds' => 'Cruds',
        'cruds-management' => [
            'countries' => 'Countries management',
            'currencies' => 'Currencies management',
            'tax-types' => 'Tax types management',
            'cost-centers' => 'Cost centers management',
            'accounting-accounts' => 'Accounting accounts management',
            'materials' => 'Materials management',
            'assets' => 'Assets management',
            'suppliers' => 'Suppliers management',
            'orders'    => 'Solicitud de pedido',
            'budgets' => 'Gestión de presupuestos',
            'provider-payment-conditions' => 'Condiciones de pago',
            'provider-payment-types' => 'Tipos de pago'
        ],

    ],
];
