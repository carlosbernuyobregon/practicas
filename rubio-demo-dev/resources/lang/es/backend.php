<?php

use function PHPSTORM_META\map;

return [
    'hi' => 'Hola',
    'my-profile' => 'Mi perfil',
    'langs' => [
        'es' => 'Español',
        'en' => 'Inglés'
    ],
    'table-empty' => 'No se ha encontrado información',
    'forms' => [
        'select-option' => ' -- seleccione una opción -- ',
        'select-user' => '-- seleccione un usuario --',
        'save'  => 'Salvar',
        'update' => 'Actualizar',
        'close' => 'Cerrar',
        'delete-confirmation' => [
            'title' => 'Confirmación de Borrado',
            'text'  => '¿Está seguro que quiere borrar este elemento?',
            'action' => 'Borrar',
        ],
        'selects' => [
            'all' => 'Mostrar todo',
            '10' => 'Mostrar 10 filas',
            '25' => 'Mostrar 25 filas',
            '50' => 'Mostrar 50 filas',
            '100' => 'Mostrar 100 filas',
        ],
        'search' => 'Buscar:',
        'lines' => [
            'bulk' => [
                'actions' => [
                    'title'                             => 'Acciones masivas',
                    'text'                              => 'Seleccione un tipo de acción',
                    'confirmation-text'                 => '¿Está seguro?',
                    'delete'                            => 'Borrar',
                    'change-tax-type'                   => 'Cambiar tipo de IVA',
                    'change-num-order'                  => 'Cambiar nro pedido',
                    'select-tax-type-text'              => 'Seleccione un nuevo tipo de IVA',
                    'insert-num-order-text'             => 'Inserte un nuevo nro de pedido',
                    'insert-num-order-placeholder-text' => 'Nro de pedido',
                    'action'                            => 'Continuar',
                    'close'                             => 'Cerrar',
                    'accept'                            => 'Aceptar'
                ]
            ]
        ],
        'invoice' => [
            'invalidation' => [
                'title'     => 'Invalidar factura',
                'text'      => 'Seleccione el motivo',
                'reasons'   => [
                    'reason-duplicated' => 'Factura duplicada',
                    'reason-incomplete' => 'Datos incompletos ',
                    'reason-proforma'   => 'Proforma',
                    'reason-no-charges' => 'Sin cargo',
                    'reason-others'     => 'Otros',
                ],
                'free-reason'               => '¿Deseas agregar algo más?',
                'free-reason-placeholder'   => 'Información adicional',
                'actions'                   => [
                    'invalidate'        => 'Invalidar',
                    'close'             => 'Cerrar',
                    'accept'            => 'Aceptar',
                    'confirmation-text' => '¿Estás seguro que deseas invalidar la factura?'
                ]
            ]
        ]
    ],
    'crud' => [
        'actions' => 'Acciones',
        'created_at' => 'Fecha de creación',
        'table_empty' => 'No se han encontrado coincidencias',
        'pageBreadcrumbs' => [
            'masters' => 'Administración',
            'list' => 'Lista'
        ],
        'list' => 'Lista',
        'masters' => 'Administracion',
        'country' => [
            'title' => 'Paises',
            'list' => [
                'add' => 'Nuevo País',
                'key_value' => 'País',
                'code'  => 'Código',
                'currency' => 'Moneda'
            ],
            'form' => [
                'add' => 'Nuevo País',
                'update' => 'Actualizar País',
                'name' => 'País',
                'name_placeholder' => 'Introduzca País',
                'description' => 'Descripción',
                'description_placeholder' => 'Introduzca Descripción',
                'code'  => 'Código',
                'code_placeholder'  => 'Introduzca Código',
                'currency' => 'Moneda',
                'customer' => 'Cliente'
            ],
            'messages' => [
                'save' => 'Información de País guardada satisfactoriamente.',
                'delete' => 'País eliminado satisfactoriamente.'
            ]
        ],
        'currency' => [
            'title' => 'Monedas',
            'list' => [
                'add' => 'Nueva Moneda',
                'code' => 'Código',
                'name' => 'Nombre'
            ],
            'form' => [
                'add' => 'Nueva Moneda',
                'update' => 'Actualizar Moneda',
                'code' => 'Código',
                'code_placeholder' => 'Introduzca Código',
                'name' => 'Nombre',
                'name_placeholder' => 'Introduzca Nombre',
                'customer' => 'Cliente'
            ],
            'messages' => [
                'save' => 'Información de Moneda guardada satisfactoriamente.',
                'delete' => 'Moneda eliminada satisfactoriamente.'
            ],
        ],'provider-payment-type' => [
            'title' => 'Tipos de Pago',
            'page_title' => 'Lista de Tipos de Pago',
            'list' => [
                'add' => 'Nuevo',
                'code' => 'Código',
                'name' => 'Nombre',
                'description' => 'Descripción',
                'default' => 'Predefinido'
            ],
            'form' => [
                'add' => 'Agregar Tipo de Pago',
                'update' => 'Actualizar Tipo de Pago',
                'code' => 'Código',
                'code_placeholder' => 'Introduzca el Código del Tipo de Pago',
                'description' => 'Descripción',
                'description_placeholder' => 'Introduzca la Descripción',
                'name' => 'Nombre',
                'name_placeholder' => 'Introduzca el Nombre del Tipo de Pago',
                'default' => [
                    'label' => 'Tipo de Pago por Defecto',
                    'options' => [
                        'yes' => 'Si',
                        'no' => 'No'
                    ]
                ],
            ],
            'messages' => [
                'save' => 'Información de Tipo de Pago de Proveedor guardada satisfactoriamente.',
                'delete' => 'Tipo de Pago de Proveedor de eliminado satisfactoriamente.'
            ],
        ],
        'tax-type' => [
            'title' => 'Tipo de Tax',
            'list' => [
                'add' => 'Nuevo Tipo de Tax',
                'type' => 'Tipo',
                'description' => 'Descripción'
            ],
            'form' => [
                'add' => 'Agregar Tipo de Tax',
                'update' => 'Actualizar Tipo de Tax',
                'type' => 'Tipo',
                'type_placeholder' => 'Introduzca Tipo de Tax',
                'description' => 'Descripción',
                'description_placeholder' => 'Introduzca Descripción',
                'percentage' => 'Porciento',
                'percentage_placeholder' => 'Introduzca Porciento',
                'default' => [
                    'label' => 'Tax por Defecto',
                    'options' => [
                        'yes' => 'Si',
                        'no' => 'No'
                    ]
                ],

                'customer' => 'Cliente'
            ],
            'messages' => [
                'save' => 'Información de Tipo de Tax guardada satisfactoriamente.',
                'delete' => 'Tipo de Tax eliminado satisfactoriamente.'
            ],
        ],
        'provider-payment-condition' => [
            'title' => 'Condiciones de pago',
            'pageTitle' => 'Lista de condiciones de pago',
            'list' => [
                'add' => 'Nueva',
                'code' => 'Código',
                'name' => 'Nombre',
                'description' => 'Descripción',
                'default' => 'Por defecto'
            ],
            'form' => [
                'add' => 'Agregar nueva condición de pago',
                'update' => 'Actualizar condición de pago',
                'name' => 'Nombre',
                'name_placeholder' => 'Introduzca el nombre',
                'description' => 'Descripción',
                'description_placeholder' => 'Introduzca la descripción',
                'code' => 'Código',
                'code_placeholder' => 'Introduzca un código',
                'default' => [
                    'label' => 'Condición de pago de por defecto',
                    'options' => [
                        'yes' => 'Si',
                        'no' => 'No'
                    ]
                ],
            ],
            'messages' => [
                'save' => 'Información de condiciones de pago guardada satisfactoriamente.',
                'delete' => 'Condición de pago eliminada correctamente'
            ],
        ],
        'user' => [
            'information' => 'Información de Usuario',
            'form' => [
                'add' => 'Agregar Usuario',
                'update' => 'Actualizar Usuario',
            ],
            'index' => [
                'title' => 'Listado de Usuarios',
                'id' => 'id',
                'complete-name' => 'Nombre de Usuario',
                'roles' => 'Roles',
                'email' => 'Email',
                'phone' => 'Teléfono',
                'actions' => 'Acciones',
            ],
            'edit' => [
                'title' => 'Formulario de Usuario',
                'id' => 'id',
                'complete-name' => 'Nombre Completo',
                'roles' => 'Roles',
                'email' => 'Email',
                'email_placeholder' => 'Introduzca Email',
                'phone' => 'Teléfono',
                'phone_placeholder' => 'Introduzca Teléfono',
                'actions' => 'Acciones',
                'name' => 'Nombre de Usuario',
                'name_placeholder' => 'Introduzca Nombre de Usuario',
                'first_name' => 'Nombre',
                'first_name_placeholder' => 'Introduzca Nombre',
                'last_name' => 'Apellido',
                'last_name_placeholder' => 'Introduzca Apellido',
                'customer' => 'Cliente',
                'new-password' => 'Nueva Contraseña',
                'new-password_placeholder' => 'Introduzca Nueva Contraseña',
                'empty-password-msg' => 'Dejar vacío para mantener la misma contraseña.',
                'confirm-password' => 'Confirme la Nueva Contraseña',
                'confirm-password_placeholder' => 'Confirme la Nueva Contraseña',
                'timezone' => 'Zona Horaria',
                'date-format' => 'Formato de Fechas',
                'number-format' => 'Formato de Números',
                'user-state' => 'Estado de la Cuenta',
                'active' => 'Activa',
                'blocked' => 'Bloqueada',
                'country' => 'Pais',
                'cost_center' => 'Centro de Coste',
                'cost_center_default_msg' => 'Por Defecto',
                'cost_center_set_default_msg' => 'Activar Por Defecto',
            ],
            'messages' => [
                'save' => 'Información de Usuario guardada satisfactoriamente.',
                'delete' => 'Usuario eliminado satisfactoriamente.',
            ]
        ],
        'role' => [
            'information' => 'Información de Roles',
            'index' => [
                'title' => 'Listado de Roles',
                'name' => 'Nombre',
                'description' => 'Descripción'
            ],
            'edit' => [
                'key' => 'Identificador del Rol',
                'name' => 'Nombre',
                'description' => 'Descripción'
            ],
            ///NEW CRUD
            'title' => 'Roles',
            'list'  => [
                'add' => 'Nuevo Rol',
                'name' => 'Nombre',
                'description' => 'Descripción',
            ],
            'form' => [
                'add' => 'Agregar Rol',
                'update' => 'Actualizar Rol',
                'name' => 'Nombre',
                'name_placeholder' => 'Introduzca Nombre del Rol',
                'description' => 'Descripción',
                'description_placeholder' => 'Introduzca Descripción del Rol',
                'customer' => 'Cliente'
            ],
            'messages' => [
                'save' => 'Información de Rol guardada satisfactoriamente.',
                'delete' => 'Rol eliminado satisfactoriamente.',
            ]
        ],
        'cost-center' => [
            'title' => 'Centro de Coste',
            'list' => [
                'add' => 'Nuevo Centro de Coste',
                'name' => 'Nombre',
                'company' => 'Compañía'
            ],
            'form' => [
                'add' => 'Agregar Centro de Coste',
                'update' => 'Actualizar Centro de Coste',
                'external_id' => 'ID Externo',
                'external_id_placeholder' => 'Introduzca ID Externo',
                'name' => 'Nombre',
                'name_placeholder' => 'Introduzca Nombre',
                'company' => 'Compañía',
                'company_placeholder' => 'Introduzca Compañía',
                'customer' => 'Cliente'
            ],
            'messages' => [
                'save' => 'Información de Centro de Coste guardado satisfactoriamente.',
                'delete' => 'Centro de Coste eliminado satisfactoriamente.',
            ],
        ],
        'accounting-account' => [
            'title' => 'Cuenta Contable',
            'list' => [
                'add' => 'Nueva Cuenta Contable',
                'name' => 'Nombre',
                'company' => 'Compañía'
            ],
            'form' => [
                'add' => 'Agregar Cuenta Contable',
                'update' => 'Actualizar Cuenta Contable',
                'external_id' => 'ID Externo',
                'external_id_placeholder' => 'Introduzca ID Externo',
                'name' => 'Nombre',
                'name_placeholder' => 'Introduzca Nombre',
                'company' => 'Compañía',
                'company_placeholder' => 'Introduzca Compañía',
                'customer' => 'Cliente'
            ],
            'messages' => [
                'save' => 'Información de Cuenta Contable guardada satisfactoriamente.',
                'delete' => 'Cuenta Contable eliminado satisfactoriamente.',
            ],
        ],
        'material' => [
            'title' => 'Materiales',
            'list' => [
                'add' => 'Nuevo Material',
                'material_name' => 'Nombre del Material',
                'company' => 'Compañía',
                'type' => 'Tipo'
            ],
            'form' => [
                'add' => 'Agregar Material',
                'update' => 'Actualizar Material',
                'external_id' => 'ID Externo',
                'external_id_placeholder' => 'Introduzca ID Externo',
                'material_name' => 'Nombre del Material',
                'material_name_placeholder' => 'Introduzca Nombre del Material',
                'company' => 'Compañía',
                'company_placeholder' => 'Introduzca Nombre del Material',
                'type' => 'Tipo',
                'type_placeholder' => 'Introduzca Tipo',
                'customer' => 'Cliente'
            ],
            'messages' => [
                'save' => 'Información de Material guardada satisfactoriamente.',
                'delete' => 'Material eliminado satisfactoriamente.',
            ],
        ],
        'asset' => [
            'title' => 'Activos',
            'list' => [
                'add' => 'Nuevo Activo',
                'subnumber' => 'Subnúmero',
                'name' => 'Nombre',
                'company' => 'Compañía',
                'status' => 'Estado'
            ],
            'form' => [
                'add' => 'Agregar Activo',
                'update' => 'Actualizar Activo',
                'external_id' => 'ID Externo',
                'external_id_placeholder' => 'Introduzca ID Externo',
                'subnumber' => 'Subnúmero',
                'subnumber_placeholder' => 'Introduzca subnúmero',
                'name' => 'Nombre',
                'name_placeholder' => 'Introduzca Nombre',
                'company' => 'Compañía',
                'company_placeholder' => 'Introduzca Compañía',
                'status' => 'Estado',
                'status_placeholder' => 'Introduzca Estado',
                'customer' => 'Cliente',
            ],
            'messages' => [
                'save' => 'Información de Activo guardada satisfactoriamente.',
                'delete' => 'Activo eliminado satisfactoriamente.',
            ],
        ],
        'budget' => [
            'title' => 'Presupuesto',
            'list' => [
                'add' => 'New budget',
                'name' => 'Name',
                'company' => 'Company',
                'accounting-account' => 'Cuenta contable',
                'budget-agrupation' => 'Agrupación de presupuesto'
            ],
            'form' => [
                'add' => 'Add budget',
                'update' => 'Update budget',
                'external_id' => 'External ID',
                'external_id_placeholder' => 'Enter External ID',
                'name' => 'Name',
                'name_placeholder' => 'Enter name',
                'company' => 'Company',
                'company_placeholder' => 'Enter company',
                'customer' => 'Customer',
            ],
            'messages' => [
                'save' => 'Budget Saved Successfully.',
                'delete' => 'Budget Deleted Successfully.'
            ],
        ],
        'supplier' => [
            'title' => 'Proveedores',
            'list' => [
                'add' => 'Nuevo Proveedor',
                'vat_number' => 'Número NIF',
                'name' => 'Nombre',
                'company' => 'Compañía',
                'payment_condition' => 'Condición de Pago',
                'payment_type' => 'Tipo de Pago',
            ],
            'form' => [
                'add' => 'Agregar Proveedor',
                'update' => 'Actualizar Proveedor',
                'external_id' => 'ID Externo',
                'external_id_placeholder' => 'Introduzca ID Externo',
                'vat_number' => 'Número NIF',
                'vat_number_placeholder' => 'Introduzca Número NIF',
                'name' => 'Nombre',
                'name_placeholder' => 'Introduzca Nombre',
                'company' => 'Compañía',
                'country' => 'País',
                'company_placeholder' => 'Entroduzca Compañía',
                'payment_condition' => 'Condición de Pago',
                'payment_condition_placeholder' => 'Introduzca Condición de Pago',
                'payment_type' => 'Tipo de Pago',
                'payment_type_placeholder' => 'Introduzca Tipo de Pago',
                'customer' => 'Cliente',
                'community_vat_number' => 'Número NIF (ESxxxxxxx)',
                'community_vat_number_placeholder' => 'Introduzca número de NIF (ESxxxxxxx)',
                'zip_code' => 'Código postal',
                'zip_code_placeholder' => 'Introduzca un código postal',
                'city' => 'Ciudad',
                'city_placeholder' => 'Introduzca una ciudad',
                'address' => 'Dirección',
                'address_placeholder' => 'Introduzca una dirección'
            ],
            'messages' => [
                'save' => 'Información de Proveedor guardada satisfactoriamente.',
                'delete' => 'Proveedor eliminado satisfactoriamente.',

            ],
        ],
        'provider' => [
            'title' => 'Proveedores',
            'list' => [
                'add' => 'Nuevo Proveedor',
                'vat_number' => 'Número NIF',
                'name' => 'Nombre',
                'company' => 'Compañía',
                'payment_condition' => 'Condición de Pago',
                'payment_type' => 'Tipo de Pago',
            ],
            'form' => [
                'add' => 'Agregar Proveedor',
                'update' => 'Actualizar Proveedor',
                'external_id' => 'ID Externo',
                'external_id_placeholder' => 'Introduzca ID Externo',
                'vat_number' => 'Número NIF',
                'vat_number_placeholder' => 'Introduzca Número NIF',
                'name' => 'Nombre',
                'name_placeholder' => 'Introduzca Nombre',
                'company' => 'Compañía',
                'country' => 'País',
                'company_placeholder' => 'Entroduzca Compañía',
                'payment_condition' => 'Condición de Pago',
                'payment_condition_placeholder' => 'Introduzca Condición de Pago',
                'payment_type' => 'Tipo de Pago',
                'payment_type_placeholder' => 'Introduzca Tipo de Pago',
                'customer' => 'Cliente',
                'community_vat_number' => 'Número NIF (ESxxxxxxx)',
                'community_vat_number_placeholder' => 'Introduzca número de NIF (ESxxxxxxx)',
                'zip_code' => 'Código postal',
                'phone_number' => 'Nro. teléfono',
                'zip_code_placeholder' => 'Introduzca un código postal',
                'city' => 'Ciudad',
                'city_placeholder' => 'Introduzca una ciudad',
                'address' => 'Dirección',
                'address_placeholder' => 'Introduzca una dirección'
            ],
            'messages' => [
                'save' => 'Información de Proveedor guardada satisfactoriamente.',
                'delete' => 'Proveedor eliminado satisfactoriamente.',

            ],
        ],
        'order' => [
            'title' => 'Órdenes',
            'list' => [
                'add' => 'Nueva Orden',
                'company' => 'Compañía',
                'order_number' => 'Número de Orden',
                'date' => 'Fecha',
                'qty' => 'Cantidad',
                'unit_price' => 'Precio Unitario',
                'net_amount' => 'Cantidad Neta',
                'total_amount' => 'Cantidad Total',
                'status' => 'Estado'
            ],
            'form' => [
                'add' => 'Agregar Orden',
                'update' => 'Actualizar Orden',
                'external_id' => 'ID Externo',
                'external_id_placeholder' => 'Introduzca ID Externo',
                'company' => 'Compañía',
                'company_placeholder' => 'Introduzca Compañía',
                'order_number' => 'Número de Orden',
                'order_number_placeholder' => 'Introduzca Número de Orden',
                'date' => 'Fecha',
                'qty' => 'Cantidad',
                'qty_placeholder' => 'Introduzca Cantidad',
                'unit_price' => 'Precio Unitario',
                'unit_price_placeholder' => 'Introduzca Precio Unitario',
                'net_amount' => 'Cantidad Neta',
                'net_amount_placeholder' => 'Introduzca Cantidad Neta',
                'total_amount' => 'Cantidad Total',
                'total_amount_placeholder' => 'Introduzca Cantidad Total',
                'position' => 'Posición',
                'position_placeholder' => 'Introduzca un número',
                'material' => 'Material',
                'position_material' => 'Posición del Material',
                'position_material_placeholder' => 'Introduzca un número',
                'supplier' => 'Proveedor',
                'supplier_code' => 'Código del Proveedor',
                'supplier_code_placeholder' => 'Introduzca Código del Proveedor',
                'payment_conditions' => 'Condiciones de Pago',
                'payment_conditions_placeholder' => 'Introduzca Condiciones de Pago',
                'payment_type' => 'Tipo de Pago',
                'payment_type_placeholder' => 'Introduzca Tipo de Pago',
                'customer' => 'Cliente',
                'description' => 'Descripción',
                'description_placeholder' => 'Introduzca Descripción',
                'buyer' => 'Comprador',
                'buyer_placeholder' => 'Introduzca Comprador',
                'purchasing_group' => 'Grupo Adquisitivo',
                'purchasing_group_placeholder' => 'Introduzca Grupo Adquisitivo',
                'document_type' => 'Tipo de Documento',
                'document_type_placeholder' => 'Introduzca Tipo de Documento',
                'status' => 'Estado',
                'status_placeholder' => 'Introduzca Estado',
                'active' => 'Activo',
                'active-options' => [
                    'yes' => 'Sí',
                    'no'  => 'No'
                ]
            ],
            'messages' => [
                'save' => 'Orden guardada satisfactoriamente.',
                'delete' => 'Orden eliminada satisfactoriamente.',
            ],
        ]
    ],
    'orders' => [
        'status' => [
            '0'     => 'Borrador de pedido',
            '1'     => 'Pendiente aprobación Manager',
            '2'     => 'Aprobado Manager',
            '3'     => 'Aprobado Dirección',
            '4'     => 'Aprobado Dirección General',
            '5'     => 'Pedido bloqueado',
            '6'     => 'Pedido cancelado',
            '7'     => 'Pedido pendiente de modificación',
            '8'     => 'Pedido aprobado',
            '9'     => 'Pedido actualizado en SAP',
            '91'    => 'Pedido actualizado en SAP correctamente',
            '92'    => 'Pedido actualizado en SAP con error',
        ]
    ],
    'deliveries' => [
        'status' => [
            '91'    => 'Delivery actualizado en SAP correctamente',
            '92'    => 'Delivery actualizado en SAP con error',
        ]
    ],
    'providers' => [
        'status' => [
            '1'    => 'Proveedor aprobado',
            '2'    => 'Proveedor rechazado',
        ]
    ],
    'mailables' => [
        'non-invoice-notification' => [
            'title'         => 'Rubió - Gestión de documentos',
            'body'          => "Estimado proveedor,
            \nLe recordamos que la dirección facturasproveedores@labrubio.com está destinada exclusivamente para la
recepción de facturas en formato pdf o formato electrónico.
\n<b>El documento adjunto no será procesado.</b>",
            'body-details'  => "\nInformación adicional:
            \nCorreo de origen: :fromEmail
            \nFecha del envío: :emailDate",
            'body-bottom'   => "Por favor, no responda el presente email. Para cualquier consulta contacte con administracion@labrubio.com.",
        ]
    ],
    'messages' => [
        'delete-confirmation' => '¿Está seguro? Esta acción no puede deshacerse.'
    ]
];
