<?php

use App\Library\Constants;

return [


    Constants::INVOICE_STATUS_IN_PROCESS                            => 'En proceso',
    Constants::INVOICE_STATUS_OCR_PROCESSING                        => 'Procesando en OCR',
    Constants::INVOICE_STATUS_SENT_TO_SAP                           => 'Enviada a SAP',
    Constants::INVOICE_STATUS_VALIDATION_PENDING                    => 'Pendiente de validar',
    Constants::INVOICE_STATUS_ACCOUNTING_PENDING                    => 'Pendiente de contabilizar',
    Constants::INVOICE_STATUS_APPROVED                              => 'Aprobada',
    Constants::INVOICE_STATUS_BLOCKED                               => 'Bloqueada',
    Constants::INVOICE_STATUS_ACCOUNTING_ERROR                      => 'Error contabilizar',
    Constants::INVOICE_STATUS_POSTED                                => 'Contabilizada',
    Constants::INVOICE_STATUS_ACCOUNTING_ENTRY_NUMBER               => 'Num. Asiento',
    Constants::INVOICE_STATUS_ACCOUNTING_DATE                       => 'Fecha contabilización',
    Constants::INVOICE_STATUS_MANAGER_APPROVAL_PENDING              => 'Pendiente aprobación manager',
    Constants::INVOICE_STATUS_ACCOUNTING_OFFICE_INFORMATION_PENDING => 'Pendiente información departamento',
    Constants::INVOICE_STATUS_MANUALLY_POSTED                       => 'Contabilizado manual',
    Constants::INVOICE_STATUS_MANUAL_ACCOUNTING_APPROVED            => 'Aprobada contabilización manual',
    Constants::INVOICE_STATUS_DUPLICATED                            => 'Duplicada',
    Constants::INVOICE_STATUS_INVALID                               => 'No válida',
    Constants::INVOICE_STATUS_IS_NOT_AN_INVOICE                     => 'No es factura'
    // ToDo: index 9 está repetido
    //'9' => 'Recibida - pendiente enviar a OCR', // Deprecated
];
