<?php

return [
    'dashboard' => 'Dashboard',
    'tasks-list' => 'Tasks Inbox',
    'options' => 'Options',
    'my-requests' => 'History of Tasks',
    'title' => [
        'masters' => 'Masters'
    ],
    'links' => [
        'users' => 'Users',
        'management' => [
            'roles' => 'Roles management',
            'users' => 'User management',
            'permissions' => 'Permissions management',
        ],
        'cruds' => 'Cruds',
        'cruds-management' => [
            'countries' => 'Countries management',
            'currencies' => 'Currencies management',
            'tax-types' => 'Tax types management',
            'cost-centers' => 'Cost centers management',
            'accounting-accounts' => 'Accounting accounts management',
            'materials' => 'Materials management',
            'assets' => 'Assets management',
            'suppliers' => 'Suppliers management',
            'orders'    => 'Orders management',
            'provider-payment-conditions' => 'Payment conditions',
            'provider-payment-types' => 'Payment types'
        ],
        'requests' => 'Business process',
        'request' => [
            'request' => 'New Request',
            'quotation' => 'New Quotation',
            'tasks-list' => 'Request Tasks',
            'history-tasks' => 'History of tasks'
        ],
        'projects' => 'Project management',
        'project' => '',
    ],



];
