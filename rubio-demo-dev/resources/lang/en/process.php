<?php

return [
    'actions' => [
        'title' => 'Acciones de proceso',
        'buttons' => [
            'exit' => 'Exit',
            'cancel' => 'Cancel request',
            'reassign' => 'Reassign',

        ],
        'COMPLETE' => 'Complete',
        'CANCEL' => 'Cancel',
        'ACCEPT' => 'Accept',
        'REJECT' => 'Reject',
        'REASSIGN' => 'Reassign',
        'exit' => 'Exit',
        'close' => 'Close',
        'cancel' => 'Cancel',
        'save' => 'Save & exit',
        'save_draft' => 'Save draft',
        'labels' =>   [
            'cancelation_reason_id' => 'Cancellation reason',
            'cancelation_comment' => 'Cancelation comment',
        ],
        'aprobar' => 'Aprobar',
        'rechazar' => 'Rechazar',
        'modificar' => 'Modificar',
        'completar' => 'Completar',
    ],
    'project' => [
        'assign' => [
            'title' => 'Assign project and subprojects'
        ],
    ],
    'request' => [
        'title' => 'Request',
        'info' => [
            'title' => 'Request information',
        ],
        'create' => [
            'title' => 'Create request',
            'source' => 'Request origin',
            'requested_at' => 'Request date',
            'name' => 'Name',
            'last_name' => 'Surname',
            'product_class' => 'Product class',
            'request_type' => 'Request type',
            'phone' => 'Phone',
            'email' => 'Email',
            'billing_country' => 'Billing country',
            'purpose' => 'Purpose',
            'customer_type' => 'Customer type',
            'product_category' => 'Product category',
            'qty' => 'Quantity',
            'qty_n' => 'Uts',
            'comments' => 'Comments',
            'event_name' => 'Event name',
            'event_web' => 'Event website',
            'company_name' => 'Company name',
            'name' => 'Name',
            'web' => 'Web',
            'institution_name' => 'Institution name',
            'company_web' => 'Company web',
            'institution_web' => 'Institution web',
            'request_delivery_date' => 'Delivery date',
            'button' => [
                'submit' => 'Submit request',
                'save_draft' => 'Save draft request',
                'reassign' => 'Reassign'
            ],
            'reassign_user' => 'Reassign task to new user',
            'cancelation_reason_id' => 'Select ancelation reason'
         ],
         'form' => [
            'request-management-title' => 'Business manager',
            'availability' => 'Availability',
            'timming' => 'Timing',
            'quotation-generation' => 'Generate Quotation',
            'currency_id' => 'Billing country currency',
            'quotation-review' => 'Quotation review',
         ]
    ],
];
