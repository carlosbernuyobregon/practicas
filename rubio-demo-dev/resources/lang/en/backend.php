<?php

return [
    'hi' => 'Hi',
    'my-profile' => 'my profile',
    'langs' => [
        'es' => 'Spanish',
        'en' => 'English'
    ],
    'table-empty' => 'No information found',
    'forms' => [
        'select-option' => ' -- select an option -- ',
        'select-user' => '-- select an user --',
        'save'  => 'Save',
        'update' => 'Update',
        'close' => 'Close',
        'delete-confirmation' => [
            'title' => 'Delete confirmation',
            'text'  => 'Are you sure you want to delete this item?',
            'action' => 'Delete',
        ],
        'selects' => [
            'all' => 'Show all',
            '10' => 'Show 10 rows',
            '25' => 'Show 25 rows',
            '50' => 'Show 50 rows',
            '100' => 'Show 100 rows',
        ],
        'search' => 'Search:',
        'lines' => [
            'bulk' => [
                'actions' => [
                    'title'                             => 'Bulk actions',
                    'text'                              => 'Select a type of action',
                    'confirmation-text'                 => 'Are you sure?',
                    'delete'                            => 'Delete',
                    'change-tax-type'                   => 'Change tax type',
                    'change-num-order'                  => 'Change order number',
                    'select-tax-type-text'              => 'Select a new tax type',
                    'insert-num-order-text'             => 'Insert a new order number',
                    'insert-num-order-placeholder-text' => 'Order number',
                    'action'                            => 'Continue',
                    'close'                             => 'Close',
                    'accept'                            => 'Accept'
                ]
            ]
        ],
        'invoice' => [
            'invalidation' => [
                'title'     => 'Invoice invalidation',
                'text'      => 'Chose a reason',
                'reasons'   => [
                    'reason-duplicated' => 'Duplicated Invoice',
                    'reason-incomplete' => 'Incomplete data',
                    'reason-proforma'   => 'Proforma',
                    'reason-no-charges' => 'No charges',
                    'reason-others'     => 'Others',
                ],
                'free-reason'               => 'Do you want to add more information?',
                'free-reason-placeholder'   => 'Complementary information',
                'actions'                   => [
                    'invalidate'        => 'Invalidate',
                    'close'             => 'Close',
                    'accept'            => 'Accept',
                    'confirmation-text' => 'Are you sure you want to invalidate the invoice?'
                ]
            ]
        ]
    ],
    'crud' => [
        'actions' => 'Actions',
        'created_at' => 'Creation date',
        'table_empty' => 'No information found',
        'pageBreadcrumbs' => [
            'masters' => 'Masters',
            'list' => 'List'
        ],
        'list' => 'List',
        'masters' => 'Masters',
        'country' => [
            'title' => 'Countries',
            'list' => [
                'add' => 'New Country',
                'key_value' => 'country',
                'code'  => 'code',
                'currency' => 'currency'
            ],
            'form' => [
                'add' => 'Add Country',
                'update' => 'Update Country',
                'name' => 'Country',
                'name_placeholder' => 'Enter Country',
                'description' => 'Description',
                'description_placeholder' => 'Enter description',
                'code'  => 'Code',
                'code_placeholder'  => 'Enter Code',
                'currency' => 'Currency',
                'customer' => 'Customer'
            ],
            'messages' => [
                'save' => 'Country Saved Successfully.',
                'delete' => 'Country Deleted Successfully.'
            ]
        ],
        'currency' => [
            'title' => 'Currencies',
            'list' => [
                'add' => 'New Currency',
                'code' => 'Code',
                'name' => 'Name'
            ],
            'form' => [
                'add' => 'Add Currency',
                'update' => 'Update Currency',
                'code' => 'Code',
                'code_placeholder' => 'Enter Code',
                'name' => 'Name',
                'name_placeholder' => 'Enter Name',
                'customer' => 'Customer'
            ],
            'messages' => [
                'save' => 'Currency Saved Successfully.',
                'delete' => 'Currency Deleted Successfully.'
            ],
        ],
        'provider-payment-type' => [
            'title' => 'Payment Type',
            'page_title' => 'Payment Types List',
            'list' => [
                'add' => 'New',
                'code' => 'Code',
                'name' => 'Name',
                'description' => 'Description',
                'default' => 'Default',
            ],
            'form' => [
                'add' => 'Add a payment type',
                'update' => 'Update a payment type',
                'code' => 'Code',
                'code_placeholder' => 'Enter Code',
                'description' => 'Desciption',
                'description_placeholder' => 'Enter Description',
                'name' => 'Name',
                'name_placeholder' => 'Enter Name',
                'default' => [
                    'label' => 'Default Provider Payment Type',
                    'options' => [
                        'yes' => 'Yes',
                        'no' => 'No'
                    ]
                ],
            ],
            'messages' => [
                'save' => 'Provider Payment Type Saved Successfully',
                'delete' => 'Provider Payment Type Deleted Successfully'
            ],
        ],
        'tax-type' => [
            'title' => 'Tax types',
            'list' => [
                'add' => 'New tax type',
                'type' => 'Type',
                'description' => 'Description'
            ],
            'form' => [
                'add' => 'Add tax type',
                'update' => 'Update tax type',
                'type' => 'Type',
                'type_placeholder' => 'Enter tax type',
                'description' => 'Description',
                'description_placeholder' => 'Enter description',
                'percentage' => 'Percentage',
                'percentage_placeholder' => 'Enter percentage',
                'default' => [
                    'label' => 'Default tax',
                    'options' => [
                        'yes' => 'Yes',
                        'no' => 'No'
                    ]
                ],

                'customer' => 'Customer'
            ],
            'messages' => [
                'save' => 'Tax type Saved Successfully.',
                'delete' => 'Tax type Deleted Successfully.'
            ],
        ],
        'provider-payment-condition' => [
            'title' => 'Payment conditions',
            'pageTitle' => 'Payment conditions list',
            'list' => [
                'add' => 'New',
                'code' => 'Code',
                'name' => 'Name',
                'description' => 'Description',
                'default' => 'Default'
            ],
            'form' => [
                'add' => 'Add a payment condition',
                'update' => 'Update a payment condition',
                'name' => 'Name',
                'name_placeholder' => 'Enter name',
                'description' => 'Description',
                'description_placeholder' => 'Enter a description',
                'code' => 'Code',
                'code_placeholder' => 'Enter a code',
                'default' => [
                    'label' => 'Default payment condition',
                    'options' => [
                        'yes' => 'Yes',
                        'no' => 'No'
                    ]
                ],

            ],
            'messages' => [
                'save' => 'Payment condition saved successfully.',
                'delete' => 'Payment condition deleted successfully.'
            ],
        ],
        'user' => [
            'information' => 'User information',
            'form' => [
                'add' => 'Add',
                'update' => 'Update',
            ],
            'index' => [
                'title' => 'User List',
                'id' => 'id',
                'complete-name' => 'User name',
                'roles' => 'User roles',
                'email' => 'Email',
                'phone' => 'Phone',
                'actions' => 'Actions',
            ],
            'edit' => [
                'title' => 'User form',
                'id' => 'id',
                'complete-name' => 'User name',
                'roles' => 'User roles',
                'email' => 'Email',
                'email_placeholder' => 'Enter Email',
                'phone' => 'Phone',
                'phone_placeholder' => 'Enter Phone',
                'actions' => 'Actions',
                'name' => 'Name',
                'name_placeholder' => 'Enter User Name',
                'first_name' => 'First name',
                'first_name_placeholder' => 'Enter First name',
                'last_name' => 'Last name',
                'last_name_placeholder' => 'Enter Last name',
                'customer' => 'Customer',
                'new-password' => 'New Password',
                'new-password_placeholder' => 'Enter New Password',
                'empty-password-msg' => 'Keep empty to preserve the same password.',
                'confirm-password' => 'Confirm New Password',
                'confirm-password_placeholder' => 'Confirm New Password',
                'timezone' => 'Timezone',
                'date-format' => 'Dates Format',
                'number-format' => 'Numbers Format',
                'user-state' => 'User State',
                'active' => 'Active',
                'blocked' => 'Blocked',
                'country' => 'Country',
                'cost_center' => 'Cost Center',
                'cost_center_default_msg' => 'Default',
                'cost_center_set_default_msg' => 'Set Default',
            ],
            'messages' => [
                'save' => 'User Saved Successfully.',
                'delete' => 'User Deleted Successfully.',
            ]
        ],
        'role' => [
            'information' => 'Role information',
            'index' => [
                'title' => 'Roles list',
                'name' => 'Rol name',
                'description' => 'Rol description'
            ],
            'edit' => [
                'key' => 'Rol key',
                'name' => 'Rol name',
                'description' => 'Rol description'
            ],
            ///NEW CRUD
            'title' => 'Roles',
            'list'  => [
                'add' => 'New Role',
                'name' => 'Role name',
                'description' => 'Role description',
            ],
            'form' => [
                'add' => 'Add Role',
                'update' => 'Update Role',
                'name' => 'Role name',
                'name_placeholder' => 'Enter Role name',
                'description' => 'Role description',
                'description_placeholder' => 'Enter Role description',
                'customer' => 'Customer'
            ],
            'messages' => [
                'save' => 'Role Saved Successfully.',
                'delete' => 'Role Deleted Successfully',
            ]
        ],
        'cost-center' => [
            'title' => 'Cost Center',
            'list' => [
                'add' => 'New cost center',
                'name' => 'Name',
                'company' => 'Company'
            ],
            'form' => [
                'add' => 'Add cost center',
                'update' => 'Update cost center',
                'external_id' => 'External ID',
                'external_id_placeholder' => 'Enter External ID',
                'name' => 'Name',
                'name_placeholder' => 'Enter name',
                'company' => 'Company',
                'company_placeholder' => 'Enter company',
                'customer' => 'Customer'
            ],
            'messages' => [
                'save' => 'Cost center Saved Successfully.',
                'delete' => 'Cost center Deleted Successfully.'
            ],
        ],
        'budget' => [
            'title' => 'Presupuesto',
            'list' => [
                'add' => 'New budget',
                'name' => 'Name',
                'company' => 'Company',
                'accounting-account' => 'Account',
                'budget-agrupation' => 'Budget agrupation'
            ],
            'form' => [
                'add' => 'Add budget',
                'update' => 'Update budget',
                'external_id' => 'External ID',
                'external_id_placeholder' => 'Enter External ID',
                'name' => 'Name',
                'name_placeholder' => 'Enter name',
                'company' => 'Company',
                'company_placeholder' => 'Enter company',
                'customer' => 'Customer'
            ],
            'messages' => [
                'save' => 'Budget Saved Successfully.',
                'delete' => 'Budget Deleted Successfully.'
            ],
        ],
        'accounting-account' => [
            'title' => 'Accounting account',
            'list' => [
                'add' => 'New accounting account',
                'name' => 'Name',
                'company' => 'Company'
            ],
            'form' => [
                'add' => 'Add accounting account',
                'update' => 'Update accounting account',
                'external_id' => 'External ID',
                'external_id_placeholder' => 'Enter External ID',
                'name' => 'Name',
                'name_placeholder' => 'Enter name',
                'company' => 'Company',
                'company_placeholder' => 'Enter company',
                'customer' => 'Customer'
            ],
            'messages' => [
                'save' => 'Accounting account Saved Successfully.',
                'delete' => 'Accounting account Deleted Successfully.'
            ],
        ],
        'material' => [
            'title' => 'Material',
            'list' => [
                'add' => 'New material',
                'material_name' => 'Material name',
                'company' => 'Company',
                'type' => 'Type'
            ],
            'form' => [
                'add' => 'Add material',
                'update' => 'Update material',
                'external_id' => 'External ID',
                'external_id_placeholder' => 'Enter External ID',
                'material_name' => 'Material Name',
                'material_name_placeholder' => 'Enter material name',
                'company' => 'Company',
                'company_placeholder' => 'Enter company',
                'type' => 'Type',
                'type_placeholder' => 'Enter type',
                'customer' => 'Customer'
            ],
            'messages' => [
                'save' => 'Material Saved Successfully.',
                'delete' => 'Material Deleted Successfully.'
            ],
        ],
        'asset' => [
            'title' => 'Asset',
            'list' => [
                'add' => 'New asset',
                'subnumber' => 'Subnumber',
                'name' => 'Name',
                'company' => 'Company',
                'status' => 'Status'
            ],
            'form' => [
                'add' => 'Add asset',
                'update' => 'Update asset',
                'external_id' => 'External ID',
                'external_id_placeholder' => 'Enter External ID',
                'subnumber' => 'Subnumber',
                'subnumber_placeholder' => 'Enter subnumber',
                'name' => 'Name',
                'name_placeholder' => 'Enter name',
                'company' => 'Company',
                'company_placeholder' => 'Enter company',
                'status' => 'Status',
                'status_placeholder' => 'Enter status',
                'customer' => 'Customer'
            ],
            'messages' => [
                'save' => 'Asset Saved Successfully.',
                'delete' => 'Asset account Deleted Successfully.'
            ],
        ],
        'supplier' => [
            'title' => 'Supplier',
            'list' => [
                'add' => 'New asset',
                'vat_number' => 'Vat number',
                'name' => 'Name',
                'company' => 'Company',
                'payment_condition' => 'Payment condition',
                'payment_type' => 'Payment type',
            ],
            'form' => [
                'add' => 'Add supplier',
                'update' => 'Update supplier',
                'external_id' => 'External ID',
                'external_id_placeholder' => 'Enter External ID',
                'vat_number' => 'VAT number',
                'vat_number_placeholder' => 'Enter VAT number',
                'name' => 'Name',
                'name_placeholder' => 'Enter name',
                'company' => 'Company',
                'country' => 'Country',
                'company_placeholder' => 'Enter company',
                'payment_condition' => 'Payment condition',
                'payment_condition_placeholder' => 'Enter payment condition',
                'payment_type' => 'Payment type',
                'payment_type_placeholder' => 'Enter payment type',
                'customer' => 'Customer',
                'community_vat_number' => 'VAT number (ESxxxxxxx)',
                'community_vat_number_placeholder' => 'Enter a complete VAT number (ESxxxxxxx)',
                'zip_code' => 'Zip code',
                'phone_number' => 'Phone number',
                'zip_code_placeholder' => 'Enter a zip code',
                'city' => 'City',
                'city_placeholder' => 'Enter a city',
                'address' => 'Address',
                'address_placeholder' => 'Enter an address'
            ],
            'messages' => [
                'save' => 'Supplier Saved Successfully.',
                'delete' => 'Supplier account Deleted Successfully.'
            ],
        ],

        'order' => [
            'title' => 'Order',
            'list' => [
                'add' => 'New order',
                'company' => 'Company',
                'order_number' => 'Order number',
                'date' => 'Date',
                'qty' => 'Qty',
                'unit_price' => 'Unit price',
                'net_amount' => 'Net amount',
                'total_amount' => 'Total amount',
                'status' => 'Status'
            ],
            'form' => [
                'add' => 'Add order',
                'update' => 'Update order',
                'external_id' => 'External ID',
                'external_id_placeholder' => 'Enter External ID',
                'company' => 'Company',
                'company_placeholder' => 'Enter company',
                'order_number' => 'Order number',
                'order_number_placeholder' => 'Enter order number',
                'date' => 'Date',
                'qty' => 'Quantity',
                'qty_placeholder' => ' Enter quantity',
                'unit_price' => 'Unit price',
                'unit_price_placeholder' => 'Enter unit price',
                'net_amount' => 'Net amount',
                'net_amount_placeholder' => 'Enter net amount',
                'total_amount' => 'Total amount',
                'total_amount_placeholder' => 'Enter total amount',
                'position' => 'Position',
                'position_placeholder' => 'Enter a number',
                'material' => 'Material',
                'position_material' => 'Position material',
                'position_material_placeholder' => 'Enter a number',
                'supplier' => 'Supplier',
                'supplier_code' => 'Supplier code',
                'supplier_code_placeholder' => 'Enter Supplier code',
                'payment_conditions' => 'Payment conditions',
                'payment_conditions_placeholder' => 'Enter payment conditions',
                'payment_type' => 'Payment type',
                'payment_type_placeholder' => 'Enter payment type',
                'customer' => 'Customer',
                'description' => 'Description',
                'description_placeholder' => 'Enter description',
                'buyer' => 'Buyer',
                'buyer_placeholder' => 'Enter a buyer',
                'purchasing_group' => 'Purchasing group',
                'purchasing_group_placeholder' => 'Enter purchasing group',
                'document_type' => 'Document type',
                'document_type_placeholder' => 'Enter document type',
                'status' => 'Status',
                'status_placeholder' => 'Enter status',
                'active' => 'Active',
                'active-options' => [
                    'yes' => 'Yes',
                    'no'  => 'No'
                ]
            ],
            'messages' => [
                'save' => 'Order Saved Successfully.',
                'delete' => 'Order account Deleted Successfully.',

            ],
        ],
    ],
    'orders' => [
        'status' => [
            '0'     => 'Borrador de pedido',
            '1'     => 'Pendiente aprobación Manager',
            '2'     => 'Aprobado Manager',
            '3'     => 'Aprobado Dirección',
            '4'     => 'Aprobado Dirección General',
            '5'     => 'Pedido bloqueado',
            '6'     => 'Pedido cancelado',
            '7'     => 'Pedido pendiente de modificación',
            '8'     => 'Pedido aprobado',
            '9'     => 'Pedido actualizado en SAP',
            '91'    => 'Pedido actualizado en SAP correctamente',
            '92'    => 'Pedido actualizado en SAP con error',
        ]
    ],
    'deliveries' => [
        'status' => [
            '91'    => 'Delivery actualizado en SAP correctamente',
            '92'    => 'Delivery actualizado en SAP con error',
        ]
    ],
    'providers' => [
        'status' => [
            '1'    => 'Proveedor aprobado',
            '2'    => 'Proveedor rechazado',
        ]
    ],
    'mailables' => [
        'non-invoice-notification' => [
            'title'         => 'Rubió - Document management',
            'body'          => 'Dear supplier,
            \nWe remind you that the mailbox facturasproveedores@labrubio.com only accepts invoices in pdf or electronic
format.
\n<b>The attached document will not be processed.</b>',
            'body-details'  => "\nAdditional information:
            \nSender email: :fromEmail
            \nSent at: :emailDate",
            'body-bottom'   => "Please do not reply to this email. For any questions contact administracion@labrubio.com.",
        ]
    ],
    'messages' => [
        'delete-confirmation' => 'Are you sure? This action can not be undone.'
    ]
];
