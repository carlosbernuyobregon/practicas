<?php

return [
    'save' => 'Se ha editado y guardado nueva información en la cabecera de factura',
    'saveLine' => 'Se han editado y guardado las lineas de factura',
    'approve' => 'Se ha aprobado la factura',
    'approveManual' => 'Se ha aprobado la solicitud de contabilización manual por el mánager.',
    'block' => 'Se ha bloqueado la factura',
    'unblock' => 'Se ha desbloqueado la factura',
    'authorize' => 'Se ha solicitado la autorización',
    'sendEmail' => 'Se ha solicitado información de la factura',
    'unapprove' => 'Se ha retirado la aprobación',
    'manual' => 'Se ha contabilizado la factura manualmente',
    'posted' => 'Se ha contabilizado la factura con el número de documento:',
    'errorPosting' => 'Ha habido un error al contabilizar:'
];
