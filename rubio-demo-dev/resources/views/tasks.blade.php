@extends('layouts.app',[
'pageTitle' => 'Tasks list',
'pageBreadcrumbs' => [
'Dashboard',
'Tasks list'
]
])

@section('content')
<div class="kt-portlet kt-portlet--mobile">
    <div class="kt-portlet__head kt-portlet__head--lg">
        <div class="kt-portlet__head-label">
            <span class="kt-portlet__head-icon">
                <i class="kt-font-brand fas fa-inbox"></i>
            </span>
            <h3 class="kt-portlet__head-title text-uppercase">
                TASK LIST
            </h3>
        </div>
        <div class="kt-portlet__head-toolbar">
            <div class="kt-portlet__head-wrapper">
                <div class="kt-portlet__head-actions">

                </div>
            </div>
        </div>
    </div>
    <div class="kt-portlet__body">
        <!--begin: Datatable -->
        <table class="table table-striped- table-bordered table-hover table-checkable" id="users_list_table">
            <thead>
                <tr class="text-uppercase">
                    <th>Request ID</th>
                    <th>Customer name</th>
                    <th>Business line</th>
                    <th>Purpose</th>
                    <th>Customer type</th>
                    <th>Request date</th>
                    <th>Request owner</th>
                    <th>Status</th>
                    <th>Delivery date</th>
                    <th>Task title</th>
                    <th>@lang('backend.crud.actions')</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($processRequests as $request)
                <tr>
                    <td>{{ $request->public_request_code }}</td>
                    <td>{{ $request->name ?? '' }} {{ $request->surname ?? ''}}</td>
                    <td>{{ $request->purpose->businessLine->name ?? ''}}</td>
                    <td>{{ $request->purpose->name ?? ''}}</td>
                    <td>{{ $request->customerType->name ?? ''}}</td>
                    <td>{{ auth()->user()->applyTimeZone($request->created_at) }}</td>
                    <td>{{ $request->owner()->name }}</td>
                    <td>{{ $request->statusLiteral() ?? '' }}</td>
                    <td>{{ auth()->user()->applyTimeZone($request->request_delivery_date) ?? '' }}</td>
                    <td>{{ $request->next_task_name }}</td>
                    <td nowrap>
                        <!--<span class="dropdown">
                            <a href="#" class="btn btn-sm btn-clean btn-icon btn-icon-md" data-toggle="dropdown"
                                aria-expanded="true">
                                <i class="fas fa-ellipsis-h"></i>
                            </a>
                            <div class="dropdown-menu dropdown-menu-right">
                                @if ($request->status == 8)
                            <a class="dropdown-item" href="{{ route('generate.docs.quotation',$request->id) }}"><i class="fas fa-edit"></i> Download quotation</a>
                                @endif

                                <a class="dropdown-item" href="#"><i class="fas fa-leaf"></i> Update Status</a>
                                <a class="dropdown-item" href="#"><i class="fas fa-print"></i> Generate Report</a>
                            </div>
                        </span>-->
                        <a href="{{ $requestUrls[$request->id] ?? route('process.open',['processName' => 'request','processId' => $request->id]) }}"
                            class="btn btn-sm btn-clean btn-icon btn-icon-md" title="View">
                            <i class="fas fa-edit"></i>
                        </a>
                        @if (isset($flowsUrls[$request->id]))
                        <a href="{{ $flowsUrls[$request->id] }}" target="_blank"
                            class="btn btn-sm btn-clean btn-icon btn-icon-md" title="Open Flow">
                            <i class="fas fa-project-diagram"></i>
                        </a>
                        @endif
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
        <!--end: Datatable -->
    </div>
</div>
@endsection
@push('css')
<!--begin::Page Vendors Styles(used by this page) -->
<link href="{{ url('/')}}/assets/vendors/custom/datatables/datatables.bundle.css" rel="stylesheet" type="text/css" />
<!--end::Page Vendors Styles -->
@endpush
@push('js')
<!--begin::Page Vendors(used by this page) -->
<script src="{{ url('/')}}/assets/vendors/custom/datatables/datatables.bundle.js" type="text/javascript"></script>

<!--end::Page Vendors -->

<!--begin::Page Scripts(used by this page) -->
<script>
    "use strict";
var KTDatatablesBasicBasic = function() {

	var initTable1 = function() {
		var table = $('#users_list_table');

		// begin first table
		table.DataTable({
			responsive: true,
            filter: true,
			// DOM Layout settings
			dom: `<'row'<'col-sm-12'tr>>
			<'row'<'col-sm-12 col-md-5'i><'col-sm-12 col-md-7 dataTables_pager'lp>>`,

			lengthMenu: [5, 10, 25, 50],

			pageLength: 10,

			language: {
				'lengthMenu': 'Display _MENU_',
			},

			// Order settings
			order: [[0, 'desc']],
		});
	};

	return {

		//main function to initiate the module
		init: function() {
			initTable1();
		},

	};

}();

jQuery(document).ready(function() {
    //KTDatatablesBasicBasic.init();
    $('#users_list_table').DataTable({
        order: [[0,"desc"]],
        dom: 'Bfrtip',
        buttons: [
            'copyHtml5',
            'excelHtml5',
            //'csvHtml5',
            'pdfHtml5',
            'pageLength'
        ],
        lengthMenu: [
            [ 10, 25, 50, -1 ],
            [ '10 rows', '25 rows', '50 rows', 'Show all' ]
        ]
    });
});
</script>
@endpush
