@extends('layouts.app', [
'pageTitle' => 'Listado de tareas de procesos',
'pageBreadcrumbs' => [
    'Procesos',
    'Tareas',
]
])

@section('content')
<div class="kt-portlet kt-portlet--mobile">
    <div class="kt-portlet__head kt-portlet__head--lg">
        <div class="kt-portlet__head-label">
            <span class="kt-portlet__head-icon">
                <i class="kt-font-brand fas fa-shopping-cart fa-lg"></i>
            </span>
            <h3 class="kt-portlet__head-title text-uppercase">
                Listado de tareas
            </h3>
        </div>
        <div class="kt-portlet__head-toolbar">
            <div class="kt-portlet__head-wrapper">
                <div class="kt-portlet__head-actions">

                </div>
            </div>
        </div>
    </div>
    <div class="kt-portlet__body">
        @include('components.alerts')
        @livewire('process.common-task-list')
    </div>
</div>
@endsection
@push('css')

@endpush
