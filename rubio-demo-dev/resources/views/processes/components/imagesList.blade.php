@foreach ($files as $file)
<li>
    <a href="route('files.delete',$file->id)" onclick="javascript:return confirm('Are you sure you want to delete this file?')">
        <img src="{{ $file->cmis_url ?? $file->url ?? '' }}" />
    </a>
</li>
@endforeach
