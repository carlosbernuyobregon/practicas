<div id="jsForm">

</div>
<div class="row mt-5 justify-content-between">
    <div class="col-12">
        <h4>@lang('process.actions.title')</h4>
    </div>
    <div class="col-12 col-md-6">
        @if (count($bpmActions) > 0)
            @foreach ($bpmActions as $bpmAction)
            @if (($bpmAction != 'cancel')&&($bpmAction != 'CANCEL'))
            <button type="button" class="btn btn-primary btn-pill btn-custom-primary btnProcessAction"
                custom-action="{{ $bpmAction }}" onclick="selectAction(this);">@lang('process.actions.'.$bpmAction)</button>
            @else
            <button type="button" class="btn btn-primary btn-pill btn-custom-primary btnProcessAction"
            data-toggle="modal" data-target="#cancelModal">@lang('process.actions.'.$bpmAction)</button>
            @endif
            @endforeach
        @elseif(($processId == null)||($processRequest->status == 0))
        <input type="submit" class="btn btn-primary btn-pill btn-custom-primary" value="@lang('process.request.create.button.submit')">
        @else
        <p>There's no actions available.</p>
        @endif
    </div>
    <div class="col-12 col-md-6 mt-4 mt-md-0">
        <a href="{{ route('tasks.list') }}"
            onclick="return confirm('Are your sure you want to exit? You will lose your changes.')"
            class="btn btn-warning float-right mx-1 btn-pill btn-custom-primary">
            @lang('process.actions.exit')
        </a>
        @if ((count($bpmActions) > 0)||(count($bpmActions) == 0&&($processId == null))||($request->status == 0))
        <button type="button" class="btn btn-secondary btn-hover-brand float-right btn-pill btn-custom-primary-reverse"
            onclick="saveAsDraft(this)">
            @if (count($bpmActions)>0)
            @lang('process.actions.save')
            @else
            @lang('process.actions.save_draft')
            @endif

        </button>
        @endif
    </div>
</div>
<div class="row mt-3 justify-content-end">

</div>
@push('css')

@endpush
@push('modals')
@if (in_array('CANCEL',$bpmActions))
<div class="modal fade" id="cancelModal" tabindex="-1" role="dialog" aria-labelledby="cancelModalLabel"
aria-hidden="true">
<form action="{{ route('process.proceed',['processName' => $processName, 'processId' => $request->id]) }}" method="POST" name="cancelationForm" id="cancelationForm">
    @csrf
    @method('PUT')
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Cancelation reason</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-12">
                        <div class="form-group">
                            <label
                                for="cancelation_reason_id">@lang('process.actions.labels.cancelation_reason_id')*:</label>
                            <select class="form-control kt-selectpicker" data-live-search="true"
                                id="cancelation_reason_id" name="cancelation_reason_id" required>
                                <option hidden disabled selected value>@lang('backend.forms.select-option')</option>
                                @foreach ($cancelationReasons as $cancelationReason)
                                <option value="{{$cancelationReason->id}}" rel="{{$cancelationReason->key_value}}"
                                    {{ (isset($request)) ? (($request->customer_type_id == $cancelationReason->id) ? 'selected' : '' ) : ''}}>
                                    {{ $cancelationReason->name }}</option>
                                @endforeach
                            </select>
                            <input type="hidden" value="{{ $quote->id ?? ''}}" name="quote_id" />
                            <input type="hidden" value="{{ $request->id ?? ''}}" name="request_id" />
                            <input type="hidden" value="{{ $request->id ?? ''}}" name="processRequest" />
                            <input type="hidden" name="bpm_sn" value="{{ $SN ?? 'SN'}}" />
                            <div id="cancelFormDinamic">

                            </div>
                        </div>
                        <div class="form-group">
                            <label
                                for="cancelation_reason_id">@lang('process.actions.labels.cancelation_comment')*:</label>
                            <textarea name="cancelation_reason_text" id="cancelation_reason_text" class="form-control" rows="3"></textarea>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">@lang('process.actions.close')</button>
                <button type="button" class="btn btn-danger" custom-action="CANCEL" onclick="cancelAction(this);">@lang('process.actions.cancel')</button>
            </div>
        </div>
    </div>
</form>
</div>
@endif
@if (isset($redirectUsers))
<div class="modal fade" id="reassignTask" tabindex="-1" role="dialog" aria-labelledby="reassignTaskLabel"
aria-hidden="true">
    <form method="post" action="{{ route('process.reassign',$sn) }}">
        @csrf
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Reassign task</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-12">

                            <div class="form-group">
                                <label
                                    for="next_task_user_id">@lang('process.request.create.reassign_user')*:</label>
                                <select class="form-control kt-selectpicker" data-live-search="true"
                                    id="next_task_user_id" name="reassign_user" required>
                                    <option hidden disabled selected value>@lang('backend.forms.select-option')
                                    </option>
                                    @foreach ($redirectUsers as $user)
                                    <option value="{{$user->id}}"
                                        {{ (isset($processRequest)) ? (($processRequest->next_task_user_id == $user->id) ? 'selected' : '' ) : ''}}>
                                        {{ $user->first_name }} {{ $user->last_name }}
                                    </option>
                                    @endforeach
                                </select>
                                <input type="hidden" value="{{ $sn }}" name="bpm_sn" />
                            </div>
                            <div class="col-12 col-md-6">
                                <input type="submit" class="btn btn-danger"
                                    value="@lang('process.request.create.button.reassign')">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary btn-custom-primary-secondary" data-dismiss="modal">Cancel</button>
                    <button type="submit" class="btn btn-danger btn-custom-primary">Reassign</button>
                </div>
            </div>
        </div>
    </form>
</div>
@endif
@endpush
@push('js')
<script src="{{url('/')}}/assets/vendors/general/jquery-validation/dist/jquery.validate.js" type="text/javascript">
</script>
<script src="{{url('/')}}/assets/vendors/general/jquery-validation/dist/additional-methods.js" type="text/javascript">
</script>
<script src="{{url('/')}}/assets/vendors/custom/js/vendors/jquery-validation.init.js" type="text/javascript"></script>
<script src="{{url('/')}}/assets/js/demo1/pages/crud/forms/validation/form-controls.js" type="text/javascript"></script>

<script>
    function saveAsDraft(action) {
        var form = action.closest('form');
        var draftInputValue = '<input type="hidden" value="1" name="draft">';
        var element = document.getElementById('jsForm');
        element.innerHTML = draftInputValue;
        form.submit();
    }
    function selectAction(action){
        var form = action.closest('form');
        var selectedAction = action.getAttribute('custom-action');
        var actionInputValue = '<input type="hidden" value="'+ selectedAction +'" name="bpm_action">';
        var element = document.getElementById('jsForm');
        element.innerHTML = actionInputValue;
        form.submit();
    }
    function cancelAction(action){
        var form = document.getElementById('cancelationForm');
        var selectedAction = action.getAttribute('custom-action');
        var actionInputValue = '<input type="hidden" value="'+ selectedAction +'" name="bpm_action">';
        var element = document.getElementById('cancelFormDinamic');
        element.innerHTML = actionInputValue;
        form.submit();
    }
</script>
@endpush
