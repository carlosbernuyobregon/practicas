<div class="kt-portlet kt-portlet--mobile">
    <div class="kt-portlet__head kt-portlet__head--lg">
        <div class="kt-portlet__head-label">
            <span class="kt-portlet__head-icon">
                <i class="kt-font-brand flaticon2-line-chart"></i>
            </span>
            <h3 class="kt-portlet__head-title text-uppercase">
                PROJECTS TASK LIST
            </h3>
        </div>
        <div class="kt-portlet__head-toolbar">
            <div class="kt-portlet__head-wrapper">
                <div class="kt-portlet__head-actions">
                    <div class="dropdown dropdown-inline">
                        <button type="button" class="btn btn-default btn-icon-sm dropdown-toggle" data-toggle="dropdown"
                            aria-haspopup="true" aria-expanded="false">
                            <i class="fas fa-download"></i> Export
                        </button>
                        <div class="dropdown-menu dropdown-menu-right">
                            <ul class="kt-nav">
                                <li class="kt-nav__section kt-nav__section--first">
                                    <span class="kt-nav__section-text">Choose an option</span>
                                </li>
                                <li class="kt-nav__item">
                                    <a href="#" class="kt-nav__link">
                                        <i class="kt-nav__link-icon fas fa-print"></i>
                                        <span class="kt-nav__link-text">Print</span>
                                    </a>
                                </li>
                                <li class="kt-nav__item">
                                    <a href="#" class="kt-nav__link">
                                        <i class="kt-nav__link-icon fas fa-copy"></i>
                                        <span class="kt-nav__link-text">Copy</span>
                                    </a>
                                </li>
                                <li class="kt-nav__item">
                                    <a href="#" class="kt-nav__link">
                                        <i class="kt-nav__link-icon fas fa-file-excel-o"></i>
                                        <span class="kt-nav__link-text">Excel</span>
                                    </a>
                                </li>
                                <li class="kt-nav__item">
                                    <a href="#" class="kt-nav__link">
                                        <i class="kt-nav__link-icon fas fa-file-text-o"></i>
                                        <span class="kt-nav__link-text">CSV</span>
                                    </a>
                                </li>
                                <li class="kt-nav__item">
                                    <a href="#" class="kt-nav__link">
                                        <i class="kt-nav__link-icon fas fa-file-pdf-o"></i>
                                        <span class="kt-nav__link-text">PDF</span>
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="kt-portlet__body">
        <!--begin: Datatable -->
        <table class="table table-striped- table-bordered table-hover table-checkable" id="task_list_table">
            <thead>
                <tr class="text-uppercase">
                    <th>Date</th>
                    <th>Piority</th>
                    <th>Workflow name</th>
                    <th>Task name</th>
                    <th>Task description</th>
                    <th>@lang('backend.crud.actions')</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($tasks as $task)
                <tr>
                    <td>{{ auth()->user()->applyTimeZone($task->taskStartDate) }}</td>
                    <td>{{ $task->priority }}</td>
                    <td>{{ $task->workflowDisplayName }}</td>
                    <td>{{ $task->activityName }}</td>
                    <td>{{ $task->workflowInstanceFolio }}</td>
                    <td nowrap>
                        <a href="{{ $task->formURL }}" class="btn btn-sm btn-clean btn-icon btn-icon-md"
                            title="Open task">
                            <i class="fas fa-edit"></i>
                        </a>
                        <a href="{{ $task->viewFlowURL }}" target="_blank"
                            class="btn btn-sm btn-clean btn-icon btn-icon-md" title="Open Flow">
                            <i class="fas fa-project-diagram"></i>
                        </a>
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
        <!--end: Datatable -->
    </div>
</div>
@push('css')
<!--begin::Page Vendors Styles(used by this page) -->
<link href="{{ url('/')}}/assets/vendors/custom/datatables/datatables.bundle.css" rel="stylesheet" type="text/css" />
<!--end::Page Vendors Styles -->
@endpush
@push('js')
<!--begin::Page Vendors(used by this page) -->
<script src="{{ url('/')}}/assets/vendors/custom/datatables/datatables.bundle.js" type="text/javascript"></script>

<!--end::Page Vendors -->

<!--begin::Page Scripts(used by this page) -->
<script>
    var table = $('#task_list_table');

    // begin first table
    table.DataTable({
        order: [[0,"desc"]],
        dom: 'Bfrtip',
        buttons: [
            'copyHtml5',
            'excelHtml5',
            //'csvHtml5',
            'pdfHtml5',
            'pageLength'
        ],
        lengthMenu: [
            [ 10, 25, 50, -1 ],
            [ '10 rows', '25 rows', '50 rows', 'Show all' ]
        ]
    });
</script>
@endpush
