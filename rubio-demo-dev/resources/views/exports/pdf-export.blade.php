<table class="table table-striped table-bordered table-hover table-checkable mt-4" id="invoices_list">
        <thead>
            <tr>
                <th style="cursor: pointer;">
                    SDAD
                </th>
                <th style="cursor: pointer">
                    PROVEEDOR
                </th>
                <th style="cursor: pointer">
                    NIF/VAT
                </th>
                <th style="cursor: pointer">
                    NÚM. FRA
                </th>
                <th style="cursor: pointer">
                    FECHA FRA
                </th>
                <th style="cursor: pointer">
                    IMPORTE
                </th>
                <th style="cursor: pointer">
                    IVA
                </th>
                <th style="cursor: pointer">
                    TOTAL
                </th>
                <th style="cursor: pointer">
                    DIVISA
                </th>
                <th style="cursor: pointer">
                    VTO.
                </th>
                <th style="cursor: pointer">
                    DOC.SAP
                </th>
                <th style="cursor: pointer">
                    PEDIDO
                </th>
            </tr>
        </thead>
        <tbody>
            @forelse($invoices as $invoice)
            <tr>
                <td>{{ $invoice->sap_company }}</td>
                <td>{{ $invoice->provider_name }}</td>
                <td>{{ $invoice->vat_number }}</td>
                <td>{{ $invoice->invoice_number }}</td>
                <td>{{ auth()->user()->applyDateFormat($invoice->date, true) ?? ''}}</td>
                <td class="text-right">{{ number_format($invoice->total_net,2,',','.') }}</td>
                <td class="text-right">{{ number_format($invoice->taxes_total_amount,2,',','.') }}</td>
                <td class="text-right">{{ number_format($invoice->total,2,',','.') }}</td>
                <td>{{ $invoice->currency ?? ''}}</td>
                <td>{{ auth()->user()->applyDateFormat($invoice->payment_date, true) ?? ''}}</td>
                <td>{{ $invoice->sap_document_number }}</td>
                <td>{{ $invoice->intern_order ?? '' }}</td>
            </tr>
            @empty
            <tr>
                <td colspan="15" class="text-center">
                    No hay facturas para mostrar
                </td>
            </tr>
            @endforelse
        </tbody>
    </table>