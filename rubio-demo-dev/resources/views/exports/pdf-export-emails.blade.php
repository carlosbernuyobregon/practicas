<table class="table table-striped table-bordered table-hover table-checkable mt-4" id="invoices_list">
    <thead>
        <tr>
            <th style="cursor: pointer;">
                REMITENTE
            </th>
            <th style="cursor: pointer">
                EMAIL
            </th>
            <th style="cursor: pointer">
                ASUNTO
            </th>
            <th style="cursor: pointer">
                NOMBRE ARCHIVO
            </th>
            <th style="cursor: pointer">
                FECHA DESCARGA
            </th>
            <th style="cursor: pointer">
                FECHA PROCESADO
            </th>
            <th style="cursor: pointer">
                ESTADO
            </th>
        </tr>
    </thead>
    <tbody>
        @forelse($invoices as $invoice)
        <tr>
            <td>{{ $invoice->provider_name ?? '' }}</td>
            <td>{{ $invoice->emailAttachment->email->from }}</td>
            <td>{{ $invoice->emailAttachment->email->subject }}</td>
            <td>{{ $invoice->emailAttachment->filename }}</td>
            <td>{{ $invoice->emailAttachment->created_at }}</td>
            <td>{{ $invoice->emailAttachment->updated_at }}</td>
            <td>@lang('status.'.$invoice->status)</td>
        </tr>
        @empty
        <tr>
            <td colspan="15" class="text-center">
                No hay facturas para mostrar
            </td>
        </tr>
        @endforelse
    </tbody>
</table>