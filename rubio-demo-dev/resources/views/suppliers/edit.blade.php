@extends('layouts.app', [
'pageTitle' => (isset($provider->id)) ? 'Editar: ' . $provider->name : 'Crear proveedor',
'pageBreadcrumbs' => [
    'Proveedores',
    (isset($provider->id)) ? 'Editar proveedor ' . $provider->name : 'Crear proveedor',
]
])

@section('content')
<div class="kt-portlet kt-portlet--mobile">
    <div class="kt-portlet__head kt-portlet__head--lg">
        <div class="kt-portlet__head-label">
            <span class="kt-portlet__head-icon">
                <i class="kt-font-brand fas fa-shopping-cart fa-lg"></i>
            </span>
            <h3 class="kt-portlet__head-title text-uppercase">
                {{ (isset($provider->id)) ? 'Editar proveedor ' . $provider->name : 'Crear proveedor' }}
            </h3>
        </div>
        <div class="kt-portlet__head-toolbar">
            <div class="kt-portlet__head-wrapper">
                <div class="kt-portlet__head-actions">

                </div>
            </div>
        </div>
    </div>
    <div class="kt-portlet__body">
        @livewire('cruds.suppliers.edit',[
            'provider' => $provider,
            'task' => $task,
            'editable' => $editable,
            'actions' => $actions,
            'processInstance' => $processInstance
        ])
    </div>
</div>
@endsection
@push('css')

@endpush
