<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="description" content="Login page example">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Rubio') }}</title>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <!--begin::Fonts -->
    <script src="https://ajax.googleapis.com/ajax/libs/webfont/1.6.16/webfont.js"></script>
    <script>
        WebFont.load({
				google: {
					"families": ["Poppins:300,400,500,600,700", "Roboto:300,400,500,600,700"]
				},
				active: function() {
					sessionStorage.fonts = true;
				}
			});
    </script>

    <!--end::Fonts -->

    <!--begin::Page Custom Styles(used by this page) -->
    <link href="{{ config('custom.iworking_public_bucket') }}/assets/css/demo1/pages/general/login/login-1.css" rel="stylesheet" type="text/css" />

    <!--end::Page Custom Styles -->

    <!--begin:: Global Mandatory Vendors -->
    <link href="{{ config('custom.iworking_public_bucket') }}/assets/vendors/general/perfect-scrollbar/css/perfect-scrollbar.css" rel="stylesheet"
        type="text/css" />

    <!--end:: Global Mandatory Vendors -->


    <!--begin::Global Theme Styles(used by all pages) -->
    <link href="{{ config('custom.iworking_public_bucket') }}/assets/css/demo1/style.bundle.css" rel="stylesheet" type="text/css" />

    <!--end::Global Theme Styles -->

    <!--begin::Layout Skins(used by all pages) -->
    <link href="{{ config('custom.iworking_public_bucket') }}/assets/css/demo1/skins/header/base/light.css" rel="stylesheet" type="text/css" />
    <link href="{{ config('custom.iworking_public_bucket') }}/assets/css/demo1/skins/header/menu/light.css" rel="stylesheet" type="text/css" />
    <link href="{{ config('custom.iworking_public_bucket') }}/assets/css/demo1/skins/brand/dark.css" rel="stylesheet" type="text/css" />
    <link href="{{ config('custom.iworking_public_bucket') }}/assets/css/demo1/skins/aside/dark.css" rel="stylesheet" type="text/css" />

    <!--end::Layout Skins -->
    <link rel="shortcut icon" href="{{ config('custom.iworking_public_bucket') }}/assets/media/logos/favicon.ico" />

    <!-- Styles -->
    <link href="{{ asset('css/custom.css') }}" rel="stylesheet">
</head>

<body>
    <!-- begin:: Page -->
    <div class="kt-grid kt-grid--ver kt-grid--root">
        <div class="kt-grid kt-grid--hor kt-grid--root  kt-login kt-login--v1" id="kt_login">
            <div
                class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--desktop kt-grid--ver-desktop kt-grid--hor-tablet-and-mobile">

                <!--begin::Aside-->
                <div class="kt-grid__item kt-grid__item--order-tablet-and-mobile-2 kt-grid kt-grid--hor kt-login__aside cm-login-screen"
                    style="background-image: url({{ asset('/img/iworkbackground.jpg') }});">

                    <!--
                        <div class="kt-grid__item">
                            <a href="#" class="kt-login__logo">
                                <img alt="{{ config('app.name') }}" src="{{ config('custom.iworking_public_bucket') }}/img/ivftrans.png" class="img-fluid" style="max-height: 100px;"/>
                            </a>
                        </div>
                    -->
                    <div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--ver">
                        <div class="kt-grid__item kt-grid__item--middle">
                            @if (App::environment('production'))
                            <h3 class="kt-login__title">Welcome to FinNet iWorking</h3>
                            @else
                            <h3 class="kt-login__title" style="color:black; text-transform:uppercase;">{{ App::environment() }} environment.</h3>
                            <h2 style="color:black;">Please, be sure you want to work here.</h2>
                            @endif
                        </div>
                    </div>
                    <div class="kt-grid__item">
                        <div class="kt-login__info">
                            <div class="kt-login__copyright">
                                &copy {{ now()->year }} Strategying
                            </div>
                            <div class="kt-login__menu">
                                <a href="#" class="kt-link">Privacy</a>
                                <a href="#" class="kt-link">Legal</a>
                                <a href="#" class="kt-link">Contact</a>
                            </div>
                        </div>
                    </div>
                </div>

                <!--begin::Aside-->

                <!--begin::Content-->
                <div
                    class="kt-grid__item kt-grid__item--fluid  kt-grid__item--order-tablet-and-mobile-1  kt-login__wrapper">

                    <!--begin::Head-->
                    <!--<div class="kt-login__head">
                        <span class="kt-login__signup-label">Don't have an account yet?</span>&nbsp;&nbsp;
                        <a href="#" class="kt-link kt-login__signup-link">Sign Up!</a>
                    </div>
                    -->
                    <!--end::Head-->

                    <!--begin::Body-->
                    <div class="kt-login__body">
                        <!--begin::Signin-->
                        <div class="kt-login__form">

                            <div class="kt-login__title">
                                <img src="{{ asset('/img/logo_rubio.jpg') }}" class="img-fluid" style="width: 200px;"/>
                            </div>
                            <div class="kt-login__title">
                                <h3>Sign In</h3>
                            </div>
                            @if (session('error'))
                            <div class="alert alert-danger alert-dismissible fade show" role="alert">
                                {{ session('error') }}
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            @endif
                            <!--begin::Form-->
                            <form class="kt-form" method="POST" action="{{ route('login') }}">
                                @csrf
                                <div class="form-group">
                                    <input id="email" placeholder="Email" type="email"
                                        class="form-control @error('email') is-invalid @enderror" name="email"
                                        value="{{ old('email') }}" required autocomplete="off" autofocus>

                                    @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                                <div class="form-group">
                                    <input id="password" type="password"
                                        class="form-control @error('password') is-invalid @enderror"
                                        placeholder="Password" name="password" required autocomplete="current-password">

                                    @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                                <div class="form-group mt-3">
                                    <div class="kt-checkbox-list">
                                        <label class="kt-checkbox">
                                            <input class="form-check-input kt-login__actions" type="checkbox"
                                                name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>
                                            {{ __('Remember Me') }}
                                            <span></span>
                                        </label>
                                    </div>
                                </div>
                                <!--begin::Action-->
                                <div class="kt-login__actions">
                                    @if (Route::has('password.request'))
                                    <a class="kt-link kt-login__link-forgot" href="{{ route('password.request') }}">
                                        {{ __('Forgot Your Password?') }}
                                    </a>
                                    @endif
                                    <button type="submit" id="kt_login_signin_submit"
                                        class="btn btn-primary btn-elevate kt-login__btn-primary">{{ __('Login') }}</button>
                                </div>

                                <!--end::Action-->
                            </form>

                            <!--end::Form-->

                            <!--end::Options-->
                        </div>

                        <!--end::Signin-->
                    </div>

                    <!--end::Body-->
                </div>

                <!--end::Content-->
            </div>
        </div>
    </div>


    <!-- begin::Global Config(global config for global JS sciprts) -->
    <script>
        var KTAppOptions = {
				"colors": {
					"state": {
						"brand": "#5d78ff",
						"dark": "#282a3c",
						"light": "#ffffff",
						"primary": "#5867dd",
						"success": "#34bfa3",
						"info": "#36a3f7",
						"warning": "#ffb822",
						"danger": "#fd3995"
					},
					"base": {
						"label": ["#c5cbe3", "#a1a8c3", "#3d4465", "#3e4466"],
						"shape": ["#f0f3ff", "#d9dffa", "#afb4d4", "#646c9a"]
					}
				}
			};
    </script>

    <!-- end::Global Config -->

    <!--begin:: Global Mandatory Vendors -->
    <script src="{{ config('custom.iworking_public_bucket') }}/assets/vendors/general/jquery/dist/jquery.js" type="text/javascript"></script>
    <script src="{{ config('custom.iworking_public_bucket') }}/assets/vendors/general/popper.js/dist/umd/popper.js" type="text/javascript">
    </script>
    <script src="{{ config('custom.iworking_public_bucket') }}/assets/vendors/general/bootstrap/dist/js/bootstrap.min.js" type="text/javascript">
    </script>
    <script src="{{ config('custom.iworking_public_bucket') }}/assets/vendors/general/js-cookie/src/js.cookie.js" type="text/javascript"></script>
    <script src="{{ config('custom.iworking_public_bucket') }}/assets/vendors/general/moment/min/moment.min.js" type="text/javascript"></script>
    <script src="{{ config('custom.iworking_public_bucket') }}/assets/vendors/general/tooltip.js/dist/umd/tooltip.min.js" type="text/javascript">
    </script>
    <script src="{{ config('custom.iworking_public_bucket') }}/assets/vendors/general/perfect-scrollbar/dist/perfect-scrollbar.js"
        type="text/javascript"></script>
    <script src="{{ config('custom.iworking_public_bucket') }}/assets/vendors/general/sticky-js/dist/sticky.min.js" type="text/javascript">
    </script>
    <script src="{{ config('custom.iworking_public_bucket') }}/assets/vendors/general/wnumb/wNumb.js" type="text/javascript"></script>

    <!--end:: Global Mandatory Vendors -->

    <!--begin::Global Theme Bundle(used by all pages) -->
    <script src="{{ config('custom.iworking_public_bucket') }}/assets/js/demo1/scripts.bundle.js" type="text/javascript"></script>

    <!--end::Global Theme Bundle -->

    <!--begin::Page Scripts(used by this page) -->
    <!--<script src="{{ config('custom.iworking_public_bucket') }}/assets/js/demo1/pages/login/login-1.js" type="text/javascript"></script>-->
    </div>
</body>

</html>
