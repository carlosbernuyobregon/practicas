@extends('layouts.app', [
'pageTitle' => 'Dashboard',
'pageBreadcrumbs' => [
    'Dashboard',
]
])

@section('content')
<div class="kt-portlet kt-portlet--mobile">
    <div class="kt-portlet__head kt-portlet__head--lg">
        <div class="kt-portlet__head-label">
            <span class="kt-portlet__head-icon">
                <i class="kt-font-brand fas fa-shopping-cart fa-lg"></i>
            </span>
            <h3 class="kt-portlet__head-title text-uppercase">
                Dashboard
            </h3>
        </div>
    </div>
    <div class="kt-portlet__body">
        <div class="row mt-2">
            <div class="col-12">
                <ul class="nav nav-tabs" id="myTab" role="tablist">
                    @if(auth()->user()->hasAnyRole(['direccion-de-pedidos','usuario-de-pedidos','admin','direccion-general']))
                    <li class="nav-item" role="presentation" wire:ignore>
                        <a class="nav-link active" id="orders-dashboard-tab" data-toggle="tab"
                            href="#orders-dashboard" role="tab" aria-controls="orders-dashboard"
                            aria-selected="true">Pedidos</a>
                    </li>
                    @endif
                    @if(auth()->user()->hasAnyRole(['administration-user','administration-manager','admin','direccion-general']))
                    <li class="nav-item" role="presentation" wire:ignore>
                        <a class="nav-link" id="invoices-dashboard-tab" data-toggle="tab"
                            href="#invoices-dashboard" role="tab" aria-controls="invoices-dashboard"
                            aria-selected="true">Facturas</a>
                    </li>
                    @endif
                </ul>
                <div class="tab-content" id="orderContent">
                    @if(auth()->user()->hasAnyRole(['direccion-de-pedidos','usuario-de-pedidos','admin','direccion-general']))
                    <div class="tab-pane fade show active" id="orders-dashboard" role="tabpanel"
                        aria-labelledby="orders-dashboard-tab" wire:ignore.self>
                        @livewire('orders.dashboard',[

                        ])
                    </div>
                    @endif
                    @if(auth()->user()->hasAnyRole(['administration-user','administration-manager','admin','direccion-general']))
                    @if(auth()->user()->hasAnyRole(['direccion-de-pedidos','usuario-de-pedidos','admin','direccion-general']))
                    <div class="tab-pane fade" id="invoices-dashboard" role="tabpanel"
                        aria-labelledby="invoices-dashboard-tab" wire:ignore.self>
                    @else
                    <div class="tab-pane fade active" id="invoices-dashboard" role="tabpanel"
                        aria-labelledby="invoices-dashboard-tab" wire:ignore.self>
                    @endif
                        <div class="row mb-5">
                            <div class="col-6" style="height: 300px">
                                <canvas id="invoicesByMonth" style=""></canvas>
                            </div>
                            <div class="col-6" style="height: 300px">
                                <canvas id="invoicesByStatus" ></canvas>
                            </div>
                        </div>
                        <div class="row mt-5">
                            <div class="col-6" style="height: 300px">
                                <canvas id="invoicesByProvider" ></canvas>
                            </div>
                            <div class="col-6" style="height: 300px">
                                <canvas id="amountByProvider" ></canvas>
                            </div>

                        </div>
                    </div>
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@push('js')

<script src="https://cdn.jsdelivr.net/npm/chart.js@2.8.0"></script>
<!--<script src="{{ url('/')}}/assets/js/demo1/pages/dashboard.js" type="text/javascript"></script>-->
<script>
    var ctx = document.getElementById('invoicesByMonth').getContext('2d');
    var chart = new Chart(ctx, {
        // The type of chart we want to create
        type: 'bar',

        // The data for our dataset
        data: {
            labels: [{!! $invoicesByMonth['month'] !!}],
            datasets: [{
                label: 'FACTURAS POR MES',
                backgroundColor: [
                'rgba(255, 99, 132)',
                'rgba(54, 162, 235)',
                'rgba(255, 206, 86)',
                'rgba(75, 192, 192)',
                'rgba(153, 102, 255)',
                'rgba(255, 159, 64)',
                'rgba(255, 99, 132)',
                'rgba(54, 162, 235)',
                'rgba(255, 206, 86)',
                'rgba(75, 192, 192)',
                'rgba(153, 102, 255)',
                'rgba(255, 159, 64)'
            ],
            borderColor: [
                'rgba(255, 99, 132, 1)',
                'rgba(54, 162, 235, 1)',
                'rgba(255, 206, 86, 1)',
                'rgba(75, 192, 192, 1)',
                'rgba(153, 102, 255, 1)',
                'rgba(255, 159, 64, 1)',
                'rgba(255, 99, 132, 1)',
                'rgba(54, 162, 235, 1)',
                'rgba(255, 206, 86, 1)',
                'rgba(75, 192, 192, 1)',
                'rgba(153, 102, 255, 1)',
                'rgba(255, 159, 64, 1)'
            ],
                data: [{!! $invoicesByMonth['count'] !!}]
            }]
        },

        // Configuration options go here
        options: {
            responsive: true,
            maintainAspectRatio: false,
            scales: {
                yAxes: [{
                    display: true,
                    ticks: {
                        beginAtZero: true,
                        min: 0,
                        precision:0
                    }
                }],
            },
            legend: {
                display: true,
                labels: {
                    boxWidth: 0,
                }
            }
        }
    });
    var ctx2 = document.getElementById('invoicesByProvider').getContext('2d');
    var myBarChart = new Chart(ctx2, {
        type: 'bar',
        data: {
            labels: [{!! $invoicesByCustomer['name'] !!}],
            datasets: [{
                label: 'FACTURAS POR PROVEEDOR',
                backgroundColor: [
                'rgba(255, 99, 132)',
                'rgba(54, 162, 235)',
                'rgba(255, 206, 86)',
                'rgba(75, 192, 192)',
                'rgba(153, 102, 255)',
                'rgba(255, 159, 64)',
                'rgba(255, 99, 132)',
                'rgba(54, 162, 235)',
                'rgba(255, 206, 86)',
                'rgba(75, 192, 192)',
                'rgba(153, 102, 255)',
                'rgba(255, 159, 64)'
            ],
            borderColor: [
                'rgba(255, 99, 132, 1)',
                'rgba(54, 162, 235, 1)',
                'rgba(255, 206, 86, 1)',
                'rgba(75, 192, 192, 1)',
                'rgba(153, 102, 255, 1)',
                'rgba(255, 159, 64, 1)',
                'rgba(255, 99, 132, 1)',
                'rgba(54, 162, 235, 1)',
                'rgba(255, 206, 86, 1)',
                'rgba(75, 192, 192, 1)',
                'rgba(153, 102, 255, 1)',
                'rgba(255, 159, 64, 1)'
            ],
                data: [{!! $invoicesByCustomer['count'] !!}]
            }]
        },
        options: {
            responsive: true,
            maintainAspectRatio: false,
            scales: {
                yAxes: [{
                    display: true,
                    ticks: {
                        beginAtZero: true,
                        min: 0,
                        precision:0
                    }
                }],

            },
            legend: {
                display: true,
                labels: {
                    boxWidth: 0,
                }
            }
        }
    });
    var ctx3 = document.getElementById('amountByProvider').getContext('2d');
    var myBarChart = new Chart(ctx3, {
        type: 'bar',
        data: {
            labels: [{!! $amountByProvider['name'] !!}],
            datasets: [{
                label: 'IMPORTE ACUMULADO POR PROVEEDOR',
                backgroundColor: [
                'rgba(255, 99, 132)',
                'rgba(54, 162, 235)',
                'rgba(255, 206, 86)',
                'rgba(75, 192, 192)',
                'rgba(153, 102, 255)',
                'rgba(255, 159, 64)',
                'rgba(255, 99, 132)',
                'rgba(54, 162, 235)',
                'rgba(255, 206, 86)',
                'rgba(75, 192, 192)',
                'rgba(153, 102, 255)',
                'rgba(255, 159, 64)'
            ],
            borderColor: [
                'rgba(255, 99, 132, 1)',
                'rgba(54, 162, 235, 1)',
                'rgba(255, 206, 86, 1)',
                'rgba(75, 192, 192, 1)',
                'rgba(153, 102, 255, 1)',
                'rgba(255, 159, 64, 1)',
                'rgba(255, 99, 132, 1)',
                'rgba(54, 162, 235, 1)',
                'rgba(255, 206, 86, 1)',
                'rgba(75, 192, 192, 1)',
                'rgba(153, 102, 255, 1)',
                'rgba(255, 159, 64, 1)'
            ],
                data: [{!! $amountByProvider['count'] !!}]
            }]
        },
        options: {
            responsive: true,
            maintainAspectRatio: false,
            scales: {
                yAxes: [{
                    display: true,
                    ticks: {
                        beginAtZero: true,
                        min: 0,
                        precision:0
                    }
                }],

            },
            legend: {
                display: true,
                labels: {
                    boxWidth: 0,
                }
            }
        }
    });

    var ctx4 = document.getElementById('invoicesByStatus').getContext('2d');
    var myBarChart = new Chart(ctx4, {
        type: 'doughnut',

        data: {
            labels: [{!! $invoicesByStatus['name'] !!}],
            datasets: [{
                label: 'FACTURAS POR ESTADO',
                backgroundColor: [
                'rgba(255, 99, 132)',
                'rgba(54, 162, 235)',
                'rgba(255, 206, 86)',
                'rgba(75, 192, 192)',
                'rgba(153, 102, 255)',
                'rgba(255, 159, 64)'
            ],
            borderColor: [
                'rgba(255, 99, 132, 1)',
                'rgba(54, 162, 235, 1)',
                'rgba(255, 206, 86, 1)',
                'rgba(75, 192, 192, 1)',
                'rgba(153, 102, 255, 1)',
                'rgba(255, 159, 64, 1)'
            ],
                data: [{!! $invoicesByStatus['count'] !!}]
            }]
        },
        options: {
            responsive: true,
            maintainAspectRatio: false,
            scales: {
            },
            legend: {
                display: true,
                labels: {
                    boxWidth: 0,
                }
            }
        }
    });

</script>
@endpush
@push('css')

@endpush
