<!DOCTYPE html>

<html lang="en">

<head>
    <meta charset="utf-8" />
    <title>{{ $page_title ?? config('app.name') }}</title>
    <meta name="description" content="Page with empty content">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link
        href="https://fonts.googleapis.com/css2?family=Poppins:wght@300;400;500;600;700&family=Roboto:wght@300;400;500;700&display=swap"
        rel="stylesheet">

    <link href="{{ config('custom.iworking_public_bucket') }}/assets/vendors/custom/fullcalendar/fullcalendar.bundle.css" rel="stylesheet"
        type="text/css" />
    <link href="{{ config('custom.iworking_public_bucket') }}/assets/vendors/general/perfect-scrollbar/css/perfect-scrollbar.css" rel="stylesheet"
        type="text/css" />
    <link href="{{ config('custom.iworking_public_bucket') }}/assets/vendors/general/tether/dist/css/tether.css" rel="stylesheet" type="text/css" />
    <link href="{{ config('custom.iworking_public_bucket') }}/assets/vendors/general/bootstrap-datepicker/dist/css/bootstrap-datepicker3.css"
        rel="stylesheet" type="text/css" />
    <link href="{{ config('custom.iworking_public_bucket') }}/assets/vendors/general/bootstrap-datetime-picker/css/bootstrap-datetimepicker.css"
        rel="stylesheet" type="text/css" />
    <link href="{{ config('custom.iworking_public_bucket') }}/assets/vendors/general/bootstrap-timepicker/css/bootstrap-timepicker.css"
        rel="stylesheet" type="text/css" />
    <link href="{{ config('custom.iworking_public_bucket') }}/assets/vendors/general/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet"
        type="text/css" />
    <link href="{{ config('custom.iworking_public_bucket') }}/assets/vendors/general/bootstrap-touchspin/dist/jquery.bootstrap-touchspin.css"
        rel="stylesheet" type="text/css" />
    <link href="{{ config('custom.iworking_public_bucket') }}/assets/vendors/general/bootstrap-select/dist/css/bootstrap-select.css" rel="stylesheet"
        type="text/css" />
    <link href="{{ config('custom.iworking_public_bucket') }}/assets/vendors/general/bootstrap-switch/dist/css/bootstrap3/bootstrap-switch.css"
        rel="stylesheet" type="text/css" />
    <link href="{{ config('custom.iworking_public_bucket') }}/assets/vendors/general/select2/dist/css/select2.css" rel="stylesheet" type="text/css" />
    <link href="{{ config('custom.iworking_public_bucket') }}/assets/vendors/general/ion-rangeslider/css/ion.rangeSlider.css" rel="stylesheet"
        type="text/css" />
    <link href="{{ config('custom.iworking_public_bucket') }}/assets/vendors/general/nouislider/distribute/nouislider.css" rel="stylesheet"
        type="text/css" />
    <link href="{{ config('custom.iworking_public_bucket') }}/assets/vendors/general/bootstrap-markdown/css/bootstrap-markdown.min.css"
        rel="stylesheet" type="text/css" />
    <link href="{{ config('custom.iworking_public_bucket') }}/assets/vendors/general/animate.css/animate.css" rel="stylesheet" type="text/css" />
    <link href="{{ config('custom.iworking_public_bucket') }}/assets/vendors/general/toastr/build/toastr.css" rel="stylesheet" type="text/css" />
    <link href="{{ config('custom.iworking_public_bucket') }}/assets/vendors/general/morris.js/morris.css" rel="stylesheet" type="text/css" />
    <link href="{{ config('custom.iworking_public_bucket') }}/assets/vendors/general/sweetalert2/dist/sweetalert2.css" rel="stylesheet"
        type="text/css" />
    <link href="{{ config('custom.iworking_public_bucket') }}/assets/vendors/general/socicon/css/socicon.css" rel="stylesheet" type="text/css" />
    <!--<link href="{{ config('custom.iworking_public_bucket') }}/assets/vendors/custom/vendors/line-awesome/css/line-awesome.css" rel="stylesheet"
        type="text/css" />
    <link href="{{ config('custom.iworking_public_bucket') }}/assets/vendors/custom/vendors/flaticon/flaticon.css" rel="stylesheet" type="text/css" />
    <link href="{{ config('custom.iworking_public_bucket') }}/assets/vendors/custom/vendors/flaticon2/flaticon.css" rel="stylesheet" type="text/css" />

    <link href="{{ asset('assets/css/fontawesome-free/css/all.min.css') }}" rel="stylesheet"
        type="text/css" />
    -->
    <script src="https://kit.fontawesome.com/a8bd06fa2d.js" crossorigin="anonymous"></script>

    <link href="{{ config('custom.iworking_public_bucket') }}/assets/css/demo1/style.bundle.css" rel="stylesheet" type="text/css" />
    <link href="{{ config('custom.iworking_public_bucket') }}/assets/css/demo1/skins/header/base/light.css" rel="stylesheet" type="text/css" />
    <link href="{{ config('custom.iworking_public_bucket') }}/assets/css/demo1/skins/header/menu/light.css" rel="stylesheet" type="text/css" />
    <link href="{{ config('custom.iworking_public_bucket') }}/assets/css/demo1/skins/brand/dark.css" rel="stylesheet" type="text/css" />
    <link href="{{ config('custom.iworking_public_bucket') }}/assets/css/demo1/skins/aside/dark.css" rel="stylesheet" type="text/css" />

    <link href="{{ asset('css/custom.css') }}" rel="stylesheet" type="text/css" />
    <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/select2-bootstrap-5-theme@1.1.1/dist/select2-bootstrap-5-theme.min.css" />
    <link rel="stylesheet" href="{{ asset('css/custom-domain.css') }}" />

    @stack('css')
    @livewireStyles
    <script src="https://cdn.jsdelivr.net/gh/alpinejs/alpine@v2.8.2/dist/alpine.min.js" defer></script>
</head>

<body
    class="kt-quick-panel--right kt-demo-panel--right kt-offcanvas-panel--right kt-header--fixed kt-header-mobile--fixed kt-subheader--fixed kt-subheader--enabled kt-subheader--solid kt-aside--enabled kt-aside--fixed kt-page--loading">

    <!-- begin:: Page -->
    @include('partials.headermobile')
    <div class="kt-grid kt-grid--hor kt-grid--root">
        <div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--ver kt-page">

            @include('partials.aside')


            <div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor kt-wrapper" id="kt_wrapper">

                @include('partials.header')

                <div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor">
                @if (auth()->user())
                    @include('partials.subheader',['pageTitle' => $pageTitle ?? '', 'pageBreadcrumbs' =>
                    $pageBreadcrumbs ?? ['']])
                @endif
                    <!-- begin:: Content -->
                    <div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content">
                        @if (session('status'))
                        <div class="alert alert-success alert-dismissible fade show" role="alert">
                            {{ session('status') }}
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        @endif
                        @if (session('warning'))
                        <div class="alert alert-danger alert-dismissible fade show" role="alert">
                            {{ session('warning') }}
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        @endif
                            @include('livewire.loader.loader')
                        @yield('content')
                    </div>

                    <!-- end:: Content -->
                </div>

                <!-- begin:: Footer -->
                <div class="kt-footer kt-grid__item kt-grid kt-grid--desktop kt-grid--ver-desktop" id="kt_footer">
                    <div class="kt-footer__copyright">
                        2020&nbsp;&copy;&nbsp;<a href="{{ route('dashboard') }}" target="_blank"
                            class="kt-link">iWorking</a>
                    </div>
                    <div class="kt-footer__menu">
                        <a href="#" target="_blank" class="kt-footer__menu-link kt-link">Varios</a>
                    </div>
                </div>

                <!-- end:: Footer -->
            </div>
        </div>
    </div>
    <div id="kt_scrolltop" class="kt-scrolltop">
        <i class="fa fa-arrow-up"></i>
    </div>

    @stack('modals')
    <script>
        var KTAppOptions = {
            "colors": {
                "state": {
                    "brand": "#5d78ff",
                    "dark": "#282a3c",
                    "light": "#ffffff",
                    "primary": "#5867dd",
                    "success": "#34bfa3",
                    "info": "#36a3f7",
                    "warning": "#ffb822",
                    "danger": "#fd3995"
                },
                "base": {
                    "label": ["#c5cbe3", "#a1a8c3", "#3d4465", "#3e4466"],
                    "shape": ["#f0f3ff", "#d9dffa", "#afb4d4", "#646c9a"]
                }
            }
        };
    </script>
    <script src="https://cdn.jsdelivr.net/npm/apexcharts"></script>
    <script src="{{ config('custom.iworking_public_bucket') }}/assets/vendors/general/jquery/dist/jquery.js" type="text/javascript"></script>
    <script src="{{ config('custom.iworking_public_bucket') }}/assets/vendors/general/popper.js/dist/umd/popper.js" type="text/javascript"></script>
    <script src="{{ config('custom.iworking_public_bucket') }}/assets/vendors/general/bootstrap/dist/js/bootstrap.min.js" type="text/javascript">
    </script>

    <script src="{{ config('custom.iworking_public_bucket') }}/assets/vendors/general/js-cookie/src/js.cookie.js" type="text/javascript"></script>
    <script src="{{ config('custom.iworking_public_bucket') }}/assets/vendors/general/perfect-scrollbar/dist/perfect-scrollbar.js" type="text/javascript"></script>

    <script src="{{ config('custom.iworking_public_bucket') }}/assets/vendors/general/sticky-js/dist/sticky.min.js" type="text/javascript"></script>
    <script src="{{ config('custom.iworking_public_bucket') }}/assets/js/demo1/scripts.bundle.js" type="text/javascript"></script>

    <script src="{{ asset('/assets/js/custom/custom-select-2.js') }}" type="text/javascript"></script>
    @livewireScripts

    @stack('js')
    <!--end::Page Scripts -->
</body>

<!-- end::Body -->

</html>
