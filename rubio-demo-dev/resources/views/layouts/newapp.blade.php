<!DOCTYPE html>

<html lang="en">

<!-- begin::Head -->

<head>
    <!--end::Base Path -->
    <meta charset="utf-8" />
    <title>{{ $page_title ?? config('app.name') }}</title>
    <meta name="description" content="Page with empty content">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!--begin::Fonts -->
<!--    <script src="https://ajax.googleapis.com/ajax/libs/webfont/1.6.16/webfont.js"></script>
    <script>
        WebFont.load({
				google: {
					"families": ["Poppins:300,400,500,600,700", "Roboto:300,400,500,600,700"]
				},
				active: function() {
					sessionStorage.fonts = true;
				}
			});
    </script>
-->
    <!--end::Fonts -->

    <!--begin::Page Vendors Styles(used by this page) -->
    <link href="https://fonts.googleapis.com/css2?family=Poppins:wght@300;400;500;600;700&family=Roboto:wght@300;400;500;700&display=swap" rel="stylesheet">

    <!--end::Page Vendors Styles -->

    <!--begin:: Global Mandatory Vendors -->

    <!--end:: Global Mandatory Vendors -->
    <!--begin:: Global Optional Vendors -->
    <link href="{{ url('/') }}/assets/vendors/general/socicon/css/socicon.css" rel="stylesheet" type="text/css" />
    <link href="{{ url('/') }}/assets/vendors/custom/vendors/line-awesome/css/line-awesome.css" rel="stylesheet"
        type="text/css" />
    <link href="{{ url('/') }}/assets/vendors/custom/vendors/flaticon/flaticon.css" rel="stylesheet" type="text/css" />
    <link href="{{ url('/') }}/assets/vendors/custom/vendors/flaticon2/flaticon.css" rel="stylesheet" type="text/css" />
    <link href="{{ url('/') }}/assets/vendors/general/@fortawesome/fontawesome-free/css/all.min.css" rel="stylesheet"
        type="text/css" />

    <!--begin::Global Theme Styles(used by all pages) -->
    <link href="{{ url('/') }}/assets/css/demo1/style.bundle.css" rel="stylesheet" type="text/css" />
    <!--end::Global Theme Styles -->

    <!--begin::Layout Skins(used by all pages) -->
    <link href="{{ url('/') }}/assets/css/demo1/skins/header/base/light.css" rel="stylesheet" type="text/css" />
    <link href="{{ url('/') }}/assets/css/demo1/skins/header/menu/light.css" rel="stylesheet" type="text/css" />
    <link href="{{ url('/') }}/assets/css/demo1/skins/brand/dark.css" rel="stylesheet" type="text/css" />
    <link href="{{ url('/') }}/assets/css/demo1/skins/aside/dark.css" rel="stylesheet" type="text/css" />
    <link href="{{ url('/') }}/css/custom.css" rel="stylesheet" type="text/css" />

    <!--end::Layout Skins -->
    <link rel="shortcut icon" href="{{ url('/') }}/favicon.ico" />
    <link rel="apple-touch-icon" sizes="180x180" href="{{ url('/') }}/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="{{ url('/') }}/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="{{ url('/') }}/favicon-16x16.png">
    <link rel="manifest" href="{{ url('/') }}/site.webmanifest">
    @stack('css')
    @livewireStyles
</head>

<!-- end::Head -->

<!-- begin::Body -->

<body
    class="kt-quick-panel--right kt-demo-panel--right kt-offcanvas-panel--right kt-header--fixed kt-header-mobile--fixed kt-subheader--fixed kt-subheader--enabled kt-subheader--solid kt-aside--enabled kt-aside--fixed kt-page--loading">

    <!-- begin:: Page -->
    @include('partials.headermobile')
    <div class="kt-grid kt-grid--hor kt-grid--root">
        <div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--ver kt-page">

            @include('partials.aside')


            <div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor kt-wrapper" id="kt_wrapper">

                @include('partials.header')

                <div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor">

                    @include('partials.subheader',['pageTitle' => $pageTitle ?? '', 'pageBreadcrumbs' => $pageBreadcrumbs ?? ['']])

                    <!-- begin:: Content -->
                    <div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content">
                        @if (session('status'))
                        <div class="alert alert-success alert-dismissible fade show" role="alert">
                            {{ session('status') }}
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        @endif
                        @yield('content')
                    </div>

                    <!-- end:: Content -->
                </div>

                <!-- begin:: Footer -->
                <div class="kt-footer kt-grid__item kt-grid kt-grid--desktop kt-grid--ver-desktop" id="kt_footer">
                    <div class="kt-footer__copyright">
                        2020&nbsp;&copy;&nbsp;<a href="{{ route('dashboard') }}" target="_blank"
                            class="kt-link">iWorking</a>
                    </div>
                    <div class="kt-footer__menu">
                        <a href="#" target="_blank"
                            class="kt-footer__menu-link kt-link">Varios</a>
                    </div>
                </div>

                <!-- end:: Footer -->
            </div>
        </div>
    </div>

    <!-- end:: Page -->



    <!-- begin::Scrolltop -->
    <div id="kt_scrolltop" class="kt-scrolltop">
        <i class="fa fa-arrow-up"></i>
    </div>

    <!-- end::Scrolltop -->



    @stack('modals')
    <!-- begin::Global Config(global config for global JS sciprts) -->
    <script>
        var KTAppOptions = {
				"colors": {
					"state": {
						"brand": "#5d78ff",
						"dark": "#282a3c",
						"light": "#ffffff",
						"primary": "#5867dd",
						"success": "#34bfa3",
						"info": "#36a3f7",
						"warning": "#ffb822",
						"danger": "#fd3995"
					},
					"base": {
						"label": ["#c5cbe3", "#a1a8c3", "#3d4465", "#3e4466"],
						"shape": ["#f0f3ff", "#d9dffa", "#afb4d4", "#646c9a"]
					}
				}
			};
    </script>

    <!-- end::Global Config -->

    <!--begin:: Global Mandatory Vendors -->
    <script src="{{ config('custom.iworking_public_bucket') }}/assets/vendors/general/jquery/dist/jquery.js" type="text/javascript"></script>
    <script src="{{ config('custom.iworking_public_bucket') }}/assets/vendors/general/popper.js/dist/umd/popper.js" type="text/javascript"></script>
    <script src="{{ config('custom.iworking_public_bucket') }}/assets/vendors/general/bootstrap/dist/js/bootstrap.min.js" type="text/javascript">
    </script>
    <script src="{{ config('custom.iworking_public_bucket') }}/assets/vendors/general/sticky-js/dist/sticky.min.js" type="text/javascript"></script>
    <script src="{{ config('custom.iworking_public_bucket') }}/assets/vendors/general/perfect-scrollbar/dist/perfect-scrollbar.js"
        type="text/javascript"></script>
        <script src="{{ config('custom.iworking_public_bucket') }}/assets/vendors/general/js-cookie/src/js.cookie.js" type="text/javascript"></script>
    <script src="{{ config('custom.iworking_public_bucket') }}/assets/js/clean.bundle.js" type="text/javascript"></script>


    <!--end::Global Theme Bundle -->


    @stack('js')
    @livewireScripts
    <!--end::Page Scripts -->
</body>

<!-- end::Body -->

</html>
