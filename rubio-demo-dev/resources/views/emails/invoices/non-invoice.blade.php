@component('mail::message')
# {{ $title }}

{!! $bodyTop !!}

{{ $bodyDetails }}

{{ $bodyBottom }}

{!! $thanks ?? '' !!}
@endcomponent
