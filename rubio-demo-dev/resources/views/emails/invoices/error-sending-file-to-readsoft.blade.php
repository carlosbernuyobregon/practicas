@component('mail::message')
# {{ $title }}

{{ $body }}

{{ $bodyData }}

{!! $thanks ?? '' !!}
@endcomponent
