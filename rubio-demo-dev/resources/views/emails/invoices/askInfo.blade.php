<div>
    Apreciado/a colaborador,
    <br>
    <br>
    Se ha recibido la factura nº {{ $invoice->invoice_number }} del proveedor {{ $invoice->provider_name }} de fecha
    {{ Carbon\Carbon::parse($invoice->date)->format('d-m-Y') }} por importe de {{ $invoice->total }} € y necesitamos la validación de la misma por su parte.
    <br>
    <br>
    Para ello deberá crear y/o verificar el cierre de la orden de Compras en el circuito de Lotus Notes para que desde el departamento de administración de Rubió podamos dar el visto bueno al pago de la misma.
    Les recordamos la obligatoriedad de que todas las peticiones a proveedores deben de llevar asociada la correspondiente petición de compras y su aprobación formal.
    <br>
    <br>
    Una vez realizado y verificado por su parte por favor responda mediante e-mail a administracion@labrubio.com
    <br>
    <br>
    PD: En anexo acompañamos copia de la factura
    <br>
    <br>
    Atentamente
    <br>
    <br>
    Rubió
    <br>
    <br>
    Administración
</div>
