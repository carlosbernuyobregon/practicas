Estimado Usuario,
<br>
Su pedido {{ $order->order_number }} tiene un nuevo estado: {{ trans('backend.orders.status.'.$order->status) }}.
<br>
Un saludo,
