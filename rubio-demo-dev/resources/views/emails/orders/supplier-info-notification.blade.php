@if(!$providerEmail)
    <h3>
        Este pedido no se ha enviado a ningún proveedor, dado que no existe email asociado.
    </h3>
@endif
<p>
    Estimado proveedor,
</p>
<p>
    Adjuntamos pedido para su tramitación.
</p>
<p>
    Por favor, ante cualquier duda, o en caso de no poder cumplir con alguno de los requisitos (producto, cantidad, fecha de entrega), contacte con la persona solicitante (en copia).
</p>
<p>
    <i>
        IMPORTANTE: La factura deberá indicar nuestro nº de pedido para que pueda ser tramitada.
        Este es un mensaje generado automáticamente. Por favor, no responda a este correo.
    </i>
</p>

