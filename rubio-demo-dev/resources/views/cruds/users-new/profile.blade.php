@extends('layouts.app', [
    'pageTitle' => 'User Profile',
    'pageBreadcrumbs' => [
        'My Profile'
    ]
])

@section('content')
    @livewire('cruds.users.profile', ['userId' => auth()->id()])
@endsection