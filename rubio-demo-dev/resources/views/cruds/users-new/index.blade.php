@extends('layouts.app', [
    'pageTitle' => 'Users list',
    'pageBreadcrumbs' => [
        'Masters',
        'Users',
        'List'
    ]
])

@section('content')
    @livewire('cruds.users.user')
@endsection