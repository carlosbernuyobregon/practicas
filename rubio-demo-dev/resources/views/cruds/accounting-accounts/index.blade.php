@extends('layouts.app', [
    'pageTitle' => 'Accounting accounts list',
    'pageBreadcrumbs' => [
        'Masters',
        'Accounting accounts',
        'List'
    ]
])

@section('content')
    @livewire('cruds.accounting-accounts.accounting-account')
@endsection