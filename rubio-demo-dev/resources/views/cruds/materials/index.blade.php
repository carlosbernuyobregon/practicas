@extends('layouts.app', [
    'pageTitle' => 'Materials list',
    'pageBreadcrumbs' => [
        'Masters',
        'Materials',
        'List'
    ]
])

@section('content')
    @livewire('cruds.materials.material')
@endsection