@extends('layouts.app', [
'pageTitle' => __('backend.crud.provider-payment-condition.pageTitle'),
'pageBreadcrumbs' => [
__('backend.crud.pageBreadcrumbs.masters'),
__('backend.crud.provider-payment-condition.title'),
__('backend.crud.pageBreadcrumbs.list'),
]
])

@section('content')
@livewire('cruds.provider-payment-conditions.provider-payment-condition')
@endsection