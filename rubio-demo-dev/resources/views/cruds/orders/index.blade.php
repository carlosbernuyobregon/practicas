@extends('layouts.app', [
    'pageTitle' => 'Orders list',
    'pageBreadcrumbs' => [
        'Masters',
        'Orders',
        'List'
    ]
])

@section('content')
    @livewire('cruds.orders.order')
@endsection