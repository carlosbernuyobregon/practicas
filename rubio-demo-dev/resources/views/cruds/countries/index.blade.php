@extends('layouts.app', [
    'pageTitle' => 'Countries list',
    'pageBreadcrumbs' => [
        'Masters',
        'Countries',
        'List'
    ]
])

@section('content')
    @livewire('cruds.countries.country')
@endsection
