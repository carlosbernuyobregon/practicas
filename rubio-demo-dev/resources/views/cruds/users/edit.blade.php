@extends('layouts.app', [
    'pageTitle' => 'Users form',
    'pageBreadcrumbs' => [
        'Masters',
        'Users',
        'Edit form'
    ]
])

@section('content')
<div class="kt-portlet kt-portlet--mobile">
    <div class="kt-portlet__head kt-portlet__head--lg">
        <div class="kt-portlet__head-label">
            <span class="kt-portlet__head-icon">
                <i class="kt-font-brand flaticon2-line-chart"></i>
            </span>
            <h3 class="kt-portlet__head-title text-uppercase">
                @lang('backend.crud.user.index.title')
            </h3>
        </div>
        <div class="kt-portlet__head-toolbar">
            @include('partials.headerActions')
        </div>
    </div>
    <div class="kt-portlet__body">
        @include('cruds.users.partials.form')
    </div>
</div>
@endsection

