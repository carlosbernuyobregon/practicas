@extends('layouts.app', [
    'pageTitle' => 'Users list',
    'pageBreadcrumbs' => [
        'Masters',
        'Users',
        'List'
    ]
])

@section('content')
<div class="kt-portlet kt-portlet--mobile">
    <div class="kt-portlet__head kt-portlet__head--lg">
        <div class="kt-portlet__head-label">
            <span class="kt-portlet__head-icon">
                <i class="kt-font-brand flaticon2-line-chart"></i>
            </span>
            <h3 class="kt-portlet__head-title text-uppercase">
                @lang('backend.crud.user.index.title')
            </h3>
        </div>
        <div class="kt-portlet__head-toolbar">
            <div class="kt-portlet__head-wrapper">
                <div class="kt-portlet__head-actions">
                    <a href="{{ route('user.create')}}" class="btn btn-brand btn-elevate btn-icon-sm">
                        <i class="fas fa-plus"></i>
                        New User
                    </a>
                </div>
            </div>
        </div>
    </div>
    <div class="kt-portlet__body">
        @include('cruds.users.partials.list')
    </div>
</div>
@endsection

