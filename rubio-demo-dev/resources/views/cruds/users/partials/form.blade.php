<div class="row">
    <div class="col-12">
        <form method="post" action="{{ isset($user) ? route('user.update', $user->id) : route('user.store') }}"
            autocomplete="off">
            @csrf
            @if (isset($user))
            @method('put')
            @endif
            <h6 class=" mb-4">@lang('backend.crud.user.information')</h6>
            <div class="pl-lg-4">
                <div class="row">
                    <div class="col-6">
                        <div class="form-group{{ $errors->has('first_name') ? ' has-danger' : '' }}">
                            <label class="form-control-label"
                                for="input-first_name">@lang('backend.crud.user.edit.name')</label>
                            <input type="text" name="name" id="input-name"
                                class="form-control form-control-alternative{{ $errors->has('name') ? ' is-invalid' : '' }}"
                                placeholder="{{ __('Name') }}" value="{{ old('name', (isset($user) ? $user->name : '')) }}"
                                required autofocus>

                            @if ($errors->has('name'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('name') }}</strong>
                            </span>
                            @endif
                        </div>
                        <div class="form-group{{ $errors->has('first_name') ? ' has-danger' : '' }}">
                            <label class="form-control-label"
                                for="input-first_name">@lang('backend.crud.user.edit.first_name')</label>
                            <input type="text" name="first_name" id="input-first_name"
                                class="form-control form-control-alternative{{ $errors->has('first_name') ? ' is-invalid' : '' }}"
                                placeholder="{{ __('first_name') }}"
                                value="{{ old('first_name', (isset($user) ? $user->first_name : '')) }}" required>

                            @if ($errors->has('first_name'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('first_name') }}</strong>
                            </span>
                            @endif
                        </div>
                        <div class="form-group{{ $errors->has('last_name') ? ' has-danger' : '' }}">
                            <label class="form-control-label"
                                for="input-last_name">@lang('backend.crud.user.edit.last_name')</label>
                            <input type="text" name="last_name" id="input-last_name"
                                class="form-control form-control-alternative{{ $errors->has('last_name') ? ' is-invalid' : '' }}"
                                placeholder="{{ __('last_name') }}"
                                value="{{ old('last_name', (isset($user) ? $user->last_name : '')) }}" required>

                            @if ($errors->has('last_name'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('last_name') }}</strong>
                            </span>
                            @endif
                        </div>
                        <div class="form-group{{ $errors->has('email') ? ' has-danger' : '' }}">
                            <label class="form-control-label" for="input-email">@lang('backend.crud.user.edit.email')</label>
                            <input type="email" name="email" id="input-email"
                                class="form-control form-control-alternative{{ $errors->has('email') ? ' is-invalid' : '' }}"
                                placeholder="{{ __('Email') }}" value="{{ old('email', (isset($user) ? $user->email : '')) }}"
                                required>

                            @if ($errors->has('email'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('email') }}</strong>
                            </span>
                            @endif
                        </div>
                        <div class="form-group{{ $errors->has('phone') ? ' has-danger' : '' }}">
                            <label class="form-control-label" for="input-phone">@lang('backend.crud.user.edit.phone')</label>
                            <input type="text" name="phone" id="input-phone"
                                class="form-control form-control-alternative{{ $errors->has('phone') ? ' is-invalid' : '' }}"
                                placeholder="{{ __('phone') }}" value="{{ old('phone', (isset($user) ? $user->phone : '')) }}"
                                required autofocus>

                            @if ($errors->has('phone'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('phone') }}</strong>
                            </span>
                            @endif
                        </div>
                        <div class="form-group{{ $errors->has('company_name') ? ' has-danger' : '' }}">
                            <label class="form-control-label" for="input-company_name">@lang('backend.crud.user.edit.company_name')</label>
                            <input type="text" name="company_name" id="input-company_name"
                                class="form-control form-control-alternative{{ $errors->has('company_name') ? ' is-invalid' : '' }}"
                                placeholder="{{ __('company_name') }}" value="{{ old('company_name', (isset($user) ? $user->company_name : '')) }}"
                                required autofocus>

                            @if ($errors->has('company_name'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('company_name') }}</strong>
                            </span>
                            @endif
                        </div>
                        <div class="form-group{{ $errors->has('company_address') ? ' has-danger' : '' }}">
                            <label class="form-control-label" for="input-company_address">@lang('backend.crud.user.edit.company_address')</label>
                            <input type="text" name="company_address" id="input-company_address"
                                class="form-control form-control-alternative{{ $errors->has('company_address') ? ' is-invalid' : '' }}"
                                placeholder="{{ __('company_address') }}" value="{{ old('company_address', (isset($user) ? $user->company_address : '')) }}"
                                required autofocus>

                            @if ($errors->has('company_address'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('company_address') }}</strong>
                            </span>
                            @endif
                        </div>
                        <div class="form-group{{ $errors->has('company_city') ? ' has-danger' : '' }}">
                            <label class="form-control-label" for="input-company_city">@lang('backend.crud.user.edit.company_city')</label>
                            <input type="text" name="company_city" id="input-company_city"
                                class="form-control form-control-alternative{{ $errors->has('company_city') ? ' is-invalid' : '' }}"
                                placeholder="{{ __('company_city') }}" value="{{ old('company_city', (isset($user) ? $user->company_city : '')) }}"
                                required autofocus>

                            @if ($errors->has('company_city'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('company_city') }}</strong>
                            </span>
                            @endif
                        </div>
                        <div class="form-group{{ $errors->has('company_state') ? ' has-danger' : '' }}">
                            <label class="form-control-label" for="input-company_state">@lang('backend.crud.user.edit.company_state')</label>
                            <input type="text" name="company_state" id="input-company_state"
                                class="form-control form-control-alternative{{ $errors->has('company_state') ? ' is-invalid' : '' }}"
                                placeholder="{{ __('company_state') }}" value="{{ old('company_state', (isset($user) ? $user->company_state : '')) }}"
                                required autofocus>

                            @if ($errors->has('company_state'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('company_state') }}</strong>
                            </span>
                            @endif
                        </div>
                        <div class="form-group{{ $errors->has('company_country') ? ' has-danger' : '' }}">
                            <label class="form-control-label" for="input-company_country">@lang('backend.crud.user.edit.company_country')</label>
                            <input type="text" name="company_country" id="input-company_country"
                                class="form-control form-control-alternative{{ $errors->has('company_country') ? ' is-invalid' : '' }}"
                                placeholder="{{ __('company_country') }}" value="{{ old('company_country', (isset($user) ? $user->company_country : '')) }}"
                                required autofocus>

                            @if ($errors->has('company_country'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('company_country') }}</strong>
                            </span>
                            @endif
                        </div>
                        <div class="form-group{{ $errors->has('company_postal_code') ? ' has-danger' : '' }}">
                            <label class="form-control-label" for="input-company_postal_code">@lang('backend.crud.user.edit.company_postal_code')</label>
                            <input type="text" name="company_postal_code" id="input-company_postal_code"
                                class="form-control form-control-alternative{{ $errors->has('company_postal_code') ? ' is-invalid' : '' }}"
                                placeholder="{{ __('company_postal_code') }}" value="{{ old('company_postal_code', (isset($user) ? $user->company_postal_code : '')) }}"
                                required autofocus>

                            @if ($errors->has('company_postal_code'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('company_postal_code') }}</strong>
                            </span>
                            @endif
                        </div>
                        @if (!isset($user))
                        <div class="form-group{{ $errors->has('password') ? ' has-danger' : '' }}">
                            <label class="form-control-label" for="input-password">{{ __('New Password') }}</label>
                            <input type="password" name="password" id="input-password"
                                class="form-control form-control-alternative{{ $errors->has('password') ? ' is-invalid' : '' }}"
                                placeholder="{{ __('New Password') }}" value="" required>

                            @if ($errors->has('password'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('password') }}</strong>
                            </span>
                            @endif
                        </div>
                        <div class="form-group">
                            <label class="form-control-label"
                                for="input-password-confirmation">{{ __('Confirm New Password') }}</label>
                            <input type="password" name="password_confirmation" id="input-password-confirmation"
                                class="form-control form-control-alternative"
                                placeholder="{{ __('Confirm New Password') }}" value="" required>
                        </div>
                        @endif

                        <hr>

                        <div class="form-group{{ $errors->has('timezone') ? ' has-danger' : '' }}">
                            <label class="form-control-label" for="input-timezone">{{ __('Timezone') }}</label>
                            <select name="timezone" id="input-timezone"
                                class="form-control form-control-alternative{{ $errors->has('timezone') ? ' is-invalid' : '' }}">
                                @foreach (timezone_identifiers_list() as $timezone)
                                <option value="{{ $timezone }}"
                                    {{ (isset($user) ? ((($timezone == old('timezone'))||($timezone == $user->timezone)) ? ' selected' : '') : '') }}>
                                    {{ $timezone }}</option>
                                @endforeach
                            </select>
                            @if ($errors->has('timezone'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('timezone') }}</strong>
                            </span>
                            @endif
                        </div>
                    </div>
                    <div class="col-6">
                        <div class="form-group row justify-content-end">
                            <label class="col-8 col-form-label">@lang('backend.crud.user.edit.roles')</label>
                            <div class="col-8">
                                <div class="kt-checkbox-list">
                                    @foreach ($roles as $role)
                                    <label class="kt-checkbox">
                                    <input type="checkbox" name="roles[{{ $role->id }}]"
                                     value="1"
                                     {{ (isset($user)) ? ($user->hasRole($role->key_value) ? 'checked':'') : '' }}> {{$role->name}}
                                        <span></span>
                                    </label>
                                    @endforeach
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-12">
                        <div class="text-right">
                            <button type="submit" class="btn btn-success mt-4 float-right">{{ __('Save') }}</button>
                        </div>
                    </div>
                </div>
            </div>
        </form>
        <hr>
        @if (isset($user))
        <form method="POST" action="{{ route('user.destroy',$user->id)}}">
            <div class="pl-lg-4">
            {{ csrf_field() }}
            {{ method_field('DELETE') }}
            <div class="row">
                <div class="col-12">
                    <div class="text-right">
                        <input type="submit" class="btn btn-danger  float-right" onclick="return confirm('{{ __("backend.messages.delete-confirmation") }}')" value="{{ __('Delete') }}">
                    </div>
                </div>
            </div>
        </div>
        </form>
        @endif
    </div>
</div>
