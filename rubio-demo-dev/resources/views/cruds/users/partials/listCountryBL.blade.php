@extends('layouts.app', [
    'pageTitle' => 'Users form',
    'pageBreadcrumbs' => [
        'Masters',
        'Users',
        'Business Line - Country relationship'
    ]
])

@section('content')
<div class="kt-portlet kt-portlet--mobile">
    <div class="kt-portlet__head kt-portlet__head--lg">
        <div class="kt-portlet__head-label">
            <span class="kt-portlet__head-icon">
                <i class="kt-font-brand flaticon2-line-chart"></i>
            </span>
            <h3 class="kt-portlet__head-title text-uppercase">
                @lang('backend.crud.user.index.title')
            </h3>
        </div>
        <div class="kt-portlet__head-toolbar">
            @include('partials.headerActions')
        </div>
    </div>
    <div class="kt-portlet__body">
        <!--begin: Datatable -->
<table class="table table-striped- table-bordered table-hover table-checkable" id="users_list_table">
    <thead>
        <tr class="text-uppercase">
            <th>@lang('backend.crud.created_at')</th>
            <th>@lang('backend.crud.user.index.complete-name')</th>
            <th>@lang('backend.crud.user.index.email')</th>
            <th>@lang('backend.crud.user.country-bl.country')</th>
            <th>@lang('backend.crud.user.country-bl.business_line')</th>
            <th>@lang('backend.crud.actions')</th>
        </tr>
    </thead>
    <tbody>
        @foreach ($users as $user)
        <tr>
            <td>{{ auth()->user()->applyDateFormat($user->created_at) ?? '' }}</td>
            <td>{{ $user->user->first_name }} {{ $user->user->last_name }}</td>
            <td>{{ $user->user->email }}</td>
            <td>{{ $user->country->name }}</td>
            <td>{{ $user->businessLine->name }}</td>
            <td nowrap>
                <a href="{{ route('user.business-line-country.edit',$user->id)}}" class="btn btn-sm btn-clean btn-icon btn-icon-md" title="View">
                    <i class="fas fa-edit"></i>
                </a>
            </td>
        </tr>
        @endforeach
    </tbody>
</table>
    </div>
</div>
@endsection





<!--end: Datatable -->
@push('css')
<!--begin::Page Vendors Styles(used by this page) -->
<link href="{{ url('/')}}/assets/vendors/custom/datatables/datatables.bundle.css" rel="stylesheet" type="text/css" />
<!--end::Page Vendors Styles -->
@endpush
@push('js')
<!--begin::Page Vendors(used by this page) -->
<script src="{{ url('/')}}/assets/js/demo1/pages/crud/datatables/basic/headers.min.js" type="text/javascript"></script>
<script src="{{ url('/')}}/assets/vendors/custom/datatables/datatables.bundle.js" type="text/javascript"></script>


<!--end::Page Vendors -->

<!--begin::Page Scripts(used by this page) -->
<script>






jQuery(document).ready(function() {
    $('#users_list_table').DataTable();
});
</script>
@endpush
