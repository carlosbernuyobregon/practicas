@extends('layouts.app', [
    'pageTitle' => 'Users form',
    'pageBreadcrumbs' => [
        'Masters',
        'Users',
        'Edit form'
    ]
])

@section('content')
<div class="kt-portlet kt-portlet--mobile">
    <div class="kt-portlet__head kt-portlet__head--lg">
        <div class="kt-portlet__head-label">
            <span class="kt-portlet__head-icon">
                <i class="kt-font-brand flaticon2-line-chart"></i>
            </span>
            <h3 class="kt-portlet__head-title text-uppercase">
                @lang('backend.crud.user.index.title')
            </h3>
        </div>
        <div class="kt-portlet__head-toolbar">
            @include('partials.headerActions')
        </div>
    </div>
    <div class="kt-portlet__body">
        div class="row">
    <div class="col-12">
        <form method="post" action="{{ isset($user) ? route('user.update', $user->id) : route('user.store') }}"
            autocomplete="off">
            @csrf
            @if (isset($user))
            @method('put')
            @endif
            <h6 class=" mb-4">@lang('backend.crud.user.information')</h6>
            <div class="pl-lg-4">
                <div class="row">
                    <div class="col-6">
                        <div class="form-group{{ $errors->has('user_id') ? ' has-danger' : '' }}">
                            <label class="form-control-label" for="input-user_id">{{ __('user_id') }}</label>
                            <select name="user_id" id="input-user_id"
                                class="form-control form-control-alternative{{ $errors->has('user_id') ? ' is-invalid' : '' }}">
                                @foreach ($users as $user)
                                <option value="{{ $user->id }}"
                                    {{ (isset($blCountryId) ? ((($user->id == old('user_id'))||($user->id == $blCountryId->user_id)) ? ' selected' : '') : '') }}>
                                    {{ $user->id }}</option>
                                @endforeach
                            </select>
                            @if ($errors->has('user_id'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('user_id') }}</strong>
                            </span>
                            @endif
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-12">
                        <div class="text-right">
                            <button type="submit" class="btn btn-success mt-4 float-right">{{ __('Save') }}</button>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>
    </div>
</div>
@endsection

