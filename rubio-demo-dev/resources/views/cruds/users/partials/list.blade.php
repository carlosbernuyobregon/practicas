<!--begin: Datatable -->
<table class="table table-striped- table-bordered table-hover table-checkable" id="users_list_table">
    <thead>
        <tr class="text-uppercase">
            <th>@lang('backend.crud.created_at')</th>
            <th>@lang('backend.crud.user.index.complete-name')</th>
            <th>@lang('backend.crud.user.index.roles')</th>
            <th>@lang('backend.crud.user.index.email')</th>
            <th>@lang('backend.crud.user.index.phone')</th>
            <th>@lang('backend.crud.actions')</th>
        </tr>
    </thead>
    <tbody>
        @foreach ($users as $user)
        <tr>
            <td>{{ $user->created_at->format('d/m/Y H:i:s') }}</td>
            <td>{{ $user->first_name }} {{ $user->last_name }}</td>
            <td>
                @foreach ($user->roles as $role)
                <span class="kt-badge kt-badge--brand kt-badge--inline kt-badge--pill">{{ $role->name }}</span>
                @endforeach
            </td>
            <td>{{ $user->email }}</td>
            <td>{{ $user->phone }}</td>
            <td nowrap>
                <a href="{{ route('user.edit',$user->id)}}" class="btn btn-sm btn-clean btn-icon btn-icon-md" title="View">
                    <i class="fas fa-edit"></i>
                </a>
            </td>
        </tr>
        @endforeach
    </tbody>
</table>

<!--end: Datatable -->
@push('css')
<!--begin::Page Vendors Styles(used by this page) -->
<link href="{{ url('/')}}/assets/vendors/custom/datatables/datatables.bundle.css" rel="stylesheet" type="text/css" />
<!--end::Page Vendors Styles -->
@endpush
@push('js')
<!--begin::Page Vendors(used by this page) -->
<script src="{{ url('/')}}/assets/js/demo1/pages/crud/datatables/basic/headers.min.js" type="text/javascript"></script>
<script src="{{ url('/')}}/assets/vendors/custom/datatables/datatables.bundle.js" type="text/javascript"></script>


<!--end::Page Vendors -->

<!--begin::Page Scripts(used by this page) -->
<script>






    "use strict";
var KTDatatablesBasicBasic = function() {

	var initTable1 = function() {
		var table = $('#users_list_table');

		// begin first table
		table.DataTable({
			responsive: true,

			// DOM Layout settings
			dom: `<'row'<'col-sm-12'tr>>
			<'row'<'col-sm-12 col-md-5'i><'col-sm-12 col-md-7 dataTables_pager'lp>>`,

			lengthMenu: [5, 10, 25, 50],

			pageLength: 10,

			language: {
				'lengthMenu': 'Display _MENU_',
			},
            filter: true,
			// Order settings
			order: [[0, 'desc']],
		});
	};

	return {

		//main function to initiate the module
		init: function() {
			initTable1();
		},

	};

}();

jQuery(document).ready(function() {
	//KTDatatablesBasicBasic.init();
    $('#users_list_table').DataTable({
        dom: 'Bfrtip',
        buttons: [
            'copyHtml5',
            'excelHtml5',
            //'csvHtml5',
            'pdfHtml5',
            'pageLength'
        ],
        lengthMenu: [
            [ 10, 25, 50, -1 ],
            [ '10 rows', '25 rows', '50 rows', 'Show all' ]
        ]
    });
});
</script>
@endpush
