@extends('layouts.app', [
    'pageTitle' => 'Budgets list',
    'pageBreadcrumbs' => [
        'Masters',
        'Budgets',
        'List'
    ]
])

@section('content')
    @livewire('cruds.budgets.budget')
@endsection
