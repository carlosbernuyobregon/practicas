<div class="row">
    <div class="col-12">
        <form method="post" action="{{ isset($role) ? route('role.update', $role->id) : route('role.store') }}"
            autocomplete="off">
            @csrf
            @if (isset($role))
            @method('put')
            @endif
            <div class="row">
                <div class="col-12 col-md-6">
                    <h6 class=" mb-4">@lang('backend.crud.role.information')</h6>
                    <div class="pl-lg-4">
                        <div class="form-group{{ $errors->has('key_value') ? ' has-danger' : '' }}">
                            <label class="form-control-label"
                                for="input-key_value">@lang('backend.crud.role.edit.key_value')</label>
                            <input type="text" name="key_value" id="input-key_value"
                                class="form-control form-control-alternative{{ $errors->has('key_value') ? ' is-invalid' : '' }}"
                                placeholder="@lang('backend.crud.role.edit.key')"
                                value="{{ old('key_value', (isset($role) ? $role->key_value : '')) }}" required autofocus>

                            @if ($errors->has('key_value'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('key_value') }}</strong>
                            </span>
                            @endif
                        </div>

                        <div class="navbar navbar-light bg-faded">
                            <ul class="nav nav-tabs">
                                @foreach (config('translatable.locales') as $locale)
                                <li class="nav-item">
                                    <a class="nav-link{{ ($locale=='en') ? ' active show' : ''}}" data-toggle="tab"
                                        href="#description-{{$locale}}">{{ $locale }}</a>
                                </li>
                                @endforeach
                            </ul>
                        </div>
                        <div class="tab-content">
                            @foreach (config('translatable.locales') as $locale)
                            <div class="tab-pane{{ ($locale=='en') ? ' active' : ''}}" id="description-{{$locale}}">
                                <div class="form-group{{ $errors->has('name') ? ' has-danger' : '' }}">
                                    <label class="form-control-label"
                                        for="input-name">@lang('backend.crud.role.edit.name')</label>
                                    <input type="text" name="{{ $locale }}[name]" id="input-name"
                                        class="form-control form-control-alternative{{ $errors->has('name') ? ' is-invalid' : '' }}"
                                        placeholder="@lang('backend.crud.role.edit.name')"
                                        value="{{ old($locale.'.name', (isset($role) ? $role->translate($locale)->name : '')) }}"
                                        {{($locale=='en') ? 'required': ''}}>

                                    @if ($errors->has('name'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                    @endif
                                </div>
                                <div class="form-group{{ $errors->has('description') ? ' has-danger' : '' }}">
                                    <label class="form-control-label"
                                        for="input-description">@lang('backend.crud.role.edit.description')</label>
                                    <textarea class="form-control" id="description-{{$locale}}" rows="3"
                                        placeholder="@lang('backend.crud.role.edit.description')"
                                        name="{{ $locale }}[description]">{{ old($locale.'.description', (isset($role) ? $role->translate($locale)->description : '')) }}</textarea>
                                </div>

                            </div>
                            @endforeach
                        </div>
                    </div>
                    <div class="text-right">
                        <button type="submit" class="btn btn-success mt-4">{{ __('Save') }}</button>
                    </div>
                </div>
            </div>

        </form>
        <hr>
        @if (isset($role))
        <form method="POST" action="{{ route('role.destroy',$role->id)}}">
            <div class="pl-lg-4">
            {{ csrf_field() }}
            {{ method_field('DELETE') }}
            <div class="row">
                <div class="col-12 col-md-6">
                    <div class="text-right">
                        <input type="submit" class="btn btn-danger  float-right" onclick="return confirm('{{ __("backend.messages.delete-confirmation") }}')" value="{{ __('Delete') }}">
                    </div>
                </div>
            </div>
        </div>
        </form>
        @endif
    </div>
</div>
