@extends('layouts.app', [
    'pageTitle' => 'Roles list',
    'pageBreadcrumbs' => [
        'Masters',
        'Roles',
        'List'
    ]
])

@section('content')
<div class="kt-portlet kt-portlet--mobile">
    <div class="kt-portlet__head kt-portlet__head--lg">
        <div class="kt-portlet__head-label">
            <span class="kt-portlet__head-icon">
                <i class="kt-font-brand flaticon2-line-chart"></i>
            </span>
            <h3 class="kt-portlet__head-title text-uppercase">
                @lang('backend.crud.role.index.title')
            </h3>
        </div>
        <div class="kt-portlet__head-toolbar">
            <div class="kt-portlet__head-wrapper">
                <div class="kt-portlet__head-actions">
                    <a href="{{ route('role.create') }}" class="btn btn-brand btn-elevate btn-icon-sm">
                        <i class="fas fa-plus"></i>
                        New Role
                    </a>
                </div>
            </div>
        </div>
    </div>
    <div class="kt-portlet__body">
        @include('cruds.roles.partials.list')
    </div>
</div>
@endsection

