@extends('layouts.app', [
    'pageTitle' => 'Assets list',
    'pageBreadcrumbs' => [
        'Masters',
        'Assets',
        'List'
    ]
])

@section('content')
    @livewire('cruds.assets.asset')
@endsection