@extends('layouts.app', [
    'pageTitle' => 'Roles list',
    'pageBreadcrumbs' => [
        'Masters',
        'Roles',
        'List'
    ]
])

@section('content')
    @livewire('cruds.roles.role')
@endsection

