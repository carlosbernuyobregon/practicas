@extends('layouts.app', [
    'pageTitle' => 'Suppliers list',
    'pageBreadcrumbs' => [
        'Masters',
        'Suppliers',
        'List'
    ]
])

@section('content')
    @livewire('cruds.suppliers.supplier')
@endsection