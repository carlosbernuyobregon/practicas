@extends('layouts.app', [
    'pageTitle' => 'Tax types list',
    'pageBreadcrumbs' => [
        'Masters',
        'Tax types',
        'List'
    ]
])

@section('content')
    @livewire('cruds.tax-types.tax-type')
@endsection
