@extends('layouts.app', [
    'pageTitle' => 'Cost centers list',
    'pageBreadcrumbs' => [
        'Masters',
        'Cost cecnters',
        'List'
    ]
])

@section('content')
    @livewire('cruds.cost-centers.cost-center')
@endsection