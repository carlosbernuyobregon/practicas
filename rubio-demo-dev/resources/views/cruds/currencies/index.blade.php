@extends('layouts.app', [
    'pageTitle' => 'Currencies list',
    'pageBreadcrumbs' => [
        'Masters',
        'Currencies',
        'List'
    ]
])

@section('content')
    @livewire('cruds.currencies.currency')
@endsection
