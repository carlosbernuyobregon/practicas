@extends('layouts.app', [
    'pageTitle' => __('backend.crud.provider-payment-type.page_title'),
    'pageBreadcrumbs' => [
        __('backend.crud.masters'),
        __('backend.crud.provider-payment-type.title'),
        __('backend.crud.list')
    ]
])

@section('content')
    @livewire('cruds.provider-payment-types.provider-payment-type')
@endsection