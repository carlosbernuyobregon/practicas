<!-- begin:: Header -->
<div id="kt_header" class="kt-header kt-grid__item  kt-header--fixed ">

    @if (auth()->user())
        <!-- begin:: Header Menu -->
        <button class="kt-header-menu-wrapper-close" id="kt_header_menu_mobile_close_btn"><i
                class="fas fa-close"></i></button>
        <div class="kt-header-menu-wrapper" id="kt_header_menu_wrapper">
            <div class="p-2">
                <a href="{{ route('dashboard')}}">
                    <img alt="{{ config('app.name') }}" src="{{ asset('img/logo_rubio.jpg') }}" class="img-fluid"
                    style="max-height: 50px;" />
                </a>
            </div>

            <div id="kt_header_menu" class="kt-header-menu kt-header-menu-mobile  kt-header-menu--layout-default ">
            </div>
        </div>

        <!-- end:: Header Menu -->

        <!-- begin:: Header Topbar -->
        <div class="kt-header__topbar">


            <!--begin: Language bar -->
            <div class="kt-header__topbar-item kt-header__topbar-item--langs">
                <div class="kt-header__topbar-wrapper" data-toggle="dropdown" data-offset="10px,0px">
                    <span class="kt-header__topbar-icon">
                        {{ App::getLocale() }}
                    </span>
                </div>
                <div
                    class="dropdown-menu dropdown-menu-fit dropdown-menu-right dropdown-menu-anim dropdown-menu-top-unround">
                    <ul class="kt-nav kt-margin-t-10 kt-margin-b-10">
                        <li class="kt-nav__item kt-nav__item--active">
                        <a href="{{ route('utils.lang.set','en') }}" class="kt-nav__link">
                                <span class="kt-nav__link-text">@lang('backend.langs.en')</span>
                            </a>
                        </li>
                        <li class="kt-nav__item">
                            <a href="{{ route('utils.lang.set','es') }}" class="kt-nav__link">

                                <span class="kt-nav__link-text">@lang('backend.langs.es')</span>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>

            <!--end: Language bar -->

                <!--begin: User Bar -->
                <div class="kt-header__topbar-item kt-header__topbar-item--user">
                    <div class="kt-header__topbar-wrapper" data-toggle="dropdown" data-offset="0px,0px">
                        <div class="kt-header__topbar-user">
                            <span class="kt-header__topbar-username kt-hidden-mobile">@lang('backend.hi'), {{ auth()->user()->first_name}}</span>

                            <!--use below badge element instead the user avatar to display username's first letter(remove kt-hidden class to display it) -->
                            <span
                                class="kt-badge kt-badge--username kt-badge--unified-success kt-badge--lg kt-badge--rounded kt-badge--bold">
                                {{ strtoupper(auth()->user()->first_name[0] ?? '') }}{{ strtoupper(auth()->user()->last_name[0] ?? '') }}
                            </span>
                        </div>
                    </div>
                    <div
                        class="dropdown-menu dropdown-menu-fit dropdown-menu-right dropdown-menu-anim dropdown-menu-top-unround dropdown-menu-xl">

                        <!--begin: Head -->
                        <div class="kt-user-card kt-user-card--skin-dark kt-notification-item-padding-x"
                            style="background-image: url({{ url('/')}}/assets/media/misc/bg-1.jpg)">
                            <div class="kt-user-card__avatar">

                                <!--use below badge element instead the user avatar to display username's first letter(remove kt-hidden class to display it) -->
                                <span class="kt-badge kt-badge--lg kt-badge--rounded kt-badge--bold kt-font-success">
                                    {{ strtoupper(auth()->user()->first_name[0] ?? '') }}{{ strtoupper(auth()->user()->last_name[0] ?? '') }}
                                </span>
                            </div>
                            <div class="kt-user-card__name">
                                {{ auth()->user()->first_name }} {{ auth()->user()->last_name}}
                            </div>
                            <!--<div class="kt-user-card__badge">
                                <span class="btn btn-success btn-sm btn-bold btn-font-md">23 messages</span>
                            </div>-->
                        </div>

                        <!--end: Head -->

                        <!--begin: Navigation -->

                        <div class="kt-notification">
                            <a href="{{ route('profile')}}" class="kt-notification__item">
                                <div class="kt-notification__item-icon">
                                    <i class="fas fa-user kt-font-success"></i>
                                </div>
                                <div class="kt-notification__item-details">
                                    <div class="kt-notification__item-title kt-font-bold">
                                        My Profile
                                    </div>
                                    <div class="kt-notification__item-time">
                                        Account settings and more
                                    </div>
                                </div>
                            </a>
                            <!--<a href="#" class="kt-notification__item">
                                <div class="kt-notification__item-icon">
                                    <i class="flaticon2-mail kt-font-warning"></i>
                                </div>
                                <div class="kt-notification__item-details">
                                    <div class="kt-notification__item-title kt-font-bold">
                                        My Messages
                                    </div>
                                    <div class="kt-notification__item-time">
                                        Inbox and tasks
                                    </div>
                                </div>
                            </a>
                            <a href="#" class="kt-notification__item">
                                <div class="kt-notification__item-icon">
                                    <i class="flaticon2-rocket-1 kt-font-danger"></i>
                                </div>
                                <div class="kt-notification__item-details">
                                    <div class="kt-notification__item-title kt-font-bold">
                                        My Activities
                                    </div>
                                    <div class="kt-notification__item-time">
                                        Logs and notifications
                                    </div>
                                </div>
                            </a>
                            <a href="#" class="kt-notification__item">
                                <div class="kt-notification__item-icon">
                                    <i class="flaticon2-hourglass kt-font-brand"></i>
                                </div>
                                <div class="kt-notification__item-details">
                                    <div class="kt-notification__item-title kt-font-bold">
                                        My Tasks
                                    </div>
                                    <div class="kt-notification__item-time">
                                        latest tasks and projects
                                    </div>
                                </div>
                            </a>
                            <a href="#" class="kt-notification__item">
                                <div class="kt-notification__item-icon">
                                    <i class="flaticon2-cardiogram kt-font-warning"></i>
                                </div>
                                <div class="kt-notification__item-details">
                                    <div class="kt-notification__item-title kt-font-bold">
                                        Billing
                                    </div>
                                    <div class="kt-notification__item-time">
                                        billing & statements <span
                                            class="kt-badge kt-badge--danger kt-badge--inline kt-badge--pill kt-badge--rounded">2
                                            pending</span>
                                    </div>
                                </div>
                            </a>-->
                            <div class="kt-notification__custom kt-space-between">
                                <a href="{{ route('logout') }}" class="btn btn-label btn-label-brand btn-sm btn-bold"
                                onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                                {{ __('Logout') }}
                                </a>
                                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                    @csrf
                                </form>

                            </div>
                        </div>

                        <!--end: Navigation -->
                    </div>
                </div>
                <!--end: User Bar -->

        </div>
    @endif

    <!-- end:: Header Topbar -->
</div>

<!-- end:: Header -->
