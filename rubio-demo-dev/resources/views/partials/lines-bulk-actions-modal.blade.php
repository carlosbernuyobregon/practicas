<div wire:ignore.self class="modal fade" id="bulkModal" tabindex="-1" role="dialog" aria-labelledby="bulkModalLabel"
    aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">@lang('backend.forms.lines.bulk.actions.title')</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">
                <p>@lang('backend.forms.lines.bulk.actions.text')</p>
                <select wire:model="chosenBulkOption" class="form-control">
                    <option value=""> - </option>
                    @foreach ($bulkOptions as $bulkOption)
                        <option value="{{ $bulkOption }}">@lang('backend.forms.lines.bulk.actions.' . $bulkOption)</option>
                    @endforeach
                </select>
                @if ($this->taxTypes)
                    <p style="margin-top: 10px">@lang('backend.forms.lines.bulk.actions.select-tax-type-text')</p>
                    <select wire:model="chosenBulkCustomValue" class="form-control">
                        <option value=""> - </option>
                        @foreach ($this->taxTypes as $taxType)
                            <option value="{{ $taxType->id }}">{{ $taxType->type }} ({{ $taxType->description }})</option>
                        @endforeach
                    </select>
                @elseif ($this->genericInput)
                    <p style="margin-top: 10px">{{ $this->genericInputText }}</p>
                    <input wire:model="chosenBulkCustomValue" type="text" class="form-control" placeholder="{{ $this->genericInputPlaceHolderText }}"/>
                @endif
            </div>
            @if (!$this->confirmAction)
                <div class="modal-footer">
                    <button wire:click="confirmBulkAction()" type="button" class="btn btn-outline-danger" id="btn_bulk_confirm" {{ $bulkConfirmDisabled ? 'disabled' : null }}>
                        @lang('backend.forms.lines.bulk.actions.action')
                    </button>
                    <button wire:click="reloadBulkData()" type="button" class="btn btn-secondary" data-dismiss="modal">@lang('backend.forms.lines.bulk.actions.close')</button>
                </div>
            @else
                <div class="modal-footer">
                    <p style="margin-top: 10px; display: block">@lang('backend.forms.lines.bulk.actions.confirmation-text')</p>
                    <button wire:click="doBulkActions()" type="button" class="btn btn-outline-warning" id="btn_bulk">
                        @lang('backend.forms.lines.bulk.actions.accept')
                    </button>
                    <button wire:click="reloadBulkData()" type="button" class="btn btn-secondary" data-dismiss="modal">@lang('backend.forms.lines.bulk.actions.close')</button>
                </div>
            @endif
        </div>
    </div>
</div>
