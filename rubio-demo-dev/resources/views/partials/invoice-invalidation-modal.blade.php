<div wire:ignore.self class="modal fade" id="invalidationModal" tabindex="-1" role="dialog" aria-labelledby="invalidationModalLabel"
    aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">@lang('backend.forms.invoice.invalidation.title')</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">
                <p>@lang('backend.forms.invoice.invalidation.text')</p>
                <select wire:model="chosenInvalidationReason" class="form-control">
                    <option value=""> - </option>
                    @foreach ($invalidationReasons as $invalidationReason)
                        <option value="{{ $invalidationReason }}">@lang('backend.forms.invoice.invalidation.reasons.' . $invalidationReason)</option>
                    @endforeach
                </select>
                <p style="margin-top: 10px">@lang('backend.forms.invoice.invalidation.free-reason')</p>
                <textarea wire:model="textInvalidationReason" class="form-control {{ !$this->textInvalidationReasonRequired ?: 'is-invalid text-danger' }}"
                          placeholder="@lang('backend.forms.invoice.invalidation.free-reason-placeholder')"
                          rows="3" cols="10"></textarea>
            </div>
            @if (!$this->confirmInvalidation)
                <div class="modal-footer">
                    <button wire:click="confirmInvalidation()" type="button" class="btn btn-outline-danger" id="btn_invalidation_confirm" {{ $invalidationConfirmDisabled ? 'disabled' : null }}>
                        @lang('backend.forms.invoice.invalidation.actions.invalidate')
                    </button>
                    <button wire:click="reloadInvalidationData()" type="button" class="btn btn-secondary" data-dismiss="modal">@lang('backend.forms.invoice.invalidation.actions.close')</button>
                </div>
            @else
                <div class="modal-footer">
                    <p style="margin-top: 10px; display: block">@lang('backend.forms.invoice.invalidation.actions.confirmation-text')</p>
                    <button wire:click="invalidateInvoice()" type="button" class="btn btn-outline-warning" id="btn_bulk">
                        @lang('backend.forms.invoice.invalidation.actions.accept')
                    </button>
                    <button wire:click="reloadInvalidationData()" type="button" class="btn btn-secondary" data-dismiss="modal">@lang('backend.forms.lines.bulk.actions.close')</button>
                </div>
            @endif
        </div>
    </div>
</div>
