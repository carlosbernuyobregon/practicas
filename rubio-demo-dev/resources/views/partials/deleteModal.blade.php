<div wire:ignore.self class="modal fade" id="delete_modal" tabindex="-1" role="dialog" aria-labelledby="deleteModalLabel"
    aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">@lang('backend.forms.delete-confirmation.title')</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <p>@lang('backend.forms.delete-confirmation.text')</p>
            </div>
            <div class="modal-footer">
                <button wire:click="delete()" type="button" class="btn btn-danger" id="btn_confirm">@lang('backend.forms.delete-confirmation.action')</button>
                <button type="button" class="btn btn-secondary" data-dismiss="modal">@lang('backend.forms.close')</button>
            </div>
        </div>
    </div>
</div>