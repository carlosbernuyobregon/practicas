<div class="kt-subheader__breadcrumbs">
    <a href="{{ route('dashboard')}}" class="kt-subheader__breadcrumbs-home">
        <i class="fas fa-tachometer-alt"></i>
    </a>
    @if(isset($pageBreadcrumbs))
    @foreach ($pageBreadcrumbs as $item)
    <span class="kt-subheader__breadcrumbs-separator"></span>
    <a href="#" class="kt-subheader__breadcrumbs-link">
        {{$item}}
    </a>
    @endforeach
    @endif
    <!-- <span class="kt-subheader__breadcrumbs-link kt-subheader__breadcrumbs-link--active">Active link</span> -->
</div>
