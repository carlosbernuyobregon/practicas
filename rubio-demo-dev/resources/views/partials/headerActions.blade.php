<div class="kt-portlet__head-wrapper">
    <div class="kt-portlet__head-actions">
        <a href="{{ url()->previous() }}" class="btn btn-brand btn-elevate btn-icon-sm">
            <i class="fas fa-arrow-left"></i>
            Go back
        </a>
    </div>
</div>
