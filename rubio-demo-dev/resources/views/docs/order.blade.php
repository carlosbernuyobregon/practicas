<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <title></title>
    <link href="{{ asset('css/print/bootstrap.css') }}" rel="stylesheet" type="text/css" />
    <style>
        footer {
            position: fixed;
            bottom: -70px;
            left: 0px;
            right: 0px;
            height: 50px;

            /** Extra personal styles **/
            background-color: white;
            color: black;
            text-align: center;
            line-height: 25px;
        }

        .page-break {
            page-break-after: always;
        }

    </style>
</head>

<body>
    <table style="margin-left:0px; margin-top:0px; border-collapse: collapse; width:100%;">
        <tr>
            <td style="width:30%">
                &nbsp;
            </td>
            <td style="width: 20%; text-align:center; padding-top:30px;">
                <img src="{{ asset('img/logo_rubio.jpg') }}" style="width: 80%;" />
            </td>
            <td style="width: 30%">
                &nbsp;
            </td>
        </tr>
    </table>
    <table style="margin-left:0px; margin-top:0px; border-collapse: collapse; width:100%;">
        <tr>
            <td style="width: 30%">
                &nbsp;
            </td>
            <td style="width: 40%; text-align:center;">
                C/Industria 29 Pol Industrial Compte de Sert
                <br>
                08755 Castellbisbal (Barcelona)
                <br>
                Tel: 937722509 Fax: 937722501
            </td>
            <td style="width: 30%">
                &nbsp;
            </td>
        </tr>
    </table>
    <table style="margin-left:0px; margin-top:20px; border-collapse: collapse; width:100%;">
        <tr>
            <td
                style="color: #000000;
                font-size:x-large;
                font-weight: bold; text-align: center;
                text-decoration: underline; text-transform: uppercase;
                padding-top: 10px; padding-bottom:25px; padding-left:10px; padding-right:10px;
                text-align:center;">
                SOLICITUD COMPRAS Y SERVICIOS
            </td>
        </tr>
    </table>
    <table style="margin-left:0px; margin-top:20px; border-collapse: collapse; width:100%;">
        <tr>
            <td style="width: 25%;">
                <b>N.º Solicitud:</b>
            </td>
            <td style="width: 25%;">
                {{ $order->order_number ?? '-' }}
            </td>
            <td style="width: 25%;">
                <b>Departamento:</b>
            </td>
            <td style="width: 25%;">
                {{ $order->costCenter->name ?? '-' }}
            </td>
        </tr>
        <tr>
            <td style="width: 25%;">
                <b>Proveedor:</b>
            </td>
            <td colspan="3">
                {{ $order->provider->name ?? '-' }}
            </td>
        </tr>
        <tr>
            <td style="width: 25%;">
                <b>Dirección:</b>
            </td>
            <td colspan="3">
                {{ $order->provider->address ?? '-' }}
            </td>
        </tr>
        <tr>
            <td style="width: 25%;">
                <b>Teléfono:</b>
            </td>
            <td style="width: 25%;">
                {{ $order->provider->phone_number ?? '-' }}
            </td>
            <td style="width: 25%;">
                <b>Fax:</b>
            </td>
            <td style="width: 25%;">
                {{ $order->provider->fax ?? '-' }}
            </td>
        </tr>
        <tr>
            <td style="width: 25%;">
                <b>E-mail:</b>
            </td>
            <td colspan="3">
                {{ $order->provider->email ?? '-' }}
            </td>
        </tr>
        <tr>
            <td style="width: 25%;">
                <b>Fecha solicitud:</b>
            </td>
            <td style="width: 25%;">
                {{ $order->created_at->format('d/m/Y') ?? '-' }}
            </td>

            <td style="width: 25%;">
                <!--<b>Fecha entrega prevista:</b>-->
                &nbsp;
            </td>
            <td style="width: 25%;">

                &nbsp;
            </td>
        </tr>
        <tr>
            <td style="width: 25%;">
                <b>Punto de entrega:</b>
            </td>
            <td style="width: 25%;">
               {{ $order->delivery_point ?? '-' }}
            </td>
            <td style="width: 25%;">
                <b>Horario de entrega:</b>
            </td>
            <td style="width: 25%;">
                De L-J de 08:00 a 14:00 y de 15:00 a 17:00h
                <br>
                Viernes de 08:00 a 14:00h
            </td>
        </tr>
    </table>
    <table class="table table-bordered" style="margin-left:0px; margin-top:40px; border-collapse: collapse; width:100%; border: 2px solid black;">
        <thead>
            <tr style="border: 2px solid black;">
                <td style="font-size: x-small; border: 2px solid black; text-transform: uppercase;"><b>Fecha de entrega</b></td>
                <td style="font-size: x-small; border: 2px solid black; text-transform: uppercase;"><b>Descripción</b></td>
                <td style="font-size: x-small; border: 2px solid black; text-transform: uppercase;"><b>Cant.</b></td>
                <td style="font-size: x-small; border: 2px solid black; text-transform: uppercase;"><b>Uds.</b></td>
                <td style="font-size: x-small; border: 2px solid black; text-transform: uppercase;"><b>Dto. %</b></td>
                <td style="font-size: x-small; border: 2px solid black; text-transform: uppercase;"><b>Precio</b></td>
                <td style="font-size: x-small; border: 2px solid black; text-transform: uppercase;"><b>Total</b></td>
            </tr>
        </thead>
        <tbody>
            @foreach ($orderLines as $item)
                <tr>
                    <td style="font-size: x-small; border: 2px solid black; text-transform: uppercase;">
                        {{ Carbon\Carbon::parse($item->delivery_date)->format('d/m/Y') ?? '-' }}
                    </td>
                    <td style="font-size: x-small; border: 2px solid black; text-transform: uppercase;">
                        {{ $item->description ?? '-' }}
                    </td>
                    <td style="font-size: x-small; border: 2px solid black; text-transform: uppercase;">
                        {{ $item->qty ?? 0 }}
                    </td>
                    <td style="font-size: x-small; border: 2px solid black; text-transform: uppercase;">
                        {{ $item->mesureUnit->unit ?? '-' }}
                    </td>
                    <td style="font-size: x-small; border: 2px solid black; text-transform: uppercase;">
                        0
                    </td>
                    <td style="font-size: x-small; border: 2px solid black; text-transform: uppercase;">
                        {{ number_format($item->net_amount, 4, ',', '.') ?? '-' }} €
                    </td>
                    <td style="font-size: x-small; border: 2px solid black; text-transform: uppercase;">
                        {{ number_format($item->qty * $item->net_amount, 4, ',', '.') ?? '-' }} €
                    </td>
                </tr>
            @endforeach
        </tbody>
        <tfoot>
            <tr>
                <td colspan="5" style="font-size: x-small; border: 2px solid black; text-transform: uppercase;">
                    &nbsp;
                </td>
                <td colspan="1" style="font-size: x-small; border: 2px solid black; text-transform: uppercase;">Total</td>
                <td colspan="1" style="font-size: x-small; border: 2px solid black; text-transform: uppercase;">
                    {{ number_format($order->totalLines(), 4, ',', '.') }} €
                </td>
            </tr>
        </tfoot>
    </table>
    <table>
        <tr>
            <td>
                <h3>Observaciones</h3>
            </td>
        </tr>
        @if($order->comments != null)
        <tr style="">
            <td style="width: 100%; margin-top: 0px; margin-bottom:10px;">
                {{ $order->comments }}
            </td>
        </tr>
        @endif
        <tr style="">
            <td style="margin-top: 100px; font-size: x-small">
                La factura correspondiente a este pedido debe estar en poder de Laboratorios Rubio, S.A., como máximo, dentro de los 5 primeros dias laborables del mes siguiente. En caso contrario el vencimiento de la misma será prorrogado 30 días más.
            </td>
        </tr>
    </table>
</body>

</html>
