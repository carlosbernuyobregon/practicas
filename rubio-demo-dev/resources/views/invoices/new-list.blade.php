@extends('layouts.app', [
'pageTitle' => $status,
'pageBreadcrumbs' => [
'Facturas',
$status,
]
])

@section('content')
<div class="kt-portlet kt-portlet--mobile">
    <div class="kt-portlet__head kt-portlet__head--lg">
        <div class="kt-portlet__head-label">
            <span class="kt-portlet__head-icon">
                <i class="kt-font-brand fas fa-file-invoice-dollar fa-lg"></i>
            </span>
            <h3 class="kt-portlet__head-title text-uppercase">
                {{ $status }}
            </h3>
        </div>
    </div>
    <div class="kt-portlet__body">
        <div class="row">
            <div class="col-12">
                @switch($table)
                    @case("email")
                        @livewire('invoices.pending',[
                            'view' => $view
                        ])
                        @break
                    @case("all-email")
                        @livewire('invoices.all-email-list',[
                            'view' => $view
                        ])
                        @break
                    @default
                        @livewire('invoices.table',[
                            'tableType' => $tableType,
                            'view' => $view,
                            'entries' => isset($entries) ? $entries : 10
                        ])
                @endswitch
            </div>
        </div>
    </div>
</div>
@endsection
