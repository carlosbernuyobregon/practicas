@extends('layouts.app', [
'pageTitle' => 'Factura: ' . $invoice->invoice_id,
'pageBreadcrumbs' => [
    'Facturas',
    'Listado de tareas',
    'Revisar factura'
]
])

@section('content')
<div class="kt-portlet kt-portlet--mobile">
    <div class="kt-portlet__head kt-portlet__head--lg">
        <div class="kt-portlet__head-label">
            <span class="kt-portlet__head-icon">
                <i class="kt-font-brand fas fa-file-invoice-dollar fa-lg"></i>
            </span>
            <h3 class="kt-portlet__head-title text-uppercase">
                Factura: {{ $invoice->provider_name }} - {{ $invoice->invoice_number }}
            </h3>
        </div>
    </div>
    <div class="kt-portlet__body">
        <div class="row">
            <div class="col-12">
                <a href="{{ url()->previous() }}" class="btn btn-primary btn-sm">Volver al listado</a>
            </div>
        </div>
        <div class="row mt-2">
            <div class="col-12 mt-3">
                <h4>Datos de cabecera</h4>
            </div>
            <div class="col-12">
                <ul class="nav nav-tabs" id="myTab" role="tablist">
                    <li class="nav-item">
                        <a class="nav-link active" href="#home">Datos principales</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#lines">Líneas de factura</a>
                    </li>
                    {{--@if (!$hasMoreThanOneLineTax)
                        <li class="nav-item">
                            <a class="nav-link" href="#home3">Pie de factura</a>
                        </li>
                    @endif--}}
                    <li class="nav-item">
                        <a class="nav-link" href="#home2">Datos secundarios</a>
                    </li>

                    @php($allowedUsersOnOcrData = explode(',', config('custom.allowed-users-special-header-buttons')))
                    <li class="nav-item">
                        <a class="nav-link" id="original-ocr-data-tab" data-toggle="tab" href="#original-ocr-data" role="tab"
                           aria-controls="original-ocr-data" aria-selected="true">Datos Originales OCR</a>
                    </li>
                    <li class="nav-item" wire:ignore>
                        <a class="nav-link" id="internal-messages-tab" data-toggle="tab" href="#internal-messages"
                           role="tab" aria-controls="internal-messages" aria-selected="true" {{--style="display: none"--}}>
                            Chat interno
                        </a>
                    </li>
                    <li class="nav-item" role="presentation" wire:ignore>
                        <a class="nav-link" id="files-tab" data-toggle="tab" href="#files" role="tab" aria-controls="files"
                           aria-selected="true">Archivos</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#audit">Auditoría</a>
                    </li>
                </ul>
                <div class="tab-content mt-3" id="myTabContent">
                    <div class="tab-pane fade active show" id="home" role="tabpanel" aria-labelledby="home-tab">
                        <div class="row">
                            <div class="col-12">
                                @livewire('invoices.header',[
                                    'invoice' => $invoice,
                                    'view' => $view
                                ])
                            </div>
                            <div class="col-12 mt-3">
                                <h4>Documento</h4>
                            </div>
                            <div class="col-12">
                                <div class="mb-2" >
                                    <button id="prev" class="btn btn-primary btn-sm">Previous</button>
                                    <button id="next" class="btn btn-primary btn-sm">Next</button>
                                    &nbsp; &nbsp;
                                    <span>Page: <span id="page_num"></span> / <span id="page_count"></span></span>
                                </div>
                                <div class="col-12 col-xl-6" style="">
                                    <canvas id="the-canvas" width="1190" class="my-5 w-100"></canvas>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane fade" id="lines" role="tabpanel" aria-labelledby="lines-tab">
                        <div class="row">
                            <div class="col-12">
                                @livewire('invoices.invoice-line-table', [
                                    'invoice' => $invoice,
                                    'view'    => $view,
                                    'entries' => config('custom.max-entries-for-invoices-lines-table')
                                ])
                            </div>
                        </div>
                    </div>
                    {{--<div class="tab-pane fade" id="home3" role="tabpanel" aria-labelledby="home3-tab">
                        <div class="row">
                            <div class="col-12">
                                <h5>Pie de factura:</h5>
                                <div class="table-responsive">
                                    <table class="table table-bordered table-hover w-100">
                                        @for ($i = 2; $i <= 5; $i++)
                                            @if ($invoice->{'total_net_' . $i})
                                                <tr>
                                                    <td class="w-1/8 font-weight-bold">Importe {{ $i - 1 }}:</td>
                                                    <td class="w-1/8">{{ number_format((float)$invoice->{'total_net_' . $i} ?? 0, 2, ',', '.') }} {{ $invoice->currency }}</td>
                                                    <td class="w-1/8 font-weight-bold">IVA {{ $i - 1 }}:</td>
                                                    <td class="w-1/8">{{ number_format((float)$invoice->{'tax_amount_' . $i} ?? 0, 2, ',', '.') }} {{ $invoice->currency }}</td>
                                                    <td class="w-1/8 font-weight-bold">TOTAL {{ $i - 1 }}:</td>
                                                    <td class="w-1/8">{{ number_format((float)$invoice->{'total_amount_' . $i} ?? 0, 2, ',' ,'.') }} {{ $invoice->currency }}</td>
                                                    <td class="w-1/8 font-weight-bold">II {{ $i - 1 }}:</td>
                                                    <td class="w-1/8">{{ number_format((float)$invoice->{'tax_rate_' . $i} ?? 0, 0, ',', '.') }}%</td>
                                                </tr>
                                            @endif
                                        @endfor
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>--}}
                    <div class="tab-pane fade" id="home2" role="tabpanel" aria-labelledby="home2-tab">
                        <div class="row">
                            <div class="col-12">
                                <h5>Datos de factura:</h5>
                                <div class="table-responsive">
                                    <table class="table table-bordered table-hover w-100">
                                        <tr>
                                            <td class="font-weight-bold" style="width: 10%">Dirección:</td>
                                            <td class="">{{ $invoice->provider_street ?? '' }} {{ $invoice->provider_postal_code ?? '' }}</td>
                                            <td class="font-weight-bold" style="width: 11%">Población:</td>
                                            <td class="">{{ $invoice->provider_city ?? '' }}</td>
                                            <td class="font-weight-bold" style="width: 10%">Provincia/Estado:</td>
                                            <td class="">{{ $invoice->provider_state ?? '' }}</td>
                                            <td class="font-weight-bold" style="width: 5%">País:</td>
                                            <td class="">{{ $invoice->provider_country ?? '' }}</td>
                                        </tr>
                                        <tr>
                                            <td class="font-weight-bold">Forma de pago:</td>
                                            <td class=" ">{{ $invoice->payment_conditions }}</td>
                                            <td class="font-weight-bold">Instrumento de pago:</td>
                                            <td class="">{{ $invoice->payment_conditions }}</td>
                                            <td class="font-weight-bold">Fecha de vencimiento:</td>
                                            <td class="">{{ auth()->user()->applyDateFormat($invoice->payment_date, true) ?? $invoice->payment_date }}</td>
                                            <td></td>
                                            <td></td>
                                        </tr>
                                        <tr>
                                            <td class="font-weight-bold">Fecha entrada email</td>
                                            <td>{{ $invoice->emailAttachment->created_at  }}</td>
                                            <td class="font-weight-bold">Proveedor</td>
                                            <td colspan="5">{{ $invoice->provider_name }} - {{ $invoice->emailAttachment->email->from }}</td>
                                        </tr>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane fade" id="audit" role="tabpanel" aria-labelledby="audit-tab">
                        <div class="row">
                            <div class="col-12">
                                @livewire('invoices.audit-table', [
                                    'invoice' => $invoice,
                                    ])
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane fade" id="original-ocr-data" role="tabpanel" aria-labelledby="original-ocr-data-tab">
                        <div class="row">
                            <div class="col-12">
                                @if (!empty($invoice->originalData))
                                    @livewire('invoices.ocr-original-data',[
                                        'ocrData' => $invoice->originalData
                                    ])
                                @else
                                    <h5>No hemos podido recuperar los Datos originales capturados por el OCR</h5>
                                @endif
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane fade" id="internal-messages" role="tabpanel" aria-labelledby="internal-messages-tab" wire:ignore.self>
                        @livewire('common.comments',[
                            'entityId'              => $invoice->id,
                            'entityType'            => 'App\Invoice',
                            'textTitle'             => 'Chat interno',
                            'editable'              => true,
                            'tabParentId'           => 'myTab', // The ul element id containing all the tabs
                            'tabId'                 => 'internal-messages-tab', // This is useful to set the unread message notification
                            'allowUsersApproach'    => true
                        ])
                    </div>
                    <div class="tab-pane fade" id="files" role="tabpanel" aria-labelledby="files-tab">
                        @livewire('common.file-upload',[
                            's3'        => true,
                            'path'      => config('custom.iworking_public_bucket_folder_invoices_docs') . '/' . now()->format('Y/m/d') . '/'
                            . (string)$invoice->id,
                            'modelId'       => (string)$invoice->id,
                            'model'         => 'App\Invoice',
                            'type'          => 'invoice-document',
                            'enableDelete'  => true
                        ])
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@push('css')
<!--begin::Page Vendors Styles(used by this page) -->
<link href="{{ url('/')}}/assets/vendors/custom/datatables/datatables.bundle.css" rel="stylesheet" type="text/css" />
<!--end::Page Vendors Styles -->
@endpush
@push('js')
<!--begin::Page Vendors(used by this page) -->
<script src="{{ url('/')}}/assets/vendors/custom/datatables/datatables.bundle.js" type="text/javascript"></script>
<script src="assets/js/pages/crud/datatables/advanced/column-visibility.js"></script>
<!--begin::Page Vendors(used by this page) -->
<script>
    jQuery(document).ready(function() {
    //KTDatatablesBasicBasic.init();
        var table = $('#invoices_lines');
		// begin first table
		table.DataTable({
			responsive: true,
			columnDefs: [
				{
					// hide columns by index number
					targets: [0, 3],
					visible: true,
				},
			],
		});
});
</script>
<script src="//mozilla.github.io/pdf.js/build/pdf.js"></script>
<script>
    $('#myTab a').on('click', function (e) {
        e.preventDefault();
        $(this).tab('show')
    });
    // If absolute URL from the remote server is provided, configure the CORS
// header on that server.
//var url = '{{ asset('files/invoices/'.$invoice->file) }}';
var url = '{{ route('invoice.download',['filename' => $invoice->filename]) }}';

// Loaded via <script> tag, create shortcut to access PDF.js exports.
var pdfjsLib = window['pdfjs-dist/build/pdf'];

// The workerSrc property shall be specified.
pdfjsLib.GlobalWorkerOptions.workerSrc = '//mozilla.github.io/pdf.js/build/pdf.worker.js';

var pdfDoc = null,
    pageNum = 1,
    pageRendering = false,
    pageNumPending = null,
    scale = 2,
    canvas = document.getElementById('the-canvas'),
    ctx = canvas.getContext('2d');

/**
 * Get page info from document, resize canvas accordingly, and render page.
 * @param num Page number.
 */
function renderPage(num) {
  pageRendering = true;
  // Using promise to fetch the page
  pdfDoc.getPage(num).then(function(page) {
    var viewport = page.getViewport({scale: scale});
    canvas.height = viewport.height;
    canvas.width = viewport.width;

    // Render PDF page into canvas context
    var renderContext = {
      canvasContext: ctx,
      viewport: viewport
    };
    var renderTask = page.render(renderContext);

    // Wait for rendering to finish
    renderTask.promise.then(function() {
      pageRendering = false;
      if (pageNumPending !== null) {
        // New page rendering is pending
        renderPage(pageNumPending);
        pageNumPending = null;
      }
    });
  });

  // Update page counters
  document.getElementById('page_num').textContent = num;
}

/**
 * If another page rendering in progress, waits until the rendering is
 * finised. Otherwise, executes rendering immediately.
 */
function queueRenderPage(num) {
  if (pageRendering) {
    pageNumPending = num;
  } else {
    renderPage(num);
  }
}

/**
 * Displays previous page.
 */
function onPrevPage() {
  if (pageNum <= 1) {
    return;
  }
  pageNum--;
  queueRenderPage(pageNum);
}
document.getElementById('prev').addEventListener('click', onPrevPage);

/**
 * Displays next page.
 */
function onNextPage() {
  if (pageNum >= pdfDoc.numPages) {
    return;
  }
  pageNum++;
  queueRenderPage(pageNum);
}
document.getElementById('next').addEventListener('click', onNextPage);

/**
 * Asynchronously downloads PDF.
 */
pdfjsLib.getDocument(url).promise.then(function(pdfDoc_) {
  pdfDoc = pdfDoc_;
  document.getElementById('page_count').textContent = pdfDoc.numPages;
  // Initial/first page rendering
  renderPage(pageNum);
});
</script>
@endpush
