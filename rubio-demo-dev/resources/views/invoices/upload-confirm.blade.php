@extends('layouts.app', [
'pageTitle' => 'Confirmación de subida',
'pageBreadcrumbs' => [
'Facturas',
'Nuevas',
'Confirmación de subida',
]
])

@section('content')
<div class="kt-portlet kt-portlet--mobile">
    <div class="kt-portlet__head kt-portlet__head--lg">
        <div class="kt-portlet__head-label">
            <span class="kt-portlet__head-icon">
                <i class="kt-font-brand fas fa-file-invoice-dollar fa-lg"></i>
            </span>
            <h3 class="kt-portlet__head-title text-uppercase">
                Confirmación de subida de facturas
            </h3>
        </div>
    </div>
    <div class="kt-portlet__body">
        <div class="row mt-2">
            <div class="col-12 mt-3">
                <h4>Nuevas facturas:</h4>
            </div>
            <div class="col-12">
                {{ $invoices }}
            </div>
        </div>
    </div>
</div>
@endsection
