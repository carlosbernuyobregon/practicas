@extends('layouts.app', [
'pageTitle' => $status,
'pageBreadcrumbs' => [
'Facturas',
$status,
]
])

@section('content')
<div class="kt-portlet kt-portlet--mobile">
    <div class="kt-portlet__head kt-portlet__head--lg">
        <div class="kt-portlet__head-label">
            <span class="kt-portlet__head-icon">
                <i class="kt-font-brand fas fa-file-invoice-dollar fa-lg"></i>
            </span>
            <h3 class="kt-portlet__head-title text-uppercase">
                {{ $status }}
            </h3>
        </div>
    </div>
    <div class="kt-portlet__body">
        <div class="row">
            <div class="col-12">
                <table class="table table-striped table-bordered table-hover table-checkable" id="invoices_list">
                    <thead>
                        <tr>
                            @if($table != 'email')
                            <th>SOCIEDAD</th>
                            <th>PROVEEDOR</th>
                            <th>CIF/NIF/VAT</th>
                            <th>N. FRA</th>
                            <th>FECHA FRA</th>
                            <th>IMPORTE</th>
                            <th>IVA</th>
                            <th>TOTAL</th>
                            <th>VTO.</th>
                            <th>CONTAB.</th>
                            <th>PEDIDO</th>
                            @else
                            <th>EMAIL</th>
                            <th>ASUNTO</th>
                            <th>NOMBRE ARCHIVO</th>
                            <th>FECHA DESCARGA</th>
                            <th>FECHA PROCESADO</th>
                            @endif
                            <th>ESTADO</th>
                            <th>ACCIONES</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($invoices as $invoice)
                            <tr>
                                @if($table != 'email')
                                    <td>{{ $invoice->sap_company }}</td>
                                    <td>{{ $invoice->provider_name }}</td>
                                    <td>{{ $invoice->vat_number }}</td>
                                    <td>{{ $invoice->invoice_number }}</td>
                                    <td>{{ $invoice->date }}</td>
                                    <td>{{ number_format($invoice->total_net,2,',','.') }} {{ $invoice->currency }}</td>
                                    <td>{{ number_format($invoice->taxes_total_amount,2,',','.') }} {{ $invoice->currency }}</td>
                                    <td>{{ number_format($invoice->total,2,',','.') }} {{ $invoice->currency }}</td>
                                    <td>{{ $invoice->payment_date ?? '' }}</td>
                                    <td>{{ $invoice->sap_document_number }}</td>
                                    <td>{{ $invoice->intern_order ?? '' }}</td>
                                    <td>@lang('status.'.$invoice->status)</td>
                                    <td>
                                        @if($view)
                                        <a href="{{ route('invoice.show',['invoice' => $invoice->id]) }}" {{--target="_blank"--}}>Visualizar</a>
                                        @else
                                        <a href="{{ route('invoice.review',['invoice' => $invoice->id]) }}" {{--target="_blank"--}}>Revisar</a>
                                        @endif
                                    </td>
                                    @else
                                    <td>{{ $invoice->emailAttachment->email->from }}</td>
                                    <td>{{ $invoice->emailAttachment->email->subject }}</td>
                                    <td>{{ $invoice->emailAttachment->filename }}</td>
                                    <td>{{ $invoice->emailAttachment->created_at }}</td>
                                    <td>{{ $invoice->emailAttachment->updated_at }}</td>
                                    <td>@lang('status.'.$invoice->status)</td>
                                    <td>
                                        <a target="_blank" href="{{ url('attachments') . '/' . $invoice->emailAttachment->email->id.'/'.$invoice->emailAttachment->filename }}">Descargar documento</a>
                                    </td>
                                @endif

                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
@endsection
@push('css')
<!--begin::Page Vendors Styles(used by this page) -->
<link href="{{ url('/')}}/assets/vendors/custom/datatables/datatables.bundle.css" rel="stylesheet" type="text/css" />
<!--end::Page Vendors Styles -->
@endpush
@push('js')
<!--begin::Page Vendors(used by this page) -->
<script src="{{ url('/')}}/assets/vendors/custom/datatables/datatables.bundle.js" type="text/javascript"></script>
@endpush
