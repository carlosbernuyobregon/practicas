@extends('layouts.app', [
'pageTitle' => Str::ucfirst('Global tasks'),
'pageBreadcrumbs' => [
'Processes',
'Validation',
'Global tasks'
]
])

@section('content')
@include('processes.components.task-list')
<div class="kt-portlet kt-portlet--mobile">
    <div class="kt-portlet__head kt-portlet__head--lg">
        <div class="kt-portlet__head-label">
            <span class="kt-portlet__head-icon">
                <i class="kt-font-brand fas fa-file-invoice-dollar fa-lg"></i>
            </span>
            <h2 class="kt-portlet__head-title text-uppercase">
                PROJECTS MANAGEMENT DASHBOARD
            </h2>
        </div>
    </div>
    <div class="kt-portlet__body">
        <div class="row">
            <div class="col-12">
                <ul class="nav nav-tabs" id="myTab" role="tablist">
                    <li class="nav-item">
                        <a class="nav-link active" id="request-tab" data-toggle="tab" href="#request" role="tab"
                            aria-controls="request" aria-selected="true">
                            GLOBAL TASKS
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="quote-tab" data-toggle="tab" href="#quote" role="tab"
                            aria-controls="quote" aria-selected="false">
                            TO PROJECTS
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="validation-tab" data-toggle="tab" href="#validation" role="tab"
                            aria-controls="validation" aria-selected="false">
                            TO CHECK
                        </a>
                    </li>
                </ul>
                <div class="tab-content mt-5" id="myTabContent">
                    <div class="tab-pane fade show active" id="request" role="tabpanel" aria-labelledby="request-tab">
                        <div class="row">
                            <div class="col-12">
                                <h1>Global Tasks</h1>
                            </div>
                        </div>
                        @include('test.projects.kanban')
                        <div class="row mt-5">
                            <div class="col-12">
                                <h1>Projects history</h1>
                            </div>
                        </div>
                        @include('test.projects.history')
                    </div>
                    <div class="tab-pane fade" id="quote" role="tabpanel" aria-labelledby="quote-tab">
                        <div class="row mt-5">
                            <div class="col-12">
                                <h1>To projects</h1>
                            </div>
                        </div>
                        @include('test.projects.projects')
                    </div>
                    <div class="tab-pane fade" id="validation" role="tabpanel" aria-labelledby="validation-tab">

                    </div>
                </div>
            </div>
        </div>

    </div>
</div>
@endsection
