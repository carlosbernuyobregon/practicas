<div class="row">
    <div class="col-12">
        <form class="kt-form kt-form--fit kt-margin-b-20">
            <div class="row kt-margin-b-20">
                <div class="col-lg-3 kt-margin-b-10-tablet-and-mobile">
                    <label>Project ID:</label>
                    <input type="text" class="form-control kt-input" placeholder="E.g: 4590" data-col-index="0">
                </div>
                <div class="col-lg-3 kt-margin-b-10-tablet-and-mobile">
                    <label>Request ID:</label>
                    <input type="text" class="form-control kt-input" placeholder="E.g: 37000-300" data-col-index="1">
                </div>
                <div class="col-lg-3 kt-margin-b-10-tablet-and-mobile">
                    <label>Country:</label>
                    <select class="form-control kt-input" data-col-index="2">
                        <option value="">Select</option>
                    </select>
                </div>
                <div class="col-lg-3 kt-margin-b-10-tablet-and-mobile">
                    <label>Agent:</label>
                    <input type="text" class="form-control kt-input" placeholder="Agent ID or name" data-col-index="4">
                </div>
            </div>
            <div class="row kt-margin-b-20 collapse" id="collapseExtraSearch">
                <div class="col-lg-3 kt-margin-b-10-tablet-and-mobile">
                    <label>Ship Date:</label>
                    <div class="input-daterange input-group" id="kt_datepicker">
                        <input type="text" class="form-control kt-input" name="start" placeholder="From"
                            data-col-index="5" />
                        <div class="input-group-append">
                            <span class="input-group-text"><i class="fas fa-ellipsis-h"></i></span>
                        </div>
                        <input type="text" class="form-control kt-input" name="end" placeholder="To"
                            data-col-index="5" />
                    </div>
                </div>
                <div class="col-lg-3 kt-margin-b-10-tablet-and-mobile">
                    <label>Status:</label>
                    <select class="form-control kt-input" data-col-index="6">
                        <option value="">Select</option>
                    </select>
                </div>
                <div class="col-lg-3 kt-margin-b-10-tablet-and-mobile">
                    <label>Type:</label>
                    <select class="form-control kt-input" data-col-index="7">
                        <option value="">Select</option>
                    </select>
                </div>
            </div>
            <div class="kt-separator kt-separator--md kt-separator--dashed"></div>
            <div class="row">
                <div class="col-lg-12">
                    <button class="btn btn-primary btn-brand--icon" id="kt_search">
                        <span>
                            <i class="fas fa-search"></i>
                            <span>Search</span>
                        </span>
                    </button>
                    &nbsp;&nbsp;
                    <button class="btn btn-secondary btn-secondary--icon" id="kt_reset">
                        <span>
                            <i class="fas fa-close"></i>
                            <span>Reset</span>
                        </span>
                    </button>
                    <button class="btn btn-primary float-right" type="button" data-toggle="collapse" data-target="#collapseExtraSearch" aria-expanded="false" aria-controls="collapseExtraSearch">
                        More filters
                      </button>
                </div>
            </div>
        </form>
    </div>
    <div class="col-12">
        <div class="kt-separator kt-separator--border-dashed kt-separator--space-md"></div>
        <table class="table table-striped- table-bordered table-hover table-checkable" id="kt_table_1">
            <thead>
                <tr>
                    <th>Type</th>
                    <th>Status</th>
                    <th>Parent Project</th>
                    <th>Project Name</th>
                    <th>Target Date</th>
                    <th>Designer</th>
                    <th>Actions</th>
                </tr>
            </thead>
        </table>
    </div>
</div>
<div class="modal fade bd-example-modal-xl" tabindex="-1" role="dialog" id="toProjectModal" aria-labelledby="toProjectModal" aria-hidden="true">
    <div class="modal-dialog modal-xl">
      <div class="modal-content">
        <div class="modal-header text-uppercase">
            <h5 class="modal-title">Project information</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <div class="modal-body">
            <div class="row">
                <div class="col-12">
                    <ul class="nav nav-tabs" id="myTab" role="tablist">
                        <li class="nav-item">
                            <a class="nav-link active" id="projectModal-tab" data-toggle="tab" href="#projectModalProject" role="tab"
                                aria-controls="projectModalproject" aria-selected="true">
                                PROJECT INFORMATION
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" id="requestModal-tab" data-toggle="tab" href="#projectModalRequest" role="tab"
                                aria-controls="projectModalRequest" aria-selected="false">
                                REQUEST INFORMATION
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" id="quoteModal-tab" data-toggle="tab" href="#projectModalQuote" role="tab"
                                aria-controls="projectModalQuote" aria-selected="false">
                                QUOTE INFORMATION
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" id="validationModal-tab" data-toggle="tab" href="#projectModalValidation" role="tab"
                                aria-controls="projectModalValidation" aria-selected="false">
                                VALIDATION INFORMATION
                            </a>
                        </li>
                    </ul>
                    <div class="tab-content mt-2 mb-5" id="myTabContent">
                        <div class="tab-pane fade show active" id="projectModalProject" role="tabpanel" aria-labelledby="projectModal-tab">
                            @include('test.projects.modal.project-info')
                        </div>
                        <div class="tab-pane fade" id="projectModalRequest" role="tabpanel" aria-labelledby="requestModal-tab">
                            Load request info...
                        </div>
                        <div class="tab-pane fade" id="projectModalQuote" role="tabpanel" aria-labelledby="quoteModal-tab">
                            Load quote info...
                        </div>
                        <div class="tab-pane fade" id="projectModalValidation" role="tabpanel" aria-labelledby="validationModal-tab">
                            Load validation info...
                        </div>
                    </div>
                </div>
                <div class="col-12">
                    @include('test.projects.modal.project-table')
                </div>
            </div>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        </div>
      </div>
    </div>
</div>
@push('css')
<!--begin::Page Vendors Styles(used by this page) -->
<link href="{{ url('/')}}/assets/vendors/custom/datatables/datatables.bundle.css" rel="stylesheet" type="text/css" />
<!--end::Page Vendors Styles -->
@endpush
@push('js')
<script src="{{ url('/')}}/assets/vendors/general/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js" type="text/javascript"></script>
<script src="{{ url('/')}}/assets/vendors/custom/js/vendors/bootstrap-datepicker.init.js" type="text/javascript"></script>
<script src="{{ url('/')}}/assets/vendors/general/bootstrap-datetime-picker/js/bootstrap-datetimepicker.min.js" type="text/javascript"></script>
<script src="{{ url('/')}}/assets/vendors/general/bootstrap-timepicker/js/bootstrap-timepicker.min.js" type="text/javascript"></script>
<script src="{{ url('/')}}/assets/vendors/custom/js/vendors/bootstrap-timepicker.init.js" type="text/javascript"></script>
<script src="{{ url('/')}}/assets/vendors/general/bootstrap-daterangepicker/daterangepicker.js" type="text/javascript"></script>
<script src="{{ url('/')}}/assets/vendors/custom/datatables/datatables.bundle.js" type="text/javascript"></script>
<!--end::Page Vendors -->

<!--begin::Page Scripts(used by this page) -->
<script>

</script>
@endpush
