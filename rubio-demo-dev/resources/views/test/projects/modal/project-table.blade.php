<div class="row">
    <div class="col-12">
        <table class="table table-striped- table-bordered table-hover table-checkable" id="subprojectsInfoTable">
            <thead>
                <tr class="text-uppercase">
                    <th>ID</th>
                    <th>Subproject name</th>
                    <th>Status</th>
                    <th>Delivery date</th>
                    <th>@lang('backend.crud.actions')</th>
                </tr>
            </thead>
            <tbody>
                @for ($i = 0; $i < 6; $i++)
                <tr>
                    <td>{{ $i }}</td>
                    <td>Subproyecto {{ $i }}</td>
                    <td>{{ array_rand(['new', 'work in progress', 'complete'])}}</td>
                    <td>20/07/2020</td>
                    <td nowrap>
                        <a href="#"
                            class="btn btn-sm btn-clean btn-icon btn-icon-md" title="View">
                            <i class="fas fa-search"></i>
                        </a>
                        <a href="#" class="btn btn-sm btn-clean btn-icon btn-icon-md mx-3"><i class="fas fa-project-diagram"></i></a>
                    </td>
                </tr>
                @endfor

            </tbody>
        </table>
    </div>
</div>
@push('css')
<!--begin::Page Vendors Styles(used by this page) -->
<link href="{{ url('/')}}/assets/vendors/custom/datatables/datatables.bundle.css" rel="stylesheet" type="text/css" />
<!--end::Page Vendors Styles -->
@endpush
@push('js')
<!--begin::Page Vendors(used by this page) -->
<script src="{{ url('/')}}/assets/vendors/custom/datatables/datatables.bundle.js" type="text/javascript"></script>

<!--end::Page Vendors -->

<!--begin::Page Scripts(used by this page) -->
<script>
jQuery(document).ready(function() {
    $('#subprojectsInfoTable').DataTable({
        order: [[0,"desc"]],
        dom: 'Bfrtip',
        buttons: [
            'copyHtml5',
            'excelHtml5',
            //'csvHtml5',
            'pdfHtml5',
            'pageLength'
        ],
        lengthMenu: [
            [ 10, 25, 50, -1 ],
            [ '10 rows', '25 rows', '50 rows', 'Show all' ]
        ]
    });
});
</script>
@endpush
