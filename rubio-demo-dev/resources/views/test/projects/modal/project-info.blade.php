<div class="row">
    <div class="col-12 col-md-6">
        <strong>Project code:</strong> X2000000
    </div>
    <div class="col-12 col-md-6">
        <strong>Request code:</strong> X2000000
    </div>
    <div class="col-12">
        <strong>Project Status:</strong> <span class="badge badge-warning">Working</span>
    </div>
</div>
<div class="kt-separator kt-separator--md kt-separator--dashed"></div>
<div class="row">
    <div class="col-12 col-md-6">
        <strong>Designer manager:</strong> Designer manager name
    </div>
    <div class="col-12 col-md-6">
        <strong>Project type:</strong> Type A
    </div>
    <div class="col-12 col-md-6">
        <strong>Graphic assignment date:</strong> 20/07/2020
    </div>
</div>
<div class="kt-separator kt-separator--md kt-separator--dashed"></div>
<div class="row">
    <div class="col-12 col-md-6">
        <strong>Request delivery date:</strong> 20/07/2020
    </div>
    <div class="col-12 col-md-6 text-danger">
        <strong>Estimated delivery date:</strong> 25/07/2020
    </div>
</div>
