<div class="row justify-content-end">
    <div class="col-12 col-md-6">
        <form>
            <div class="form-row justify-content-end">
                <div class="col-auto">
                    <label class="sr-only" for="inlineFormInputGroup">Search</label>
                    <div class="input-group mb-2">
                        <div class="input-group-prepend">
                            <div class="input-group-text"><i class="fas fa-search"></i></div>
                        </div>
                        <input type="text" class="form-control" id="inlineFormInputGroup"
                            placeholder="Username">
                    </div>
                </div>
                <div class="col-auto">
                    <button type="submit" class="btn btn-primary mb-2">Search</button>
                </div>
            </div>
        </form>
    </div>
</div>
<div class="row">
    <div class="col-4">
        <h3>NEW</h3>
        <div class="card card-custom mb-3">
            <div class="card-header bg-primary text-white">
                <h5 class="card-title">PROJECT TITLE - P20000005</h5>
            </div>
            <div class="card-body">
                <h6 class="mb-3"><strong>Request id:</strong> P20000005</h6>
                <h6 class="mb-3"><i class="fas fa-user"></i> Nombre de cliente</h6>
                <h6 class="mb-3"><i class="fas fa-globe"></i> Japan</h6>
                <h6 class="mb-3"><i class="fas fa-user-tie"></i> Nombre de business manager</h6>
                <h6 class="mb-3"><i class="fas fa-calendar"></i> 20/07/2020</h6>
                <h6 class="mb-3"><i class="fas fa-paint-brush"></i> Nombre diseñador</h6>
                <div class="collapse" id="project1">
                    <hr>
                    <h6>SUBPROYECTOS</h6>
                    <table class="w-100">
                        <thead>
                            <tr>
                                <th>SUBPROJECT</th>
                                <th>STATUS</th>
                                <th>ACTIONS</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>Subproyecto 1</td>
                                <td><span class="badge badge-warning">Pending</span></td>
                                <td><a href="">Ver</a></td>
                            </tr>
                            <tr>
                                <td>Subproyecto 2</td>
                                <td><span class="badge badge-warning">Pending</span></td>
                                <td><a href="">Ver</a></td>
                            </tr>
                            <tr>
                                <td>Subproyecto 3</td>
                                <td><span class="badge badge-warning">Pending</span></td>
                                <td><a href="">Ver</a></td>
                            </tr>
                        </tbody>
                    </table>
                    <hr>
                    <h6>COMMENTS</h6>
                    <p class="card-text">With supporting text below as a natural lead-in to
                        additional content.</p>
                </div>
            </div>
            <div class="card-footer bg-white text-right">
                <a href="#" class="mx-3"><i class="fas fa-project-diagram"></i></a>
                <a data-toggle="collapse" href="#project1" role="button" aria-expanded="false"
                    aria-controls="project1" class="btn btn-outline-primary btn-sm">Ver más</a>
            </div>
        </div>
        <div class="card card-custom mb-3">
            <div class="card-header bg-primary text-white">
                <h5 class="card-title">PROJECT TITLE - P20000005</h5>
            </div>
            <div class="card-body">
                <h6 class="mb-3"><strong>Request id:</strong> P20000005</h6>
                <h6 class="mb-3"><i class="fas fa-user"></i> Nombre de cliente</h6>
                <h6 class="mb-3"><i class="fas fa-globe"></i> Japan</h6>
                <h6 class="mb-3"><i class="fas fa-user-tie"></i> Nombre de business manager</h6>
                <h6 class="mb-3"><i class="fas fa-calendar"></i> 20/07/2020</h6>
                <h6 class="mb-3"><i class="fas fa-paint-brush"></i> Nombre diseñador</h6>
                <div class="collapse" id="project2">
                    <hr>
                    <h6>SUBPROYECTOS</h6>
                    <table class="w-100">
                        <thead>
                            <tr>
                                <th>SUBPROJECT</th>
                                <th>STATUS</th>
                                <th>ACTIONS</th>
                            </tr>
                        </thead>
                        <tr>
                            <td>Subproyecto 1</td>
                            <td><span class="badge badge-warning">Pending</span></td>
                            <td><a href="">Ver</a></td>
                        </tr>
                        <tr>
                            <td>Subproyecto 2</td>
                            <td><span class="badge badge-warning">Pending</span></td>
                            <td><a href="">Ver</a></td>
                        </tr>
                        <tr>
                            <td>Subproyecto 3</td>
                            <td><span class="badge badge-warning">Pending</span></td>
                            <td><a href="">Ver</a></td>
                        </tr>
                    </table>
                    <hr>
                    <h6>COMMENTS</h6>
                    <p class="card-text">With supporting text below as a natural lead-in to
                        additional content.</p>
                </div>
            </div>
            <div class="card-footer bg-white text-right">
                <a href="#" class="mx-3"><i class="fas fa-project-diagram"></i></a>
                <a data-toggle="collapse" href="#project2" role="button" aria-expanded="false"
                    aria-controls="project2" class="btn btn-outline-primary btn-sm">Ver más</a>
            </div>
        </div>
    </div>
    <div class="col-4">
        <h3>WORK IN PROGRESS</h3>
        <div class="card card-custom mb-3">
            <div class="card-header bg-danger text-white">
                <h5 class="card-title">PROJECT TITLE - P20000005</h5>
            </div>
            <div class="card-body">
                <h6 class="mb-3"><strong>Request id:</strong> P20000005</h6>
                <h6 class="mb-3"><i class="fas fa-user"></i> Nombre de cliente</h6>
                <h6 class="mb-3"><i class="fas fa-globe"></i> Japan</h6>
                <h6 class="mb-3"><i class="fas fa-user-tie"></i> Nombre de business manager</h6>
                <h6 class="mb-3 text-danger"><i class="fas fa-calendar"></i> 20/07/2020</h6>
                <h6 class="mb-3"><i class="fas fa-paint-brush"></i> Nombre diseñador</h6>
                <div class="collapse" id="project5">
                    <hr>
                    <h6>SUBPROYECTOS</h6>
                    <table class="w-100">
                        <thead>
                            <tr>
                                <th>SUBPROJECT</th>
                                <th>STATUS</th>
                                <th>ACTIONS</th>
                            </tr>
                        </thead>
                        <tr>
                            <td>Subproyecto 1</td>
                            <td><span class="badge badge-warning">Pending</span></td>
                            <td><a href="">Ver</a></td>
                        </tr>
                        <tr>
                            <td>Subproyecto 2</td>
                            <td><span class="badge badge-danger">Delayed</span></td>
                            <td><a href="">Ver</a></td>
                        </tr>
                        <tr>
                            <td>Subproyecto 3</td>
                            <td><span class="badge badge-success">Finished</span></td>
                            <td><a href="">Ver</a></td>
                        </tr>
                    </table>
                    <hr>
                    <h6>COMMENTS</h6>
                    <p class="card-text">With supporting text below as a natural lead-in to
                        additional content.</p>
                </div>
            </div>
            <div class="card-footer bg-white text-right">
                <a href="#" class="mx-3"><i class="fas fa-project-diagram"></i></a>
                <a data-toggle="collapse" href="#project5" role="button" aria-expanded="false"
                    aria-controls="project5" class="btn btn-outline-primary btn-sm">Ver más</a>
            </div>
        </div>
        <div class="card card-custom mb-3">
            <div class="card-header bg-warning text-white">
                <h5 class="card-title">PROJECT TITLE - P20000005</h5>
            </div>
            <div class="card-body">
                <h6 class="mb-3"><strong>Request id:</strong> P20000005</h6>
                <h6 class="mb-3"><i class="fas fa-user"></i> Nombre de cliente</h6>
                <h6 class="mb-3"><i class="fas fa-globe"></i> Japan</h6>
                <h6 class="mb-3"><i class="fas fa-user-tie"></i> Nombre de business manager</h6>
                <h6 class="mb-3 text-warning"><i class="fas fa-calendar"></i> 20/07/2020</h6>
                <h6 class="mb-3"><i class="fas fa-paint-brush"></i> Nombre diseñador</h6>
                <div class="collapse" id="project4">
                    <hr>
                    <h6>SUBPROYECTOS</h6>
                    <table class="w-100">
                        <thead>
                            <tr>
                                <th>SUBPROJECT</th>
                                <th>STATUS</th>
                                <th>ACTIONS</th>
                            </tr>
                        </thead>
                        <tr>
                            <td>Subproyecto 1</td>
                            <td><span class="badge badge-warning">Pending</span></td>
                            <td><a href="">Ver</a></td>
                        </tr>
                        <tr>
                            <td>Subproyecto 2</td>
                            <td><span class="badge badge-danger">Delayed</span></td>
                            <td><a href="">Ver</a></td>
                        </tr>
                        <tr>
                            <td>Subproyecto 3</td>
                            <td><span class="badge badge-success">Finished</span></td>
                            <td><a href="">Ver</a></td>
                        </tr>
                    </table>
                    <hr>
                    <h6>COMMENTS</h6>
                    <p class="card-text">With supporting text below as a natural lead-in to
                        additional content.</p>
                </div>
            </div>
            <div class="card-footer bg-white text-right">
                <a href="#" class="mx-3"><i class="fas fa-project-diagram"></i></a>
                <a data-toggle="collapse" href="#project4" role="button" aria-expanded="false"
                    aria-controls="project4" class="btn btn-outline-primary btn-sm">Ver más</a>
            </div>
        </div>
        <div class="card card-custom mb-3">
            <div class="card-header bg-success text-white">
                <h5 class="card-title">PROJECT TITLE - P20000005</h5>
            </div>
            <div class="card-body">
                <h6 class="mb-3"><strong>Request id:</strong> P20000005</h6>
                <h6 class="mb-3"><i class="fas fa-user"></i> Nombre de cliente</h6>
                <h6 class="mb-3"><i class="fas fa-globe"></i> Japan</h6>
                <h6 class="mb-3"><i class="fas fa-user-tie"></i> Nombre de business manager</h6>
                <h6 class="mb-3 text-success"><i class="fas fa-calendar"></i> 20/07/2020</h6>
                <h6 class="mb-3"><i class="fas fa-paint-brush"></i> Nombre diseñador</h6>
                <div class="collapse" id="project3">
                    <hr>
                    <h6>SUBPROYECTOS</h6>
                    <table class="w-100">
                        <thead>
                            <tr>
                                <th>SUBPROJECT</th>
                                <th>STATUS</th>
                                <th>ACTIONS</th>
                            </tr>
                        </thead>
                        <tr>
                            <td>Subproyecto 1</td>
                            <td><span class="badge badge-warning">Pending</span></td>
                            <td><a href="">Ver</a></td>
                        </tr>
                        <tr>
                            <td>Subproyecto 2</td>
                            <td><span class="badge badge-danger">Delayed</span></td>
                            <td><a href="">Ver</a></td>
                        </tr>
                        <tr>
                            <td>Subproyecto 3</td>
                            <td><span class="badge badge-success">Finished</span></td>
                            <td><a href="">Ver</a></td>
                        </tr>
                    </table>
                    <hr>
                    <h6>COMMENTS</h6>
                    <p class="card-text">With supporting text below as a natural lead-in to
                        additional content.</p>
                </div>
            </div>
            <div class="card-footer bg-white text-right">
                <a href="#" class="mx-3"><i class="fas fa-project-diagram"></i></a>
                <a data-toggle="collapse" href="#project3" role="button" aria-expanded="false"
                    aria-controls="project3" class="btn btn-outline-primary btn-sm">Ver más</a>
            </div>
        </div>
    </div>
    <div class="col-4">
        <h3 class="">CLOSED</h3>
        <div class="card card-custom mb-3">
            <div class="card-header bg-primary text-white">
                <h5 class="card-title">PROJECT TITLE - P20000005</h5>
            </div>
            <div class="card-body">
                <h6 class="mb-3"><strong>Request id:</strong> P20000005</h6>
                <h6 class="mb-3"><i class="fas fa-user"></i> Nombre de cliente</h6>
                <h6 class="mb-3"><i class="fas fa-globe"></i> Japan</h6>
                <h6 class="mb-3"><i class="fas fa-user-tie"></i> Nombre de business manager
                </h6>
                <h6 class="mb-3"><i class="fas fa-calendar"></i> 20/07/2020
                </h6>
                <h6 class="mb-3"><i class="fas fa-paint-brush"></i> Nombre diseñador</h6>
                <div class="collapse" id="project6">
                    <hr>
                    <h6>SUBPROYECTOS</h6>
                    <table class="w-100">
                        <thead>
                            <tr>
                                <th>SUBPROJECT</th>
                                <th>STATUS</th>
                                <th>ACTIONS</th>
                            </tr>
                        </thead>
                        <tr>
                            <td>Subproyecto 1</td>
                            <td><span class="badge badge-success">Finished</span></td>
                            <td><a href="">Ver</a></td>
                        </tr>
                        <tr>
                            <td>Subproyecto 2</td>
                            <td><span class="badge badge-success">Finished</span></td>
                            <td><a href="">Ver</a></td>
                        </tr>
                        <tr>
                            <td>Subproyecto 3</td>
                            <td><span class="badge badge-success">Finished</span></td>
                            <td><a href="">Ver</a></td>
                        </tr>
                    </table>
                    <hr>
                    <h6>COMMENTS</h6>
                    <p class="card-text">With supporting text below as a natural lead-in to
                        additional content.</p>
                </div>
            </div>
            <div class="card-footer bg-white text-right">
                <a href="#" class="mx-3"><i class="fas fa-project-diagram"></i></a>
                <a data-toggle="collapse" href="#project6" role="button"
                    aria-expanded="false" aria-controls="project6"
                    class="btn btn-outline-primary btn-sm">Ver más</a>
            </div>
        </div>
    </div>
</div>
