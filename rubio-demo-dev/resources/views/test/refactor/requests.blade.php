@extends('layouts.newapp', [
'pageTitle' => Str::ucfirst('Global tasks'),
'pageBreadcrumbs' => [
'Processes',
'Validation',
'Global tasks'
]
])

@section('content')
<div class="kt-portlet kt-portlet--mobile">
    <div class="kt-portlet__head kt-portlet__head--lg">
        <div class="kt-portlet__head-label">
            <span class="kt-portlet__head-icon">
                <i class="kt-font-brand fas fa-file-invoice-dollar fa-lg"></i>
            </span>
            <h2 class="kt-portlet__head-title text-uppercase">
                TITLE
            </h2>
        </div>
    </div>
    <div class="kt-portlet__body">
        @livewire('requests.form',[
        'SN' => $SN ?? null,
        'processRequest' => $processRequest->id ?? null,
        'processName' => $processName ?? null
        ])
    </div>
</div>
@endsection
@push('css')
<link href="{{url('/')}}/assets/vendors/general/bootstrap-datepicker/dist/css/bootstrap-datepicker3.css"
    rel="stylesheet" type="text/css" />
<link href="{{url('/')}}/assets/vendors/general/bootstrap-datetime-picker/css/bootstrap-datetimepicker.css"
    rel="stylesheet" type="text/css" />
<link href="{{url('/')}}/assets/vendors/general/bootstrap-select/dist/css/bootstrap-select.css" rel="stylesheet"
    type="text/css" />
<link href="{{url('/')}}/assets/vendors/general/summernote/dist/summernote.css" rel="stylesheet" type="text/css" />
@endpush


@push('js')
<script src="{{url('/')}}/assets/vendors/general/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"
    type="text/javascript"></script>
<script src="{{url('/')}}/assets/vendors/general/bootstrap-select/dist/js/bootstrap-select.js" type="text/javascript">
</script>
<script src="{{url('/')}}/assets/vendors/general/summernote/dist/summernote.js" type="text/javascript"></script>
<!--
<script src="{{url('/')}}/assets/vendors/custom/js/vendors/bootstrap-datepicker.init.js" type="text/javascript">
</script>
    <script src="{{url('/')}}/assets/js/demo1/pages/crud/forms/widgets/bootstrap-datepicker.js" type="text/javascript">
</script>
<script src="{{url('/')}}/assets/js/demo1/pages/crud/forms/widgets/bootstrap-select.js" type="text/javascript">
</script>
<script src="{{url('/')}}/assets/vendors/general/summernote/dist/summernote.js" type="text/javascript"></script>
<script src="{{url('/')}}/assets/vendors/general/jquery-validation/dist/jquery.validate.js" type="text/javascript">
</script>
<script src="{{url('/')}}/assets/vendors/general/jquery-validation/dist/additional-methods.js" type="text/javascript">
</script>
<script src="{{url('/')}}/assets/vendors/custom/js/vendors/jquery-validation.init.js" type="text/javascript"></script>
<script src="{{url('/')}}/assets/js/demo1/pages/crud/forms/validation/form-controls.js" type="text/javascript"></script>
<script src="{{ url('/')}}/assets/vendors/general/inputmask/dist/jquery.inputmask.bundle.js" type="text/javascript">
</script>
<script src="{{ url('/')}}/assets/vendors/general/inputmask/dist/inputmask/inputmask.date.extensions.js"
    type="text/javascript">
</script>
<script src="{{ url('/')}}/assets/vendors/general/inputmask/dist/inputmask/inputmask.numeric.extensions.js"
    type="text/javascript">
</script>


-->
<script>
        document.addEventListener('livewire:load', function () {
            $('.kt-datepicker').datepicker({
            rtl: KTUtil.isRTL(),
                todayBtn: "linked",
                clearBtn: true,
                todayHighlight: true,
                //templates: arrows
            });
            $('.kt-selectpicker').selectpicker();
            $('.summernote').summernote({
                height: 150,
                toolbar: [
                    // [groupName, [list of button]]
                    ['style', ['bold', 'italic', 'underline', 'clear']],
                    ['fontsize', ['fontsize']],
                    ['para', ['ul', 'ol', 'paragraph']],
                    ['height', ['height']]
                ]
            });
        })

</script>
@endpush

