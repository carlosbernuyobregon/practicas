<div class="modal-dialog" role="document" style="max-width:800px !important;">
    <div class="row">
        <div class="col-12">
            @if (session()->has('alert'))
                <div class="alert alert-danger">
                    {{ session('alert') }}
                </div>
            @endif
            @if (session()->has('success'))
                <div class="alert alert-success">
                    {{ session('success') }}
                </div>
            @endif
        </div>
    </div>
    <div class="modal-content">
        <div class="modal-header">
            <h5 class="modal-title">Alta Proveedor</h5>
            <button type="button" wire:click="clearProviderModal()" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">×</span>
            </button>
        </div>
        <div class="modal-body">
            <div class="row">
                <div class="row w-100">
                    <div class="col-12 col-md-4">
                        <div class="form-group{{ $errors->has('provider.company') ? ' has-danger' : '' }}">
                            <label class="form-control-label" for="provider-company">Sociedad *:</label>
                            <select wire:model.lazy="provider.company" class="form-control" name="provider-company">
                                <option value="">Seleccionar compañía</option>
                                @foreach ($companies as $company)
                                    <option value="{{ $company->code }}">{{ $company->code }} -
                                        {{ $company->name }}</option>
                                @endforeach
                            </select>
                            @error('provider.company') <span class="error text-danger">{{ $message }}</span> @enderror
                        </div>
                    </div>
                    <div class="col-12 col-md-4">
                        <div class="form-group {{ $errors->has('provider.name') ? ' has-danger' : '' }}">
                            <label for="name">@lang('backend.crud.provider.form.name') *:</label>
                            <input wire:model.lazy="provider.name" type="text" id="name_input"
                                   class="form-control {{ $errors->has('provider.name') ? ' is-invalid' : '' }}"
                                   placeholder="@lang('backend.crud.provider.form.name_placeholder')" />
                            @error('provider.name')<span class="text-danger">{{ $message }}</span>@enderror
                        </div>
                    </div>
                    <div class="col-12 col-md-4">
                        <div class="form-group {{ $errors->has('provider.vat_number') ? ' has-danger' : '' }}">
                            <label for="name">@lang('backend.crud.provider.form.vat_number') *:</label>
                            <input wire:model.lazy="provider.vat_number" type="text" id="vat_number_input"
                                   class="form-control {{ $errors->has('provider.vat_number') ? ' is-invalid' : '' }}"
                                   placeholder="@lang('backend.crud.provider.form.vat_number_placeholder')" />
                            @error('provider.vat_number')<span class="text-danger">{{ $message }}</span>@enderror
                        </div>
                    </div>
                    <div class="col-12 col-md-4">
                        <div class="form-group {{ $errors->has('provider.email') ? ' has-danger' : '' }}">
                            <label for="name">Email administración:</label>
                            <input wire:model.lazy="provider.email" type="text" id="email_input"
                                   class="form-control {{ $errors->has('provider.email') ? ' is-invalid' : '' }}"
                                   placeholder="Email administración" />
                            @error('provider.email')<span class="text-danger">{{ $message }}</span>@enderror
                        </div>
                    </div>
                    <div class="col-12 col-md-4">
                        <div class="form-group {{ $errors->has('provider.email_orders') ? ' has-danger' : '' }}">
                            <label for="name">Email pedidos *:</label>
                            <input wire:model.lazy="provider.email_orders" type="text" id="email_orders_input"
                                   class="form-control {{ $errors->has('provider.email_orders') ? ' is-invalid' : '' }}"
                                   placeholder="Email pedidos" />
                            @error('provider.email_orders')<span class="text-danger">{{ $message }}</span>@enderror
                        </div>
                    </div>
                </div>
                <div class="row w-100">
                    <div class="col-12 col-md-4">
                        <div class="form-group {{ $errors->has('provider.payment_condition') ? ' has-danger' : '' }}">
                            <label for="provider-payment-condition">@lang('backend.crud.provider.form.payment_condition') *:</label>
                            <select wire:model.lazy="provider.payment_condition" class="form-control" name="provider-payment-condition">
                                <option value="">Seleccionar condición pago</option>
                                @foreach ($providerPaymentTerms as $providerPaymentTerm)
                                    <option value="{{ $providerPaymentTerm->code }}" @if($providerPaymentTerm->is_default) selected @endif>{{ $providerPaymentTerm->code }} - {{ $providerPaymentTerm->name }}</option>
                                @endforeach
                            </select>
                            @error('provider.payment_condition')<span class="text-danger">{{ $message }}</span>@enderror
                        </div>
                    </div>
                    <div class="col-12 col-md-4">
                        <div class="form-group {{ $errors->has('provider.payment_type') ? ' has-danger' : '' }}">
                            <label for="provider-payment-type">@lang('backend.crud.provider.form.payment_type') *:</label>
                            <select wire:model.lazy="provider.payment_type" class="form-control" name="provider-payment-type">
                                <option value="">Seleccionar tipo pago</option>
                                @foreach ($providerPaymentTypes as $providerPaymentType)
                                    <option value="{{ $providerPaymentType->code }}" @if($providerPaymentType->is_default) selected @endif>{{ $providerPaymentType->name }}</option>
                                @endforeach
                            </select>
                            @error('provider.payment_type')<span class="text-danger">{{ $message }}</span>@enderror
                        </div>
                    </div>
                    <div class="col-12 col-md-4">
                        <div class="form-group">
                            <label class="col-8 col-form-label">Solicitud de Pedido:</label>
                            <div class="kt-checkbox-list">
                                <span class="kt-switch kt-switch--sm kt-switch--brand">
                                    <label>
                                        <input wire:model="provider.receiving_order_copy" name="toggle_rgpd" type="checkbox"
                                               class="form-control">
                                        <p style="display:inline-flex; padding: 5px 0 0 5px">
                                            @if(isset($provider))
                                                @if ($provider->receiving_order_copy) Desactivar @else Activar @endif envío al Proveedor
                                            @endif
                                        </p>
                                        <span style="margin-left: 5px"></span>
                                    </label>
                                </span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row w-100">
                    <div class="col-12 col-md-4">
                        <div class="form-group {{ $errors->has('provider.address') ? ' has-danger' : '' }}">
                            <label for="name">@lang('backend.crud.provider.form.address')</label>
                            <input wire:model.lazy="provider.address" type="text" id="address_input"
                                   class="form-control {{ $errors->has('provider.address') ? ' is-invalid' : '' }}"
                                   placeholder="@lang('backend.crud.provider.form.address')" />
                            @error('provider.address')<span class="text-danger">{{ $message }}</span>@enderror
                        </div>
                    </div>
                    <div class="col-12 col-md-4">
                        <div class="form-group {{ $errors->has('provider.city') ? ' has-danger' : '' }}">
                            <label for="name">@lang('backend.crud.provider.form.city')</label>
                            <input wire:model.lazy="provider.city" type="text" id="city_input"
                                   class="form-control {{ $errors->has('provider.city') ? ' is-invalid' : '' }}"
                                   placeholder="@lang('backend.crud.provider.form.city')" />
                            @error('provider.city')<span class="text-danger">{{ $message }}</span>@enderror
                        </div>
                    </div>
                    <div class="col-12 col-md-4">
                        <div class="form-group {{ $errors->has('provider.zip_code') ? ' has-danger' : '' }}">
                            <label for="name">@lang('backend.crud.provider.form.zip_code')</label>
                            <input wire:model.lazy="provider.zip_code" type="text" id="zip_code_input"
                                   class="form-control {{ $errors->has('provider.zip_code') ? ' is-invalid' : '' }}"
                                   placeholder="@lang('backend.crud.provider.form.zip_code')" />
                            @error('provider.zip_code')<span class="text-danger">{{ $message }}</span>@enderror
                        </div>
                    </div>
                </div>
                <div class="row w-100">
                    <div class="col-12 col-md-4">
                        <div class="form-group {{ $errors->has('provider.country') ? ' has-danger' : '' }}">
                            <label for="provider-country">@lang('backend.crud.provider.form.country') *:</label>
                            <select wire:model.lazy="provider.country" class="form-control" name="provider-country">
                                <option value="">Seleccionar país</option>
                                @foreach ($countries as $country)
                                    <option value="{{ $country->code }}">{{ $country->name }}</option>
                                @endforeach
                            </select>
                            @error('provider.country')<span class="text-danger">{{ $message }}</span>@enderror
                        </div>
                    </div>
                    <div class="col-12 col-md-4">
                        <div class="form-group {{ $errors->has('provider.phone_number') ? ' has-danger' : '' }}">
                            <label for="provider-phone-number">@lang('backend.crud.provider.form.phone_number') *:</label>
                            <input wire:model.lazy="provider.phone_number" type="text" name="provider-phone-number"
                                   class="form-control {{ $errors->has('provider.phone_number') ? ' is-invalid' : '' }}"
                                   placeholder="@lang('backend.crud.provider.form.phone_number')" />
                            @error('provider.phone_number')<span class="text-danger">{{ $message }}</span>@enderror
                        </div>
                    </div>
                    <div class="col-12 col-md-4">
                        <div class="form-group {{ $errors->has('provider.mediator') ? ' has-danger' : '' }}">
                            <label for="provider-mediator">Interlocutor *:</label>
                            <select wire:model.lazy="provider.mediator" class="form-control" name="provider-mediator">
                                <option value="">Seleccionar Interlocutor</option>
                                @foreach ($mediators as $mediator)
                                    <option value="{{ $mediator->person }}">{{ $mediator->name }} - {{ $mediator->person }}</option>
                                @endforeach
                            </select>
                            @error('provider.mediator')<span class="text-danger">{{ $message }}</span>@enderror
                        </div>
                    </div>
                    <div class="col-12 col-md-4">
                        <div class="form-group {{ $errors->has('provider.iban') ? ' has-danger' : '' }}">
                            <label for="iban">IBAN</label>
                            <input wire:model.lazy="provider.iban" type="text" name="provider-iban"
                                   class="form-control {{ $errors->has('provider.iban') ? ' is-invalid' : '' }}"
                                   placeholder="IBAN" />
                            @error('provider.iban')<span class="text-danger">{{ $message }}</span>@enderror
                        </div>
                    </div>
                </div>
                <div class="row w-100">
                    <div class="col-12 col-md-12">
                        <div class="form-group">
                            <div class="kt-checkbox-list">
                                <span class="kt-switch kt-switch--success kt-switch--sm">
                                    <label>
                                        <input wire:click="$toggle('showRgpd')" name="toggle_rgpd" type="checkbox"
                                               class="form-control" @if ($showRgpd) checked @endif>
                                        <p style="display:inline-flex; padding-top: 5px">Protección de Datos</p>
                                        <span></span>
                                    </label>
                                </span>
                                <p>
                                    Marcad esta casilla y cumplimentar si el proveedor va a tener acceso a documentación,
                                    programas o alguna base de datos de Rubió que contenga información personal, más allá de la persona de contacto.
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
                @if ($showRgpd)
                    <hr />
                    <div class="row w-100">
                        <h5 class="modal-title col-12 col-md-12 mb-3">Protección de Datos</h5>
                        <div class="col-12 col-md-4">
                            <div class="form-group {{ $errors->has('provider.rgpd_name') ? ' has-danger' : '' }}">
                                <label for="rgpd_name">Nombre</label>
                                <input wire:model="provider.rgpd_name" type="text" name="rgpd_name"
                                       class="form-control {{ $errors->has('provider.rgpd_name') ? ' is-invalid' : '' }}"
                                       placeholder="Nombre" />
                                @error('provider.rgpd_name')
                                <span class="text-danger">{{ $message }}</span>
                                @enderror
                            </div>
                        </div>
                        <div class="col-12 col-md-4">
                            <div class="form-group {{ $errors->has('provider.rgpd_last_name') ? ' has-danger' : '' }}">
                                <label for="rgpd_last_name">Apellidos</label>
                                <input wire:model="provider.rgpd_last_name" type="text" name="rgpd_last_name"
                                       class="form-control {{ $errors->has('provider.rgpd_last_name') ? ' is-invalid' : '' }}"
                                       placeholder="Apellidos" />
                                @error('provider.rgpd_last_name')
                                <span class="text-danger">{{ $message }}</span>
                                @enderror
                            </div>
                        </div>
                        <div class="col-12 col-md-4">
                            <div class="form-group {{ $errors->has('provider.rgpd_dni') ? ' has-danger' : '' }}">
                                <label for="rgpd_dni">DNI</label>
                                <input wire:model="provider.rgpd_dni" type="text" name="rgpd_dni"
                                       class="form-control {{ $errors->has('provider.rgpd_dni') ? ' is-invalid' : '' }}"
                                       placeholder="DNI" />
                                @error('provider.rgpd_dni')
                                <span class="text-danger">{{ $message }}</span>
                                @enderror
                            </div>
                        </div>
                        <div class="col-12 col-md-4">
                            <div class="form-group {{ $errors->has('provider.rgpd_email') ? ' has-danger' : '' }}">
                                <label for="rgpd_email">Email</label>
                                <input wire:model="provider.rgpd_email" type="text" name="rgpd_email"
                                       class="form-control {{ $errors->has('provider.rgpd_email') ? ' is-invalid' : '' }}"
                                       placeholder="Email" />
                                @error('provider.rgpd_email')
                                <span class="text-danger">{{ $message }}</span>
                                @enderror
                            </div>
                        </div>
                        <div class="col-12 col-md-4">
                            <div class="form-group {{ $errors->has('provider.rgpd_role') ? ' has-danger' : '' }}">
                                <label for="rgpd_role">Cargo</label>
                                <input wire:model="provider.rgpd_role" type="text" name="rgpd_role"
                                       class="form-control {{ $errors->has('provider.rgpd_role') ? ' is-invalid' : '' }}"
                                       placeholder="Cargo / Posición" />
                                @error('provider.rgpd_role')
                                <span class="text-danger">{{ $message }}</span>
                                @enderror
                            </div>
                        </div>
                        <div class="col-12 col-md-4">
                            <div class="form-group {{ $errors->has('provider.rgpd_service_description') ? ' has-danger' : '' }}">
                                <label for="rgpd_service_description">Servicio a prestar</label>
                                <textarea wire:model="provider.rgpd_service_description"
                                          class="form-control {{ $errors->has('provider.rgpd_service_description') ? ' is-invalid' : '' }}"
                                          placeholder="Descripción servicio a prestar">
                                </textarea>
                                @error('provider.rgpd_service_description')
                                    <span class="text-danger">{{ $message }}</span>
                                @enderror
                            </div>
                        </div>
                    </div>
                @endif
                <div class="row w-100">
                    @if (!empty($this->provider) && $this->provider->id != null && in_array($this->provider->payment_type, $providerPaymentTypeRequiringFiles))
{{--                    @if ($this->provider->id != null)--}}
                        <div class="col-12">
                            @livewire('common.file-upload',[
                                's3'                => true,
                                'path'              => config('custom.iworking_public_bucket_folder') . '/provider/' . now()->format('Y/m/d') . '/'
                                . (string)$provider->id,
                                'modelId'           => (string)$provider->id,
                                'model'             => 'App\Models\Provider',
                                'type'              => 'provider-attachment',
                                'enableUpload'      => $editable,
                                'enableDelete'      => $editable,
                                'eventIdentifier'   => 'providerFilesUpdated',
                                'title'             => 'Certificado bancario *:'
                            ], key(time() . 'file-uploader'))
                        </div>
                    @endif
                </div>
            </div>
        </div>
        <div class="modal-footer">
            @if($this->provider && $this->provider->id == null)
                <button type="button" wire:click="saveProvider" class="btn btn-success">
                    @if ($this->provider->id && in_array($this->provider->payment_type, $providerPaymentTypeRequiringFiles))
                        Subir documentación proveedor
                    @else
                        Guardar proveedor
                    @endif
                </button>
            @else
                <button type="button" wire:click="initNewProviderWorkflow('{{ ($this->provider) ? $this->provider->id : false }}')" class="btn btn-success"
                        @if (!$this->provider || ($this->provider->files()->count() == 0 && in_array($this->provider->payment_type, $providerPaymentTypeRequiringFiles))) disabled @endif>
                    Iniciar validación proveedor
                </button>
            @endif
            <button type="button" class="btn btn-danger"
                    onclick="return confirm('{{ __("backend.messages.delete-confirmation") }}') || event.stopImmediatePropagation()"
                    wire:click="rejectProvider" data-dismiss="modal">
                Cancelar
            </button>
            <button wire:click="clearProviderModal()" type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
        </div>
    </div>
</div>
