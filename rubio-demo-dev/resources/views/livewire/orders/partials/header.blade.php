<div class="row">
    <div class="col-8">
        <label class="form-control-label" for="input-key_value">Tipo de pedido: </label>
        @if(!$orderType)
        <div>
            @foreach ($orderTypeValue as $key => $value)
            <button type="button" wire:click="$set('orderType','{{ $key }}')" wire:loading.attr="disabled"
                class="btn btn-sm btn-primary d-flex p-4 py-lg-2 float-left mr-2" @if($key != 'K') disabled @endif >
                {{ $value ?? '-' }}
            </button>
            @endforeach
        </div>

        @error('order.company') <span class="error text-danger">{{ $message }}</span> @enderror
        @else
            <input type="text" value="{{ $orderTypeValue[$orderType] }}" class="form-control" readonly disabled/>
        @endif
    </div>
</div>
<div class="@if(!$orderType) d-none @endif">
    <hr>
    <div class="row">
        <div class="col-12 col-md-3">
            <div class="form-group{{ $errors->has('order.company') ? ' has-danger' : '' }}">
                <label class="form-control-label" for="order.company">Sociedad *:</label>
                <select wire:model.lazy="order.company" class="form-control" id="order.company" @if(
                    $order->status > App\Library\Constants::ORDER_STATUS_DRAFT &&
                    $order->status != App\Library\Constants::ORDER_STATUS_MODIFICATION_PENDING
                ) readonly disabled @endif>
                    <option value="">-- Seleccionar compañía --</option>
                    @foreach ($companies as $company)
                        <option value="{{ $company->code }}" selected="selected">{{ $company->code }} -
                            {{ $company->name }}</option>
                    @endforeach
                </select>
                @error('order.company') <span class="error text-danger">{{ $message }}</span> @enderror
            </div>
        </div>
        <div class="col-12 col-md-3">
            <div class="form-group">
                <label for="fecha" class="form-control-label">Fecha *:</label>
                <input type="date" wire:model.lazy="order.date" id="order.date"
                    value="{{ $order->date ?? today()->format('d-m-Y') }}"
                    class="form-control"  @if(
                    $order->status > App\Library\Constants::ORDER_STATUS_DRAFT &&
                    $order->status != App\Library\Constants::ORDER_STATUS_MODIFICATION_PENDING
                ) readonly disabled @endif/>
                @error('order.date') <span class="error text-danger">{{ $message }}</span> @enderror
            </div>
        </div>
        <div class="col-12 col-md-6">
            <div class="form-group{{ $errors->has('key_value') ? ' has-danger' : '' }}">
                <label class="form-control-label" for="order.provider_id">Proveedor *:</label>
                <div class="input-group mb-0">
                    @if($order->provider_id == null)
                        <input type="text" wire:model="providerSearch" class="form-control" @if($providerForm) readonly disabled @endif/>
                        <div class="input-group-append">
                            <button wire:click="newProvider" class="btn btn-primary" type="button">
                                Crear proveedor
                            </button>
                        </div>
                    @else
                        <input type="text" wire:model="provider.name" class="form-control" readonly/>
                        @if($order->status == 0)
                        <div class="input-group-append">
                            <button wire:click="unsetProvider" class="btn btn-danger" type="button">
                                Quitar proveedor
                            </button>
                        </div>
                        @endif
                    @endif
                </div>
                @error('order.provider_id') <span class="error text-danger">{{ $message }}</span>@enderror
            </div>
        </div>
        @if($providerSearch != '')
            <hr>
            <div class="col-12 py-3 my-3 border border-primary border-1">
                <h6>Proveedores</h6>
                <table class="table table-striped table-bordered table-hover table-checkable">
                    <thead>
                        <tr>
                            <th>ID</th>
                            <th>Nombre</th>
                            <th>VAT</th>
                            <th>Action</th>

                        </tr>
                    </thead>
                    <tbody>
                        @forelse ($providersList as $item)
                        <tr>
                            <td>{{ $item->external_id }}</td>
                            <td>{{ $item->name ?? '-' }}</td>
                            <td>{{ $item->vat_number ?? '-' }}</td>
                            <td>
                                <div class="btn-group my-1" role="group" aria-label="">
                                    <button wire:click="setProvider('{{ $item->id }}')" class="btn btn-sm btn-warning"
                                        title="Editar línea"><i class="fa fa-hand-pointer"></i></button>
                                </div>
                            </td>
                        </tr>
                        @empty
                        <tr>
                            <td colspan="11">
                                No hay proveedores disponibles
                            </td>
                        </tr>
                        @endforelse
                    </tbody>
                </table>
                {{ $providersList->links() }}
            </div>
            <hr>
        @endif

        <div class="col-12 py-3 my-3 border border-primary border-1" style="@if(!$providerForm)display:none @endif">
            @include('livewire.orders.partials.provider')
        </div>

        <div class="col-12 col-md-3">
            <div class="form-group{{ $errors->has('deliveryPointOption') ? ' has-danger' : '' }}">
                <label class="form-control-label" for="input-key_value">Punto de entrega *:</label>
                <select wire:model="deliveryPointOption" class="form-control" @if(
                    $order->status > App\Library\Constants::ORDER_STATUS_DRAFT &&
                    $order->status != App\Library\Constants::ORDER_STATUS_MODIFICATION_PENDING
                ) readonly disabled @endif>
                    <option value="">Punto de entrega</option>
                    @foreach ($deliveryPoints as $option)
                        <option value="{{ $option }}">{{ $option }}</option>
                    @endforeach
                </select>
                @error('order.delivery_point') <span class="error text-danger">{{ $message }}</span> @enderror
            </div>
        </div>
        @if($this->deliveryPointOption == 'Otros')
            <div class="col-12 col-md-3">
                <label class="form-control-label" for="input-key_value">Detalle punto de entrega:</label>
                <input type="text" wire:model="order.delivery_point" placeholder="Punto de entrega" class="form-control" @if($order->status > 0) readonly disabled @endif />
            </div>
        @endif
        <div class="col-12 col-md-4">
            <div class="form-group">
                <label class="col-8 col-form-label">Solicitud de Pedido:</label>
                <div class="kt-checkbox-list">
                    <span class="kt-switch kt-switch--sm kt-switch--brand">
                        <label>
                            <input wire:model="order.receiving_order_copy" name="toggle_order_copy" type="checkbox" class="form-control">
                            <p style="display:inline-flex; padding: 5px 0 0 5px">
                                @if ($order->receiving_order_copy) Desactivar @else Activar @endif envío al Proveedor
                            </p>
                            <span style="margin-left: 5px"></span>
                        </label>
                    </span>
                    <div class="w-100">
                        <span class="text-sm font-italic text-danger">Tendrá prioridad sobre ficha del Proveedor</span>
                    </div>
                </div>
            </div>
        </div>
        <div class="row w-100">
            <div class="col-12 mb-5">
                @if ($task && (($order->status == 0)||(auth()->user()->id == $order->user_id)))
                    <label class="form-control-label" for="input-key_value">Añadir comentarios internos:</label>
                    <textarea wire:model.lazy="order.comments" class="form-control {{ $errors->has('order.comments') ? ' is-invalid' : '' }}"
                              cols="10" rows="3">
                    </textarea>
                @else
                    @if ($order->comments)
                        <label class="form-control-label" for="input-key_value">Comentarios internos:</label>
                        {{ $order->comments }}
                    @endif
                @endif
                @error('order.comments') <span class="text-danger">{{ $message }}</span> @enderror
            </div>
        </div>
    </div>
    <hr>
    <div class="row">
        <div class="col-12 col-md-4">
            <label class="form-control-label" for="invoice_number">Nº factura:</label>
            <input type="text"
                   wire:model="order.invoice_number"
                   placeholder="Nº de factura"
                   class="form-control" @if($order->status > 0) readonly disabled @endif />
            @error('order.invoice_number') <span class="error text-danger">{{ $message }}</span> @enderror
        </div>
        <div class="col-12 col-md-4">
            <label class="form-control-label" for="invoice_date">Fecha factura:</label>
            <input type="date"
                   wire:model="order.invoice_date"
                   placeholder="Fecha de factura"
                   class="form-control" @if($order->status > 0) readonly disabled @endif />
            @error('order.invoice_date') <span class="error text-danger">{{ $message }}</span> @enderror
        </div>
        <div class="col-12 col-md-4">
            <label class="form-control-label" for="invoice_total_net">Importe factura (Base Imponible):</label>
            <input type="text"
                   wire:model="order.invoice_total_net"
                   placeholder="Total neto"
                   class="form-control" @if($order->status > 0) readonly disabled @endif />
            @error('order.invoice_total_net') <span class="error text-danger">{{ $message }}</span> @enderror
        </div>
    </div>
{{--    @if($order->status > 0)--}}
        <hr>
        <div class="row div col-12 col-md-4 offset-md-8">
            <label class="form-control-label" for="order_total_net">Importe total:</label>
            <input type="text"
                    class="form-control" readonly disabled
                    value="{{ number_format($order->totalLines(),2,',','.') }} €"/>

        </div>
{{--    @endif--}}
</div>
<div wire:ignore.self class="modal fade" id="newProviderOrderModal" tabindex="-1" role="dialog" aria-labelledby="bulkModalLabel" aria-hidden="true">
    @include('livewire.orders.partials.provider')
</div>

@push('js')
    <script type="text/javascript">
        window.addEventListener('showNewProviderModal', () => {
            $('#newProviderOrderModal').modal('show');
        });
        window.addEventListener('hideNewProviderModal', () => {
            $('#newProviderOrderModal').modal('hide');
        });
    </script>
@endpush
