<div>
    <div class="dt-buttons btn-group mb-2">
        <div class="btn-group">
        </div>
        <div class="btn-group">
            @if (!$filtersMode)
                <button wire:click="$toggle('filtersMode')" class="btn btn-primary" type="button" data-toggle="collapse" data-target="#collapseFilters" aria-expanded="false" aria-controls="collapseFilters">
                    Mostrar filtros
                </button>
            @else
                <button wire:click="$toggle('filtersMode')" class="btn btn-warning" type="button" data-toggle="collapse" data-target="#collapseFilters" aria-expanded="false" aria-controls="collapseFilters">
                    Ocultar filtros
                </button>
            @endif
        </div>
    </div>
    <div class="collapse" id="collapseFilters" wire:ignore.self>
        <div class="row">
            <div class="col-6 col-lg-10">
                <h5>Filtros:</h5>
            </div>
            <div class="col-6 col-lg-2">
                @if (session()->has('search'))
                    <button wire:click="resetFilters()" class="btn btn-sm btn-danger btn-inline float-right"><i class="fas fa-times"></i> Eliminar filtros</button>
                @endif
            </div>
        </div>
        <div class="row">
            <div class="col-12 col-md-2">
                <div class="form-group">
                    <label for="from" class="form-control-label">Desde
                        *:</label>
                    <input type="date" wire:model="search.from" id="fecha" class="form-control"
                           max="{{ today()->format('Y-m-d') }}">
                </div>
            </div>
            <div class="col-12 col-md-2">
                <div class="form-group">
                    <label for="to" class="form-control-label">Hasta
                        *:</label>
                    <input type="date" wire:model="search.to" id="fecha" class="form-control"
                           max="{{ today()->format('Y-m-d') }}">
                </div>
            </div>
            <div class="col-12 col-md-3">
                <div class="form-group">
                    <label for="cost_center_id" class="form-control-label">Centro de coste:</label>
                    <select wire:model="search.costCenterId" class="form-control">
                        <option value="">-- Seleccionar centro de coste --</option>
                        @foreach ($costCenters as $costCenterItem)
                            <option value="{{ $costCenterItem->id }}">{{ $costCenterItem->name }}
                                ({{ (int) $costCenterItem->external_id }})</option>
                        @endforeach
                    </select>
                </div>
            </div>

            <div class="col-12 col-md-3">
                <div class="form-group">
                    <label for="provider_id" class="form-control-label">Proveedor:</label>
                    {{--<select wire:model="search.providerId" class="form-control">
                        <option value="">-- Seleccionar proveedor --</option>
                        @foreach ($providers as $provider)
                            <option value="{{ $provider->id }}">{{ $provider->name }}</option>
                        @endforeach
                    </select>--}}
                    @livewire('components.select.custom-select', [
                        'objectValue'                   => !empty($search['providerId']) ? $search['providerId'] : false,
                        'selectName'                    => 'providerCustomSelect',
                        'selectId'                      => 'providerCustomSelect',
                        'modelToCreateOptions'          => App\Models\Provider::class,
                        'scopeToCreateOptions'          => false,
                        'optionIdFieldMapping'          => 'id',
                        'includeJsCdn'                  => false,
                        'placeholder'                   => '-- Seleccionar un proveedor --',
                        'parentComponentName'           => App\Http\Livewire\Orders\Dashboard::getName(),
                        'parentEvent'                   => 'changedCustomSelect',
                        'modelPropertyName'             => 'search.providerId',
                        'checkValuesDebugButton'        => false,
                        'optionFieldOrderBy'            => 'name',
                        'optionFieldOrderDirection'     => 'asc',
                        'minimumInputLength'            => '3',
{{--                        'hardResetSelect'               => true,--}}
                    ], key('providerCustomSelect'))
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-12 col-md-6">
            <div style="height: 24rem;">
                <livewire:livewire-column-chart key="{{ $ordersByMonth->reactiveKey() }}" :column-chart-model="$ordersByMonth" />
            </div>
        </div>
        <div class="col-12 col-md-6">
            <div style="height: 24rem;">
                <livewire:livewire-column-chart key="{{ $amountByMonth->reactiveKey() }}" :column-chart-model="$amountByMonth" />
            </div>
        </div>
        <div class="col-12 col-md-6 mt-4">
            <div style="height: 30rem;">
                <livewire:livewire-column-chart key="{{ $amountByProvider->reactiveKey() }}" :column-chart-model="$amountByProvider" />
            </div>
        </div>
        <div class="col-12 col-md-6 mt-4">
            <div style="height: 30rem;">
                <livewire:livewire-column-chart key="{{ $amountByCeCo->reactiveKey() }}" :column-chart-model="$amountByCeCo" />
            </div>
        </div>
    </div>
</div>
@push('css')
    @livewireChartsScripts
    <link href="{{ config('custom.iworking_public_bucket') }}/assets/vendors/general/select2/dist/css/select2.css"
        rel="stylesheet" type="text/css" />
    <link rel="stylesheet"
        href="https://cdn.jsdelivr.net/npm/select2-bootstrap-5-theme@1.1.1/dist/select2-bootstrap-5-theme.min.css" />
@endpush
@push('js')
    <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
    <script>
        $(document).ready(function() {
            /*$('.form-select').select2({
                theme: "bootstrap-5",
                containerCssClass: "select2--large", // For Select2 v4.0
                selectionCssClass: "select2--large", // For Select2 v4.1
                dropdownCssClass: "select2--large",
            });
            $('.form-select').on('change', function(e) {
                @this.set($(this).attr("wiremodel"), e.target.value);
                //livewire.emit('selectedCompanyItem', e.target.value)
            });*/
        });
        document.addEventListener("DOMContentLoaded", () => {

            Livewire.hook('message.processed', (message, component) => {
            })
        });
    </script>
@endpush
