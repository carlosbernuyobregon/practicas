<div>
    <div class="row mt-2">
        <div class="col-12 col-md-6 py-2">
            <h4>Nuevo pedido</h4>
        </div>
        <div class="col-12 col-md-6 text-right">
            <div wire:loading class="">
                <div class="lds-ring">
                    <div></div>
                    <div></div>
                    <div></div>
                    <div></div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-12">
            @if (session()->has('alert'))
                <div class="alert alert-danger">
                    {{ session('alert') }}
                </div>
            @endif
            @if (session()->has('success'))
                <div class="alert alert-success">
                    {{ session('success') }}
                </div>
            @endif
        </div>
    </div>
    <div class="row">
        <div class="col-12">
            <ul class="nav nav-tabs" id="myTab" role="tablist">
                <li class="nav-item" role="presentation" wire:ignore>
                    <a class="nav-link @if($this->order->id == null ) active @endif" id="order-header-tab" data-toggle="tab"
                        href="#order-header" role="tab" aria-controls="order-header"
                        aria-selected="true">Cabecera pedido</a>
                </li>
                <li class="nav-item" role="presentation" wire:ignore>
                    <a class="nav-link @if($this->order->id == null) disabled @else active @endif" id="order-lines-tab" data-toggle="tab" href="#order-lines" role="tab"
                        aria-controls="order-lines" aria-selected="true">Líneas de pedido</a>
                </li>
                <li class="nav-item" role="presentation" wire:ignore>
                    <a class="nav-link @if($this->order->id == null) disabled @endif" id="order-messages-tab" data-toggle="tab" href="#order-messages"
                        role="tab" aria-controls="order-messages" aria-selected="true">Chat interno</a>
                </li>
                <li class="nav-item" role="presentation" wire:ignore>
                    <a class="nav-link @if($this->order->id == null) disabled @endif" id="order-files-tab" data-toggle="tab" href="#order-files" role="tab" aria-controls="order-files"
                        aria-selected="true">Archivos</a>
                </li>
                <li class="nav-item" role="presentation" wire:ignore>
                    <a class="nav-link @if($this->order->id == null) disabled @endif" id="order-audit-tab" data-toggle="tab" href="#order-audit" role="tab"
                        aria-controls="order-audit" aria-selected="true">Auditoría</a>
                </li>
                <li class="nav-item" role="presentation" wire:ignore>
                    <a class="nav-link @if($order->status != 91) disabled @endif" id="order-deliveries-tab" data-toggle="tab" href="#order-deliveries" role="tab"
                        aria-controls="order-deliveries" aria-selected="true">Entregas</a>
                </li>
            </ul>
            <div class="tab-content" id="orderContent">
                <div class="tab-pane fade @if($this->order->id == null ) show active @endif" id="order-header" role="tabpanel"
                    aria-labelledby="order-header-tab" wire:ignore.self>
                    @include('livewire.orders.partials.header')
                </div>
                <div class="tab-pane fade @if($this->order->id != null ) show active @endif" id="order-lines" role="tabpanel"
                    aria-labelledby="order-lines-tab" wire:ignore.self>
                    @livewire('orders.order-lines-form',[
                        'orderId' => $order->id,
                        'editable' => $editable
                    ])
                </div>
                <div class="tab-pane fade" id="order-messages" role="tabpanel"
                    aria-labelledby="order-messages-tab" wire:ignore.self>
                    @livewire('common.comments',[
                        'entityId' => $order->id,
                        'entityType' => 'App\Order',
                        'textTitle' => 'Chat interno',
                        'editable' => true
                    ])
                </div>
                <div class="tab-pane fade" id="order-files" role="tabpanel"
                    aria-labelledby="order-files-tab" wire:ignore.self>
                    @livewire('common.file-upload',[
                        's3' => true,
                        'path' => config('custom.iworking_public_bucket_folder_orders') . '/' . now()->format('Y/m/d') . '/'
                        . (string)$order->id,
                        'modelId' => (string)$order->id,
                        'model' => 'App\Order',
                        'type' => 'order-attachment',
                        'enableUpload' => $editable ? true : ($uploadFiles ? true : false),
                        'enableDelete' => $editable ? true : ($uploadFiles ? true : false),
                    ])
                </div>
                <div class="tab-pane fade" id="order-audit" role="tabpanel"
                    aria-labelledby="order-audit-tab" wire:ignore.self>
                    @livewire('common.audit-table',[
                        'dataValue' => $order
                    ])
                </div>
                <div class="tab-pane fade" id="order-deliveries" role="tabpanel"
                    aria-labelledby="order-deliveries-tab" wire:ignore.self>
                    @livewire('orders.delivery-confirmation-form',[
                        'order' => $this->order,
                        'editable' => auth()->user()->hasAnyRole(['direccion-de-pedidos','direccion-general']) ? false : true
                    ])
                </div>
            </div>
        </div>
    </div>
    <hr>
    @if($task)
    <div class="row">
        <div class="col-12 col-md-6">
            @if($this->order->bpm_id == null)
                @if($enableComplete)
                    <button type="button" wire:click="initProcess" class="btn btn-sm btn-success d-flex p-4 py-lg-2"
                        wire:loading.attr="disabled" >
                        Emitir
                    </button>
                @endif
            @else
                @livewire('process.actions',[
                    'actions' => $actions,
                    'processInstance' => $processInstance
                ])
            @endif
        </div>
        <div class="col-12 col-md-6">
            <div class="text-right">
                @if(!isset($processInstance))
                <div class="btn-group my-1" role="group" aria-label="">
                    <button type="button" wire:click="saveOrder" class="btn btn-sm btn-success d-flex p-4 py-lg-2"
                        wire:loading.attr="disabled"
                        @if(!$orderType) disabled @endif>
                        Guardar borrador
                    </button>
                    @if(($order->bpm_id == null)&&($order->user_id == auth()->id()))
                    <button type="button" class="btn btn-sm btn-danger d-flex p-4 py-lg-2" wire:loading.attr="disabled"
                            onclick="return confirm('{{ __("backend.messages.delete-confirmation") }}') || event.stopImmediatePropagation()"
                            wire:click="deleteOrder">
                        Borrar pedido
                    </button>
                    @endif
                </div>
                @else
                <div class="btn-group my-1" role="group" aria-label="">
                    <a href="{{ route('order.tasks') }}" class="btn btn-sm btn-success d-flex p-4 py-lg-2">
                        Salir
                    </a>
                </div>
                @endif

            </div>
        </div>
    </div>
    @else
        @if($order->status == 8)
        <div class="row">
            <div class="col-12 col-md-6">
                <div class="btn-group my-1" role="group" aria-label="">
                    <a href="{{ route('order.document',['order' => $order->id]) }}" target="_blank" wire:click="generatePdf" class="btn btn-sm btn-success d-flex p-4 py-lg-2">
                        Generar Documento de pedido
                    </a >
                </div>
            </div>
            <div class="col-12 col-md-6">
                <div class="btn-group my-1" role="group" aria-label="">
                    <a href="{{ route('order.tasks') }}" class="btn btn-sm btn-success d-flex p-4 py-lg-2">
                        Salir
                    </a>
                </div>
            </div>
        </div>
        @endif
    @endif
</div>
