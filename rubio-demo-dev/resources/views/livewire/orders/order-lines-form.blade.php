<div>
    <div class="row">
        <div class="col-12">
            <h5>Nueva línea</h5>
        </div>
    </div>
    <div class="row">
        <div class="col-12">
            @if (session()->has('alert'))
                <div class="alert alert-danger">
                    {{ session('alert') }}
                </div>
            @endif
            @if (session()->has('success'))
                <div class="alert alert-success">
                    {{ session('success') }}
                </div>
            @endif
        </div>
    </div>
    @if($editable)
    <div class="row">
        <div class="col-12 col-md-3">
            <div class="form-group">
                <label for="cost_center_id" class="form-control-label">Centro de coste *:</label>
                @if(count($costCenters)>0)
                <select wire:model="orderLine.cost_center_id" class="form-control">
                    <option value="">Seleccionar centro de coste</option>
                    @foreach ($costCenters as $costCenterItem)
                    <option value="{{ $costCenterItem->id }}">{{ $costCenterItem->name }}
                        ({{ (int)$costCenterItem->external_id }})</option>
                    @endforeach
                </select>
                @error('orderLine.cost_center_id') <span class="error text-danger">{{ $message }}</span>@enderror
                @else
                <input type="text" wire:model="orderLine.cost_center_id" class="form-control" readonly>
                @endif
            </div>
        </div>
        @if(auth()->user()->hasRole('marketing'))
        <div class="col-12 col-md-3">
            <div class="form-group" {{--wire:loading.remove--}}>
                <label for="budget_id" class="form-control-label">Agrupación presupuestaria *:
                    <span wire:loading wire:target="orderLine.cost_center_id" style="padding-left: 15px; vertical-align: middle">
                        <div class="la-ball-clip-rotate la-dark la-sm" style="display: inline-block;">
                            <div class="align-content-center center-block"></div>
                        </div>
                    </span>
                </label>
                <select wire:model="budgetAgrupationId" class="form-control">
                    <option value="">Seleccionar agrupación presupuestaria</option>
                    <option value="others">Sin agrupación</option>
                    @foreach ($budgetAgrupations as $agrupation)
                    <option value="{{ $agrupation->id }}">{{ $agrupation->name }}</option>
                    @endforeach
                </select>
                @error('orderLine.budget_id') <span class="error text-danger">{{ $message }}</span>
                @enderror
            </div>
        </div>
        @endif
        <div class="col-12 col-md-3">
            <div class="form-group" {{--wire:loading.remove--}}>
                <label for="budget_id" class="form-control-label">Partida presupuestaria *:
                    <span wire:loading
                    @if(auth()->user()->hasRole('marketing'))
                        wire:target="budgetAgrupationId"
                    @else
                        wire:target="orderLine.cost_center_id"
                    @endif
                    style="padding-left: 15px; vertical-align: middle">
                        <div class="la-ball-clip-rotate la-dark la-sm" style="display: inline-block;">
                            <div class="align-content-center center-block"></div>
                        </div>
                    </span>
                </label>
                <select wire:model="orderLine.budget_id" class="form-control">
                    <option value="">Seleccionar partida presupuestaria</option>
                    @foreach ($pepElements as $pepElement)
                    <option value="{{ $pepElement->id }}">{{ $pepElement->name }}</option>
                    @endforeach
                </select>
                @error('orderLine.budget_id') <span class="error text-danger">{{ $message }}</span>
                @enderror
            </div>
        </div>

        <div class="col-12 col-md-2">
            <div class="form-group">
                <label for="document_type" class="form-control-label">Cantidad *:</label>
                <input type="text" wire:model="orderLine.qty" class="form-control" step="0.01">
                @error('orderLine.qty') <span class="error text-danger">{{ $message }}</span> @enderror
            </div>
        </div>
        <div class="col-12 col-md-2">
            <div class="form-group" {{--wire:ignore--}}>
                <label for="unit" class="form-control-label">Unidad *:</label>
                {{--@livewire('components.select.custom-select', [
                    'objectValue'                   => '3836249a-28f2-4168-82a8-3d9099aed8f2',
                    'selectName'                    => 'measureUnitIdCustomSelect',
                    'selectId'                      => 'measureUnitIdCustomSelect',
                    'modelToCreateOptions'          => App\MesureUnit::class,
                    'optionIdFieldMapping'          => 'id',
                    'includeJsCdn'                  => true,
                    'placeholder'                   => 'Seleccione una unidad de medida',
                    'parentComponentName'           => App\Http\Livewire\Orders\OrderLinesForm::getName(),
                    'parentEvent'                   => 'changedCustomSelect',
                    'modelPropertyName'             => 'mesure_unit_id',
                    'checkValuesDebugButton'        => false,
                    'optionFieldOrderBy'            => 'name',
                    'optionFieldOrderDirection'     => 'asc',
--}}{{--                    'hardResetSelect'               => true,--}}{{--
                ], key('measureUnitIdCustomSelect'))--}}
                <select wire:model="orderLine.mesure_unit_id" class="form-control" id="orderLine.mesure_unit_id">
                    <option value="">Seleccionar unidad</option>
                    @foreach ($mesureUnits as $mesureUnit)
                        @if($mesureUnit->name != '')
                            <option value="{{ $mesureUnit->id }}">{{ $mesureUnit->name }} ({{ $mesureUnit->unit }})</option>
                        @endif
                    @endforeach
                </select>
                @error('orderLine.mesure_unit_id') <span class="error text-danger">{{ $message }}</span>@enderror
            </div>
        </div>
        <div class="col-12 col-md-2">
            <div class="form-group">
                <label for="orderLine.net_amount" class="form-control-label">Precio unitario *:</label>
                <input type="text" wire:model="orderLine.net_amount" class="form-control" step="0.0001">
                @error('orderLine.net_amount') <span class="error text-danger">{{ $message }}</span>@enderror
            </div>
        </div>
        <div class="col-12 col-md-2">
            <div class="form-group">
                <label for="fecha" class="form-control-label">Moneda *:</label>
                <select wire:model="orderLine.currency" class="form-control">
                    <option value="">Seleccionar moneda</option>
                        @foreach ($currencies as $currency)
                            <option value="{{ Str::upper($currency) }}">{{ Str::upper($currency) }}</option>
                        @endforeach
                </select>
                {{--@livewire('components.select.custom-select', [
                    'objectValue'                   => 'EUR',
                    'selectName'                    => 'currencyCustomSelect',
                    'selectId'                      => 'currencyCustomSelect',
                    'modelToCreateOptions'          => App\Currency::class,
                    'scopeToCreateOptions'          => 'getShortList',
                    'optionIdFieldMapping'          => 'code',
                    'includeJsCdn'                  => false,
                    'placeholder'                   => 'Seleccione una moneda',
                    'parentComponentName'           => App\Http\Livewire\Orders\OrderLinesForm::getName(),
                    'parentEvent'                   => 'changedCustomSelect',
                    'modelPropertyName'             => 'currency',
                    'checkValuesDebugButton'        => false,
                    'optionFieldOrderBy'            => 'name',
                    'optionFieldOrderDirection'     => 'asc',
--}}{{--                    'hardResetSelect'               => true,--}}{{--
                ], key('currencyCustomSelect'))--}}
                @error('orderLine.currency') <span class="error text-danger">{{ $message }}</span>@enderror
            </div>
        </div>
        <div class="col-12 col-md-2">
            <div class="form-group">
                <label for="orderLine.delivery_date" class="form-control-label">Fecha de entrega
                    *:</label>
                <input type="date" wire:model="orderLine.delivery_date" id="fecha" class="form-control"
                    min="{{ today()->format('Y-m-d') }}">
                @error('orderLine.delivery_date') <span class="error text-danger">{{ $message }}</span>
                @enderror
            </div>
        </div>
        <div class="col-12 col-md-4">
            <div class="form-group">
                <label for="description" class="form-control-label">Descripción material *:</label>
                <textarea wire:model.lazy="orderLine.description"
                          class="form-control {{ $errors->has('orderLine.description') ? ' is-invalid' : '' }}"
                          cols="10" rows="1">
                </textarea>
{{--                <input type="text" wire:model="orderLine.description" id="description" class="form-control">--}}
                @error('orderLine.description') <span class="error text-danger">{{ $message }}</span>
                @enderror
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-12 col-md-12">
            <div class="form-group">
                <div class="kt-checkbox-list">
                    <span class="kt-switch kt-switch--success kt-switch--sm">
                        <label>
{{--                            <input wire:click="$toggle('showPaObjectData')" name="togglePaObjectData" type="checkbox"--}}
                            <input {{--wire:click="togglePaObjectData"--}} id="togglePaObjectData" name="togglePaObjectData" type="checkbox"
                                   class="form-control">
                            <p style="display:inline-flex; padding-top: 5px">Ingresar datos Objeto PA</p>
                            <span></span>
                        </label>
                    </span>
                    <p>Marcad esta casilla y cumplimentar los datos relativos al Objeto PA.</p>
                </div>
            </div>
        </div>

{{--        @if ($showPaObjectData)--}}
            <div id="PAData" class="row" style="display: none" wire:ignore>
                <hr />
                <div class="col-12 col-md-4">
                    <div class="form-group">
                        <label for="pa_business_area" class="form-control-label">Area de negocio:</label>
                        @if (count($paBusinessAreas) > 0)
                            <select wire:model="orderLine.pa_business_area" class="form-control">
                                <option value="">Seleccionar area de negocio</option>
                                @foreach ($paBusinessAreas as $paBusinessArea)
                                    <option value="{{ $paBusinessArea->area }}">{{ $paBusinessArea->name }}
                                        ({{ $paBusinessArea->area }})</option>
                                @endforeach
                            </select>
                            @error('orderLine.pa_business_area') <span class="error text-danger">{{ $message }}</span>@enderror
                        @else
                            <input type="text" wire:model="orderLine.pa_business_area" class="form-control" readonly>
                        @endif
                    </div>
                </div>
                <div class="col-12 col-md-4">
                    <div class="form-group">
                        <label for="pa_distribution_channel" class="form-control-label">Canal de distribución:</label>
                        @if (count($paDistributionChannels) > 0)
                            <select wire:model="orderLine.pa_distribution_channel" class="form-control">
                                <option value="">Seleccionar canal de distribución</option>
                                @foreach ($paDistributionChannels as $paDistributionChannel)
                                    <option value="{{ $paDistributionChannel->channel }}">{{ $paDistributionChannel->name }}
                                        ({{ $paDistributionChannel->channel }})</option>
                                @endforeach
                            </select>
                            @error('orderLine.pa_distribution_channel') <span class="error text-danger">{{ $message }}</span>@enderror
                        @else
                            <input type="text" wire:model="orderLine.pa_distribution_channel" class="form-control" readonly>
                        @endif
                    </div>
                </div>
                <div class="col-12 col-md-4">
                    <div class="form-group">
                        <label for="pa_country" class="form-control-label">País:</label>
                        @if (count($countries) > 0)
                            <select wire:model="orderLine.pa_country" class="form-control">
                                <option value="">Seleccionar país</option>
                                @foreach ($countries as $country)
                                    <option value="{{ $country->code }}">{{ $country->name }}</option>
                                @endforeach
                            </select>
                            @error('orderLine.pa_country') <span class="error text-danger">{{ $message }}</span>@enderror
                        @else
                            <input type="text" wire:model="orderLine.pa_country" class="form-control" readonly>
                        @endif
                    </div>
                </div>
                <div class="col-12 col-md-4">
                    <div class="form-group">
                        <label for="pa_order" class="form-control-label">Orden:</label>
                        @if (count($paOrders) > 0)
                            <select wire:model="orderLine.pa_order" class="form-control">
                                <option value="">Seleccionar orden</option>
                                @foreach ($paOrders as $paOrder)
                                    <option value="{{ $paOrder->order }}">{{ $paOrder->name }}
                                        ({{ $paOrder->order }})</option>
                                @endforeach
                            </select>
                            @error('orderLine.pa_order') <span class="error text-danger">{{ $message }}</span>@enderror
                        @else
                            <input type="text" wire:model="orderLine.pa_order" class="form-control" readonly>
                        @endif
                    </div>
                </div>
                <div class="col-12 col-md-4">
                    <div class="form-group">
                        <label for="pa_person" class="form-control-label">Persona:</label>
                        @if (count($paPersons) > 0)
                            <select wire:model="orderLine.pa_person" class="form-control">
                                <option value="">Seleccionar persona</option>
                                @foreach ($paPersons as $paPerson)
                                    <option value="{{ $paPerson->person }}">{{ $paPerson->name }}
                                        ({{ $paPerson->person }})</option>
                                @endforeach
                            </select>
                            @error('orderLine.pa_person') <span class="error text-danger">{{ $message }}</span>@enderror
                        @else
                            <input type="text" wire:model="orderLine.pa_person" class="form-control" readonly>
                        @endif
                    </div>
                </div>
                <div class="col-12 col-md-4">
                    <div class="form-group">
                        <label for="pa_family" class="form-control-label">Familia:</label>
                        @if (count($paClients) > 0)
                            <select wire:model="orderLine.pa_family" class="form-control">
                                <option value="">Seleccionar familia</option>
                                @foreach ($paFamilies as $paFamily)
                                    <option value="{{ $paFamily->family }}">{{ $paFamily->name }}
                                        ({{ $paFamily->family }})</option>
                                @endforeach
                            </select>
                            @error('orderLine.pa_family') <span class="error text-danger">{{ $message }}</span>@enderror
                        @else
                            <input type="text" wire:model="orderLine.pa_family" class="form-control" readonly>
                        @endif
                    </div>
                </div>
                <div class="col-12 col-md-4">
                    <div class="form-group">
                        <label for="pa_client" class="form-control-label">Cliente:</label>
                        @if (count($paClients) > 0)
                            <select wire:model="orderLine.pa_client" class="form-control">
                                <option value="">Seleccionar cliente</option>
                                @foreach ($paClients as $paClient)
                                    <option value="{{ $paClient->client }}">{{ $paClient->name }}
                                        ({{ $paClient->client }})</option>
                                @endforeach
                            </select>
                            @error('orderLine.pa_client') <span class="error text-danger">{{ $message }}</span>@enderror
                        @else
                            <input type="text" wire:model="orderLine.pa_client" class="form-control" readonly>
                        @endif
                    </div>
                </div>
            </div>
{{--        @endif--}}
    </div>
    <div class="row">
        <div class="col-12">
            <div class="text-right">
                <span wire:loading wire:target="addLine, cancelLine" style="padding-right: 10px; vertical-align: middle">
                    <div class="la-ball-clip-rotate la-dark la-m" style="display: inline-block;">
                        <div class="align-content-center center-block"></div>
                    </div>
                </span>
                <button type="button" wire:click="addLine" class="btn btn-success btn-sm" {{ $addLineDisabled ? 'disabled' : null }}>
                    @if(null == $orderLine->id)
                    Añadir linea
                    @else
                    Guardar linea
                    @endif
                </button>
                <button type="button" wire:click="cancelLine" class="btn btn-danger btn-sm">
                    @if(null == $orderLine->id)
                    Cancelar linea
                    @else
                    No guardar cambios
                    @endif
                </button>
            </div>
        </div>
    </div>
    @endif
    <div class="row mt-4">
        <div class="col-12">
            <table class="table table-striped table-bordered table-hover table-checkable">
                <thead>
                    <tr>
                        <th></th>
                        <th>Posición</th>
                        <th>Descripción Material</th>
                        <th>Centro de coste</th>
                        <th>Partida presupuestaria</th>
                        <!--<th>Imputación</th>
                        <th>Localización</th>-->
                        <th>Cantidad</th>
                        <th>Unidad de medida</th>
                        <th>Precio Unitario</th>
                        <th>Total</th>
                        <th>Moneda</th>
                        <th>Fecha de entrega</th>
                        @if($editable)
                        <th>Action</th>
                        @endif
                    </tr>
                </thead>
                <tbody>
                    @forelse ($lines as $line)
                    <tr>
                        <td>
                            @if ($line->PaObjectDataAvailable())
                                <a class="" href="#collapseRow-{{ $line->id }}" data-toggle="collapse" role="button" aria-expanded="false" aria-controls="collapseRow">
                                    <i class="fa fa-caret-right"></i> PA
                                </a>
                            @endif
                        </td>
                        <td>{{ $line->position }}</td>
                        <td>{{ $line->description ?? '-' }}</td>
                        <td>
                            {{ $line->costCenter->name }} ({{ (int)$line->costCenter->external_id }})
                        </td>
                        <td>{{ $line->budget->name ?? '-' }}</td>

                        <td>{{ number_format($line->qty,2,',','.') ?? '-' }}</td>
                        <td>{{ $line->mesureUnit->unit ?? '-' }}</td>
                        <td>{{ number_format($line->net_amount,4,',','.') ?? '-' }}</td>
                        <td>{{ number_format($line->qty * $line->net_amount,4,',','.') ?? '-' }}</td>
                        <td>{{ Str::upper($line->currency) }}</td>
                        <td>{{ auth()->user()->applyDateFormat($line->delivery_date) ?? '-' }}</td>
                        @if($editable)
                        <td>

                            <div class="btn-group my-1" role="group" aria-label="">
                                <button wire:click="editLine('{{ $line->id }}')" class="btn btn-sm btn-warning pl-3 pr-2"
                                    title="Editar línea"><i class="fa fa-edit"></i></button>
                                <button class="btn btn-sm btn-danger pl-3 pr-2"
                                    onclick="confirm('{{ trans('backend.messages.delete-confirmation') }}') || event.stopImmediatePropagation();"
                                    wire:click="deleteLine('{{ $line->id }}')"
                                    title="Eliminar línea"><i class="fa fa-trash"></i></button>
                            </div>

                        </td>
                        @endif
                    </tr>
                    <tr class="collapse table-in-table-row" id="collapseRow-{{ $line->id }}"  wire:ignore.self>
                        <td></td>
                        <td colspan="12">
                            <table class="pa-object-table">
                                <tr>
                                    <td class="table-header font-weight-bold" style="width: 16%;">Área de negocio</td>
                                    <td class="table-header font-weight-bold" style="width: 14%;">Canal de distribución</td>
                                    <td class="table-header font-weight-bold" style="width: 14%;">País</td>
                                    <td class="table-header font-weight-bold" style="width: 14%;">Orden</td>
                                    <td class="table-header font-weight-bold" style="width: 14%;">Persona</td>
                                    <td class="table-header font-weight-bold" style="width: 14%;">Familia</td>
                                    <td class="table-header font-weight-bold" style="width: 14%;">Cliente</td>
                                </tr>
                                <tr class="nohover">
                                    <td>{{ $paBusinessAreas->where('area', $line->pa_business_area)->first()->name ?? '--' }}</td>
                                    <td>{{ $paDistributionChannels->where('channel', $line->pa_distribution_channel)->first()->name ?? '--' }}</td>
                                    <td>{{ $line->pa_country }}</td>
                                    <td>{{ $paOrders->where('order', $line->pa_order)->first()->name ?? '--' }}</td>
                                    <td>{{ $paPersons->where('person', $line->pa_person)->first()->name ?? '--' }}</td>
                                    <td>{{ $paFamilies->where('family', $line->pa_family)->first()->name ?? '--' }}</td>
                                    <td>{{ $paClients->where('client', $line->pa_client)->first()->name ?? '--' }}</td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    @empty
                    <tr>
                        <td colspan="11">
                            Aún no se han añadido líneas
                        </td>
                    </tr>
                    @endforelse
                </tbody>
            </table>
            {{ $lines->links() }}
        </div>
    </div>
</div>
@push('js')
    <script type="text/javascript">
        $(document).ready(function () {
            $('#togglePaObjectData').on('click', function (e) {
                console.log('togglePaObjectData');
                $('#PAData').fadeToggle(500);
                //@this.set($(this).attr("wiremodel"),e.target.value);
                //$wire.resetPAData()
                Livewire.emit('resetPADataListener')

            });
        });
    </script>
@endpush

<style>
    .table-in-table-row td {
        vertical-align: middle !important;
    }
    .pa-object-table {
        position: relative;
        margin: 0 auto;
        padding: 0;
        width: 100%;
        height: auto;
        border-collapse: collapse;
        background-color: #b3e8ca;
    }
    .pa-object-table > tbody > tr:hover {
        background:#b3e8ca;
    }
    .table-header {
        background-color: #b3d7f5;
    }

    <!-- Loading spinner styles -->
    /*!
 * Load Awesome v1.1.0 (http://github.danielcardoso.net/load-awesome/)
 * Copyright 2015 Daniel Cardoso <@DanielCardoso>
 * Licensed under MIT
 */
    .clip-rotate-center {
        display: block;
        margin-left: auto;
        margin-right: auto;
    }
    .la-ball-clip-rotate,
    .la-ball-clip-rotate > div {
        position: relative;
        -webkit-box-sizing: border-box;
        -moz-box-sizing: border-box;
        box-sizing: border-box;
    }
    .la-ball-clip-rotate {
        display: block;
        font-size: 0;
        color: #fff;
    }
    .la-ball-clip-rotate.la-dark {
        color: #333;
    }
    .la-ball-clip-rotate > div {
        display: inline-block;
        float: none;
        background-color: currentColor;
        border: 0 solid currentColor;
    }
    .la-ball-clip-rotate {
        width: 32px;
        height: 32px;
    }
    .la-ball-clip-rotate > div {
        width: 32px;
        height: 32px;
        background: transparent;
        border-width: 2px;
        border-bottom-color: transparent;
        border-radius: 100%;
        -webkit-animation: ball-clip-rotate .75s linear infinite;
        -moz-animation: ball-clip-rotate .75s linear infinite;
        -o-animation: ball-clip-rotate .75s linear infinite;
        animation: ball-clip-rotate .75s linear infinite;
    }
    .la-ball-clip-rotate.la-sm {
        width: 16px;
        height: 16px;
    }
    .la-ball-clip-rotate.la-sm > div {
        width: 16px;
        height: 16px;
        border-width: 1px;
    }
    .la-ball-clip-rotate.la-m {
        width: 22px;
        height: 22px;
    }
    .la-ball-clip-rotate.la-m > div {
        width: 22px;
        height: 22px;
        border-width: 1px;
    }
    .la-ball-clip-rotate.la-2x {
        width: 64px;
        height: 64px;
    }
    .la-ball-clip-rotate.la-2x > div {
        width: 64px;
        height: 64px;
        border-width: 4px;
    }
    .la-ball-clip-rotate.la-3x {
        width: 96px;
        height: 96px;
    }
    .la-ball-clip-rotate.la-3x > div {
        width: 96px;
        height: 96px;
        border-width: 6px;
    }
    /*
     * Animation
     */
    @-webkit-keyframes ball-clip-rotate {
        0% {
            -webkit-transform: rotate(0deg);
            transform: rotate(0deg);
        }
        50% {
            -webkit-transform: rotate(180deg);
            transform: rotate(180deg);
        }
        100% {
            -webkit-transform: rotate(360deg);
            transform: rotate(360deg);
        }
    }
    @-moz-keyframes ball-clip-rotate {
        0% {
            -moz-transform: rotate(0deg);
            transform: rotate(0deg);
        }
        50% {
            -moz-transform: rotate(180deg);
            transform: rotate(180deg);
        }
        100% {
            -moz-transform: rotate(360deg);
            transform: rotate(360deg);
        }
    }
    @-o-keyframes ball-clip-rotate {
        0% {
            -o-transform: rotate(0deg);
            transform: rotate(0deg);
        }
        50% {
            -o-transform: rotate(180deg);
            transform: rotate(180deg);
        }
        100% {
            -o-transform: rotate(360deg);
            transform: rotate(360deg);
        }
    }
    @keyframes ball-clip-rotate {
        0% {
            -webkit-transform: rotate(0deg);
            -moz-transform: rotate(0deg);
            -o-transform: rotate(0deg);
            transform: rotate(0deg);
        }
        50% {
            -webkit-transform: rotate(180deg);
            -moz-transform: rotate(180deg);
            -o-transform: rotate(180deg);
            transform: rotate(180deg);
        }
        100% {
            -webkit-transform: rotate(360deg);
            -moz-transform: rotate(360deg);
            -o-transform: rotate(360deg);
            transform: rotate(360deg);
        }
    }
</style>
