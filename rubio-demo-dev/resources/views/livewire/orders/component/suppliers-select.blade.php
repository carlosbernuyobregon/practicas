<div>
    <div wire:ignore>
        <div class="input-group mb-3">
            <select class="form-control" id="suppliersSelect">
                <option value="">-- Seleccionar proveedor --</option>
                @foreach ($suppliers as $supplierL)
                @if(!Str::startsWith($supplierL->name,'**'))
                <option value="{{ $supplierL->id }}">{{ (integer)$supplierL->external_id }} -
                    {{ $supplierL->name }}
                    @endif
                </option>
                @endforeach
            </select>
            <div class="input-group-append">
                <button wire:click="newSupplier" class="btn btn-primary" type="button">
                    Alta de proveedor
                </button>
            </div>
        </div>

    </div>
</div>
@push('css')
<link href="{{ config('custom.iworking_public_bucket') }}/assets/vendors/general/select2/dist/css/select2.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/select2-bootstrap-5-theme@1.1.1/dist/select2-bootstrap-5-theme.min.css" />
@endpush
@push('js')
<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
    <script>
        $(document).ready(function() {

        $('#suppliersSelect').select2({
            theme: "bootstrap-5",
        });

        $('#suppliersSelect').on('change', function (e) {

            var data = $('#suppliersSelect').select2("val");

            @this.set('supplier', data);

        });

    });

    </script>
@endpush
