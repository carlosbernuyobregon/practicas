<div>
    <div class="row">
        <div class="col-12">
            <h5>Entregas de pedidos</h5>
        </div>
    </div>
    <div class="row">
        <div class="col-12">
            @if (session()->has('alert'))
                <div class="alert alert-danger">
                    {{ session('alert') }}
                </div>
            @endif
            @if (session()->has('success'))
                <div class="alert alert-success">
                    {{ session('success') }}
                </div>
            @endif
        </div>
    </div>
    @if($editable)
    <div class="row">
        <div class="col-12 col-md-6">
            <div class="form-group">
                <label for="cost_center_id" class="form-control-label">Línea de producto *:</label>
                <select wire:model="delivery.order_line_id" class="form-control">
                    <option value="">Seleccionar producto entregado</option>
                    @foreach ($order->orderLines as $orderLine)
                    <option value="{{ $orderLine->id }}">{{ $orderLine->description }} - cantidad total: {{ $orderLine->qty ?? '-' }}</option>
                    @endforeach
                </select>
                @error('delivery.order_line_id') <span class="error text-danger">{{ $message }}</span>@enderror
            </div>
        </div>
        <div class="col-12 col-md-3">
            {{--<div class="form-group">
                <label for="delivery.amount" class="form-control-label">Importe entregado *:</label>
                <input type="text" wire:model="delivery.amount" class="form-control" step="0.0001">
                @error('delivery.amount') <span class="error text-danger">{{ $message }}</span>@enderror
            </div>--}}
            <div class="form-group">
                <label for="delivery.qty" class="form-control-label">Cantidad entregada *:</label>
                <input type="text" wire:model="delivery.qty" class="form-control">
                @error('delivery.qty') <span class="error text-danger">{{ $message }}</span>@enderror
            </div>
        </div>
        <div class="col-12 col-md-3">
            <div class="form-group">
                <label for="delivery.delivered_at" class="form-control-label">Fecha de entrega
                    *:</label>
                <input type="date" wire:model="delivery.delivered_at" id="fecha" class="form-control"
                    min="{{ today()->format('Y-m-d') }}">
                @error('delivery.delivered_at') <span class="error text-danger">{{ $message }}</span>
                @enderror
            </div>
        </div>
        <div class="col-12">
            <div class="text-right">
                <button type="button" wire:click="addLine" class="btn btn-success btn-sm" {{ $addLineDisabled ? 'disabled' : null }}>
                    @if(null == $delivery->id)
                    Añadir linea
                    @else
                    Guardar linea
                    @endif
                </button>
                <button type="button" wire:click="cancelLine" class="btn btn-danger btn-sm">
                    @if(null == $delivery->id)
                    Cancelar linea
                    @else
                    No guardar cambios
                    @endif
                </button>
            </div>
        </div>
    </div>
    @endif
    <div class="row mt-2">
        <div class="col-12">
            <table class="table table-striped table-bordered table-hover table-checkable">
                <thead>
                    <tr>
                        <th>Posición</th>
                        <th>Fecha entrega</th>
                        <th>Descripción Material</th>
                        <th>Entregado (cantidad)</th>
                        @if($editable)
                        <th>Action</th>
                        @endif
                    </tr>
                </thead>
                <tbody>
                    @forelse ($deliveries as $item)
                    <tr>
                        <td>{{ $item->position ?? '-' }}</td>
                        <td>{{ auth()->user()->applyDateFormat($item->delivered_at) ?? '-' }}</td>
                        <td>{{ $item->orderLine->description ?? '-' }}</td>
                        <td>
{{--                            {{ number_format($item->amount,4,',','.') ?? '-' }}--}}
                            {{ $item->qty ?? '-' }}
                        </td>
                        <td>
                            @if(!in_array($item->status, [91,92]))
                            <div class="btn-group my-1" role="group" aria-label="">
                                <button wire:click="editLine('{{ $item->id }}')" class="btn btn-sm btn-warning pl-3 pr-2"
                                    title="Editar línea"><i class="fa fa-edit"></i></button>
                                <button class="btn btn-sm btn-danger pl-3 pr-2"
                                    onclick="confirm('{{ trans('backend.messages.delete-confirmation') }}') || event.stopImmediatePropagation();"
                                    wire:click="deleteLine('{{ $item->id }}')"
                                    title="Eliminar línea"><i class="fa fa-trash"></i></button>
                            </div>
                            @else
                            @lang('backend.deliveries.status.' . $item->status)
                            @endif
                        </td>
                    </tr>
                    @empty
                    <tr>
                        <td colspan="11">
                            Aún no se han añadido entregas
                        </td>
                    </tr>
                    @endforelse
                </tbody>
            </table>
            {{ $deliveries->links() }}
        </div>
    </div>
</div>
