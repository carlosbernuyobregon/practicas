<div>
    <div class="kt-portlet__head kt-portlet__head--lg">
        <div class="kt-portlet__head-label">
            <span class="kt-portlet__head-icon">
                <i class="kt-font-brand flaticon2-line-chart"></i>
            </span>
            <h3 class="kt-portlet__head-title text-uppercase">
                Pedidos
            </h3>
        </div>
        <div class="kt-portlet__head-toolbar">
            <div class="kt-portlet__head-wrapper">
                <div class="kt-portlet__head-actions">
                    <a href="{{ route('order.new') }}" class="btn btn-primary btn-sm btn-icon-sm" data-toggle="tooltip"
                        data-placement="top" title="Create">
                        <i class="fas fa-plus"></i>
                        Nuevo pedido
                    </a>
                </div>
            </div>
        </div>
    </div>
    <div class="kt-portlet__body">
        <!--begin: Datatable -->
        <div id="users_list_table_wrapper" class="dataTables_wrapper dt-bootstrap4 no-footer">
            <div class="dt-buttons btn-group mb-3">
                <div class="btn-group">
                    <select wire:model="entries" class="custom-select" tabindex="0" aria-controls="users_list_table"
                        type="button" aria-haspopup="true" aria-expanded="false">
                        <option value="10">@lang('backend.forms.selects.10')</option>
                        <option value="25">@lang('backend.forms.selects.25')</option>
                        <option value="50">@lang('backend.forms.selects.50')</option>
{{--                        <option value="100">@lang('backend.forms.selects.100')</option>--}}
                    </select>
                </div>
                <div class="btn-group">
                    @if (!$filtersMode)
                        <button wire:click="$toggle('filtersMode')" class="btn btn-primary" type="button" data-toggle="collapse" data-target="#collapseFilters" aria-expanded="false" aria-controls="collapseFilters">
                            Mostrar filtros
                        </button>
                    @else
                        <button wire:click="$toggle('filtersMode')" class="btn btn-warning" type="button" data-toggle="collapse" data-target="#collapseFilters" aria-expanded="false" aria-controls="collapseFilters">
                            Ocultar filtros
                        </button>
                    @endif
                </div>
            </div>
            <div class="collapse" id="collapseFilters" wire:ignore.self>
                <div class="row">
                    <div class="col-6 col-lg-10">
                        <h5>Filtros:</h5>
                    </div>
                    <div class="col-6 col-lg-2">
                        @if (session()->has('search'))
                            <button wire:click="clearFilters()" class="btn btn-sm btn-danger btn-inline float-right"><i class="fas fa-times"></i> Eliminar filtros</button>
                        @endif
                    </div>
                </div>
                <div class="row mb-3">
                    <div class="col-6 col-lg-3 col-xl-2 mt-2">
                        <label for="orderNumber" class="font-weight-bold">Núm. Pedido:</label>
                        <input wire:model.debounce.300ms="search.orderNumber" type="search"
                               class="form-control form-control-sm" name="orderNumber" id="orderNumber" placeholder="Núm. Pedido">
                    </div>
                    <div class="col-6 col-lg-3 col-xl-2 mt-2">
                        <label for="company" class="font-weight-bold">Sociedad:</label>
                        <select wire:model.debounce.300ms="search.company" name="company" id="company"
                                class="form-control form-control-sm">
                            <option value=""> ---- </option>
                            <option value="1000">1000 - Rubió</option>
                            <option value="2000">2000 - Products and tecnology</option>
                        </select>
                    </div>
                    <div class="col-6 col-lg-3 col-xl-2 mt-2">
                        <label for="cost_center_id" class="font-weight-bold">Centro de coste:</label>
                        <select wire:model.debounce.300ms="search.costCenter" name="cost_center_id" id="cost_center_id"
                                class="form-control form-control-sm">
                            <option value=""> ---- </option>
                            @foreach($costCenters as $costCenter)
                            <option value="{{ $costCenter->id }}">{{ $costCenter->name }} ({{ (int)$costCenter->external_id }})</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="col-6 col-lg-3 col-xl-2 mt-2">
                        <label for="vatNumber" class="font-weight-bold">NIF/VAT:</label>
                        <input wire:model.debounce.300ms="search.vatNumber" type="search"
                               class="form-control form-control-sm" name="vatNumber" id="vatNumber" placeholder="NIF/VAT">
                    </div>
                    <div class="col-6 col-lg-3 col-xl-2 mt-2">
                        <label for="provider" class="font-weight-bold">Proveedor:</label>
                        <input wire:model.debounce.300ms="search.provider" type="search"
                               class="form-control form-control-sm" name="provider" id="provider" placeholder="Proveedor">
                    </div>
                    <div class="col-6 col-lg-3 col-xl-2 mt-2">
                        <label for="provider" class="font-weight-bold">Aprobador:</label>
                        <input wire:model.debounce.300ms="search.manager" type="search"
                               class="form-control form-control-sm" name="manager" id="manager" placeholder="Aprobador">
                    </div>
                    <div class="col-6 col-lg-3 col-xl-2 mt-2">
                        <label for="orderCompany" class="font-weight-bold">Estado:</label>
                        <select wire:model.debounce.300ms="search.status" name="status" id="status"
                                class="form-control form-control-sm">
                            <option value=""> ---- </option>
                            @foreach(App\Helpers\Helpers::buildOrdersStatusArray() as $status => $name)
                                <option value="{{ $status }}">{{ $name }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="row mb-3">
                    {{-- There is no total field, since is a calculated values, therefore is not possible to query on mutators / appends --}}
                    {{--<div class="col-6 col-lg-3 col-xl-2 mt-2">
                        <label for="total" class="font-weight-bold">Total:</label>
                        <div class="form-row">
                            <div class="form-group col-md-6">
                                <input wire:model.debounce.300ms="search.totalMax" type="search"
                                       class="form-control form-control-sm" name="totalMax" id="totalMax" placeholder="Min">
                            </div>
                            <div class="form-group col-md-6">
                                <input wire:model.debounce.300ms="search.totalMin" type="search"
                                       class="form-control form-control-sm" name="totalMin" id="totalMin" placeholder="Max">
                            </div>
                        </div>
                    </div>--}}
                    {{--Hidden until relationship is defined back--}}
                    {{--<div class="col-6 col-lg-3 col-xl-2 mt-2">
                        <label for="manager" class="font-weight-bold">Aprobador:</label>
                        <input wire:model.debounce.300ms="search.manager" type="search"
                               class="form-control form-control-sm" name="manager" id="manager" placeholder="Aprobador">
                    </div>--}}
                    <div class="col-8 col-lg-6 col-xl-6 mt-2">
                        <label for="createdAt" class="font-weight-bold">Fecha de creación:</label>
                        <div class="form-row">
                            <div class="form-group col-md-6">
                                <label for="dateFrom">Desde</label>
                                <input wire:model.debounce.300ms="search.createdAtFrom" type="date"
                                       class="form-control form-control-sm" name="createdAtFrom" id="createdAtFrom" placeholder="Desde">
                            </div>
                            <div class="form-group col-md-6">
                                <label for="dateTo">Hasta</label>
                                <input wire:model.debounce.300ms="search.createdAtTo" type="date"
                                       class="form-control form-control-sm" name="createdAtTo" id="createdAtTo" placeholder="Hasta">
                            </div>
                        </div>
                    </div>
                    <div class="col-8 col-lg-6 col-xl-6 mt-2">
                        <label for="date" class="font-weight-bold">Fecha de entrega:</label>
                        <div class="form-row">
                            <div class="form-group col-md-6">
                                <label for="dateFrom">Desde</label>
                                <input wire:model.debounce.300ms="search.dateFrom" type="date"
                                       class="form-control form-control-sm" name="dateFrom" id="dateFrom" placeholder="Desde">
                            </div>
                            <div class="form-group col-md-6">
                                <label for="dateTo">Hasta</label>
                                <input wire:model.debounce.300ms="search.dateTo" type="date"
                                       class="form-control form-control-sm" name="dateTo" id="dateTo" placeholder="Hasta">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="table-responsive">
                <table class="table table-striped- table-bordered table-hover table-checkable mt-4" id="users_list_table">
                    <thead>
                        <tr class="text-uppercase">
                            <th wire:click="sortBy('order_number')" style="cursor: pointer">
                                Nº Pedido
                                @include('partials._sort-icon',['field'=>'order_number'])
                            </th>
                            <th wire:click="sortBy('cost_center_id')" style="cursor: pointer">
                                Ceco
                                @include('partials._sort-icon',['field'=>'order_number'])
                            </th>
                            <th wire:click="sortBy('user_id')" style="cursor: pointer">
                                Autor
                                @include('partials._sort-icon',['field'=>'user_id'])
                            </th>
                            <th wire:click="sortBy('company')" style="cursor: pointer">
                                Sociedad
                                @include('partials._sort-icon',['field'=>'company'])
                            </th>
                            <th wire:click="sortBy('created_at')" style="cursor: pointer">
                                F. emisión
                                @include('partials._sort-icon',['field'=>'created_at'])
                            </th>

                            <th wire:click="sortBy('date')" style="cursor: pointer">
                                F. entrega
                                @include('partials._sort-icon',['field'=>'date'])
                            </th>

                            <th {{--wire:click="sortBy('vat_number')" --}}style="cursor: pointer">
                                Nif
                                {{--@include('partials._sort-icon',['field'=>'vat_number'])--}}
                            </th>
                            <th wire:click="sortBy('provider_id')" style="cursor: pointer">
                                Proveedor
                                @include('partials._sort-icon',['field'=>'provider_id'])
                            </th>
                            <th {{--wire:click="sortBy('total_amount')"--}} style="cursor: pointer">
                                Importe
                                {{--@include('partials._sort-icon',['field'=>'total_amount'])--}}
                            </th>
                            <th wire:click="sortBy('status')" style="cursor: pointer">
                                @lang('backend.crud.order.list.status')
                                @include('partials._sort-icon',['field'=>'status'])
                            </th>
                            <th wire:click="sortBy('manager_id')" style="cursor: pointer">
                                Aprobador
                                @include('partials._sort-icon',['field'=>'manager_id'])
                            </th>
                            <th>@lang('backend.crud.actions')</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($orders as $order)
                            <tr>
                                <td>{{ $order->order_number ?? '' }}</td>
                                @if($order->cost_center_id != null)
                                <td>{{ (int)$order->costCenter->external_id ?? '' }}</td>
                                @else
                                <td>&nbsp;</td>
                                @endif
                                <td>{{ $order->user->full_name ?? '' }}</td>
                                <td>{{ $order->company == 1000 ? 'Laboratorios Rubió' : 'Products and Technology' }}</td>
                                <td>{{ $order->created_at != null
                                ? auth()->user()->applyDateFormat($order->created_at)
                                : '' }}
                                </td>

                                <td>{{ auth()->user()->applyDateFormat($order->date) ?? '' }}</td>

                                <td>{{ $order->provider->vat_number ?? ''}}</td>
                                <td>{{ $order->provider->name ?? '' }}</td>
                                <td>{{ number_format($order->totalLines(),2,',','.') }} €</td>
                                <td>{{ trans('backend.orders.status.'.$order->status) }}</td>
                                <td>{{ $order->manager->full_name ?? '' }}</td>
                                <td nowrap>
                                    @if($task)
                                    <a href="{{ route('order.edit',$order->id) }}" type="button"
                                        class="btn btn-sm btn-clean btn-icon btn-icon-md" data-toggle="tooltip"
                                        data-placement="top" title="Edit">
                                        <i class="fas fa-edit"></i>
                                    </a>
                                    @else
                                    <a href="{{ route('order.show',$order->id) }}" type="button"
                                        class="btn btn-sm btn-clean btn-icon btn-icon-md" data-toggle="tooltip"
                                        data-placement="top" title="Edit">
                                        <i class="fas fa-search"></i>
                                    </a>
                                    @endif
                                    @if(Auth::user()->hasRole('admin') && ($order->status == 0))
                                    <button wire:click="confirmDelete('{{ $order->id }}')" type="button"
                                        class="btn btn-sm btn-clean btn-icon btn-icon-md"
                                        title="Delete">
                                        <i class="fas fa-trash"></i>
                                    </button>
                                    @endif
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
            <div class="dataTables_info">
                {{ $orders->links() }}
            </div>
        </div>
    </div>
</div>
