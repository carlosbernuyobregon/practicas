<div>
    <div class="row mb-3">
        <div class="col-6">
            @if($order->order_number != '')
            <h6><strong>Número de pedido:</strong> {{ $order->order_number ?? '' }}</h6>
            @endif
            @if($orderType != null)
            <h6><strong>Tipo de pedido:</strong> {{ $orderTypeValue[$this->orderType] }}</h6>
            @endif
            @if($costCenter != null)
            <p><strong>Centro de coste: </strong>{{ $costCenter->name }} ({{ (int)$costCenter->external_id }})</p>
            @endif
        </div>
        <div class="col-6">
            <div class="text-right">
                <div wire:loading class="mr-2 mt-2">
                    <div class="lds-ring">
                        <div></div>
                        <div></div>
                        <div></div>
                        <div></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-12">
            <ul class="nav nav-tabs" id="myTab" role="tablist">
                <li class="nav-item" role="presentation" wire:ignore>
                    <a class="nav-link @if(!isset($order->id)) active @endif" id="order-header-tab" data-toggle="tab"
                        href="#order-header" role="tab" aria-controls="order-header"
                        aria-selected="@if(!isset($order->id)) true @else false @endif">Cabecera pedido</a>
                </li>
                @if($order->id != null)
                <li class="nav-item" role="presentation" wire:ignore>
                    <a class="nav-link active" id="order-lines-tab" data-toggle="tab" href="#order-lines" role="tab"
                        aria-controls="order-lines" aria-selected="true">Líneas de pedido</a>
                </li>

                <li class="nav-item" role="presentation" wire:ignore>
                    <a class="nav-link" id="internal-messages-tab" data-toggle="tab" href="#internal-messages"
                        role="tab" aria-controls="internal-messages" aria-selected="true">Chat interno</a>
                </li>
                <li class="nav-item" role="presentation" wire:ignore>
                    <a class="nav-link" id="files-tab" data-toggle="tab" href="#files" role="tab" aria-controls="files"
                        aria-selected="true">Archivos</a>
                </li>
                <li class="nav-item" role="presentation" wire:ignore>
                    <a class="nav-link" id="audit-lines-tab" data-toggle="tab" href="#audit-lines" role="tab"
                        aria-controls="audit-lines" aria-selected="true">Auditoria</a>
                </li>
                @endif
            </ul>
            <div class="tab-content" id="myTabContent">
                <div class="tab-pane fade @if(!isset($order->id)) show active @endif" id="order-header" role="tabpanel"
                    aria-labelledby="order-header-tab" wire:ignore.self>
                    <div class="row">
                        @if($orderType == null)
                        <div class="col-12 col-md-4 mb-3">
                            <div class="btn-group my-1" role="group" aria-label="">
                                @if($order->status == App\Library\Constants::ORDER_STATUS_DRAFT)
                                @foreach ($orderTypeValue as $key => $value)
                                <button type="button" wire:click="$set('orderType','{{ $key }}')"
                                    wire:loading.attr="disabled" class="btn btn-sm btn-primary d-flex p-4 py-lg-2"
                                    @if($key!='ceco' ) disabled @endif>
                                    {{ $value }}
                                </button>
                                @endforeach
                                @endif
                            </div>
                            <!--
                            <div class="form-group{{ $errors->has('key_value') ? ' has-danger' : '' }}">
                                <label class="form-control-label" for="input-key_value">Tipo de pedido</label>
                                <select wire:model="orderType" class="form-control">
                                    @foreach ($orderTypeValue as $key => $value)
                                    <option value="{{  $key }}" @if($key == 'ceco') selected @endif>{{ $value }}</option>
                                    @endforeach
                                </select>
                                @error('orderType') <span class="error text-danger">{{ $message }}</span> @enderror
                            </div>
                            -->
                        </div>
                        @else
                        <div class="col-12 col-md-4">
                            <div class="form-group{{ $errors->has('key_value') ? ' has-danger' : '' }}">
                                <label class="form-control-label" for="input-key_value">Sociedad *:</label>
                                @if($order->status != App\Library\Constants::ORDER_STATUS_APPROVED_BY_GENERAL_MANAGEMENT)
                                <select wire:model.defer="orderCompany" class="form-control" @if($order->status == App\Library\Constants::ORDER_STATUS_MODIFICATION_PENDING) disabled
                                    @endif>
                                    <option value="1000" selected="selected">1000 - Rubió</option>
                                    <option value="2000">2000 - Products and tecnology</option>
                                </select>
                                @error('orderCompany') <span class="error text-danger">{{ $message }}</span> @enderror
                                @else
                                <input type="text"
                                    value="{{ ($order->company == 1000) ?  '1000 - Rubió' : '2000 - Products and tecnology' }}"
                                    class="form-control" readonly />
                                @endif
                            </div>
                        </div>

                        <div class="col-12 col-md-3">
                            <div class="form-group">
                                <label for="fecha" class="form-control-label">Fecha</label>
                                <input type="date" wire:model.defer="order.date" value="{{ ($order->order_number == null) ? $order->date : today()->format('d-m-Y') }}" class="form-control" />
                                <!--<input type="date" wire:model="order.date" id="fecha" class="form-control" value="">-->
                                @error('fecha') <span class="error text-danger">{{ $message }}</span> @enderror
                            </div>
                        </div>

                        <div class="col-12 col-md-5">
                            <div class="form-group{{ $errors->has('key_value') ? ' has-danger' : '' }}">
                                <label class="form-control-label" for="input-key_value">Proveedor *:</label>
                                @if($order->status != App\Library\Constants::ORDER_STATUS_APPROVED_BY_GENERAL_MANAGEMENT)
                                @if($newSupplier == null)
                                <div class="input-group mb-3">
                                    @if($order->id == null)
                                    <select class="form-control form-select" id="order.provider_id_id" wireModel="order.provider_id">
                                        <option value="">-- Seleccionar proveedor --</option>
                                        @foreach ($providers as $provider)
                                        @if(!Str::startsWith($provider->name,'**'))
                                        <option value="{{ $provider->id }}" {{ ($order->provider_id != null) ? ($order->provider_id == $provider->id ? 'selected' : '')  : '' }}>
                                            {{ (integer)$provider->external_id }} - {{ $provider->name }} - {{ $provider->vat_number ?? ''}}
                                        </option>
                                        @endif

                                        @endforeach
                                    </select>
                                    <div class="input-group-append">
                                        <button wire:click="newSupplier" class="btn btn-primary" type="button">
                                            Alta de proveedor
                                        </button>
                                    </div>
                                    @else
                                    <input type="text"
                                    value="{{ (int)$order->provider->external_id }} - {{ $order->provider->name }} - {{ $order->provider->vat_number ?? ''}}"
                                    class="form-control" readonly />
                                    @endif
                                </div>
                                @error('order.provider_id') <span class="error text-danger">{{ $message }}</span>
                                @enderror
                                @else
                                <input type="text" wire:model="order.provider_id"
                                    value="{{ $newSupplier->id }} - {{ $newSupplier->name }}"
                                    class="form-control" readonly />
                                @endif
                                @else
                                <input type="text"
                                    value="{{ (integer)$order->provider->external_id }} - {{ $order->provider->name }} - {{ $order->provider->vat_number ?? ''}}"
                                    class="form-control" readonly />
                                @endif
                            </div>
                        </div>
                        <div class="col-12 col-md-4">
                            <div class="form-group{{ $errors->has('deliveryPointOption') ? ' has-danger' : '' }}">
                                <label class="form-control-label" for="input-key_value">Punto de entrega *:</label>
                                <select wire:model="deliveryPointOption" class="form-control" @if($order->status == App\Library\Constants::ORDER_STATUS_MODIFICATION_PENDING) disabled
                                    @endif>
                                    <option value="">Punto de entrega</option>
                                    @foreach (['Castellbisbal','Picking literaturas','Picking','Otros'] as $option)
                                    <option value="{{ $option }}" >{{ $option }}</option>
                                    @endforeach
                                </select>
                                @error('deliveryPointOption') <span class="error text-danger">{{ $message }}</span> @enderror
                            </div>
                        </div>
                        <div class="col-12 col-md-4">
                            @if($deliveryPointOption == 'Otros')
                            <label class="form-control-label" for="input-key_value">Detalle punto de entrega:</label>
                            <input type="text"
                                wire:model="deliveryPointOthers"
                                placeholder="Punto de entrega"
                                class="form-control" />
                            @endif
                        </div>

                        @endif
                        @if(false)
                        @if(isset($order->document_type))
                        <div class="col-12 col-md-3">
                            <div class="form-group">
                                <label for="document_type" class="form-control-label">Clase de documento:</label>
                                <input type="text" wire:model="order.document_type" id="document_type"
                                    class="form-control" readonly>
                            </div>
                        </div>
                        @endif
                        @endif
                    </div>
                    @if($newSupplierForm&&$provider == null)
                    <hr>
                    <div class="row">
                        <div class="col-12">
                            <h5>Alta proveedor</h5>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-12 col-md-4">
                            <div class="form-group{{ $errors->has('key_value') ? ' has-danger' : '' }}">
                                <label class="form-control-label" for="input-key_value">Sociedad *:</label>
                                @if($order->status != App\Library\Constants::ORDER_STATUS_APPROVED_BY_GENERAL_MANAGEMENT)
                                <select wire:model="provider.company" class="form-control" @if($order->status == App\Library\Constants::ORDER_STATUS_MODIFICATION_PENDING) disabled
                                    @endif>
                                    <option value="1000" selected="selected">1000 - Rubió</option>
                                    <option value="2000">2000 - Products and tecnology</option>
                                </select>
                                @error('provider.company') <span class="error text-danger">{{ $message }}</span> @enderror
                                @else
                                <input type="text"
                                    value="{{ ($order->company == 1000) ?  '1000 - Rubió' : '2000 - Products and tecnology' }}"
                                    class="form-control" readonly />
                                @endif
                            </div>
                        </div>
                        <div class="col-12 col-md-4">
                            <div class="form-group {{ $errors->has(`provider.name`) ? ' has-danger' : '' }}">
                                <label for="name">@lang('backend.crud.provider.form.name')</label>
                                <input wire:model="provider.name" type="text" id="name_input"
                                    class="form-control {{ $errors->has('provider.name') ? ' is-invalid' : '' }}"
                                    placeholder="@lang('backend.crud.provider.form.name_placeholder')" />
                                @error('provider.name')
                                <span class="text-danger">{{ $message }}</span>
                                @enderror
                            </div>
                        </div>
                        <div class="col-12 col-md-4">
                            <div class="form-group {{ $errors->has(`provider.vat_number`) ? ' has-danger' : '' }}">
                                <label for="name">@lang('backend.crud.provider.form.vat_number')</label>
                                <input wire:model="provider.vat_number" type="text" id="vat_number_input"
                                    class="form-control {{ $errors->has('provider.vat_number') ? ' is-invalid' : '' }}"
                                    placeholder="@lang('backend.crud.provider.form.vat_number_placeholder')" />
                                @error('provider.vat_number')
                                <span class="text-danger">{{ $message }}</span>
                                @enderror
                            </div>
                        </div>
                        <div class="col-12 col-md-4">
                            <div
                                class="form-group {{ $errors->has(`provider.payment_condition`) ? ' has-danger' : '' }}">
                                <label for="name">@lang('backend.crud.provider.form.payment_condition')</label>
                                <input wire:model="provider.payment_condition" type="text" id="payment_condition_input"
                                    class="form-control {{ $errors->has('provider.payment_condition') ? ' is-invalid' : '' }}"
                                    placeholder="@lang('backend.crud.provider.form.payment_condition')" />
                                @error('provider.payment_condition')
                                <span class="text-danger">{{ $message }}</span>
                                @enderror
                            </div>
                        </div>
                        <div class="col-12 col-md-4">
                            <div class="form-group {{ $errors->has(`provider.payment_type`) ? ' has-danger' : '' }}">
                                <label for="name">@lang('backend.crud.provider.form.payment_type')</label>
                                <input wire:model="provider.payment_type" type="text" id="payment_type_input"
                                    class="form-control {{ $errors->has('provider.payment_type') ? ' is-invalid' : '' }}"
                                    placeholder="@lang('backend.crud.provider.form.payment_type')" />
                                @error('provider.payment_type')
                                <span class="text-danger">{{ $message }}</span>
                                @enderror
                            </div>
                        </div>
                        <div class="col-12 col-md-4">
                            <div class="text-right">
                                <button wire:click="saveSupplier()"
                                    class="btn btn-brand btn-elevate btn-icon-sm">@lang('backend.forms.save')</button>
                                <button wire:click="rejectSupplier()" type="button"
                                    class="btn btn-secondary close-btn">Cancelar</button>
                            </div>
                        </div>
                    </div>
                    <hr>
                    @endif
                    <div class="row">
                        <div class="col-12">
                            <div class="form-group">
                                <label for="description" class="form-control-label">Observaciones:</label>
                                <textarea wire:model.lazy="order.description" id="description" class="form-control"
                                    rows="4" @if($order->status == App\Library\Constants::ORDER_STATUS_MODIFICATION_PENDING) readonly @endif></textarea>
                                @error('order.description') <span class="error text-danger">{{ $message }}</span>
                                @enderror
                            </div>
                        </div>
                    </div>
                </div>
                <div class="tab-pane fade @if(isset($order->id)) show active @endif" id="order-lines" role="tabpanel"
                    aria-labelledby="order-lines-tab" wire:ignore.self>
                    @if($order->id != null)
                    @if(in_array($order->status, [App\Library\Constants::ORDER_STATUS_DRAFT , App\Library\Constants::ORDER_STATUS_MODIFICATION_PENDING]))
                    <div class="row">
                        <div class="col-12">
                            <h5>Nueva línea</h5>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-12 col-md-3">
                            <div class="form-group">
                                <label for="cost_center_id" class="form-control-label">Centro de coste *:</label>
                                <select wire:model.defer="orderLine.cost_center_id" class="form-control">
                                    <option value="">-- Seleccionar centro de coste --</option>
                                    @foreach ($costCenters as $costCenterItem)
                                    <option value="{{ $costCenterItem->id }}" @if($costCenterItem->id ==
                                        $costCenter->id) selected="selected" @endif>{{ $costCenterItem->name }}
                                        ({{ (int)$costCenterItem->external_id }})</option>
                                    @endforeach
                                </select>
                                @error('orderLine.cost_center_id') <span class="error text-danger">{{ $message }}</span>
                                @enderror
                            </div>
                        </div>
                        <div class="col-12 col-md-3">
                            <div class="form-group">
                                <label for="fecha" class="form-control-label">Partida presupuestaria *:</label>
                                <select wire:model.defer="orderLine.budget_id" class="form-control">
                                    <option value="">-- Seleccionar partida presupuestaria --</option>
                                    @foreach ($pepElements as $pepElement)
                                    <option value="{{ $pepElement->id }}">{{ $pepElement->name }}</option>
                                    @endforeach
                                </select>
                                @error('orderLine.budget_id') <span class="error text-danger">{{ $message }}</span>
                                @enderror
                            </div>
                        </div>
                        <div class="col-12 col-md-2">
                            <div class="form-group">
                                <label for="document_type" class="form-control-label">Cantidad *:</label>
                                <input type="text" wire:model.defer="orderLine.qty" class="form-control" step="0.01">
                                @error('orderLine.qty') <span class="error text-danger">{{ $message }}</span> @enderror
                            </div>
                        </div>
                        <div class="col-12 col-md-2">
                            <div class="form-group" wire:ignore>
                                <label for="unit" class="form-control-label">Unidad *:</label>
{{--                                <select class="form-control form-select" wire.model.defer="orderLine.mesure_unit_id" id="mesure_unit_select">--}}
                                <select class="form-control form-select" wireModel="orderLine.mesure_unit_id" id="orderLine.mesure_unit_id">
                                    @foreach ($mesureUnits as $mesureUnit)
                                    @if($mesureUnit->name!='')
                                    <option value="{{ $mesureUnit->id }}">
                                        {{ $mesureUnit->name }}
                                    </option>
                                    @endif
                                    @endforeach
                                </select>
                                @error('orderLine.mesure_unit_id') <span class="error text-danger">{{ $message }}</span>
                                @enderror
                            </div>
                        </div>
                        <div class="col-12 col-md-2">
                            <div class="form-group">
                                <label for="orderLine.net_amount" class="form-control-label">Precio unitario *:</label>
                                <input type="text" wire:model.defer="orderLine.net_amount" class="form-control"
                                    step="0.0001">
                                @error('orderLine.net_amount') <span class="error text-danger">{{ $message }}</span>
                                @enderror
                            </div>
                        </div>
                        <div class="col-12 col-md-2">
                            <div class="form-group">
                                <label for="fecha" class="form-control-label">Moneda:</label>
                                <select wire:model.defer="orderLine.currency" class="form-control">
                                    @foreach ($currencies as $currency)
                                    <option value="{{ $currency }}" >{{ Str::upper($currency) }}
                                    </option>
                                    @endforeach
                                </select>
                                @error('orderLine.currency') <span class="error text-danger">{{ $message }}</span>@enderror
                            </div>
                        </div>
                        <div class="col-12 col-md-2">
                            <div class="form-group">
                                <label for="orderLine.delivery_date" class="form-control-label">Fecha de entrega
                                    *:</label>
                                <input type="date" wire:model.defer="orderLine.delivery_date" id="fecha" class="form-control"
                                    min="{{ today()->format('Y-m-d') }}">
                                @error('orderLine.delivery_date') <span class="error text-danger">{{ $message }}</span>
                                @enderror
                            </div>
                        </div>
                        <div class="col-12 col-md-8">
                            <div class="form-group">
                                <label for="description" class="form-control-label">Descripción material *:</label>
                                <input type="text" wire:model.defer="orderLine.description" id="description"
                                    class="form-control">
                                @error('orderLine.description') <span class="error text-danger">{{ $message }}</span>
                                @enderror
                            </div>
                        </div>

                        <!--
                        <div class="col-12 col-md-3">
                            <div class="form-group">
                                <label for="fecha" class="form-control-label">Iva:</label>
                                <select wire:model="orderLine.tax_type_id" class="form-control">
                                    <option value="">-- Seleccionar Tipo Iva --</option>
                                    @foreach ($taxTypes as $taxType)
                                    <option value="{{ $taxType->id }}">{{ $taxType->type }} - {{ $taxType->description }}
                                    </option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        -->
                        <div class="col-12">
                            <div class="text-right">
                                <button type="button" wire:click="addLine" class="btn btn-success">
                                    @if(null == $orderLine->id)
                                    Añadir linea
                                    @else
                                    Guardar linea
                                    @endif
                                </button>
                            </div>
                        </div>
                    </div>
                    @endif

                    <div class="row mt-2">
                        <div class="col-12">
                            <table class="table table-striped table-bordered table-hover table-checkable">
                                <thead>
                                    <tr>
                                        <th>Posición</th>
                                        <th>Descripción Material</th>
                                        <th>Centro de coste</th>
                                        <th>Partida presupuestaria</th>
                                        <!--<th>Imputación</th>
                                        <th>Localización</th>-->
                                        <th>Cantidad</th>
                                        <th>Unidad de medida</th>
                                        <th>Precio Unitario</th>
                                        <th>Total</th>
                                        <th>Moneda</th>
                                        <th>Fecha de entrega</th>
                                        @if(in_array($order->status, [App\Library\Constants::ORDER_STATUS_DRAFT , App\Library\Constants::ORDER_STATUS_MODIFICATION_PENDING]))
                                        <th>Action</th>
                                        @endif
                                    </tr>
                                </thead>
                                <tbody>
                                    @forelse ($orderLines as $line)
                                    <tr>
                                        <td>{{ $line->position }}</td>
                                        <td>{{ $line->description ?? '-' }}</td>
                                        <td>
                                            {{ $line->costCenter->name }} ({{ (int)$line->costCenter->external_id }})
                                        </td>
                                        <td>{{ $line->budget->name ?? '-' }}</td>

                                        <td>{{ number_format($line->qty,2,',','.') ?? '-' }}</td>
                                        <td>{{ $line->mesureUnit->unit ?? '-' }}</td>
                                        <td>{{ number_format($line->net_amount,4,',','.') ?? '-' }}</td>
                                        <td>{{ number_format($line->qty * $line->net_amount,4,',','.') ?? '-' }}</td>
                                        <td>{{ Str::upper($line->currency) }}</td>
                                        <td>{{ $line->delivery_date ?? '-' }}</td>
                                        @if(in_array($order->status, [App\Library\Constants::ORDER_STATUS_DRAFT , App\Library\Constants::ORDER_STATUS_MODIFICATION_PENDING]))
                                        <td>

                                            <div class="btn-group my-1" role="group" aria-label="">
                                                @if(in_array($order->status, [App\Library\Constants::ORDER_STATUS_DRAFT , App\Library\Constants::ORDER_STATUS_MODIFICATION_PENDING]))
                                                <button wire:click="editLine('{{ $line->id }}')"
                                                    class="btn btn-sm btn-warning" title="Editar línea"><i
                                                        class="fa fa-edit"></i></button>
                                                <button wire:click="deleteLine('{{ $line->id }}')"
                                                    class="btn btn-sm btn-danger"
                                                    onclick="return confirm({{ __('backend.messages.delete-confirmation') }});"
                                                    title="Eliminar línea"><i class="fa fa-trash"></i></button>
                                                @endif
                                            </div>

                                        </td>
                                        @endif
                                    </tr>
                                    @empty
                                    <tr>
                                        <td colspan="11">
                                            Aún no se han añadido líneas
                                        </td>
                                    </tr>
                                    @endforelse
                                </tbody>
                            </table>
                            {{ $orderLines->links() }}
                        </div>
                    </div>
                    @endif
                </div>
                <div class="tab-pane fade" id="internal-messages" role="tabpanel"
                    aria-labelledby="internal-messages-tab" wire:ignore.self>
                    @livewire('common.comments',[
                    'entityId' => $order->id,
                    'entityType' => 'App\Order',
                    'textTitle' => 'Chat interno',
                    'editable' => true
                    ])
                </div>
                <div class="tab-pane fade" id="files" role="tabpanel" aria-labelledby="files-tab" wire:ignore.self>
                    @livewire('common.file-upload',[
                    's3' => true,
                    'path' => config('custom.iworking_public_bucket_folder_orders') . '/' . now()->format('Y/m/d') . '/'
                    . (string)$order->id,
                    'modelId' => (string)$order->id,
                    'model' => 'App\Order',
                    'type' => 'order-attachment',
                    'enableDelete' => false
                    ])
                </div>
                <div class="tab-pane fade" id="audit-lines" role="tabpanel" aria-labelledby="audit-lines-tab">
                    <div class="row">
                        <div class="col-12">
                            No existe auditoria de pedido
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <hr>
    <div class="row my-3">
        <div class="col-12">
            <div class="text-right">
                <h4>Total: {{ number_format($order->totalLines(),2,',','.') }} €</h4>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-6">
            <div class="btn-group my-1" role="group" aria-label="">
                @if ((!$task)||(in_array($order->status, [App\Library\Constants::ORDER_STATUS_MODIFICATION_PENDING])))
                    @if($lines>0)
                        <button wire:click="approve()" class="btn btn-sm btn-success d-flex p-4 py-lg-2" data-toggle="tooltip"
                            data-placement="bottom" title="Emitir">
                            <i class="fas fa-check-circle"></i><span class="d-none d-lg-block">Emitir</span>
                        </button>
                        <button wire:click="cancel()" class="btn btn-sm btn-danger d-flex p-4 py-lg-2" data-toggle="tooltip"
                            data-placement="bottom" title="Cancelar pedido">
                            <i class="fas fa-lock"></i><span class="d-none d-lg-block">Cancelar pedido</span>
                        </button>
                        <button wire:click="hold()" class="btn btn-sm btn-warning d-flex p-4 py-lg-2" data-toggle="tooltip"
                            data-placement="bottom" title="Bloquear pedido">
                            <i class="fas fa-lock"></i><span class="d-none d-lg-block">Bloquear pedido</span>
                        </button>
                    @endif
                @else
                @if($editable)
                @if(in_array($order->status, [
                        App\Library\Constants::ORDER_STATUS_PENDING_APPROVAL_BY_MANAGER ,
                        App\Library\Constants::ORDER_STATUS_APPROVED_BY_MANAGER,
                        App\Library\Constants::ORDER_STATUS_APPROVED_BY_MANAGEMENT
                    ]))
                    <button wire:click="approve()" class="btn btn-sm btn-success d-flex p-4 py-lg-2" data-toggle="tooltip"
                        data-placement="bottom" title="Aprobar pedido">
                        <i class="fas fa-check"></i><span class="d-none d-lg-block">Aprobar pedido</span>
                    </button>
                    <button wire:click="cancel()" class="btn btn-sm btn-danger d-flex p-4 py-lg-2" data-toggle="tooltip"
                        data-placement="bottom" title="Cancelar pedido">
                        <i class="fas fa-times"></i><span class="d-none d-lg-block">Eliminar</span>
                    </button>

                    <button wire:click="modificate()" class="btn btn-sm btn-primary d-flex p-4 py-lg-2"
                        data-toggle="tooltip" data-placement="bottom" title="Modificar pedido">
                        <i class="fas fa-undo-alt"></i><span class="d-none d-lg-block">Devolver pedido</span>
                    </button>
                    <button wire:click="hold()" class="btn btn-sm btn-warning d-flex p-4 py-lg-2" data-toggle="tooltip"
                        data-placement="bottom" title="Bloquear pedido">
                        <i class="fas fa-lock"></i><span class="d-none d-lg-block">Bloquear pedido</span>
                    </button>
                @endif
                @endif
                @if(in_array($order->status, [App\Library\Constants::ORDER_STATUS_BLOCKED]))
                    <button wire:click="hold()" class="btn btn-sm btn-success d-flex p-4 py-lg-2" data-toggle="tooltip"
                        data-placement="bottom" title="Desbloquear pedido">
                        <i class="fas fa-unlock"></i><span class="d-none d-lg-block">Desbloquear pedido</span>
                    </button>
                @endif
                @endif
            </div>
        </div>
        <div class="col-6">
            <div class="text-right">
                <div class="btn-group my-1" role="group" aria-label="">
                    @if($order->status == App\Library\Constants::ORDER_STATUS_DRAFT)
                    @if($lines==0)
                    <button type="button" wire:click="saveOrder" class="btn btn-sm btn-success d-flex p-4 py-lg-2"
                    @if(($order->orderLines->count()==0)&&($order->id != null)) disabled @endif
                    >
                        Guardar pedido
                    </button>
                    @endif
                    <button type="button" class="btn btn-sm btn-danger d-flex p-4 py-lg-2"
                            onclick="return confirm('{{ __("backend.messages.delete-confirmation") }}')
                                || event.stopImmediatePropagation()" wire:click="deleteOrder">
                        Borrar pedido
                    </button>

                    @endif
                </div>

            </div>
        </div>
        @if($showDenyReason)
        <div class="col-12 mt-4">
            <div class="form-group">
                <label for="deny_reason" class="form-control-label">Comentarios:</label>
                <textarea wire:model.lazy="order.deny_reason" id="deny_reason" class="form-control"
                    rows="4" @if($order->status == App\Library\Constants::ORDER_STATUS_MODIFICATION_PENDING) readonly @endif></textarea>
                @error('order.deny_reason') <span class="error text-danger">{{ $message }}</span>
                @enderror
            </div>
        </div>
        <div class="col-12">
            <button type="button" wire:click="confirmCancel" class="btn btn-sm btn-danger d-flex p-4 py-lg-2">
                Confirmar Denegar
            </button>
        </div>
        @endif
    </div>



</div>
@push('css')
<link href="{{ config('custom.iworking_public_bucket') }}/assets/vendors/general/select2/dist/css/select2.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/select2-bootstrap-5-theme@1.1.1/dist/select2-bootstrap-5-theme.min.css" />
@endpush
@push('js')
<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
    <script>
        $(document).ready(function() {
            $('.form-select').select2({
                theme: "bootstrap-5",
                containerCssClass: "select2--large", // For Select2 v4.0
                selectionCssClass: "select2--large", // For Select2 v4.1
                dropdownCssClass: "select2--large",
            });
            $('.form-select').on('change', function (e) {
                console.log(e.target.value);
                @this.set($(this).attr("wiremodel"),e.target.value);
                //livewire.emit('selectedCompanyItem', e.target.value)
            });
        });
        document.addEventListener("DOMContentLoaded", () => {

            Livewire.hook('message.processed', (message, component) => {

            });
        });

    </script>
@endpush
