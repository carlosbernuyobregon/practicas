<div>
    <div class="kt-portlet__body">
        <!--begin: Datatable -->
        <div id="users_list_table_wrapper" class="dataTables_wrapper dt-bootstrap4 no-footer">
            <div class="dt-buttons btn-group mb-3">
                <div class="btn-group">
                    <select wire:model="entries" class="custom-select" tabindex="0" aria-controls="users_list_table"
                        type="button" aria-haspopup="true" aria-expanded="false">
                        <option value="10">@lang('backend.forms.selects.10')</option>
                        <option value="25">@lang('backend.forms.selects.25')</option>
                        <option value="50">@lang('backend.forms.selects.50')</option>
                        <option value="100">@lang('backend.forms.selects.100')</option>
                    </select>
                </div>
            </div>
            <div class="table-responsive">
                <table class="table table-striped- table-bordered table-hover table-checkable" id="users_list_table">
                    <thead>
                        <tr class="text-uppercase">
                            <th>Número de pedido</th>
                            <th>Fecha de creación</th>
                            <th>Sociedad</th>
                            <th>Fecha de entrega</th>
                            <th>Autor</th>
                            <th>Nif</th>
                            <th>Proveedor</th>
                            <th>Importe total</th>
                            <th>@lang('backend.crud.order.list.status')</th>
                            <th>Aprobador</th>
                            <th>@lang('backend.crud.actions')</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($tasks as $order)
                            <tr>

                                <td>{{ $order['data']->order_number ?? '' }}</td>
                                <td>{{ $order['data']->created_at != null
                                ? auth()->user()->applyDateFormat($order['data']->created_at)
                                : '' }}
                                </td>
                                <td>{{ $order['data']->company == 1000 ? 'Laboratorios Rubió' : 'Products and Technology' }}</td>
                                <td>{{ auth()->user()->applyDateFormat($order['data']->date) ?? '' }}</td>
                                <td>{{ $order['data']->user->full_name ?? '' }}</td>
                                <td>{{ $order['data']->provider->vat_number ?? ''}}</td>
                                <td>{{ $order['data']->provider->name ?? '' }}</td>
                                <td>{{ number_format($order['data']->totalLines(),2,',','.') }} €</td>
                                <td>{{ trans('backend.orders.status.'.$order['data']->status) }}</td>
                                <td>{{ $order['data']->manager->full_name ?? '' }}</td>
                                <td nowrap>
                                    <a href="{{ $order['formUrl'] }}" type="button"
                                        class="btn btn-sm btn-clean btn-icon btn-icon-md" data-toggle="tooltip"
                                        data-placement="top" title="Edit">
                                        <i class="fas fa-edit"></i>
                                    </a>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>


</div>
