<div>
    <div>
        @if (session()->has('message') or !empty($message))
        <div class="alert alert-success">
            {{ session('message') ?? $message }}
        </div>
        @endif
        @if (session()->has('error')/* or $errors->any()*/ or !empty($error))
            <div class="alert alert-danger">
                {{ session('message') ?? $errors->first() ?? $error }}
            </div>
        @endif
    </div>
    <div id="invoices_lines_wrapper" class="dataTables_wrapper dt-bootstrap4 no-footer">
        <div class="row">
            <div class="col-12">
                <div class="btn-group">
                    <select wire:model="entries" class="custom-select" tabindex="0" aria-controls="users_list_table"
                        type="button" aria-haspopup="true" aria-expanded="false">
                        <option value="1000">@lang('backend.forms.selects.all')</option>
                        <option value="10">@lang('backend.forms.selects.10')</option>
                        <option value="25">@lang('backend.forms.selects.25')</option>
                        <option value="50">@lang('backend.forms.selects.50')</option>
                        <option value="100">@lang('backend.forms.selects.100')</option>
                    </select>
                </div>
                <div class="btn-group mx-1">
                    @if (!$filtersMode)
                        <button wire:click="modeFilters()" class="btn btn-primary d-flex p-4 py-lg-2" type="button" data-toggle="collapse" data-target="#collapseFilters" aria-expanded="false" aria-controls="collapseFilters">
                            Mostrar filtros
                        </button>
                    @else
                        <button wire:click="modeFilters()" class="btn btn-warning d-flex p-4 py-lg-2" type="button" data-toggle="collapse" data-target="#collapseFilters" aria-expanded="false" aria-controls="collapseFilters">
                            Ocultar filtros
                        </button>
                    @endif
                </div>
                @if(auth()->user()->hasRole('administration-user') && !$view)
                    @if($editMode)
                        <div class="btn-group mx-1">
                            <button wire:click="save()" class="btn btn-success d-flex p-4 py-lg-2"><i class="fas fa-check"></i> Guardar</button>
                        </div>
                        <div class="btn-group mx-1">
                            <button wire:click="mode()" class="btn btn-secondary d-flex p-4 py-lg-2"><i class="fas fa-undo"></i> Cancelar</button>
                        </div>
                    @else
                        @if($invoice->status != 9 && $invoice->status != 2 && $invoice->status != 6)
                            <div class="btn-group mx-1">
                                <button wire:click="mode()" class="btn btn-primary d-flex p-4 py-lg-2"><i class="fas fa-edit"></i>Editar</button>
                            </div>
                            <div class="btn-group mx-1">
                                <button wire:click="recalculateLineTotals()" class="btn btn-google d-flex p-4 py-lg-2" data-toggle="tooltip" data-placement="bottom" title="Recalcular totales">
                                    <i class="fas fa-edit"></i><span class="d-none d-lg-block">Recalcular totales</span>
                                </button>
                            </div>
                            <div class="btn-group mx-1">
                                <button wire:click="showModal()" class="btn btn-dark d-flex p-4 py-lg-2" data-toggle="tooltip" data-placement="bottom" {{ $bulkDisabled ? 'disabled' : null }} title="Acciones multilíneas">
                                    <i class="fas fa-hammer"></i><span class="d-none d-lg-block">Acciones masivas</span>
                                </button>
                            </div>
                        @endif
                    @endif
                @endif
{{--                </div>--}}
            </div>
        </div>
        <div class="collapse" id="collapseFilters" wire:ignore>
            <div class="row">
                <div class="col-10">
                    <h5>Filtros</h5>
                </div>
                <div class="col-2">

                </div>
            </div>

            <div class="row">
                <div class="col-6 col-lg-3 col-xl-2 mt-2">
                    <label for="" class="font-weight-bold">Pedido:</label>
                    <input wire:model.debounce.300ms="search.itemOrderNumBulk" type="search"
                    class="form-control form-control-sm" id="searchItemOrderNumBulk"
                    placeholder="Pedido">
                </div>
                <div class="col-6 col-lg-3 col-xl-2 mt-2">
                    <label for="" class="font-weight-bold">Pedido:</label>
                    <input wire:model.debounce.300ms="search.itemOrderNumBulk" type="search"
                    class="form-control form-control-sm" id="searchItemOrderNumBulk"
                    placeholder="Pedido">
                </div>
                <div class="col-6 col-lg-2 col-xl-2 mt-2">
                    <label for="" class="font-weight-bold">Posición</label>
                    <input wire:model.debounce.300ms="search.itemOrderLineBulk" type="search"
                    class="form-control form-control-sm" id="searchItemOrderLineBulk"
                    placeholder="Posición">
                </div>
                <div class="col-12 col-lg-3 col-xl-2 mt-2">
                    <label for="material" class="font-weight-bold">Material:</label>
                    <input wire:model.debounce.300ms="search.materialCode" type="search"
                        class="form-control form-control-sm" id="searchMaterialCode"
                        placeholder="Material">
                </div>
                <div class="col-12 col-lg-4 col-xl-4 mt-2">
                    <label for="description" class="font-weight-bold">Descripción:</label>
                    <input wire:model.debounce.300ms="search.materialDescription" type="search"
                        class="form-control form-control-sm" id="searchMaterialDescription"
                        placeholder="Descripción">
                </div>
                <div class="col-6 col-lg-2 col-xl-1 mt-2">
                    <label for="qty" class="font-weight-bold">Cant. mín.:</label>
                    <input wire:model.debounce.300ms="search.materialQtyMin" type="search"
                        class="form-control form-control-sm" id="searchMaterialQtyMin"
                        placeholder="Cant. mínima">
                </div>
                <div class="col-6 col-lg-2 col-xl-1 mt-2">
                    <label for="qty" class="font-weight-bold">Cant. Max:</label>
                    <input wire:model.debounce.300ms="search.materialQtyMax" type="search"
                        class="form-control form-control-sm" id="searchMaterialQtyMax"
                        placeholder="Cant. MAX">
                </div>
                <div class="col-6 col-lg-2 col-xl-1 mt-2">
                    <label for="" class="font-weight-bold">Precio U. mín.:</label>
                    <input wire:model.debounce.300ms="search.unitPriceMin" type="search"
                        class="form-control form-control-sm" id="searchUnitPriceMin"
                        placeholder="Precio U. mínimo">
                </div>
                <div class="col-6 col-lg-2 col-xl-1 mt-2">
                    <label for="" class="font-weight-bold">Precio U. máx:</label>
                    <input wire:model.debounce.300ms="search.unitPriceMax" type="search"
                        class="form-control form-control-sm" id="searchUnitPriceMax"
                        placeholder="Precio U. máximo">
                </div>
                <div class="col-6 col-lg-2 mt-2">
                    <label for="net" class="font-weight-bold">Total Neto mín.:</label>
                    <input wire:model.debounce.300ms="search.netAmountMin" type="search"
                        class="form-control form-control-sm" id="searchNetAmountMin"
                        placeholder="Neto mínimo">
                </div>
                <div class="col-6 col-lg-2 mt-2">
                    <label for="net" class="font-weight-bold">Total Neto máx.:</label>
                    <input wire:model.debounce.300ms="search.netAmountMax" type="search"
                        class="form-control form-control-sm" id="searchNetAmountMax"
                        placeholder="Neto máximo">
                </div>
                <div class="col-6 col-lg-2 mt-2">
                    <label for="total" class="font-weight-bold">Imp. IVA mín.:</label>
                    <input wire:model.debounce.300ms="search.totalAmountMin" type="search"
                        class="form-control form-control-sm" id="searchTotalAmountMin"
                        placeholder="Importe IVA mínimo">
                </div>
                <div class="col-6 col-lg-2 mt-2">
                    <label for="total" class="font-weight-bold">Imp. IVA máx:</label>
                    <input wire:model.debounce.300ms="search.totalAmountMax" type="search"
                        class="form-control form-control-sm" id="searchTotalAmountMax"
                        placeholder="Importe IVA máximo">
                </div>
            </div>
        </div>
        @if ($this->editMode)
            <div class="d-flex justify-content-end mt-3">
                <div class="col-6 col-xl-2 d-flex justify-content-end">
                    <button wire:click="addNewInvoiceLine()" class="btn btn-brand btn-elevate btn-icon-sm">
                        <i class="fas fa-plus"></i> Nueva línea
                    </button>
                </div>
            </div>
        @endif
        <div class="table-responsive">
            <table class="table table-striped table-bordered table-hover table-checkable mt-4" id="invoices_list">
                <thead>
                    <tr>
                        <th class="text-center align-bottom">
                            <input type="checkbox" wire:model="selectAll" id="selectAllCb" />
                        </th>
                        <th wire:click="sortBy('item_order_num_bulk')" style="cursor: pointer; width: 8%">
                            PEDIDO
                                @include('partials._sort-icon',['field'=>'item_order_num_bulk'])
                        </th>
                        <th wire:click="sortBy('item_order_line_bulk')" style="cursor: pointer; width: 5%">
                            POS
                            @include('partials._sort-icon',['field'=>'item_order_line_bulk'])
                        </th>
                        <th wire:click="sortBy('material_code')" style="cursor: pointer;width: 8%">
                            MATERIAL
                                @include('partials._sort-icon',['field'=>'material_code'])
                        </th>
                        <th wire:click="sortBy('material_description')" style="cursor: pointer;width: 12%">
                            DESCRIPCIÓN
                                @include('partials._sort-icon',['field'=>'material_description'])
                        </th>
                        <th wire:click="sortBy('material_qty')" style="cursor: pointer; width: 8%">
                            CANT.
                                @include('partials._sort-icon',['field'=>'material_qty'])
                        </th>
                        <th wire:click="sortBy('unit_price')" style="cursor: pointer; width: 8%">
                            PRECIO U.
                                @include('partials._sort-icon',['field'=>'unit_price'])
                        </th>
                        <th wire:click="sortBy('gross_amount')" style="cursor: pointer; width: 8%">
                            T. BRUTO
                            @include('partials._sort-icon',['field'=>'gross_amount'])
                        </th>
                        <th wire:click="sortBy('discount_amount')" style="cursor: pointer; width: 8%">
                            DESCUENTO
                            @include('partials._sort-icon',['field'=>'discount_amount'])
                        </th>
                        <th wire:click="sortBy('discount_rate')" style="cursor: pointer; width: 8%">
                            % DESC.
                            @include('partials._sort-icon',['field'=>'discount_rate'])
                        </th>
                        <th wire:click="sortBy('net_amount')" style="cursor: pointer; width: 8%">
                            BASE IMP
                                @include('partials._sort-icon',['field'=>'net_amount'])
                        </th>
                        <th wire:click="sortBy('tax_type_id')" style="cursor: pointer;width:8%">
                            TIPO DE IVA
                            @include('partials._sort-icon',['field'=>'tax_type_id'])
                        </th>
                        <th style="width: 7%">
                            IMP. IVA
                        </th>
                        {{--<th wire:click="sortBy('irpf')" style="cursor: pointer; width: 8%">
                            IRPF
                            @include('partials._sort-icon',['field'=>'irpf'])
                        </th>--}}
                        <th wire:click="sortBy('total_amount')" style="cursor: pointer; width: 8%">
                            TOTAL LINEA
                                @include('partials._sort-icon',['field'=>'total_amount'])
                        </th>
                        <th style="cursor: pointer; width: 4%">
                            @if ($editMode)
                                ACCIONES
                            @else
                                REVISIÓN
                            @endif
                        </th>
                    </tr>
                </thead>
                <tbody>
                    @php($sumGrossAmount  = 0)
                    @php($sumDiscountAmount  = 0)
                    @php($sumNetAmount  = 0)
                    @php($sumTaxAmount  = 0)
                    @php($sumIrpfAmount  = 0)
                    @php($sumTotalAmount  = 0)
                    @forelse($invoiceLines as $line)
                        @php($sumGrossAmount    += $line->gross_amount)
                        @php($sumDiscountAmount += $line->discount_amount)
                        @php($sumNetAmount      += $line->net_amount)
                        @php($sumTaxAmount      += $line->taxes_amount)
                        @php($sumIrpfAmount     += $line->irpf)
                        @php($sumTotalAmount    += $line->total_amount)
                        <span style="display: none">{{ $line->checkinvoiceline() }}</span>
                        <tr>
                            <td class="text-center align-middle">
                                <input type="checkbox" wire:model="selectedLines.{{ $line->id }}" class="kt-checkbox-list line-check"/>
                            </td>
                            <td>
                                @if ($editMode)
                                <div class="form-group">
                                    <input wire:model="invoiceLinesArray.{{$line->id}}.item_order_num_bulk"  type="text"
                                        class="form-control {{ $errors->has('invoiceLinesArray.'.$line->id.'.item_order_num_bulk') ? ' is-invalid' : '' }}"
                                        name="" id="item_order_num_bulk_{{ $line->id }}">
                                    @error('invoiceLinesArray.'.$line->id.'.item_order_num_bulk')
                                        <span class="text-danger">{{ $message }}</span>
                                    @enderror
                                </div>
                                @else
                                    {{ $line->item_order_num_bulk == null ? $invoice->intern_order : $line->item_order_num_bulk }}
                                @endif
                            </td>
                            <td>
                                @if ($editMode)
                                <div class="form-group">
                                    <input wire:model="invoiceLinesArray.{{$line->id}}.item_order_line_bulk"  type="text"
                                           class="form-control {{ $errors->has('invoiceLinesArray.'.$line->id.'.item_order_line_bulk') ? ' is-invalid' : '' }}"
                                           id="item_order_line_bulk_{{ $line->id }}" step="10" min="0" style="width: 100%">
                                    @error('invoiceLinesArray.'.$line->id.'.item_order_line_bulk')<span class="text-danger">{{ $message }}</span>
                                    @enderror
                                </div>
                                @else
                                    {{ $line->item_order_line_bulk }}
                                @endif
                            </td>
                            <td>
                                @if ($editMode)
                                    <input wire:model="invoiceLinesArray.{{$line->id}}.material_code" type="text"
                                        class="form-control" id="material_code_{{ $line->id }}">
                                @else
                                    {{ $line->material_code }}
                                @endif
                            </td>
                            <td>
                                @if ($editMode)
                                    <textarea wire:model="invoiceLinesArray.{{$line->id}}.material_description"
                                        class="form-control" id="material_description_{{ $line->id }}"></textarea>
                                @else
                                    {!! $line->material_description !!}
                                @endif
                            </td>
                            <td>
                                @if ($editMode)
                                    <input wire:model="invoiceLinesArray.{{$line->id}}.material_qty" type="number"
                                           class="form-control {{ $errors->has('invoiceLinesArray.'.$line->id.'.unit_price') ? ' is-invalid' : '' }}"
                                           step="0.5" id="material_qty_{{ $line->id }}">
                                @else
                                    <span @if($line->lineErrorQty)style="color: darkred; background: orange; padding: 2px"@endif()>
                                        {{ $line->material_qty ?? '-' }}
                                    </span>
                                @endif
                                @error('invoiceLinesArray.'.$line->id.'.material_qty') <span class="text-danger">{{ $message }}</span>@enderror
                            </td>
                            <td>
                                @if ($editMode)
                                    <input wire:model="invoiceLinesArray.{{$line->id}}.unit_price" type="text"
                                        class="form-control {{ $errors->has('invoiceLinesArray.'.$line->id.'.unit_price') ? ' is-invalid' : '' }}"
                                        id="unit_price_{{ $line->id }}">
                                    @error('invoiceLinesArray.'.$line->id.'.unit_price')<span class="text-danger">{{ $message }}</span>
                                    @enderror
                                @else
                                    {{ $line->unit_price }} {{ $defaultCurrency }}
                                @endif
                            </td>
                            <td>
                                @if ($editMode)
                                    <div class="form-group">
                                        <input wire:model="invoiceLinesArray.{{$line->id}}.gross_amount" type="text"
                                               class="form-control {{ $errors->has('invoiceLinesArray.'.$line->id.'.gross_amount') ? ' is-invalid' : '' }}"
                                               name="" id="gross_amount_{{ $line->id }}">
                                        @error('invoiceLinesArray.'.$line->id.'.gross_amount')
                                            <span class="text-danger">{{ $message }}</span>
                                        @enderror
                                    </div>
                                @else
                                    {{ number_format($line->gross_amount, 2, ',', '.') }} {{ $defaultCurrency }}
                                @endif
                            </td>
                            <td>
                                @if ($editMode)
                                    <div class="form-group">
                                        <input wire:model="invoiceLinesArray.{{$line->id}}.discount_amount"  type="text"
                                               class="form-control {{ $errors->has('invoiceLinesArray.'.$line->id.'.discount_amount') ? ' is-invalid' : '' }}"
                                               id="discount_amount_{{ $line->id }}">
                                        @error('invoiceLinesArray.'.$line->id.'.discount_amount') <span class="text-danger">{{ $message }}</span> @enderror
                                    </div>
                                @else
                                    {{ number_format($line->discount_amount, 2, ',', '.') }} {{ $defaultCurrency }}
                                @endif
                            </td>
                            <td>
                                @if ($editMode)
                                    <div class="form-group">
                                        <input wire:model="invoiceLinesArray.{{$line->id}}.discount_rate"  type="text"
                                               class="form-control {{ $errors->has('invoiceLinesArray.'.$line->id.'.discount_rate') ? ' is-invalid' : '' }}"
                                               id="discount_rate_{{ $line->id }}">
                                        @error('invoiceLinesArray.'.$line->id.'.discount_rate') <span class="text-danger">{{ $message }}</span> @enderror
                                    </div>
                                @else
                                    {{ $line->discount_rate }} %
                                @endif
                            </td>
                            <td>
                                @if ($editMode)
                                    <div class="form-group">
                                        <input wire:model="invoiceLinesArray.{{$line->id}}.net_amount" type="text"
                                            class="form-control {{ $errors->has('invoiceLinesArray.'.$line->id.'.net_amount') ? ' is-invalid' : '' }}"
                                            name="" id="net_amount_{{ $line->id }}">
                                        @error('invoiceLinesArray.'.$line->id.'.net_amount')
                                            <span class="text-danger">{{ $message }}</span>
                                        @enderror
                                    </div>
                                @else
                                    {{ number_format($line->net_amount,2,',','.') }} {{ $defaultCurrency }}
                                @endif
                            </td>
                            <td>
                                @if($editMode)
                                    <select wire:model="invoiceLinesArray.{{$line->id}}.tax_type_id" class="form-control">
                                        <option value=""> - </option>
                                        @foreach ($taxes as $tax)
                                        <option value="{{ $tax->id }}">{{ $tax->type }} - {{ $tax->description }}</option>
                                        @endforeach
                                    </select>
                                    @error('invoiceLinesArray.'.$line->id.'.tax_type_id') <span class="text-danger">{{ $message }}</span> @enderror
                                @else
                                    <span @if($line->lineErrorTaxType)style="color: darkred; background: orange; padding: 2px"@endif()>
                                        {{ $line->tax->type ?? '' }} - {{ $line->tax->description ?? '' }}
                                    </span>
                                @endif
                            </td>
                            <td>
                                <span @if($line->lineErrorTaxesTotalAmount)style="color: darkred; background: orange; padding: 2px"@endif()>
                                    {{ number_format($line->taxes_amount, 4,',','.') }} {{ $defaultCurrency }}
                                </span>
                            </td>
                            {{--<td>
                                @if ($editMode)
                                    <div class="form-group">
                                        <input wire:model="invoiceLinesArray.{{$line->id}}.irpf"  type="text"
                                            class="form-control {{ $errors->has('invoiceLinesArray.'.$line->id.'.irpf') ? ' is-invalid' : '' }}"
                                            id="irpf_{{ $line->id }}">
                                    </div>
                                @else
                                    {{ number_format($line->irpf,2,',','.') }} {{ $defaultCurrency }}
                                @endif
                                @error('invoiceLinesArray.'.$line->id.'.irpf') <span class="text-danger">{{ $message }}</span> @enderror
                            </td>--}}
                            <td>
                                <span @if($line->lineErrorTotalAmount)style="color: darkred; background: orange; padding: 2px"@endif()>
                                    {{--@if ($line->total_amount != 0)
                                        {{ number_format($line->total_amount,2,',','.') }} {{ $defaultCurrency }}
                                    @else
                                        {{ number_format($line->net_amount + $line->taxes_amount - $line->irpf,2,',','.') }} {{ $defaultCurrency }}
                                    @endif--}}
                                    {{ number_format($line->total_amount, 2, ',', '.') }} {{ $defaultCurrency }}
                                </span>

                            </td>
                            <td class="text-center">
                                @if ($editMode)
                                    <button wire:click="confirmDelete('{{$line->id }}')" type="button" class="btn btn-sm btn-clean btn-icon btn-icon-md"
                                        data-toggle="modal" data-target="#delete_modal" data-toggle="tooltip"
                                        data-placement="top" title="Eliminar">
                                        <i class="fas fa-trash"></i>
                                    </button>
                                @else
                                    @if ($line->checkinvoiceline())
                                        <i class="fas fa-check text-success"></i>
                                    @else
                                        <i class="fas fa-times text-danger"></i>
                                    @endif
                                @endif
                            </td>
                        </tr>
                    @empty
                        <tr>
                            <td colspan="12" class="text-center">
                                No hay líneas para mostrar
                            </td>
                        </tr>
                    @endforelse
                    @if ($invoiceLines->isNotEmpty())
                        <tr>
                            <td colspan="1" class="table-header font-weight-bold">Total:</td>
                            <td colspan="1" class="table-header">-</td>
                            <td colspan="1" class="table-header">-</td>
                            <td colspan="1" class="table-header">-</td>
                            <td colspan="1" class="table-header">-</td>
                            <td colspan="1" class="table-header">-</td>
                            <td colspan="1" class="table-header total-value">{{ number_format($sumGrossAmount, 2, ',', '.') }} {{ $defaultCurrency }}</td>
                            <td colspan="1" class="table-header total-value">{{ number_format($sumDiscountAmount, 2, ',', '.') }} {{ $defaultCurrency }}</td>
                            <td colspan="1" class="table-header">-</td>
                            <td colspan="1" class="table-header total-value">{{ number_format($sumNetAmount, 2, ',', '.') }} {{ $defaultCurrency }}</td>
                            <td colspan="1" class="table-header">-</td>
                            <td colspan="1" class="table-header total-value">{{ number_format($sumTaxAmount, 4, ',', '.') }} {{ $defaultCurrency }}</td>
{{--                            <td colspan="1" class="table-header total-value">{{ number_format($sumIrpfAmount, 2, ',', '.') }} {{ $defaultCurrency }}</td>--}}
                            <td colspan="1" class="table-header total-value">{{ number_format($sumTotalAmount, 2, ',', '.') }} {{ $defaultCurrency }}</td>
                            <td colspan="1" class="table-header total-value">-</td>
                        </tr>
                    @endif
                </tbody>
            </table>
        </div>
        <div class="dataTables_info">
            {{ $invoiceLines->links() }}
            Mostrando de {{$invoiceLines->firstItem() ?? '0'}} a {{$invoiceLines->lastItem() ?? '0'}} de {{$invoiceLines->total()}} lineas
        </div>
    </div>
    @include('partials.deleteModal')
    @include('partials.lines-bulk-actions-modal')
</div>
@push('js')
    <script type="text/javascript">
        window.addEventListener('deleteItem', () => {
            $('#delete_modal').modal('hide');
        });
        window.addEventListener('showModal', () => {
            $('#bulkModal').modal('show');
        });
        window.addEventListener('hideModal', () => {
            $('#bulkModal').modal('hide');
        });
        window.addEventListener('checkAll', () => {
            $('.line-check').prop('checked', true);
        });
        window.addEventListener('uncheckAll', () => {
            $('.line-check').prop('checked', false);
        });
        window.addEventListener('uncheckHeaderLineCheckbox', () => {
            $('#selectAllCb').prop('checked', false);
        });
    </script>
@endpush
