<div>
    <div id="invoices_lines_wrapper" class="dataTables_wrapper dt-bootstrap4 no-footer">
        <h5>Auditoría</h5>
        <div class="row">
            <div class="col-12">
                <div class="dt-buttons btn-group my-2">
                    <div class="btn-group">
                        <select wire:model="entries" class="custom-select" tabindex="0" aria-controls="users_list_table"
                            type="button" aria-haspopup="true" aria-expanded="false">
                            <option value="10">@lang('backend.forms.selects.10')</option>
                            <option value="25">@lang('backend.forms.selects.25')</option>
                            <option value="50">@lang('backend.forms.selects.50')</option>
                            <option value="100">@lang('backend.forms.selects.100')</option>
                        </select>
                    </div>
                </div>
            </div>
        </div>
        <table class="table table-striped table-bordered table-hover table-checkable mt-5" id="invoices_list">
            <thead>
                <tr>
                    <th style="width: 15%">Usuario</th>
                    <th style="">Estado</th>
                    <th style="width: 10%">Fecha cambio</th>
                    <th style="width: 40%">Motivo</th>
                </tr>
            </thead>
            <tbody>
                @forelse ($auditLines as $audit)
                    <tr>
                        <td>{{ $audit->user->name ?? 'Sistema'}}</td>
                        <td><strong> @lang('status.'.$audit->status) </strong></td>
                        <td>{{ $audit->created_at }}</td>
                        <td>{{ $audit->text }}</td>
                    </tr>
                @empty
                    <tr>
                        <td colspan="4" class="text-center">
                            No hay líneas para mostrar
                        </td>
                    </tr>
                @endforelse   
            </tbody>
        </table>
        <div class="dataTables_info">
            {{ $auditLines->links() }}
            Showing {{$auditLines->firstItem() ?? '0'}} to {{$auditLines->lastItem() ?? '0'}} out of {{$auditLines->total()}}
        </div>
    </div>
</div>
