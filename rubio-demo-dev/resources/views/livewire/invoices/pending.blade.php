<div>
    <div class="dt-buttons btn-group d-flex justify-content-start mb-2">
        <div class="btn-group">
            <select wire:model="entries" class="custom-select" tabindex="0" aria-controls="users_list_table"
                type="button" aria-haspopup="true" aria-expanded="false">
                <option value="10">@lang('backend.forms.selects.10')</option>
                <option value="25">@lang('backend.forms.selects.25')</option>
                <option value="50">@lang('backend.forms.selects.50')</option>
                <option value="100">@lang('backend.forms.selects.100')</option>
            </select>
        </div>
        <div class="btn-group">
            @if (!$filtersMode)
                <button wire:click="modeFilters()" class="btn btn-primary" type="button" data-toggle="collapse" data-target="#collapseFilters" aria-expanded="false" aria-controls="collapseFilters">
                    Mostrar filtros
                </button>
            @else
                <button wire:click="modeFilters()" class="btn btn-warning" type="button" data-toggle="collapse" data-target="#collapseFilters" aria-expanded="false" aria-controls="collapseFilters">
                    Ocultar filtros
                </button>
            @endif
        </div>
    </div>
    <div class="collapse" id="collapseFilters" wire:ignore.self>
        <div class="row">
            <div class="col-6 col-xl-10">
                <h5>Filtros:</h5>
            </div>
            <div class="col-6 col-xl-2">
                @if (session()->has('emailSearch'))
                    <button wire:click="clearFilters()" class="btn btn-sm btn-danger btn-inline float-right"><i class="fas fa-times"></i> Eliminar filtros</button>
                @endif
            </div>
        </div>
        <div class="row">
            <div class="col-6 col-lg-3 col-xl-2 mt-4">
                <label for="email" class="font-weight-bold">Email:</label>
                <input wire:model.debounce.300ms="emailSearch.email" type="search"
                    class="form-control form-control-sm" name="" id="" placeholder="Email">
            </div>
            <div class="col-6 col-lg-8 col-xl-2 mt-4">
                <label for="subject" class="font-weight-bold">Asunto:</label>
                <input wire:model.debounce.300ms="emailSearch.subject" type="search"
                    class="form-control form-control-sm" name="" id="" placeholder="Asunto">
            </div>
            
            <div class="col-6 col-lg-3 col-xl-2 mt-4">
                <label for="fromDate" class="font-weight-bold">Desde fecha descarga:</label>
                <input wire:model.debounce.2000ms="emailSearch.fromDownloadDate" type="date"
                        class="form-control form-control-sm" name="emailSearchFromDownloadDate" id="emailSearchFromDownloadDate">
            </div>
            <div class="col-6 col-lg-3 col-xl-2 mt-4">
                <label for="toDate" class="font-weight-bold">Hasta fecha descarga:</label>
                <input wire:model.debounce.2000ms="emailSearch.toDownloadDate" type="date"
                        class="form-control form-control-sm" name="emailSearchToDownloadDate" id="emailSearchToDownloadDate">
            </div>
            <div class="col-6 col-lg-3 col-xl-2 mt-4">
                <label for="fromDatePayment" class="font-weight-bold">Desde fecha procesado:</label>
                <input wire:model.debounce.2000ms="emailSearch.fromProcessedDate" type="date"
                        class="form-control form-control-sm" name="emailSearchFromProcessedDate" id="emailSearchFromProcessedDate">
            </div>
            <div class="col-6 col-lg-3 col-xl-2 mt-4">
                <label for="toDatePayment" class="font-weight-bold">Hasta fecha procesado:</label>
                <input wire:model.debounce.2000ms="emailSearch.toProcessedDate" type="date"
                        class="form-control form-control-sm" name="emailSearchToProcessedDate" id="emailSearchToProcessedDate">
            </div>
        </div>
    </div>
    <div class="table-responsive">
        <table class="table table-striped table-bordered table-hover table-checkable mt-4" id="invoices_list">
            <thead>
                <tr>
                    <th>
                        REMITENTE
                        @include('partials._sort-icon',['field'=>'sap_company'])
                    </th>
                    <th wire:click="sortBy('emails.from')">
                        EMAIL
                        @include('partials._sort-icon',['field'=>'emails.from'])
                    </th>
                    <th wire:click="sortBy('emails.subject')">
                        ASUNTO
                        @include('partials._sort-icon',['field'=>'emails.subject'])
                    </th>
                    <th wire:click="sortBy('email_attachments.filename')" style="width: 10%">
                        NOMBRE ARCHIVO
                        @include('partials._sort-icon',['field'=>'email_attachments.filename'])
                    </th>
                    <th wire:click="sortBy('email_attachments.created_at')" style="width: 10%">
                        FECHA DESCARGA
                        @include('partials._sort-icon',['field'=>'email_attachments.created_at'])
                    </th>
                    <th wire:click="sortBy('email_attachments.updated_at')" style="width: 11%">
                        FECHA PROCESADO
                        @include('partials._sort-icon',['field'=>'email_attachments.updated_at'])
                    </th>
                    <th style="width: 6%">
                        ESTADO
                    </th>
                    <th style="width: 6%">
                        ACCIONES
                    </th>
                </tr>
            </thead>
            <tbody>
                @forelse($invoices as $invoice)
                    <tr>
                        <td>{{ $invoice->provider_name }}</td>
                        <td>{{ $invoice->emailAttachment->email->from }}</td>
                        <td>{{ $invoice->emailAttachment->email->subject }}</td>
                        <td>{{ $invoice->emailAttachment->filename }}</td>
                        <td>{{ $invoice->emailAttachment->created_at }}</td>
                        <td>{{ $invoice->emailAttachment->updated_at }}</td>
                        <td>@lang('status.'.$invoice->status)</td>
                        <td>
                            <a target="_blank" href="{{ url('attachments') . '/' . $invoice->emailAttachment->email->id.'/'.$invoice->emailAttachment->filename }}">Descargar documento</a>
                        </td>
                    </tr>
                @empty
                    <tr>
                        <td colspan="8" class="text-center">
                            There are no invoices to show
                        </td>
                    </tr>
                @endforelse
            </tbody>
        </table>
    </div>
    <div class="dataTables_info">
        {{ $invoices->links() }}
        Mostrando {{$invoices->firstItem() ?? '0'}} a {{$invoices->lastItem() ?? '0'}} de {{$invoices->total()}} emails
    </div>
</div>