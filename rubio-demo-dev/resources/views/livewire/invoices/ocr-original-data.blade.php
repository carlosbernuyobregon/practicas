<div>
    <h5>Datos originales capturados por el OCR:</h5>
    <div>
        @if (session()->has('message') or !empty($message))
            <div class="alert alert-success">
                {{ session('message') ?? $message }}
            </div>
        @endif
        @if (session()->has('error') or !empty($error))
            <div class="alert alert-danger">
                {{ session('error') ?? $error }}
            </div>
        @endif
    </div>
    @if ($ocrData)
        <div class="table-responsive">
            <table class="table table-bordered table-hover w-100">
                <tr>
                    <td colspan="6">
                        Estado:
                        @if ($ocrData->was_edited)
                            <strong>Datos originales editados el día {{ Illuminate\Support\Carbon::parse($ocrData->edited_at)->format('d-m-Y H:i:s') }}</strong>
                        @else
                            <strong>Datos originales no han sido editados nunca</strong>
                        @endif
                    </td>
                    <td colspan="6"></td>
                <tr>
                    <td colspan="2" class="table-header font-weight-bold">Total IRPF:</td>
                    <td colspan="2" class="total-value">
                        @if ($editMode)
                            <input wire:model.lazy="ocrData.taxes_irpf_amount" class="form-control form-control-sm ml-1 {{ $errors->has('ocrData.taxes_irpf_amount') ? ' is-invalid' : '' }}"
                                   type="text" name="" id="taxes_irpf_amount">
                        @else
                            {{ number_format($ocrData->taxes_irpf_amount, 2, ',', '.') }} {{ $ocrData->currency }}
                        @endif
                        @error('ocrData.taxes_irpf_amount') <span class="text-danger">{{ $message }}</span> @enderror
                    </td>
                    <td colspan="2" class="table-header font-weight-bold">II:</td>
                    <td colspan="2" class="total-value">
                        @if ($editMode)
                            <input wire:model.lazy="ocrData.taxes_total_amount" class="form-control form-control-sm ml-1 {{ $errors->has('ocrData.taxes_total_amount') ? ' is-invalid' : '' }}"
                                   type="text" name="" id="taxes_total_amount">
                        @else
                            {{ number_format($ocrData->taxes_total_amount, 2, ',', '.') }} {{ $ocrData->currency }}
                        @endif
                        @error('ocrData.taxes_total_amount') <span class="text-danger">{{ $message }}</span> @enderror
                    </td>
                    <td colspan="2" class="table-header font-weight-bold">TOTAL FACTURA:</td>
                    <td colspan="2" class="total-value">
                        @if ($editMode)
                            <input wire:model.lazy="ocrData.total" class="form-control form-control-sm ml-1 {{ $errors->has('ocrData.total') ? ' is-invalid' : '' }}"
                                   type="text" name="" id="total">
                        @else
                            {{ number_format($ocrData->total, 2, ',', '.') }} {{ $ocrData->currency }}
                        @endif
                        @error('ocrData.total') <span class="text-danger">{{ $message }}</span> @enderror
                    </td>
                </tr>
            </table>
        </div>
        <div class="row">
        <div class="col-12">
            @if($editMode)
                <div class="btn-group my-1">
                    <button wire:click="save()" class="btn btn-sm btn-success" data-toggle="tooltip" data-placement="bottom" title="Guardar cambios">
                        <i class="fas fa-check"></i> Guardar cambios
                    </button>
                </div>
                <div class="btn-group my-1">
                    <button wire:click="cancel()" class="btn btn-sm btn-secondary" data-toggle="tooltip" data-placement="bottom" title="Cancelar">
                        <i class="fas fa-undo"></i> Cancelar
                    </button>
                </div>
            @else
                <div class="btn-group my-1" role="group" aria-label="Editar datos">
                    <button wire:click="mode()" class="btn btn-sm btn-primary d-flex p-4 py-lg-2" data-toggle="tooltip" data-placement="bottom" title="Editar datos">
                        <i class="fas fa-edit"></i><span class="d-none d-lg-block">Editar datos</span>
                    </button>
                </div>
            @endif
        </div>
    </div>
    @else
        <h5>No hemos podido recuperar los Datos originales capturados por el OCR</h5>
    @endif
</div>
