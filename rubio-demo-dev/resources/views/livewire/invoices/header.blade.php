<div>
    <h5>Datos de factura:</h5>
    <div>
        @if (session()->has('message') or !empty($message))
            <div class="alert alert-success">
                {{ session('message') ?? $message }}
            </div>
        @endif
        @if (session()->has('error') or !empty($error))
            <div class="alert alert-danger">
                {{ session('error') ?? $error }}
            </div>
        @endif
    </div>
    <div class="table-responsive">
        <table class="table table-bordered table-hover w-100">
            <tr>
                <td colspan="2">
                    Estado: <strong>@lang('status.'.$invoice->status)</strong>
                </td>
                <td colspan="10">
                    <div class="row">
                        <div class="col-4 col-lg-4 text-center">
                            @if ($this->invoice->filename != null)
                                <span class="text-success text-uppercase font-weight-bold">
                                    Factura descargada <i class="fas fa-file-pdf text-success"></i>
                                </span>
                            @else
                                <span class="text-danger text-uppercase font-weight-bold" data-toggle="tooltip"
                                    data-placement="top" title="El documento de la factura no se ha podido descargar">
                                    Factura no descargada <i class="fas fa-file-pdf text-danger"></i>
                                </span>
                            @endif
                        </div>
                        <div class="col-4 col-lg-4 text-center">
                            @if ($isInvoiceCorrect)
                                <span class="text-success text-uppercase font-weight-bold" data-toggle="tooltip"
                                    data-placement="top" title="Los totales de la factura no cuadran con las líneas">
                                    Total Factura correcto <i class="fas fa-check text-success"></i>
                                </span>
                            @else
                                <span class="text-danger text-uppercase font-weight-bold">
                                    Total Factura incorrecto o dato faltante <i class="fas fa-times text-danger"></i>
                                </span>
                            @endif
                        </div>
                        <div class="col-4 col-lg-4 text-center">
                            @if ($areInvoiceLinesCorrect)
                                <span class="text-success text-uppercase font-weight-bold" data-toggle="tooltip"
                                    data-placement="top" title="Las líneas presentan datos erroneos">
                                    Líneas de Factura correctas <i class="fas fa-check text-success"></i>
                                </span>
                            @else
                                <span class="text-danger text-uppercase font-weight-bold">
                                    Error en líneas de Factura <i class="fas fa-times text-danger"></i>
                                </span>
                            @endif
                        </div>
                    </div>
                </td>
            </tr>
            <tr @if ($isInvoiceCorrect && empty($invoiceWarnings))style="display: none" @endif>
                <td colspan="12">
                    @if (!empty($invoiceErrors) && count(array_unique($invoiceErrors)) > 1)
                        <span class="text-danger text-uppercase font-weight-bold">Errores:</span>
                        @foreach ($invoiceErrors as $key => $error)
                            @if ($error)
                                <p style="margin: 0.5rem 0 !important">- {!! $error !!}</p>
                            @endif
                        @endforeach
                    @endif
                    @if (!empty($invoiceWarnings))
                        <span class="text-danger text-uppercase font-weight-bold">Advertencia:</span>
                        @foreach ($invoiceWarnings as $key => $warning)
                            @if ($warning)
                                @if ($key == 'warningText')
                                    <p class="text-warning text-uppercase font-weight-bold"
                                        style="margin: 0.5rem 0 !important">- {!! $warning !!}</p>
                                @else
                                    <p style="margin: 0.5rem 0 !important">- {!! $warning !!}</p>
                                @endif
                            @endif
                        @endforeach
                    @endif
                    {{-- <p class="badge-header"><span>Referencia de errores en la tabla</span></p>
                    <p><span class="badge badge-danger">Error</span> El valor es distinto al capturado por el OCR. Constatar con valores del la factura pdf</p>
                    <p><span class="badge badge-warning">Advertencia</span> El valor no pudo ser capturado por el OCR y por lo tanto no podemos comparar. Constatar con valores del la factura pdf</p> --}}
                </td>
            </tr>
            <tr>
                <td colspan="2" class="font-weight-bold" style="font-size: 14px">Sociedad:</td>
                <td colspan="2" class="" style="">
                    @if ($editMode)
                        <select wire:model="invoice.sap_company" class="form-control form-control-sm">
                            <option value="1000">1000 - Rubió</option>
                            <option value="2000">2000 - P&T</option>
                        </select>
                    @else
                        {{ $invoice->sap_company }}
                    @endif
                </td>
                <td colspan="2" class="font-weight-bold" style="font-size: 14px">Tipo de documento:</td>
                <td colspan="2" class="">
                    @if ($editMode)
                        <select wire:model="invoice.document_type"
                            class="form-control form-control-sm {{ $errors->has('invoice.document_type') ? ' is-invalid' : '' }}">
                            <option value="1">Factura</option>
                            <option value="2">Abono</option>
                            <option value="3">Cargo posterior</option>
                            <option value="4">Descargo posterior</option>
                        </select>
                    @else
                        @switch($this->invoice->document_type)
                            @case(1)
                                Factura
                            @break
                            @case(2)
                                Abono
                            @break
                            @case(3)
                                Cargo posterior
                            @break
                            @case(4)
                                Descargo posterior
                            @break
                            @default
                        @endswitch
                        @if ($this->invoice->document_type_label) &nbsp({{ ucfirst(strtolower($this->invoice->document_type_label)) }}) @endif
                    @endif
                    @error('invoice.document_type') <span class="text-danger">{{ $message }}</span> @enderror
                </td>
                <td colspan="2" class="font-weight-bold" style="font-size: 14px">N. de pedido:</td>
                <td colspan="2" class="">
                    @if ($editMode)
                        <input type="text" wire:model="invoice.intern_order" value=""
                            class="form-control form-control-sm {{ $errors->has('invoice.intern_order') ? ' is-invalid' : '' }}">
                    @else
                        {{ $invoice->intern_order ?? '' }}
                    @endif
                    @error('invoice.intern_order') <span class="text-danger">{{ $message }}</span> @enderror
                </td>
            </tr>
            <tr>
                <td colspan="2" class="font-weight-bold" style="font-size: 14px">CIF/NIF/VAT:</td>
                <td colspan="2" class="">
                    @if ($editMode)
                        <input wire:model="invoice.vat_number" type="text" value=""
                            class="form-control form-control-sm {{ $errors->has('invoice.vat_number') or ($unknownProviderError ? ' is-invalid' : '') }}">
                    @else
                        {{ $invoice->vat_number }}
                    @endif
                    @if ($unknownProviderError)
                        <span class="text-danger">{{ $unknownProviderError }}</span>
                    @endif
                    @error('invoice.vat_number') <span class="text-danger">{{ $message }}</span> @enderror
                </td>
                <td colspan="2" class="font-weight-bold" style="font-size: 14px">Proveedor:</td>
                <td colspan="2" class="">
                    {!! $invoice->provider_name !!}
                    @if ($unknownProviderError)
                        <p><span class="text-danger">{{ $unknownProviderError }}</span></p>
                    @endif
                </td>
                <td colspan="2" class="font-weight-bold" style="font-size: 14px">N. de factura:</td>
                <td colspan="2" class="">
                    @if ($editMode)
                        <input wire:model="invoice.invoice_number" type="text"
                            class="form-control form-control-sm {{ $errors->has('invoice.invoice_number') ? ' is-invalid' : '' }}">
                    @else
                        {{ $invoice->invoice_number }}
                    @endif
                    @error('invoice.invoice_number') <span class="text-danger">{{ $message }}</span> @enderror
                </td>
            </tr>
            <tr>
                <td colspan="2" class="font-weight-bold" style="font-size: 14px">Fecha factura:</td>
                <td colspan="2" class="">
                    @if ($editMode)
                        <input wire:model="invoice.date" type="date"
                            class="form-control form-control-sm {{ $errors->has('invoice.date') ? ' is-invalid' : '' }}"
                            name="date" id="invoice-date">
                    @else
                        {{ auth()->user()->applyDateFormat($invoice->date, true) ?? '' }}
                    @endif
                    @error('invoice.date') <span class="text-danger">{{ $message }}</span> @enderror
                </td>
                <td colspan="2" class="font-weight-bold" style="font-size: 14px">Fecha de vencimiento:</td>
                <td colspan="2" class="">
                    @if ($editMode)
                        <input wire:model="invoice.payment_date" type="date"
                            class="form-control form-control-sm {{ $errors->has('invoice.payment_date') ? ' is-invalid' : '' }}"
                            name="date" id="payment-date">
                    @else
                        {{ auth()->user()->applyDateFormat($invoice->payment_date, true) ?? '' }}
                    @endif
                    @error('invoice.payment_date') <span class="text-danger">{{ $message }}</span> @enderror
                </td>
                <td colspan="2" class="font-weight-bold" style="font-size: 14px">Fecha operación:</td>
                <td colspan="2">
                    @if ($editMode)
                        <input wire:model="invoice.operation_date" type="date"
                            class="form-control form-control-sm {{ $errors->has('invoice.operation_date') ? ' is-invalid' : '' }}"
                            name="operation_date" id="invoice-operation-date">
                    @else
                        {{ auth()->user()->applyDateFormat($invoice->operation_date, true) ?? null }}
                    @endif
                    @error('invoice.operation_date') <span class="text-danger">{{ $message }}</span> @enderror
                </td>
            </tr>

            <tr>
                <td colspan="2" class="font-weight-bold" style="font-size: 14px">Moneda:</td>
                <td colspan="2" class="">
                    @if ($editMode)
                        <select wire:model="invoice.currency"
                            class="form-control {{ $errors->has('invoice.currency') ? ' is-invalid' : '' }}">
                            @foreach ($currencies as $currency)
                                <option value="{{ $currency->code }}">{{ $currency->code }} -
                                    {{ $currency->name }}</option>
                            @endforeach
                        </select>
                    @else
                        {{ $invoice->currency }}
                    @endif
                </td>
                <td colspan="2" class="font-weight-bold" style="font-size: 14px">Tipo de cambio:</td>
                <td colspan="2" class="">
                    @if ($editMode && $invoice->currency != 'EUR')
                        <input type="number" step="0.01" wire:model="invoice.exchange_type"
                            class="form-control form-control-sm {{ $errors->has('invoice.exchange_type') ? ' is-invalid' : '' }}"
                            lang="es_ES">
                    @else
                        {{ $invoice->exchange_type == 0 ? number_format(1, 4, ',', '.') : number_format($invoice->exchange_type, 4, ',', '.') }}
                    @endif
                    @error('invoice.exchange_type') <span class="text-danger">{{ $message }}</span> @enderror
                </td>
                <td colspan="2" class=""></td>
                <td colspan="2" class=""></td>
            </tr>
            {{-- invoice-partial-header-multiple-tax-type view works for both type of invoices since recalculation
            works the same way if the invoice have a single or multiple tax type --}}
            @include('livewire.common.invoice-partial-header-multiple-tax-type')
            <tr>
                <td colspan="2" class="font-weight-bold" style="font-size: 14px">Datos de venta / comercial:</td>
                <td colspan="10">
                    @if ($editMode)
                        <input type="text" wire:model.lazy="invoice.comercial_sales"
                            class="form-control {{ $errors->has('invoice.comercial_sales') ? ' is-invalid' : '' }}"
                            maxlength="250">
                    @else
                        {{ $invoice->comercial_sales }}
                    @endif
                    @error('invoice.comercial_sales') <span class="text-danger">{{ $message }}</span> @enderror
                </td>
            </tr>
        </table>
    </div>
    @if (!$view)
        <div class="row">
            <div class="col-12">
                Añadir comentarios internos:
            </div>
            <div class="col-12 mb-5">
                @if ($editMode || $editCommentsMode)
                    <textarea wire:model.lazy="invoice.comments"
                        class="form-control {{ $errors->has('invoice.comments') ? ' is-invalid' : '' }}" cols="10"
                        rows="3">
                </textarea>
                @else
                    {{ $invoice->comments }}
                @endif
                @error('invoice.comments') <span class="text-danger">{{ $message }}</span> @enderror
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                @if (auth()->user()->hasRole('administration-user'))
                    @if ($editMode || ($editDocSAPMode && $editCommentsMode))
                        <div class="btn-group my-1">
                            <button wire:click="save()" class="btn btn-sm btn-success" data-toggle="tooltip"
                                data-placement="bottom" title="Guardar cambios">
                                <i class="fas fa-check"></i> Guardar cambios
                            </button>
                        </div>
                        <div class="btn-group my-1">
                            <button wire:click="cancel()" class="btn btn-sm btn-secondary" data-toggle="tooltip"
                                data-placement="bottom" title="Cancelar">
                                <i class="fas fa-undo"></i> Cancelar
                            </button>
                        </div>
                    @else
                        @if (!in_array($invoice->status, [App\Library\Constants::INVOICE_STATUS_MANAGER_APPROVAL_PENDING, App\Library\Constants::INVOICE_STATUS_ACCOUNTING_PENDING, App\Library\Constants::INVOICE_STATUS_POSTED, App\Library\Constants::INVOICE_STATUS_INVALID]))
                            <div class="btn-group my-1" role="group" aria-label="Editar datos">
                                <button wire:click="mode()" class="btn btn-sm btn-primary d-flex p-4 py-lg-2"
                                    data-toggle="tooltip" data-placement="bottom" title="Editar datos">
                                    <i class="fas fa-edit"></i><span class="d-none d-lg-block">Editar datos</span>
                                </button>
                            </div>
                            <div class="btn-group my-1" role="group" aria-label="Recalcular totales">
                                <button wire:click="recalculateTotals()"
                                    class="btn btn-sm btn-google d-flex p-4 py-lg-2" data-toggle="tooltip"
                                    data-placement="bottom" title="Recalcular totales">
                                    <i class="fas fa-edit"></i><span class="d-none d-lg-block">Recalcular
                                        totales</span>
                                </button>
                            </div>
                            @if ($invoice->status == 13)
                                <div class="btn-group my-1" role="group" aria-label="Marcar como no duplicada">
                                    <button wire:click="markAsNotDuplicated()"
                                        class="btn btn-sm btn-instagram d-flex p-4 py-lg-2" data-toggle="tooltip"
                                        data-placement="bottom" title="Marcar como no duplicada">
                                        <i class="fas fa-edit"></i><span class="d-none d-lg-block">Marcar como no
                                            duplicada</span>
                                    </button>
                                </div>
                            @endif
                        @endif
                        @if ($invoice->status == 1 || $invoice->status == 12)
                            <div class="btn-group my-1" role="group" aria-label="">
                                <button wire:click="approve()" class="btn btn-sm btn-success d-flex p-4 py-lg-2"
                                    data-toggle="tooltip" data-placement="bottom" title="Aprobar factura">
                                    <i class="fas fa-check-circle"></i><span class="d-none d-lg-block">Aprobar
                                        factura</span>
                                </button>
                            </div>
                            <div class="btn-group my-1" role="group" aria-label="">
                                <button wire:click="block()" class="btn btn-sm btn-danger d-flex p-4 py-lg-2"
                                    data-toggle="tooltip" data-placement="bottom" title="Bloquear factura">
                                    <i class="fas fa-lock"></i><span class="d-none d-lg-block">Bloquear
                                        factura</span>
                                </button>
                            </div>
                            @if ($this->invoiceCanBeInvalidated())
                                <div class="btn-group my-1" role="group" aria-label="">
                                    <button wire:click="showInvalidationModal()"
                                        class="btn btn-sm btn-warning d-flex p-4 py-lg-2 float-right"
                                        data-toggle="tooltip" data-placement="bottom"
                                        title="Marcar como factura no válida">
                                        <i class="fas fa-ban"></i><span class="d-none d-lg-block">No válida</span>
                                    </button>
                                </div>
                            @endif
                            <div class="btn-group my-1" role="group" aria-label="">
                                <button wire:click="manualAccounting()"
                                    class="btn btn-sm btn-outline-success d-flex p-4 py-lg-2" data-toggle="tooltip"
                                    data-placement="bottom" title="Contabilizar manualamente">
                                    <i class="far fa-check-circle"></i><span class="d-none d-lg-block">Contabilizar
                                        manualmente</span>
                                </button>
                            </div>
                            <div class="btn-group my-1" role="group" aria-label="">
                                <button wire:click="authorize()" class="btn btn-sm btn-primary d-flex p-4 py-lg-2"
                                    data-toggle="tooltip" data-placement="bottom" title="Solicitar autorización">
                                    <i class="fas fa-user-check"></i><span class="d-none d-lg-block">Solicitar
                                        autorización</span>
                                </button>
                            </div>
                            @if (!$askForInfoMode && $invoice->department == null)
                                <div class="btn-group my-1" role="group" aria-label="">
                                    <button wire:click="askForInfo()" class="btn btn-sm btn-warning d-flex p-4 py-lg-2"
                                        data-toggle="tooltip" data-placement="bottom" title="Solicitar información">
                                        <i class="fas fa-question-circle"></i><span
                                            class="d-none d-lg-block">Solicitar información</span>
                                    </button>
                                </div>
                            @endif
                        @else
                            @if ($invoice->status == 2)
                                <div class="btn-group my-1" role="group" aria-label="">
                                    <button wire:click="unapprove()" class="btn btn-sm btn-danger d-flex p-4 py-lg-2"
                                        data-toggle="tooltip" data-placement="bottom" title="Desaprobar">
                                        <i class="fas fa-times-circle"></i><span
                                            class="d-none d-lg-block">Desaprobar</span>
                                    </button>
                                </div>
                            @endif
                            @if ($invoice->status == 4)
                                <div class="btn-group my-1" role="group" aria-label="">
                                    <button wire:click="unblock()" class="btn btn-sm btn-danger d-flex p-4 py-lg-2"
                                        data-toggle="tooltip" data-placement="bottom" title="Desbloquear">
                                        <i class="fas fa-unlock"></i><span
                                            class="d-none d-lg-block">Desbloquear</span>
                                    </button>
                                </div>
                            @endif
                            @if ($this->invoiceCanBeInvalidated())
                                <div class="btn-group my-1" role="group" aria-label="">
                                    <button wire:click="showInvalidationModal()"
                                        class="btn btn-sm btn-warning d-flex p-4 py-lg-2 float-right"
                                        data-toggle="tooltip" data-placement="bottom"
                                        title="Marcar como factura no válida">
                                        <i class="fas fa-ban"></i><span class="d-none d-lg-block">No válida</span>
                                    </button>
                                </div>
                            @endif
                        @endif
                    @endif
                @else
                    @if ($editCommentsMode)
                        <div class="btn-group my-1">
                            <button wire:click="save()" class="btn btn-sm btn-success" data-toggle="tooltip"
                                data-placement="bottom" title="Guardar cambios">
                                <i class="fas fa-check"></i> Guardar cambios
                            </button>
                        </div>
                        <div class="btn-group my-1">
                            <button wire:click="cancel()" class="btn btn-sm btn-secondary" data-toggle="tooltip"
                                data-placement="bottom" title="Cancelar">
                                <i class="fas fa-undo"></i> Cancelar
                            </button>
                        </div>
                    @else
                        @if ($invoice->status == 9)
                            <div class="btn-group my-1" role="group" aria-label="Añadir comentarios">
                                <button wire:click="mode()" class="btn btn-sm btn-primary d-flex p-4 py-lg-2"
                                    data-toggle="tooltip" data-placement="bottom" title="Añadir comentarios">
                                    <i class="fas fa-edit"></i><span class="d-none d-lg-block">Añadir
                                        comentarios</span>
                                </button>
                            </div>
                            <div class="btn-group my-1" role="group" aria-label="">
                                <button wire:click="manualAccountingApproved()"
                                    class="btn btn-sm btn-success d-flex p-4 py-lg-2" data-toggle="tooltip"
                                    data-placement="bottom" title="Aprobar contabilización manual">
                                    <i class="fas fa-check-circle"></i><span class="d-none d-lg-block">Aprobar contab.
                                        manual</span>
                                </button>
                            </div>
                            <div class="btn-group my-1">
                                <button wire:click="unapprove()" class="btn btn-sm btn-danger d-flex p-4 py-lg-2"
                                    data-toggle="tooltip" data-placement="bottom" title="Rechazar factura">
                                    <i class="fas fa-times-circle"></i><span class="d-none d-lg-block">Rechazar
                                        factura</span>
                                </button>
                            </div>
                            <div class="btn-group my-1">
                                <button wire:click="block()" class="btn btn-sm btn-danger d-flex p-4 py-lg-2"
                                    data-toggle="tooltip" data-placement="bottom" title="Bloquear factura">
                                    <i class="fas fa-lock"></i><span class="d-none d-lg-block">Bloquear
                                        factura</span>
                                </button>
                            </div>
                        @endif
                    @endif
                    @if ($invoice->status == 2)
                        <button wire:click="unapprove()" class="btn btn-sm btn-danger d-flex p-4 py-lg-2"
                            data-toggle="tooltip" data-placement="bottom" title="Desaprobar">
                            <i class="fas fa-times-circle"></i><span class="d-none d-lg-block">Desaprobar</span>
                        </button>
                    @endif
                @endif
                <a href="{{ $invoice->file_url }}" target="_blank" class="btn btn-sm btn-primary my-2"
                    data-toggle="tooltip" data-placement="bottom" title="Descargar documento">
                    <i class="fas fa-arrow-circle-down"></i>Descargar documento
                </a>
                @php($allowedUsersSpecialButtons = explode(',', config('custom.allowed-users-special-header-buttons')))
                @if (in_array(auth()->user()->email, $allowedUsersSpecialButtons))
                    <div class="btn-group my-1">
                        <a href="{{ route('invoice.json', [$invoice->id]) }}" target="_blank"
                            class="btn btn-sm btn-dark my-2" data-toggle="tooltip" data-placement="bottom"
                            title="Ver Json">
                            <i class="fas fa-crutch"></i>Ver Json
                        </a>
                    </div>
                @endif
            </div>
        </div>
        @if (!$editMode &&
    $askForInfoMode &&
    auth()->user()->hasRole('administration-user'))
            <div class="row mt-3">
                @if (!$emailSent)
                    <div class="col-12">
                        @if ($askForInfoMode)
                            <div class="form-row">
                                <div class="form-group col-3">
                                    <label class="sr-only">Destinatario</label>
                                    <select wire:model="askForInfoUserId" class="form-control">
                                        <option value="">-- Seleccionar opción --</option>
                                        @foreach ($users as $user)
                                            <option value="{{ $user->id }}">{{ $user->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="form-group col-3">
                                    <label class="sr-only">Añadir email manualmente:</label>
                                    <div class="input-group mb-2">
                                        <div class="input-group-prepend">
                                            <div class="input-group-text">@</div>
                                        </div>
                                        <input wire:model="askForInfoEmail" type="email" class="form-control"
                                            placeholder="email@example.com">
                                    </div>
                                    @error('askForInfoEmail') <span class="text-danger">{{ $message }}</span>
                                    @enderror
                                </div>
                                <div class="form-check pt-2 pl-5 col-2">
                                    <input wire:model="askForInfoCopy" class="form-check-input" type="checkbox"
                                        id="inlineCheckbox1" value="1">
                                    <label class="form-check-label" for="inlineCheckbox1">Recibir copia</label>
                                </div>
                                @if ($askForInfoUserId != null || $askForInfoEmail != null)
                                    <div class="form-group col-2">
                                        <button wire:click="sendEmail()" class="btn btn-sm btn-danger"><i
                                                class="fas fa-envelope"></i>
                                            Enviar email</button>
                                    </div>
                                @endif
                            </div>
                        @endif
                    </div>
                @endif
                <div class="col-12 py-0 mb-5">
                    @error('email.sent') <span class="error">{{ $message }}</span> @enderror
                </div>
            </div>
        @endif
    @else
        <div class="row">
            <div class="col-12">
                Comentarios internos:
            </div>
            <div class="col-12 mb-5">
                @if ($editMode || $editCommentsMode)
                    <textarea wire:model.lazy="invoice.comments"
                        class="form-control {{ $errors->has('invoice.comments') ? ' is-invalid' : '' }}" cols="10"
                        rows="3"></textarea>
                @else
                    {{ $invoice->comments }}
                @endif
                @error('invoice.comments') <span class="text-danger">{{ $message }}</span> @enderror
            </div>
            <div class="col-12">
                @if ($editDocSAPMode || $editCommentsMode)
                    <div class="btn-group my-1">
                        <button wire:click="save()" class="btn btn-sm btn-success" data-toggle="tooltip"
                            data-placement="bottom" title="Guardar cambios">
                            <i class="fas fa-check"></i> Guardar cambios
                        </button>
                    </div>
                    <div class="btn-group my-1">
                        <button wire:click="cancel()" class="btn btn-sm btn-secondary" data-toggle="tooltip"
                            data-placement="bottom" title="Cancelar">
                            <i class="fas fa-undo"></i> Cancelar
                        </button>
                    </div>
                @else
                    @if ($invoice->status == 11 || $invoice->status == 5)
                        <div class="btn-group my-1" role="group" aria-label="Editar datos">
                            <button wire:click="mode()" class="btn btn-sm btn-primary d-flex p-4 py-lg-2"
                                data-toggle="tooltip" data-placement="bottom" title="Editar datos">
                                <i class="fas fa-edit"></i><span class="d-none d-lg-block">Editar datos</span>
                            </button>
                        </div>
                    @endif
                @endif
                @if (auth()->user()->hasRole('admin') && $invoice->status == 2)
                    <button wire:click="block()" class="btn btn-sm btn-danger"><i class="fas fa-lock"></i>
                        Bloquear factura</button>
                    <button wire:click="unapprove()" class="btn btn-sm btn-success"><i class="fas fa-check"></i>
                        Volver a validar</button>
                @endif
                <a href="{{ route('invoice.download', ['filename' => $invoice->filename]) }}" target="_blank"
                    class="btn btn-sm btn-primary">
                    <i class="fas fa-arrow-circle-down"></i> Descargar documento
                </a>
            </div>
        </div>
    @endif
    @include('partials.invoice-invalidation-modal')
</div>
@push('css')
    <style>
        .inside-table {
            position: relative;
            margin: 0px auto;
            padding: 0;
            width: 100%;
            height: auto;
            border-collapse: collapse;
            /*text-align: center;*/
        }

        .totals-table {
            /*background-color: #fffbdb;*/
            background-color: #b3e8ca;
        }

        .table-header {
            background-color: #b3d7f5;
        }

        .total-value {
            font-weight: 500;
            font-style: oblique;
        }

        .check-error {
            background-color: #fd397a !important;
        }

        .check-warning {
            background-color: #ffb822 !important;
        }

        .badge-header {
            margin-top: 15px;
        }

        .badge {
            font-weight: 500;
            color: black !important;
            width: 100px !important;
            margin-right: 10px;
        }

        .badge-pill {
            width: 30px !important;
            margin-right: 5px;
        }

        .info-tooltip {
            float: right;
            font-weight: 500;
        }

        .table-in-table-row td {
            vertical-align: middle !important;
        }

    </style>
@endpush

@push('js')
    <script type="text/javascript">
        window.addEventListener('showInvalidationModal', () => {
            $('#invalidationModal').modal('show');
        });
        window.addEventListener('hideInvalidationModal', () => {
            $('#invalidationModal').modal('hide');
        });
    </script>
@endpush
