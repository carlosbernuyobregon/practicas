<div class="kt-portlet kt-portlet--mobile">
    <div class="kt-portlet__head kt-portlet__head--lg">
        <div class="kt-portlet__head-label">
            <span class="kt-portlet__head-icon">
                <i class="kt-font-brand fas fa-file-invoice-dollar fa-lg"></i>
            </span>
            <h3 class="kt-portlet__head-title text-uppercase">
                {{ $status }}
            </h3>
        </div>
    </div>
    <div class="kt-portlet__body">
        <div id="users_list_table_wrapper" class="dataTables_wrapper dt-bootstrap4 no-footer">
            <div class="dt-buttons btn-group">
                <div class="btn-group">
                    <select wire:model="entries" class="custom-select" tabindex="0" aria-controls="users_list_table"
                        type="button" aria-haspopup="true" aria-expanded="false">
                        <option value="10">@lang('backend.forms.selects.10')</option>
                        <option value="25">@lang('backend.forms.selects.25')</option>
                        <option value="50">@lang('backend.forms.selects.50')</option>
                        <option value="100">@lang('backend.forms.selects.100')</option>
                    </select>
                </div>
            </div>
            <!--<div id="users_list_table_filter" class="dataTables_filter">
                <label>@lang('backend.forms.search')
                    <input wire:model="search" type="search" class="form-control form-control-sm" placeholder=""
                        aria-controls="users_list_table">
                </label>
            </div>-->
            <table class="table table-striped table-bordered table-hover table-checkable" id="invoices_list">
                <thead>
                    <tr>
                        <th wire:click="sortBy('sap_company')" style="cursor: pointer;">
                            SOCIEDAD
                                @include('partials._sort-icon',['field'=>'sap_company'])
                        </th>
                        <th wire:click="sortBy('provider_name')" style="cursor: pointer">
                            PROVEEDOR
                                @include('partials._sort-icon',['field'=>'provider_name'])
                        </th>
                        <th wire:click="sortBy('vat_number')" style="cursor: pointer">
                            CIF/NIF/VAT
                                @include('partials._sort-icon',['field'=>'vat_number'])
                        </th>
                        <th wire:click="sortBy('invoice_number')" style="cursor: pointer">
                            N. FRA
                                @include('partials._sort-icon',['field'=>'invoice_number'])
                        </th>
                        <th wire:click="sortBy('date')" style="cursor: pointer">
                            FECHA FRA
                                @include('partials._sort-icon',['field'=>'date'])
                        </th>
                        <th wire:click="sortBy('total_net')" style="cursor: pointer">
                            IMPORTE
                                @include('partials._sort-icon',['field'=>'total_net'])
                        </th>
                        <th wire:click="sortBy('taxes_total_amount')" style="cursor: pointer">
                            IVA
                                @include('partials._sort-icon',['field'=>'taxes_total_amount'])
                        </th>
                        <th wire:click="sortBy('total')" style="cursor: pointer">
                            TOTAL
                                @include('partials._sort-icon',['field'=>'total'])
                        </th>
                        <th wire:click="sortBy('payment_date')" style="cursor: pointer">
                            VTO.
                            @include('partials._sort-icon',['field'=>'payment_date'])
                        </th>
                        <th wire:click="sortBy('sap_document_number')" style="cursor: pointer">
                            CONTAB.
                            @include('partials._sort-icon',['field'=>'sap_document_number'])
                        </th>
                        <th wire:click="sortBy('intern_order')" style="cursor: pointer">
                            PEDIDO
                            @include('partials._sort-icon',['field'=>'intern_order'])
                        </th>
                        <th wire:click="sortBy('status')" style="cursor: pointer">
                            ESTADO
                                @include('partials._sort-icon',['field'=>'status'])
                        </th>
                        <th>
                            ACCIONES
                        </th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td><input wire:model.debounce.300ms="search.sapCompany" type="search" class="form-control form-control-sm" name="" id=""></td>
                        <td><input wire:model.debounce.300ms="search.providerName" type="search" class="form-control form-control-sm" name="" id=""></td>
                        <td><input wire:model.debounce.300ms="search.vatNumber" type="search" class="form-control form-control-sm" name="" id=""></td>
                        <td><input wire:model.debounce.300ms="search.invoiceNumber" type="search" class="form-control form-control-sm" name="" id=""></td>
                        <td><input wire:model.debounce.300ms="search.date" type="search" class="form-control form-control-sm" name="" id=""></td>
                        <td>
                            <input wire:model.debounce.300ms="search.totalNetMin" type="search" class="form-control form-control-sm" name="searchTotalNetMin" id="searchTotalNetMin" placeholder="MIN">
                            <input wire:model.debounce.300ms="search.totalNetMax" type="search" class="form-control form-control-sm" name="searchTotalNetMax" id="searchTotalNetMax" placeholder="MAX">
                        </td>
                        <td>
                            <input wire:model.debounce.300ms="search.taxesTotalAmountMin" type="search" class="form-control form-control-sm" name="searchTaxesTotalAmountMin" id="searchTaxesTotalAmountMin" placeholder="MIN">
                            <input wire:model.debounce.300ms="search.taxesTotalAmountMax" type="search" class="form-control form-control-sm" name="searchTaxesTotalAmountMax" id="searchTaxesTotalAmountMax" placeholder="MAX">
                        </td>
                        <td>
                            <input wire:model.debounce.300ms="search.totalMin" type="search" class="form-control form-control-sm" name="searchTotalMin" id="searchTotalMin" placeholder="MIN">
                            <input wire:model.debounce.300ms="search.totalMax" type="search" class="form-control form-control-sm" name="searchTotalMax" id="searchTotalMax" placeholder="MAX">
                        </td>
                        <td>
                            <input wire:model.debounce.300ms="search.paymentDate" type="search" class="form-control form-control-sm" name="searchPaymentDate" id="searchPaymentDate">
                        </td>
                        <td>
                            <input wire:model.debounce.300ms="search.sapDocumentNumber" type="search" class="form-control form-control-sm" name="searchSapDocumentNumber" id="searchSapDocumentNumber">
                        </td>
                        <td>
                            <input wire:model.debounce.300ms="search.internOrder" type="search" class="form-control form-control-sm" name="searchInternOrder" id="searchInternOrder">
                        </td>
                        <td></td>
                        <td></td>
                    </tr>
                    @forelse($invoices as $invoice)
                        <tr>
                            <td>{{ $invoice->sap_company }}</td>
                            <td>{{ $invoice->provider_name }}</td>
                            <td>{{ $invoice->vat_number }}</td>
                            <td>{{ $invoice->invoice_number }}</td>
                            <td>{{ $invoice->date }}</td>
                            <td>{{ number_format($invoice->total_net,2,',','.') }} {{ $invoice->currency }}</td>
                            <td>{{ number_format($invoice->taxes_total_amount,2,',','.') }} {{ $invoice->currency }}</td>
                            <td>{{ number_format($invoice->total,2,',','.') }} {{ $invoice->currency }}</td>
                            <td>{{ $invoice->payment_date ?? '' }}</td>
                            <td>{{ $invoice->sap_document_number }}</td>
                            <td>{{ $invoice->intern_order ?? '' }}</td>
                            <td>@lang('status.'.$invoice->status)</td>
                            <td>
                                @if($view)
                                    <a href="{{ route('invoice.show',['invoice' => $invoice->id]) }}" {{--target="_blank"--}}>Visualizar</a>
                                @else
                                    <a href="{{ route('invoice.review',['invoice' => $invoice->id]) }}" {{--target="_blank"--}}>Revisar</a>
                                @endif
                            </td>
                        </tr>
                    @empty
                        <tr>
                            <td colspan="9" class="text-center">
                                There are no invoices to show
                            </td>
                        </tr>
                    @endforelse
                </tbody>
            </table>
            <div class="dataTables_info">
                {{ $invoices->links() }}
                Showing {{$invoices->firstItem() ?? '0'}} to {{$invoices->lastItem() ?? '0'}} out of {{$invoices->total()}} invoices
            </div>
        </div>
    </div>
</div>
