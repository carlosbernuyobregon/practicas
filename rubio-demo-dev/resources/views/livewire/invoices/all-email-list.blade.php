<div>
    @if (session()->has('error'))
        <div class="alert alert-danger">
            {{ session('error') }}
        </div>
    @endif
    <div class="dt-buttons btn-group d-flex justify-content-start mb-2">
        <div class="btn-group">
            <select wire:model="entries" class="custom-select" tabindex="0" aria-controls="users_list_table"
                type="button" aria-haspopup="true" aria-expanded="false">
                <option value="10">@lang('backend.forms.selects.10')</option>
                <option value="25">@lang('backend.forms.selects.25')</option>
                <option value="50">@lang('backend.forms.selects.50')</option>
                <option value="100">@lang('backend.forms.selects.100')</option>
            </select>
        </div>
        <div class="btn-group">
            @if (!$filtersMode)
                <button wire:click="$toggle('filtersMode')" class="btn btn-primary" type="button" data-toggle="collapse" data-target="#collapseFilters" aria-expanded="false" aria-controls="collapseFilters">
                    Mostrar filtros
                </button>
            @else
                <button wire:click="$toggle('filtersMode')" class="btn btn-warning" type="button" data-toggle="collapse" data-target="#collapseFilters" aria-expanded="false" aria-controls="collapseFilters">
                    Ocultar filtros
                </button>
            @endif
        </div>
        <div class="btn-group ml-3">
            <button wire:click="downloadExcel" class="btn btn-success rounded-left pl-3 pr-2 " type="button" data-toggle="tooltip" data-placement="top" title="Exportar tabla a Excel">
                <i class="fas fa-file-excel m-0"></i>
            </button>
        </div>
        <div class="btn-group">
            <button wire:click="exportToPDF" class="btn btn-danger pl-3 pr-2" type="button" data-toggle="tooltip" data-placement="top" title="Exportar tabla a PDF">
                <i class="fas fa-file-pdf"></i>
            </button>
        </div>
        <div class="btn-group">
            <button wire:click="downloadZip" class="btn btn-warning pl-3 pr-2" type="button" data-toggle="tooltip" data-placement="top" title="Descargar facturas segun filtraje">
                <i class="fas fa-file-download"></i>
            </button>
            <span wire:loading.delay wire:target="exportToPDF, downloadExcel, downloadZip" class="align-middle ml-2 pt-2">
                Descargando...
            </span>
        </div>
    </div>
    <div class="collapse" id="collapseFilters" wire:ignore.self>
        <div class="row">
            <div class="col-6 col-xl-10">
                <h5>Filtros:</h5>
            </div>
            <div class="col-6 col-xl-2">
                @if (session()->has('allEmailSearch'))
                    <button wire:click="clearFilters()" class="btn btn-sm btn-danger btn-inline float-right"><i class="fas fa-times"></i> Eliminar filtros</button>
                @endif
            </div>
        </div>
        <div class="row">
            <div class="col-6 col-lg-3 col-xl-2">
                <label for="email" class="font-weight-bold">Remitente:</label>
                <input wire:model.debounce.300ms="allEmailSearch.providerName" type="search"
                    class="form-control form-control-sm" name="" id="" placeholder="Remitente">
            </div>
            <div class="col-6 col-lg-3 col-xl-2">
                <label for="email" class="font-weight-bold">Email:</label>
                <input wire:model.debounce.300ms="allEmailSearch.email" type="search"
                    class="form-control form-control-sm" name="" id="" placeholder="Email">
            </div>
            <div class="col-12 col-lg-6 col-xl-4">
                <label for="subject" class="font-weight-bold">Asunto:</label>
                <input wire:model.debounce.300ms="allEmailSearch.subject" type="search"
                    class="form-control form-control-sm" name="" id="" placeholder="Asunto">
            </div>
             <div class="col-6 col-lg-3 col-xl-2">
                <label for="fromDate" class="font-weight-bold">Desde fecha descarga:</label>
                <input wire:model.debounce.2000ms="allEmailSearch.fromDownloadDate" type="date"
                        class="form-control form-control-sm" name="allEmailSearchFromDownloadDate" id="allEmailSearchFromDownloadDate">
            </div>
            <div class="col-6 col-lg-3 col-xl-2">
                <label for="toDate" class="font-weight-bold">Hasta fecha descarga:</label>
                <input wire:model.debounce.2000ms="allEmailSearch.toDownloadDate" type="date"
                        class="form-control form-control-sm" name="allEmailSearchToDownloadDate" id="allEmailSearchToDownloadDate">
            </div>

            <div class="col-6 col-lg-3 col-xl-2">
                <label for="fromDatePayment" class="font-weight-bold">Desde fecha procesado:</label>
                <input wire:model.debounce.2000ms="allEmailSearch.fromProcessedDate" type="date"
                        class="form-control form-control-sm" name="allEmailSearchFromProcessedDate" id="allEmailSearchFromProcessedDate">
            </div>
            <div class="col-6 col-lg-3 col-xl-2">
                <label for="toDatePayment" class="font-weight-bold">Hasta fecha procesado:</label>
                <input wire:model.debounce.2000ms="allEmailSearch.toProcessedDate" type="date"
                        class="form-control form-control-sm" name="allEmailSearchToProcessedDate" id="allEmailSearchToProcessedDate">
            </div>
            <div class="col-6 col-lg-3 col-xl-2">
                <label for="status" class="font-weight-bold">Estado:</label>
                <select wire:model.debounce.300ms="allEmailSearch.status" class="form-control form-control-sm">
                    <option value="">Todos</option>
                        @foreach ($statusTypes as $key => $status)
                            @if (in_array($key, $statusToShow))
                                <option value="{{ $key }}">{{ $status }}</option>
                            @endif
                        @endforeach
                </select>
            </div>
        </div>
    </div>
    <div class="table-responsive">
        <table class="table table-striped table-bordered table-hover table-checkable mt-4" id="invoices_list">
            <thead>
                <tr>
                    <th wire:click="sortBy('provider_name')" style="width: 15%">
                        REMITENTE
                        @include('partials._sort-icon',['field'=>'provider_name'])
                    </th>
                    <th wire:click="sortBy('emails.from')">
                        EMAIL
                        @include('partials._sort-icon',['field'=>'emails.from'])
                    </th>
                    <th wire:click="sortBy('emails.subject')">
                        ASUNTO
                        @include('partials._sort-icon',['field'=>'emails.subject'])
                    </th>
                    <th wire:click="sortBy('email_attachments.filename')" style="width: 10%">
                        NOMBRE ARCHIVO
                        @include('partials._sort-icon',['field'=>'email_attachments.filename'])
                    </th>
                    <th wire:click="sortBy('email_attachments.created_at')" style="width: 10%">
                        FECHA DESCARGA
                        @include('partials._sort-icon',['field'=>'email_attachments.created_at'])
                    </th>
                    <th wire:click="sortBy('email_attachments.updated_at')" style="width: 11%">
                        FECHA PROCESADO
                        @include('partials._sort-icon',['field'=>'email_attachments.updated_at'])
                    </th>
                    <th wire:click="sortBy('status')">
                        ESTADO
                        @include('partials._sort-icon',['field'=>'status'])
                    </th>
                    <th style="width: 6%">
                        ACCIONES
                    </th>
                </tr>
            </thead>
            <tbody>
                @forelse($invoices as $invoice)
                    <tr>
                        <td>{{ $invoice->provider_name }}</td>
                        <td>{{ $invoice->emailAttachment->email->from }}</td>
                        <td>{{ $invoice->emailAttachment->email->subject }}</td>
                        <td>{{ $invoice->emailAttachment->filename }}</td>
                        <td>{{ $invoice->downloaded_at }}</td>
                        <td>{{ $invoice->processed_at }}</td>
                        <td>@lang('status.'.$invoice->status)</td>
                        <td>
                            <a href="{{ route('invoice.download',['filename' => $invoice->filename]) }}">Descargar documento</a>
                            <!--<a target="_blank" href="{{ url('attachments') . '/' . $invoice->emailAttachment->email->id.'/'.$invoice->emailAttachment->filename }}">Descargar documento</a>-->
                        </td>
                    </tr>
                @empty
                    <tr>
                        <td colspan="8" class="text-center">
                            There are no invoices to show
                        </td>
                    </tr>
                @endforelse
            </tbody>
        </table>
    </div>
    <div class="dataTables_info">
        {{ $invoices->links() }}
        Mostrando {{$invoices->firstItem() ?? '0'}} a {{$invoices->lastItem() ?? '0'}} de {{$invoices->total()}} emails
    </div>
</div>
