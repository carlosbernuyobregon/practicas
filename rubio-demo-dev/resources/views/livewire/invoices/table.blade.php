<div>
    @if (session()->has('error'))
        <div class="alert alert-danger">
            {{ session('error') }}
        </div>
    @endif
    <div class="dt-buttons btn-group mb-2">
        <div class="btn-group">
            <select wire:model="entries" class="custom-select" tabindex="0" aria-controls="users_list_table"
                type="button" aria-haspopup="true" aria-expanded="false">
                @if ($tableType == 'pending')
{{--                    <option value="1000">@lang('backend.forms.selects.all')</option>--}}
                @endif
                <option value="10">@lang('backend.forms.selects.10')</option>
                <option value="25">@lang('backend.forms.selects.25')</option>
                <option value="50">@lang('backend.forms.selects.50')</option>
                <option value="100">@lang('backend.forms.selects.100')</option>
            </select>
        </div>
        <div class="btn-group">
            @if (!$filtersMode)
                <button wire:click="$toggle('filtersMode')" class="btn btn-primary" type="button" data-toggle="collapse" data-target="#collapseFilters" aria-expanded="false" aria-controls="collapseFilters">
                    Mostrar filtros
                </button>
            @else
                <button wire:click="$toggle('filtersMode')" class="btn btn-warning" type="button" data-toggle="collapse" data-target="#collapseFilters" aria-expanded="false" aria-controls="collapseFilters">
                    Ocultar filtros
                </button>
            @endif
        </div>
        <div class="btn-group ml-3">
            <button wire:click="downloadExcel" class="btn btn-success rounded-left pl-3 pr-2 " type="button" data-toggle="tooltip" data-placement="top" title="Exportar tabla a Excel">
                <i class="fas fa-file-excel m-0"></i>
            </button>
        </div>
        <div class="btn-group">
            <button wire:click="exportToPDF" class="btn btn-danger pl-3 pr-2" type="button" data-toggle="tooltip" data-placement="top" title="Exportar tabla a PDF">
                <i class="fas fa-file-pdf"></i>
            </button>
        </div>
        <div class="btn-group">
            <button wire:click="downloadZip" class="btn btn-warning pl-3 pr-2" type="button" data-toggle="tooltip" data-placement="top" title="Descargar facturas segun filtraje">
                <i class="fas fa-file-download"></i>
            </button>
            <span wire:loading.delay wire:target="exportToPDF, downloadExcel, downloadZip" class="align-middle ml-2 pt-2">
                Descargando...
            </span>
        </div>
    </div>
    <div class="collapse" id="collapseFilters" wire:ignore.self>
        <div class="row">
            <div class="col-6 col-lg-10">
                <h5>Filtros:</h5>
            </div>
            <div class="col-6 col-lg-2">
                @if (session()->has('search'))
                    <button wire:click="clearFilters()" class="btn btn-sm btn-danger btn-inline float-right"><i class="fas fa-times"></i> Eliminar filtros</button>
                @endif
            </div>
        </div>
        <div class="row">
            <div class="col-6 col-lg-3 col-xl-2 mt-2">
                <label for="sapCompany" class="font-weight-bold">Sociedad:</label>
                <select wire:model.debounce.300ms="search.sapCompany" class="form-control form-control-sm">
                    <option value="1000">1000 - Laboratorios Rubió </option>
                    <option value="2000">2000 - Products & Technology</option>
                </select>
            </div>
            <div class="col-6 col-lg-3 col-xl-2 mt-2">
                <label for="providerName" class="font-weight-bold">Proveedor:</label>
                <input wire:model.debounce.300ms="search.providerName" type="search"
                    class="form-control form-control-sm" name="" id="" placeholder="Proveedor">
            </div>
            <div class="col-6 col-lg-3 col-xl-2 mt-2">
                <label for="vatNumber" class="font-weight-bold">NIF/VAT:</label>
                <input wire:model.debounce.300ms="search.vatNumber" type="search"
                    class="form-control form-control-sm" name="" id="" placeholder="CIF/NIF/VAT">
            </div>
            <div class="col-6 col-lg-3 col-xl-2 mt-2">
                <label for="invoiceNumber" class="font-weight-bold">Núm. Factura:</label>
                <input wire:model.debounce.300ms="search.invoiceNumber" type="search"
                    class="form-control form-control-sm" name="" id="" placeholder="Núm. Factura">
            </div>
            <div class="col-6 col-lg-3 col-xl-2 mt-2">
                <label for="fromDate" class="font-weight-bold">Desde fecha:</label>
                <input wire:model.debounce.2000ms="search.fromDate" type="date"
                            class="form-control form-control-sm" name="searchFromDate" id="searchFromDate"
                            placeholder="Desde fecha">
            </div>
            <div class="col-6 col-lg-3 col-xl-2 mt-2">
                <label for="toDate" class="font-weight-bold">Hasta fecha:</label>
                <input wire:model.debounce.2000ms="search.toDate" type="date"
                            class="form-control form-control-sm" name="searchToDate" id="searchToDate"
                            placeholder=" Hasta fecha">
            </div>
            <div class="col-6 col-lg-3 col-xl-2 mt-2">
                <label for="totalNetMin" class="font-weight-bold">Importe mín.:</label>
                <input wire:model.debounce.300ms="search.totalNetMin" type="search"
                            class="form-control form-control-sm" name="searchTotalNetMin" id="searchTotalNetMin"
                            placeholder="Importe mínimo">
            </div>
            <div class="col-6 col-lg-3 col-xl-2 mt-2">
                <label for="sapCompany" class="font-weight-bold">Importe max:</label>
                <input wire:model.debounce.300ms="search.totalNetMax" type="search"
                            class="form-control form-control-sm" name="searchTotalNetMax" id="searchTotalNetMax"
                            placeholder="Importe máximo">
            </div>
            <div class="col-6 col-lg-3 col-xl-2 mt-2">
                <label for="taxesTotalAmountMin" class="font-weight-bold">IVA mín.:</label>
                <input wire:model.debounce.300ms="search.taxesTotalAmountMin" type="search"
                            class="form-control form-control-sm" name="searchTaxesTotalAmountMin"
                            id="searchTaxesTotalAmountMin" placeholder=" IVA mínimo">
            </div>
            <div class="col-6 col-lg-3 col-xl-2 mt-2">
                <label for="taxesTotalAmountMax" class="font-weight-bold">IVA máx.:</label>
                <input wire:model.debounce.300ms="search.taxesTotalAmountMax" type="search"
                            class="form-control form-control-sm" name="searchTaxesTotalAmountMax"
                            id="searchTaxesTotalAmountMax" placeholder="IVA máximo">
            </div>
            <div class="col-6 col-lg-3 col-xl-4 mt-2">
                <label for="currency" class="font-weight-bold">Tipo de IVA:</label>
                <select wire:model="search.taxType" class="form-control form-control-sm">
                    <option value="">Todos</option>
                    @foreach ($taxes as $tax)
                    <option value="{{ $tax->id }}">{{ $tax->type }} - {{ $tax->description }}</option>
                    @endforeach
                </select>
            </div>
            <div class="col-6 col-lg-3 col-xl-2 mt-2">
                <label for="totalMin" class="font-weight-bold">Total mín.:</label>
                <input wire:model.debounce.300ms="search.totalMin" type="search"
                            class="form-control form-control-sm" name="searchTotalMin" id="searchTotalMin"
                            placeholder="Total mínimo">
            </div>
            <div class="col-6 col-lg-3 col-xl-2 mt-2">
                <label for="totalMax" class="font-weight-bold">Total máx.:</label>
                <input wire:model.debounce.300ms="search.totalMax" type="search"
                            class="form-control form-control-sm" name="searchTotalMax" id="searchTotalMax"
                            placeholder="Total máximo">
            </div>
            <div class="col-6 col-lg-3 col-xl-2 mt-2">
                <label for="currency" class="font-weight-bold">Divisa:</label>
                <input wire:model.debounce.300ms="search.currency" type="search"
                    class="form-control form-control-sm" name="" id="" placeholder="Divisa">
            </div>
            <div class="col-6 col-lg-3 col-xl-2 mt-2">
                <label for="fromDatePayment" class="font-weight-bold">Desde fecha vencimiento:</label>
                <input wire:model.debounce.2000ms="search.fromDatePayment" type="date"
                            class="form-control form-control-sm" name="searchFromDatePayment" id="searchFromDatePayment"
                            placeholder="Desde fecha">
            </div>
            <div class="col-6 col-lg-3 col-xl-2 mt-2">
                <label for="toDatePayment" class="font-weight-bold">Hasta fecha vencimiento:</label>
                <input wire:model.debounce.2000ms="search.toDatePayment" type="date"
                            class="form-control form-control-sm" name="searchToDatePayment" id="searchToDatePayment"
                            placeholder=" Hasta fecha">
            </div>
            <div class="col-6 col-lg-3 col-xl-2 mt-2">
                <label for="sapDocumentNumber" class="font-weight-bold">Doc. SAP:</label>
                <input wire:model.debounce.300ms="search.sapDocumentNumber" type="search"
                    class="form-control form-control-sm" name="searchSapDocumentNumber"
                    id="searchSapDocumentNumber" placeholder="Contab.">
            </div>
            <div class="col-6 col-lg-3 col-xl-2 mt-2">
                <label for="internOrder" class="font-weight-bold">Pedido:</label>
                <input wire:model.debounce.300ms="search.internOrder" type="search"
                    class="form-control form-control-sm" name="searchInternOrder" id="searchInternOrder"
                    placeholder="Pedido">
            </div>
            <div class="col-6 col-lg-2 mt-2">
                <label for="status" class="font-weight-bold">Estado:</label>
                <select wire:model.debounce.300ms="search.status" class="form-control form-control-sm">
                    <option value="">Todos</option>
                        @foreach ($statusTypes as $key => $status)
                            @if (in_array($key, $statusToShow))
                                <option value="{{ $key }}">{{ $status }}</option>
                            @endif
                        @endforeach
                </select>
            </div>
            <div class="col-6 col-lg-2 col-xl-1 mt-2">
                <label for="status" class="font-weight-bold">Revisión:</label>
                <select wire:model.debounce.300ms="search.isCorrect" class="form-control form-control-sm">
                    <option value="">Todas</option>
                    <option value="1">Correctas <i class="fas fa-check text-success"></i></option>
                    <option value="0">Incorrectas <i class="fas fa-times text-danger"></i></option>
                </select>
            </div>
            <div class="col-6 col-lg-2 mt-2">
                <label for="status" class="font-weight-bold">Tipo Documento:</label>
                <select wire:model.debounce.300ms="search.documentType" class="form-control form-control-sm">
                    <option value="">Todos</option>
                    <option value="1">Factura</option>
                    <option value="2">Abono</option>
                    <option value="3">Cargo posterior</option>
                    <option value="4">Descargo posterior</option>

                </select>
            </div>
            <div class="col-6 col-lg-3 col-xl-2 mt-2">
                <label for="fromContabDate" class="font-weight-bold">Desde fecha contabilización:</label>
                <input wire:model.debounce.2000ms="search.fromContabDate" type="date"
                       class="form-control form-control-sm" name="searchFromContabDate" id="searchFromContabDate"
                       placeholder="Desde fecha">
            </div>
            <div class="col-6 col-lg-3 col-xl-2 mt-2">
                <label for="toContabDate" class="font-weight-bold">Hasta fecha contabilización:</label>
                <input wire:model.debounce.2000ms="search.toContabDate" type="date"
                       class="form-control form-control-sm" name="searchToContabDate" id="searchToContabDate"
                       placeholder=" Hasta fecha">
            </div>
            <div class="col-6 col-lg-3 col-xl-2 mt-2">
                <label class="font-weight-bold">
                    <input wire:model.debounce.2000ms="search.unreadMessages" class="mr-1" type="checkbox" id="cbox1" value="first_checkbox"> Facturas con mensajes no leídos
                </label>
            </div>
        </div>
    </div>
    <div class="table-responsive">
        <table class="table table-striped table-bordered table-hover table-checkable mt-4" id="invoices_list">
            <thead>
                <tr>
                    <th></th>
                    <!--<th wire:click="sortBy('sap_company')" style="cursor: pointer;">
                        SDAD
                        @include('partials._sort-icon',['field'=>'sap_company'])
                    </th>-->
                    <th wire:click="sortBy('provider_name')" style="cursor: pointer">
                        PROVEEDOR
                        @include('partials._sort-icon',['field'=>'provider_name'])
                    </th>
                    <th wire:click="sortBy('vat_number')" style="cursor: pointer">
                        NIF/VAT
                        @include('partials._sort-icon',['field'=>'vat_number'])
                    </th>
                    <th wire:click="sortBy('invoice_number')" style="cursor: pointer">
                        NÚM. FRA
                        @include('partials._sort-icon',['field'=>'invoice_number'])
                    </th>
                    <th wire:click="sortBy('date')" style="cursor: pointer">
                        FECHA FRA
                        @include('partials._sort-icon',['field'=>'date'])
                    </th>
                    <th wire:click="sortBy('total_net')" style="cursor: pointer">
                        BASE IMP.
                        @include('partials._sort-icon',['field'=>'total_net'])
                    </th>
                    <th wire:click="sortBy('taxes_total_amount')" style="cursor: pointer">
                        IVA
                        @include('partials._sort-icon',['field'=>'taxes_total_amount'])
                    </th>
                    <th {{--wire:click="sortBy('concat_tax_types')"--}} style="cursor: pointer">
                        II
                        @include('partials._sort-icon',['field'=>'currency'])
                    </th>
                    <th wire:click="sortBy('total')" style="cursor: pointer">
                        TOTAL
                        @include('partials._sort-icon',['field'=>'total'])
                    </th>
                    <th wire:click="sortBy('sap_document_number')" style="cursor: pointer">
                        DOC.SAP
                        @include('partials._sort-icon',['field'=>'sap_document_number'])
                    </th>
                    <th wire:click="sortBy('contabilized_at')" style="cursor: pointer">
                        F. CONTAB
                        @include('partials._sort-icon',['field'=>'contabilized_at'])
                    </th>
                    <th wire:click="sortBy('intern_order')" style="cursor: pointer">
                        PEDIDO
                        @include('partials._sort-icon',['field'=>'intern_order'])
                    </th>
                    <th wire:click="sortBy('status')" style="cursor: pointer">
                        ESTADO
                        @include('partials._sort-icon',['field'=>'status'])
                    </th>
                    @if (auth()->user()->hasRole('admin'))
                        <th wire:click="sortBy('downloaded_at')" style="cursor: pointer">
                            DESCARGA
                            @include('partials._sort-icon',['field'=>'downloaded_at'])
                        </th>
                    @endif
                    <th>
                        ACCIONES
                    </th>
                </tr>
            </thead>
            <tbody class="datatable-body">
                @forelse($invoices as $invoice)
                <tr>
                    <td>
                        <a class="" href="#collapseRow-{{ $invoice->id }}" data-toggle="collapse" role="button" aria-expanded="false" aria-controls="collapseRow">
                            <i class="fa fa-caret-right"></i>
                        </a>
                    </td>
                    <!--<td>{{ $invoice->sap_company }}</td>-->
                    <td>
                        {{ $invoice->provider_name }}
                        @if ($invoice->has_unread_messages > 0)
                            <span class="badge badge-pill badge-danger float-right text-dark">{{ $invoice->has_unread_messages }} <i class="fas fa-envelope text-dark"></i></span>
                        @elseif ($invoice->has_unread_messages === 0)
                            <span class="badge badge-pill badge-success float-right"><i class="fas fa-envelope text-dark"></i></span>
                        @endif
                    </td>
                    <td>{{ $invoice->vat_number }}</td>
                    <td>{{ $invoice->invoice_number }}</td>
                    <td>{{ auth()->user()->applyDateFormat($invoice->date, true) ?? ''}}</td>
                    <td class="text-right">{{ number_format($invoice->total_net,2,',','.') }}</td>
                    <td class="text-right">{{ number_format($invoice->taxes_total_amount,2,',','.') }}</td>
                    <td>{{ $invoice->concat_tax_types }}</td>
                    <td class="text-right">{{ number_format($invoice->total,2,',','.') }}</td>
                    <td>{{ $invoice->sap_document_number }}</td>
                    <td>{{ auth()->user()->applyDateFormat($invoice->contabilized_at, true) ?? ''}}</td>
                    <td>{{ $invoice->intern_order ?? '' }}</td>
                    <td>@lang('status.'.$invoice->status)</td>
                    @if (auth()->user()->hasRole('admin'))
                        <td>{{ $invoice->downloaded_at ?? '' }}</td>
                    @endif
                    <td>
                        @if($view)
                        <a href="{{ route('invoice.show',['invoice' => $invoice->id]) }}" {{--target="_blank"--}}>Visualizar</a>
                        @else
                        <a href="{{ route('invoice.review',['invoice' => $invoice->id]) }}" {{--target="_blank"--}}>Revisar</a>
                        @endif
                        <span class="text-right ml-1">
                            @if ($invoice->checkInvoice())
                                <i class="fas fa-check text-success"></i>
                            @else
                                <i class="fas fa-times text-danger"></i>
                            @endif
                        </span>
                    </td>
                </tr>
                <tr class="collapse" id="collapseRow-{{ $invoice->id }}"  wire:ignore.self>
                    <td></td>
                    <td colspan="12">
                        <table>
                            <tr>
                                <th>SOCIEDAD</th>
                                <td>{{ $invoice->sap_company }}</td>
                            </tr>
                            <tr>
                                <th>DIVISA</th>
                                <td>{{ $invoice->currency ?? ''}}</td>
                            </tr>
                            <tr>
                                <th>VENCIMIENTO</th>
                                <td>{{ auth()->user()->applyDateFormat($invoice->payment_date, true) ?? ''}}</td>
                            </tr>
                        </table>
                    </td>
                </tr>
                @empty
                <tr>
                    <td colspan="15" class="text-center">
                        No hay facturas para mostrar
                    </td>
                </tr>
                @endforelse
            </tbody>
        </table>
    </div>
    <div class="dataTables_info">
        {{ $invoices->links() }}
        Mostrando {{$invoices->firstItem() ?? '0'}} a {{$invoices->lastItem() ?? '0'}} de
        {{$invoices->total()}} facturas
    </div>
</div>
