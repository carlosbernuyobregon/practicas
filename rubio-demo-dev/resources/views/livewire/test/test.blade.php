<div>
    <div>

        <h1>Laravel 8 Livewire Select2 Example - Tutsmake.com</h1>

        <strong>Select2 Dropdown: {{ $selCity }}</strong>

        <div wire:ignore>

            <select class="form-control form-select2" id="select2">

                <option value="">-- Select City --</option>

                @foreach($cities as $city)

                <option value="{{ $city }}">{{ $city }}</option>

                @endforeach

            </select>

        </div>

    </div>




</div>


