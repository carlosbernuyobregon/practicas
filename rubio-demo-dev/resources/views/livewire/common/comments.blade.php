<div>
    <div class="row">
        <div class="col-4">
            <h5>{{ $textTitle }}</h5>
        </div>
        @if ($allowUsersApproach)
            <div class="col-8">
                @if (!empty($usersList))
                <div class="mb-5">
                    <h5>Usuarios disponibles</h5>
                    @forelse ($usersList as $users)
                        <a wire:click="toggleSelectedUsers('{{ $users->id }}')" href="#" style="width: 150px !important; margin-bottom: 5px" class="badge badge-secondary">{{ Illuminate\Support\Str::limit($users->first_name . ' ' . $users->last_name, 17) }}</a>
                    @empty
                        <p>Todos los usuarios habilitados han sido agregados al chat</p>
                    @endforelse
                </div>
                @endif
                @if (!empty($selectedUsers))
                    <div class="{{--mt-3--}} mb-5">
                        <h5>Usuarios agregados</h5>
                        @forelse ($selectedUsers as $key => $selectedUser)
                            <a wire:click="toggleSelectedUsers('{{ $selectedUser['id'] }}')" href="#" style="width: 150px !important; margin-bottom: 5px" class="badge badge-success">{{ Illuminate\Support\Str::limit($selectedUser['first_name'] . ' ' . $selectedUser['last_name'], 17) }}</a>
                        @empty
                            <p>Agrega usuarios al chat</p>
                        @endforelse
                    </div>
                @endif
            </div>
        @endif
    </div>
    <div class="row">
        <div class="col-12">
            @forelse ($comments as $comment)
                <div class="card card-custom gutter-b mb-1">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-2">
                                <h6 class="card-label">
                                    {{ $comment->user->full_name }}
                                    <span class="font-weight-normal font-italic small">
                                        <p class="mt-1">{{ auth()->user()->applyDateTimeZoneFormat($comment->created_at) }}</p>
                                    </span>
                                </h6>
                            </div>
                            <div class="col-10">
                                <span class="font-weight-bold" style="color: black !important;">{!! $comment->comment !!}</span>
                            </div>

                        </div>


                    </div>
                </div>
            @empty
                No hay mensajes relacionados aún.
            @endforelse
        </div>
    </div>
    @if ($editable)
        <hr>
        <div class="row mb-3">
            <div class="col-12">
                <h5>Nuevo mensaje</h5>
            </div>
        </div>

        <div class="row">
            <div class="col-12">
                <div class="form-group">
                    <div wire:ignore>
                        <textarea  wire:model.lazy="newComment" class="form-control" rows="3">{{ $newComment}}</textarea>
                        @error('newComment') <span class="text-danger">{{ $message }}</span> @enderror
                    </div>
                </div>
            </div>
            <div class="col-12">
                <button wire:click.prevent="save()" class="btn btn-primary">Guardar mensaje</button>
            </div>
        </div>
    @endif
</div>

@push('js')
    <script type="text/javascript">
        window.onload = function() {
            // Send event to show or hide the chat tab depending con component configuration
            if ('{{ $allowUsersApproach }}') {
                Livewire.emit('setTabOnlyForAllowedUsers');
            }

            // Send event to start building the new message tab notification
            Livewire.emit('setTabNewMessageNotification');
        };

        // Show tab only to allowed users
        window.addEventListener('showTabOnlyForAllowedUsers', event => {
            var tabSelector = $('#' + event.detail.tabId);
            tabSelector.css('display', 'none');
            // tabSelector.css('display', 'block');
        });

        // Add new message count on tab
        window.addEventListener('showNewMessageNotification', event => {
            // let tabParentSelector   = $('#' + event.detail.tabParentId);
            var tabSelector         = $('#' + event.detail.tabId);

            if (event.detail.messageCount > 0) {
                tabSelector.append(
                    '<span id="new-messages-count" class="new-messages-count badge badge-pill badge-danger">' + event.detail.messageCount + '</span>'
                );
            }
        });

        // Defaults for tab clicking and cleaning the new message tab notification
        var tabSelector;
        var tabParentSelector;

        if ('{{ $this->tabId }}') {
            tabSelector = $('#' + '{{ $this->tabId }}');
        }

        if ('{{ $this->tabParentId }}') {
            tabParentSelector = $('#' + '{{ $this->tabParentId }} a');
        }

        if (tabSelector && tabParentSelector) {
            tabParentSelector.on('click', function (e) {
                e.preventDefault();

                if ($(this).attr('id') === '{{ $this->tabId }}') {
                    $('#new-messages-count').remove();
                    Livewire.emit('setNewMessageAsRead');
                }
            });
        }

    </script>
@endpush
