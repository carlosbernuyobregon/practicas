<div>
    <div class="row">
        <div class="col-12">
            <h5>{{ $title }}</h5>
        </div>
    </div>
    @if($enableUpload)
    <div class="row">
        <div class="col-6">
            <div wire.ignore>
                <form wire:submit.prevent="save">
                    <div class="input-group mb-3">
                        <input type="file" wire:model="files" class="form-control" multiple>
                        <div class="input-group-append">
                            <button type="submit" class="btn btn-sm btn-primary">Subir archivos</button>
                        </div>
                    </div>
                    @error('files.*') <span class="error">{{ $message }}</span> @enderror
                </form>
            </div>
            <div wire:loading class="mr-2 mt-2">
                <div class="lds-ring">
                    <div></div>
                    <div></div>
                    <div></div>
                    <div></div>
                </div>
            </div>
        </div>
    </div>
    @endif
    <div class="row">
        <div class="col-12">
            <table class="table table-striped table-bordered table-hover table-checkable">
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>Nombre del archivo</th>
                        <th>Fecha de creación</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    @forelse ($filteredFiles as $listFile)
                    <tr>
                        <td>{{ $listFile->id }}</td>
                        <td>{{ $listFile->name ?? '-' }}</td>
                        <th>
                            {{ $listFile->created_at->format('d/m/Y') }}
                        </th>
                        <td>
                            <div class="btn-group my-1" role="group" aria-label="">
                                <a href="{{ route('file.download',['file' => $listFile->id]) }}" class="btn btn-sm btn-warning">
                                    <i class="fas fa-file-download" style="padding-right: 0 !important;"></i>
                                </a>
                                {{--@if(false)
                                <button wire:click="editLine('{{ $line->id }}')"
                                    class="btn btn-sm btn-warning" title="Editar línea"><i class="fa fa-edit"></i></button>
                                <button wire:click="deleteLine('{{ $line->id }}')" class="btn btn-sm btn-danger"
                                    onclick="return confirm('backend.messages.delete-confirmation');" title="Eliminar línea"><i class="fa fa-trash"></i></button>
                                @endif--}}
                            </div>
                            @if ($enableDelete)
                                <div class="btn-group my-1" role="group" aria-label="">
                                    <a onclick="confirm('¿Seguro que deseas borrar el archivo?') || event.stopImmediatePropagation()" wire:click="delete('{{ $listFile->id }}')" href="#" class="btn btn-sm btn-danger">
                                        <i class="fas fa-trash" style="padding-right: 0 !important;"></i>
                                    </a>
                                </div>
                            @endif

                        </td>
                    </tr>
                    @empty
                    <tr>
                        <td colspan="11">
                            Aún no se han añadido archivos
                        </td>
                    </tr>
                    @endforelse
                </tbody>
            </table>
            {{ $filteredFiles->links() }}
        </div>
    </div>
</div>
