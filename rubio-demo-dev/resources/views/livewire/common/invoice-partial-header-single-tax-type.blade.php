<tr class="table-in-table-row">
    <td colspan="4">
        <table class="inside-table">
            <tr>
                <td colspan="2" class=" font-weight-bold" style="font-size: 14px">Total Base Imponible:</td>
                <td colspan="2" class="">
                    @if ($editMode)
                        <input wire:model="invoice.total_net" class="form-control form-control-sm {{ $errors->has('invoice.total_net') ? ' is-invalid' : '' }}"
                               type="text" name="" id="total_net">
                    @else
                        {{ number_format($invoice->total_net,2,',','.') }} {{ $invoice->currency }}
                    @endif
                    @error('invoice.total_net') <span class="text-danger">{{ $message }}</span> @enderror
                </td>
            </tr>
            <td colspan="2" class=" font-weight-bold" style="font-size: 14px">Base Imponible sin IVA 0%<br />(cálculo IRPF en SAP):</td>
            <td colspan="2" class="">
                {{ number_format($invoice->total_net_for_non_zero_tax_percentage, 2, ',', '.') }} {{ $invoice->currency }}
                @error('invoice.total_net') <span class="text-danger">{{ $message }}</span> @enderror
            </td>
            <tr>
            </tr>
        </table>
    </td>
    <td colspan="4">
        <table class="inside-table">
            <tr>
                <td class="font-weight-bold" style="font-size: 14px; width: 20% !important;">Descuento:</td>
                <td class="" style="width:30% !important;">
                    @if ($editMode)
                        <input wire:model.lazy="invoice.discount_amount" class="form-control form-control-sm {{ $errors->has('invoice.discount_amount') ? ' is-invalid' : '' }}"
                               type="text" name="" id="discount_amount">
                    @else
                        {{ number_format($invoice->discount_amount, 2, ',', '.') }} {{ $invoice->currency }}
                    @endif
                    @error('invoice.discount_amount') <span class="text-danger">{{ $message }}</span> @enderror
                </td>
                <td class="font-weight-bold" style="font-size: 14px; width: 20%">IRPF:</td>
                <td class="" style="width: 30% !important;">
                    @if ($editMode)
                        <input wire:model.lazy="invoice.taxes_irpf_amount" class="form-control form-control-sm ml-1 {{ $errors->has('invoice.taxes_irpf_amount') ? ' is-invalid' : '' }}"
                               type="text" name="" id="taxes_irpf_amount">
                    @else
                        {{ number_format($invoice->taxes_irpf_amount,2,',','.') }} {{ $invoice->currency }}
                    @endif
                    @error('invoice.taxes_irpf_amount') <span class="text-danger">{{ $message }}</span> @enderror
                </td>
            </tr>
        </table>
    </td>
    <td colspan="4">
        <table class="inside-table">
            <tr>
                <td class="font-weight-bold" style="font-size: 14px; width: 20%">Cuadre IVA: <span class="font-weight-normal font-italic small">(&pm; 2)</span> </td>
                <td class="" style="width: 30% !important;">
                    @if ($editMode)
                        <input wire:model.lazy="invoice.tax_balance_range" class="form-control form-control-sm ml-1 {{ $errors->has('invoice.tax_balance_range') ? ' is-invalid' : '' }}"
                               type="number" name="" id="tax_balance_range" min="-2" max="2">
                    @else
                        {{ number_format($invoice->tax_balance_range, 0, ',', '.') }}
                        @if ($invoice->tax_balance_range == 1)
                            cént.
                        @else
                            cts
                        @endif
                    @endif
                    @error('invoice.tax_balance_range') <span class="text-danger">{{ $message }}</span> @enderror
                </td>
                <td class="font-weight-bold" style="font-size: 14px; width: 20% !important;">Total Impuestos:</td>
                <td class="" style="width:30% !important;">
                    @if ($editMode)
                        <input wire:model.lazy="invoice.taxes_total_amount" class="form-control form-control-sm {{ $errors->has('invoice.taxes_total_amount') ? 'is-invalid' : '' }}"
                               type="text" name="" id="taxes_total_amount">
                    @else
                        {{ number_format($invoice->taxes_total_amount,2,',','.') }} {{ $invoice->currency }} ({{ $invoice->taxes_percent }}%)
                    @endif
                    @error('invoice.taxes_total_amount') <span class="text-danger">{{ $message }}</span> @enderror
                </td>
            </tr>
        </table>
    </td>
</tr>
<tr>
    <td colspan="2" class=" font-weight-bold" style="font-size: 14px">Total Factura:</td>
    <td colspan="2" class="">{{ number_format($invoice->total,2,',','.') }} {{ $invoice->currency }}</td>

    <td colspan="2" class="font-weight-bold" style="font-size: 14px">Documento SAP:</td>
    <td colspan="2">
        @if ($editMode || $editDocSAPMode)
            <input wire:model="invoice.sap_document_number" type="text" class="form-control form-control-sm ml-1 {{ $errors->has('invoice.sap_document_number') ? ' is-invalid' : '' }}">
        @else
            {{ $invoice->sap_document_number }}
        @endif
        @error('invoice.sap_document_number') <span class="text-danger">{{ $message }}</span>@enderror
    </td>
    <td colspan="2" class="font-weight-bold" style="font-size: 14px">Fecha contabilización:</td>
    <td colspan="2">
        @if ($editMode || $editDocSAPMode)
            <input wire:model="invoice.contabilized_at" type="date"
                   class="form-control form-control-sm {{ $errors->has('invoice.contabilized_at') ? ' is-invalid' : '' }}"
                   name="contabilized_at" id="invoice-contabilized_at">
        @else
            {{ auth()->user()->applyDateFormat($invoice->contabilized_at, true) ?? '' }}
        @endif
        @error('invoice.contabilized_at') <span class="text-danger">{{ $message }}</span> @enderror
    </td>
</tr>

