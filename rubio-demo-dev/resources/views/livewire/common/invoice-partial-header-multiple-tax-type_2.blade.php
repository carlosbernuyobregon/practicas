@php($countVar  = 0)
@for ($i = 2; $i <= 5; $i++)
    @php($netVar    = 'total_net_' . $i)
    @php($taxVar    = 'tax_amount_' . $i)
    @php($taxType   = 'tax_type_' . $i)
    @php($taxRate   = 'tax_rate_' . $i)
    @php($totalVar  = 'total_amount_' . $i)
    @php($countVar++)

    @if ($invoice->{$netVar} != 0 && $invoice->{$totalVar} != 0)
        <tr>
            <td colspan="2" class=" font-weight-bold" style="font-size: 14px">Base Imponible {{ \App\Helpers\Helpers::numberToRoman($countVar) }}:</td>
            <td colspan="2" class="">
                {{ number_format($invoice->{$netVar}, 2, ',', '.') }} {{ $invoice->currency }}
                @error('invoice.' . $netVar) <span class="text-danger">{{ $message }}</span> @enderror
            </td>
            <td colspan="2" class=" font-weight-bold" style="font-size: 14px">Impuestos {{ \App\Helpers\Helpers::numberToRoman($countVar) }}: <span style="font-weight: normal !important;">({{ $invoice->{$taxType} }} - {{ $invoice->{$taxRate} }}%)</span></td>
            <td colspan="2" class="">
                {{ number_format($invoice->{$taxVar}, 2, ',', '.') }} {{ $invoice->currency }}
                @error('invoice.' . $taxVar) <span class="text-danger">{{ $message }}</span> @enderror
            </td>
            <td colspan="2" class=" font-weight-bold" style="font-size: 14px">Total {{ \App\Helpers\Helpers::numberToRoman($countVar) }}:</td>
            <td colspan="2" class="">
                {{ number_format($invoice->{$totalVar}, 2, ',', '.') }} {{ $invoice->currency }}
                @error('invoice.' . $totalVar) <span class="text-danger">{{ $message }}</span> @enderror
            </td>
        </tr>
    @endif
@endfor
<tr class="table-in-table-row">
    <td colspan="4">
        <table class="inside-table">
            <tr>
                <td colspan="2" class=" font-weight-bold" style="font-size: 14px">Total Base Imponible:</td>
                <td colspan="2" class="">
                    {{ number_format($invoice->total_net, 2, ',', '.') }} {{ $invoice->currency }}
                    @error('invoice.total_net') <span class="text-danger">{{ $message }}</span> @enderror
                </td>
            </tr>
                <td colspan="2" class=" font-weight-bold" style="font-size: 14px">Base Imponible sin IVA 0%<br />(cálculo IRPF en SAP):</td>
                <td colspan="2" class="">
                    {{ number_format($invoice->total_net_for_non_zero_tax_percentage, 2, ',', '.') }} {{ $invoice->currency }}
                    @error('invoice.total_net') <span class="text-danger">{{ $message }}</span> @enderror
                </td>
            <tr>
            </tr>
        </table>
    </td>
    {{--<td colspan="2" class="font-weight-bold" style="font-size: 14px;">Total Impuestos:</td>
    <td colspan="2" class="">{{ number_format($invoice->taxes_total_amount, 2, ',', '.') }} {{ $invoice->currency }}</td>--}}
    <td colspan="4">
        <table class="inside-table">
            <tr>
                <td class="font-weight-bold" style="font-size: 14px; width: 20%">Cuadre IVA: <span class="font-weight-normal font-italic small">(&pm; 2)</span> </td>
                <td class="" style="width: 30% !important;">
                    @if ($editMode)
                        <input wire:model.lazy="invoice.tax_balance_range" class="form-control form-control-sm ml-1 {{ $errors->has('invoice.tax_balance_range') ? ' is-invalid' : '' }}"
                               type="number" name="" id="tax_balance_range" min="-2" max="2">
                    @else
                        {{ number_format($invoice->tax_balance_range, 0, ',', '.') }}
                        @if ($invoice->tax_balance_range == 1)
                            cént.
                        @else
                            cts
                        @endif
                    @endif
                    @error('invoice.tax_balance_range') <span class="text-danger">{{ $message }}</span> @enderror
                </td>
                <td class="font-weight-bold" style="font-size: 14px; width: 20% !important;">Total Impuestos:</td>
                <td class="" style="width:30% !important;">
                    {{ number_format($invoice->taxes_total_amount, 2, ',', '.') }} {{ $invoice->currency }}
                </td>
            </tr>
        </table>
    </td>
    <td colspan="2" class="font-weight-bold" style="font-size: 14px;">Total Factura:</td>
    <td colspan="2" class="">{{ number_format($invoice->total,2,',','.') }} {{ $invoice->currency }}</td>
</tr>
<tr>
    <td colspan="2" class="font-weight-bold" style="font-size: 14px;">IRPF:</td>
    <td colspan="2" class="">
        @if ($editMode)
            <input wire:model.lazy="invoice.taxes_irpf_amount" class="form-control form-control-sm ml-1 {{ $errors->has('invoice.taxes_irpf_amount') ? ' is-invalid' : '' }}"
                   type="text" name="" id="taxes_irpf_amount">
        @else
            {{ number_format($invoice->taxes_irpf_amount, 2, ',', '.') }} {{ $invoice->currency }}
        @endif
        @error('invoice.taxes_irpf_amount') <span class="text-danger">{{ $message }}</span> @enderror
    </td>
    <td colspan="2" class="font-weight-bold" style="font-size: 14px;">Descuento:</td>
    <td colspan="2" class="">
        @if ($editMode)
            <input wire:model="invoice.discount_amount" class="form-control form-control-sm {{ $errors->has('invoice.discount_amount') ? ' is-invalid' : '' }}"
                   type="text" name="" id="discount_amount">
        @else
            {{ number_format($invoice->discount_amount, 2, ',', '.') }} {{ $invoice->currency }}
        @endif
        @error('invoice.discount_amount') <span class="text-danger">{{ $message }}</span> @enderror
    </td>
    <td colspan="2"></td>
    <td colspan="2"></td>
</tr>
<tr>
    <td colspan="2" class="font-weight-bold" style="font-size: 14px">Documento SAP:</td>
    <td colspan="2">
        @if ($editMode || $editDocSAPMode)
            <input wire:model="invoice.sap_document_number" type="text" class="form-control form-control-sm ml-1 {{ $errors->has('invoice.sap_document_number') ? ' is-invalid' : '' }}">
        @else
            {{ $invoice->sap_document_number }}
        @endif
        @error('invoice.sap_document_number') <span class="text-danger">{{ $message }}</span>@enderror
    </td>
    <td colspan="2" class="font-weight-bold" style="font-size: 14px">Fecha contabilización:</td>
    <td colspan="2">
        @if ($editMode || $editDocSAPMode)
            <input wire:model="invoice.contabilized_at" type="date"
                   class="form-control form-control-sm {{ $errors->has('invoice.contabilized_at') ? ' is-invalid' : '' }}"
                   name="contabilized_at" id="invoice-contabilized_at">
        @else
            {{ auth()->user()->applyDateFormat($invoice->contabilized_at, true) ?? '' }}
        @endif
        @error('invoice.contabilized_at') <span class="text-danger">{{ $message }}</span> @enderror
    </td>
    <td colspan="2"></td>
    <td colspan="2"></td>
</tr>
