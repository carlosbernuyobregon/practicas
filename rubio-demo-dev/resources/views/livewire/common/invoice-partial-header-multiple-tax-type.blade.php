<tr class="table-in-table-row">
    <td colspan="12">
        <table class="inside-table totals-table">
            <tr>
                <td class="table-header font-weight-bold" style="width: 16%;">Impuestos</td>
                <td class="table-header font-weight-bold" style="width: 14%;">Base Imponible</td>
                <td class="table-header font-weight-bold" style="width: 14%;">II</td>
                <td class="table-header font-weight-bold" style="width: 14%;">Total (BI + II)</td>
                <td class="table-header font-weight-bold" style="width: 14%;">Ajuste II <span class="font-weight-normal font-italic small">(&pm; 2) cts</td>
                <td class="table-header font-weight-bold" style="width: 14%;">Total II ajustado</td>
                <td class="table-header font-weight-bold" style="width: 14%;">Total BI + II (ajustado)</td>
            </tr>
            @php($countVar  = 0)
            @php($sumAdjustedTotal = 0)
            @php($sumTaxBalance = 0)
            @php($biPlusII = 0)
            @php($genericOcrErrorText = 'El valor es distinto al capturado por el OCR. Constatar con valores de la factura pdf')
            @php($genericOcrWarningText = 'El valor no pudo ser capturado por el OCR y por lo tanto no podemos comparar. Constatar con valores de la factura pdf')
            @for ($i = 2; $i <= 5; $i++)
                @php($grossVar          = 'total_gross_' . $i)
                @php($netVar            = 'total_net_' . $i)
                @php($taxVar            = 'tax_amount_' . $i)
                @php($adjustedTaxVar    = 'adjusted_tax_amount_' . $i)
                @php($taxRate           = 'tax_rate_' . $i)
                @php($taxType           = 'tax_type_' . $i)
                @php($taxBalance        = 'tax_balance_range_' . $i)
                @php($sumTaxBalance     += intval($invoice->{$taxBalance}))
                @php($biPlusII          += $invoice->{$netVar} + $invoice->{$taxVar})
                @php($totalVar          = 'total_amount_' . $i)
                @php($totalAdjustedVar  = 'adjusted_total_amount_' . $i)
                @php($sumAdjustedTotal  += $invoice->{$totalAdjustedVar})
                @php($countVar++)

                @if ($invoice->{$netVar} != 0 && $invoice->{$totalVar} != 0)
                    <tr>
                        {{-- Impuestos --}}
                        <td class="">{{ $invoice->{$taxType} }} - {{ $invoice->{$taxRate} }}%</td>
                        {{-- Base Imponible --}}
                        <td class="">{{ number_format($invoice->{$netVar}, 2, ',', '.') }} {{ $invoice->currency }}
                            @error('invoice.' . $netVar) <span class="text-danger">{{ $message }}</span> @enderror
                        </td>
                        {{-- II --}}
                        <td class="">
                            {{ number_format($invoice->{$taxVar}, 2, ',', '.') }} {{ $invoice->currency }}
                            @error('invoice.' . $taxVar) <span class="text-danger">{{ $message }}</span> @enderror
                        </td>
                        {{-- Total (BI + II) --}}
                        <td class="">
                            {{ number_format($invoice->{$netVar} + $invoice->{$taxVar}, 2, ',', '.') }} {{ $invoice->currency }}
                        </td>
                        {{-- Ajuste II --}}
                        <td class="">
                            @if ($editMode)
                                <input wire:model.lazy="invoice.{{ $taxBalance }}" class="form-control form-control-sm ml-1 {{ $errors->has('invoice.' . $taxBalance) ? ' is-invalid' : '' }}"
                                       type="number" name="" id="{{ $taxBalance }}" min="-2" max="2">
                            @else
                                {{ number_format($invoice->{$taxBalance}, 0, ',', '.') }}
                                @if ($invoice->{$taxBalance} == 1)
                                    cént.
                                @else
                                    cts
                                @endif
                            @endif
                            @error('invoice.' . $taxBalance) <span class="text-danger">{{ $message }}</span> @enderror
                        </td>
                        {{-- II Ajustado --}}
                        <td class="">
                            {{ number_format($invoice->{$adjustedTaxVar}, 2, ',', '.') }} {{ $invoice->currency }}
                            @error('invoice.' . $totalVar) <span class="text-danger">{{ $message }}</span> @enderror
                        </td>
                        {{-- Total BI + II (ajustado) --}}
                        <td class="{{--@if ($invoice->{$totalVar} != $invoice->{$totalAdjustedVar}) check-error @endif--}}">
                            {{ number_format($invoice->{$totalAdjustedVar}, 2, ',', '.') }} {{ $invoice->currency }}
                            @error('invoice.' . $totalVar) <span class="text-danger">{{ $message }}</span> @enderror
                        </td>
                    </tr>
                @endif
            @endfor
            <tr>
                <td colspan="1" class="table-header font-weight-bold" style="">Total:</td>
                <td colspan="1" class="total-value table-header">
                    {{ number_format($invoice->total_net, 2, ',', '.') }} {{ $invoice->currency }}
                    @error('invoice.total_net') <span class="text-danger">{{ $message }}</span> @enderror
                </td>
                <td colspan="1" class="total-value table-header">
                    {{ number_format($invoice->taxes_total_amount_without_adjustment, 2, ',', '.') }} {{ $invoice->currency }}
                </td>
                <td colspan="1" class="total-value table-header">{{ number_format($biPlusII, 2, ',', '.') }} {{ $invoice->currency }}</td>
                <td colspan="1" class="total-value table-header">{{ number_format($sumTaxBalance, 0, ',', '.') }}
                    @if ($sumTaxBalance == 1)
                        cént.
                    @else
                        cts
                    @endif
                </td>
                <td colspan="1" class="total-value table-header @if ($originalTotalTaxesError) check-{{ $originalTotalTaxesError }}@endif">
                    {{ number_format($invoice->taxes_total_amount, 2, ',', '.') }} {{ $invoice->currency }}
                    @if ($originalTotalTaxesError)<span class="info-tooltip" data-toggle="tooltip" data-placement="bottom" title="{{
                        ($originalTotalTaxesError == \App\Library\Constants::CHECK_ERROR) ? $genericOcrErrorText : $genericOcrWarningText
                    }}">&#9432;</span>@endif
                    @error('invoice.total_net') <span class="text-danger">{{ $message }}</span> @enderror
                </td>
                <td colspan="1" class="total-value table-header">{{ number_format($sumAdjustedTotal, 2, ',', '.') }} {{ $invoice->currency }}</td>
            </tr>

        </table>
    </td>
</tr>
<tr class="table-in-table-row">
    <td colspan="12">
        <table class="inside-table totals-table">
            <tr>
                <td class="table-header font-weight-bold" style="width: 15%">Base II cálculo IRPF:</td>
                {{-- ToDo: Comentado hast que se apruebe la edición del valor y se dispare la lógica de valor calculado vs editado. El campo que lee SAP es el mismo que ya existía así que no hay que ajustar nada de esa parte  --}}
                <td colspan="1" class="total-value  @if ($baseForIrpfError) check-{{ $baseForIrpfError }}@endif">
                    @if ($editMode)
                        <input wire:model.lazy="invoice.total_net_for_non_zero_tax_percentage" class="form-control form-control-sm ml-1 {{ $errors->has('invoice.total_net_for_non_zero_tax_percentage') ? ' is-invalid' : '' }}"
                               type="text" name="" id="total_net_for_non_zero_tax_percentage">
                    @else
                        @if ($invoice->total_net_for_non_zero_tax_percentage != 0)
                            @php($baseForIrpfErrorTxt = 'El valor no se corresponde con la fórmula: Base II cálculo IRPF - IRPF + IVA = TOTAL FACTURA')
                            {{ number_format($invoice->total_net_for_non_zero_tax_percentage, 2, ',', '.') }} {{ $invoice->currency }}
                            @if ($baseForIrpfError)<span class="info-tooltip" data-toggle="tooltip" data-placement="bottom" title="{{ $baseForIrpfErrorTxt }}">&#9432;</span>@endif
                        @else
                            {{ number_format($invoice->total_net_for_non_zero_tax_percentage_calculated, 2, ',', '.') }} {{ $invoice->currency }}
                        @endif
                    @endif
                    @error('invoice.total_net_for_non_zero_tax_percentage') <span class="text-danger">{{ $message }}</span> @enderror
{{--                    {{ number_format($invoice->total_net_for_non_zero_tax_percentage, 2, ',', '.') }} {{ $invoice->currency }}--}}
                </td>
                <td class="table-header font-weight-bold" style="width: 15%">Total IRPF:</td>
                <td colspan="1" class="total-value  @if ($originalTotalIrpfError) check-{{ $originalTotalIrpfError }} @endif">
                    @if ($editMode)
                        <input wire:model.lazy="invoice.taxes_irpf_amount" class="form-control form-control-sm ml-1 {{ $errors->has('invoice.taxes_irpf_amount') ? ' is-invalid' : '' }}"
                               type="text" name="" id="taxes_irpf_amount">
                    @else
                        {{ number_format($invoice->taxes_irpf_amount, 2, ',', '.') }} {{ $invoice->currency }}
                        @if ($originalTotalIrpfError)<span class="info-tooltip" data-toggle="tooltip" data-placement="bottom" title="{{
                            ($originalTotalIrpfError == \App\Library\Constants::CHECK_ERROR) ? $genericOcrErrorText : $genericOcrWarningText
                        }}">&#9432;</span>@endif
                    @endif
                    @error('invoice.taxes_irpf_amount') <span class="text-danger">{{ $message }}</span> @enderror
                </td>
                <td class="table-header font-weight-bold" style="width: 15%">Total Factura:</td>
                <td colspan="1" class="total-value @if ($originalTotalError) check-{{ $originalTotalError }} @endif">
                    {{ number_format($invoice->total, 2, ',', '.') }} {{ $invoice->currency }}
                    @if ($originalTotalError)<span class="info-tooltip" data-toggle="tooltip" data-placement="bottom" title="{{
                        ($originalTotalError == \App\Library\Constants::CHECK_ERROR) ? $genericOcrErrorText : $genericOcrWarningText
                    }}">&#9432;</span>@endif
                </td>
            </tr>
        </table>
    </td>
</tr>
<tr>
    <td colspan="2" class="font-weight-bold" style="">Documento SAP:</td>
    <td colspan="2">
        @if ($editMode || $editDocSAPMode)
            <input wire:model.lazy="invoice.sap_document_number" type="text" class="form-control form-control-sm ml-1 {{ $errors->has('invoice.sap_document_number') ? ' is-invalid' : '' }}">
        @else
            {{ $invoice->sap_document_number }}
        @endif
        @error('invoice.sap_document_number') <span class="text-danger">{{ $message }}</span>@enderror
    </td>
    <td colspan="2" class="font-weight-bold" style="">Fecha contabilización:</td>
    <td colspan="2">
        @if ($editMode || $editDocSAPMode)
            <input wire:model.lazy="invoice.contabilized_at" type="date"
                   class="form-control form-control-sm {{ $errors->has('invoice.contabilized_at') ? ' is-invalid' : '' }}"
                   name="contabilized_at" id="invoice-contabilized_at">
        @else
            {{ auth()->user()->applyDateFormat($invoice->contabilized_at, true) ?? '' }}
        @endif
        @error('invoice.contabilized_at') <span class="text-danger">{{ $message }}</span> @enderror
    </td>
    <td colspan="2"></td>
    <td colspan="2"></td>
</tr>
