<div class="{{--position-relative--}}">
    <div wire:ignore>
        <select id="{{ $selectId }}" class="form-control form-select" name="{{ $selectName }}"
                wire:model="objectValue">
            <option value="">{{ $placeholder }}</option>
            @foreach ($objectsAll as $object)
                <option value="{{ $object->{$optionIdFieldMapping} }}">{{ $object->{$optionNameFieldMapping} }}</option>
            @endforeach
        </select>
    </div>

    @if ($checkValuesDebugButton)
        <button wire:click="checkValues()" class="btn btn-sm btn-success" >
            <i class="fas fa-check"></i> check values
        </button>
    @endif

</div>

@push('js')
    @if ($includeJsCdn)
        <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
    @endif
    <script>
        $(document).ready(function() {

            // Defaults
            let objectValue             = '{{ $objectValue }}';
            let customId                = '{{ $selectId }}';
            let customSelector          = $('#' + customId);

            // If there is an existent value representing a selected option from the select, assign that value to the
            // Selector and trigger a change event. Then hydrate the component property
            if (objectValue !== '' && customSelector.val('{{ $objectValue }}').trigger('change')) {
                @this.set('objectValue', '{{ $objectValue }}');
            }

            // Settings
            customSelector.select2({
                theme: "bootstrap-5",
                containerCssClass: "select2--large", // For Select2 v4.0
                selectionCssClass: "select2--large", // For Select2 v4.1
                dropdownCssClass: "select2--small",
                minimumInputLength: '{{ $minimumInputLength ?? 0 }}', // Only start searching when the user has input 3 or more characters

            });

            // On change event
            customSelector.on('change', function (e) {
                @this.set('objectValue', e.target.value);
                // Livewire.emitTo('invoices.header', 'checkValuesEvent', e.target.value); // This emit to event is being
                // triggered from a component's method (updatedObjectValue). This is another way of doing it
            });
        });

    </script>
@endpush
