<div>
    <div class="row justify-content-between">
        <div class="col-12">
            <h4>@lang('process.actions.title')</h4>
        </div>
        <div class="col-12">
            <div wire:loading class="">
                <div class="lds-ring">
                    <div></div>
                    <div></div>
                    <div></div>
                    <div></div>
                </div>
            </div>
        </div>
        <div class="col-12">
            @if (count($actions) > 0)
                @foreach ($actions as $action)
                <button type="button" wire:click="submitAction('{{ trim($action) }}')" class="btn btn-primary"
                        custom-action="{{ trim($action) }}" wire:loading.attr="disabled">@lang('process.actions.'.trim($action))</button>
                @endforeach
            @else
            <p>There's no actions available.</p>
            @endif
        </div>
    </div>
</div>
