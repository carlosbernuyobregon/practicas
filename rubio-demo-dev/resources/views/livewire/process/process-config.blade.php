<html>
<body>
@livewireStyles()
<div class="container">
  <div>PROCESS CONFIG</div>
  <table class="table table-bordered data-table">
    <thead>
    <tr>
        <th>ID</th>
        <th>Key Value</th>
        <th>Model</th>
        <th>BPM ID</th>
        <th>BPM Version ID</th>
        <th>BPM Name</th>
        <th>Name</th>
        <th>Description</th>
        <th>Tenant ID</th>
        <th>Variables</th>
        <th>Version</th>
        <th>Active</th>
        <th>Created At</th>
        <th>Updated At</th>
       <!-- <td>Form URL </td> -->
    </tr>
</thead>
    @foreach($process as $item)
    <tr>
    <td>{{ $item->id }}</td>
    <td>{{ $item->key_value }}</td>
    <td>{{ $item->model }}</td>
    <td>{{ $item->bpm_id }}</td>
    <td>{{ $item->bpm_version_id }}</td>
    <td>{{ $item->bpm_name }}</td>
    <td>{{ $item->name }}</td>
    <td>{{ $item->description }}</td>
    <td>{{ $item->tenant_id }}</td>
    <td>{{ $item->variables }}</td>
    <td>{{ $item->version }}</td>
    <td>{{ $item->active }}</td>
    <td>{{ $item->created_at }}</td>
    <td>{{ $item->updated_at }}</td>
    <td>
        <a href="{{ route('process.edit',[$item->id]) }}">Editar</a>
    </td>
    </tr>
    @endforeach

</table>
</div>
<div>
    <button type="button" wire:click.prevent="$emit('showModal', 'Add New Process')">Add New Process</button>
   <!-- <button type="button" wire:click.prevent="$emit('showModal', 'Edit Process')">Edit Process</button> -->
   <!-- <button type="button" wire:click.prevent="$emit('showModal', 'Delete Process')">Delete Process</button> -->
    <button type="button" wire:click.wire:click="delete">Delete All Processes</button>
</div>


<livewire:process.process-config-modal />
@livewireScripts()

</body>
