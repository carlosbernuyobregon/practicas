@php
    $show = true;
    //dd($editData);

// $this->editData = $editData;
//dd($this->editData);
@endphp
<div>
    <div class="modal fade @if($show === true) show @endif"
         id="processConfigModal"
         style="display: @if($show === true)
                 block
         @else
                 none
         @endif;
         tabindex="-1"
         role="dialog"
         aria-labelledby="processConfigModalLabel"
         aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="processConfigModalLabel">Process Definition</h5>
                </div>
                <div class="modal-body"></div>
                <br>
                <div class="header">
                </div>
                <div class="container">
                     <tr>
                    <label for="id">ID</label><br>
                    <input type="text" id="id" wire:model="editData.id"><br><br>
                    <label for="key_value">Key Value</label><br><br>
                    <input type="text" id="Key value" wire:model="editData.key_value"><br><br>
                    <label for="model">Model</label><br><br>
                    <input type="text" id="model" wire:model="editData.model"><br><br>
                    <label for="bpm_id">BPM ID</label><br><br>
                    <input type="text" id="bpm_id" wire:model="editData.bpm_id"><br><br>
                    <label for="bpm_version_id">BPM VERSION ID</label><br><br>
                    <input type="text" id="bpm_version_id" wire:model="editData.bpm_version_id"><br><br>
                    <label for="bpm_name">BPM NAME</label><br><br>
                    <input type="text" id="bpm_name" wire:model="editData.bpm_name"><br><br>
                    <label for="name">Name</label><br>
                    <input type="text" id="name" wire:model="editData.name"><br><br>
                    <label for="description">Description</label><br>
                    <input type="text" id="description" wire:model="editData.description"><br><br>
                    <label for="tenant_id">Tenant ID</label><br>
                    <input type="text" id="tenant_id" wire:model="editData.tenant_id"><br><br>
                    <label for="variables">Variables</label><br>
                    <input type="text" id="variables" wire:model="editData.variables"><br><br>
                    <label for="version">Version</label><br>
                    <input type="text" id="version" wire:model="editData.version"><br><br>
                    <label for="active">Active</label><br>
                    <input type="checkbox" id="active" wire:model="editData.active"><br><br>
                </tr>
            </table>
        </div>

                <div class="modal-footer">
                            <button class="btn btn-secondary"
                            type="button"
                            wire:click.prevent="editProcess()">Save</button>
                            <button class="btn btn-secondary"
                            type="button"
                            wire:click.prevent="returnParent()">Cancel Edit</button>
                            <button class="btn btn-secondary"
                            type="button"
                            wire:click.prevent="deleteProcess()">Delete</button>

                </div>
            </div>
        </div>
    </div>
    <!-- Let's also add the backdrop / overlay here -->
    <div class="modal-backdrop fade show"
         id="backdrop"
         style="display: @if($show === true)
                 block
         @else
                 none
         @endif;"></div>
</div>

