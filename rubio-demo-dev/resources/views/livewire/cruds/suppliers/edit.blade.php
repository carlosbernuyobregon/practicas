<div>
    <div class="row mt-2">
        <div class="col-12 col-md-6 py-2">
            <h4>Editar proveedor</h4>
        </div>
        <div class="col-12 col-md-6 text-right">
            <div wire:loading class="">
                <div class="lds-ring">
                    <div></div>
                    <div></div>
                    <div></div>
                    <div></div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-12">
            @if (session()->has('alert'))
                <div class="alert alert-danger">
                    {{ session('alert') }}
                </div>
            @endif
            @if (session()->has('success'))
                <div class="alert alert-success">
                    {{ session('success') }}
                </div>
            @endif
        </div>
    </div>
    <div class="row">
        <div class="col-12">
            <ul class="nav nav-tabs" id="myTab" role="tablist">
                <li class="nav-item" role="presentation" wire:ignore>
                    <a class="nav-link active" id="supplier-header-tab" data-toggle="tab"
                        href="#supplier-header" role="tab" aria-controls="supplier-header"
                        aria-selected="true">Ficha proveedor</a>
                </li>
                <!--
                <li class="nav-item" role="presentation" wire:ignore>
                    <a class="nav-link" id="supplier-messages-tab" data-toggle="tab" href="#supplier-messages"
                        role="tab" aria-controls="supplier-messages" aria-selected="true">Chat interno</a>
                </li>
                -->
                <li class="nav-item" role="presentation" wire:ignore>
                    <a class="nav-link" id="supplier-files-tab" data-toggle="tab" href="#supplier-files" role="tab" aria-controls="supplier-files"
                        aria-selected="true">Archivos</a>
                </li>
                <li class="nav-item" role="presentation" wire:ignore>
                    <a class="nav-link" id="supplier-audit-tab" data-toggle="tab" href="#supplier-audit" role="tab"
                        aria-controls="supplier-audit" aria-selected="true">Auditoría</a>
                </li>
            </ul>
            <div class="tab-content" id="orderContent">
                <div class="tab-pane fade show active" id="supplier-header" role="tabpanel"
                    aria-labelledby="supplier-header-tab" wire:ignore.self>
                    @include('livewire.cruds.suppliers.partials.header')
                </div>
                <!--
                <div class="tab-pane fade" id="supplier-messages" role="tabpanel"
                    aria-labelledby="supplier-messages-tab" wire:ignore.self>
                    @livewire('common.comments',[
                        'entityId' => $provider->id,
                        'entityType' => 'App\Models\Provider',
                        'textTitle' => 'Chat interno',
                        'editable' => true
                    ])
                </div>
                -->
                <div class="tab-pane fade" id="supplier-files" role="tabpanel"
                    aria-labelledby="supplier-files-tab" wire:ignore.self>
                    @livewire('common.file-upload',[
                        's3' => true,
                        'path' => config('custom.iworking_public_bucket_folder') . '/provider/' . now()->format('Y/m/d') . '/'
                        . (string)$provider->id,
                        'modelId' => (string)$provider->id,
                        'model' => 'App\Models\Provider',
                        'type' => 'provider-attachment',
                        'enableUpload' => $editable,
                        'enableDelete' => $editable,
                        'eventIdentifier' => 'providerFilesUpdated',
                        'title' => 'Certificado bancario *:'
                        ], key(time() . 'file-uploader'))
                </div>
                <div class="tab-pane fade" id="supplier-audit" role="tabpanel"
                    aria-labelledby="supplier-audit-tab" wire:ignore.self>
                    @livewire('common.audit-table',[
                        'dataValue' => $provider
                    ])
                </div>
            </div>
        </div>
    </div>
    <hr>
    <div class="row">
        <div class="col-12 col-md-6">
            @livewire('process.actions',[
                    'actions' => $actions,
                    'processInstance' => $processInstance
                ])
        </div>
        <div class="col-12 col-md-6">
            <div class="row">
                <div class="col-12">

                </div>
                <div class="col-12">
                    <div class="btn-group my-1" role="group" aria-label="">
                        <a href="{{ route('processes.task-list') }}" class="btn btn-sm btn-success d-flex p-4 py-lg-2">
                            Salir
                        </a>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>
