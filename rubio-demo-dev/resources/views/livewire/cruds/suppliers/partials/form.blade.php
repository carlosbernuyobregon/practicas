<div wire:ignore.self id="formModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="formModalLabel"
    aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                @if (!isset($this->supplier->id) || $this->supplier->id == null)
                    <h5 class="modal-title" id="formModalLabel">@lang('backend.crud.supplier.form.add')</h5>
                @else
                    <h5 class="modal-title" id="formModalLabel">@lang('backend.crud.supplier.form.update')</h5>
                @endif
                <button wire:click.prevent="resetInputs()" type="button" class="close" data-dismiss="modal"
                    aria-label="Close">
                    <span aria-hidden="true close-btn">×</span>
                </button>
            </div>
            <div class="modal-body">
                @if (session()->has('error'))
                    <div class="alert alert-danger alert-dismissible fade show" role="alert">
                        {{ session('error') }}
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                @endif
                <form>
                    @if (!isset($this->supplier->id) || $this->supplier->id == null)
                        <!--external id-->
                        <div class="form-group {{ $errors->has(`supplier.external_id`) ? ' has-danger' : '' }}">
                            <label for="name">@lang('backend.crud.supplier.form.external_id')</label>
                            <input wire:model="supplier.external_id" type="text" id="external_id_input"
                                class="form-control {{ $errors->has('supplier.external_id') ? ' is-invalid' : '' }}"
                                placeholder="@lang('backend.crud.supplier.form.external_id_placeholder')" />
                            @error('supplier.external_id')
                                <span class="text-danger">{{ $message }}</span>
                            @enderror
                        </div>
                    @endif

                    <div class="form-group {{ $errors->has(`supplier.vat_number`) ? ' has-danger' : '' }}">
                        <label for="name">@lang('backend.crud.supplier.form.vat_number')</label>
                        <input wire:model="supplier.vat_number" type="text" id="vat_number_input"
                            class="form-control {{ $errors->has('supplier.vat_number') ? ' is-invalid' : '' }}"
                            placeholder="@lang('backend.crud.supplier.form.vat_number_placeholder')" />
                        @error('supplier.vat_number')
                            <span class="text-danger">{{ $message }}</span>
                        @enderror
                    </div>

                    <div class="form-group {{ $errors->has(`supplier.community_vat_number`) ? ' has-danger' : '' }}">
                        <label for="name">@lang('backend.crud.supplier.form.community_vat_number')</label>
                        <input wire:model="supplier.community_vat_number" type="text" id="community_vat_number_input"
                            class="form-control {{ $errors->has('supplier.community_vat_number') ? ' is-invalid' : '' }}"
                            placeholder="@lang('backend.crud.supplier.form.community_vat_number_placeholder')" />
                        @error('supplier.community_vat_number')
                            <span class="text-danger">{{ $message }}</span>
                        @enderror
                    </div>

                    <div class="form-group {{ $errors->has(`supplier.name`) ? ' has-danger' : '' }}">
                        <label for="name">@lang('backend.crud.supplier.form.name')</label>
                        <input wire:model="supplier.name" type="text" id="name_input"
                            class="form-control {{ $errors->has('supplier.name') ? ' is-invalid' : '' }}"
                            placeholder="@lang('backend.crud.supplier.form.name_placeholder')" />
                        @error('supplier.name')
                            <span class="text-danger">{{ $message }}</span>
                        @enderror
                    </div>

                    <div class="form-group {{ $errors->has(`supplier.company`) ? ' has-danger' : '' }}">
                        <label for="name">@lang('backend.crud.supplier.form.company')</label>
                        <input wire:model="supplier.company" type="text" id="company_input"
                            class="form-control {{ $errors->has('supplier.company') ? ' is-invalid' : '' }}"
                            placeholder="@lang('backend.crud.supplier.form.company')" />
                        @error('supplier.company')
                            <span class="text-danger">{{ $message }}</span>
                        @enderror
                    </div>

                    <div class="form-group {{ $errors->has(`supplier.payment_condition`) ? ' has-danger' : '' }}">
                        <label for="payment-condition">@lang('backend.crud.supplier.form.payment_condition')</label>
                        <select wire:model="supplier.payment_condition" class="form-control" name="payment-condition">
                            <option value="">Seleccionar condición pago</option>
                            @foreach ($providerPaymentTerms as $providerPaymentTerm)
                                <option value="{{ $providerPaymentTerm->code }}" @if($providerPaymentTerm->is_default) selected @endif>{{ $providerPaymentTerm->code }} - {{ $providerPaymentTerm->name }}</option>
                            @endforeach
                        </select>
                        @error('supplier.payment_condition')
                            <span class="text-danger">{{ $message }}</span>
                        @enderror
                    </div>

                    <div class="form-group {{ $errors->has(`supplier.payment_type`) ? ' has-danger' : '' }}">
                        <label for="payment-type">@lang('backend.crud.supplier.form.payment_type')</label>
                        <select wire:model.lazy="supplier.payment_type" class="form-control" name="payment-type">
                            <option value="">Seleccionar tipo pago</option>
                            @foreach ($providerPaymentTypes as $providerPaymentType)
                                <option value="{{ $providerPaymentType->code }}" @if($providerPaymentType->is_default) selected @endif>{{ $providerPaymentType->name }}</option>
                            @endforeach
                        </select>
                        @error('supplier.payment_type')
                            <span class="text-danger">{{ $message }}</span>
                        @enderror
                    </div>

                    <div class="form-group {{ $errors->has(`supplier.iban`) ? ' has-danger' : '' }}">
                        <label for="name">IBAN</label>
                        <input wire:model="supplier.iban" type="text" id="iban_input"
                               class="form-control {{ $errors->has('supplier.iban') ? ' is-invalid' : '' }}"
                               placeholder="IBAN" />
                        @error('supplier.iban')
                        <span class="text-danger">{{ $message }}</span>
                        @enderror
                    </div>
                    <div class="form-group {{ $errors->has('provider.email') ? ' has-danger' : '' }}">
                        <label for="email">Email administración</label>
                        <input wire:model.lazy="provider.email" type="text" name="provider-email"
                               class="form-control {{ $errors->has('provider.email') ? ' is-invalid' : '' }}"
                               placeholder="Email administración" />
                        @error('provider.email')<span class="text-danger">{{ $message }}</span>@enderror
                    </div>
                    <div class="form-group {{ $errors->has('provider.email_orders') ? ' has-danger' : '' }}">
                        <label for="email_orders">Email comercial</label>
                        <input wire:model.lazy="provider.email_orders" type="text" name="provider-email_orders"
                               class="form-control {{ $errors->has('provider.email_orders') ? ' is-invalid' : '' }}"
                               placeholder="Email comercial" />
                        @error('provider.email_orders')<span class="text-danger">{{ $message }}</span>@enderror
                    </div>

                    <div class="form-group {{ $errors->has('provider.mediator') ? ' has-danger' : '' }}">
                        <label for="provider-mediator">Interlocutor *:</label>
                        <select wire:model.lazy="supplier.mediator" class="form-control" name="provider-mediator">
                            <option value="">Seleccionar Interlocutor</option>
                            @foreach ($mediators as $mediator)
                                <option value="{{ $mediator->person }}">{{ $mediator->name }} - {{ $mediator->person }}</option>
                            @endforeach
                        </select>
                        @error('provider.mediator')<span class="text-danger">{{ $message }}</span>@enderror
                    </div>

                    <div class="form-group">
                        <label>Solicitud de Pedido:</label>
                        <div class="kt-checkbox-list">
                            <span class="kt-switch kt-switch--sm kt-switch--brand">
                                <label>
                                    <input wire:model="supplier.receiving_order_copy" name="toggle_rgpd" type="checkbox"
                                           class="form-control">
                                    <p style="display:inline-flex; padding: 5px 0 0 5px">
                                        @if ($this->supplier && $this->supplier->receiving_order_copy)
                                            Desactivar
                                        @else
                                            Activar
                                        @endif
                                            envío al Proveedor
                                    </p>
                                    <span style="margin-left: 5px"></span>
                                </label>
                            </span>
                        </div>
                    </div>

                    <div class="form-group {{ $errors->has('supplier.customer_id') ? ' has-danger' : '' }}">
                        <label for="customer_id">@lang('backend.crud.supplier.form.customer')</label>
                        <select class="form-control {{ $errors->has('supplier.customer_id') ? ' is-invalid' : '' }}"
                            id="customer_id_input" wire:model="supplier.customer_id">
                            <option value="">@lang('backend.forms.select-option')</option>
                            @foreach ($customers as $customer)
                                <option value="{{ $customer->id }}">{{ $customer->name }}</option>
                            @endforeach
                        </select>
                        @error('supplier.customer_id')<span class="text-danger">{{ $message }}</span>
                        @enderror
                    </div>

                    <div class="form-group {{ $errors->has('supplier.country') ? ' has-danger' : '' }}">
                        <label for="country_id">@lang('backend.crud.supplier.form.country')</label>
                        <select wire:model='supplier.country'
                                class="form-control {{ $errors->has('supplier.country') ? ' is-invalid' : '' }}"
                                id="country_input" wire:model="supplier.country">
                            <option value="">@lang('backend.forms.select-option')</option>
                            @foreach ($this->countries as $country)
                                <option value="{{ $country->code }}">
                                    {{ Str::title($country->key_value) }}</option>
                            @endforeach
                        </select>
                        @error('supplier.country')<span class="text-danger">{{ $message }}</span>
                        @enderror
                    </div>

                    <div class="form-group {{ $errors->has(`supplier.zip_code`) ? ' has-danger' : '' }}">
                        <label for="name">@lang('backend.crud.supplier.form.zip_code')</label>
                        <input wire:model="supplier.zip_code" type="text" id="zip_code_input"
                               class="form-control {{ $errors->has('supplier.zip_code') ? ' is-invalid' : '' }}"
                               placeholder="@lang('backend.crud.supplier.form.zip_code_placeholder')" />
                        @error('supplier.zip_code')
                        <span class="text-danger">{{ $message }}</span>
                        @enderror
                    </div>

                    <div class="form-group {{ $errors->has(`supplier.city`) ? ' has-danger' : '' }}">
                        <label for="name">@lang('backend.crud.supplier.form.city')</label>
                        <input wire:model="supplier.city" type="text" id="city_input"
                               class="form-control {{ $errors->has('supplier.city') ? ' is-invalid' : '' }}"
                               placeholder="@lang('backend.crud.supplier.form.city_placeholder')" />
                        @error('supplier.city')
                        <span class="text-danger">{{ $message }}</span>
                        @enderror
                    </div>

                    <div class="form-group {{ $errors->has(`supplier.address`) ? ' has-danger' : '' }}">
                        <label for="name">@lang('backend.crud.supplier.form.address')</label>
                        <input wire:model="supplier.address" type="text" id="address_input"
                               class="form-control {{ $errors->has('supplier.address') ? ' is-invalid' : '' }}"
                               placeholder="@lang('backend.crud.supplier.form.address_placeholder')" />
                        @error('supplier.address')
                        <span class="text-danger">{{ $message }}</span>
                        @enderror
                    </div>

                    <div class="form-group">
                        <div class="kt-checkbox-list">
                            <span class="kt-switch kt-switch--success kt-switch--sm">
                                <label>
                                    <input wire:click="$toggle('showRgpd')" name="toggle_rgpd" type="checkbox"
                                           class="form-control" @if ($showRgpd) checked @endif>
                                    <p style="display:inline-flex; padding-top: 5px">Protección de Datos</p>
                                    <span></span>
                                </label>
                            </span>
                            <p>
                                Marcad esta casilla y cumplimentar si el proveedor va a tener acceso a documentación,
                                programas o alguna base de datos de Rubió que contenga información personal, más allá de la persona de contacto.
                            </p>
                        </div>
                    </div>

                    @if ($showRgpd)
                        <hr />
                        <h5 class="modal-title">Protección de Datos</h5>

                        <div class="form-group {{ $errors->has('supplier.rgpd_name') ? ' has-danger' : '' }}">
                            <label for="rgpd_name">Nombre</label>
                            <input wire:model="supplier.rgpd_name" type="text" name="rgpd_name"
                                   class="form-control {{ $errors->has('supplier.rgpd_name') ? ' is-invalid' : '' }}"
                                   placeholder="Nombre" />
                            @error('supplier.rgpd_name')
                            <span class="text-danger">{{ $message }}</span>
                            @enderror
                        </div>
                        <div class="form-group {{ $errors->has('supplier.rgpd_last_name') ? ' has-danger' : '' }}">
                            <label for="rgpd_last_name">Apellidos</label>
                            <input wire:model="supplier.rgpd_last_name" type="text" name="rgpd_last_name"
                                   class="form-control {{ $errors->has('supplier.rgpd_last_name') ? ' is-invalid' : '' }}"
                                   placeholder="Apellidos" />
                            @error('supplier.rgpd_last_name')
                            <span class="text-danger">{{ $message }}</span>
                            @enderror
                        </div>
                        <div class="form-group {{ $errors->has('supplier.rgpd_dni') ? ' has-danger' : '' }}">
                            <label for="rgpd_dni">DNI</label>
                            <input wire:model="supplier.rgpd_dni" type="text" name="rgpd_dni"
                                   class="form-control {{ $errors->has('supplier.rgpd_dni') ? ' is-invalid' : '' }}"
                                   placeholder="DNI" />
                            @error('supplier.rgpd_dni')
                            <span class="text-danger">{{ $message }}</span>
                            @enderror
                        </div>
                        <div class="form-group {{ $errors->has('supplier.rgpd_email') ? ' has-danger' : '' }}">
                            <label for="rgpd_email">Email</label>
                            <input wire:model="supplier.rgpd_email" type="text" name="rgpd_email"
                                   class="form-control {{ $errors->has('supplier.rgpd_email') ? ' is-invalid' : '' }}"
                                   placeholder="Email" />
                            @error('supplier.rgpd_email')
                            <span class="text-danger">{{ $message }}</span>
                            @enderror
                        </div>
                        <div class="form-group {{ $errors->has('supplier.rgpd_role') ? ' has-danger' : '' }}">
                            <label for="rgpd_role">Cargo</label>
                            <input wire:model="supplier.rgpd_role" type="text" name="rgpd_role"
                                   class="form-control {{ $errors->has('supplier.rgpd_role') ? ' is-invalid' : '' }}"
                                   placeholder="Cargo / Posición" />
                            @error('supplier.rgpd_role')
                            <span class="text-danger">{{ $message }}</span>
                            @enderror
                        </div>
                        <div class="form-group {{ $errors->has('supplier.rgpd_service_description') ? ' has-danger' : '' }}">
                            <label for="rgpd_service_description">Servicio a prestar</label>
                            <textarea wire:model="supplier.rgpd_service_description"
                                      class="form-control {{ $errors->has('supplier.rgpd_service_description') ? ' is-invalid' : '' }}"
                                      placeholder="Descripción servicio a prestar">
                            </textarea>
                            @error('supplier.rgpd_service_description')
                            <span class="text-danger">{{ $message }}</span>
                            @enderror
                        </div>
                    @endif
                    <div class="row">
                        <div class="col-12">
                            <div class="text-right">
                                @if (!isset($this->supplier->id) || $this->supplier->id == null)
                                    <button wire:click.prevent="save()"
                                        class="btn btn-brand btn-elevate btn-icon-sm">@lang('backend.forms.save')</button>
                                @else
                                    <button wire:click.prevent="save()"
                                        class="btn btn-brand btn-elevate btn-icon-sm btn-dark">@lang('backend.forms.update')</button>
                                @endif
                                <button wire:click.prevent="resetInputs()" type="button"
                                    class="btn btn-secondary close-btn"
                                    data-dismiss="modal">@lang('backend.forms.close')</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
