<div class="kt-portlet kt-portlet--mobile">
    <div class="kt-portlet__head kt-portlet__head--lg">
        <div class="kt-portlet__head-label">
            <span class="kt-portlet__head-icon">
                <i class="kt-font-brand flaticon2-line-chart"></i>
            </span>
            <h3 class="kt-portlet__head-title text-uppercase">
                @lang('backend.crud.cost-center.title')
            </h3>
        </div>
        <div class="kt-portlet__head-toolbar">
            <div class="kt-portlet__head-wrapper">
                <div class="kt-portlet__head-actions">
                    <button wire:click="addNew()" class="btn btn-brand btn-elevate btn-icon-sm" data-toggle="tooltip"
                        data-placement="top" title="Create">
                        <i class="fas fa-plus"></i>
                        @lang('backend.crud.cost-center.list.add')
                    </button>
                </div>
            </div>
        </div>
    </div>
    <div class="kt-portlet__body">
        @if (session()->has('status'))
            <div class="alert alert-success alert-dismissible fade show" role="alert">
                {{ session('status') }}
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
        @endif
        @if (session()->has('error'))
            <div class="alert alert-danger alert-dismissible fade show" role="alert">
                {{ session('error') }}
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
        @endif
        <!--begin: Datatable -->
        <div id="users_list_table_wrapper" class="dataTables_wrapper dt-bootstrap4 no-footer">
            <div class="dt-buttons btn-group">
                <div class="btn-group">
                    <select wire:model="entries" class="custom-select" tabindex="0" aria-controls="users_list_table"
                        type="button" aria-haspopup="true" aria-expanded="false">
                        <option value="10">@lang('backend.forms.selects.10')</option>
                        <option value="25">@lang('backend.forms.selects.25')</option>
                        <option value="50">@lang('backend.forms.selects.50')</option>
                        <option value="100">@lang('backend.forms.selects.100')</option>
                    </select>
                </div>
            </div>
            <div id="users_list_table_filter" class="dataTables_filter">
                <label>@lang('backend.forms.search')
                    <input wire:model="search" type="search" class="form-control form-control-sm" placeholder=""
                        aria-controls="users_list_table">
                </label>
            </div>
            <div class="table-responsive">
                <table class="table table-striped- table-bordered table-hover table-checkable" id="users_list_table">
                    <thead>
                        <tr class="text-uppercase">
                            <th>Id de CeCo</th>
                            <th>@lang('backend.crud.created_at')</th>
                            <th>@lang('backend.crud.cost-center.list.name')</th>
                            <th>@lang('backend.crud.cost-center.list.company')</th>
                            <th>@lang('backend.crud.actions')</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($costCenters as $costCenter)
                            <tr>
                                <td>{{ $costCenter->external_id ?? '' }}</td>
                                <td>{{ $costCenter->created_at != null
    ? auth()->user()->applyDateFormat($costCenter->created_at)
    : '' }}
                                </td>
                                <td>{{ $costCenter->name ?? '' }}</td>
                                <td>{{ $costCenter->company ?? '' }}</td>
                                <td nowrap>
                                    <button wire:click="edit('{{ $costCenter->id }}')" type="button"
                                        class="btn btn-sm btn-clean btn-icon btn-icon-md" data-toggle="tooltip"
                                        data-placement="top" title="Edit">
                                        <i class="fas fa-edit"></i>
                                    </button>
                                    <button wire:click="confirmDelete('{{ $costCenter->id }}')" type="button"
                                        class="btn btn-sm btn-clean btn-icon btn-icon-md" data-toggle="modal"
                                        data-target="#delete_modal" data-toggle="tooltip" data-placement="top"
                                        title="Delete">
                                        <i class="fas fa-trash"></i>
                                    </button>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
            <div class="dataTables_info">
                {{ $costCenters->links() }}
            </div>
        </div>

        <!--end: Datatable -->
        @push('css')
            <!--begin::Page Vendors Styles(used by this page) -->
            <link href="{{ url('/') }}/assets/vendors/custom/datatables/datatables.bundle.css" rel="stylesheet"
                type="text/css" />
            <!--end::Page Vendors Styles -->
        @endpush
        @push('js')
            <!--begin::Page Vendors(used by this page) -->
            <script
                src="{{ config('custom.iworking_public_bucket') }}/assets/js/demo1/pages/crud/datatables/basic/headers.min.js"
                type="text/javascript">
            </script>
            <script
                src="{{ config('custom.iworking_public_bucket') }}/assets/vendors/custom/datatables/datatables.bundle.js"
                type="text/javascript">
            </script>
        @endpush

        @include('livewire.cruds.cost-centers.partials.form')
        @include('partials.deleteModal')
    </div>
</div>

@push('js')
    <script type="text/javascript">
        window.addEventListener('showModal', () => {
            $('#formModal').modal('show');
        });
        window.addEventListener('costCenterStore', () => {
            $('#formModal').modal('hide');
        });
        window.addEventListener('deleteItem', () => {
            $('#delete_modal').modal('hide');
        });

    </script>
@endpush
