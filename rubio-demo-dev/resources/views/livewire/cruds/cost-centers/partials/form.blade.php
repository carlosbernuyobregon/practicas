<div wire:ignore.self id="formModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="formModalLabel"
    aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                @if (!isset($this->costCenter->id) || $this->costCenter->id == null)
                    <h5 class="modal-title" id="formModalLabel">@lang('backend.crud.cost-center.form.add')</h5>
                @else
                    <h5 class="modal-title" id="formModalLabel">@lang('backend.crud.cost-center.form.update'): {{ $this->costCenter->external_id ?? '' }}</h5>
                @endif
                <button wire:click.prevent="resetInputs()" type="button" class="close" data-dismiss="modal"
                    aria-label="Close">
                    <span aria-hidden="true close-btn">×</span>
                </button>
            </div>
            <div class="modal-body">
                @if (session()->has('error'))
                    <div class="alert alert-danger alert-dismissible fade show" role="alert">
                        {{ session('error') }}
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                @endif
                <form>

                    @if (!isset($this->costCenter->id) || $this->costCenter->id == null)
                        <!--external id-->
                        <div class="form-group {{ $errors->has(`costCenter.external_id`) ? ' has-danger' : '' }}">
                            <label for="name">@lang('backend.crud.cost-center.form.external_id')</label>
                            <input wire:model="costCenter.external_id" type="text" id="external_id_input"
                                class="form-control {{ $errors->has('costCenter.external_id') ? ' is-invalid' : '' }}"
                                placeholder="@lang('backend.crud.cost-center.form.external_id_placeholder')" />
                            @error('costCenter.external_id')
                                <span class="text-danger">{{ $message }}</span>
                            @enderror
                        </div>
                    @endif

                    <div class="form-group {{ $errors->has(`costCenter.name`) ? ' has-danger' : '' }}">
                        <label for="name">@lang('backend.crud.cost-center.form.name')</label>
                        <input wire:model="costCenter.name" type="text" id="name_input"
                            class="form-control {{ $errors->has('costCenter.name') ? ' is-invalid' : '' }}"
                            placeholder="@lang('backend.crud.cost-center.form.name_placeholder')" />
                        @error('costCenter.name')
                            <span class="text-danger">{{ $message }}</span>
                        @enderror
                    </div>

                    <div class="form-group {{ $errors->has(`costCenter.company`) ? ' has-danger' : '' }}">
                        <label for="name">@lang('backend.crud.cost-center.form.company')</label>
                        <input wire:model="costCenter.company" type="text" id="company_input"
                            class="form-control {{ $errors->has('costCenter.company') ? ' is-invalid' : '' }}"
                            placeholder="@lang('backend.crud.cost-center.form.company')" />
                        @error('costCenter.company')
                            <span class="text-danger">{{ $message }}</span>
                        @enderror
                    </div>

                    <div class="form-group {{ $errors->has('costCenter.customer_id') ? ' has-danger' : '' }}">
                        <label for="customer_id">@lang('backend.crud.cost-center.form.customer')</label>
                        <select class="form-control {{ $errors->has('costCenter.customer_id') ? ' is-invalid' : '' }}"
                            id="customer_id_input" wire:model="costCenter.customer_id">
                            <option value="">@lang('backend.forms.select-option')</option>
                            @foreach ($customers as $customer)
                                <option value="{{ $customer->id }}">{{ $customer->name }}</option>
                            @endforeach
                        </select>
                        @error('costCenter.customer_id')<span class="text-danger">{{ $message }}</span>
                        @enderror
                    </div>
                    <hr>
                    <div class="form-group {{ $errors->has('costCenter.manager_1_id') ? ' has-danger' : '' }}">
                        <label for="manager_1_id">Aprobador nivel 1</label>
                        <select class="form-control {{ $errors->has('costCenter.manager_1_id') ? ' is-invalid' : '' }}"
                            id="manager_1_id_input" wire:model="costCenter.manager_1_id">
                            <option value="">@lang('backend.forms.select-option')</option>
                            @foreach ($users as $user)
                                <option value="{{ $user->id }}">{{ $user->full_name }}</option>
                            @endforeach
                        </select>
                        @error('costCenter.manager_1_id')<span class="text-danger">{{ $message }}</span>
                        @enderror
                    </div>
                    <div class="form-group {{ $errors->has(`costCenter.amount_level_1`) ? ' has-danger' : '' }}">
                        <label for="name">Importe aprobación nivel 1</label>
                        <input wire:model="costCenter.amount_level_1" type="text" id="amount_level_1_input"
                            class="form-control {{ $errors->has('costCenter.amount_level_1') ? ' is-invalid' : '' }}"
                            placeholder="Importe aprobación nivel 1" />
                        @error('costCenter.amount_level_1')
                            <span class="text-danger">{{ $message }}</span>
                        @enderror
                    </div>
                    <div class="form-group {{ $errors->has(`costCenter.amount_level_secondary_1`) ? ' has-danger' : '' }}">
                        <label for="name">Importe aprobación (sin presupuesto) nivel 1</label>
                        <input wire:model="costCenter.amount_level_secondary_1" type="text" id="amount_level_secondary_1_input"
                            class="form-control {{ $errors->has('costCenter.amount_level_secondary_1') ? ' is-invalid' : '' }}"
                            placeholder="Importe aprobación nivel 1" />
                        @error('costCenter.amount_level_secondary_1')
                            <span class="text-danger">{{ $message }}</span>
                        @enderror
                    </div>
                    <div class="form-group {{ $errors->has('costCenter.manager_2_id') ? ' has-danger' : '' }}">
                        <label for="manager_2_id">Aprobador nivel 2</label>
                        <select class="form-control {{ $errors->has('costCenter.manager_2_id') ? ' is-invalid' : '' }}"
                            id="manager_2_id_input" wire:model="costCenter.manager_2_id">
                            <option value="">@lang('backend.forms.select-option')</option>
                            @foreach ($users as $user)
                                <option value="{{ $user->id }}">{{ $user->full_name }}</option>
                            @endforeach
                        </select>
                        @error('costCenter.manager_2_id')<span class="text-danger">{{ $message }}</span>
                        @enderror
                    </div>
                    <div class="form-group {{ $errors->has(`costCenter.amount_level_2`) ? ' has-danger' : '' }}">
                        <label for="name">Importe aprobación nivel 2</label>
                        <input wire:model="costCenter.amount_level_2" type="text" id="amount_level_2_input"
                            class="form-control {{ $errors->has('costCenter.amount_level_2') ? ' is-invalid' : '' }}"
                            placeholder="Importe aprobación nivel 2" />
                        @error('costCenter.amount_level_2')
                            <span class="text-danger">{{ $message }}</span>
                        @enderror
                    </div>
                    <div class="form-group {{ $errors->has(`costCenter.amount_level_secondary_2`) ? ' has-danger' : '' }}">
                        <label for="name">Importe aprobación (sin presupuesto) nivel 2</label>
                        <input wire:model="costCenter.amount_level_secondary_2" type="text" id="amount_level_secondary_2_input"
                            class="form-control {{ $errors->has('costCenter.amount_level_secondary_2') ? ' is-invalid' : '' }}"
                            placeholder="Importe aprobación nivel 2" />
                        @error('costCenter.amount_level_secondary_2')
                            <span class="text-danger">{{ $message }}</span>
                        @enderror
                    </div>
                    <div class="form-group {{ $errors->has('costCenter.manager_3_id') ? ' has-danger' : '' }}">
                        <label for="manager_3_id">Aprobador nivel 3</label>
                        <select class="form-control {{ $errors->has('costCenter.manager_3_id') ? ' is-invalid' : '' }}"
                            id="manager_3_id_input" wire:model="costCenter.manager_3_id">
                            <option value="">@lang('backend.forms.select-option')</option>
                            @foreach ($users as $user)
                                <option value="{{ $user->id }}">{{ $user->full_name }}</option>
                            @endforeach
                        </select>
                        @error('costCenter.manager_3_id')<span class="text-danger">{{ $message }}</span>
                        @enderror
                    </div>
                    <div class="form-group {{ $errors->has(`costCenter.amount_level_3`) ? ' has-danger' : '' }}">
                        <label for="name">Importe aprobación nivel 3</label>
                        <input wire:model="costCenter.amount_level_3" type="text" id="amount_level_3_input"
                            class="form-control {{ $errors->has('costCenter.amount_level_3') ? ' is-invalid' : '' }}"
                            placeholder="Importe aprobación nivel 3" />
                        @error('costCenter.amount_level_3')
                            <span class="text-danger">{{ $message }}</span>
                        @enderror
                    </div>
                    <div class="form-group {{ $errors->has(`costCenter.amount_level_secondary_3`) ? ' has-danger' : '' }}">
                        <label for="name">Importe aprobación (sin presupuesto) nivel 3</label>
                        <input wire:model="costCenter.amount_level_secondary_3" type="text" id="amount_level_secondary_3_input"
                            class="form-control {{ $errors->has('costCenter.amount_level_secondary_3') ? ' is-invalid' : '' }}"
                            placeholder="Importe aprobación nivel 3" />
                        @error('costCenter.amount_level_secondary_3')
                            <span class="text-danger">{{ $message }}</span>
                        @enderror
                    </div>
                    <div class="row">
                        <div class="col-12">
                            <div class="text-right">
                                @if (!isset($this->costCenter->id) || $this->costCenter->id == null)
                                    <button wire:click.prevent="save()"
                                        class="btn btn-brand btn-elevate btn-icon-sm">@lang('backend.forms.save')</button>
                                @else
                                    <button wire:click.prevent="save()"
                                        class="btn btn-brand btn-elevate btn-icon-sm btn-dark">@lang('backend.forms.update')</button>
                                @endif
                                <button wire:click.prevent="resetInputs()" type="button"
                                    class="btn btn-secondary close-btn"
                                    data-dismiss="modal">@lang('backend.forms.close')</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
