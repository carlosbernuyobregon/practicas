<div wire:ignore.self id="formModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="formModalLabel"
    aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                @if (!isset($this->asset->id) || $this->asset->id == null)
                    <h5 class="modal-title" id="formModalLabel">@lang('backend.crud.asset.form.add')</h5>
                @else
                    <h5 class="modal-title" id="formModalLabel">@lang('backend.crud.asset.form.update')</h5>
                @endif
                <button wire:click.prevent="resetInputs()" type="button" class="close" data-dismiss="modal"
                    aria-label="Close">
                    <span aria-hidden="true close-btn">×</span>
                </button>
            </div>
            <div class="modal-body">
                @if (session()->has('error'))
                    <div class="alert alert-danger alert-dismissible fade show" role="alert">
                        {{ session('error') }}
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                @endif
                <form>
                    @if (!isset($this->asset->id) || $this->asset->id == null)
                        <!--external id-->
                        <div class="form-group {{ $errors->has(`asset.external_id`) ? ' has-danger' : '' }}">
                            <label for="name">@lang('backend.crud.asset.form.external_id')</label>
                            <input wire:model="asset.external_id" type="text" id="external_id_input"
                                class="form-control {{ $errors->has('asset.external_id') ? ' is-invalid' : '' }}"
                                placeholder="@lang('backend.crud.asset.form.external_id_placeholder')" />
                            @error('asset.external_id')
                                <span class="text-danger">{{ $message }}</span>
                            @enderror
                        </div>
                    @endif

                    <div class="form-group {{ $errors->has(`asset.subnumber`) ? ' has-danger' : '' }}">
                        <label for="name">@lang('backend.crud.asset.form.subnumber')</label>
                        <input wire:model="asset.subnumber" type="text" id="subnumber_input"
                            class="form-control {{ $errors->has('asset.subnumber') ? ' is-invalid' : '' }}"
                            placeholder="@lang('backend.crud.asset.form.subnumber_placeholder')" />
                        @error('asset.subnumber')
                            <span class="text-danger">{{ $message }}</span>
                        @enderror
                    </div>
                    <div class="form-group {{ $errors->has(`asset.name`) ? ' has-danger' : '' }}">
                        <label for="name">@lang('backend.crud.asset.form.name')</label>
                        <input wire:model="asset.name" type="text" id="name_input"
                            class="form-control {{ $errors->has('asset.name') ? ' is-invalid' : '' }}"
                            placeholder="@lang('backend.crud.asset.form.name_placeholder')" />
                        @error('asset.name')
                            <span class="text-danger">{{ $message }}</span>
                        @enderror
                    </div>

                    <div class="form-group {{ $errors->has(`asset.company`) ? ' has-danger' : '' }}">
                        <label for="name">@lang('backend.crud.asset.form.company')</label>
                        <input wire:model="asset.company" type="text" id="company_input"
                            class="form-control {{ $errors->has('asset.company') ? ' is-invalid' : '' }}"
                            placeholder="@lang('backend.crud.asset.form.company')" />
                        @error('asset.company')
                            <span class="text-danger">{{ $message }}</span>
                        @enderror
                    </div>

                    <div class="form-group {{ $errors->has(`asset.status`) ? ' has-danger' : '' }}">
                        <label for="name">@lang('backend.crud.asset.form.status')</label>
                        <input wire:model="asset.status" type="text" id="status_input"
                            class="form-control {{ $errors->has('asset.status') ? ' is-invalid' : '' }}"
                            placeholder="@lang('backend.crud.asset.form.status')" />
                        @error('asset.status')
                            <span class="text-danger">{{ $message }}</span>
                        @enderror
                    </div>

                    <div class="form-group {{ $errors->has('asset.customer_id') ? ' has-danger' : '' }}">
                        <label for="customer_id">@lang('backend.crud.asset.form.customer')</label>
                        <select class="form-control {{ $errors->has('asset.customer_id') ? ' is-invalid' : '' }}"
                            id="customer_id_input" wire:model="asset.customer_id">
                            <option value="">@lang('backend.forms.select-option')</option>
                            @foreach ($customers as $customer)
                                <option value="{{ $customer->id }}">{{ $customer->name }}</option>
                            @endforeach
                        </select>
                        @error('asset.customer_id')<span class="text-danger">{{ $message }}</span>
                        @enderror
                    </div>

                    <div class="row">
                        <div class="col-12">
                            <div class="text-right">
                                @if (!isset($this->asset->id) || $this->asset->id == null)
                                    <button wire:click.prevent="save()"
                                        class="btn btn-brand btn-elevate btn-icon-sm">@lang('backend.forms.save')</button>
                                @else
                                    <button wire:click.prevent="save()"
                                        class="btn btn-brand btn-elevate btn-icon-sm btn-dark">@lang('backend.forms.update')</button>
                                @endif
                                <button wire:click.prevent="resetInputs()" type="button"
                                    class="btn btn-secondary close-btn"
                                    data-dismiss="modal">@lang('backend.forms.close')</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>