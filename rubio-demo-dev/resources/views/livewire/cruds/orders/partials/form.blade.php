<div wire:ignore.self id="formModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="formModalLabel"
    aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                @if (!isset($this->order->id) || $this->order->id == null)
                    <h5 class="modal-title" id="formModalLabel">@lang('backend.crud.order.form.add')</h5>
                @else
                    <h5 class="modal-title" id="formModalLabel">@lang('backend.crud.order.form.update')</h5>
                @endif
                <button wire:click.prevent="resetInputs()" type="button" class="close" data-dismiss="modal"
                    aria-label="Close">
                    <span aria-hidden="true close-btn">×</span>
                </button>
            </div>
            <div class="modal-body">
                @if (session()->has('error'))
                    <div class="alert alert-danger alert-dismissible fade show" role="alert">
                        {{ session('error') }}
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                @endif
                <form>
                    <div class="row">
                        <div class="col-md-6">
                            @if (!isset($this->order->id) || $this->order->id == null)
                                <!--external id-->
                                <div class="form-group {{ $errors->has(`order.external_id`) ? ' has-danger' : '' }}">
                                    <label for="name">@lang('backend.crud.order.form.external_id')</label>
                                    <input wire:model="order.external_id" type="text" id="external_id_input"
                                        class="form-control {{ $errors->has('order.external_id') ? ' is-invalid' : '' }}"
                                        placeholder="@lang('backend.crud.order.form.external_id_placeholder')" />
                                    @error('order.external_id')
                                        <span class="text-danger">{{ $message }}</span>
                                    @enderror
                                </div>
                            @endif

                            <!--position-->
                            <div class="form-group {{ $errors->has('order.position') ? ' has-danger' : '' }}">
                                <label for="order">@lang('backend.crud.order.form.position')</label>
                                <input wire:model="order.position" type="number" min="0" id="position_input"
                                    class="form-control {{ $errors->has('order.position') ? ' is-invalid' : '' }}"
                                    placeholder="@lang('backend.crud.order.form.position_placeholder')" />
                                @error('order.position')
                                    <span class="text-danger">{{ $message }}</span>
                                @enderror
                            </div>

                            <!--Material-->
                            <div class="form-group {{ $errors->has('order.material_id') ? ' has-danger' : '' }}">
                                <label for="material_id">@lang('backend.crud.order.form.material')</label>
                                <select
                                    class="form-control {{ $errors->has('order.material_id') ? ' is-invalid' : '' }}"
                                    id="material_id_input" wire:model="order.material_id">
                                    <option value="">@lang('backend.forms.select-option')</option>
                                    @foreach ($materials as $material)
                                        <option value="{{ $material->id }}">{{ $material->material_name }}</option>
                                    @endforeach
                                </select>
                                @error('order.material_id')<span class="text-danger">{{ $message }}</span>
                                @enderror
                            </div>

                            <!--position material-->
                            <div
                                class="form-group {{ $errors->has('order.position_material') ? ' has-danger' : '' }}">
                                <label for="order">@lang('backend.crud.order.form.position_material')</label>
                                <input wire:model="order.position_material" type="number" min="0"
                                    id="position_material_input"
                                    class="form-control {{ $errors->has('order.position_material') ? ' is-invalid' : '' }}"
                                    placeholder="@lang('backend.crud.order.form.position_material_placeholder')" />
                                @error('order.position_material')
                                    <span class="text-danger">{{ $message }}</span>
                                @enderror
                            </div>

                            <!--Supplier id-->
                            <div class="form-group {{ $errors->has('order.provider_id') ? ' has-danger' : '' }}">
                                <label for="provider_id">@lang('backend.crud.order.form.supplier')</label>
                                <select
                                    class="form-control {{ $errors->has('order.provider_id') ? ' is-invalid' : '' }}"
                                    id="provider_id_input" wire:model="order.provider_id">
                                    <option value="">@lang('backend.forms.select-option')</option>
                                    @foreach ($suppliers as $supplier)
                                        <option value="{{ $supplier->id }}">{{ $supplier->name }}</option>
                                    @endforeach
                                </select>
                                @error('order.provider_id')<span class="text-danger">{{ $message }}</span>
                                @enderror
                            </div>

                            <!--Supplier code-->
                            <div class="form-group {{ $errors->has(`order.supplier_code`) ? ' has-danger' : '' }}">
                                <label for="name">@lang('backend.crud.order.form.supplier_code')</label>
                                <input wire:model="order.supplier_code" type="text" id="supplier_code_input"
                                    class="form-control {{ $errors->has('order.supplier_code') ? ' is-invalid' : '' }}"
                                    placeholder="@lang('backend.crud.order.form.supplier_code_placeholder')" />
                                @error('order.supplier_code')
                                    <span class="text-danger">{{ $message }}</span>
                                @enderror
                            </div>

                            <div class="form-group {{ $errors->has(`order.company`) ? ' has-danger' : '' }}">
                                <label for="name">@lang('backend.crud.order.form.company')</label>
                                <input wire:model="order.company" type="text" id="company_input"
                                    class="form-control {{ $errors->has('order.company') ? ' is-invalid' : '' }}"
                                    placeholder="@lang('backend.crud.order.form.company')" />
                                @error('order.company')
                                    <span class="text-danger">{{ $message }}</span>
                                @enderror
                            </div>

                            <div class="form-group {{ $errors->has(`order.qty`) ? ' has-danger' : '' }}">
                                <label for="qty">@lang('backend.crud.order.form.qty')</label>
                                <input wire:model="order.qty" type="number" id="qty_input" min="0"
                                    class="form-control {{ $errors->has('order.qty') ? ' is-invalid' : '' }}"
                                    placeholder="@lang('backend.crud.order.form.qty_placeholder')" />
                                @error('order.qty')
                                    <span class="text-danger">{{ $message }}</span>
                                @enderror
                            </div>

                            <div class="form-group {{ $errors->has(`order.unit_price`) ? ' has-danger' : '' }}">
                                <label for="unit_price">@lang('backend.crud.order.form.unit_price')</label>
                                <input wire:model="order.unit_price" type="number" id="unit_price_input" min="0"
                                    step="any"
                                    class="form-control {{ $errors->has('order.unit_price') ? ' is-invalid' : '' }}"
                                    placeholder="@lang('backend.crud.order.form.unit_price_placeholder')" />
                                @error('order.unit_price')
                                    <span class="text-danger">{{ $message }}</span>
                                @enderror
                            </div>

                            <div
                                class="form-group {{ $errors->has(`order.payment_conditions`) ? ' has-danger' : '' }}">
                                <label for="name">@lang('backend.crud.order.form.payment_conditions')</label>
                                <input wire:model="order.payment_conditions" type="text" id="payment_conditions_input"
                                    class="form-control {{ $errors->has('order.payment_conditions') ? ' is-invalid' : '' }}"
                                    placeholder="@lang('backend.crud.order.form.payment_conditions')" />
                                @error('order.payment_conditions')
                                    <span class="text-danger">{{ $message }}</span>
                                @enderror
                            </div>

                            <div class="form-group {{ $errors->has(`order.payment_type`) ? ' has-danger' : '' }}">
                                <label for="name">@lang('backend.crud.order.form.payment_type')</label>
                                <input wire:model="order.payment_type" type="text" id="payment_type_input"
                                    class="form-control {{ $errors->has('order.payment_type') ? ' is-invalid' : '' }}"
                                    placeholder="@lang('backend.crud.order.form.payment_type')" />
                                @error('order.payment_type')
                                    <span class="text-danger">{{ $message }}</span>
                                @enderror
                            </div>


                        </div>
                        <div class="col-md-6">
                            <!--order number-->
                            <div class="form-group {{ $errors->has(`order.order_number`) ? ' has-danger' : '' }}">
                                <label for="name">@lang('backend.crud.order.form.order_number')</label>
                                <input wire:model="order.order_number" type="text" id="order_number_input"
                                    class="form-control {{ $errors->has('order.order_number') ? ' is-invalid' : '' }}"
                                    placeholder="@lang('backend.crud.order.form.order_number_placeholder')" />
                                @error('order.order_number')
                                    <span class="text-danger">{{ $message }}</span>
                                @enderror
                            </div>

                            <!--date-->
                            <div class="form-group {{ $errors->has(`order.date`) ? ' has-danger' : '' }}">
                                <label for="name">@lang('backend.crud.order.form.date')</label>
                                <input wire:model="order.date" type="date" id="date_input"
                                    class="form-control {{ $errors->has('order.date') ? ' is-invalid' : '' }}" />
                                @error('order.date')
                                    <span class="text-danger">{{ $message }}</span>
                                @enderror
                            </div>

                            <!--buyer-->
                            <div class="form-group {{ $errors->has(`order.buyer`) ? ' has-danger' : '' }}">
                                <label for="name">@lang('backend.crud.order.form.buyer')</label>
                                <input wire:model="order.buyer" type="text" id="buyer_input"
                                    class="form-control {{ $errors->has('order.buyer') ? ' is-invalid' : '' }}"
                                    placeholder="@lang('backend.crud.order.form.buyer_placeholder')" />
                                @error('order.buyer')
                                    <span class="text-danger">{{ $message }}</span>
                                @enderror
                            </div>

                            <!--purchasing group-->
                            <div
                                class="form-group {{ $errors->has(`order.purchasing_group`) ? ' has-danger' : '' }}">
                                <label for="name">@lang('backend.crud.order.form.purchasing_group')</label>
                                <input wire:model="order.purchasing_group" type="text" id="purchasing_group_input"
                                    class="form-control {{ $errors->has('order.purchasing_group') ? ' is-invalid' : '' }}"
                                    placeholder="@lang('backend.crud.order.form.purchasing_group_placeholder')" />
                                @error('order.purchasing_group')
                                    <span class="text-danger">{{ $message }}</span>
                                @enderror
                            </div>

                            <!--Document type-->
                            <div class="form-group {{ $errors->has(`order.document_type`) ? ' has-danger' : '' }}">
                                <label for="name">@lang('backend.crud.order.form.document_type')</label>
                                <input wire:model="order.document_type" type="text" id="document_type_input"
                                    class="form-control {{ $errors->has('order.document_type') ? ' is-invalid' : '' }}"
                                    placeholder="@lang('backend.crud.order.form.document_type_placeholder')" />
                                @error('order.document_type')
                                    <span class="text-danger">{{ $message }}</span>
                                @enderror
                            </div>

                            <!--net amount-->
                            <div class="form-group {{ $errors->has(`order.net_amount`) ? ' has-danger' : '' }}">
                                <label for="name">@lang('backend.crud.order.form.net_amount')</label>
                                <input wire:model="order.net_amount" type="number" id="net_amount_input" min="0"
                                    step="any"
                                    class="form-control {{ $errors->has('order.net_amount') ? ' is-invalid' : '' }}"
                                    placeholder="@lang('backend.crud.order.form.net_amount_placeholder')" />
                                @error('order.net_amount')
                                    <span class="text-danger">{{ $message }}</span>
                                @enderror
                            </div>

                            <!--total amount-->
                            <div class="form-group {{ $errors->has(`order.total_amount`) ? ' has-danger' : '' }}">
                                <label for="name">@lang('backend.crud.order.form.total_amount')</label>
                                <input wire:model="order.total_amount" type="number" id="total_amount_input" min="0"
                                    step="any"
                                    class="form-control {{ $errors->has('order.total_amount') ? ' is-invalid' : '' }}"
                                    placeholder="@lang('backend.crud.order.form.total_amount_placeholder')" />
                                @error('order.total_amount')
                                    <span class="text-danger">{{ $message }}</span>
                                @enderror
                            </div>

                            <!--status-->
                            <div class="form-group {{ $errors->has(`order.status`) ? ' has-danger' : '' }}">
                                <label for="name">@lang('backend.crud.order.form.status')</label>
                                <input wire:model="order.status" type="number" id="status_input"
                                    class="form-control {{ $errors->has('order.status') ? ' is-invalid' : '' }}"
                                    placeholder="@lang('backend.crud.order.form.status_placeholder')" />
                                @error('order.status')
                                    <span class="text-danger">{{ $message }}</span>
                                @enderror
                            </div>

                            <!--active-->
                            <div class="form-group {{ $errors->has('order.active') ? ' has-danger' : '' }}">
                                <label for="type">@lang('backend.crud.order.form.active')</label>
                                <div class="row">
                                    <div class="col-12 mt-3">
                                        <!--order type-->
                                        <div
                                            class="form-check form-check-inline {{ $errors->has('order.active') ? ' is-invalid' : '' }}">
                                            <input wire:model="order.active" class="form-check-input" type="radio"
                                                name="inlineRadioOptions" id="inlineRadio1" value="1">
                                            <label class="form-check-label"
                                                for="inlineRadio1">@lang('backend.crud.order.form.active-options.yes')</label>
                                        </div>
                                        <div
                                            class="form-check form-check-inline {{ $errors->has('order.type') ? ' is-invalid' : '' }}">
                                            <input wire:model="order.active" class="form-check-input" type="radio"
                                                name="inlineRadioOptions" id="inlineRadio2" value="0">
                                            <label class="form-check-label"
                                                for="inlineRadio2">@lang('backend.crud.order.form.active-options.no')</label>
                                        </div>
                                    </div>
                                </div>
                                @error('order.active')
                                    <span class="text-danger">{{ $message }}</span>
                                @enderror
                            </div>

                            <div class="form-group {{ $errors->has('order.customer_id') ? ' has-danger' : '' }}">
                                <label for="customer_id">@lang('backend.crud.order.form.customer')</label>
                                <select
                                    class="form-control {{ $errors->has('order.customer_id') ? ' is-invalid' : '' }}"
                                    id="customer_id_input" wire:model="order.customer_id">
                                    <option value="">@lang('backend.forms.select-option')</option>
                                    @foreach ($customers as $customer)
                                        <option value="{{ $customer->id }}">{{ $customer->name }}</option>
                                    @endforeach
                                </select>
                                @error('order.customer_id')<span class="text-danger">{{ $message }}</span>
                                @enderror
                            </div>

                            <!--Description-->
                            <div class="form-group{{ $errors->has('order.description') ? ' has-danger' : '' }}">
                                <label class="form-control-label"
                                    for="input-description">@lang('backend.crud.order.form.description')</label>
                                <textarea wire:model="order.description" class="form-control" id="description_input"
                                    rows="3"
                                    placeholder="@lang('backend.crud.order.form.description_placeholder')"></textarea>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-12">
                            <div class="text-right">
                                @if (!isset($this->order->id) || $this->order->id == null)
                                    <button wire:click.prevent="save()"
                                        class="btn btn-brand btn-elevate btn-icon-sm">@lang('backend.forms.save')</button>
                                @else
                                    <button wire:click.prevent="save()"
                                        class="btn btn-brand btn-elevate btn-icon-sm btn-dark">@lang('backend.forms.update')</button>
                                @endif
                                <button wire:click.prevent="resetInputs()" type="button"
                                    class="btn btn-secondary close-btn"
                                    data-dismiss="modal">@lang('backend.forms.close')</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
