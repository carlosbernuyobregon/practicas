<div wire:ignore.self id="formModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="formModalLabel"
    aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                @if (!isset($this->country->id) || $this->country->id == null)
                    <h5 class="modal-title" id="formModalLabel">@lang('backend.crud.country.form.add')</h5>
                @else
                    <h5 class="modal-title" id="formModalLabel">@lang('backend.crud.country.form.update')</h5>
                @endif
                <button wire:click.prevent="resetInputs()" type="button" class="close" data-dismiss="modal"
                    aria-label="Close">
                    <span aria-hidden="true close-btn">×</span>
                </button>
            </div>
            <div class="modal-body">
                @if (session()->has('error') || $errors->has('country.key_value'))
                    <div class="alert alert-danger alert-dismissible fade show" role="alert">
                        {{ session()->has('error') ? session('error') : $errors->first('country.key_value') }}
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                @endif
                <nav>
                    <div class="nav nav-tabs" id="nav-tab" role="tablist">
                        @foreach ($locales as $locale)
                            <a wire:ignore class="nav-item nav-link {{ $locale === $locales[0] ? 'active' : '' }}"
                                id="nav-{{ $locale }}-tab" data-toggle="tab" href="#nav-{{ $locale }}" role="tab"
                                aria-controls="nav-{{ $locale }}">{{ $locale }}</a>
                        @endforeach
                    </div>
                </nav>
                <form>
                    <div class="tab-content" id="nav-tabContent">
                        @foreach ($locales as $locale)
                            <div wire:ignore.self
                                class="tab-pane fade {{ $locale === $locales[0] ? 'show active' : '' }}"
                                id="nav-{{ $locale }}" role="tabpanel" aria-labelledby="nav-{{ $locale }}-tab">
                                <div
                                    class="form-group {{ $errors->has('data.' . $locale . '.name') ? ' has-danger' : '' }}">
                                    <label
                                        for="name_{{ $locale }}">@lang('backend.crud.country.form.name')</label>
                                    <input wire:model="data.{{ $locale }}.name" type="text"
                                        id="name_input_{{ $locale }}"
                                        class="form-control {{ $errors->has('data.' . $locale . '.name') ? ' is-invalid' : '' }}"
                                        placeholder="@lang('backend.crud.country.form.name_placeholder')" />
                                    @if ($errors->has('data.' . $locale . '.name'))
                                        <span
                                            class="text-danger">{{ $errors->first('data.' . $locale . '.name') }}</span>
                                    @endif
                                </div>

                                <div
                                    class="form-group{{ $errors->has('data.' . $locale . '.description') ? ' has-danger' : '' }}">
                                    <label class="form-control-label"
                                        for="input-description">@lang('backend.crud.country.form.description')</label>
                                    <textarea wire:model="data.{{ $locale }}.description" class="form-control"
                                        id="description-{{ $locale }}" rows="3"
                                        placeholder="@lang('backend.crud.country.form.description_placeholder')"
                                        name="{{ $locale }}[description]"></textarea>
                                </div>

                                <div class="form-group {{ $errors->has('country.code') ? ' has-danger' : '' }}">
                                    <label for="code">@lang('backend.crud.country.form.code')</label>
                                    <input type="text" id="codeInput_{{ $locale }}"
                                        class="form-control {{ $errors->has('country.code') ? ' is-invalid' : '' }}"
                                        placeholder="@lang('backend.crud.country.form.code_placeholder')"
                                        wire:model="country.code" maxlength="2" />
                                    @error('country.code')<span class="text-danger">{{ $message }}</span>
                                    @enderror
                                </div>

                                <div class="form-group {{ $errors->has('country.currency_id') ? ' has-danger' : '' }}">
                                    <label for="currency_id">@lang('backend.crud.country.form.currency')</label>
                                    <select class="form-control {{ $errors->has('country.currency_id') ? ' is-invalid' : '' }}"
                                        id="currency_id_input_{{ $locale }}" wire:model="country.currency_id">
                                        <option value="">@lang('backend.forms.select-option')</option>
                                        @foreach ($currencies as $currency)
                                            <option value="{{ $currency->id }}">{{ $currency->name }}</option>
                                        @endforeach
                                    </select>
                                    @error('country.currency_id')<span class="text-danger">{{ $message }}</span>
                                    @enderror
                                </div>

                                <div class="form-group {{ $errors->has('country.customer_id') ? ' has-danger' : '' }}">
                                    <label for="customer_id">@lang('backend.crud.country.form.customer')</label>
                                    <select class="form-control {{ $errors->has('country.customer_id') ? ' is-invalid' : '' }}"
                                        id="customer_id_input_{{ $locale }}" wire:model="country.customer_id">
                                        <option value="">@lang('backend.forms.select-option')</option>
                                        @foreach ($customers as $customer)
                                            <option value="{{ $customer->id }}">{{ $customer->name }}</option>
                                        @endforeach
                                    </select>
                                    @error('country.customer_id')<span class="text-danger">{{ $message }}</span>
                                    @enderror
                                </div>

                                <div class="row">
                                    <div class="col-12">
                                        <div class="text-right">
                                            @if (!isset($this->country->id) || $this->country->id == null)
                                                <button wire:click.prevent="save()"
                                                    class="btn btn-brand btn-elevate btn-icon-sm">@lang('backend.forms.save')</button>
                                            @else
                                                <button wire:click.prevent="save()"
                                                    class="btn btn-brand btn-elevate btn-icon-sm btn-dark">@lang('backend.forms.update')</button>
                                            @endif
                                            <button wire:click.prevent="resetInputs()" type="button"
                                                class="btn btn-secondary close-btn"
                                                data-dismiss="modal">@lang('backend.forms.close')</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>