<div wire:ignore.self id="formModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="formModalLabel"
    aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                @if (!isset($this->providerPayment->id) || $this->providerPayment->id == null)
                <h5 class="modal-title" id="formModalLabel">@lang('backend.crud.provider-payment-condition.form.add')
                </h5>
                @else
                <h5 class="modal-title" id="formModalLabel">@lang('backend.crud.provider-payment-condition.form.update')
                </h5>
                @endif
                <button wire:click.prevent="resetInputs()" type="button" class="close" data-dismiss="modal"
                    aria-label="Close">
                    <span aria-hidden="true close-btn">×</span>
                </button>
            </div>
            <div class="modal-body">
                @if (session()->has('error') || $errors->has('providerPayment.key_value'))
                <div class="alert alert-danger alert-dismissible fade show" role="alert">
                    {{ session()->has('error') ? session('error') : $errors->first('providerPayment.key_value') }}
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                @endif
                <form>
                    <div class="form-group {{ $errors->has(`providerPayment.code`) ? ' has-danger' : '' }}">
                        <label for="name">@lang('backend.crud.provider-payment-condition.form.code')</label>
                        <input wire:model="providerPayment.code" maxlength="2" type="text" id="type_input"
                            class="form-control {{ $errors->has('providerPayment.code') ? ' is-invalid' : '' }}"
                            placeholder="@lang('backend.crud.provider-payment-condition.form.code_placeholder')" />
                        @error('providerPayment.code')
                        <span class="text-danger">{{ $message }}</span>
                        @enderror
                    </div>

                    <div class="form-group {{ $errors->has(`providerPayment.name`) ? ' has-danger' : '' }}">
                        <label for="name">@lang('backend.crud.provider-payment-condition.form.name')</label>
                        <input wire:model="providerPayment.name" type="text" id="type_input"
                            class="form-control {{ $errors->has('providerPayment.name') ? ' is-invalid' : '' }}"
                            placeholder="@lang('backend.crud.provider-payment-condition.form.name_placeholder')" />
                        @error('providerPayment.name')
                        <span class="text-danger">{{ $message }}</span>
                        @enderror
                    </div>

                    <div class="form-group {{ $errors->has('providerPayment.description') ? ' has-danger' : '' }}">
                        <label
                            for="description">@lang('backend.crud.provider-payment-condition.form.description')</label>
                        <textarea wire:model="providerPayment.description" type="text" id="description_input"
                            class="form-control {{ $errors->has('providerPayment.description') ? ' is-invalid' : '' }}"
                            placeholder="@lang('backend.crud.provider-payment-condition.form.description_placeholder')"></textarea>
                        @error('providerPayment.description')<span class="text-danger">{{ $message }}</span>
                        @enderror
                    </div>

                    <div class="form-group {{ $errors->has('providerPayment.is_default') ? ' has-danger' : '' }}">
                        <label for="type">@lang('backend.crud.provider-payment-condition.form.default.label')</label>
                        <div class="row">
                            <div class="col-12 mt-3">
                                <div
                                    class="form-check form-check-inline {{ $errors->has('providerPayment.is_default') ? ' is-invalid' : '' }}">
                                    <input wire:model="providerPayment.is_default" class="form-check-input" type="radio"
                                        name="inlineRadioOptions" id="inlineRadio1" value="1">
                                    <label class="form-check-label"
                                        for="inlineRadio1">@lang('backend.crud.provider-payment-condition.form.default.options.yes')</label>
                                </div>
                                <div
                                    class="form-check form-check-inline {{ $errors->has('providerPayment.is_default') ? ' is-invalid' : '' }}">
                                    <input wire:model="providerPayment.is_default" class="form-check-input" type="radio"
                                        name="inlineRadioOptions" id="inlineRadio2" value="0">
                                    <label class="form-check-label"
                                        for="inlineRadio2">@lang('backend.crud.provider-payment-condition.form.default.options.no')</label>
                                </div>
                            </div>
                        </div>
                        @error('providerPayment.is_default')
                        <span class="text-danger">{{ $message }}</span>
                        @enderror
                    </div>


                    <div class="col-12">
                        <div class="text-right">
                            @if (!isset($this->providerPayment->id) || $this->providerPayment->id == null)
                            <button wire:click.prevent="save()"
                                class="btn btn-brand btn-elevate btn-icon-sm">@lang('backend.forms.save')</button>
                            @else
                            <button wire:click.prevent="save()"
                                class="btn btn-brand btn-elevate btn-icon-sm btn-dark">@lang('backend.forms.update')</button>
                            @endif
                            <button wire:click.prevent="resetInputs()" type="button" class="btn btn-secondary close-btn"
                                data-dismiss="modal">@lang('backend.forms.close')</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>