<div class="kt-portlet kt-portlet--mobile">
    <div class="kt-portlet__head kt-portlet__head--lg">
        <div class="kt-portlet__head-label">
            <span class="kt-portlet__head-icon">
                <i class="kt-font-brand flaticon2-line-chart"></i>
            </span>
            <h3 class="kt-portlet__head-title text-uppercase">
                @lang('backend.crud.provider-payment-type.title')
            </h3>
        </div>
        <div class="kt-portlet__head-toolbar">
            <div class="kt-portlet__head-wrapper">
                <div class="kt-portlet__head-actions">
                    <button wire:click="addNew()" class="btn btn-brand btn-elevate btn-icon-sm" data-toggle="tooltip" data-placement="top"
                        title="Create">
                        <i class="fas fa-plus"></i>
                        @lang('backend.crud.provider-payment-type.list.add')
                    </button>
                </div>
            </div>
        </div>
    </div>
    <div class="kt-portlet__body">
        @if (session()->has('status'))
            <div class="alert alert-success alert-dismissible fade show" role="alert">
                {{ session('status') }}
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
        @endif
        @if (session()->has('error'))
            <div class="alert alert-danger alert-dismissible fade show" role="alert">
                {{ session('error') }}
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
        @endif
        <!--begin: Datatable -->
        <div id="users_list_table_wrapper" class="dataTables_wrapper dt-bootstrap4 no-footer">
            <div class="dt-buttons btn-group">
                <div class="btn-group">
                    <select wire:model="entries" class="custom-select" tabindex="0" aria-controls="users_list_table"
                        type="button" aria-haspopup="true" aria-expanded="false">
                        <option value="10">@lang('backend.forms.selects.10')</option>
                        <option value="25">@lang('backend.forms.selects.25')</option>
                        <option value="50">@lang('backend.forms.selects.50')</option>
                        <option value="100">@lang('backend.forms.selects.100')</option>
                    </select>
                </div>
                <div class="btn-group ml-3">
                    <button wire:click="downloadExcel" class="btn btn-success rounded-left pl-3 pr-2" type="button" data-toggle="tooltip" data-placement="top" title="Exportar tabla Excel">
                        <i class="fas fa-file-excel m-0"></i>
                    </button>
                </div>
            </div>
            <div id="users_list_table_filter" class="dataTables_filter">
                <label>@lang('backend.forms.search')
                    <input wire:model="search" type="search" class="form-control form-control-sm" placeholder=""
                        aria-controls="users_list_table">
                </label>
            </div>
            
            <div class="table-responsive">
                <table class="table table-striped- table-bordered table-hover table-checkable" id="users_list_table">
                    <thead>
                        <tr class="text-uppercase">
                            <th class="col-1" wire:click="sortBy('created_at')" style="cursor: pointer">@lang('backend.crud.created_at') @include('partials._sort-icon',['field'=>'created_at'])</th>
                            <th class="col-1" wire:click="sortBy('code')" style="cursor: pointer">@lang('backend.crud.provider-payment-type.list.code') @include('partials._sort-icon',['field'=>'code'])</th>
                            <th class="col-2" wire:click="sortBy('name')" style="cursor: pointer">@lang('backend.crud.provider-payment-type.list.name') @include('partials._sort-icon',['field'=>'name'])</th>
                            <th class="col-3" wire:click="sortBy('description')" style="cursor: pointer">@lang('backend.crud.provider-payment-type.list.description') @include('partials._sort-icon',['field'=>'description'])</th>
                            <th class="col-1" wire:click="sortBy('is_default')" style="cursor: pointer">@lang('backend.crud.provider-payment-type.list.default') @include('partials._sort-icon',['field'=>'is_default'])</th>
                            <th class="col-1" style="cursor: pointer">@lang('backend.crud.actions')</th>
                        </tr>
                    </thead>
                    <tbody>
                        @forelse ($providerPaymentTypes as $providerPaymentType)
                            <tr>
                                <td>{{ $providerPaymentType->created_at != null ? auth()->user()
                                                                                        ->applyDateFormat($providerPaymentType->created_at) : '' }}
                                </td>
                                <td>{{ $providerPaymentType->code ?? '' }}</td>
                                <td>{{ $providerPaymentType->name ?? '' }}</td>
                                <td>{{ $providerPaymentType->description ?? '' }}</td>
                                <td>
                                    @if($providerPaymentType->is_default == '1')
                                    @lang('backend.crud.provider-payment-type.form.default.options.yes')
                                    @elseif($providerPaymentType->is_default == '0')
                                    @lang('backend.crud.provider-payment-type.form.default.options.no')
                                    @else
                                    
                                    @endif
                                </td>
                                <td nowrap>
                                    <button wire:click="edit('{{ $providerPaymentType->id }}')" type="button"
                                        class="btn btn-sm btn-clean btn-icon btn-icon-md" data-toggle="tooltip" data-placement="top" title="Edit">
                                        <i class="fas fa-edit"></i>
                                    </button>
                                    <button wire:click="confirmDelete('{{ $providerPaymentType->id }}')" type="button" class="btn btn-sm btn-clean btn-icon btn-icon-md"
                                        data-toggle="modal" data-target="#delete_modal"
                                        data-toggle="tooltip" data-placement="top" title="Delete">
                                        <i class="fas fa-trash"></i>
                                    </button>
                                </td>
                            </tr>
                        @empty
                            <tr>
                                <td colspan="6" >@lang('backend.table-empty')</td>
                            </tr>
                        @endforelse
                    </tbody>
                </table>
            </div>
            <div class="dataTables_info">
                {{ $providerPaymentTypes->links() }}
            </div>
        </div>

        <!--end: Datatable -->
        @push('css')
            <!--begin::Page Vendors Styles(used by this page) -->
            <link href="{{ url('/') }}/assets/vendors/custom/datatables/datatables.bundle.css" rel="stylesheet"
                type="text/css" />
            <!--end::Page Vendors Styles -->
        @endpush
        @push('js')
            <!--begin::Page Vendors(used by this page) -->
            <script src="{{ config('custom.iworking_public_bucket') }}/assets/js/demo1/pages/crud/datatables/basic/headers.min.js" type="text/javascript">
            </script>
            <script src="{{ config('custom.iworking_public_bucket') }}/assets/vendors/custom/datatables/datatables.bundle.js" type="text/javascript">
            </script>
        @endpush

        @include('livewire.cruds.provider-payment-types.partials.form')
        @include('partials.deleteModal')
    </div>
</div>

@push('js')
<script type="text/javascript">

    window.addEventListener('showModal', () => {
        $('#formModal').modal('show');
    });

    window.addEventListener('providerPaymentTypeStore', () => {
        $('#formModal').modal('hide');
    });
    window.addEventListener('deleteItem', () => {
        $('#delete_modal').modal('hide');
    });
</script>
@endpush
