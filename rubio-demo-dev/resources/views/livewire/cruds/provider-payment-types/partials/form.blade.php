<div wire:ignore.self id="formModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="formModalLabel"
    aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                @if (!isset($this->providerPaymentType->id) || $this->providerPaymentType->id == null)
                    <h5 class="modal-title" id="formModalLabel">@lang('backend.crud.provider-payment-type.form.add')</h5>
                @else
                    <h5 class="modal-title" id="formModalLabel">@lang('backend.crud.provider-payment-type.form.update')</h5>
                @endif
                <button wire:click.prevent="resetInputs()" type="button" class="close" data-dismiss="modal"
                    aria-label="Close">
                    <span aria-hidden="true close-btn">×</span>
                </button>
            </div>
            <div class="modal-body">
                @if (session()->has('error') || $errors->has('providerPaymentType.key_value'))
                    <div class="alert alert-danger alert-dismissible fade show" role="alert">
                        {{ session()->has('error') ? session('error') : $errors->first('providerPaymentType.key_value') }}
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                @endif
                <form>
                    <div class="form-group {{ $errors->has(`providerPaymentType.code`) ? ' has-danger' : '' }}">
                        <label for="code">@lang('backend.crud.provider-payment-type.form.code')</label>
                        <input wire:model="providerPaymentType.code" maxlength="2" type="text" id="code_input"
                            class="form-control {{ $errors->has('providerPaymentType.code') ? ' is-invalid' : '' }}"
                            placeholder="@lang('backend.crud.provider-payment-type.form.code_placeholder')" />
                        @error('providerPaymentType.code')
                            <span class="text-danger">{{ $message }}</span>
                        @enderror
                    </div>
                    <div class="form-group {{ $errors->has('providerPaymentType.name') ? ' has-danger' : '' }}">
                        <label for="name">@lang('backend.crud.provider-payment-type.form.name')</label>
                        <input wire:model="providerPaymentType.name"  maxlength="255" type="text" min="0" max="100" id="name_input"
                                class="form-control {{ $errors->has('providerPaymentType.name') ? ' is-invalid' : '' }}"
                                placeholder="@lang('backend.crud.provider-payment-type.form.name_placeholder')" />
                        @error('providerPaymentType.name')<span class="text-danger">{{ $message }}</span>
                        @enderror
                    </div>

                    <div class="form-group {{ $errors->has(`providerPaymentType.description`) ? ' has-danger' : '' }}">
                        <label for="description">@lang('backend.crud.provider-payment-type.form.description')</label>
                        <textarea wire:model="providerPaymentType.description" rows="7"  maxlength="255" type="text" id="description_input"
                            class="form-control {{ $errors->has('providerPaymentType.description') ? ' is-invalid' : '' }}"
                            placeholder="@lang('backend.crud.provider-payment-type.form.description_placeholder')"></textarea>
                        @error('providerPaymentType.description')
                            <span class="text-danger">{{ $message }}</span>
                         @enderror
                    </div>
                    <div class="col-12 col-md-6">
                        <div class="form-group {{ $errors->has('providerPaymentType.is_default') ? ' has-danger' : '' }}">
                            <label for="type">@lang('backend.crud.provider-payment-type.form.default.label')</label>
                            <div class="row">
                                <div class="col-12 mt-3">
                                    <div
                                        class="form-check form-check-inline {{ $errors->has('providerPaymentType.is_default') ? ' is-invalid' : '' }}">
                                        <input wire:model="providerPaymentType.is_default" class="form-check-input"
                                            type="radio" name="inlineRadioOptions" id="inlineRadio1"
                                            value="1">
                                        <label class="form-check-label"
                                            for="inlineRadio1">@lang('backend.crud.provider-payment-type.form.default.options.yes')</label>
                                    </div>
                                    <div
                                        class="form-check form-check-inline {{ $errors->has('providerPaymentType.is_default') ? ' is-invalid' : '' }}">
                                        <input wire:model="providerPaymentType.is_default" class="form-check-input"
                                            type="radio" name="inlineRadioOptions" id="inlineRadio2"
                                            value="0">
                                        <label class="form-check-label"
                                            for="inlineRadio2">@lang('backend.crud.provider-payment-type.form.default.options.no')</label>
                                    </div>
                                </div>
                            </div>
                            @error('providerPaymentType.is_default')
                                <span class="text-danger">{{ $message }}</span>
                            @enderror
                        </div>
                    </div>


                    <div class="row">
                        <div class="col-12">
                            <div class="text-right">
                                @if (!isset($this->providerPaymentType->id) || $this->providerPaymentType->id == null)
                                    <button wire:click.prevent="save()"
                                        class="btn btn-brand btn-elevate btn-icon-sm">@lang('backend.forms.save')</button>
                                @else
                                    <button wire:click.prevent="save()"
                                        class="btn btn-brand btn-elevate btn-icon-sm btn-dark">@lang('backend.forms.update')</button>
                                @endif
                                <button wire:click.prevent="resetInputs()" type="button"
                                    class="btn btn-secondary close-btn"
                                    data-dismiss="modal">@lang('backend.forms.close')</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>