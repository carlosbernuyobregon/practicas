<div wire:ignore.self id="formModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="formModalLabel"
    aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                @if (!isset($this->currency->id) || $this->currency->id == null)
                    <h5 class="modal-title" id="formModalLabel">@lang('backend.crud.currency.form.add')</h5>
                @else
                    <h5 class="modal-title" id="formModalLabel">@lang('backend.crud.currency.form.update')</h5>
                @endif
                <button wire:click.prevent="resetInputs()" type="button" class="close" data-dismiss="modal"
                    aria-label="Close">
                    <span aria-hidden="true close-btn">×</span>
                </button>
            </div>
            <div class="modal-body">
                @if (session()->has('error') || $errors->has('currency.key_value'))
                    <div class="alert alert-danger alert-dismissible fade show" role="alert">
                        {{ session()->has('error') ? session('error') : $errors->first('currency.key_value') }}
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                @endif
                <form>
                    <div class="form-group {{ $errors->has(`currency.name`) ? ' has-danger' : '' }}">
                        <label for="name">@lang('backend.crud.currency.form.name')</label>
                        <input wire:model="currency.name" type="text" id="name_input"
                            class="form-control {{ $errors->has('currency.name') ? ' is-invalid' : '' }}"
                            placeholder="@lang('backend.crud.currency.form.name_placeholder')" />
                        @error('currency.name')
                            <span class="text-danger">{{ $message }}</span>
                        @enderror
                    </div>
                    <div class="form-group {{ $errors->has('currency.code') ? ' has-danger' : '' }}">
                        <label for="code">@lang('backend.crud.currency.form.code')</label>
                        <input wire:model="currency.code" maxlength="3" type="text" id="code_input"
                            class="form-control {{ $errors->has('currency.code') ? ' is-invalid' : '' }}"
                            placeholder="@lang('backend.crud.currency.form.code_placeholder')" />
                        @error('currency.code')<span class="text-danger">{{ $message }}</span>
                        @enderror
                    </div>

                    <div class="form-group {{ $errors->has('currency.customer_id') ? ' has-danger' : '' }}">
                        <label for="customer_id">@lang('backend.crud.currency.form.customer')</label>
                        <select class="form-control {{ $errors->has('currency.customer_id') ? ' is-invalid' : '' }}"
                            id="customer_id_input" wire:model="currency.customer_id">
                            <option value="">@lang('backend.forms.select-option')</option>
                            @foreach ($customers as $customer)
                                <option value="{{ $customer->id }}">{{ $customer->name }}</option>
                            @endforeach
                        </select>
                        @error('currency.customer_id')<span class="text-danger">{{ $message }}</span>
                        @enderror
                    </div>

                    <div class="row">
                        <div class="col-12">
                            <div class="text-right">
                                @if (!isset($this->currency->id) || $this->currency->id == null)
                                    <button wire:click.prevent="save()"
                                        class="btn btn-brand btn-elevate btn-icon-sm">@lang('backend.forms.save')</button>
                                @else
                                    <button wire:click.prevent="save()"
                                        class="btn btn-brand btn-elevate btn-icon-sm btn-dark">@lang('backend.forms.update')</button>
                                @endif
                                <button wire:click.prevent="resetInputs()" type="button"
                                    class="btn btn-secondary close-btn"
                                    data-dismiss="modal">@lang('backend.forms.close')</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

