<div wire:ignore.self id="formModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="formModalLabel"
    aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                @if (!isset($this->budget->id) || $this->budget->id == null)
                    <h5 class="modal-title" id="formModalLabel">@lang('backend.crud.cost-center.form.add')</h5>
                @else
                    <h5 class="modal-title" id="formModalLabel">@lang('backend.crud.cost-center.form.update'): {{ $this->budget->external_id ?? '' }}</h5>
                @endif
                <button wire:click.prevent="resetInputs()" type="button" class="close" data-dismiss="modal"
                    aria-label="Close">
                    <span aria-hidden="true close-btn">×</span>
                </button>
            </div>
            <div class="modal-body">
                @if (session()->has('error'))
                    <div class="alert alert-danger alert-dismissible fade show" role="alert">
                        {{ session('error') }}
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                @endif
                <form>
                    <div class="form-group {{ $errors->has('budget.name') ? ' has-danger' : '' }}">
                        <label for="name">@lang('backend.crud.cost-center.form.name')</label>
                        <input wire:model="budget.name" type="text" id="name_input"
                            class="form-control {{ $errors->has('budget.name') ? ' is-invalid' : '' }}"
                            placeholder="@lang('backend.crud.cost-center.form.name_placeholder')" />
                        @error('budget.name')
                            <span class="text-danger">{{ $message }}</span>
                        @enderror
                    </div>
                    <div class="form-group {{ $errors->has('budget.accounting-account') ? ' has-danger' : '' }}">
                        <label for="accounting-account">Cuenta contable</label>
                        <select class="form-control {{ $errors->has('budget.accounting-account') ? ' is-invalid' : '' }}"
                            id="accounting-account_input" wire:model="budget.accounting_account_id">
                            <option value="">@lang('backend.forms.select-option')</option>
                            @foreach ($accountingAccounts as $item)
                                <option value="{{ $item->id }}">{{ $item->name }} ({{ $item->external_id }})</option>
                            @endforeach
                        </select>
                        @error('budget.accounting-account')<span class="text-danger">{{ $message }}</span>
                        @enderror
                    </div>
                    <div class="form-group {{ $errors->has('budget.budget-agrupation') ? ' has-danger' : '' }}">
                        <label for="budget-agrupation">Agrupación presupuestaria</label>
                        <select class="form-control {{ $errors->has('budget.budget-agrupation') ? ' is-invalid' : '' }}"
                            id="budget-agrupation_input" wire:model="budget.budget_agrupation_id">
                            <option value="">@lang('backend.forms.select-option')</option>
                            @foreach ($budgetAgrupations as $item)
                                <option value="{{ $item->id }}">{{ $item->name }} ({{ $item->company->name }})</option>
                            @endforeach
                        </select>
                        @error('budget.budget-agrupation')<span class="text-danger">{{ $message }}</span>
                        @enderror
                    </div>
                    <hr>
                    <div class="row">
                        <div class="col-12">
                            <div class="form-group {{ $errors->has('budget.accounting-account') ? ' has-danger' : '' }}">
                                <label for="accounting-account">Añadir centro de coste</label>
                                <select class="form-control {{ $errors->has('budget.accounting-account') ? ' is-invalid' : '' }}"
                                    id="accounting-account_input" wire:model="newCostCenterId">
                                    <option value="">@lang('backend.forms.select-option')</option>
                                    @foreach ($costCenters as $item)
                                        <option value="{{ $item->id }}">{{ $item->name }} ({{ (int)$item->external_id }})</option>
                                    @endforeach
                                </select>
                                @error('budget.accounting-account')<span class="text-danger">{{ $message }}</span>
                                @enderror
                            </div>
                        </div>
                        <div class="col-12 pb-2">
                            <button wire:click.prevent="addCostCenter()"
                                        class="btn btn-brand btn-elevate btn-icon-xs" @if($newCostCenterId == '') disabled @endif>Añadir</button>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-12">
                            <table class="table table-striped table-bordered table-hover table-checkable">
                                <thead>
                                    <tr>
                                        <th>Centro de coste</th>
                                        <th>Acción</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @forelse ($budget->costCenters as $costCenter)
                                    <tr>
                                        <td>{{ $costCenter->name }} ({{ (int)$costCenter->external_id }})</td>
                                        <td><button wire.click="removeCostCenter('{{ $costCenter->id }}')" class="btn btn-danger btn-elevate btn-icon-xs">Quitar</button></td>
                                    </tr>
                                    @empty
                                    <tr>
                                        <td colspan="2">No se han vinculado centros de coste</td>
                                    </tr>
                                    @endforelse
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-12">
                            <div class="text-right">
                                @if (!isset($this->budget->id) || $this->budget->id == null)
                                    <button wire:click.prevent="save()"
                                        class="btn btn-brand btn-elevate btn-icon-sm">@lang('backend.forms.save')</button>
                                @else
                                    <button wire:click.prevent="save()"
                                        class="btn btn-brand btn-elevate btn-icon-sm btn-dark">@lang('backend.forms.update')</button>
                                @endif
                                <button wire:click.prevent="resetInputs()" type="button"
                                    class="btn btn-secondary close-btn"
                                    data-dismiss="modal">@lang('backend.forms.close')</button>
                            </div>
                        </div>
                    </div>

                </form>
            </div>
        </div>
    </div>
</div>
