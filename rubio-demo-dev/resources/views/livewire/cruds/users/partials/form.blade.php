<div wire:ignore.self id="formModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="formModalLabel"
    aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                @if (!isset($this->user->id) || $this->user->id == null)
                    <h5 class="modal-title" id="formModalLabel">@lang('backend.crud.user.form.add')</h5>
                @else
                    <h5 class="modal-title" id="formModalLabel">@lang('backend.crud.user.form.update')</h5>
                @endif
                <button wire:click.prevent="resetInputs()" type="button" class="close" data-dismiss="modal"
                    aria-label="Close">
                    <span aria-hidden="true close-btn">×</span>
                </button>
            </div>
            <div class="modal-body">
                @if (session()->has('error'))
                    <div class="alert alert-danger alert-dismissible fade show" role="alert">
                        {{ session('error') }}
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                @endif
                <form autocomplete="off">
                    <h6 class=" mb-4">@lang('backend.crud.user.information')</h6>
                    <div class="pl-lg-4">
                        <div class="row">
                            <div class="col-6">
                                <div class="form-group{{ $errors->has('user.name') ? ' has-danger' : '' }}">
                                    <label class="form-control-label"
                                        for="input-first_name">@lang('backend.crud.user.edit.name')</label>
                                    <input wire:model="user.name" type="text" name="name" id="input-name"
                                        class="form-control form-control-alternative{{ $errors->has('user.name') ? ' is-invalid' : '' }}"
                                        placeholder="@lang('backend.crud.user.edit.name_placeholder')" required
                                        autofocus>

                                    @if ($errors->has('user.name'))
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('user.name') }}</strong>
                                        </span>
                                    @endif
                                </div>
                                <div class="form-group{{ $errors->has('user.first_name') ? ' has-danger' : '' }}">
                                    <label class="form-control-label"
                                        for="input-first_name">@lang('backend.crud.user.edit.first_name')</label>
                                    <input wire:model="user.first_name" type="text" name="first_name"
                                        id="input-first_name"
                                        class="form-control form-control-alternative{{ $errors->has('user.first_name') ? ' is-invalid' : '' }}"
                                        placeholder="@lang('backend.crud.user.edit.first_name_placeholder')" required>

                                    @if ($errors->has('user.first_name'))
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('user.first_name') }}</strong>
                                        </span>
                                    @endif
                                </div>
                                <div class="form-group{{ $errors->has('user.last_name') ? ' has-danger' : '' }}">
                                    <label class="form-control-label"
                                        for="input-last_name">@lang('backend.crud.user.edit.last_name')</label>
                                    <input wire:model="user.last_name" type="text" name="last_name" id="input-last_name"
                                        class="form-control form-control-alternative{{ $errors->has('user.last_name') ? ' is-invalid' : '' }}"
                                        placeholder="@lang('backend.crud.user.edit.last_name_placeholder')" required>

                                    @if ($errors->has('user.last_name'))
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('user.last_name') }}</strong>
                                        </span>
                                    @endif
                                </div>
                                <div class="form-group{{ $errors->has('user.email') ? ' has-danger' : '' }}">
                                    <label class="form-control-label"
                                        for="input-email">@lang('backend.crud.user.edit.email')</label>
                                    <input wire:model="user.email" type="email" name="email" id="input-email"
                                        class="form-control form-control-alternative{{ $errors->has('user.email') ? ' is-invalid' : '' }}"
                                        placeholder="@lang('backend.crud.user.edit.email_placeholder')" required>

                                    @if ($errors->has('user.email'))
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('user.email') }}</strong>
                                        </span>
                                    @endif
                                </div>
                                <div class="form-group{{ $errors->has('user.phone') ? ' has-danger' : '' }}">
                                    <label class="form-control-label"
                                        for="input-phone">@lang('backend.crud.user.edit.phone')</label>
                                    <input wire:model="user.phone" type="text" name="phone" id="input-phone"
                                        class="form-control form-control-alternative{{ $errors->has('user.phone') ? ' is-invalid' : '' }}"
                                        placeholder="@lang('backend.crud.user.edit.phone_placeholder')" required
                                        autofocus>

                                    @if ($errors->has('user.phone'))
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('user.phone') }}</strong>
                                        </span>
                                    @endif
                                </div>

                                <div class="form-group {{ $errors->has('user.country_id') ? ' has-danger' : '' }}">
                                    <label for="country_id">@lang('backend.crud.user.edit.country')</label>
                                    <select wire:model='user.country_id'
                                        class="form-control {{ $errors->has('user.country_id') ? ' is-invalid' : '' }}"
                                        id="country_id_input" wire:model="user.country_id">
                                        <option value="">@lang('backend.forms.select-option')</option>
                                        @foreach ($this->countries as $country)
                                            <option value="{{ $country->id }}">
                                                {{ Str::title($country->key_value) }}</option>
                                        @endforeach
                                    </select>
                                    @error('user.country_id')<span class="text-danger">{{ $message }}</span>
                                    @enderror
                                </div>

                                <div class="form-group{{ $errors->has('password') ? ' has-danger' : '' }}">
                                    <label class="form-control-label"
                                        for="input-password">@lang('backend.crud.user.edit.new-password')</label>
                                    <input wire:model.lazy="password" type="password" name="password" id="input-password"
                                        class="form-control form-control-alternative{{ $errors->has('password') ? ' is-invalid' : '' }}"
                                        placeholder="@lang('backend.crud.user.edit.new-password_placeholder')"
                                        aria-describedby="passwordHelp">
                                    @isset($this->user->id)
                                        <small id="passwordHelp"
                                            class="form-text text-muted">@lang('backend.crud.user.edit.empty-password-msg')</small>
                                    @endisset

                                    @if ($errors->has('password'))
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('password') }}</strong>
                                        </span>
                                    @endif
                                </div>
                                <div
                                    class="form-group {{ $errors->has('password_confirmation') ? ' has-danger' : '' }}">
                                    <label class="form-control-label"
                                        for="input-password-confirmation">@lang('backend.crud.user.edit.confirm-password')</label>
                                    <input wire:model.lazy="password_confirmation" type="password"
                                        name="password_confirmation" id="input-password-confirmation"
                                        class="form-control form-control-alternative {{ $errors->has('password') ? ' is-invalid' : '' }}"
                                        placeholder="@lang('backend.crud.user.edit.confirm-password_placeholder')"
                                        value="" required>

                                    @if ($errors->has('password_confirmation'))
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('password_confirmation') }}</strong>
                                        </span>
                                    @endif
                                </div>

                                <hr>

                                <div class="form-group {{ $errors->has('user.date_format') ? ' has-danger' : '' }}">
                                    <label for="date_format">@lang('backend.crud.user.edit.date-format')</label>
                                    <select wire:model='user.date_format'
                                        class="form-control {{ $errors->has('user.date_format') ? ' is-invalid' : '' }}"
                                        id="date_format_input" wire:model="user.date_format">
                                        <option value="">@lang('backend.forms.select-option')</option>
                                        @foreach ($this->dateFormats as $format => $name)
                                            <option value="{{ $format }}">{{ $name }}</option>
                                        @endforeach
                                    </select>
                                    @error('user.date_format')<span class="text-danger">{{ $message }}</span>
                                    @enderror
                                </div>

                                <div
                                    class="form-group {{ $errors->has('user.numbers_format') ? ' has-danger' : '' }}">
                                    <label for="numbers_format">@lang('backend.crud.user.edit.number-format')</label>
                                    <select wire:model='user.numbers_format'
                                        class="form-control {{ $errors->has('user.numbers_format') ? ' is-invalid' : '' }}"
                                        id="numbers_format_input" wire:model="user.numbers_format">
                                        <option value="">@lang('backend.forms.select-option')</option>
                                        @foreach ($this->numberFormats as $format)
                                            <option value="{{ $format }}">{{ $format }}</option>
                                        @endforeach
                                    </select>
                                    @error('user.numbers_format')<span class="text-danger">{{ $message }}</span>
                                    @enderror
                                </div>


                                <div class="form-group{{ $errors->has('user.timezone') ? ' has-danger' : '' }}">
                                    <label class="form-control-label"
                                        for="input-timezone">@lang('backend.crud.user.edit.timezone')</label>
                                    <select wire:model="user.timezone" name="timezone" id="input-timezone"
                                        class="form-control form-control-alternative{{ $errors->has('user.timezone') ? ' is-invalid' : '' }}">
                                        <option value="">@lang('backend.forms.select-option')</option>
                                        @foreach (timezone_identifiers_list() as $timezone)
                                            <option value="{{ $timezone }}">{{ $timezone }}</option>
                                        @endforeach
                                    </select>
                                    @if ($errors->has('user.timezone'))
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('user.timezone') }}</strong>
                                        </span>
                                    @endif
                                </div>

                                <div class="form-group {{ $errors->has('user.customer_id') ? ' has-danger' : '' }}">
                                    <label for="customer_id">@lang('backend.crud.user.edit.customer')</label>
                                    <select
                                        class="form-control {{ $errors->has('user.customer_id') ? ' is-invalid' : '' }}"
                                        id="customer_id_input" wire:model="user.customer_id">
                                        <option value="">@lang('backend.forms.select-option')</option>
                                        @foreach ($customers as $customer)
                                            <option value="{{ $customer->id }}">{{ $customer->name }}</option>
                                        @endforeach
                                    </select>
                                    @error('user.customer_id')<span class="text-danger">{{ $message }}</span>
                                    @enderror
                                </div>
                            </div>
                            <div class="col-6">
                                <div class="form-group row justify-content-center">
                                    <label class="col-8 col-form-label">@lang('backend.crud.user.edit.roles')</label>
                                    <div class="col-8">
                                        <div class="kt-checkbox-list">
                                            @foreach ($roles as $role)
                                                <label class="kt-checkbox">
                                                    <input wire:model="userRoles.{{ $role->id }}"
                                                        id="role_{{ $role->id }}" type="checkbox"
                                                        name="roles[{{ $role->id }}]">
                                                    {{ $role->translate(App::getLocale())->name }}
                                                    <span></span>
                                                </label>
                                            @endforeach
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group row justify-content-center">
                                    <label
                                        class="col-8 col-form-label">@lang('backend.crud.user.edit.user-state')</label>
                                    <div class="col-8">
                                        <div class="kt-checkbox-list">
                                            <label class="kt-checkbox">
                                                <input wire:model="active" type="checkbox">
                                                @lang('backend.crud.user.edit.active')
                                                <span></span>
                                            </label>
                                            <label class="kt-checkbox">
                                                <input wire:model="blocked" type="checkbox">
                                                @lang('backend.crud.user.edit.blocked')
                                                <span></span>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group row justify-content-center">
                                    <label class="col-8 col-form-label">@lang('backend.crud.user.edit.cost_center')</label>
                                    <div class="col-8">
                                        <select class="form-control mb-3" wire:model="cost_center_value"
                                            wire:change="onSelectCostCenter($event.target.value)">
                                            <option value="">@lang('backend.forms.select-option')</option>
                                            @foreach ($cost_centers as $cost_center)
                                                <option value="{{ $cost_center->id }}">{{ $cost_center->name }} ({{ (int)$cost_center->external_id }})
                                                </option>
                                            @endforeach
                                        </select>

                                        @if (count($cost_centers_selected) > 0)
                                            <div class="kt-list-timeline">
                                                <div class="kt-list-timeline__items">
                                                    @foreach ($cost_centers_selected as $selected_cost_center)
                                                        <div class="kt-list-timeline__item">
                                                            <span
                                                                class="kt-list-timeline__text">{{ $selected_cost_center->name }} ({{ (int)$selected_cost_center->external_id }})</span>
                                                            @if ($selected_cost_center->id == $default_cost_center)
                                                                <span class="kt-list-timeline__time kt-font-bolder kt-font-danger">@lang('backend.crud.user.edit.cost_center_default_msg')</span>
                                                            @else
                                                                <span class="kt-list-timeline__time"><a href="#" class="kt-link kt-font-bolder"
                                                                        wire:click.prevent="setDefaultCostCenter('{{ $selected_cost_center->id }}')">@lang('backend.crud.user.edit.cost_center_set_default_msg')</a></span>
                                                            @endif
                                                            <button type="button"
                                                                wire:click="deleteSelectedCostCenter('{{ $selected_cost_center->id }}')"
                                                                class="btn btn-sm btn-clean btn-icon btn-icon-sm"
                                                                title="Delete">
                                                                <i class="fas fa-trash" aria-hidden="true"></i>
                                                            </button>

                                                        </div>
                                                    @endforeach
                                                </div>
                                            </div>
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-12">
                                <div class="text-right">
                                    @if (!isset($this->user->id) || $this->user->id == null)
                                        <button wire:click.prevent="save()"
                                            class="btn btn-brand btn-elevate btn-icon-sm">@lang('backend.forms.save')</button>
                                    @else
                                        <button wire:click.prevent="save()"
                                            class="btn btn-brand btn-elevate btn-icon-sm btn-dark">@lang('backend.forms.update')</button>
                                    @endif
                                    <button wire:click.prevent="resetInputs()" type="button"
                                        class="btn btn-secondary close-btn"
                                        data-dismiss="modal">@lang('backend.forms.close')</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
