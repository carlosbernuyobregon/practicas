<div wire:ignore.self id="formModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="formModalLabel"
    aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                @if (!isset($this->material->id) || $this->material->id == null)
                    <h5 class="modal-title" id="formModalLabel">@lang('backend.crud.material.form.add')</h5>
                @else
                    <h5 class="modal-title" id="formModalLabel">@lang('backend.crud.material.form.update')</h5>
                @endif
                <button wire:click.prevent="resetInputs()" type="button" class="close" data-dismiss="modal"
                    aria-label="Close">
                    <span aria-hidden="true close-btn">×</span>
                </button>
            </div>
            <div class="modal-body">
                @if (session()->has('error'))
                    <div class="alert alert-danger alert-dismissible fade show" role="alert">
                        {{ session('error') }}
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                @endif
                <form>

                    @if (!isset($this->material->id) || $this->material->id == null)
                        <!--external id-->
                        <div class="form-group {{ $errors->has(`material.external_id`) ? ' has-danger' : '' }}">
                            <label for="name">@lang('backend.crud.material.form.external_id')</label>
                            <input wire:model="material.external_id" type="text" id="external_id_input"
                                class="form-control {{ $errors->has('material.external_id') ? ' is-invalid' : '' }}"
                                placeholder="@lang('backend.crud.material.form.external_id_placeholder')" />
                            @error('material.external_id')
                                <span class="text-danger">{{ $message }}</span>
                            @enderror
                        </div>
                    @endif

                    <div class="form-group {{ $errors->has(`material.material_name`) ? ' has-danger' : '' }}">
                        <label for="name">@lang('backend.crud.material.form.material_name')</label>
                        <input wire:model="material.material_name" type="text" id="material_name_input"
                            class="form-control {{ $errors->has('material.material_name') ? ' is-invalid' : '' }}"
                            placeholder="@lang('backend.crud.material.form.material_name_placeholder')" />
                        @error('material.material_name')
                            <span class="text-danger">{{ $message }}</span>
                        @enderror
                    </div>

                    <div class="form-group {{ $errors->has(`material.company`) ? ' has-danger' : '' }}">
                        <label for="name">@lang('backend.crud.material.form.company')</label>
                        <input wire:model="material.company" type="text" id="company_input"
                            class="form-control {{ $errors->has('material.company') ? ' is-invalid' : '' }}"
                            placeholder="@lang('backend.crud.material.form.company')" />
                        @error('material.company')
                            <span class="text-danger">{{ $message }}</span>
                        @enderror
                    </div>

                    <div class="form-group {{ $errors->has(`material.type`) ? ' has-danger' : '' }}">
                        <label for="name">@lang('backend.crud.material.form.type')</label>
                        <input wire:model="material.type" type="text" id="type_input"
                            class="form-control {{ $errors->has('material.type') ? ' is-invalid' : '' }}"
                            placeholder="@lang('backend.crud.material.form.type')" />
                        @error('material.type')
                            <span class="text-danger">{{ $message }}</span>
                        @enderror
                    </div>

                    <div class="form-group {{ $errors->has('material.customer_id') ? ' has-danger' : '' }}">
                        <label for="customer_id">@lang('backend.crud.material.form.customer')</label>
                        <select class="form-control {{ $errors->has('material.customer_id') ? ' is-invalid' : '' }}"
                            id="customer_id_input" wire:model="material.customer_id">
                            <option value="">@lang('backend.forms.select-option')</option>
                            @foreach ($customers as $customer)
                                <option value="{{ $customer->id }}">{{ $customer->name }}</option>
                            @endforeach
                        </select>
                        @error('material.customer_id')<span class="text-danger">{{ $message }}</span>
                        @enderror
                    </div>

                    <div class="row">
                        <div class="col-12">
                            <div class="text-right">
                                @if (!isset($this->material->id) || $this->material->id == null)
                                    <button wire:click.prevent="save()"
                                        class="btn btn-brand btn-elevate btn-icon-sm">@lang('backend.forms.save')</button>
                                @else
                                    <button wire:click.prevent="save()"
                                        class="btn btn-brand btn-elevate btn-icon-sm btn-dark">@lang('backend.forms.update')</button>
                                @endif
                                <button wire:click.prevent="resetInputs()" type="button"
                                    class="btn btn-secondary close-btn"
                                    data-dismiss="modal">@lang('backend.forms.close')</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>