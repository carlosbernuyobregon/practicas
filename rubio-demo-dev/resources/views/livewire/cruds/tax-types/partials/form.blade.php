<div wire:ignore.self id="formModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="formModalLabel"
    aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                @if (!isset($this->taxType->id) || $this->taxType->id == null)
                    <h5 class="modal-title" id="formModalLabel">@lang('backend.crud.tax-type.form.add')</h5>
                @else
                    <h5 class="modal-title" id="formModalLabel">@lang('backend.crud.tax-type.form.update')</h5>
                @endif
                <button wire:click.prevent="resetInputs()" type="button" class="close" data-dismiss="modal"
                    aria-label="Close">
                    <span aria-hidden="true close-btn">×</span>
                </button>
            </div>
            <div class="modal-body">
                @if (session()->has('error') || $errors->has('taxType.key_value'))
                    <div class="alert alert-danger alert-dismissible fade show" role="alert">
                        {{ session()->has('error') ? session('error') : $errors->first('taxType.key_value') }}
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                @endif
                <form>
                    <div class="form-group {{ $errors->has(`taxType.type`) ? ' has-danger' : '' }}">
                        <label for="name">@lang('backend.crud.tax-type.form.type')</label>
                        <input wire:model="taxType.type" type="text" maxlength="2" id="type_input"
                            class="form-control {{ $errors->has('taxType.type') ? ' is-invalid' : '' }}"
                            placeholder="@lang('backend.crud.tax-type.form.type_placeholder')" />
                        @error('taxType.type')
                            <span class="text-danger">{{ $message }}</span>
                        @enderror
                    </div>

                    <div class="form-group {{ $errors->has('taxType.description') ? ' has-danger' : '' }}">
                        <label for="description">@lang('backend.crud.tax-type.form.description')</label>
                        <textarea wire:model="taxType.description" type="text" id="description_input"
                            class="form-control {{ $errors->has('taxType.description') ? ' is-invalid' : '' }}"
                            placeholder="@lang('backend.crud.tax-type.form.description_placeholder')"></textarea>
                        @error('taxType.description')<span class="text-danger">{{ $message }}</span>
                        @enderror
                    </div>

                    <div class="row">
                        <div class="col-12 col-md-6">
                            <div class="form-group {{ $errors->has(`taxType.percentage`) ? ' has-danger' : '' }}">
                                <label for="name">@lang('backend.crud.tax-type.form.percentage')</label>
                                <input wire:model="taxType.percentage" type="number" min="0" max="100" id="percentage_input"
                                    class="form-control {{ $errors->has('taxType.percentage') ? ' is-invalid' : '' }}"
                                    placeholder="@lang('backend.crud.tax-type.form.percentage_placeholder')" />
                                @error('taxType.percentage')
                                    <span class="text-danger">{{ $message }}</span>
                                @enderror
                            </div>
                        </div>

                        <div class="col-12 col-md-6">
                            <div class="form-group {{ $errors->has('taxType.default') ? ' has-danger' : '' }}">
                                <label for="type">@lang('backend.crud.tax-type.form.default.label')</label>
                                <div class="row">
                                    <div class="col-12 mt-3">
                                        <div
                                            class="form-check form-check-inline {{ $errors->has('taxType.default') ? ' is-invalid' : '' }}">
                                            <input wire:model="taxType.default" class="form-check-input"
                                                type="radio" name="inlineRadioOptions" id="inlineRadio1"
                                                value="1">
                                            <label class="form-check-label"
                                                for="inlineRadio1">@lang('backend.crud.tax-type.form.default.options.yes')</label>
                                        </div>
                                        <div
                                            class="form-check form-check-inline {{ $errors->has('taxType.default') ? ' is-invalid' : '' }}">
                                            <input wire:model="taxType.default" class="form-check-input"
                                                type="radio" name="inlineRadioOptions" id="inlineRadio2"
                                                value="0">
                                            <label class="form-check-label"
                                                for="inlineRadio2">@lang('backend.crud.tax-type.form.default.options.no')</label>
                                        </div>
                                    </div>
                                </div>
                                @error('taxType.default')
                                    <span class="text-danger">{{ $message }}</span>
                                @enderror
                            </div>
                        </div>
                    </div>

                    <div class="form-group {{ $errors->has('taxType.customer_id') ? ' has-danger' : '' }}">
                        <label for="customer_id">@lang('backend.crud.tax-type.form.customer')</label>
                        <select class="form-control {{ $errors->has('taxType.customer_id') ? ' is-invalid' : '' }}"
                            id="customer_id_input" wire:model="taxType.customer_id">
                            <option value="">@lang('backend.forms.select-option')</option>
                            @foreach ($customers as $customer)
                                <option value="{{ $customer->id }}">{{ $customer->name }}</option>
                            @endforeach
                        </select>
                        @error('taxType.customer_id')<span class="text-danger">{{ $message }}</span>
                        @enderror
                    </div>

                    <div class="row">
                        <div class="col-12">
                            <div class="text-right">
                                @if (!isset($this->taxType->id) || $this->taxType->id == null)
                                    <button wire:click.prevent="save()"
                                        class="btn btn-brand btn-elevate btn-icon-sm">@lang('backend.forms.save')</button>
                                @else
                                    <button wire:click.prevent="save()"
                                        class="btn btn-brand btn-elevate btn-icon-sm btn-dark">@lang('backend.forms.update')</button>
                                @endif
                                <button wire:click.prevent="resetInputs()" type="button"
                                    class="btn btn-secondary close-btn"
                                    data-dismiss="modal">@lang('backend.forms.close')</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>