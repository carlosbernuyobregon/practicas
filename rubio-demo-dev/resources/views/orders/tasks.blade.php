@extends('layouts.app', [
'pageTitle' => 'Tareas',
'pageBreadcrumbs' => [
    'Pedidos',
    'Tareas',
]
])

@section('content')
<div class="kt-portlet kt-portlet--mobile">
    <div class="kt-portlet__head kt-portlet__head--lg">
        <div class="kt-portlet__head-label">
            <span class="kt-portlet__head-icon">
                <i class="kt-font-brand fas fa-shopping-cart fa-lg"></i>
            </span>
            <h3 class="kt-portlet__head-title text-uppercase">
                Listado de tareas
            </h3>
        </div>
        <div class="kt-portlet__head-toolbar">
            <div class="kt-portlet__head-wrapper">
                <div class="kt-portlet__head-actions">
                    <a href="{{ route('order.new') }}" class="btn btn-primary btn-sm btn-icon-sm" data-toggle="tooltip"
                        data-placement="top" title="Create">
                        <i class="fas fa-plus"></i>
                        Nuevo pedido
                    </a>
                </div>
            </div>
        </div>
    </div>
    <div class="kt-portlet__body">
        @include('components.alerts')
        @livewire('orders.task-list')
    </div>
</div>
@endsection
@push('css')

@endpush
