@extends('layouts.app', [
'pageTitle' => 'Dashboard de pedidos',
'pageBreadcrumbs' => [
    'Pedidos',
    'Dashboard',
]
])

@section('content')
<div class="kt-portlet kt-portlet--mobile">
    <div class="kt-portlet__head kt-portlet__head--lg">
        <div class="kt-portlet__head-label">
            <span class="kt-portlet__head-icon">
                <i class="kt-font-brand fas fa-shopping-cart fa-lg"></i>
            </span>
            <h3 class="kt-portlet__head-title text-uppercase">
                Dashboard de pedidos
            </h3>
        </div>
    </div>
    <div class="kt-portlet__body">
        <div class="row mt-2">
            <div class="col-12">
                @livewire('orders.dashboard',[

                ])
            </div>
        </div>
    </div>
</div>
@endsection
