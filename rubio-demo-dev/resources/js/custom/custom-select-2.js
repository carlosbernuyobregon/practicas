// Events for custom select 2 component
window.addEventListener('editingSelect', (data) => {
    let selectId    = data.detail.selectId;
    let objectValue = data.detail.objectValue;
    let objectField = data.detail.objectField;

    $('#' + selectId).val(objectValue).trigger('change')
});

window.addEventListener('resetCustomSelect', (dataReset) => {
    let selectIdReset       = dataReset.detail.selectId;
    let objectValueReset    = dataReset.detail.objectValue;
    let resetSelector       = $('#' + selectIdReset);

    /*console.log('---------------------');
    console.log('resetCustomSelect');
    console.log('selectIdReset: ' + selectIdReset);
    console.log('objectValueReset: ' + objectValueReset);
    console.log(resetSelector);
    console.log('current select val: ' + resetSelector.val());*/
    if (objectValueReset === false) {
        // resetSelector.val(null).trigger('change');
        // console.log('action none');
        // } else if (objectValueReset !== resetSelector.val()) {
    } else {
        // console.log('action change');
        // console.log(resetSelector);
        resetSelector.val(objectValueReset).trigger('change');
    }
});

window.addEventListener('hardResetCustomSelect', (dataHardReset) => {
    let selectIdHardReset       = dataHardReset.detail.selectId;
    let objectValueHardReset    = dataHardReset.detail.objectValue;
    let hardResetSelector       = $('#' + selectIdHardReset);

    console.log('hard reset with value: ' + objectValueHardReset);
    console.log('current select val: ' + hardResetSelector.val());
    hardResetSelector.val(null).trigger('change');
});
