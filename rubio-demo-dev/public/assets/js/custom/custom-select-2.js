// Events for custom select 2 component
window.addEventListener('editingSelect', (data) => {
    let selectId    = data.detail.selectId;
    let objectValue = data.detail.objectValue;
    let objectField = data.detail.objectField;

    $('#' + selectId).val(objectValue).trigger('change')
});

window.addEventListener('resetCustomSelect', (dataReset) => {
    let selectIdReset       = dataReset.detail.selectId;
    let objectValueReset    = dataReset.detail.objectValue;
    let resetSelector       = $('#' + selectIdReset);

    if (objectValueReset === false) {
        // resetSelector.val(null).trigger('change');
    // } else if (objectValueReset !== resetSelector.val()) {
    } else {
        resetSelector.val(objectValueReset).trigger('change');
    }
});

window.addEventListener('hardResetCustomSelect', (dataHardReset) => {
    let selectIdHardReset       = dataHardReset.detail.selectId;
    let objectValueHardReset    = dataHardReset.detail.objectValue;
    let hardResetSelector       = $('#' + selectIdHardReset);

    hardResetSelector.val(null).trigger('change');
});
