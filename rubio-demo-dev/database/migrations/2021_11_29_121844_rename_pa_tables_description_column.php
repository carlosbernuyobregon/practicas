<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class RenamePaTablesDescriptionColumn extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('pa_orders', function(Blueprint $table) {
            $table->renameColumn('description', 'name');
        });

        Schema::table('pa_business_areas', function(Blueprint $table) {
            $table->renameColumn('description', 'name');
        });

        Schema::table('pa_clients', function(Blueprint $table) {
            $table->renameColumn('description', 'name');
        });

        Schema::table('pa_families', function(Blueprint $table) {
            $table->renameColumn('description', 'name');
        });

        Schema::table('pa_persons', function(Blueprint $table) {
            $table->renameColumn('description', 'name');
        });

        Schema::table('pa_distribution_channels', function(Blueprint $table) {
            $table->renameColumn('description', 'name');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('pa_orders', function(Blueprint $table) {
            $table->renameColumn('name', 'description');
        });

        Schema::table('pa_business_areas', function(Blueprint $table) {
            $table->renameColumn('name', 'description');
        });

        Schema::table('pa_clients', function(Blueprint $table) {
            $table->renameColumn('name', 'description');
        });

        Schema::table('pa_families', function(Blueprint $table) {
            $table->renameColumn('name', 'description');
        });

        Schema::table('pa_persons', function(Blueprint $table) {
            $table->renameColumn('name', 'description');
        });

        Schema::table('pa_distribution_channels', function(Blueprint $table) {
            $table->renameColumn('name', 'description');
        });
    }
}
