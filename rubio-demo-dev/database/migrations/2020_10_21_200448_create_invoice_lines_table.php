<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateInvoiceLinesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('invoice_lines', function (Blueprint $table) {
            $table->uuid('id')->primary();
            $table->foreignUuid('invoice_id');
            $table->string('material_code')->nullable();
            $table->longText('material_description')->nullable();
            $table->string('material_qty')->nullable();
            $table->string('packaging_type')->nullable();
            $table->float('unit_price',16,4)->nullable();
            $table->string('currency')->nullable();
            $table->string('taxes')->nullable();
            $table->float('net_amount',16,4)->nullable();
            $table->string('irpf')->nullable();
            $table->float('total_amount',16,4)->nullable();
            $table->string('taxes_id')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('invoice_lines');
    }
}
