<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePepElementLinesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pep_element_lines', function (Blueprint $table) {
            $table->uuid('id')->primary();
            $table->uuid('customer_id');
            $table->uuid('pep_element_id');
            $table->string('external_id')->nullable();
            $table->string('name')->nullable();
            $table->string('sap_number_id')->nullable();
            $table->string('type')->nullable();
            $table->string('class')->nullable();
            $table->string('name_2')->nullable();
            $table->string('pep_object_sap')->nullable();
            $table->string('top_node_id')->nullable();
            $table->string('order')->nullable();
            $table->string('status')->nullable();
            $table->string('company_co')->nullable();
            $table->string('manageging_cost_center')->nullable();
            $table->string('planification_element')->nullable();
            $table->string('imputation_element')->nullable();
            $table->string('company_fi')->nullable();
            $table->string('pep_object_type')->nullable();
            $table->string('currency')->nullable();
            $table->string('costing_schema')->nullable();
            $table->string('interests_schema')->nullable();
            $table->string('investment_profile')->nullable();
            $table->string('pep_statistic_element')->nullable();
            $table->string('afec_id')->nullable();
            $table->string('afec_subnumber')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pep_element_lines');
    }
}
