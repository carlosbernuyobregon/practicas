<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddFlagAlertToInvoicesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('invoices', function (Blueprint $table) {
            $table->string('alert_flag_from_ocr')->nullable();
        });

        Schema::table('invoice_original_ocr_data', function (Blueprint $table) {
            $table->string('alert_flag_from_ocr')->nullable()->after('ocr_reference');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('invoices', function (Blueprint $table) {
            $table->dropColumn('alert_flag_from_ocr');
        });

        Schema::table('invoice_original_ocr_data', function (Blueprint $table) {
            $table->dropColumn('alert_flag_from_ocr');
        });
    }
}
