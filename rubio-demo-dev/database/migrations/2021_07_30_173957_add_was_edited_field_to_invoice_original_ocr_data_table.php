<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddWasEditedFieldToInvoiceOriginalOcrDataTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('invoice_original_ocr_data', function (Blueprint $table) {
            $table->boolean('was_edited')->nullable();
            $table->dateTime('edited_at')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('invoice_original_ocr_data', function (Blueprint $table) {
            $table->dropColumn(['was_edited', 'edited_at']);
        });
    }
}
