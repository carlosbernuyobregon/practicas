<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddImportApprovalLevelsToCostCentersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('cost_centers', function (Blueprint $table) {
            $table->float('amount_level_1',8,2,true)->default(0);
            $table->float('amount_level_2',8,2,true)->default(0);
            $table->float('amount_level_3',8,2,true)->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('cost_centers', function (Blueprint $table) {
            $table->dropColumn(['amount_level_1','amount_level_2','amount_level_3']);
        });
    }
}
