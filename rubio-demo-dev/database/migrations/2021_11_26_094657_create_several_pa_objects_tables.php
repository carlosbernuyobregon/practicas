<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSeveralPaObjectsTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pa_orders', function (Blueprint $table) {
            $table->id();
            $table->string('order');
            $table->string('description')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });

        Schema::create('pa_business_areas', function (Blueprint $table) {
            $table->id();
            $table->string('area');
            $table->string('description')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });

        Schema::create('pa_clients', function (Blueprint $table) {
            $table->id();
            $table->string('client');
            $table->string('description')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });

        Schema::create('pa_families', function (Blueprint $table) {
            $table->id();
            $table->string('family');
            $table->string('description')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });

        Schema::create('pa_persons', function (Blueprint $table) {
            $table->id();
            $table->string('person');
            $table->string('description')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });

        Schema::create('pa_distribution_channels', function (Blueprint $table) {
            $table->id();
            $table->string('channel');
            $table->string('description')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pa_orders');
        Schema::dropIfExists('pa_business_areas');
        Schema::dropIfExists('pa_clients');
        Schema::dropIfExists('pa_families');
        Schema::dropIfExists('pa_persons');
        Schema::dropIfExists('pa_distribution_channels');
    }
}
