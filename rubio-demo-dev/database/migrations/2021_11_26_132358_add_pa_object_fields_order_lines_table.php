<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddPaObjectFieldsOrderLinesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('order_lines', function (Blueprint $table) {
            $table->string('pa_client')->nullable()->after('currency');
            $table->string('pa_family')->nullable()->after('currency');
            $table->string('pa_person')->nullable()->after('currency');
            $table->string('pa_order')->nullable()->after('currency');
            $table->string('pa_country')->nullable()->after('currency');
            $table->string('pa_distribution_channel')->nullable()->after('currency');
            $table->string('pa_business_area')->nullable()->after('currency');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('order_lines', function (Blueprint $table) {
            $table->dropColumn([
                'pa_client',
                'pa_family',
                'pa_person',
                'pa_order',
                'pa_country',
                'pa_distribution_channel',
                'pa_business_area',
            ]);
        });
    }
}
