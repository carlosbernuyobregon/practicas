<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddTaxBalanceRangeAndTotalNetForNonZeroTaxPercentageFieldsToInvoicesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('invoices', function (Blueprint $table) {
            $table->integer('tax_balance_range')->nullable()->default(0)->after('taxes_irpf_amount');
            $table->double('total_net_for_non_zero_tax_percentage', 16, 4)->nullable()->default(0)->after('total_net');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('invoices', function (Blueprint $table) {
            $table->dropColumn(['tax_balance_range', 'total_net_for_non_zero_tax_percentage']);
        });
    }
}
