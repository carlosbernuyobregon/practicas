<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddCustomZoneFieldsToCountriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('countries', function (Blueprint $table) {
            $table->boolean('rest_of_the_world_zone')->nullable()->after('currency_id');
            $table->boolean('non_eu_zone')->nullable()->after('currency_id');
            $table->boolean('eu_zone')->nullable()->after('currency_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('countries', function (Blueprint $table) {
            $table->dropColumn(['eu_zone', 'non_eu_zone', 'rest_of_the_world_zone']);
        });
    }
}
