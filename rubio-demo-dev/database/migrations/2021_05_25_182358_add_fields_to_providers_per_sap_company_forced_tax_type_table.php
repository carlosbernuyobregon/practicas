<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddFieldsToProvidersPerSapCompanyForcedTaxTypeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('providers_per_sap_company_forced_tax_type', function (Blueprint $table) {
            $table->string('tax_type_fallback')->after('tax_type_acronym');
            $table->renameColumn('tax_type_id', 'tax_type_id_fallback');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('providers_per_sap_company_forced_tax_type', function (Blueprint $table) {
            $table->renameColumn('tax_type_id_fallback', 'tax_type_id');
        });
    }
}
