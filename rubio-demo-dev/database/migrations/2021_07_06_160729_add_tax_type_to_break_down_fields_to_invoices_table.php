<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddTaxTypeToBreakDownFieldsToInvoicesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('invoices', function (Blueprint $table) {
            $table->string('tax_type_2', 16)->nullable()->after('tax_rate_2');
            $table->string('tax_type_3', 16)->nullable()->after('tax_rate_3');
            $table->string('tax_type_4', 16)->nullable()->after('tax_rate_4');
            $table->string('tax_type_5', 16)->nullable()->after('tax_rate_5');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('invoices', function (Blueprint $table) {
            $table->dropColumn(['tax_type_2', 'tax_type_3', 'tax_type_4', 'tax_type_5']);
        });
    }
}
