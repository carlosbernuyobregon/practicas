<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEmailAttachInvoiceTrackingTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('email_attach_invoice_tracking', function (Blueprint $table) {
            $table->id();
            $table->string('message_uuid');
            $table->string('email_uuid')->nullable();
            $table->json('attachments_id')->nullable();
            $table->json('invoices_id')->nullable();
            $table->json('sent_to_ocr_by_invoice_id')->nullable();
            $table->json('failed_sent_to_ocr_by_invoice_id')->nullable();
            $table->boolean('email_double_check')->default(0);
            $table->boolean('email_seen')->default(0);
            $table->boolean('email_moved')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('email_attach_invoice_tracking');
    }
}
