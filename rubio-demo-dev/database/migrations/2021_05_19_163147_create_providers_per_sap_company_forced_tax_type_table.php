<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProvidersPerSapCompanyForcedTaxTypeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('providers_per_sap_company_forced_tax_type', function (Blueprint $table) {
            $table->uuid('id')->primary();
            $table->string('sap_company');
            $table->string('tax_type_acronym');
            $table->string('tax_type_qualification');
            $table->string('tax_type_id')->nullable();
            $table->string('provider_vat_number');
            $table->string('provider_vat_account');
            $table->string('provider_name');
//            $table->unique(['sap_company', 'provider_vat_number'], 'sap_company_provider_vat_number_unique');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('providers_per_sap_company_forced_tax_type');
    }
}
