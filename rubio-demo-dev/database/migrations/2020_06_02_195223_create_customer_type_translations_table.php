<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCustomerTypeTranslationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('customer_type_translations', function (Blueprint $table) {
            $table->uuid('id')->primary();
            $table->uuid('customer_type_id');
            $table->string('name');
            $table->string('description');
            $table->string('locale')->index();
            $table->timestamps();
            $table->unique(['customer_type_id', 'locale']);
            $table->foreign('customer_type_id')->references('id')->on('customer_types')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('customer_type_translations');
    }
}
