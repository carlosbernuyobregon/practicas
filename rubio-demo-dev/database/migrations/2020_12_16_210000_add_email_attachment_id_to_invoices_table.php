<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddEmailAttachmentIdToInvoicesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('invoices', function (Blueprint $table) {
            //
            $table->uuid('email_attachment_id')->nullable();
            $table->boolean('sent_to_ocr')->default(false);
            $table->boolean('sent_to_certify')->default(false);
            $table->boolean('ocr_processed')->default(false);
            $table->boolean('certify_generated')->default(false);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('invoices', function (Blueprint $table) {
            $table->dropColumn(['email_attachment_id','sent_to_ocr','sent_to_certify','ocr_processed','certify_generated']);
        });
    }
}
