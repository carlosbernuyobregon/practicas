<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddQtyAndStatusFieldsToDeliveriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('deliveries', function (Blueprint $table) {
            $table->string('external_id')->nullable()->after('amount');
            $table->integer('status')->nullable()->after('amount');
            $table->float('qty',16,4)->nullable()->after('amount');
            $table->dateTime('sap_updated_at')->nullable()->after('delivered_at');
            $table->float('amount',16,4)->nullable()->change();
            $table->dropColumn('sent_to_sap_at');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('deliveries', function (Blueprint $table) {
            $table->dropColumn(['status', 'qty', 'external_id', 'sap_updated_at']);
            $table->dateTime('sent_to_sap_at')->nullable()->after('delivered_at');
        });
    }
}
