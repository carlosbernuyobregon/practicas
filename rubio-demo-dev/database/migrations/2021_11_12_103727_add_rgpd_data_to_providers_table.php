<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddRgpdDataToProvidersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('providers', function (Blueprint $table) {
            $table->string('rgpd_name')->nullable();
            $table->string('rgpd_last_name')->nullable();
            $table->string('rgpd_dni')->nullable();
            $table->string('rgpd_email')->nullable();
            $table->string('rgpd_role')->nullable();
            $table->uuid('rgpd_user_id')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('providers', function (Blueprint $table) {
            $table->dropColumn([
                'rgpd_name',
                'rgpd_last_name',
                'rgpd_dni',
                'rgpd_email',
                'rgpd_role',
                'rgpd_user_id',
            ]);
        });
    }
}
