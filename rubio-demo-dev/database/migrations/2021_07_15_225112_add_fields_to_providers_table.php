<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddFieldsToProvidersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('providers', function (Blueprint $table) {
            $table->string('company')->nullable();
            $table->string('payment_condition')->nullable();
            $table->string('payment_type')->nullable();
            $table->string('external_id')->nullable();
            $table->string('country')->nullable()->change();
            $table->uuid('supplier_id')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('providers', function (Blueprint $table) {
            $table->dropColumn(['company','payment_condition','payment_type','external_id']);
        });
    }
}
