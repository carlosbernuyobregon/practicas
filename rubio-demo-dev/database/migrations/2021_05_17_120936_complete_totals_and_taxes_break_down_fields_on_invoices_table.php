<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CompleteTotalsAndTaxesBreakDownFieldsOnInvoicesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('invoices', function (Blueprint $table) {
            $table->float('total_amount_2',16,4)->nullable()->after('tax_amount_2');
            $table->float('total_amount_3',16,4)->nullable()->after('tax_amount_3');
            $table->float('total_amount_4',16,4)->nullable()->after('tax_amount_4');
            $table->float('total_amount_5',16,4)->nullable()->after('tax_amount_5');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('invoices', function (Blueprint $table) {
            $table->dropColumn([
                'total_amount_2', 'total_amount_3', 'total_amount_4', 'total_amount_5'
            ]);
        });
    }
}
