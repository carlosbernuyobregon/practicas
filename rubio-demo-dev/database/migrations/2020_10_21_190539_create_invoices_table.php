<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateInvoicesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('invoices', function (Blueprint $table) {
            $table->uuid('id')->primary();
            $table->string('sap_company')->nullable();
            $table->string('invoice_number')->nullable();
            $table->string('lotus_id')->nullable();
            $table->string('taxes_id')->nullable();
            $table->string('customer_name')->nullable();
            $table->date('date')->nullable();
            $table->string('document_type')->nullable();
            $table->string('vat_number')->nullable();
            $table->string('provider_name')->nullable();
            $table->string('provider_country')->nullable();
            $table->string('intern_order')->nullable();
            $table->string('payment_conditions')->nullable();
            $table->date('payment_date')->nullable();
            $table->string('intern_vat_number')->nullable();
            $table->float('total',16,4)->nullable();
            $table->float('total_net',16,4)->nullable();
            $table->float('taxes_total_amount',16,4)->nullable();
            $table->string('taxes_percent')->nullable();
            $table->string('taxes_type')->nullable();
            $table->float('taxes_irpf_amount',16,4)->nullable();
            $table->float('taxes_other_amount',16,4)->nullable();
            $table->string('currency')->nullable();
            $table->integer('status')->default(0);
            $table->string('file')->nullable();
            $table->float('exchange_type',16,4,true)->default(0.0);
            $table->string('sap_error_code')->nullable();
            $table->string('sap_error_description')->nullable();
            $table->string('sap_document_number')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('invoices');
    }
}
