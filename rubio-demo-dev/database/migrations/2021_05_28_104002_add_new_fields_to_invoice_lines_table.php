<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddNewFieldsToInvoiceLinesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('invoice_lines', function (Blueprint $table) {
            $table->float('discount_rate',8,2)->nullable()->after('discount_amount');
            $table->string('qty_measuring_unit')->nullable();
            $table->string('unit_price_measuring_unit')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('invoice_lines', function (Blueprint $table) {
            $table->dropColumn(['discount_rate', 'qty_measuring_unit', 'unit_price_measuring_unit']);
        });
    }
}
