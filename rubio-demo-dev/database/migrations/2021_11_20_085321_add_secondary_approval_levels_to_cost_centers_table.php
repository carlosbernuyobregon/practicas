<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddSecondaryApprovalLevelsToCostCentersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('cost_centers', function (Blueprint $table) {
            $table->float('amount_level_secondary_1',8,2,true)->default(0);
            $table->float('amount_level_secondary_2',8,2,true)->default(0);
            $table->float('amount_level_secondary_3',8,2,true)->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('cost_centers', function (Blueprint $table) {
            $table->dropColumn(['amount_level_secondary_1','amount_level_secondary_2','amount_level_secondary_3']);
        });
    }
}
