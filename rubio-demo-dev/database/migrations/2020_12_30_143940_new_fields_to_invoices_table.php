<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class NewFieldsToInvoicesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('invoices', function (Blueprint $table) {
            $table->string('delivery_amount',16,4)->nullable();
            $table->string('total_net_2',16,4)->nullable();
            $table->string('tax_rate_2',16,4)->nullable();
            $table->string('tax_amount_2',16,4)->nullable();
            $table->string('total_net_3',16,4)->nullable();
            $table->string('tax_rate_3',16,4)->nullable();
            $table->string('tax_amount_3',16,4)->nullable();
            $table->string('total_net_4',16,4)->nullable();
            $table->string('tax_rate_4',16,4)->nullable();
            $table->string('tax_amount_4',16,4)->nullable();
            $table->string('total_net_5',16,4)->nullable();
            $table->string('tax_rate_5',16,4)->nullable();
            $table->string('tax_amount_5',16,4)->nullable();
            $table->string('bank_code_number')->nullable();
            $table->string('bank_iban')->nullable();
            $table->float('discount_amount',16,4)->nullable();
        });;
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('invoices', function (Blueprint $table) {
            $table->dropColumn(['delivery_amount','total_net_2','tax_rate_2','tax_amount_2','total_net_3','tax_rate_3','tax_amount_3','total_net_4','tax_rate_4','tax_amount_4','total_net_5','tax_rate_5','tax_amount_5','bank_code_number','bank_iban','discount_amount']);
        });
    }
}
