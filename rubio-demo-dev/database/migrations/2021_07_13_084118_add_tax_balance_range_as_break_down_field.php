<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddTaxBalanceRangeAsBreakDownField extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('invoices', function (Blueprint $table) {
            $table->integer('tax_balance_range_2')->nullable()->default(0)->after('tax_type_2');
            $table->integer('tax_balance_range_3')->nullable()->default(0)->after('tax_type_3');
            $table->integer('tax_balance_range_4')->nullable()->default(0)->after('tax_type_4');
            $table->integer('tax_balance_range_5')->nullable()->default(0)->after('tax_type_5');

            $table->float('adjusted_tax_amount_2', 16,4)->nullable()->default(0)->after('tax_balance_range_2');
            $table->float('adjusted_tax_amount_3', 16,4)->nullable()->default(0)->after('tax_balance_range_3');
            $table->float('adjusted_tax_amount_4', 16,4)->nullable()->default(0)->after('tax_balance_range_4');
            $table->float('adjusted_tax_amount_5', 16,4)->nullable()->default(0)->after('tax_balance_range_5');

            $table->dropColumn('tax_balance_range');
            $table->float('tax_balance_total', 16,4)->nullable()->default(0)->after('taxes_irpf_amount');
            $table->float('taxes_total_amount_without_adjustment',16,4)->nullable();

            $table->float('adjusted_total_amount_2', 16,4)->nullable()->default(0)->after('total_amount_2');
            $table->float('adjusted_total_amount_3', 16,4)->nullable()->default(0)->after('total_amount_3');
            $table->float('adjusted_total_amount_4', 16,4)->nullable()->default(0)->after('total_amount_4');
            $table->float('adjusted_total_amount_5', 16,4)->nullable()->default(0)->after('total_amount_5');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('invoices', function (Blueprint $table) {
            $table->dropColumn(['tax_balance_range_2', 'tax_balance_range_3', 'tax_balance_range_4', 'tax_balance_range_5']);
            $table->dropColumn(['adjusted_tax_amount_2', 'adjusted_tax_amount_3', 'adjusted_tax_amount_4', 'adjusted_tax_amount_5']);
            $table->integer('tax_balance_range')->nullable()->default(0)->after('taxes_irpf_amount');
            $table->dropColumn('tax_balance_total');
            $table->dropColumn('taxes_total_amount_without_adjustment');
            $table->dropColumn(['adjusted_total_amount_2', 'adjusted_total_amount_3', 'adjusted_total_amount_4', 'adjusted_total_amount_5']);
        });
    }
}
