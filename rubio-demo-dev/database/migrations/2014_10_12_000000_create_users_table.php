<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->uuid('id')->primary();
            $table->string('name');
            $table->string('phone')->nullable();
            $table->string('first_name')->default('');
            $table->string('last_name')->default('');
            $table->string('active')->default(true);
            $table->string('blocked')->default(false);
            $table->string('email')->unique();
            $table->timestamp('email_verified_at')->nullable();
            $table->string('password');
            $table->string('timezone')->default('Europe/Madrid');
            $table->uuid('country_id')->nullable();
            $table->string('date_format')->default('d/m/YY');
            $table->string('numbers_format')->default('decimals');
            $table->string('default_locale',2)->default('en');
            $table->uuid('company_id')->nullable();
            $table->string('samaccountname')->nullable();
            $table->string('guid')->nullable();
            $table->string('domain')->nullable();
            $table->string('adpwd')->nullable();
            $table->softDeletes();
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
