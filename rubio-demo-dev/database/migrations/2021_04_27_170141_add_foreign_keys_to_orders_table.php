<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddForeignKeysToOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('orders', function (Blueprint $table) {
/*
$table->foreign('customer_id')->references('id')->on('customers')->nullOnDelete();
            $table->foreign('material_id')->references('id')->on('materials')->nullOnDelete();
            $table->foreign('supplier_id')->references('id')->on('suppliers')->nullOnDelete();
            */
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('orders', function (Blueprint $table) {
            /*
            $table->dropForeign('orders_customer_id_foreign');
            $table->dropForeign('orders_material_id_foreign');
            $table->dropForeign('orders_supplier_id_foreign');
            */
        });
    }
}
