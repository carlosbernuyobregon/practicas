<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateInvoiceOriginalOcrDataTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('invoice_original_ocr_data', function (Blueprint $table) {
            $table->id('id');
            $table->uuid('invoice_id');
            $table->integer('status')->default(0);
            $table->double('total_net', 16, 4)->nullable();
            $table->double('taxes_irpf_amount', 16, 4)->nullable();
            $table->double('taxes_total_amount', 16, 4)->nullable();
            $table->double('total', 16, 4)->nullable();
            $table->string('taxes_percent')->nullable();
            $table->string('filename')->nullable();
            $table->string('sap_company')->nullable();
            $table->string('invoice_number')->nullable();
            $table->string('lotus_id')->nullable();
            $table->string('taxes_id')->nullable();
            $table->string('customer_name')->nullable();
            $table->date('date')->nullable();
            $table->date('operation_date')->nullable();
            $table->string('document_type')->nullable();
            $table->string('vat_number')->nullable();
            $table->string('provider_name')->nullable();
            $table->string('provider_country')->nullable();
            $table->string('intern_order')->nullable();
            $table->string('payment_conditions')->nullable();
            $table->date('payment_date')->nullable();
            $table->string('intern_vat_number')->nullable();
            $table->string('taxes_type')->nullable();
            $table->double('taxes_other_amount', 16, 4)->nullable();
            $table->string('currency')->nullable();
            $table->string('duplicated_from')->nullable();
            $table->string('file')->nullable();
            $table->double('exchange_type', 16, 4)->unsigned()->default(0.0000);
            $table->string('sap_error_code')->nullable();
            $table->string('sap_error_description')->nullable();
            $table->string('sap_document_number')->nullable();
            $table->char('email_attachment_id', 36)->nullable();
            $table->tinyInteger('sent_to_ocr')->default(0);
            $table->tinyInteger('sent_to_certify')->default(0);
            $table->tinyInteger('ocr_processed')->default(0);
            $table->tinyInteger('certify_generated')->default(0);
            $table->string('department')->nullable();
            $table->string('comercial_sales')->nullable();
            $table->char('customer_id', 36)->nullable();
            $table->string('delivery_amount', 16)->nullable();
            $table->string('total_net_2', 16)->nullable();
            $table->string('tax_rate_2', 16)->nullable();
            $table->string('tax_amount_2', 16)->nullable();
            $table->string('total_net_3', 16)->nullable();
            $table->string('tax_rate_3', 16)->nullable();
            $table->string('tax_amount_3', 16)->nullable();
            $table->string('total_net_4', 16)->nullable();
            $table->string('tax_rate_4', 16)->nullable();
            $table->string('tax_amount_4', 16)->nullable();
            $table->string('total_net_5', 16)->nullable();
            $table->string('tax_rate_5', 16)->nullable();
            $table->string('tax_amount_5', 16)->nullable();
            $table->string('bank_code_number')->nullable();
            $table->string('bank_iban')->nullable();
            $table->double('discount_amount', 16, 4)->nullable();
            $table->longText('comments')->nullable();
            $table->dateTime('sap_update')->nullable();
            $table->string('provider_street')->nullable();
            $table->string('provider_city')->nullable();
            $table->string('provider_postal_code')->nullable();
            $table->string('provider_state')->nullable();
            $table->dateTime('downloaded_at')->nullable();
            $table->tinyInteger('is_correct')->nullable()->default(0);
            $table->dateTime('processed_at')->nullable();
            $table->dateTime('contabilized_at')->nullable();
            $table->string('aws_route')->nullable();
            $table->text('ocr_reference')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('invoice_original_ocr_data');
    }
}
