<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrderLinesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('order_lines', function (Blueprint $table) {
            $table->uuid('id')->primary();
            $table->foreignUuid('customer_id');
            $table->foreignUuid('order_id');
            $table->string('company')->nullable();
            $table->longText('description')->nullable();
            $table->integer('position')->default(10);
            $table->uuid('material_id')->nullable();
            $table->string('material_name')->nullable();
            $table->foreignUuid('imputation_type_id')->nullable();
            $table->foreignUuid('location_id')->nullable();
            $table->foreignUuid('tax_type_id')->nullable();
            $table->string('tax_type')->nullable();
            $table->float('net_amount',16,4)->default(0.0);
            $table->float('total_amount',16,4)->default(0.0);
            $table->integer('qty')->default(0);
            $table->foreignUuid('accounting_account_id')->nullable();
            $table->foreignUuid('cost_center_id')->nullable();
            $table->foreignUuid('pep_element_id')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('order_lines');
    }
}
