<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddFieldsToOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('orders', function (Blueprint $table) {
            $table->longText('description')->nullable();
            $table->string('order_number')->nullable();
            $table->date('date')->nullable();
            $table->string('buyer')->nullable();
            $table->string('purchasing_group')->nullable();
            $table->string('document_type')->nullable();
            $table->float('net_amount',16,4)->nullable();
            $table->float('total_amount',16,4)->nullable();
            $table->integer('status')->default(0);
            $table->boolean('active')->default(true);

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('orders', function (Blueprint $table) {
            $table->dropColumn([
                'description',
                'order_number',
                'buyer',
                'purchasing_group',
                'document_type',
                'net_amount',
                'total_amount',
                'status',
                'active'
            ]);
        });
    }
}
