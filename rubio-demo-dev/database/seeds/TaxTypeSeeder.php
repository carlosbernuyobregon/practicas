<?php

use App\TaxType;
use Illuminate\Database\Seeder;

class TaxTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $taxes = array(
            0 => array('tipo' => 'AG', 'desc' =>'IVA soportado Agencias Viajes', 'percentage' => 0),
            1 => array('tipo' => 'A0', 'desc' =>'IVA soportado adquisición bienes CEE 0%', 'percentage' => 0),
            2 => array('tipo' => 'A1', 'desc' =>'IVA soportado adquisición bienes CEE 4%', 'percentage' => 4),
            3 => array('tipo' => 'A2', 'desc' =>'IVA soportado adquisición bienes CEE 10%', 'percentage' => 10),
            4 => array('tipo' => 'A3', 'desc' =>'IVA soportado adquisición bienes CEE 21%', 'percentage' => 21),
            5 => array('tipo' => 'A4', 'desc' =>'IVA soportado adq. bienes inv. CEE 0%', 'percentage' => 0),
            6 => array('tipo' => 'A5', 'desc' =>'IVA soportado adq. bienes inv. CEE 4%', 'percentage' => 4),
            7 => array('tipo' => 'A6', 'desc' =>'IVA soportado adq. bienes inv. CEE 10%', 'percentage' => 10),
            8 => array('tipo' => 'A7', 'desc' =>'IVA soportado adq. bienes inv. CEE 21%', 'percentage' => 21),
            9 => array('tipo' => 'I0', 'desc' =>'IVA soportado importación 0%', 'percentage' => 0),
            10 => array('tipo' => 'I1', 'desc' =>'IVA soportado importación 4%', 'percentage' => 4),
            11 => array('tipo' => 'I2', 'desc' =>'IVA soportado importación 10%', 'percentage' => 10),
            12 => array('tipo' => 'I3', 'desc' =>'IVA soportado importación 21%', 'percentage' => 21),
            13 => array('tipo' => 'I4', 'desc' =>'IVA diferido importación 4%', 'percentage' => 4),
            14 => array('tipo' => 'I5', 'desc' =>'IVA diferido importación 10%', 'percentage' => 10),
            15 => array('tipo' => 'I6', 'desc' =>'IVA diferido importación 21%', 'percentage' => 21),
            16 => array('tipo' => 'J0', 'desc' =>'IVA soportado adq. bienes inv. terceros países 0%', 'percentage' => 0),
            17 => array('tipo' => 'J1', 'desc' =>'IVA soportado adq. bienes inv. terceros países 4%', 'percentage' => 4),
            18 => array('tipo' => 'J2', 'desc' =>'IVA soportado adq. bienes inv. terceros países 10%', 'percentage' => 10),
            19 => array('tipo' => 'J3', 'desc' =>'IVA soportado adq. bienes inv. terceros países 21%', 'percentage' => 21),
            20 => array('tipo' => 'N1', 'desc' =>'IVA no deducible 4%', 'percentage' => 4),
            21 => array('tipo' => 'N2', 'desc' =>'IVA no deducible 10%', 'percentage' => 10),
            22 => array('tipo' => 'N3', 'desc' =>'IVA no deducible 21%', 'percentage' => 21),
            23 => array('tipo' => 'P0', 'desc' =>'IVA soportado servicios CEE 0%', 'percentage' => 0),
            24 => array('tipo' => 'P1', 'desc' =>'IVA soportado servicios CEE 4%', 'percentage' => 4),
            25 => array('tipo' => 'P2', 'desc' =>'IVA soportado servicios CEE 10%', 'percentage' => 10),
            26 => array('tipo' => 'P3', 'desc' =>'IVA soportado servicios CEE 21%', 'percentage' => 21),
            27 => array('tipo' => 'P4', 'desc' =>'IVA Sujeto Pasivo en Operaciones Interiores 4%', 'percentage' => 4),
            28 => array('tipo' => 'P5', 'desc' =>'IVA Sujeto Pasivo en Operaciones Interiores 10%', 'percentage' => 10),
            29 => array('tipo' => 'P6', 'desc' =>'IVA Sujeto Pasivo en Operaciones Interiores 21%', 'percentage' => 21),
            30 => array('tipo' => 'Q0', 'desc' =>'IVA soportado ISP servicios terceros países 0%', 'percentage' => 0),
            31 => array('tipo' => 'Q1', 'desc' =>'IVA soportado ISP servicios terceros países 4%', 'percentage' => 4),
            32 => array('tipo' => 'Q2', 'desc' =>'IVA soportado ISP servicios terceros países 10%', 'percentage' => 10),
            33 => array('tipo' => 'Q3', 'desc' =>'IVA soportado ISP servicios terceros países 21%', 'percentage' => 21),
            34 => array('tipo' => 'S0', 'desc' =>'IVA soportado operación interior sujeto exento', 'percentage' => 0),
            35 => array('tipo' => 'S1', 'desc' =>'IVA soportado operación interior 4%', 'percentage' => 4),
            36 => array('tipo' => 'S2', 'desc' =>'IVA soportado operación interior 10%', 'percentage' => 10),
            37 => array('tipo' => 'S3', 'desc' =>'IVA soportado operación interior 21%', 'percentage' => 21),
            38 => array('tipo' => 'S4', 'desc' =>'IVA soportado adq. bienes inv. nacional 0%', 'percentage' => 0),
            39 => array('tipo' => 'S5', 'desc' =>'IVA soportado adq. bienes inv. nacional 4%', 'percentage' => 4),
            40 => array('tipo' => 'S6', 'desc' =>'IVA soportado adq. bienes inv. nacional 10%', 'percentage' => 10),
            41 => array('tipo' => 'S7', 'desc' =>'IVA soportado adq. bienes inv. nacional 21%', 'percentage' => 21),
            42 => array('tipo' => 'V0', 'desc' =>'IVA soportado tracto sucesivo op.int 0%', 'percentage' => 0),
            43 => array('tipo' => 'V1', 'desc' =>'IVA soportado tracto sucesivo op.int 4%', 'percentage' => 4),
            44 => array('tipo' => 'V2', 'desc' => 'IVA soportado tracto sucesivo op.int 10%', 'percentage' => 10),
            45 => array('tipo' => 'V3', 'desc' => 'IVA soportado tracto sucesivo op.int 21%', 'percentage' => 21),
            46 => array('tipo' => 'ZS', 'desc' =>'IVA soportado operaciones no sujetas', 'percentage' => 0),
        );
        foreach($taxes as $tax){
            TaxType::create([
                'type' => $tax['tipo'],
                'description' => $tax['desc'],
                'percentage' => $tax['percentage']
            ]);
        }

    }
}
