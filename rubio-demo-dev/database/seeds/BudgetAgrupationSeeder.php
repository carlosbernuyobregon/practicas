<?php

use App\Budget;
use App\Company;
use App\Customer;
use App\Models\BudgetAgrupation;
use App\Role;
use Illuminate\Database\Seeder;

class BudgetAgrupationSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        BudgetAgrupation::truncate();

        $agrupations = [
            'Viajes',
            'Otros'
        ];
        $travelBudgets = [
            'Gastos Viajes, Hoteles etc..',
            'Gastos viajes',
            'Viajes',
            'Viajes, parking y Kms',
            'Viaje varios'
        ];
        $customer = Customer::where('email','info@rubio.com')->value('id');
        $company = Company::where('customer_id',$customer)
                        ->where('code',1000)
                        ->value('id');
        $role = Role::where('key_value','marketing')->first();
        if(!$role){
            $newRole = Role::create([
                'key_value'             => 'marketing',
                'customer_id'           => $customer,
                'es'                    => [
                    'name'              => 'Marketing',
                    'description'       => 'Perfil Marketing'
                ],
                'en'                    => [
                    'name'              => 'Marketing',
                    'description'       => 'Marketing profile'
                ]
            ]);
        }

        /*
        foreach($agrupations as $item){
            $budgetAgrupation = BudgetAgrupation::create([
                                    'customer_id'       => $customer,
                                    'company_id'        => $company,
                                    'name'              => $item,
                                    'others'            => ($item == 'Otros' ? true : false)
                                ]);
            if($item == 'Viajes'){
                foreach($travelBudgets as $budgetsNames){
                    $budgets = Budget::where('name','like',$budgetsNames)->get();
                    foreach($budgets as $b){
                        $b->budget_agrupation_id = (string)$budgetAgrupation->id;
                        $b->save();
                    }
                }
            }
        }
        */
    }
}
