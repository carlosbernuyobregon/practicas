<?php

use App\Invoice;
use App\InvoiceLine;
use Illuminate\Database\Seeder;

class InvoiceTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            '0' => [
                'sap_company' => '1000',
                'invoice_number' => 'SI217049201',
                'customer_name' => 'Laboratorios Rubio, S.A.',
                'date' => '2020/11/05',
                'document_type' => 'invoice',
                'vat_number' => 'ESA61744033',
                'provider_name' => 'Azelis España S.A.U.',
                'provider_country' => 'Spain',
                'intern_order' => '4500004904',
                'payment_conditions' => '60 days',
                'payment_date' => '2020/11/05',
                'intern_vat_number' => 'ESA08222465',
                'total' => '41745.00',
                'total_net' => '34500.00',
                'taxes_total_amount' => '7245.00',
                'taxes_percent' => '21%',
                'taxes_irpf_amount' => '0',
                'taxes_id' => 'S3',
                'taxes_other_amount' => '0',
                'status' => 1,
                'file' => '4500004904_AZELIS.pdf',
                'currency' => 'EUR'
            ],
            '1' => [
                'sap_company' => '1000',
                'invoice_number' => '5820218',
                'lotus_id' => 'MP-00238/201',
                'customer_name' => 'Laboratorios Rubio, S.A.',
                'date' => '2020/11/05',
                'document_type' => 'invoice',
                'vat_number' => 'ESW0065869J',
                'provider_name' => 'PUROLITE (INT.) LTD.',
                'provider_country' => 'Spain',
                'intern_order' => '4500004985',
                'payment_conditions' => 'pagaré',
                'payment_date' => '2020/11/05',
                'intern_vat_number' => 'ESA08222465',
                'total' => '206184.00',
                'total_net' => '170400.00',
                'taxes_total_amount' => '35784.00',
                'taxes_percent' => '21%',
                'taxes_irpf_amount' => '0',
                'taxes_other_amount' => '0',
                'status' => 1,
                'file' => '4500004985_Purolite.pdf',
                'currency' => 'EUR',
                'taxes_id' => 'S3',
            ],
            '2' => [
                'sap_company' => '1000',
                'invoice_number' => 'SI217049201',
                'customer_name' => 'Laboratorios Rubio, S.A.',
                'date' => '2020/11/05',
                'document_type' => 'invoice',
                'vat_number' => 'ESA61744033',
                'provider_name' => 'Azelis España S.A.U.',
                'provider_country' => 'Spain',
                'intern_order' => '4500005271',
                'payment_conditions' => 'pagaré',
                'payment_date' => '2020/11/05',
                'intern_vat_number' => 'ESA08222465',
                'total' => '206184.00',
                'total_net' => '170400.00',
                'taxes_total_amount' => '35784.00',
                'taxes_percent' => '21%',
                'taxes_irpf_amount' => '0',
                'taxes_other_amount' => '0',
                'status' => 1,
                'file' => '4500005271_Purolite.pdf',
                'currency' => 'EUR',
                'taxes_id' => 'S3',
            ],
        ];
        $lines = [
            '0' => [
                '0' => [
                    'material_code' => '142465-DRPL000050KG0(M35222300)',
                    'material_description' => 'HYDRALAZINE HCL EP<br>HIDRALACINA HCL EP GEN<br>50 Kg Plastico tampor/bidon (3.00)<br>HYDRALAZINE HCL EP<br>Peso Bruto :165.00<br>Commodity/Taric code: 29339980SDN217050535 Fecha de embarque29/06/2020 150.00kg<br>Nº Lote : 003659<br>País de Origen:JPN',
                    'material_qty' => '150',
                    'packaging_type' => '',
                    'unit_price' => '230',
                    'currency' => 'EUR',
                    'taxes' => '21%',
                    'net_amount' => '34500.00',
                    'irpf' => '0',
                    'total_amount' => '41745.00',
                    'taxes_id' => 'S3',
                ]
            ],
            '1' => [
                '0' => [
                    'material_code' => '58099',
                    'material_description' => 'A430MR',
                    'material_qty' => '6000',
                    'packaging_type' => 'BI',
                    'unit_price' => '28.40',
                    'currency' => 'EUR',
                    'taxes' => '21%',
                    'net_amount' => '170400.00',
                    'irpf' => '0',
                    'total_amount' => '35784.00',
                    'taxes_id' => 'S3',
                ]
            ],
            '2' => [
                '0' => [
                    'material_code' => '58214',
                    'material_description' => 'A430MR',
                    'material_qty' => '6000',
                    'packaging_type' => 'BI',
                    'unit_price' => '28.40',
                    'currency' => 'EUR',
                    'taxes' => '21%',
                    'net_amount' => '170400.00',
                    'irpf' => '0',
                    'total_amount' => '35784.00',
                    'taxes_id' => 'S3',
                ]
            ],
        ];
        $i = 0;
        foreach($data as $invoice){
//            dd($invoice);
            $invoiceData = Invoice::create($invoice);
            $l = 0;
            foreach ($lines[$i] as $line){
//                dd($line);
                $line['invoice_id'] = $invoiceData->id;
                InvoiceLine::create($line);
                $l++;
            }
            $i++;
        }
    }
}
