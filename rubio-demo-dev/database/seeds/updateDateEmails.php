<?php

use App\Email;
use App\EmailAttachment;
use App\Invoice;
use App\Readsoft;
use Carbon\Carbon;
use Webklex\PHPIMAP\ClientManager;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use Illuminate\Database\Seeder;

class updateDateEmails extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $cm = new ClientManager();

        $client = $cm->make([
            'host'          =>  config('custom.email_integration.host'),
            'port'          => config('custom.email_integration.port'),
            'encryption'    => config('custom.email_integration.encryption'),
            'validate_cert' => config('custom.email_integration.validate_cert'),
            'username'      => config('custom.email_integration.username'),
            'password'      => config('custom.email_integration.password'),
            'protocol'      => config('custom.email_integration.protocol')
        ]);
        $client->connect();
        $folders = $client->getFolders();

        $folder = $client->getFolder('INBOX');
        $messages = $folder->query()->all()->limit(20)->get();
        $i = 0;
        foreach ($messages as $message) {
            $existingEmail = Email::where('email_uid', (string)$message->getUid())->where('double_check', true)->first();

            if ($existingEmail == null) {
                $i++;
                $existingEmail = Email::where('email_uid', (string)$message->getUid())->first();

                if ($existingEmail == null) {
                    try {
                        $to = (string)$message->getTo()[0]->mail;
                    } catch (\Throwable $th) {
                        $to = 'not available';
                    }
                    try {
                        $date = $message->getAttributes()['date'][0];
                    } catch (\Throwable $th) {
                        $date = now();
                    }
                    $data = [
                        'email_uid' => (string)$message->getUid() ?? '',
                        'email' => utf8_encode((string)$message->getMessageId()) ?? '',
                        'from' => utf8_encode((string)$message->getFrom()[0]->mail) ?? '',
                        'attachments' => $message->getAttachments()->count() ?? 0,
                        'to' => $to,
                        'subject' => utf8_encode((string)$message->getSubject()) ?? '',
                        'email_date' => $date,
                    ];
                    try {
                        $email = Email::create($data);
                    } catch (\Exception $th) {
                        dd($th);
                    }
                } else {
                    try {
                        $date = $message->getAttributes()['date'][0];
                    } catch (\Throwable $th) {
                        $date = now();
                    }
                    $existingEmail->double_check = true;
                    $existingEmail->email_date = $date;
                    $existingEmail->save();
                    $email = $existingEmail;
                }

                //dd($email);
                if (($email != null) && ($message->getAttachments()->count() > 0)) {
                    $attachmentNumber = EmailAttachment::where('email_id', $email->id)->count();
                    if ($attachmentNumber == 0) {
                        $attachments = $message->getAttachments();
                        $localPath = sys_get_temp_dir() . '/';
                        $dateString  = Carbon::now()->format('Y/m/d');
                        $awsPath = config('custom.iworking_public_bucket_folder_invoices') . '/' . $dateString . '/' . (string)$email->id;
                        $attachments = $message->getAttachments();
                        foreach ($attachments as $attachment) {
                            $newFilename = Str::uuid() . '.' . $attachment->getExtension();
                            $filePath = $localPath . $newFilename;
                            $attachment->save($localPath, $newFilename);
                            $awsPathWithName = $awsPath . '/' . $newFilename;
                            $awsFileName = Storage::disk('s3')->putFileAs($awsPath, $filePath, $newFilename);
                            if (File::exists($filePath)) {
                                File::delete($filePath);
                            }
                            $attachNewDoc = EmailAttachment::create([
                                'email_id' => (string)$email->id,
                                'original_filename' => (string)$attachment->getName(),
                                'filename' => $newFilename,
                                'filetype' => (string)$attachment->getContentType(),
                                'aws_route' => $awsFileName
                            ]);
                            $invoice = Invoice::create([
                                'email_attachment_id' => (string)$attachNewDoc->id,
                                'downloaded_at' => now(),
                                'status' => 0,
                                'filename' => $newFilename,
                                'aws_route' => $awsFileName
                            ]);
                            $readsoft = new Readsoft();
                            $connect = $readsoft->connect();
                            if ($connect->getStatusCode() == 200) {
                                try {
                                    $readsoft->sendFile($awsFileName, (string)$attachNewDoc->id, $awsPath, $attachNewDoc->filetype);
                                } catch (\Throwable $th) {
                                    echo 'document error: ' . (string)$attachment->getContentType();
                                    echo "\n";
                                }
                            } else {
                                echo 'Readsoft connection error.';
                                die();
                            }
                            $invoice->sent_to_ocr = true;
                            $invoice->save();
                        }
                    }
                }
                $message->setFlag('Seen');
            }else{
                $message->setFlag('Seen');
                if($message->move('INBOX.Processed') == true){
                    echo 'Message has ben moved';
                    echo "\n";
                }else{
                    echo 'Message could not be moved';
                    echo "\n";
                }
                sleep(5);
            }
        }
        $invoices = $i;
        echo $invoices;
    }
}
