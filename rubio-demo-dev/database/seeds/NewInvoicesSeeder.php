<?php

use App\Invoice;
use App\InvoiceLine;
use Illuminate\Database\Seeder;

class NewInvoicesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            '0' => [
                'sap_company' => '1000',
                'invoice_number' => '0020001503',
                'customer_name' => 'Laboratorios Rubio, S.A.',
                'date' => '2020/10/20',
                'document_type' => '1',
                'vat_number' => 'B59939959',
                'provider_name' => 'CARRETILLAS TERRASSA, S.L.',
                'provider_country' => 'ES',
                'intern_order' => '0000959',
                'payment_conditions' => 'Domiciliación',
                'payment_date' => '2020/12/25',
                'intern_vat_number' => 'ESA08222465',
                'total' => '393.25',
                'total_net' => '325.00',
                'taxes_total_amount' => '68.25',
                'taxes_percent' => '21%',
                'taxes_irpf_amount' => '0',
                'taxes_id' => 'S3',
                'taxes_other_amount' => '0',
                'status' => 1,
                'file' => 'Fac0020001503.pdf',
                'currency' => 'EUR'
            ],
            '1' => [
                'sap_company' => '1000',
                'invoice_number' => '0092021600',
                'lotus_id' => '',
                'customer_name' => 'Laboratorios Rubio, S.A.',
                'date' => '2020/10/23',
                'document_type' => '1',
                'vat_number' => 'ESW0065869J',
                'provider_name' => 'QUIMIDROGA S.A.',
                'provider_country' => 'ES',
                'intern_order' => '4500006117',
                'payment_conditions' => 'Transferencia 60 DIAS',
                'payment_date' => '2020/12/23',
                'intern_vat_number' => 'ESA08222465',
                'total' => '1694.00',
                'total_net' => '1400.00',
                'taxes_total_amount' => '294.00',
                'taxes_percent' => '21%',
                'taxes_irpf_amount' => '0',
                'taxes_other_amount' => '0',
                'status' => 1,
                'file' => '0092021600.pdf',
                'currency' => 'EUR',
                'taxes_id' => 'S3',
            ],
            '2' => [
                'sap_company' => '1000',
                'invoice_number' => 'F / 20201632',
                'customer_name' => 'Laboratorios Rubio, S.A.',
                'date' => '2020/10/27',
                'document_type' => '1',
                'vat_number' => 'A08786014',
                'provider_name' => 'Graficas Maculart, S.A.',
                'provider_country' => 'ES',
                'intern_order' => '',
                'payment_conditions' => 'Transferencia',
                'payment_date' => '2021/01/25',
                'intern_vat_number' => 'ESA08222465',
                'total' => '363.00',
                'total_net' => '300.00',
                'taxes_total_amount' => '63.00',
                'taxes_percent' => '21%',
                'taxes_irpf_amount' => '0',
                'taxes_other_amount' => '0',
                'status' => 1,
                'file' => 'F20201632.pdf',
                'currency' => 'EUR',
                'taxes_id' => 'S3',
            ],
        ];
        $lines = [
            '0' => [
                '0' => [
                    'material_code' => 'Albarán nº : 0000174151 Fecha : 19/10/2020',
                    'material_description' => 'TRANSPALETA MANUAL<br>MARCA Y MODELO : LIFTER GS<br>BASTIDOR : HLI0719621<br>CAPACIDAD DE CARGA : 2200 KG<br>HORQUILLAS : 1150X560<br>RODILLOS : BOOGIE N/BN',
                    'material_qty' => '1',
                    'packaging_type' => '',
                    'unit_price' => '325',
                    'currency' => 'EUR',
                    'taxes' => '21%',
                    'net_amount' => '325.00',
                    'irpf' => '0',
                    'total_amount' => '393.25',
                    'taxes_id' => 'S3',
                ]
            ],
            '1' => [
                '0' => [
                    'material_code' => ' 202848 GOMA XANTANA ALIM. NORMAL FN',
                    'material_description' => '',
                    'material_qty' => '200',
                    'packaging_type' => '',
                    'unit_price' => '7000.00',
                    'currency' => 'EUR',
                    'taxes' => '21%',
                    'net_amount' => '1400.00',
                    'irpf' => '0',
                    'total_amount' => '1694.00',
                    'taxes_id' => 'S3',
                ]
            ],
            '2' => [
                '0' => [
                    'material_code' => 'Estuches anónimos METALFENIDATO 84 caps',
                    'material_description' => '',
                    'material_qty' => '30',
                    'packaging_type' => '',
                    'unit_price' => '10.00',
                    'currency' => 'EUR',
                    'taxes' => '21%',
                    'net_amount' => '300.00',
                    'irpf' => '0',
                    'total_amount' => '363.00',
                    'taxes_id' => 'S3',
                ]
            ],
        ];
        $i = 0;
        foreach($data as $invoice){
//            dd($invoice);
            $invoiceData = Invoice::create($invoice);
            $l = 0;
            foreach ($lines[$i] as $line){
//                dd($line);
                $line['invoice_id'] = $invoiceData->id;
                InvoiceLine::create($line);
                $l++;
            }
            $i++;
        }
    }
}
