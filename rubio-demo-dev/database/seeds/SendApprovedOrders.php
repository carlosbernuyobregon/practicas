<?php

use App\Library\Constants;
use App\Mail\SupplierOrderNotification;
use App\Order;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Mail;

class SendApprovedOrders extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        /*
        const ORDER_STATUS_APPROVED                         = 8; // Pedido aprobado
        const ORDER_STATUS_UPDATED_IN_SAP                   = 9; // Pedido actualizado en SAP
        const ORDER_STATUS_UPDATED_IN_SAP_SUCCESS           = 91; // Pedido actualizado en SAP CORRECTAMENTE
        const ORDER_STATUS_UPDATED_IN_SAP_ERROR             = 92; // Pedido actualizado en SAP ERRONEAMENTE
        */
        $orders = Order::where('status',Constants::ORDER_STATUS_UPDATED_IN_SAP)
                            ->orWhere('status',Constants::ORDER_STATUS_UPDATED_IN_SAP_SUCCESS)
                            ->orWhere('status',Constants::ORDER_STATUS_UPDATED_IN_SAP_ERROR)
                            ->with(['provider'])
                            ->get();

        foreach($orders as $order){
            $emails = [];
            $emailProvider = false;
            if($order->provider->email != null){
                $emails[] = $order->provider->email;
                $emailProvider = true;
            }
            if($order->provider->email_orders != null){
                $emails[] = $order->provider->email_orders;
                $emailProvider = true;
            }
            if($order->user->email != null){
                $emails[] = $order->user->email;
            }
            if(count($emails) > 0){
                if(App::environment('production')){
                    Mail::to($emails)
                        ->send(new SupplierOrderNotification($order, $emailProvider));
                    //Mail::to([$order->provider->email_orders,$order->provider->email])->send(new SupplierOrderNotification($order));    //code...
                }else{
                    Mail::to(['lpiqueras@strategying.com','fcampos@strategying.com','lluis.piqueras@gmail.com','dplanas@strategying.com'])
                        ->send(new SupplierOrderNotification($order,$emailProvider));
                }

            }

        }
    }
}
