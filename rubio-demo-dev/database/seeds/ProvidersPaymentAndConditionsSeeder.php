<?php

use App\Models\ProviderPaymentCondition;
use App\Models\ProviderPaymentType;
use Illuminate\Database\Seeder;

class ProvidersPaymentAndConditionsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $types = [
            1 => 'Transferencia SEPA',
            2 => 'Pagaré',
            3 => 'Cheque',
            4 => 'Confirming',
            5 => 'Efectivo',
            6 => 'Transferencia extranjero',
            7 => 'Recibo domiciliado',
            8 => 'Visa banco'
        ];
        $conditions = [
            'Z001' => 'Pago inmediato',
            'Z002' => 'Pago inmediato a día 25',
            'Z003' => '30 días fecha factura',
            'Z004' => '30 días fecha factura. Pago día 25',
            'Z005' => '45 días fecha factura',
            'Z006' => '45 días fecha factura. Pago día 25',
            'Z007' => '60 días fecha factura',
            'Z008' => '60 días fecha factura. Pago día 25',
            'Z009' => '75 días fecha factura. Pago día 25',
            'Z010' => '90 días fecha factura',
            'Z011' => '90 días fecha factura. Pago día 25',
            'Z012' => '15 días fecha factura',
        ];

        foreach ($types as $key => $val) {
            ProviderPaymentType::create([
                'code'          => $key,
                'name'          => $val,
                'description'   => $val,
            ]);
        }

        foreach ($conditions as $key => $val) {
            ProviderPaymentCondition::create([
                'code'          => $key,
                'name'          => $val,
                'description'   => $val,
            ]);
        }

        ProviderPaymentType::where('code', 1)->update(['is_default' => true]);
        ProviderPaymentCondition::where('code', 'Z001')->update(['is_default' => true]);

    }
}
