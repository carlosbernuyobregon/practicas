<?php

use App\Role;
use Illuminate\Support\Str;
use Illuminate\Database\Seeder;

class RoleTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            'key_value' => 'admin',
            'es' => [
                'name' => 'Administrador',
                'description' => 'Administrador del sistema'
            ],
            'en' => [
                'name' => 'Administrator',
                'description' => 'System administrator'
            ],
        ];
        Role::create($data);
        $data = [
            'key_value' => 'administration-user',
            'es' => [
                'name' => 'Usuario departamento de administración',
                'description' => ''
            ],
            'en' => [
                'name' => 'Administration department user',
                'description' => ''
            ],
        ];
        Role::create($data);
        $data = [
            'key_value' => 'administration-manager',
            'es' => [
                'name' => 'Manager departamento de administración',
                'description' => ''
            ],
            'en' => [
                'name' => 'Administration department manager',
                'description' => ''
            ],
        ];
        Role::create($data);

    }
}
