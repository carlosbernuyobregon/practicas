<?php

use App\CustomerType;
use Illuminate\Database\Seeder;

class CustomerTypeTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $data = [
            'key_value' => 'company',
            'es' => [
                'name' => 'Empresa',
                'description' => ''
            ],
            'en' => [
                'name' => 'Company',
                'description' => ''
            ],
        ];
        CustomerType::create($data);

    }
}
