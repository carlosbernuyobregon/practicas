<?php

use App\Company;
use App\CostCenter;
use App\Customer;
use App\Role;
use App\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Rap2hpoutre\FastExcel\Facades\FastExcel;

class UpdateCecosUsersAndBudgets extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $filePathMaster = resource_path('imports/cecos-users-and-budgets-04012022.xlsx');

        // Import data as collection
        $usersInfo   = FastExcel::sheet(2)->import($filePathMaster);
        $customer = Customer::where('email','info@rubio.com')->value('id');
        $roleUser = Role::where('key_value','usuario-de-pedidos')->value('id');
        $roleManager = Role::where('key_value','manager-de-pedidos')->value('id');
        $roleMarketing = Role::where('key_value','marketing')->value('id');
        $roleDirector = Role::where('key_value','direccion-de-pedidos')->value('id');
        $roleGeneral = Role::where('key_value','direccion-general')->value('id');
        foreach ($usersInfo as $item) {
            $userName = explode(' ',$item['USER']);
            $user = User::updateOrCreate([
                        'email' => trim($item['EMAIL_USER'])
                    ],[
                        'name' => trim($item['EMAIL_USER']),
                        'first_name' => trim($userName[0]),
                        'last_name' => trim($userName[1] ?? ''),
                        'password' => Hash::make('Pruebas2022'),
                        'customer_id' => $customer,
                    ]);
            //dump($user);
            $userName = explode(' ',$item['MANAGER']);
            $manager = User::updateOrCreate([
                        'email' => trim($item['EMAIL_MANAGER'])
                    ],[
                        'name' => trim($item['MANAGER']),
                        'first_name' => trim($userName[0]),
                        'last_name' => trim($userName[1] ?? ''),
                        'password' => Hash::make('Pruebas2022'),
                        'customer_id' => $customer,
                    ]);
                    //dump($manager);
            $userName = explode(' ',$item['DIRECTOR']);
            $director = User::updateOrCreate([
                        'email' => trim($item['EMAIL_DIRECTOR'])
                    ],[
                        'name' => trim($item['DIRECTOR']),
                        'first_name' => trim($userName[0]),
                        'last_name' => trim($userName[1] ?? ''),
                        'password' => Hash::make('Pruebas2022'),
                        'customer_id' => $customer,
                    ]);
                    //dump($director);
            $userName = explode(' ',$item['GENERAL']);
            $general = User::updateOrCreate([
                        'email' => trim($item['EMAIL_GENERAL'])
                    ],[
                        'name' => trim($item['GENERAL']),
                        'first_name' => trim($userName[0]),
                        'last_name' => trim($userName[1] ?? ''),
                        'password' => Hash::make('Pruebas2022'),
                        'customer_id' => $customer,
                    ]);
                    //dump($general);
            $company = Company::where('code',trim($item['SOCIEDAD']))->first();
            //dump($company);
            $costCenter = CostCenter::where('external_id','000000' . trim($item['CECO']))
                            ->where('company',$item['SOCIEDAD'])
                            ->first();
            if($costCenter != null){
                $costCenter->update([
                    'name'                      => $item['CECO_DESCRIPTION'],
                    'manager_1_id'              => (string)$manager->id,
                    'manager_2_id'              => (string)$director->id,
                    'manager_3_id'              => (string)$general->id,
                    'amount_level_1'            => 0,
                    'amount_level_2'            => 4999,
                    'amount_level_3'            => 9999,
                    'amount_level_secondary_1'  => 0,
                    'amount_level_secondary_2'  => 4999,
                    'amount_level_secondary_3'  => 9999,
                ]);
            }else{
                $costCenter = CostCenter::create([
                    'customer_id' => $customer,
                    'external_id' => '000000' . trim($item['CECO']),
                    'name' => trim($item['CECO_DESCRIPTION']),
                    'manager_1_id' => (string)$manager->id,
                    'manager_2_id' => (string)$director->id,
                    'manager_3_id' => (string)$general->id,
                    'amount_level_1' => 0,
                    'amount_level_2' => 4999,
                    'amount_level_3' => 9999,
                    'amount_level_secondary_1' => 0,
                    'amount_level_secondary_2' => 4999,
                    'amount_level_secondary_3' => 9999,
                ]);
            }
            //dump($costCenter);

            $exists = DB::table('cost_center_user')->where('cost_center_id',(string)$costCenter->id)
                ->where('user_id',(string)$user->id)->exists();
            if($user->default_cost_center_id == null){
                $user->default_cost_center_id = (string)$costCenter->id;
                $user->save();
            }
            if(!$exists){
                try {
                    $user->costCenters()->attach((string)$costCenter->id);
                } catch (\Throwable $th) {
                    dump($costCenter);
                }
            }
            if(!$user->hasRole('marketing',true)){
                $user->roles()->attach($roleMarketing);
            }
            if(!$user->hasRole('usuario-de-pedidos',true)){
                $user->roles()->attach($roleUser);
            }
            $exists = DB::table('cost_center_user')->where('cost_center_id',(string)$costCenter->id)
                ->where('user_id',(string)$manager->id)->exists();
            if(!$exists){
                try {
                    $manager->costCenters()->attach((string)$costCenter->id);
                } catch (\Throwable $th) {
                    dump($costCenter);
                }
            }
            if(!$manager->hasRole('manager-de-pedidos',true)){
                $manager->roles()->attach($roleManager);
            }
            $exists = DB::table('cost_center_user')->where('cost_center_id',(string)$costCenter->id)
                ->where('user_id',(string)$director->id)->exists();
            if(!$exists){
                try {
                    $director->costCenters()->attach((string)$costCenter->id);
                } catch (\Throwable $th) {
                    dump($costCenter);
                }
            }
            if(!$director->hasRole('direccion-de-pedidos',true)){
                $director->roles()->attach($roleDirector);
            }
            $exists = DB::table('cost_center_user')->where('cost_center_id',(string)$costCenter->id)
                ->where('user_id',(string)$general->id)->exists();
            if(!$exists){
                try {
                    $general->costCenters()->attach((string)$costCenter->id);
                } catch (\Throwable $th) {
                    dump($costCenter);
                }
            }
            if(!$general->hasRole('direccion-general',true)){
                $general->roles()->attach($roleManager);
            }
            //die();
        }


    }
}
