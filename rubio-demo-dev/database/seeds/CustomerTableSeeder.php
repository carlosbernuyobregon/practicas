<?php

use App\Customer;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Str;

class CustomerTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $token = base64_encode(random_bytes(64));
        $token = strtr($token, '+/', '-_');

        Customer::create([
            'name' => 'New Strategying in Action S.L.',
            'email' => 'info@strategying.com',
            'public_api_key' => Str::uuid(),
            'secret_api_key' => Crypt::encryptString($token),
            'non_encripted' => $token
        ]);
        $token = base64_encode(random_bytes(64));
        $token = strtr($token, '+/', '-_');
        Customer::create([
            'name' => 'Laboratorios Rubio S.A.',
            'email' => 'info@rubio.com',
            'public_api_key' => Str::uuid(),
            'secret_api_key' => Crypt::encryptString($token),
            'non_encripted' => $token
        ]);
    }
}
