<?php

use App\Setting;
use Illuminate\Database\Seeder;

class CustomSettingsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $data = [
            'key_value' => 'request_count',
            'value' => '',
            'count' => 0,
        ];
        Setting::create($data);
    }
}
