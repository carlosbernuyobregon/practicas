<?php

use App\Models\Provider;
use App\Order;
use App\Supplier;
use Illuminate\Database\Seeder;

class MergeSuppliersToProvidersTable extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $suppliers = Supplier::all();
        foreach($suppliers as $supplier){
            $newProvider = Provider::where('vat_number',$supplier->vat_number)
                ->orWhere('creditor_number',(int)$supplier->external_id)
                ->first();
            if($newProvider){
                $newProvider->update([
                    'company' => $supplier->company,
                    'payment_condition' => $supplier->payment_condition,
                    'payment_type' => $supplier->payment_type,
                    'external_id' => $supplier->external_id,
                    'customer_id' => $supplier->customer_id,
                    'supplier_id' => $supplier->id,
                    'name' => $supplier->name
                ]);
            }else{
                $newProvider = Provider::create([
                    'vat_number' => $supplier->vat_number,
                    'creditor_number' => (int)$supplier->external_id,
                    'company' => $supplier->company,
                    'payment_condition' => $supplier->payment_condition,
                    'payment_type' => $supplier->payment_type,
                    'external_id' => $supplier->external_id,
                    'customer_id' => $supplier->customer_id,
                    'supplier_id' => $supplier->id,
                    'name' => $supplier->name
                ]);
            }
            $orders = Order::where('supplier_id',$supplier->id)->get();
            if($orders->count()>0){
                echo $orders->count() . "\n";
                $i = 0;
                foreach($orders as $order){
                    echo (string)$newProvider->id . "\n" . $supplier->id . "\n ******************* \n";
                    $order->supplier_id = (string)$newProvider->id;
                    $order->save();
                    $i++;
                }
                echo $i . "\n";
            }

        }

    }
}
