<?php

use App\Models\Provider;
use App\Models\ProvidersPerSapCompanyForcedTaxType;
use App\TaxType;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Str;
use Rap2hpoutre\FastExcel\Facades\FastExcel;
use function Clue\StreamFilter\prepend;

class WipeProvidersFromExcelSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Get the source file path
        $filePathMaster = resource_path('imports/full-providers-10-2021.xlsx');

        // Check if file exists
        if (File::exists($filePathMaster)) {

            // Import data as collection
            $providers = FastExcel::import($filePathMaster);

            // Defaults
            $providerNew = [];
            $prodCompanyId = '70caf923-764c-4c32-903c-f18c60ffe7a1';

            $providers->each(function ($provider, $key) use ($prodCompanyId) {
                $company        = trim($provider['bukrs']);

                if (!in_array($company, ['1000', '2000'])) {
                    // This is like a 'continue' for Collection each method
                    return true;
                }

                $vat            = trim($provider['stcd1']);
                $communityVat   = trim($provider['stceg']);
                $name           = trim($provider['name1']);

                if ($vat == '' && $communityVat == '') {
                    $name = '@@@@@' . $name;
                }

                $providerNew['vat_number']              = $vat;
                $providerNew['community_vat_number']    = $communityVat;
                $providerNew['name']                    = $name;
                $providerNew['country']                 = trim($provider['Pais']);
                $providerNew['address']                 = trim($provider['Calle']);
                $providerNew['city']                    = trim($provider['Ciudad']);
                $providerNew['zip_code']                = trim($provider['CP']);
                $providerNew['payment_condition']       = trim($provider['zterm']);
                $providerNew['payment_type']            = trim($provider['zwels']);
                $providerNew['company']                 = $company;
                $providerNew['customer_id']             = $prodCompanyId;
                $providerNew['external_id']             = trim($provider['lifnr']);

                Provider::create($providerNew);
            });
        }
    }
}
