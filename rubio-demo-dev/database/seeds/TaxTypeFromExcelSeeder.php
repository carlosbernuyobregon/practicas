<?php

use App\Library\Constants;
use App\TaxType;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\File;
use Rap2hpoutre\FastExcel\Facades\FastExcel;

class TaxTypeFromExcelSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        /*
         * En esta tabla deberían estar informados todos los que son de las letras V-N-S-Q-A-P-I
         * S =Proveedores Nacionales
         * N = IVA no deducible
         * I = proveedores de importación que hay muchas facturas en 2020 de Rubió
         * V=Tracto Sucesivo
         * Q=Proveedores terceros países
         * A=Proveedores UE;
         * Autorepertutido= P,Q,A
         *

         * */
        // Get the source file path
        $filePath = resource_path('imports/masterIVA.xlsx');

        // Check if file exists
        if (File::exists($filePath)) {
            // Import data as collection
            $newTaxType = FastExcel::import($filePath);

            // Defaults
            $field = [];

            $newTaxType->each(function ($item, $key) {
                if ($item['Porcentaje'] !== '') {
                    switch ($item['Tipo']) {
                        //A, P y Q
                        case substr($item['Tipo'], 0, 1) == 'A':
                        case substr($item['Tipo'], 0, 1) == 'P':
                        case substr($item['Tipo'], 0, 1) == 'Q':
                            $specialType = Constants::TAX_SPECIAL_TYPE_AUTOREPERCUTIDO;
                            break;
                        default:
                            $specialType = null;
                            break;
                    }

                    // Exception of the rule, this tax type applies no special conditions when calculation is done
                    if ($item['Tipo'] == 'AG') {
                        $specialType = null;
                    }

                    $field['description']   = $item['Descripción'];
                    $field['percentage']    = $item['Porcentaje'];
                    $field['special_type']  = $specialType;
                    $field['active']        = true;

                    TaxType::updateOrCreate(
                        ['type' => $item['Tipo']],
                        $field
                    );
                    dump('updating type ' . $item['Tipo']);
                } else {
                    dump('--------------------- skipping temporarily unsupported type ' . $item['Tipo']);
                }
            });
        }
    }
}
