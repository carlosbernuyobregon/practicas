<?php

use App\Country;
use Illuminate\Database\Seeder;

class CountryCustomZoneSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // UE
        Country::whereIn(
            'code',
            [
                'AT', 'BE', 'BG', 'HR', 'CY', 'CZ', 'DK', 'EE', 'FI', 'FR', 'DE', 'GR', 'HU', 'IE', 'IT', 'LV', 'LT',
                'LU', 'MT', 'NL', 'PL', 'PT', 'RO', 'SK', 'SI', 'ES', 'SE'
            ]
        )
            ->update(['eu_zone' => true]);

        // Rest of Europe
        Country::whereIn(
            'code',
            [
                'AL', 'AD', 'AM', 'BY', 'BA', 'FO', 'GE', 'GI', 'IS', 'LI', 'MK', 'MD', 'MC', 'NO', 'RU', 'SM', 'RS',
                'CH', 'TR', 'UA', 'GB', 'VA'
            ]
        )
            ->update(['non_eu_zone' => true]);

        // Rest of the world
        Country::whereNull('eu_zone')
            ->whereNull('non_eu_zone')
            ->update(['rest_of_the_world_zone' => true]);
    }
}

/*
 *
Alemania
'DE'

Grecia
'GR'

Austria
'AT'

Hungría
'HU'

Bélgica
'BE'

Irlanda
'IE'

Bulgaria
'BG'

Italia
'IT'

Chequia
'CZ'

Letonia
'LV'

Chipre
'CY'

Lituania
'LT',

Croacia
'HR'

Luxemburgo
'LU'

Dinamarca
'DK'

Malta
'MT'

Eslovaquia
'SK'

Países Bajos
'NL'

Eslovenia
'SI'

Polonia
'PL'

España
'ES'

Portugal
'PT'

Estonia
'EE'

Rumanía
'RO'

Finlandia
'FI'

Suecia
'SE'

Francia
'FR'


 * */
