<?php

use App\CostCenter;
use App\Country;
use App\Role;
use App\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class OrderBasicConfigurationSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        /*
        Managers:
            - Alex Tamayo (IT)
            - Toni Felis (Finanzas)
            - Andrés Ramón: aramon@labrubio.com (Compras)
        Directors:
            - Andrés Ramón (IT)
            - Andrés Ramón (Finanzas)
            - Toni Félis (Compras)
        Directora general:
            - Isabel Ramírez
        Usuaris:
            - Yolanda (administració i compres)
            - Sara (administració i compres)
            - Isabel (administració i compres)
            - Alba Marín amarin@labrubio.com (administració)
            - Zeus Díaz zdiaz@labrubio.com (IT)
            - David Rodriguez drodriguez@labrubio.com (IT)
        */


        $roles = [
            'Usuario de pedidos',
            'Manager de pedidos',
            'Dirección de pedidos',
            'Dirección general'
        ];
        foreach($roles as $role){
            Role::create([
                'key_value' => Str::slug($role),
                'name' => $role
            ]);
        }

        User::firstOrCreate([
            'email' => 'aramon@labrubio.com',
        ],[
            'name' => 'Andrés Ramón',
            'phone' => '789789789',
            'first_name' => 'Andrés',
            'last_name' => 'Ramón',
            'password' => bcrypt('secret'),
            'country_id' => Country::where('key_value', 'spain')->value('id'),
            'default_locale' => 'es',
        ]);
        User::firstOrCreate([
            'email' => 'amarin@labrubio.com',
        ],[
            'name' => 'Alba Marín',
            'phone' => '789789789',
            'first_name' => 'Alba',
            'last_name' => 'Marín',
            'password' => bcrypt('secret'),
            'country_id' => Country::where('key_value', 'spain')->value('id'),
            'default_locale' => 'es',
        ]);
        User::firstOrCreate([
            'email' => 'drodriguez@labrubio.com',
        ],[
            'name' => 'David Rodriguez',
            'phone' => '789789789',
            'first_name' => 'David',
            'last_name' => 'Rodriguez',
            'password' => bcrypt('secret'),
            'country_id' => Country::where('key_value', 'spain')->value('id'),
            'default_locale' => 'es',
        ]);
        User::firstOrCreate([
            'email' => 'zdiaz@labrubio.com',
        ],[
            'name' => 'Zeus Diaz',
            'phone' => '789789789',
            'first_name' => 'Zeus',
            'last_name' => 'Diaz',
            'password' => bcrypt('secret'),
            'country_id' => Country::where('key_value', 'spain')->value('id'),
            'default_locale' => 'es',
        ]);


        $atamayo = User::where('email','like','%atamayo%')->first();
        $tfelis = User::where('email','like','%afelis%')->first();
        $aramon = User::where('email','like','%aramon%')->first();
        $iramirez = User::where('email','like','%iramirez%')->first();
        DB::table('cost_centers')->where('external_id','like','%1072%')->update([
            'manager_1_id' => $atamayo->id,
            'manager_2_id' => $aramon->id,
            'manager_3_id' => $iramirez->id,
        ]);
        DB::table('cost_centers')->where('external_id','like','%1099%')->update([
            'manager_1_id' => $aramon->id,
            'manager_2_id' => $tfelis->id,
            'manager_3_id' => $iramirez->id,
        ]);
        DB::table('cost_centers')->where('external_id','like','%1070%')->update([
            'manager_1_id' => $tfelis->id,
            'manager_2_id' => $aramon->id,
            'manager_3_id' => $iramirez->id,
        ]);
        DB::table('cost_center_user')->truncate();

        $finanzas = [
            'ybeza',
            'sortega',
            'iramirez',
            'amarin',
            'aramon',
            'afelis',
            'admin'
        ];
        $costCenter = CostCenter::where('external_id','like','%1072%')->first();
        if($costCenter){
            foreach($finanzas as $u){
                $user = User::where('email','like','%' . $u . '%')->first();
                $user->costCenters()->attach($costCenter->id);
            }
        }
        $compras = [
            'ybeza',
            'sortega',
            'iramirez',
            'afelis',
            'aramon',
            'admin'
        ];
        $costCenter = CostCenter::where('external_id','like','%1099%')->first();
        if($costCenter){
            foreach($compras as $u){
                $user = User::where('email','like','%' . $u . '%')->first();
                $user->costCenters()->attach($costCenter->id);
            }
        }
        $it = [
            'drodriguez',
            'zdiaz',
            'atamayo',
            'aramon',
            'admin'
        ];
        $costCenter = CostCenter::where('external_id','like','%1070%')->first();
        if($costCenter){
            foreach($it as $u){
                $user = User::where('email','like','%' . $u . '%')->first();
                $user->costCenters()->attach($costCenter->id);
            }
        }
    }
}
