<?php

use App\Setting;
use Illuminate\Database\Seeder;

class SettingTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $data = [
            'key_value' => 'request_count',
            'value' => '',
            'count' => 0,
        ];
        Setting::create($data);
        $data = [
            'key_value' => 'quotation_count',
            'value' => '',
            'count' => 0,
        ];
        Setting::create($data);
        $data = [
            'key_value' => 'project_count',
            'value' => '',
            'count' => 0,
        ];
        Setting::create($data);
    }
}
