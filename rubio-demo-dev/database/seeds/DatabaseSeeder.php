<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // Old
        /*
        // Base Seeders
        $this->call(CustomSettingsTableSeeder::class);
        //$this->call(SizeTableSeeder::class);
        //$this->call(ProductClassTableSeeder::class);
        //$this->call(ProductCategoryTableSeeder::class);
        $this->call(CurrencyTableSeeder::class);
        $this->call(CountryTableSeeder::class);
        $this->call(TaxTypeSeeder::class);
        //$this->call(SourceTableSeeder::class);
        //$this->call(BusinessLineTableSeeder::class);
        //$this->call(CustomerTypeTableSeeder::class);
        //$this->call(PackagingCustomizationTypeTableSeeder::class);
        //$this->call(ProductCustomizationTypeTableSeeder::class);
        //$this->call(ProductTableSeeder::class);
        //$this->call(CancelationReasonTableSeeder::class);
        //$this->call(ProcessTableSeeder::class);
        $this->call(CustomerTypeTableSeeder::class);
        // Composite Seeders

        $this->call(RoleTableSeeder::class);
        //$this->call(PurposeTableSeeder::class);
        $this->call(UserTableSeeder::class);
        //    $this->call(InvoiceTableSeeder::class);
        //    $this->call(NewInvoicesSeeder::class);
        //$this->call(CountryTableSeeder::class);
        $this->call(UpdateCountriesSAPSeeder::class);
        $this->call(CustomerTableSeeder::class);
        */

        // New
        /*
        $this->call(PopulateRubioCompaniesSeeder::class);
        $this->call(ProviderRulesSeeder::class);
        $this->call(UpdateCountriesSAPSeeder::class);
        //$this->call(PaTablesSeeder::class);
        $this->call(AssignAccountingAccountIdToOrderLines::class);
        //$this->call(BudgetDefinitiveSeeder::class);
        $this->call(OrderBasicConfigurationSeeder::class);
        $this->call(PaTablesSeeder::class);
        $this->call(ProvidersPaymentAndConditionsSeeder::class);
        */
        //$this->call(BudgetAgrupationSeeder::class);
        $this->call(FinalBudgetUpdate2022::class);
        $this->call(UpdateCecosUsersAndBudgets::class);

    }
}
