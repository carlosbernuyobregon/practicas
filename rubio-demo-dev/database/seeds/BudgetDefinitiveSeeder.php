<?php

use App\AccountingAccount;
use App\Budget;
use App\CostCenter;
use App\Customer;
use Illuminate\Database\Seeder;
use Rap2hpoutre\FastExcel\Facades\FastExcel;

/**
 * Class BudgetDefinitiveSeeder
 *
 * This must be the main seeder to keep Budgets up to date, including versioning
 *
 */
class BudgetDefinitiveSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Get the source file path
        $filePathMaster = resource_path('imports/partidas_pto_agrupacion_responsable_10_12_2021.xlsx');

        // Import data as collection
        $budgetsFromExcel = FastExcel::sheet(1)
            ->import($filePathMaster);
//            ->where('CUENTA_MAYOR', 622001);

        // Defaults
        $customerId = Customer::where('email', 'info@rubio.com')->value('id');

        foreach ($budgetsFromExcel as $key => $budgetItem) {

            // Get the Cost Center and Accounting Account
            $formattedCeCo      = str_pad($budgetItem['CECO'], 10, '0', STR_PAD_LEFT);
            $costCenter         = CostCenter::where('external_id', $formattedCeCo)->first();
            $formattedAccount   = str_pad($budgetItem['CUENTA_MAYOR'], 10, '0', STR_PAD_LEFT);
            $accountingAccount  = AccountingAccount::where('external_id', $formattedAccount)->first();

            if ($costCenter) {
                // Get the Budget
                $budget = Budget::where('customer_id', $customerId)
                    ->where('cost_center_id', $costCenter->id)
                    ->where('name', $budgetItem['PARTIDA_PRESUPUESTARIA'])
                    ->first();

                // Update values
                if ($budget) {
                    // Clean amount
                    $cleanAmount                    = (float)str_replace(',','.', $budgetItem['IMPORTE']);
                    $budget->group                  = $budgetItem['AGRUPACION'] != '-' ? $budgetItem['AGRUPACION'] : null;
                    $budget->initial                = round($budget->initial + $cleanAmount, 2);
                    $budget->accounting_account_id  = $accountingAccount->id ?? null;
                    $budget->version                = $budgetItem['VERSION'] != '' ? $budgetItem['VERSION'] : null;

                    $budget->save();
                } else {
                    // Create budget if there is no previous one
                    $cleanAmount = (float)str_replace(',','.', $budgetItem['IMPORTE']);

                    $newBudgetData =  [
                        'customer_id'           => $customerId,
                        'cost_center_id'        => $costCenter->id,
                        'name'                  => $budgetItem['PARTIDA_PRESUPUESTARIA'],
                        //'name'                  => $budgetItem['PARTIDA_PRESUPUESTARIA'] != '-' ? $budgetItem['PARTIDA_PRESUPUESTARIA'] : null,
                        'initial'               => round($cleanAmount, 2),
                        'non_stock'             => true,
                        'group'                 => $budgetItem['AGRUPACION'] != '-' ? $budgetItem['AGRUPACION'] : null,
                        'accounting_account_id' => $accountingAccount->id ?? null,
                        'version'               => $budgetItem['VERSION'] != '' ? $budgetItem['VERSION'] : null
                    ];

                    $budget = Budget::create($newBudgetData);
                }
            }
        }
    }
}
