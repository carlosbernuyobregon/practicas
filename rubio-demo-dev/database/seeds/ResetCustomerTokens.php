<?php

use App\Customer;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Str;

class ResetCustomerTokens extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $customer = Customer::find('4da12574-dfe2-441b-abfd-ebd12e780c41');
        $token = base64_encode(random_bytes(64));
        $token = strtr($token, '+/', '-_');
        $customer->public_api_key = Str::uuid();
        $customer->secret_api_key = Crypt::encryptString($token);
        $customer->non_encripted = $token;
        $customer->save();

    }
}
