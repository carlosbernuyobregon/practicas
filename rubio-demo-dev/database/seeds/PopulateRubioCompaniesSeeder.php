<?php

use App\AccountingAccount;
use App\Company;
use App\Customer;
use Illuminate\Database\Seeder;

class PopulateRubioCompaniesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $customer = Customer::where('email','info@rubio.com')->first();
        $companies = [
            [
                'name' => 'Rubió',
                'code' => '1000',
                'customer_id' => (string)$customer->id
            ],
            [
                'name' => 'Products and tecnology',
                'code' => '2000',
                'customer_id' => (string)$customer->id
            ]
        ];
        foreach($companies as $item){
            Company::create($item);
        }
        AccountingAccount::where('customer_id',null)->orWhere('customer_id','!=',null)->update([
            'customer_id' => (string)$customer->id
        ]);
    }
}
