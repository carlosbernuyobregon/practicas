<?php

use App\Models\PaBusinessArea;
use App\Models\PaClient;
use App\Models\PaDistributionChannel;
use App\Models\PaFamily;
use App\Models\PaOrder;
use App\Models\PaPerson;
use Illuminate\Database\Seeder;
use Rap2hpoutre\FastExcel\Facades\FastExcel;

class PaTablesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Get the source file path
        $filePathMaster = resource_path('imports/Info-CO-PA-26112021.xlsx');

        // Import data as collection
        $paDataPerson   = FastExcel::sheet(1)->import($filePathMaster);
        $paDataFamily   = FastExcel::sheet(2)->import($filePathMaster);
        $paDataClient   = FastExcel::sheet(3)->import($filePathMaster);
        $paDataOrder    = FastExcel::sheet(4)->import($filePathMaster);

        // Business Areas
        $dataBA = [
            '01' => 'Prescripción',
            '02' => 'OTC',
            '03' => 'DX'
        ];

        PaBusinessArea::truncate();

        foreach ($dataBA as $key => $ba) {
            PaBusinessArea::create([
                'area'      => $key,
                'name'      => $ba
            ]);
        }

        // Distribution Channels
        $dataDC = [
            '01' => 'Nacional',
            '02' => 'Exportación'
        ];

        PaDistributionChannel::truncate();

        foreach ($dataDC as $key => $dc) {
            PaDistributionChannel::create([
                'channel'   => $key,
                'name'      => $dc
            ]);
        }

        // Orders
        PaOrder::truncate();

        foreach ($paDataOrder as $do) {
            PaOrder::create([
                'order'     => trim($do['SAP_ID']),
                'name'      => trim($do['NAME'])
            ]);
        }

        // Persons
        PaPerson::truncate();

        foreach ($paDataPerson as $dp) {
            PaPerson::create([
                'person'    => trim($dp['SAP_ID']),
                'name'      => trim($dp['NAME'])
            ]);
        }

        // Families
        PaFamily::truncate();

        foreach ($paDataFamily as $df) {
            PaFamily::create([
                'family'    => trim($df['SAP_ID']),
                'name'      => trim($df['NAME'])
            ]);
        }

        // Clients
        PaClient::truncate();

        foreach ($paDataClient as $dc) {
            PaClient::create([
                'client'    => trim($dc['SAP_ID']),
                'name'      => trim(str_replace('"', '', $dc['NAME']))
            ]);
        }
    }
}
