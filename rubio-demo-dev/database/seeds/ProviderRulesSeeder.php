<?php

use App\Models\ProviderRule;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\File;
use Rap2hpoutre\FastExcel\Facades\FastExcel;

class ProviderRulesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Forced tax type rules
        $filePathSpecial = resource_path('imports/providersForcedTaxType.xlsx');

        if (File::exists($filePathSpecial)) {
            // Import data as collection
            $providersTaxTypeSheetOne = FastExcel::sheet(1)->import($filePathSpecial); // Trato Sucesivo
            $providersTaxTypeSheetTwo = FastExcel::sheet(2)->import($filePathSpecial); // No Deducible

            // Defaults
            $groupedProviders   = [
                'Tracto Sucesivo'   => $providersTaxTypeSheetOne,
                'No Deducible'      => $providersTaxTypeSheetTwo
            ];

            foreach ($groupedProviders as $qualification => $groupedProvider) {
                $groupedProvider->each(function ($provider, $key) {

                    if ($provider['NIF'] !== 'NIF') {
                        $providerTaxTypeRule['provider_vat_number']     = trim($provider['NIF']);
                        $providerTaxTypeRule['force_tax_type']          = true;

                        try {
                            ProviderRule::updateOrCreate(
                                [
                                    'provider_vat_number' => $providerTaxTypeRule['provider_vat_number']
                                ],
                                $providerTaxTypeRule
                            );

                        } catch (PDOException $e) {
                            dd($e->getMessage(), $providerTaxTypeRule, $provider);
                        }
                    }
                });
            }
        }

        // Blocked invoices due to unit measures
        $umBlockedVATProviders = [
            'A08002073', // QUIMIDROGA S.A
            'B08400574', // J.SOLER FERRER S.L
            'W0065869J', // PUROLITE (INT. ) LTD
            'A61744033', // AZELIS ESPAÑA, S.A.
            'A58099680', // IBERICA GRAFICA, S.A.
            'A08119356', // FARMAQUIMICA ESPAÑOLA S.A
        ];

        foreach ($umBlockedVATProviders as $blockProviderVAT) {
            $providerUMBlocked['provider_vat_number']     = $blockProviderVAT;
            $providerUMBlocked['block_due_unit_measure']  = true;

            try {
                ProviderRule::updateOrCreate(
                    [
                        'provider_vat_number' => $providerUMBlocked['provider_vat_number']
                    ],
                    $providerUMBlocked
                );

            } catch (PDOException $e) {
                dd($e->getMessage(), $providerUMBlocked);
            }
        }
    }
}
