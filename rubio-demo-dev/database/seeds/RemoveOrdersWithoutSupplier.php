<?php

use App\Order;
use Illuminate\Database\Seeder;


class RemoveOrdersWithoutSupplier extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $orders = Order::whereNull('provider_id')->get();
        echo $orders->count();
        foreach($orders as $item){
            echo $item->id;
            $item->delete();
        }
    }
}
