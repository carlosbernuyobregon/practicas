<?php

use App\User;
use Illuminate\Database\Seeder;

class AdminUserResetPassword extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $random = str_shuffle('abcdefghjklmnopqrstuvwxyzABCDEFGHJKLMNOPQRSTUVWXYZ234567890!$%^&!$%^&');
        $password = substr($random, 0, 16);
        $user = User::where('email', 'admin@example.com')->update(['password' => bcrypt($password)]);
        echo "New password: ";
        echo $password;
        echo "\n";
    }
}
