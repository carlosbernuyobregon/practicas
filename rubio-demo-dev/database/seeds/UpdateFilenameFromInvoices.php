<?php

use App\Invoice;
use Illuminate\Database\Seeder;

class UpdateFilenameFromInvoices extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $invoices = Invoice::where('email_attachment_id','!=',null)->get();
        foreach($invoices as $invoice){
            $invoice->filename = $invoice->emailAttachment->filename;
            $invoice->save();
        }
    }
}
