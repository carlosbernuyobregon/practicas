<?php

use App\AccountingAccount;
use App\Budget;
use App\Company;
use App\CostCenter;
use App\Customer;
use App\Models\BudgetAgrupation;
use App\Role;
use App\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Rap2hpoutre\FastExcel\Facades\FastExcel;

class FinalBudgetUpdate2022 extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $line = 0;
        $filePathMaster = resource_path('imports/P2022_partidas_pto_agrupacion_responsable_28122021.xlsx');
        Budget::truncate();
        BudgetAgrupation::truncate();
        CostCenter::truncate();



        // Import data as collection
        $budgetsFromExcel = FastExcel::sheet(2)
            ->import($filePathMaster);
//            ->where('CUENTA_MAYOR', 622001);
        $total = 0;
        $customerId = Customer::where('email', 'info@rubio.com')->value('id');
        User::where('customer_id',null)->update(['customer_id' => $customerId]);
        $companies  = Company::where('customer_id',$customerId)->get();
        $role = Role::where('key_value','marketing')->first();
        if(!$role){
            $newRole = Role::create([
                'key_value'             => 'marketing',
                'customer_id'           => $customerId,
                'es'                    => [
                    'name'              => 'Marketing',
                    'description'       => 'Perfil Marketing'
                ],
                'en'                    => [
                    'name'              => 'Marketing',
                    'description'       => 'Marketing profile'
                ]
            ]);
        }
        foreach($companies as $company){
            $companiesArray[$company->code] = $company;
            $budgetAgrupation = BudgetAgrupation::create([
                'customer_id'       => $customerId,
                'company_id'        => (string)$company->id,
                'name'              => 'Sin agrupación',
                'others'            => true
            ]);
        }
        $sociedad   = null;
        $ceco       = null;
        $cuenta     = null;
        foreach ($budgetsFromExcel as $key => $budgetItem) {
            //dump($budgetItem);
            /*
            if($sociedad != $budgetItem['SOCIEDAD']){
                $sociedad       = $budgetItem['SOCIEDAD'];
                $companyId      = Company::where('customer_id',$customerId)
                                    ->where('code',$budgetItem['SOCIEDAD'])
                                    ->value('id');
            }
            */
            $formattedCeCo      = str_pad($budgetItem['CECO'], 10, '0', STR_PAD_LEFT);
            $formatedAccAccount = str_pad($budgetItem['CUENTA_MAYOR'], 10, '0', STR_PAD_LEFT);
            $costCenter         = CostCenter::firstOrCreate([
                                        'external_id'   => $formattedCeCo,
                                        'customer_id'   => $customerId,
                                        'company'    => trim($budgetItem['SOCIEDAD'])
                                    ]);
            $accAccount         = AccountingAccount::firstOrCreate([
                                        'external_id'   => $formatedAccAccount,
                                        'customer_id'   => $customerId,
                                        'company'    => trim($budgetItem['SOCIEDAD'])
                                    ],[
                                        'name'          => trim($budgetItem['NOMBRE_CUENTA_MAYOR'])
                                    ]);


            $budgetAgrupation = null;
            if($budgetItem['AGRUPACION'] != ''){
                $budgetAgrupation   = BudgetAgrupation::firstOrCreate([
                                            'name'      => $budgetItem['AGRUPACION'],
                                            'customer_id'       => $customerId,
                                            'company_id'        => (string)$companiesArray[trim($budgetItem['SOCIEDAD'])]->id,
                                        ]);
                echo (string)$budgetAgrupation->id;
                echo "\n";
                //sleep(1);
            }

            $budgetItemNew         = Budget::where('customer_id',$customerId)
                                        //->where('cost_center_id',(string)$costCenter->id)
                                        ->where('name',trim($budgetItem['PARTIDA_PRESU']))
                                        ->where('accounting_account_id',(string)$accAccount->id)
                                        ->first();
            if($budgetItemNew == null){
                $budgetItemNew     = Budget::create([
                                        'customer_id'           => $customerId,
                                        //'company_id'          => $companyId,
                                        //'cost_center_id'        => (string)$costCenter->id,
                                        'accounting_account_id' => (string)$accAccount->id,
                                        'name'                  => trim($budgetItem['PARTIDA_PRESU']),
                                        'non_stock'             => true,
                                        'budget_agrupation_id'  => $budgetAgrupation ? (string)$budgetAgrupation->id : null,
                                        'initial'               => trim($budgetItem['PRESUPUESTO_2022']),
                                    ]);
                echo $budgetItem['PARTIDA_PRESU'] . " - " . $budgetItemNew->initial . "\n";

            }else{
                $budgetItemNew->initial    = $budgetItem['PRESUPUESTO_2022'] + $budgetItemNew->initial;
                echo $budgetItem['PARTIDA_PRESU'] . " - " . $budgetItemNew->initial . "\n";
                $budgetItemNew->save();
            }
            $relationExists = DB::table('budget_cost_center')
                                ->where('cost_center_id',(string)$costCenter->id)
                                ->where('budget_id',(string)$budgetItemNew->id)
                                ->exists();
            if(!$relationExists){
                $budgetItemNew->costCenters()->attach((string)$costCenter->id);
            }
            $total = $total + $budgetItem['PRESUPUESTO_2022'];
            /*
            $budgetItem         = Budget::firstOrCreate([
                                        'customer_id'   => $customerId,
                                        //'company_id'    => $companyId,
                                        'cost_center_id'    => (string)$costCenter->id,
                                        'accounting_account_id' => (string)$accAccount->id,
                                        'name'          => $budgetItem['PARTIDA_PRESU'],
                                    ],[
                                        'non_stock'         => true,
                                        'budget_agrupation_id' => $budgetAgrupation ? (string)$budgetAgrupation->id : null
                                    ]);
            */
            $line++;
        }

        echo "Total = " . $total . "\n";
        echo "Total lineas = $line \n";
    }
}
