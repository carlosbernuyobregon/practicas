<?php

use App\Models\Provider;
use App\Models\ProvidersPerSapCompanyForcedTaxType;
use App\TaxType;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Str;
use Rap2hpoutre\FastExcel\Facades\FastExcel;

class ProvidersFromExcelSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Get the source file path
        $filePathMaster = resource_path('imports/masterProviders.xlsx');

        // Check if file exists
        if (File::exists($filePathMaster)) {

            // Import data as collection
            $providers = FastExcel::import($filePathMaster);

            // Defaults
            $providerNew = [];

            $providers->each(function ($provider, $key) {
                $providerNew['creditor_number']       = trim($provider['Acreedor']);
                $providerNew['vat_number']            = trim($provider['Nº ident.fis.1']);
                $providerNew['community_vat_number']  = trim($provider['N.I.F. comunitario']);
                $providerNew['name']                  = trim($provider['Nombre 1']) . ' ' . trim($provider['Nombre 2']);
                $providerNew['country']               = trim($provider['País']);
                $providerNew['address']               = trim($provider['Calle']);
                $providerNew['city']                  = trim($provider['Población']);
                $providerNew['zip_code']              = trim($provider['Cód.postal']);

                Provider::updateOrCreate(
                    ['creditor_number' => $provider['Acreedor']],
                    $providerNew
                );
            });
        }

        $filePathSpecial = resource_path('imports/providersForcedTaxType.xlsx');

        if (File::exists($filePathSpecial)) {
            // Import data as collection
            $providersTaxTypeSheetOne = FastExcel::sheet(1)->import($filePathSpecial); // Trato Sucesivo
            $providersTaxTypeSheetTwo = FastExcel::sheet(2)->import($filePathSpecial); // No Deducible

            // Defaults
            $uniqueness         = [];
            $groupedProviders   = [
                'Tracto Sucesivo'   => $providersTaxTypeSheetOne,
                'No Deducible'      => $providersTaxTypeSheetTwo
            ];

            foreach ($groupedProviders as $qualification => $groupedProvider) {
                $groupedProvider->each(function ($provider, $key) use ($qualification, &$uniqueness) {

                    if ($provider['NIF'] !== 'NIF') {
                        // Skip headers
                        // Get data from model
                        $taxType = TaxType::where('type', trim($provider['Tipo Iva']))->first();

                        $providerTaxType['sap_company']             = trim($provider['Sociedad Rubio']);
                        $providerTaxType['tax_type_id_fallback']    = ($taxType) ? $taxType->id : null;
                        $providerTaxType['tax_type_acronym']        = trim($provider['Tipo Iva Generico']);
                        $providerTaxType['tax_type_fallback']       = ($taxType) ? $taxType->type : $provider['Tipo Iva'];
                        $providerTaxType['tax_type_qualification']  = $qualification;
                        $providerTaxType['provider_vat_number']     = trim($provider['NIF']);
                        $providerTaxType['provider_vat_account']    = trim($provider['Cuenta']);
                        $providerTaxType['provider_name']           = trim($provider['Nombre Proveedor']);

                        try {
                            ProvidersPerSapCompanyForcedTaxType::updateOrCreate(
                                [
                                    'sap_company'           => $providerTaxType['sap_company'],
                                    'provider_vat_number'   => $providerTaxType['provider_vat_number'],
                                    'tax_type_fallback'     => $providerTaxType['tax_type_fallback']
                                ],
                                $providerTaxType
                            );

                            if (!in_array($providerTaxType['sap_company'] . '-' . $providerTaxType['provider_vat_number'], $uniqueness)) {
                                $uniqueness[] =  $providerTaxType['sap_company'] . '-' . $providerTaxType['provider_vat_number'] . '-' . $providerTaxType['tax_type_fallback'];
                            } else {
//                                dump('NOT UNIQUE', $providerTaxType, $provider, $uniqueness);
                            }

                        } catch (PDOException $e) {
                            dd($e->getMessage(), $providerTaxType, $provider);
                        }
                    }
                });
            }
        }
    }
}
