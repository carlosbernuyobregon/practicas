<?php

use App\Customer;
use App\Role;
use App\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class NewOrderVisualizationRole extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $customerId = Customer::where('email','info@rubio.com')->value('id');
        Role::create([
            'key_value' => 'orders-viewer',
            'customer_id' => $customerId,
            'es' => [
                'name' => 'Visualizador de pedidos',
                'description' => 'Puede ver los pedidos que han sido creados por otros usuarios.',
            ],
            'en' => [
                'name' => 'Orders viewer',
                'description' => 'Can see orders created by other users.',
            ],
        ]);
    }
}
