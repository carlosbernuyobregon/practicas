<?php

use App\Country;
use App\Role;
use App\User;
use Illuminate\Database\Seeder;

class RubioUsers extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            'name' => 'Alex Tamayo',
            'phone' => '789789789',
            'first_name' => 'Alex',
            'last_name' => 'Tamayo',
            'email' => 'atamayo@labrubio.com',
            'password' => bcrypt('secret'),
            'country_id' => Country::where('key_value', 'spain')->value('id'),
            'default_locale' => 'es',
        ];
        $user = User::create($data);
        $user->roles()->attach(Role::where('key_value', 'admin')->first());
        $data = [
            'name' => 'Antoni Felis',
            'phone' => '789789789',
            'first_name' => 'Antoni',
            'last_name' => 'Felis',
            'email' => 'afelis@labrubio.com',
            'password' => bcrypt('secret'),
            'country_id' => Country::where('key_value', 'spain')->value('id'),
            'default_locale' => 'es',
        ];
        $user = User::create($data);
        $user->roles()->attach(Role::where('key_value', 'administration-manager')->first());
        $data = [
            'name' => 'Isabel Ramirez',
            'phone' => '789789789',
            'first_name' => 'Isabel',
            'last_name' => 'Ramirez',
            'email' => 'iramirez@labrubio.com',
            'password' => bcrypt('secret'),
            'country_id' => Country::where('key_value', 'spain')->value('id'),
            'default_locale' => 'es',
        ];
        $user = User::create($data);
        $user->roles()->attach(Role::where('key_value', 'administration-user')->first());
        $data = [
            'name' => 'Sara Ortega',
            'phone' => '789789789',
            'first_name' => 'Sara',
            'last_name' => 'Ortega',
            'email' => 'sortega@labrubio.com',
            'password' => bcrypt('secret'),
            'country_id' => Country::where('key_value', 'spain')->value('id'),
            'default_locale' => 'es',
        ];
        $user = User::create($data);
        $user->roles()->attach(Role::where('key_value', 'administration-user')->first());
    }
}
