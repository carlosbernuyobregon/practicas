<?php

use App\MesureUnit;
use Illuminate\Database\Seeder;

class MesureUnitsFromSap extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $mesureUnits = array(
            0 => array('unit' => '%', 'description' => 'Porcentaje'),
            1 => array('unit' => '%O', 'description' => 'Tanto por mil'),
            2 => array('unit' => 'D', 'description' => 'Días'),
            3 => array('unit' => 'CMS', 'description' => 'Centímetro/Segundo'),
            4 => array('unit' => '000', 'description' => 'Metro/Minuto'),
            5 => array('unit' => 'µL', 'description' => 'Microlitro'),
            6 => array('unit' => 'µF', 'description' => 'Microfaradio'),
            7 => array('unit' => 'PF', 'description' => 'Picofaradio'),
            8 => array('unit' => 'GOH', 'description' => 'Gigaohmio'),
            9 => array('unit' => 'GM3', 'description' => 'Gramo/Metro cúbico'),
            10 => array('unit' => 'ACR', 'description' => 'Acre'),
            11 => array('unit' => 'KD3', 'description' => 'Kilogramo/Decímetro cúbico'),
            12 => array('unit' => 'KML', 'description' => 'Kilomol'),
            13 => array('unit' => 'KN', 'description' => 'Kilonewton'),
            14 => array('unit' => 'MN', 'description' => 'Meganewton'),
            15 => array('unit' => 'MGO', 'description' => 'Megaohmio'),
            16 => array('unit' => 'NMP', 'description' => 'Megavoltio'),
            17 => array('unit' => 'µA', 'description' => 'Microamperio'),
            18 => array('unit' => 'BOL', 'description' => 'Bolsa'),
            19 => array('unit' => 'BLI', 'description' => 'Blister'),
            20 => array('unit' => 'LC', 'description' => 'Botella'),
            21 => array('unit' => 'ºC', 'description' => 'º centigrados'),
            22 => array('unit' => 'CO', 'description' => 'Milifaradio'),
            23 => array('unit' => 'M/M', 'description' => 'Mol por metro cúbico'),
            24 => array('unit' => 'M/L', 'description' => 'Mol por litro'),
            25 => array('unit' => 'NR', 'description' => 'Nanoamperio'),
            26 => array('unit' => 'C3S', 'description' => 'Centímetro cúbico/Segundo'),
            27 => array('unit' => 'NF', 'description' => 'Nanofaradio'),
            28 => array('unit' => 'NMM', 'description' => 'Newton/Milímetro cuadrado'),
            29 => array('unit' => 'CM3', 'description' => 'Centímetro cúbico'),
            30 => array('unit' => 'DM3', 'description' => 'Decímetro cúbico'),
            31 => array('unit' => 'CFU', 'description' => 'CFU/g'),
            32 => array('unit' => 'CM', 'description' => 'Centímetros'),
            33 => array('unit' => 'CM2', 'description' => 'Centímetro cuadrado'),
            34 => array('unit' => 'CL', 'description' => 'Centilitro'),
            35 => array('unit' => 'S/M', 'description' => 'Siemens por metro'),
            36 => array('unit' => 'TOM', 'description' => 'Tonelada/Metro cúbico'),
            37 => array('unit' => 'VA', 'description' => 'Voltioamperio'),
            38 => array('unit' => 'DEG', 'description' => 'Grado'),
            39 => array('unit' => 'DM', 'description' => 'Decímetro'),
            40 => array('unit' => 'TR', 'description' => 'Tambor'),
            41 => array('unit' => 'DOC', 'description' => 'Docena'),
            42 => array('unit' => 'C/U', 'description' => 'cada uno'),
            43 => array('unit' => 'RS', 'description' => 'unites enzimas'),
            44 => array('unit' => 'EML', 'description' => 'unites enzimas/Milímetro'),
            45 => array('unit' => 'ETI', 'description' => 'Etiquetas'),
            46 => array('unit' => 'EUR', 'description' => 'Euros'),
            47 => array('unit' => 'TF', 'description' => 'Pie'),
            48 => array('unit' => 'PI2', 'description' => 'Pie cuadrado'),
            49 => array('unit' => 'PI3', 'description' => 'Pie cúbico'),
            50 => array('unit' => 'G', 'description' => 'Gramo'),
            51 => array('unit' => 'G/L', 'description' => 'Gramo/Litro'),
            52 => array('unit' => 'GaL', 'description' => 'Gramo aditivo/Litro'),
            53 => array('unit' => 'GAU', 'description' => 'Gramo oro'),
            54 => array('unit' => 'GCA', 'description' => 'g GCA/unit'),
            55 => array('unit' => 'GCO', 'description' => 'G CO2'),
            56 => array('unit' => 'GGL', 'description' => 'g glicoc/g'),
            57 => array('unit' => 'GJ', 'description' => 'Gigajulio'),
            58 => array('unit' => 'GLC', 'description' => 'mg glicoc/g'),
            59 => array('unit' => 'GLI', 'description' => 'Gramo/Litro'),
            60 => array('unit' => 'GLN', 'description' => 'Galón (EE.UU.)'),
            61 => array('unit' => 'GPM', 'description' => 'Galones por milla (EE.UU)'),
            62 => array('unit' => 'GM', 'description' => 'Gramo/Mol'),
            63 => array('unit' => 'GM2', 'description' => 'Gramo/Metro cuadrado'),
            64 => array('unit' => 'gmL', 'description' => 'g/mL'),
            65 => array('unit' => 'GPH', 'description' => 'Galones por hora (EE.UU)'),
            66 => array('unit' => 'µGV', 'description' => 'Microgramo/Metro cúbico'),
            67 => array('unit' => 'º', 'description' => 'º grados'),
            68 => array('unit' => 'GRD', 'description' => 'Grande'),
            69 => array('unit' => 'GPA', 'description' => 'Gramo principio activo'),
            70 => array('unit' => 'H', 'description' => 'Hora'),
            71 => array('unit' => 'HA', 'description' => 'Hectárea'),
            72 => array('unit' => 'HL', 'description' => 'Hectolitro'),
            73 => array('unit' => 'PUL', 'description' => 'Pulgada'),
            74 => array('unit' => '\'2', 'description' => 'Pulgada cuadrada'),
            75 => array('unit' => '\'3', 'description' => 'Pulgada cúbica'),
            76 => array('unit' => 'JHR', 'description' => 'Años (annum)'),
            77 => array('unit' => 'JKG', 'description' => 'Julio/Kilogramo'),
            78 => array('unit' => 'JKK', 'description' => 'Capac.térmica espec.'),
            79 => array('unit' => 'JMO', 'description' => 'Julio/Mol'),
            80 => array('unit' => 'BID', 'description' => 'Bidón'),
            81 => array('unit' => 'CAR', 'description' => 'Cartón'),
            82 => array('unit' => 'KG', 'description' => 'Kilogramo'),
            83 => array('unit' => 'KGM', 'description' => 'Kilogramo/Mol'),
            84 => array('unit' => 'KGS', 'description' => 'Kilogramo/Segundo'),
            85 => array('unit' => 'KGV', 'description' => 'Kilogramo/Metro cúbico'),
            86 => array('unit' => 'KGP', 'description' => 'Kilogramo principio activo'),
            87 => array('unit' => 'CA', 'description' => 'Caja'),
            88 => array('unit' => 'KJK', 'description' => 'Kilojulio/Kilogramo'),
            89 => array('unit' => 'KJM', 'description' => 'Kilojulio/Mol'),
            90 => array('unit' => 'KM', 'description' => 'Kilómetro'),
            91 => array('unit' => 'KM2', 'description' => 'Kilómetro cuadrado'),
            92 => array('unit' => 'KMH', 'description' => 'Kilómetro/Hora'),
            93 => array('unit' => 'KMN', 'description' => 'Kelvin/Minuto'),
            94 => array('unit' => 'KMS', 'description' => 'Kelvin/Segundo'),
            95 => array('unit' => 'KP', 'description' => 'Kilopondio'),
            96 => array('unit' => 'KPA', 'description' => 'Kilopascal'),
            97 => array('unit' => 'KT', 'description' => 'Kilotoneladas'),
            98 => array('unit' => 'KVA', 'description' => 'Kilovoltioamperio'),
            99 => array('unit' => 'KWK', 'description' => 'Kg principio activo/kg'),
            100 => array('unit' => 'L', 'description' => 'Litro'),
            101 => array('unit' => 'LMI', 'description' => 'Litro/Minuto'),
            102 => array('unit' => 'LB', 'description' => 'Libra (pound)'),
            103 => array('unit' => 'UP', 'description' => 'unit de potencia'),
            104 => array('unit' => 'LCK', 'description' => 'Litro por 100 km'),
            105 => array('unit' => 'LMS', 'description' => 'Litro/Molécula-segundo'),
            106 => array('unit' => 'LPH', 'description' => 'Litro por hora'),
            107 => array('unit' => 'M', 'description' => 'Metro'),
            108 => array('unit' => 'M/S', 'description' => 'Metro/Segundo'),
            109 => array('unit' => 'M2', 'description' => 'Metro cuadrado'),
            110 => array('unit' => 'm2g', 'description' => 'm2/g'),
            111 => array('unit' => 'M-2', 'description' => '1 / Metro cuadrado'),
            112 => array('unit' => 'M2S', 'description' => 'Metro cuadrado/Segundo'),
            113 => array('unit' => 'M3', 'description' => 'Metro cúbico'),
            114 => array('unit' => 'M3D', 'description' => 'Metros cúbicos/día'),
            115 => array('unit' => 'M3S', 'description' => 'Metro cúbico/Segundo'),
            116 => array('unit' => 'MC', 'description' => ''),
            117 => array('unit' => 'MEG', 'description' => 'mEq K/g'),
            118 => array('unit' => 'MEJ', 'description' => 'Megajulio'),
            119 => array('unit' => 'MG', 'description' => 'Miligramo'),
            120 => array('unit' => 'MGL', 'description' => 'Miligramo/Litro'),
            121 => array('unit' => 'MGV', 'description' => 'Miligramo/Metro cúbico'),
            122 => array('unit' => 'MGU', 'description' => 'mg/unit'),
            123 => array('unit' => 'MI', 'description' => 'Milla'),
            124 => array('unit' => 'MI2', 'description' => 'Milla cuadrada'),
            125 => array('unit' => 'µM', 'description' => 'Micrometro'),
            126 => array('unit' => 'MIN', 'description' => 'Minuto'),
            127 => array('unit' => 'MIS', 'description' => 'Microsegundo'),
            128 => array('unit' => 'mgg', 'description' => 'mg K/g'),
            129 => array('unit' => 'ML', 'description' => 'Mililitro'),
            130 => array('unit' => 'MLW', 'description' => 'Milílitro principio activo'),
            131 => array('unit' => 'MM', 'description' => 'Milímetro'),
            132 => array('unit' => 'MM2', 'description' => 'Milímetro cuadrado'),
            133 => array('unit' => 'MM3', 'description' => 'Milímetro cúbico'),
            134 => array('unit' => 'MNM', 'description' => 'Milinewton/Metro'),
            135 => array('unit' => 'MON', 'description' => 'Meses'),
            136 => array('unit' => 'MLG', 'description' => 'Millas por galón (EEUU)'),
            137 => array('unit' => 'MPS', 'description' => 'Milipascal por segundo'),
            138 => array('unit' => 'M3H', 'description' => 'Metro cúbico/Hora'),
            139 => array('unit' => 'MS', 'description' => 'Milisegundo'),
            140 => array('unit' => 'MS2', 'description' => 'Metro/Segundo al cuadrado'),
            141 => array('unit' => 'MSU', 'description' => 'mS/cm'),
            142 => array('unit' => 'MH', 'description' => 'Metro/Hora'),
            143 => array('unit' => 'MVA', 'description' => 'Megavoltioamperio'),
            144 => array('unit' => 'MWH', 'description' => 'Megavatio/Hora'),
            145 => array('unit' => 'Na2', 'description' => 'mL Na2S2O3 0,05M'),
            146 => array('unit' => 'Na5', 'description' => 'mL NaOH 0,05M'),
            147 => array('unit' => 'NAM', 'description' => 'Nanometro'),
            148 => array('unit' => 'NaO', 'description' => 'mL NaOH 0,1N'),
            149 => array('unit' => 'N/M', 'description' => 'Newton/Metro'),
            150 => array('unit' => 'NS', 'description' => 'Nanosegundo'),
            151 => array('unit' => 'NU', 'description' => 'No unit de medida'),
            152 => array('unit' => 'OCM', 'description' => 'Resistencia eléctrica espec.'),
            153 => array('unit' => 'AA', 'description' => 'Resistencia eléctrica espec.'),
            154 => array('unit' => 'OZ', 'description' => 'Onza'),
            155 => array('unit' => 'OZL', 'description' => 'Onza líquida EE.UU.'),
            156 => array('unit' => 'P', 'description' => 'Puntos'),
            157 => array('unit' => 'PAA', 'description' => 'Par'),
            158 => array('unit' => 'BTO', 'description' => 'Bulto'),
            159 => array('unit' => 'PAL', 'description' => 'Paleta'),
            160 => array('unit' => 'PAS', 'description' => 'Pascal-segundo'),
            161 => array('unit' => 'CEB', 'description' => 'Participación en grupo en %'),
            162 => array('unit' => 'PMI', 'description' => '1/minuto'),
            163 => array('unit' => 'PMR', 'description' => 'Grado de permeación SI'),
            164 => array('unit' => 'PPB', 'description' => 'Partes por billón'),
            165 => array('unit' => 'PPM', 'description' => 'Parts per million'),
            166 => array('unit' => 'PPT', 'description' => 'Partes por trillón'),
            167 => array('unit' => 'PRM', 'description' => 'Grado de permeación'),
            168 => array('unit' => 'PRS', 'description' => 'Número de personas'),
            169 => array('unit' => 'PS', 'description' => 'Picosegundo'),
            170 => array('unit' => 'PT', 'description' => 'Pinta EE.UU., líquido'),
            171 => array('unit' => 'CTO', 'description' => 'Cuarto galón, EE.UU., líquido'),
            172 => array('unit' => 'RHO', 'description' => 'Gramos/Centímetro cúbico'),
            173 => array('unit' => 'ROL', 'description' => 'Rollos'),
            174 => array('unit' => 'SOB', 'description' => 'Sobres'),
            175 => array('unit' => 'PI', 'description' => 'Pieza'),
            176 => array('unit' => 'HRA', 'description' => 'Horas'),
            177 => array('unit' => 'DÍA', 'description' => 'Días'),
            178 => array('unit' => 'TS', 'description' => 'Miles'),
            179 => array('unit' => 'T', 'description' => 'Tonelada'),
            180 => array('unit' => 'TON', 'description' => 'Tonelada EE.UU.'),
            181 => array('unit' => '001', 'description' => ''),
            182 => array('unit' => 'UA', 'description' => 'UA'),
            183 => array('unit' => 'µGL', 'description' => 'Microgramo/Litro'),
            184 => array('unit' => 'UN', 'description' => 'unit'),
            185 => array('unit' => 'MSC', 'description' => 'Microsiemens por centímetro'),
            186 => array('unit' => 'MLF', 'description' => 'Millimol por litro'),
            187 => array('unit' => 'VAL', 'description' => 'Artículo de valor'),
            188 => array('unit' => 'WCH', 'description' => 'Semanas'),
            189 => array('unit' => 'WMK', 'description' => 'Conductibilidad del calor'),
            190 => array('unit' => 'WTL', 'description' => 'Velocidad de evaporación'),
            191 => array('unit' => 'YD', 'description' => 'Yarda'),
            192 => array('unit' => 'YD2', 'description' => 'Yarda cuadrada'),
            193 => array('unit' => 'YD3', 'description' => 'Yarda cúbica'),
            194 => array('unit' => 'ZUC', 'description' => 'unit de consumo'),
            195 => array('unit' => 'kgb', 'description' => 'Kilogramo/Barril EE.UU.'),
        );
        foreach($mesureUnits as $unit){
            MesureUnit::create([
                'unit' => $unit['unit'],
                'name' => $unit['description']
            ]);
        }
    }
}
