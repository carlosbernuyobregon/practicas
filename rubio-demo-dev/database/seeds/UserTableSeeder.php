<?php

use App\Country;
use App\Role;
use App\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Str;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $data = [
            'name' => 'Admin',
            'phone' => '789789789',
            'first_name' => 'Usuario',
            'last_name' => 'Administrador',
            'email' => 'admin@example.com',
            'password' => bcrypt('secret'),
            'country_id' => Country::where('key_value', 'spain')->value('id'),
            'default_locale' => 'es',
        ];
        $user = User::create($data);
        $user->roles()->attach(Role::where('key_value', 'admin')->first());
        $user->roles()->attach(Role::where('key_value', 'administration-manager')->first());
        $data = [
            'name' => 'Lluis Piqueras',
            'phone' => '789789789',
            'first_name' => 'Lluis',
            'last_name' => 'Piqueras',
            'email' => 'lpiqueras@example.com',
            'password' => bcrypt('secret'),
            'country_id' => Country::where('key_value', 'spain')->value('id'),
            'default_locale' => 'es',
        ];
        $user = User::create($data);
        $user->roles()->attach(Role::where('key_value', 'administration-user')->first());
        $data = [
            'name' => 'Dani Planas',
            'phone' => '789789789',
            'first_name' => 'Dani',
            'last_name' => 'Planas',
            'email' => 'dplanas@example.com',
            'password' => bcrypt('secret'),
            'country_id' => Country::where('key_value', 'spain')->value('id'),
            'default_locale' => 'es',
        ];
        $user = User::create($data);
        $user->roles()->attach(Role::where('key_value', 'administration-user')->first());
        $data = [
            'name' => 'Frank Campos',
            'phone' => '789789789',
            'first_name' => 'Frank',
            'last_name' => 'Campos',
            'email' => 'fcampos@example.com',
            'password' => bcrypt('secret'),
            'country_id' => Country::where('key_value', 'spain')->value('id'),
            'default_locale' => 'es',
        ];
        $user = User::create($data);
        $user->roles()->attach(Role::where('key_value', 'administration-user')->first());
        $data = [
            'name' => 'Enric Puigdemont',
            'phone' => '789789789',
            'first_name' => 'Enric',
            'last_name' => 'Puigdemont',
            'email' => 'epuigdemont@example.com',
            'password' => bcrypt('secret'),
            'country_id' => Country::where('key_value', 'spain')->value('id'),
            'default_locale' => 'es',
        ];
        $user = User::create($data);
        $user->roles()->attach(Role::where('key_value', 'administration-user')->first());
        $data = [
            'name' => 'Albert Queralt',
            'phone' => '789789789',
            'first_name' => 'Albert',
            'last_name' => 'Queralt',
            'email' => 'aqueralt@example.com',
            'password' => bcrypt('secret'),
            'country_id' => Country::where('key_value', 'spain')->value('id'),
            'default_locale' => 'es',
        ];
        $user = User::create($data);
        $user->roles()->attach(Role::where('key_value', 'administration-user')->first());
        $data = [
            'name' => 'Sergio Sentias',
            'phone' => '789789789',
            'first_name' => 'Sergio',
            'last_name' => 'Sentias',
            'email' => 'ssentias@example.com',
            'password' => bcrypt('secret'),
            'country_id' => Country::where('key_value', 'spain')->value('id'),
            'default_locale' => 'es',
        ];
        $user = User::create($data);
        $user->roles()->attach(Role::where('key_value', 'administration-user')->first());
    }
}
