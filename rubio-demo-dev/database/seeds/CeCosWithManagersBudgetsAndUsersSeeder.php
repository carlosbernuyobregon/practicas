<?php

use App\CostCenter;
use App\Country;
use App\Role;
use App\User;
use Illuminate\Database\Seeder;
use Rap2hpoutre\FastExcel\Facades\FastExcel;

class CeCosWithManagersBudgetsAndUsersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Get the source file path
        $filePathMaster = resource_path('imports/cecos-managers-budgets-30-11-2021.xlsx');

        // Import data as collection
        $ceCosDataWithManagers  = FastExcel::sheet(2)->import($filePathMaster);
        $backUpManager          = User::where('email', 'atamayo@labrubio.com')->first();
        $backUpManagerTwo       = User::where('email', 'aramon@labrubio.com')->first();
        $backUpManagerThree     = User::where('email', 'iramirez@labrubio.com')->first();

        if (!$backUpManager || !$backUpManagerTwo || !$backUpManagerThree) {
            dd('Error. Three back up managers are needed.');
        }

        foreach ($ceCosDataWithManagers as $ceCosDataWithManager) {
            //dd($ceCosDataWithManager);
            // Get the CeCo
            $ceCo = CostCenter::where('external_id','%'.$ceCosDataWithManager['CECO'].'%')->first();

            if ($ceCo) {
                $mangerOne          = User::where('email', $ceCosDataWithManager['MAIL_RESPONSABLE'])->first();
                $mangerTwo          = User::where('email', $ceCosDataWithManager['MAIL_DIRECTOR'])->first();
                $mangerThree        = User::where('email', $ceCosDataWithManager['MAIL_DG'])->first();

                $ceCo->manager_1_id = $mangerOne->id ?? $backUpManager->id;
                $ceCo->manager_2_id = $mangerTwo->id ?? $backUpManagerTwo->id;
                $ceCo->manager_3_id = env('production') ? $mangerThree->id ?? $backUpManagerThree->id : $backUpManagerThree->id;
                $ceCo->save();
            }
        }
    }
}
