<?php

use App\OrderLine;
use Illuminate\Database\Seeder;

class AssignAccountingAccountIdToOrderLines extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Get all order lines with accounting_account_id null
        $lines = OrderLine::with(['budget.accountingAccount'])->whereNull('accounting_account_id')->get();

        foreach ($lines as $line) {
            if (!empty($line->budget->accountingAccount)) {
                $line->accounting_account_id = $line->budget->accountingAccount->id;
                $line->save();
            }
        }
    }
}
