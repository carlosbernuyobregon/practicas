<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;
use App\Http\Middleware\AdministrationRoles;
use Illuminate\Http\Client\Request as ClientRequest;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Auth::routes();
Route::get('/dev/projects','BackendController@projects');
Route::match(['get', 'post'],'ajax/process/projects/get/all','Api\ProjectController@getProjectList')->name('process.project.get.all');
Route::get('protected/files/{filename}','InvoiceController@downloadInvoice')->name('invoice.download');
Route::get('test/integration/invoices','InvoicesIntegrationController@integrateToReadsoft')->name('test');
Route::get('test/integration/returnInvoices','InvoicesIntegrationController@getFromReadsoft')->name('test2');


Route::get('/', 'HomeController@index')->name('home');

Route::group(['middleware' => 'auth'], function () {
    Route::get('user/business-line-country','UserController@businessLineCountryUserList')->name('user.business-line-country');
    Route::get('user/business-line-country/create','UserController@businessLineCountryUserCreate')->name('user.business-line-country.create');
    Route::get('user/business-line-country/{blCountryId}','UserController@businessLineCountryUserEdit')->name('user.business-line-country.edit');
    // CRUDS
    Route::resource('user-old', 'UserController');
    Route::resource('role-old', 'RoleController');
    Route::resource('permission','PermissionController');
    //Route::get('user/profile','UserController@profile')->name('profile.edit');

    Route::view('profile', 'cruds.users-new.profile')->name('profile');


    //New CRUDS
    //Only admins can access
    Route::group(['middleware' => 'admin'], function (){
        Route::get('users/', 'CrudController@users')->name('cruds.users');
        Route::get('roles/', 'CrudController@roles')->name('cruds.roles');
        Route::get('countries/', 'CrudController@countries')->name('cruds.countries');
        Route::get('currencies/', 'CrudController@currencies')->name('cruds.currencies');
        Route::get('tax-types/', 'CrudController@taxTypes')->name('cruds.tax-types');
        Route::get('cost-centers/', 'CrudController@costCenters')->name('cruds.cost-centers');
        Route::get('budgets/', 'CrudController@budgets')->name('cruds.budgets');
        Route::get('accounting-accounts/', 'CrudController@accountingAccounts')->name('cruds.accounting-accounts');
        Route::get('materials/', 'CrudController@materials')->name('cruds.materials');
        Route::get('assets-management/', 'CrudController@assets')->name('cruds.assets');
        Route::get('provider-payment-conditions/', 'CrudController@providerPaymentConditions')->name('cruds.provider-payment-conditions');
        Route::get('provider-payment-types/', 'CrudController@providerPaymentTypes')->name('cruds.provider_payment_types');
//        Route::get('orders/', 'CrudController@orders')->name('cruds.orders');
    });
    Route::get('suppliers/', 'CrudController@suppliers')->name('cruds.suppliers');

    // COMMON PAGES
    Route::get('dashboard','BackendController@dashboard')->name('dashboard');
    //Route::get('tasks/list','BackendController@tasksList')->name('tasks.list');
    Route::get('tasks','ProcessController@getMyTask')->name('tasks.all.list');

    //Only users with administration roles can access
    Route::group(['middleware' => 'administrationRoles'], function() {
        Route::get('tasks','ProcessController@getMyTask')->name('tasks.list');
    });

    Route::get('tasks/list','ProcessController@getTasks')->name('tasks.old.list');
    Route::get('tasks/all/list','ProcessController@getAllTasks')->name('tasks-old.all.list');
    Route::get('tasks/list/submited','BackendController@tasksListSubmited')->name('tasks.list.submited');
    Route::get('locale/{locale}','BackendController@setLocale')->name('utils.lang.set');
    //Route::get('projects','ProjectController@index')->name('projects.dashboard');
    //Route::get('projects/tasks','ProjectController@tasks')->name('projects.tasks');


    // PROCESSES ROUTES
    Route::get('/process/{processName}/{processInstance?}/{processId?}/{processActivity?}',function($processName,$processInstance = null, $processId = null, $processActivity = null, Request $request){
        $app = app();
        $controller = $app->make('App\Http\Controllers\\'.ucfirst(strtolower($processName)).'Controller');
        $parameters = [
            'processName' => $processName,
            'processId' => $processId,
            'processInstance' => $processInstance,
            'processActivity' => $processActivity,
            'request' => $request,
        ];
        return $controller->callAction('open',$parameters);
    })->name('process.open');
    Route::get('/review/{processName}/{processId?}/{processActivity?}',function($processName,$processId = null, $processActivity = null, Request $request){
        $app = app();
        $controller = $app->make('App\Http\Controllers\\'.ucfirst(strtolower($processName)).'Controller');
        //$controller = $app->make('App\Http\Controllers\\RequestController');
        $parameters = [
        'processName' => $processName,
        'processId' => $processId,
        'processActivity' => $processActivity,
        'request' => $request,
        ];
        return $controller->callAction('review',$parameters);
    })->name('process.review');
    Route::match(['put', 'post'], '/process/{processName}/{processId?}', function ($processName, $processId = null, Request $request) {
        $app = app();
        $controller = $app->make('App\Http\Controllers\\'.ucfirst(strtolower($processName)).'Controller');
        $parameters = [
            'processName' => $processName,
            'processId' => $processId,
            'request' => $request,
        ];
        return $controller->callAction('proceed',$parameters = $parameters);
    })->name('process.proceed');
    Route::post('process/reassign/{sn}','ProcessController@reassignTask')->name('process.reassign');
    Route::post('process/workflow/end','ProcessController@endWorkflow')->name('process.workflow.end');

    // AJAX ROUTES
    Route::get('ajax/currency','BackendController@getCurrencyByCountry');
    //Route::get('ajax/process/quote-line','RequestController@getQuoteLine')->name('process.request.quote.lines');
    /* RequestController doesn't exist
    Route::get('ajax/process/quote/{quote}','RequestController@quoteLinesByQuote')->name('process.request.quote.lines');
    Route::get('ajax/process/quote/total/{quote?}','RequestController@quoteTotal')->name('process.request.quote.total');
    Route::get('ajax/process/quote/line/{quote?}','RequestController@quoteLineDetail')->name('process.request.quote.line.detail');
    Route::get('ajax/process/quote/line/remove/{quote?}','RequestController@quoteLineRemove')->name('process.request.quote.line.remove');
    Route::get('ajax/process/request/{processRequest}/quotes','RequestController@listQuotesByRequest')->name('process.request.quotes');
    Route::post('ajax/process/quote-line','RequestController@addQuoteLine')->name('process.request.quote.add'); */


    Route::post('ajax/files/upload/{model}/{id}','FileController@fileUpload')->name('files.upload');
    Route::post('ajax/files/delete/{id?}','FileController@fileDelete')->name('files.delete');
    Route::get('ajax/files/view/list/{type}/{modelId}','FileController@fileGetList')->name('files.list.type');


    // PUBLIC FILE ROUTES
    Route::get('public/files/view/{id}','FileController@fileView')->name('files.view');
    Route::get('public/files/download/{id}','FileController@fileDownload')->name('files.download');

    // DOCS
    //Route::get('generate/docs/quotation/{processRequest}','RequestController@downloadQuotation')->name('generate.docs.quotation');



    // PROVISIONAL
    Route::get('provisional/validation','BackendController@validationPage')->name('provisional.validation');
    Route::get('test','BackendController@testPage')->name('test.page');
    Route::view('test/live','test.projects.livewire')->name('test.live');

    Route::view('test/refactor/requests','test.refactor.requests');
    //Route::get('dev/design-upload','ProjectController@design');


    Route::get('invoices/processed','InvoiceController@processed')->name('invoice.processed');

    //Only users with administration roles can access
    Route::group(['middleware' => 'administrationRoles'], function() {
        Route::get('invoices/all','InvoiceController@all')->name('invoice.all');
    });

    //Only admins can access
    Route::group(['middleware' => 'mixedAdmin'], function () {
        Route::get('invoices/pending', 'InvoiceController@pending')->name('invoice.pending');
        Route::get('invoices/list','InvoiceController@list')->name('invoice.list');
    });

    Route::get('invoices/{invoice}/json','InvoiceController@showMeTheJson')->name('invoice.json');
    Route::get('invoices/{invoice}/review','InvoiceController@review')->name('invoice.review');
    Route::get('invoices/{invoice}','InvoiceController@show')->name('invoice.show');
    Route::get('orders','OrderController@index')->name('order.index');
    Route::get('orders/dashboard','OrderController@dashboard')->name('order.dashboard');
    Route::get('orders/manager','OrderController@indexAdmin')->name('order.indexAdmin');
    Route::get('orders/new','OrderController@new')->name('order.new');
    Route::get('orders/{order}','OrderController@edit')->name('order.edit');
    Route::get('orders/show/{order}','OrderController@show')->name('order.show');
    Route::view('/new-provider','orders.new-providers')->name('supplier.add-new-direct');
    Route::view('orders/tasks/new', 'orders.tasks')->name('order.tasks');
    Route::get('task-list/orders/status/{status}','OrderController@test')->name('order.taskList');


    Route::get('files/download/{file}','FileController@fileDownload')->name('file.download');
    Route::get('testing/','OrderController@test');
    Route::get('testing/order/{order}','OrderController@test');

    Route::view('/process-task-list', 'processes.task-list')->name('processes.task-list');
    Route::get('order/document/{order}','OrderController@orderFile')->name('order.document');

});

