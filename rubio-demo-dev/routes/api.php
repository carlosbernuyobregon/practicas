<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::prefix('v1')->group(function () {
    Route::get('process/project/create/quotation/{quoteId}','Api\ProjectController@createFromQuoteId');

    Route::get('process/project/{project}/assign/packaging','Api\ProjectController@assignPackagingProjects');
    Route::get('process/project/{project}/subprojects','Api\ProjectController@getSubprojects');
    Route::get('process/project/{project}/subprojects/packaging','Api\ProjectController@getPackagignSubprojects');
    Route::get('process/project/{project}/subprojects/product','Api\ProjectController@getNonPackagingSubrojects');
    Route::post('process/projects/get/all','Api\ProjectController@getProjectList');
    Route::get('process/project/generatePublicCodes',                               'Api\ProjectController@generatePublicCodes');

    // Export
    Route::get('integration/{publicApiKey}/invoices/',                              'Api\IntegrationController@invoicesToSap')->name('integration.invoices-to-sap');
    Route::get('integration/{publicApiKey}/orders/',                                'Api\IntegrationController@ordersToSap')->name('integration.orders-to-sap');
    Route::get('integration/{publicApiKey}/providers/',                             'Api\IntegrationController@providersToSap')->name('integration.providers-to-sap');
    Route::get('integration/{publicApiKey}/providers/confirm',                      'Api\IntegrationController@providersToSap')->name('integration.providers-to-sap');
    Route::get('integration/{publicApiKey}/commodities',                            'Api\IntegrationController@commoditiesToSap')->name('integration.commodities-to-sap');
    // Import
    Route::post('integration/{publicApiKey}/master-data/import/cost-center',        'Api\IntegrationController@costCenterImport')->name('integration.import.cost-center');
    Route::post('integration/{publicApiKey}/master-data/import/accounting-account', 'Api\IntegrationController@accountingAccountImport')->name('integration.import.accounting-account');
    Route::post('integration/{publicApiKey}/master-data/import/material',           'Api\IntegrationController@materialImport')->name('integration.import.material');
    Route::post('integration/{publicApiKey}/master-data/import/supplier',           'Api\IntegrationController@supplierImport')->name('integration.import.supplier');
    Route::post('integration/{publicApiKey}/master-data/import/order',              'Api\IntegrationController@orderImport')->name('integration.import.order');
    Route::post('integration/{publicApiKey}/master-data/import/asset',              'Api\IntegrationController@assetImport')->name('integration.import.asset');
    Route::post('integration/{publicApiKey}/master-data/import/pep-element',        'Api\IntegrationController@pepElementImport')->name('integration.import.pep-element');
    Route::post('integration/{publicApiKey}/master-data/import/budget',             'Api\IntegrationController@budgetImport')->name('integration.import.budget');
    Route::post('integration/{publicApiKey}/master-data/import/taxes',              'Api\IntegrationController@taxesImport')->name('integration.import.taxes');
//    Route::post('integration/{publicApiKey}/master-data/import/orders',             'Api\IntegrationController@ordersImport')->name('integration.import.orders');
//    Route::post('integration/{publicApiKey}/master-data/import/expenses',           'Api\IntegrationController@epensesImport')->name('integration.import.expenses');

    // Responses
    Route::post('integration/{publicApiKey}/invoices/{invoice}/set',                'Api\IntegrationController@invoiceUpdateFromSap')->name('integration.invoices-confirmation');
    Route::post('integration/{publicApiKey}/orders/{order}/set',                    'Api\IntegrationController@orderUpdateFromSap')->name('integration.orders-confirmation');
    Route::post('integration/{publicApiKey}/commodities/{delivery}/set',            'Api\IntegrationController@commodityUpdateFromSap')->name('integration.commodities-confirmation');


    // Camunda api
    Route::post('integration/{publicApiKey}/process/order/{order}/task/assignee',       'Api\Process\OrderController@userAssignee');
    Route::post('integration/{publicApiKey}/process/order/{order}/set/status',          'Api\Process\OrderController@setStatus');
    Route::post('integration/{publicApiKey}/process/order/{order}/set/audit',           'Api\Process\OrderController@setAudit');
    Route::post('integration/{publicApiKey}/process/order/{order}/set/confirm',         'Api\Process\OrderController@confirmOrder');
    Route::post('integration/{publicApiKey}/process/provider/{provider}/set/status',    'Api\Process\SupplierController@setStatus');
});
